//const ipcRenderer = require('electron').ipcRenderer;
const {webFrame, ipcRenderer} = require('electron');

var colorTypes = [ "#F7FE2E", "#FA8258", "#58FA82", "#81BEF7", "#888888" ]; 
var colorIndex = 0;

webFrame.setVisualZoomLevelLimits(1, 1);
webFrame.setLayoutZoomLevelLimits(0, 0);

ipcRenderer.on('asynchronous-message', (event, arg) => {
	console.log('received msg from ui process: ', arg);

	let msg = document.getElementById("message");
	msg.innerHTML = arg;
	ipcRenderer.send('asynchronous-message', 'success');
});

function initialize() 
{
	console.log('init');

	back = document.getElementById("back");
	back.addEventListener("click", changeColor);
}

function changeColor ()
{
	console.log('color: ', colorTypes[colorIndex]);

	colorIndex = (colorIndex + 1) % colorTypes.length;
	back.style.backgroundColor = colorTypes[colorIndex];
}
