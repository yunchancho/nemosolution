var fs = require('fs');
var fsExtra = require('fs-extra');
var http = require('http');
var { exec, spawn } = require('child_process')
var Q = require('q');
var winston = require('winston');
var _ = require('underscore');
var mime = require('mime');
var telebot = require('node-telegram-bot-api');

var bot;
var tokenPath = '/etc/nemosnow/botkey'
var shareDirPrefix = '/samba/chatbot';
var nativeImageApp = '/opt/pkgs/nemo.image/exec';
var nativeVideoApp = '/opt/pkgs/nemo.player/exec';
var nativePdfApp = '/opt/pkgs/nemo.pdf/exec';
var electronApp = '/usr/bin/electron';
var msDocViewerUrl = 'https://view.officeapps.live.com/op/view.aspx?src=';
var googleMapUrl = 'http://maps.google.com/maps?q=';
var googleMapUrl2 = 'http://www.google.co.kr/maps/@';
var postItUrl = '/opt/pkgs/nemo.ai-chatbot/postit';
var containerUrl = '/opt/pkgs/nemo.web-container';
var instagramUrl = 'http://www.instagram.com/explore/tags/';
var findfoodUrl = 'http://www.mangoplate.com/search/';
var googlingUrl = 'http://www.google.co.kr/search?q=';
const { ShellManager } = require('./shell-manager')

const shell = new ShellManager()
shell.initialize()

function handleVideo (msg) {
	console.log(msg);
	bot.downloadFile(msg.video.file_id, shareDirPrefix)
	.then(function (filePath) {
		console.log('downloaded:', filePath);
    shell.run({ type: "app", path: nativeVideoApp, args: [ '-f', filePath ] })
	})
	.catch(function (err) {
		console.log(err.stack);
	});
}

function handlePhoto (msg) {
	console.log(msg);
	var origin = msg.photo.pop();
	console.log('origin photo: ', origin);
	bot.downloadFile(origin.file_id, shareDirPrefix)
	.then(function (filePath) {
		console.log('downloaded:', filePath);
    shell.run({ type: "app", path: nativeImageApp, args: [ '-f', filePath ] })
	})
	.catch(function (err) {
		console.log(err.stack);
	});
}

function handleVoice(msg) {
	console.log(msg);
	bot.downloadFile(msg.voice.file_id, shareDirPrefix)
	.then(function (filePath) {
		console.log('downloaded:', filePath);
    shell.run({ type: "app", path: nativeVideoApp, args: [ '-f', filePath ] })
	})
	.catch(function (err) {
		console.log(err.stack);
	});
}

function handleAudio(msg) {
	console.log(msg);
	bot.downloadFile(msg.audio.file_id, shareDirPrefix)
	.then(function (filePath) {
		console.log('downloaded:', filePath);
    shell.run({ type: "app", path: nativeVideoApp, args: [ '-f', filePath ] })
	})
	.catch(function (err) {
		console.log(err.stack);
	});
}

function handleDocument(msg) {
	console.log(msg);

	bot.getFileLink(msg.document.file_id)
	.then(function (url) {
		console.log(url);
		var type = msg.document.mime_type;
		if (type.indexOf('pdf') >= 0) {
			bot.downloadFile(msg.document.file_id, shareDirPrefix)
			.then(function (filePath) {
				//spawn(electronApp, ['--touch-events', msDocViewerUrl + url]);
				//exec(nativeWebBrowser2 + url);
        shell.run({ type: "app", path: nativePdfApp, args: [ '-f', filePath ] })
			})
			.catch(function (err) {
				console.log(err);
			});
		} else if (type.indexOf('officedocument') >= 0) {
      shell.run({
        type: "xapp",
        path: electronApp,
        args: [ '--touch-events', containerUrl, `${msDocViewerUrl}${url}` ]
      })

		} else if (type.indexOf('image') >= 0) {
			bot.downloadFile(msg.document.file_id, shareDirPrefix)
			.then(function (filePath) {
        shell.run({ type: "app", path: nativeImageApp, args: ['-f', filePath ] })
			})
			.catch(function (err) {
				console.log(err);
			});
		} else if (type.indexOf('video') >= 0) {
			bot.downloadFile(msg.document.file_id, shareDirPrefix)
			.then(function (filePath) {
        shell.run({ type: "app", path: nativeVideoApp, args: [ '-f', filePath ] })
			})
			.catch(function (err) {
				console.log(err);
			});

		} else {
			console.log('not recognized mime type:', type);
		}
	})
	.catch(function (err) {
		console.log(err);
	});
}

function handleText(msg) {
	console.log(msg);
  if (!msg.entities) {
    shell.run({ 
      type: "xapp",
      path: electronApp,
      args: ['--touch-events', postItUrl, msg.text ]
    })
    return 
  }

	_.each(msg.entities, function (entity) {
    console.log("text entity.type: ", entity.type)
		if (entity.type == 'url') {
			var url = msg.text.substring(entity.offset, entity.offset + entity.length);
			console.log('url:', url);
      shell.run({ type: "xapp", path: electronApp, args: ['--touch-events', url ] })
		}
	});
}

function handleLocation(msg) {
	console.log(msg);
	var query = msg.location.latitude + ',' + msg.location.longitude;
	var url = googleMapUrl + query;

  // with container?
  shell.run({ type: "xapp", path: electronApp, args: ['--touch-events', url ] })
}

function handlePostit(msg, match) {
	console.log(msg);
	let message = "포스트잇 메시지를 적어주세요.";
	bot.sendMessage(msg.from.id, message, {
    reply_markup: {
			force_reply: true
    }
  })
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

    bot.onReplyToMessage(chatId, messageId, function (reply) {
      shell.run({ 
        type: "xapp",
        path: electronApp,
        args: ['--touch-events', postItUrl, reply.text ]
      })
    });
	})
	.catch(function (err) {
		console.log(err);
	});
}

function handleFindinsta(msg, match) {
	console.log(msg);
	let message = "검색할 태그를 적어주세요(# 빼고)";
	bot.sendMessage(msg.from.id, message, {
    reply_markup: {
			force_reply: true
    }
  })
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

		bot.onReplyToMessage(chatId, messageId, function (reply) {
			_.each(reply.text.split(' '), (tag) => {
        shell.run({ 
          type: "xapp",
          path: electronApp,
          args: ['--touch-events', containerUrl, instagramUrl + tag ]
        })
			});
		});
	})
	.catch(function (err) {
		console.log(err);
	});
}

function handleFindfood(msg, match) {
	console.log(msg);
	let message = "검색할 음식 또는 지역을 적어주세요";
	bot.sendMessage(msg.from.id, message, {
    reply_markup: {
			force_reply: true
    }
  })
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

		bot.onReplyToMessage(chatId, messageId, function (reply) {
      shell.run({ 
        type: "xapp",
        path: electronApp,
        args: ['--touch-events', containerUrl, findfoodUrl + reply.text ]
      })
		});
	})
	.catch(function (err) {
		console.log(err);
	});
}

function handleGoogling(msg, match) {
	console.log(msg);
	let message = "검색어를 적어주세요";
	bot.sendMessage(msg.from.id, message, {
    reply_markup: {
			force_reply: true
    }
  })
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

		bot.onReplyToMessage(chatId, messageId, function (reply) {
			let url = googlingUrl + reply.text;
      shell.run({ 
        type: "xapp",
        path: electronApp,
        args: ['--touch-events', containerUrl, url ]
      })
		});
	})
	.catch(function (err) {
		console.log(err);
	});
}

var initialize = function () {
  let token = null
  try {
    token = fs.readFileSync(tokenPath, { encoding: 'utf8' })
  } catch (err) {
    console.log('There is not valid chatbot key')
    process.exit(-1)
  }

	bot = new telebot(token.trim(), { polling: true });
	bot.getMe().then(function (msg) {
		winston.log('bot info: ', JSON.stringify(msg));
	}).catch(function (err) {
		winston.log('bot err: ', err);
	});

  Q.nfcall(fs.stat, shareDirPrefix)
	.then(function (stats) {
		console.log(shareDirPrefix, 'exists already');
	})
	.catch(function (err) {
    exec(`mkdir -p ${shareDirPrefix}`, (err, stdout, stderr) => {
		  fs.chmodSync(shareDirPrefix, 0777);
    })
	})
	.finally(function () {
		bot.on('video', handleVideo);
		bot.on('audio', handleAudio);
		bot.on('voice', handleVoice);
		bot.on('photo', handlePhoto);
		bot.on('document', handleDocument);
		bot.on('text', handleText);
		bot.on('location', handleLocation);
		bot.onText(/\/postit/, handlePostit);
		bot.onText(/\/findinsta/, handleFindinsta);
		bot.onText(/\/findfood/, handleFindfood);
		bot.onText(/\/googling/, handleGoogling);
	});

	return true;
}

module.exports.run = initialize;
