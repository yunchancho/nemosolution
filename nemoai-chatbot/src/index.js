var winston = require('winston');
var bot = require('./bot');

// setting winston log
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {
	colorize: true,
	timestamp: true,
	humanReadableUnhandledException: true,
});

if (!bot.run()) {
	process.exit(-1);
}

process.on('unhandledRejection', function (reason, p) {
	winston.info("Unhandled Rejection at: ", p, " reason: ", reason);
});

process.on('uncaughtException', function (err) {
	winston.info('Uncaught Exception: ', err);
});
