const net = require('net')

const defaultSocketOptions = {
  path: "/tmp/nemo.bus.0"

}

class ShellManager {
  constructor(socketOptions) {
    this.socket = null
    this.actions = []
    this.socketOptions = socketOptions || defaultSocketOptions
  }

  initialize() {
    try {
      this.socket = net.connect(this.socketOptions, () => console.log('connected to nemoshell'))
      this.socket.on('connect', () => console.log('connected to nemoshell'))
      this.socket.on('error', (error) => console.log(error))
      this.socket.on('end', () => console.log('disconnected from nemoshell'))
    } catch (err) {
      console.log('nemoshell socket error: ', err)
    }
  }

  run(meta) {
    let payload = {
      from: "/nemospeech",
      to: "/nemoshell",
      command: {
        pitchscreen: meta.pickscreen || "off",
        pickscreen: meta.pickscreen || "off",
        layer: meta.layer || "on",
        keypad: meta.keypad || "on",
        resize: meta.resize || "on",
        scale: meta.scale || "on",
        type: meta.type,
        path: meta.path,
        args: meta.args.join(';')
      }
    }

    //console.log('payload: ', payload)
    this.socket.write(JSON.stringify(payload), () => console.log('sent data to nemoshell!'))
  }

  test() {
    // This is just for test of running application by nemoshell
    this.run({})
  }
}

module.exports = { ShellManager }
