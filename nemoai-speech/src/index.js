const { SpeechManager } = require('./lib/speech-manager')
const { ActionManager } = require('./lib/action-manager')

const am = new ActionManager()
const sm = new SpeechManager()

async function coroutine() {
  await am.initialize()
  //am.test()

  sm.initialize((stt) => { stt && am.do(stt) })
  sm.run()
}

coroutine()

console.log('start voice recognition service...')
