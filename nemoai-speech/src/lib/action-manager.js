const net = require('net')
const mongo = require('mongodb');
const Promise = require('bluebird')
const MongoClient = Promise.promisifyAll(mongo).MongoClient;
const meta = require('../meta.json')
const nodePackage = require('../../manifest.json')

const defaultSocketOptions = {
  path: meta.nemoBusSocketPath
}

class ActionManager {
  constructor(socketOptions) {
    this.socket = null
    this.actions = []
    this.socketOptions = socketOptions || defaultSocketOptions
  }

  initialize() {
    async function co(self) {
      try {
        self.socket = net.connect(self.socketOptions, () => console.log('connected to nemoshell'))
        self.socket.on('connect', () => console.log('connected to nemoshell'))
        self.socket.on('error', (error) => console.log(error))
        self.socket.on('end', () => console.log('disconnected from nemoshell'))
        self.actions = await getActions()
        console.log(self.actions)
      } catch (err) {
        console.log('nemoshell socket error: ', err)
      }
    }
    return co(this)
  }

  do(stt) {
    console.log(`stt from google: ${stt}`)
    for (let action of this.actions) {
      if (action.slots.includes(stt.toLowerCase().replace(/\s+/g, ''))) {
        try {
          runApplication(this.socket, action.application)
        } catch (err) {
          console.log(err)
        }
      }
    }
  }

  test() {
    // This is just for test of running application by nemoshell
    runApplication(this.socket, this.actions[0].application)
  }
}

function runApplication(socket, meta) {
  let payload = {
    from: "/nemospeech",
    to: "/nemoshell",
    command: {
      pitchscreen: meta.pickscreen || "off",
      pickscreen: meta.pickscreen || "off",
      layer: meta.layer || "on",
      keypad: meta.keypad || "on",
      resize: meta.resize || "on",
      scale: meta.scale || "on",
      type: meta.type,
      path: meta.path,
      args: meta.args.join(';')
    }
  }

  //console.log('payload: ', payload)
  socket.write(JSON.stringify(payload), () => console.log('sent data to nemoshell!'))
}

function getActions() {
  async function co() {
    try {
      const db = await MongoClient.connectAsync(meta.nemodbPath)
      let docs = await db.collection(nodePackage.pkgname).find({}).toArrayAsync()

      if (docs[0] && docs[0].actions.length) {
        return docs[0].actions
      }
    } catch (err) {
      console.log('mongo error:', err)
    }

    //TODO we need to read local db first.
    const defaultActionsPath = `${__dirname}/../../${meta.defaultActionsPath}`
    let actions = require(defaultActionsPath)

    return actions
  }

  return co()
}

module.exports = { ActionManager }
