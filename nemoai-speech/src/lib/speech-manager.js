// This env should be set before running speech api
process.env['GOOGLE_APPLICATION_CREDENTIALS'] = `${__dirname}/../../configs/credentials.json`

const record = require('node-record-lpcm16')
const speech = require('@google-cloud/speech')()

const encoding = 'LINEAR16'
const sampleRate = 16000
const resetIntervalMsec = 60000

class SpeechManager {
  constructor(option) {
    this.speechOption = {
      config: {
        encoding: encoding,
        sampleRate: sampleRate,
        languageCode: 'en-US'
      },
      //singleUtterance: false,
      //interimResults: false
    }

    this.speechOption = Object.assign(this.speechOption, option)
    this.voiceStream = null
    this.actionCb = () => {}
  }

  initialize(actionCb) {
    if (actionCb && typeof actionCb === 'function') {
      this.actionCb = actionCb
    }
  }

  run() {
    async function co(self) {
      try {
        streamVoice(self.speechOption, self.actionCb)
        setInterval(() => {
          streamVoice(self.speechOption, self.actionCb)
        }, resetIntervalMsec)
      } catch (err) {
        console.log('speech recognition error')
        Promise.reject(err)
      }
    }
    return co(this)
  }
}

function streamVoice (speechOption, actionCb) {
  console.log('new voice stream created...')
  const voiceStream = speech.createRecognizeStream(speechOption)
  voiceStream.on('error', console.error)
  voiceStream.on('data', (data) => actionCb(data.results.trim()))

  record.stop()
  record.start({ sampleRate: sampleRate, threshold: 0 }).pipe(voiceStream)
}

module.exports = { SpeechManager }
