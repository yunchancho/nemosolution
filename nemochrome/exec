#!/usr/bin/env node

const { exec, spawn } = require('child_process')
const Promise = require('bluebird')
const mongo = require('mongodb');
const MongoClient = Promise.promisifyAll(mongo).MongoClient;
const deepMerge = require('deepmerge')
const ffi = require('ffi')

const meta = require('./meta.json')
const nodePackage = require('./manifest.json')

let libc_v0 = ffi.Library('libc', {
  "execl": [ "int", [ "string", "string", "int" ] ],
})

let libc_v1 = ffi.Library('libc', {
  "execl": [ "int", [ "string", "string", "string", "int" ] ],
})

let libc_v2 = ffi.Library('libc', {
  "execl": [ "int", [ "string", "string", "string", "string", "int" ] ],
})

async function run() {
  let db = await MongoClient.connectAsync(meta.nemodbPath)
  let docs = await db.collection(nodePackage.pkgname).find({}).toArrayAsync()
  let appConfig = nodePackage.config
  if (docs.length) {
    appConfig = deepMerge(appConfig, docs[0].config)
  } 

  let args = []

  // only url can be passed as param
  if (process.argv.slice(2).length) {
    args.push(process.argv.slice(2)[0])
  }

  if (appConfig.enableTouchEvents) {
    args.push('--touch-events')
  }

  console.log(args)

  if (args.length === 0) {
    libc_v0.execl(meta.chromePath, meta.chromePath, 0)
  } else if (args.length === 1) {
    libc_v1.execl(meta.chromePath, meta.chromePath, ...args, 0)
  } else if (args.length === 2) {
    libc_v2.execl(meta.chromePath, meta.chromePath, ...args, 0)
  } else {
    console.error('unknown arguments')
    process.exit(-1)
  }
}

run()
