const fs = require('fs')
const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const usbmem = require('./lib/usbmem')
const jwt = require('./lib/jwtoken')
const setting = require('./lib/setting')
const meta = require('./meta.json')

// mount all inserted usb memory
usbmem.mountAll().catch(err => console.log(`error: ${err}`))

let app = express()

app.use('/', express.static(path.join(__dirname, `/../${meta.clientDirname}`)))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "X-Requested-With")
  res.header('Access-Control-Allow-Headers', 'Content-Type')
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
  next()
})

for (let api of Object.keys(setting.apis)) {
	if (!api.match(/auth|device/)) {
		app.all(`/${api}/*`, jwt.verifyAccessTokenMiddleware)
	}
  const apiModule = path.join(__dirname, `./routes/${api}`)
	app.use(`/${api}`, require(apiModule))
}

const server = app.listen(meta.port, function () {
  let host = server.address().address
  let port = server.address().port
  console.log(`node server at http://${host}:${port}`)
})
