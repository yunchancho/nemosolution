const co = require("co");
const Promise = require("bluebird");
const AWS = require('aws-sdk');

class DynamoDb {
	constructor(param) {
		this._table = param.swKey;
		this._projection = {
			attrs: ['#n', '#v'],
			name: { '#n': 'name', '#v': 'value' }
		}

		if (param.swMode != 'cloud') {
			AWS.config.update({
				region: param.region,
				endpoint: param.endpoint
			});
		} else {
			AWS.config.update({
				region: param.region,
				accessKeyId: param.secrets.keyId,
				secretAccessKey: param.secrets.accessKey
			});
		}

		this.dynamodb = Promise.promisifyAll(new AWS.DynamoDB());
		this.dynamodoc = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());
	}

	initialize() {
		const param = { TableName: this._table }
		const initCoroutine = co.wrap(function* (self) {
			try {
				yield self.dynamodb.describeTableAsync(param);
			} catch (err) {
				throw `${self._table} table is invalid`;
			}
			console.log('db initialized');
			return true;
		});

		return initCoroutine(this);
	}

	getAll(option) {
		const param = { 
			TableName: this._table,
			ProjectionExpression: this._projection.attrs.join(','),
			ExpressionAttributeNames: this._projection.name
		};
		if (option && option.exceptList) {
			param.ScanFilter = {
				name: {
					ComparisonOperator: 'NE',
					AttributeValueList: option.exceptList
				}
			};
		}

		let coroutine = co.wrap(function* (self) {
			let docs = [];
			try {
				let result = yield self.dynamodoc.scanAsync(param);
				//console.log(`dynamodb result: ${JSON.stringify(result, null, 2)}`);

				for (let doc of result.Items) {
					docs.push(doc);
				}
			} catch (err) {
				throw err;
			}

			return docs;
		});


		return coroutine(this);
	}

	get(collection) {
		const param = { 
			TableName: this._table,
		 	Key: {
			 	name: collection
		 	},
			ProjectionExpression: this._projection.attrs.join(','),
			ExpressionAttributeNames: this._projection.name
		}

		let coroutine = co.wrap(function* (self) {
			let doc;
			try {
				doc = yield self.dynamodoc.getAsync(param);
			} catch (err) {
				throw err;
			}

			return { name: doc.name, value: doc.value }
		});

		return coroutine(this);
	}

	setAll(collectionObjs) {
		if (!collectionObjs.length) {
			return Promise.resolve('none');
		}

		// collectionObj must have 'name', 'value' attribute and their value
		let param = {
			RequestItems: {
			}
		}

		param.RequestItems[this._table] = [];

		for (let collection of collectionObjs) {
			let request = {
				PutRequest: {
					Item: collection
				}
			}

			param.RequestItems[this._table].push(request);
		}

		return this.dynamodoc.batchWriteAsync(param);
	}

	set(collection, docs) {
		const param = {
			TableName: this._table,
			Item: {
				name: collection,
				value: docs 
			}
		}

		let coroutine = co.wrap(function* (self) {
			let ret;
			try {
				ret = yield self.dynamodoc.putAsync(param);
			} catch (err) {
				throw err;
			}

			return ret; 
		});

		return coroutine(this);
	}

	unset(collection) {
		const param = { 
			TableName: this._table,
		 	Key: {
			 	name: collection
		 	}
		}

		let coroutine = co.wrap(function* (self) {
			let ret;
			try {
				ret = yield self.dynamodoc.deleteAsync(param);
			} catch (err) {
				throw err;
			}

			return ret; 
		});

		return coroutine(this);
	}

	unsetAll(collectionObjs) {
		if (!collectionObjs.length) {
			return Promise.resolve('none');
		}

		let param = {
			RequestItems: {
			}
		}

		param.RequestItems[this._table] = [];

		for (let collection of collectionObjs) {
			let request = {
				DeleteRequest: {
					Key: {
					 	name: collection.name
					}
				}
			}
			param.RequestItems[this._table].push(request);
		}

		return this.dynamodoc.batchWriteAsync(param);
	}

	isExisted() {
		const param = { TableName: this._table }
		return dynamodb.describeTableAsync(param);
	}
}

module.exports = {
	DynamoDb
}
