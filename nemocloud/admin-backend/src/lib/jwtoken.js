const co = require('co')
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'))
const jwt = require('jsonwebtoken')
const meta = require('../meta.json')

function create(userData, expiredSec) {
	let payload = { data: userData }

	if (expiredSec) {
		payload.exp = Math.floor(Date.now() / 1000) + expiredSec
	}

	let coroutine = co.wrap(function* () {
		let hash = yield fs.readFileAsync(meta.localInfraSecretFilePath, { encoding: 'utf8' })
		let token = jwt.sign(payload, hash)
		console.log(`new token is ${token}`)
		return token
	})

	return coroutine()
}

function verify(token) {
	let coroutine = co.wrap(function* () {
		let hash = yield fs.readFileAsync(meta.localInfraSecretFilePath, { encoding: 'utf8' })
		yield new Promise((resolve, reject) => {
			jwt.verify(token, hash, (err, decoded) => {
				if (err) {
					console.log(`failed to verify jwt: ${err.stack}`)
					return reject(err)
				}
				console.log(`decoded userdata: ${decoded}`)
				resolve()
			})
		})
		return true
	})

	return coroutine()
}

function verifyAccessTokenMiddleware(req, res, next) {
	console.log('verify token middleware')
	let token = req.body.token || req.query.token || req.headers['x-access-token']

	if (!token) {
		return res.status(403).json({
			code: 'AUTH_ERROR_ACCESS_TOKEN_EMPTY'
		})
	}
	
	verify(token).then(result => next())
	.catch(err => {
		res.status(403).json({ code: 'AUTH_ERROR_ACCESS_TOKEN_INVALID' })
	})
}

module.exports = { create, verify, verifyAccessTokenMiddleware }
