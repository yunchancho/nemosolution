const co = require('co')
const AWS = require('aws-sdk')
const Promise = require('bluebird')
const dns = Promise.promisifyAll(require('dns'))
const systeminfo = require('systeminformation')
const meta = require('../meta.json')

function invoke(deviceInfo) {
	AWS.config.update({
		region: meta.awsRegion,
	})

	let lambda = new AWS.Lambda()
	let coroutine = co.wrap(function* () {
    // check internet is available first
		yield dns.resolveAsync('snow.nemoux.net') 
		let hw = yield systeminfo.system()

		deviceInfo.hwKey = hw.uuid
		let context = new Buffer(JSON.stringify(deviceInfo)).toString('base64')
		let params = {
			FunctionName: meta.lambdaControlDeviceFunctionName,
			ClientContext: context
		}
		return new Promise((resolve, reject) => {
			lambda.invoke(params, (err, data) => {
				if (err) {
					return reject(err)
				}
				if (!!data.FunctionError) {
					console.log(`failed to verify device by server error: ${data.Payload}`)
					let payload = JSON.parse(data.Payload)
					return reject(JSON.parse(payload.errorMessage))
				}

				resolve(JSON.parse(data.Payload))
			})
		})
	})

	return coroutine()
}

module.exports = { invoke }
