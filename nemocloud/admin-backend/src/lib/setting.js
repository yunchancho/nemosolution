const meta = require('../meta.json')
const fs = require('fs')
const path = require('path')

const initialSetting = {
	apis: {
		system: {},
		backup: {},
		restore: {
			runOnBoot: {
				package: false,
				content: false,
				config: false
			}
		},
		audio: {},
		network: {},
		device: {},
		auth: {},
		package: {}
	}
}

// initial setting
if (!fs.existsSync(meta.deviceSettingFilePath)) {
	let data = JSON.stringify(initialSetting, null, 2)
		console.log(data)
	if (!fs.existsSync(path.dirname(meta.deviceSettingFilePath))) {
		fs.mkdirSync(path.dirname(meta.deviceSettingFilePath))
	}
	fs.writeFileSync(meta.deviceSettingFilePath, data, { encoding: 'utf8' })
}

module.exports = initialSetting
