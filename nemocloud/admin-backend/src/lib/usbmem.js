const co = require('co')
const exec = require('child_process').exec
const fs = require('fs')
const systeminfo = require('systeminformation')
const _ = require('underscore')
const meta = require('../meta.json')

const mountBase = '/mnt/nemosnow'
const udevWaitTime = 5000

// TODO usb-detection module causes cpu use to be 100%
// So, we need to disable auto load/unload until finding proper solution
/*
const usb = require('usb-detection')
usb.on('add', info => addUsb(info))
usb.on('remove', info => removeUsb(info))
*/

function addUsb(info) {
	setTimeout(() => {
		if (!info.serialNumber) {
			console.log(`failed to mount usb. The serial number does not exist: ${info.deviceName}`)
			return; 
		}
		co(function* () {
			let devices = yield systeminfo.blockDevices()
				console.log(devices)
			let found = _.findWhere(devices, { serial: info.serialNumber })

			let usbparts = devices.filter(device => {
				console.log(JSON.stringify(device, null, 2))
				return (device.type === 'part') && ~device.name.indexOf(found.name) && !device.mount
			})

			yield mountUsbParts(usbparts)
		})
	}, udevWaitTime)
}

function removeUsb(info) {
	console.log(info)
	setTimeout(() => {
		if (!info.serialNumber) {
			console.log(`failed to umount usb. The serial number does not exist: ${info.deviceName}`)
			return; 
		}
		co(function* () {
			let devices = yield systeminfo.blockDevices()
				console.log(devices)
			let found = _.findWhere(devices, { serial: info.serialNumber })

			let usbparts = devices.filter(device => {
				console.log(JSON.stringify(device, null, 2))
				return (device.type === 'part') && ~device.name.indexOf(found.name) && device.mount
			})

			yield umountUsbParts(usbparts)
		})
		.catch(err => console.log(err.stack))
	}, 0)
}

function mountUsbParts(usbparts) {
	let coroutine = co.wrap(function* () {
		for (let part of usbparts) {
			let mountPoint = `${mountBase}/${part.uuid}`
			if (!fs.existsSync(mountBase)) {
				fs.mkdirSync(mountBase)
			}

			if (!fs.existsSync(mountPoint)) {
				fs.mkdirSync(mountPoint)
			}

			try {
				yield new Promise((resolve, reject) => {
					let command = `sudo mount /dev/${part.name} ${mountPoint}`
					exec(command, (err, stdout, stderr) => {
						if (err) {
							console.log(`failed to mount /dev/${part.name}`)
							reject(err)
						}
						console.log(`success to mount /dev/${part.name} onto ${mountPoint}`)
						resolve()
					})
				})
			} catch (err) {
				console.log(err.stack)
			}
		}
		return true 
	})

	return coroutine()
}

function umountUsbParts(usbparts) {
	let coroutine = co.wrap(function* () {
		for (let part of usbparts) {
			/* This may not be necessary
			 *
			let mountPoint = `${mountBase}/${part.uuid}`
			if (!fs.existsSync(mountPoint)) {
				return
			}
			*/

			try {
				yield new Promise((resolve, reject) => {
					let command = `sudo umount /dev/${part.name}`
					exec(command, (err, stdout, stderr) => {
						if (err) {
							console.log(`failed to umount /dev/${part.name}`)
							reject(err)
						}
						console.log(`success to umount /dev/${part.name}`)
						resolve()
					})
				})
			} catch (err) {
				console.log(err.stack)
			}
		}
		return true 
	})

	return coroutine()
}

function mountAll() {
	let coroutine = co.wrap(function* () {
		let devices = yield systeminfo.blockDevices()
		let usbdisks = devices.filter(device => {
			return (device.type === 'disk') && device.removable
		})

		let usbparts = []
		for (let disk of usbdisks) {
			let list = devices.filter(device => {
				return (device.type === 'part') && ~device.name.indexOf(disk.name) && !device.mount
			})

			usbparts = usbparts.concat(list)
		}

		console.log(JSON.stringify(usbparts, null, 2))

		let result = yield mountUsbParts(usbparts)
		return { result }
	})

	return coroutine()
}

function umountAll() {
	let coroutine = co.wrap(function* () {
		let devices = yield systeminfo.blockDevices()
		let usbdisks = devices.filter(device => {
			return (device.type === 'disk') && device.removable
		})

		let usbparts = []
		for (let disk of usbdisks) {
			let list = devices.filter(device => {
				return (device.type === 'part') && ~device.name.indexOf(disk.name) && device.mount
			})

			usbparts = usbparts.concat(list)
		}

		console.log(JSON.stringify(usbparts, null, 2))

		let result = yield umountUsbParts(usbparts)
		return { result }
	})

	return coroutine()
}

function umountOne(usbpart) {
	// usbpart format should be compatible with systeminforamtion's block device data
	let coroutine = co.wrap(function* () {
		let result = yield umountUsbParts([usbpart])
		return { result }
	})
	
	return coroutine()
}

module.exports = {
	mountAll, umountAll, umountOne
}
