const co = require('co')
const fs = require('fs')
const path = require('path')
const express = require('express')
const exec = require('child_process').exec
const meta = require('../meta.json')

const pactl = '/usr/bin/pactl'

let router = express.Router()

const audioFile = path.join(path.dirname(process.execPath), `../${meta.audioSampleFile}`)

if(!fs.existsSync(audioFile)) {
	console.log('test sample file not exists..')
}
// register test sample
exec(`${pactl} upload-sample ${audioFile} test`, (err, stdout, stderr) => {
  if (err) { 
    console.log(err)
    return 
  }
  console.log('success to upload sample audio: ', stdout)
})

router.get('/sinks', (req, res) => {
	listSinks().then(result => res.json(result))
	.catch(err => res.status(400).json(err))
})

router.post('/sink/:id/volume', (req, res) => {
	let value = req.body.value

	if (value === undefined || value < 0 || value > 100) {
		return res.status(400).json({
			code: 'AUDIO_ERROR_SINK_VOLUME_INVALID_PARAM'
		})
	}

	setVolume(req.params.id, value).then(result => res.json(result))
	.catch(err => res.status(400).json(err))
})

router.post('/sink/:id/mute', (req, res) => {
	let value = req.body.value

	if (value === undefined || ![0, 1].includes(value)) {
		return res.status(400).json({
			code: 'AUDIO_ERROR_SINK_MUTE_INVALID_PARAM'
		})
	}

	setMute(req.params.id, value).then(result => res.json(result))
	.catch(err => res.status(400).json(err))
})

router.get('/sink/:id/play', (req, res) => {
	playSample(req.params.id).then(result => res.json(result))
	.catch(err => res.status(400).json(err))
})


function playSample(sink) {
	let coroutine = co.wrap(function* () {
		try {
			yield new Promise((resolve, reject) => {
				exec(`${pactl} play-sample test ${sink}`, (err, stdout, stderr) => {
					if (err) {
						return reject(err)
					}

					resolve()
				})
			})
		} catch (err) {
			console.log(`failed to play sample: ${err}`)
			throw err
		}

		return { result: true }
	})

	return coroutine()
}

function setMute(sink, value) {
	let coroutine = co.wrap(function* () {
		try {
			yield new Promise((resolve, reject) => {
				exec(`${pactl} set-sink-mute ${sink} ${value}`, (err, stdout, stderr) => {
					if (err) {
						return reject(err)
					}

					resolve()
				})
			})
		} catch (err) {
			console.log(`failed to set mute: ${err}`)
			throw err
		}

		return { result: true }
	})

	return coroutine()
}

function setVolume(sink, value) {
	let coroutine = co.wrap(function* () {
		try {
			yield new Promise((resolve, reject) => {
				exec(`${pactl} set-sink-volume ${sink} ${value}%`, (err, stdout, stderr) => {
					if (err) {
						return reject(err)
					}

					resolve()
				})
			})
		} catch (err) {
			console.log(`failed to set volume: ${err}`)
			throw err
		}

		return { result: true }
	})

	return coroutine()
}

function listSinks() {
	let coroutine = co.wrap(function* () {
		let sinks = []
		try {
			let output = yield new Promise((resolve, reject) => {
				exec(`${pactl} list sinks`, (err, stdout, stderr) => {
					if (err) {
						return reject(err)
					}

					resolve(stdout)
				})
			})

			sinks = parseOutput(output)
		} catch (err) {
			console.log(`failed to execute ${pactl}: ${err}`)
		}

		return sinks
	})

	return coroutine()
} 

function parseOutput(output) {
	let items = output.split('\n\n')
	let sinks = []
	for (let item of items) {
		let lines = item.split('\n')
		let sink = {}

		sink.sink = lines.filter(line => line.includes('Sink'))[0].split('#')[1].trim()
		sink.name = lines.filter(line => line.includes('Name'))[0].split(':')[1].trim()
		let mute = lines.filter(line => line.includes('Mute'))[0].split(':')[1].trim()
		sink.mute = mute == 'yes'? true: false
		sink.desc = lines.filter(line => line.includes('Description'))[0].split(':')[1].trim()
		let volume = lines.filter(line => line.includes('Volume'))[0].split(':')[2].trim()
		sink.volume = volume.split('/')[1]

		sinks.push(sink)
	}

	return sinks
}

module.exports = router
