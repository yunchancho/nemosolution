const co = require('co')
const express = require('express')
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'))
const bcrypt = require('bcrypt')
const jwt = require('../lib/jwtoken')
const meta = require('../meta.json')

const defaultId = 'admin'
const initialPasswd = '1234567890'
const saltRounds = 10

const initialStatusValue = 'initial'

let router = express.Router()

co(function* () {
	if (fs.existsSync(meta.localInfraSecretFilePath)) {
		return
	}

	yield setPassword(initialPasswd)
	console.log(`success to set initial secret`)
})
.catch(err => console.log(`failed to set initial secret for local infra: ${err}`))

// In auth apis, we should selectively use middleware
router.use('/passwd', jwt.verifyAccessTokenMiddleware)

router.post('/login', (req, res) => {
	let reqAuth = req.body
	if (reqAuth.id !== defaultId) {
		return res.status(400).json({ code: 'AUTH_ERROR_LOGIN_WRONG_ID' })
	}

	co(function* () {
		yield comparePassword(reqAuth.passwd)
		let token = yield jwt.create(defaultId, 60 * 60) // expired after 60 sec
		let deviceVerified = false 
		if (fs.existsSync(meta.swKeyFilePath)) {
			deviceVerified = true 
		} 

		res.json({ token, deviceVerified })
	})
	.catch(err => {
		console.log(`fail to login: ${err.stack}`)
		res.status(400).json({ code: 'AUTH_ERROR_LOGIN_WRONG_PASSWORD' })
	})
})

router.post('/passwd', (req, res) => {
	let newPasswd = req.body.newPasswd
	co(function* () {
		yield comparePassword(req.body.oldPasswd)
		let result = yield setPassword(req.body.newPasswd)
		res.json({ result })
	})
	.catch(err => {
		console.log(`fail to set new password: ${err}`)
		res.status(400).json({ code: 'AUTH_ERROR_PASSWORD_NOT_PROPER' })
	})
})

function setPassword(passwd) {
	return co(function* () {
		let hash = yield bcrypt.hash(passwd, saltRounds)
		yield fs.writeFileAsync(meta.localInfraSecretFilePath, hash, { encoding: 'utf8' })
		return true 
	}) 
}

function comparePassword(passwd) {
	return co(function* () {
		let hash = yield fs.readFileAsync(meta.localInfraSecretFilePath, { encoding: 'utf8' })
		let result = yield bcrypt.compare(passwd, hash)
		if (!result) {
			throw 'This is wrong password..'
		}
		return true 
	}) 
}



module.exports = router
