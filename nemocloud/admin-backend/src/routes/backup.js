const path = require('path')
const co = require('co')
const express = require('express')
const Promise = require('bluebird')
const mongo = require('mongodb')
const MongoClient = Promise.promisifyAll(mongo).MongoClient
const fs = Promise.promisifyAll(require('fs'))
const exec = require('child_process').exec
const moment = require('moment')
const meta = require('../meta.json')
const { DynamoDb } = require('../lib/dynamodb')
const lambda = require('../lib/lambda')

const s3Scheme = 's3://'
const s3LocalTmpDir = '/tmp'
const reservedCollections = [
	'system.indexes'
];
const reservedSystemLogs = [
	'__system__'
]

let router = express.Router()

router.get('/status', (req, res) => {
	co(function* () {
		// get installed pkg list from local mongodb
		let installedPkgs = yield getInstalledPkgs()
		let backablePkgs = []
		for (let pkg of installedPkgs) {
			try {
				let size = yield new Promise((resolve, reject) => {
					exec(`du -b -d 0 ${meta.logBasePath}/${pkg}`, function (err, stdout, stderr) {
						if (err) {
							return reject({ code: 'BACKUP_ERROR_GET_LOGS_SIZE' })
						}
						resolve(parseInt(stdout.split('\t')[0]))
					})
				})
				let period = yield getLogCreationPeriod(`${meta.logBasePath}/${pkg}`)
				backablePkgs.push({ name: pkg, size, period })

			} catch (err) {
				console.log(`failed to get log size for ${pkg}`)
			}
		}

		// get collections from local db
		let backableCollections = yield getLocalDBCollectionNames()
		let result = {
			log: {
				totalSize: backablePkgs.length? backablePkgs.map(pkg => pkg.size).reduce((prev, next) => prev + next): 0,
				backablePkgs
			},
			config: {
				backableCollections
			}
		}
		res.json(result)
	})
	.catch(err => {
		console.log(err)
		res.status(400).json(err)
	})
})

router.post('/logs', (req, res) => {
	co(function* () {
		let dest = req.body.dest
		let backupPkgs = []
		if (dest.includes(s3Scheme)) {
			backupPkgs = yield backupLogsToS3(dest)
		} else {
			backupPkgs = yield backupLogsToUsb(dest)
		}
		res.json({ result: true, backupPkgs })
	})
	.catch(err => {
		console.log(err)
		res.status(400).json(err)
	})
})

router.post('/configs', (req, res) => {
	co(function* () {
		let dest = req.body.dest
		// get swky, mode, endpoint
		let swKey, swMode

		try {
			swKey = yield fs.readFileAsync(meta.swKeyFilePath, { encoding: 'utf8' })
			swMode = yield fs.readFileAsync(meta.swModeFilePath, { encoding: 'utf8' })
			swKey = swKey.trim()
			swMode = swMode.trim()
			if (!swKey || !swMode) {
				throw 'no swkey or swmode'
			}
		} catch (err)  {
			console.log(err)
			throw { code: 'BACKUP_ERROR_COPY_CONFIGS_NO_KEY_OR_MODE' }
		}

		// TODO in case of cloud mode, we should invoke lambda with key, mode
		// so that we could get region data exactly.
		// But, in case of local mode, this region cloud be any value.
		let region = 'ap-northeast-2'
		let secrets = {}
		if (dest.includes(s3Scheme)) {
			secrets = yield getAccessSecrets()
		}
		console.log('swKey:', swKey, 'swMode:', swMode)
		let db = new DynamoDb({ swKey, swMode, secrets, region, endpoint: meta.awsLocalEndpoint })

		try {
			yield db.initialize()
		} catch (err) {
			console.log(err)
			throw { code: 'BACKUP_ERROR_COPY_CONFIGS_NO_DB_TABLE' }
		}

		let collections = yield db.getAll()
		//console.log('collections: ', JSON.stringify(collections, null, 2))

		let backupCollections = []
		if (dest.includes(s3Scheme)) {
			backupCollections = yield backupConfigsToS3(dest, collections, secrets)
		} else {
			backupCollections = yield backupConfigsToUsb(dest, collections)
		}

		res.json({ result: true, backupCollections })
	})
	.catch(err => {
		console.log(err)
		res.status(400).json(err)
	})
})

function checkBackupDestination(destPath) {
	return co(function* () {
		if (!destPath) {
			throw { code: 'BACKUP_ERROR_DESTINATION_EMPTY' }
		}

		// check dest is directory
		let stats = yield fs.statAsync(destPath)
		if (stats.isFile()) {
			throw { code: 'BACKUP_ERROR_DESTINATION_NOT_DIRECTORY' }
		}
	})
}

function createBackupUsbDirectory(destPath, type) {
	let typeDirName = null
	if (type === 'log') {
		typeDirName = meta.backupLogDirname
	} else if (type === 'config')  {
		typeDirName = meta.backupConfigDirname
	} else {
		return Promise.reject({ code: 'BACKUP_ERROR_CREATION_DIRECTORY_TYPE_ERROR' })
	}

	let baseDir = new Date().toISOString().replace(/T/, '_').replace(/:/g, '-').replace(/\..+/g, '')
	let backupPath = `${destPath}/${meta.backupPathPrefix}/${typeDirName}/${baseDir}`

	return new Promise((resolve, reject) => {
		exec(`mkdir -p ${backupPath}`, function (err, stdout, stderr) {
			if (err) {
				return reject({ code: 'BACKUP_ERROR_CREATION_DIRECTORY' })
			}
			resolve(backupPath)
		})
	})
}

function createBackupS3Directory(destPath, type) {
	let typeDirName = null
	if (type === 'log') {
		typeDirName = meta.backupLogDirname
	} else if (type === 'config')  {
		typeDirName = meta.backupConfigDirname
	} else {
		return Promise.reject({ code: 'BACKUP_ERROR_CREATION_DIRECTORY_TYPE_ERROR' })
	}

	//let baseDir = new Date().toISOString().replace(/T/, '_').replace(/:/g, '-').replace(/\..+/g, '')
	let backupPath = `${destPath}/${typeDirName}`

	return Promise.resolve(backupPath)
}

function removeDirectory(dirPath) {
	return new Promise((resolve, reject) => {
		exec(`rm -rf ${dirPath}`, function (err, stdout, stderr) {
			if (err) {
				return reject({ code: 'BACKUP_ERROR_REMOVE_DIRECTORY' })
			}
			resolve()
		})
	})
}

function getInstalledPkgs() {
	return co(function* () {
		let pkgs = []
		try {
			const db = yield MongoClient.connectAsync(meta.dbPath)
			let pkglist = db.collection(meta.installedPkgsCollection)
			let docs = yield pkglist.find({}).toArrayAsync()
			pkgs = docs.map(doc => doc.pkgname)
		} catch (err) {
			console.log(err.stack)
			throw { code: 'BACKUP_ERROR_GET_INSTALLED_PKG_LIST' }
		}

		pkgs = [...pkgs, ...reservedSystemLogs]
		return pkgs
	})
}

function getLocalDBCollectionNames() {
	return co(function* () {
		let collNames = []
		try {
			const db = yield MongoClient.connectAsync(meta.dbPath)
			let list = yield db.listCollections().toArrayAsync();
			collNames = list.map(item => item.name).filter(name => !reservedCollections.includes(name))
		} catch (err) {
			console.log(err.stack)
			throw { code: 'BACKUP_ERROR_GET_COLLECTION_LIST' }
		}
		return collNames
	})
}

function getLogCreationPeriod(dirname) {
	return co(function* () {
		if (!fs.existsSync(dirname)) {
			throw { code: 'BACKUP_ERROR_GET_LOGS_PERIOD' }
		}
		try {
			let files = yield fs.readdirAsync(dirname)
			let logs = []
			for(let file of files) {
				let stats = yield fs.statAsync(`${dirname}/${file}`)
				if (stats.isDirectory()) {
					continue
				}
				logs.push({ file, mtime: stats.mtime.getTime() })
			}

			if (!logs.length) {
				return { start: 0, end: 0 }
			}

			logs.sort((prev, next) => prev.mtime - next.mtime)
			return { start: logs[0].mtime, end: logs[logs.length - 1].mtime }
		} catch (err) {
			console.log(err.stack)
		}
	})
}

function backupLogsToUsb(dest) {
	let coroutine = co.wrap(function* () {
		// check dest is directory
		yield checkBackupDestination(dest)

		// create new backup directory in dest
		let backupPath = yield createBackupUsbDirectory(dest, 'log')

		// get installed pkg list from local mongo db
		let installedPkgs = yield getInstalledPkgs()
		let backupPkgs = []
		for (let pkg of installedPkgs) {
			try {
				yield new Promise((resolve, reject) => {
					exec(`cp -r ${meta.logBasePath}/${pkg} ${backupPath}`, function (err, stdout, stderr) {
						if (err) {
							return reject({ code: 'BACKUP_ERROR_COPY_LOGS_TO_USB_MEMORY' })
						}
						backupPkgs.push(pkg)
						resolve()
					})
				})
				yield removeDirectory(`${meta.logBasePath}/${pkg}`)
			} catch (err) {
				console.log(`failed to backup for ${pkg}`)
			}
		}

		if (!backupPkgs.length) {
			console.log(`${backupPath} has been removed because there is no files`)
			yield removeDirectory(backupPath)
		}

		return backupPkgs
	})

	return coroutine()
}

function backupLogsToS3(dest) {
	let coroutine = co.wrap(function* () {
		let backupPath = yield createBackupS3Directory(dest, 'log')
		let backupPkgs = []

		let secrets = yield getAccessSecrets()
		let env = Object.assign(process.env, {
			'AWS_ACCESS_KEY_ID': secrets.keyId,
			'AWS_SECRET_ACCESS_KEY': secrets.accessKey
		})

    const today = moment().format('YYYY-MM-DD')
		let installedPkgs = yield getInstalledPkgs()

		for (let pkg of installedPkgs) {
			try { 
				yield new Promise((resolve, reject) => {
					let command = `aws s3 mv ${meta.logBasePath}/${pkg}/ ${backupPath}/${pkg}/${today} --recursive`
					exec(command, { env }, function (err, stdout, stderr) {
						if (err) {
              console.log(err)
							return reject({ code: 'BACKUP_ERROR_COPY_LOGS_TO_AWS_S3' })
						}
						backupPkgs.push(pkg)
						resolve()
					})
				})
			} catch (err) {
				console.log(`failed to backup for ${pkg}: ${JSON.stringify(err, null, 2)}`)
			}
		}

		return backupPkgs
	})

	return coroutine()
}


function backupConfigsToUsb(dest, collections) {
	return co(function* () {
		// check dest is directory
		yield checkBackupDestination(dest)

		// create new backup directory in dest
		let backupPath = yield createBackupUsbDirectory(dest, 'config')
		let backupCollections = []

		for (let coll of collections) {
			try {
				let data = JSON.stringify(coll, null, 2)
				let savePath = path.join(backupPath, `${coll.name}.json`) 
				yield fs.writeFileAsync(savePath, data, { encoding: 'utf8' })
				backupCollections.push(coll.name)
			} catch (err) {
				console.log(`failed to backup for ${coll.name}: ${err}`)
			}
		}

		if (!backupCollections.length) {
			console.log(`${backupPath} has been removed because there is no files`)
			yield removeDirectory(backupPath)
		}

		return backupCollections
	})
}

function backupConfigsToS3(dest, collections, secrets) {
	return co(function* () {
		// create new backup directory in dest
		let backupPath = yield createBackupS3Directory(dest, 'config')
		let backupCollections = []

		for (let coll of collections) {
			try {
				let data = JSON.stringify(coll, null, 2)
				let savePath = path.join(s3LocalTmpDir, `${coll.name}.json`) 
				yield fs.writeFileAsync(savePath, data, { encoding: 'utf8' })
				backupCollections.push(coll.name)
			} catch (err) {
				console.log(`failed to backup for ${coll.name}: ${err}`)
			}
		}

		let env = Object.assign(process.env, {
			'AWS_ACCESS_KEY_ID': secrets.keyId,
			'AWS_SECRET_ACCESS_KEY': secrets.accessKey
		})
		yield new Promise((resolve, reject) => {
			let command = `aws s3 cp ${s3LocalTmpDir} ${backupPath}`
			command += ` --recursive --exclude "*" --include "*.json"`
			console.log(command)
			exec(command, { env }, function (err, stdout, stderr) {
				if (err) {
					//console.log(stderr)
					// TODO how to handle error by warnings 
					//return reject({ code: 'BACKUP_ERROR_COPY_CONFIGS_TO_AWS_S3' })
				}
				console.log(stdout)
				resolve()
			})
		})

		return backupCollections
	})
}

function getAccessSecrets() {
	return co(function* () {
		let swKey = yield fs.readFileAsync(meta.swKeyFilePath, { encoding: 'utf8' })
		swKey = swKey.trim()

		let secrets = yield lambda.invoke({ api: 'getAccessSecrets', swKey })
		return secrets
	})
}

module.exports = router
