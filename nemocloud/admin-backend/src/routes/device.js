const co = require('co')
const express = require('express')
const Promise = require('bluebird')
const { exec } = require('child_process')
const fs = Promise.promisifyAll(require('fs'))
const dns = Promise.promisifyAll(require('dns'))
const lambda = require('../lib/lambda')
const jwt = require('../lib/jwtoken')
const meta = require('../meta.json')

const initialStatusValue = 'initial'
const activatedStatusValue = 'activated'
const deactivatedStatusValue = 'deactivated'
const errorStatusValue = 'error'

const allowedStatuses = ['wait_confirm'];
const notAllowedStatuses = [activatedStatusValue, deactivatedStatusValue];

const allowedModes = ['local', 'cloud']

let router = express.Router()

// In auth apis, we should selectively use middleware
tokenApis = [ '/swkey', '/mode' ]
router.use(tokenApis, jwt.verifyAccessTokenMiddleware)

router.get('/verified', (req, res) => {
  let deviceVerified = false 
  try {
    if (fs.existsSync(meta.swKeyFilePath)) {
      deviceVerified = true 
    } 
	  res.json({ deviceVerified })
  } catch (err) {
    console.log(`failed to get device verification: ${err}`)
  }

	res.json({ deviceVerified })
})

router.get('/swkey', (req, res) => {
	let swkey = null
	try {
		if (!fs.existsSync(meta.swKeyFilePath)) {
			throw 'no file'
		} else {
			swKey = fs.readFileSync(meta.swKeyFilePath, { encoding: 'utf8' })
			swKey = swKey.trim()
		}
		res.json({ result: true, swKey })
	} catch (err) {
		res.status(400).json({ code: 'DEVICE_ERROR_GET_SWKEY' })
	}
})

router.get('/status', (req, res) => {
	let coroutine = co.wrap(function* () {
		let status = null;
		try {
			if (!fs.existsSync(meta.swKeyFilePath)) {
				status = initialStatusValue
			} else {
        let internet = false
        try {
		      yield dns.resolveAsync('snow.nemoux.net') 
          internet = true
        } catch (err) {
          console.log('not connected to internet')
        }

        if (!internet) {
          // TODO we need to say undetermined status to frontend
          // but temporarily return just activated status
          status = activatedStatusValue
        } else {
          let swKey = yield fs.readFileAsync(meta.swKeyFilePath, { encoding: 'utf8' })
          swKey = swKey.trim()
          let result = yield getDeviceStatus(swKey)
          status = result.status
        }
			}
		} catch (err) {
			throw { code: 'DEVICE_ERROR_GET_STATUS' }
		}

		return { result: true, status }
	})

	coroutine().then(result => res.json(result))
	.catch(err => res.status(400).json(err))
})

router.post('/status', (req, res) => {
  let timeoutSec = req.body.timeoutSec || 5
	let coroutine = co.wrap(function* () {
		let result = null;
		let error = null;
		let newStatus = req.body.status;

		if (!newStatus || !allowedStatuses.includes(newStatus)) {
			throw {
				code: 'DEVICE_ERROR_SET_STATUS_NOT_PROPER_PARAM',
			}
		}

		if (!req.body.swKey) {
			throw {
				code: 'DEVICE_ERROR_SET_STATUS_NOT_SW_KEY',
			}
		} else {
			let swKey = req.body.swKey.trim()
			result = yield verifyDeviceStatus(swKey, newStatus)
			yield fs.writeFileAsync(meta.swKeyFilePath, swKey, { encoding: 'utf8' })
			yield fs.writeFileAsync(meta.swModeFilePath, result.mode, { encoding: 'utf8' })
		}

		return { result: true, status: result.status, mode: result.mode }
	})

	coroutine().then(result => {
		exec(`sleep ${timeoutSec}s && reboot`)
    res.json(result)
  })
	.catch(err => {
		console.log(`err: ${JSON.stringify(err)}`)
		res.status(400).json(err)
	})

})

router.get('/mode', (req, res) => {
	let coroutine = co.wrap(function* () {
		let swKey = yield fs.readFileAsync(meta.swKeyFilePath, { encoding: 'utf8' })
		swKey = swKey.trim()

		let result = yield getDeviceMode(swKey)
		return { result: true, mode: result.mode, cloudAllowed: parseInt(result.cloudAllowed) }
	})

	coroutine().then(result => res.json(result))
	.catch(err => res.status(400).json(err))
})

router.post('/mode', (req, res) => {
	let coroutine = co.wrap(function* () {
		let newMode = req.body.mode
		if (newMode === undefined || req.body.keepConfig === undefined) {
			throw { code: 'DEVICE_ERROR_SET_MODE_EMPTY_PARAM' }
		}

		if (!allowedModes.includes(newMode)) {
			throw { code: 'DEVICE_ERROR_SET_MODE_INVALID_MODE' }
		}

		if (!fs.existsSync(meta.swKeyFilePath)) {
			throw { code: 'DEVICE_ERROR_SET_MODE_EMPTY_SW_KEY' }
		} 

		let swKey = yield fs.readFileAsync(meta.swKeyFilePath, { encoding: 'utf8' })
		swKey = swKey.trim()

		let result = yield switchDeviceMode(swKey, newMode)
		yield fs.writeFileAsync(meta.swModeFilePath, result.mode, { encoding: 'utf8' })

		//TODO we need to handle something for keeping configs
		if (req.body.keepConfig) {
			console.log('keey configs: ', req.body.keepConfig)
		}

		return { result: true, mode: result.mode }
	})
	
	coroutine().then(result => res.json(result))
	.catch(err => res.status(400).json(err))
})

function getDeviceStatus(swKey) {
	let api = 'getDeviceStatus'
	return lambda.invoke({ api, swKey })
}

function getDeviceMode(swKey) {
	let api = 'getDeviceMode'

	let coroutine = co.wrap(function* () {
		let internet = false
		try {
			yield dns.resolveAsync('www.google.com')
			internet = true
		} catch (err) {
			console.log(`failed to resolve the host: ${err}`)
		}

		if (!internet) {
			if (!fs.existsSync(meta.swModeFilePath)) {
				throw { code: 'DEVICE_ERROR_GET_MODE_NO_MODE_FILE' }
			}
			let mode = yield fs.readFileAsync(meta.swModeFilePath, { encoding: 'utf8' })
			return { mode: mode.trim(), cloudAllowed: null }
		}

		return lambda.invoke({ api, swKey })
	})

	return coroutine()
}

function switchDeviceMode(swKey, mode) {
	let api = 'switchDeviceMode'
	return lambda.invoke({ api, swKey, mode })
}

function verifyDeviceStatus(swKey, status) {
	let api = 'verifyDeviceStatus'
	return lambda.invoke({ api, swKey, status })
}

module.exports = router
