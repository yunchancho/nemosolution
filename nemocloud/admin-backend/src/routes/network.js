const co = require('co')
const fs = require('fs')
const express = require('express')
const Promise = require('bluebird')
const dns = Promise.promisifyAll(require('dns'))
const systeminfo = require('systeminformation')
const exec = require('child_process').exec

const staticPrefix = 'static'
const dhcpPrefix = 'dhcp'

let router = express.Router()

let reservedIfceStates = [ '(connected)', '(connecting)', '(disconnected)', '(unmanaged)', '(unavailable)' ]

router.get('/status', (req, res) => {
	// internet connection
	// which interface is connected as default
	co(function* () {
		let internet = false
		try {
			yield dns.resolveAsync('snow.nemoux.net')
			internet = true
		} catch (err) {
			console.log(`failed to resolve the host: ${err}`)
		}
		let ifce = yield systeminfo.networkInterfaceDefault()
		let status = yield new Promise((resolve, reject) => {
			exec('nmcli networking', (err, stdout, stderr) => {
				if (err) {
					return reject(err)
				}
				resolve(stdout.trim())
			})
		})

		res.json({ status, internet, ifce })
	})
	.catch(err => {
		console.log(err.stack)
		res.status(400).json({ code: 'NETWORK_ERROR_GET_CURRENT_STATUS' })
	})
})

router.post('/status', (req, res) => {
	let status = req.body.status

	if (status === undefined || !['enabled', 'disabled'].includes(status)) {
		return res.status(400).json({
			code: 'NETWORK_ERROR_SET_STATUS_INVALID_PARAM'
		})
	}

	co(function* () {
	 	yield new Promise((resolve, reject) => {
			let value = (status === 'enabled') ? 'on': 'off'
			exec(`nmcli networking ${value}`, (err, stdout, stderr) => {
				if (err) {
					return reject(err)
				}
				resolve(stdout.trim())
			})
		})
		res.json({ result: true, status })
	})
	.catch(err => {
		console.log(err.stack)
		res.status(400).json({ code: 'NETWORK_ERROR_SET_STATUS' })
	})
})

router.get('/ifces', (req, res) => {
	// return current connection state. on / off
	co(function* () {
		let output = yield new Promise((resolve, reject) => {
			exec('nmcli device show', (err, stdout, stderr) => {
				if (err) {
					return reject(err)
				}
				resolve(stdout)
			})
		})

		//output = output.replace(/:/, '&')
		console.log(output)
		let items = output.split('\n\n')

		let ifces = []
		for (let item of items) {
			let lines = item.split('\n')
			let ifce = {}

			ifce.device = lines.filter(line => line.includes('GENERAL.DEVICE'))[0].replace(/:/, '&').split('&')[1].trim()
			ifce.type = lines.filter(line => line.includes('GENERAL.TYPE'))[0].replace(/:/, '&').split('&')[1].trim()
			ifce.hwaddr = lines.filter(line => line.includes('GENERAL.HWADDR'))[0].replace(/:/, '&').split('&')[1].trim()
			let stateLine = lines.filter(line => line.includes('GENERAL.STATE'))[0]
			for (let ris of reservedIfceStates) {
				if (!stateLine.includes(ris)) {
					continue
				}
				ifce.state = ris.slice(1, -1)
				break
			}
			if (ifce.state === 'connected') {
				ifce.connection = lines.filter(line => line.includes('GENERAL.CONNECTION'))[0].replace(/:/, '&').split('&')[1].trim()
				ifce.ip = lines.filter(line => line.includes('IP4.ADDRESS'))[0].replace(/:/, '&').split('&')[1].trim().split('/')[0]
				// in case of ethernet, we need to get more detail with dns 
				let output = yield new Promise((resolve, reject) => {
					exec(`nmcli connection show id '${ifce.connection}'`, (err, stdout, stderr) => {
						if (err) {
							return reject(err)
						}
						resolve(stdout)
					})
				})
				sublines = output.split('\n')
				let method = sublines.filter(line => line.includes('ipv4.method'))[0].split(':')[1].trim() 
				ifce.dhcp = (method === 'auto')? true: false
				let dns = sublines.filter(line => line.includes('ipv4.dns'))[0].split(':')[1].trim()
				ifce.dns = (dns === "")? []: dns.split(',')
				ifce.gw = sublines.filter(line => line.includes('ipv4.gateway'))[0].split(':')[1].trim()
			}

			ifces.push(ifce)
		}

		res.json(ifces)
	})
	.catch(err => {
		console.log(err.stack)
		res.status(400).json({ code: 'NETWORK_ERROR_GET_ALL_INTERFACES' })
	})
})

router.get('/wifi/status', (req, res) => {
	co(function* () {
		let status = yield new Promise((resolve, reject) => {
			exec('nmcli radio wifi', (err, stdout, stderr) => {
				if (err) {
					return reject(err)
				}
				resolve(stdout.trim())
			})
		})
		res.json({ status })
	})
	.catch(err => {
		console.log(err.stack)
		res.status(400).json({ code: 'NETWORK_ERROR_GET_WIFI_STATUS' })
	})
})

router.post('/wifi/status', (req, res) => {
	let status = req.body.status

	if (status === undefined || !['enabled', 'disabled'].includes(status)) {
		return res.status(400).json({
			code: 'NETWORK_ERROR_SET_WIFI_STATUS_INVALID_PARAM'
		})
	}

	co(function* () {
	 	yield new Promise((resolve, reject) => {
			let value = (status === 'enabled') ? 'on': 'off'
			exec(`nmcli radio wifi ${value}`, (err, stdout, stderr) => {
				if (err) {
					return reject(err)
				}
				resolve(stdout.trim())
			})
		})
		res.json({ result: true, status })
	})
	.catch(err => {
		console.log(err.stack)
		res.status(400).json({ code: 'NETWORK_ERROR_SET_WIFI_STATUS' })
	})
})

router.get('/wifi/:ifce/accesspoints', (req, res) => {
	let ifce = req.params.ifce

	if (ifce === undefined) {
		return res.status(400).json({
			code: 'NETWORK_ERROR_GET_ACCESS_POINTS'
		})
	}

	co(function* () {
		yield new Promise((resolve, reject) => {
			exec('nmcli device wifi rescan', (err, stdout, stderr) => {
				if (err) {
					return reject(err)
				}
				resolve()
			})
		})

		let output = yield new Promise((resolve, reject) => {
			exec(`nmcli device wifi list ifname ${ifce}`, (err, stdout, stderr) => {
				if (err) {
					return reject(err)
				}
				resolve(stdout)
			})
		})

		let lines = output.split('\n')
		lines = lines.slice(1).filter(line => !!line)

		let accessPoints = []
		for (let line of lines) {
			let ap = { connected: false }
			if (line[0] === '*') {
				ap.connected = true
				line = line.replace(/\*/, "")
			}

			let fields = line.split(/[ \t]+/)
			let infraIndex = fields.indexOf('Infra')
			let names = fields.slice(1, infraIndex)
			ap.name = names.join(' ')
			ap.mode = fields[infraIndex]
			ap.channel = parseInt(fields[infraIndex + 1])
			ap.rate = `${fields[infraIndex + 2]} ${fields[infraIndex + 3]}`
			ap.signal = fields[infraIndex + 4]
			ap.security = fields.slice(infraIndex + 6).filter(sec => !!sec)

			accessPoints.push(ap)
		}

		res.json(accessPoints)
	})
	.catch(err => {
		console.log(err.stack)
		res.status(400).json({ code: 'NETWORK_ERROR_GET_ALL_INTERFACES' })
	})
})

router.post('/wifi/:ifce/connection', (req, res) => {
	let ifce = req.params.ifce

	if (req.body.ssid === undefined) {
		return res.status(400).json({
			code: 'NETWORK_ERROR_CONNECT_WIFI_INVALID_PARAM'
		})
	}

	co(function* () {
		let output = yield new Promise((resolve, reject) => {
			let command = `nmcli device wifi connect ${req.body.ssid}`
			command += req.body.passwd? ` password ${req.body.passwd}`: ''
			command += ` ifname ${ifce}`
			exec(command, (err, stdout, stderr) => {
				if (err) {
					return reject(err)
				}
				resolve()
			})
		})
		yield new Promise((resolve, reject) => {
			let command = `nmcli device connect ${ifce}`
			exec(command, (err, stdout, stderr) => {
				if (err) {
					return reject(err)
				}
				resolve()
			})
		})
		res.json({ result: true, output: output })
	})
	.catch(err => {
		console.log(err.stack)
		res.status(400).json({ code: 'NETWORK_ERROR_CONNECT_WIFI' })
	})
})

router.post('/wifi/:ifce/disconnection', (req, res) => {
	let ifce = req.params.ifce

	co(function* () {
		yield new Promise((resolve, reject) => {
			let command = `nmcli device disconnect ${ifce}`
			exec(command, (err, stdout, stderr) => {
				if (err) {
					return reject(err)
				}
				resolve()
			})
		})
		res.json({ result: true })
	})
	.catch(err => {
		console.log(err.stack)
		res.status(400).json({ code: 'NETWORK_ERROR_DISCONNECT_WIFI' })
	})
})

router.post('/ethernet/:ifce/connection', (req, res) => {
	let ifce = req.params.ifce
	let params = req.body
	let profile = `${params.dhcp? dhcpPrefix: staticPrefix}-${ifce}`

	co(function* () {
		try {
			yield new Promise((resolve, reject) => {
				let command = `nmcli connection delete '${dhcpPrefix}-${ifce}'`
				command += `; nmcli connection delete '${staticPrefix}-${ifce}'`
				exec(command, (err, stdout, stderr) => {
					if (err) {
						return reject(err)
					}
					resolve()
				})
			})
		} catch (err) {
			console.log(err)
		}

		yield new Promise((resolve, reject) => {
			let command = `nmcli connection add type ethernet con-name '${profile}' ifname ${ifce}`
			if (!params.dhcp) {
				command += ` ip4 ${params.ip}/24 gw4 ${params.gw}`
			}
			exec(command, (err, stdout, stderr) => {
				if (err) {
					return reject(err)
				}
				resolve()
			})
		})

		if (!params.dhcp && params.dns.length) {
			yield new Promise((resolve, reject) => {
				let command = `nmcli connection modify '${profile}' ipv4.dns '${params.dns.join(' ')}'`
				exec(command, (err, stdout, stderr) => {
					if (err) {
						return reject(err)
					}
					resolve()
				})
			})
		}

		try {
			yield new Promise((resolve, reject) => {
				let command = `nmcli connection up '${profile}'`
				exec(command, (err, stdout, stderr) => {
					if (err) {
						return reject(err)
					}
					resolve()
				})
			})
		} catch (err) {
			throw { code: 'NETWORK_ERROR_ETHERNET_CONNECT_NOT_ACTIVATED' }
		}

		res.json({ result: true })
	})
	.catch(err => {
		console.log(err)
		if (!err.code) {
			err = { code: 'NETWORK_ERROR_ETHERNET_CONNECT' }
		}
		res.status(400).json(err)
	})

})

module.exports = router
