const zlib = require('zlib')
const co = require('co')
const express = require('express')
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))

const fileScheme = 'file:'
const indexFile = 'Packages.gz'

let router = express.Router()

router.get('/index', (req, res) => {
	co(function* () {
		if (!req.query.host || !req.query.dir) {
			throw { code: 'PACKAGE_ERROR_GET_INDEX_EMPTY_PARAMS' }
		}

		console.log('query:', req.query)

		let host = req.query.host
		let dir = req.query.dir

		if (!host.includes(fileScheme)) {
			throw { code: 'PACKAGE_ERROR_GET_INDEX_INVALID_LOCAL_HOST' }
		}

		let content = null
		try {
			const indexFilePath = `${host.split(fileScheme)[1]}/${dir}/${indexFile}`
			let tmpIndexFilePath = '/tmp/'
			tmpIndexFilePath += `${host.split(fileScheme)[1]}/${dir}`.replace(/\//g, '_')

			console.log('path:', indexFilePath)

			if (fs.existsSync(indexFilePath)) {
				fs.createReadStream(indexFilePath)
					.pipe(zlib.createGunzip())
					.pipe(fs.createWriteStream(tmpIndexFilePath))
					.on('close', () => {
						content = fs.readFileSync(tmpIndexFilePath, { encoding: 'utf8' })
						res.json({ host, dir, content })
					})
			} else {
				throw { code: 'PACKAGE_ERROR_GET_INDEX_NO_FILE' }
			}
		} catch (err) {
			console.log(err)
			throw { code: 'PACKAGE_ERROR_GET_INDEX_FILE_IO_ERROR' }
		}
	})
	.catch(err => {
		res.status(400).json(err)
	})
})

module.exports = router
