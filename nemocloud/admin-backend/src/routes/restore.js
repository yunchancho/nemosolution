const co = require('co')
const path = require('path')
const express = require('express')
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))
const systeminfo = require('systeminformation')
const exec = require('child_process').exec
const meta = require('../meta.json')

const usbScheme = '@usb'

const allowedAttrs = [
	'runOnBoot'
]

let router = express.Router()

router.get('/status', (req, res) => {
	co(function* () {
		let deviceStatus = yield fs.readFileAsync(meta.deviceSettingFilePath, { encoding: 'utf8' })
		res.json(JSON.parse(deviceStatus).apis.restore)
	})
	.catch(err => {
		console.log(err)
		res.status(400).json({ code: 'RESTORE_ERROR_GET_CURRENT_STATUS' })
	})
})

/*
 * GET /usb/:devname request's query 
 *
 * package, config attr could be omitted because these path are fixed in usb
 *
{
	packages: '/packages/',
	config: '/configs/',
	contents: ['/contents/a', '/contents/b']
}
*/


/*
 * GET /usb/:devname response's payload
 *
{
	config: { src: '@usb/configs', size: 100, counts: 2 },
	packages: [
		{ src: '@usb/packages/system', size: 10000, counts: 5 },
		{ src: '@usb/packages/shell', size: 20000, counts: 2 },
		{ src: '@usb/packages/theme', size: 10000, counts: 1 },
		{ src: '@usb/packages/app', size: 0, counts: 0 },
	],
	content: [
		{ src: '@usb/contents/a', size: 2222, counts: 10 },
		{ src: '@usb/contents/b', size: 2222, counts: 10 },
	]
}
*/

function getDirectoryInformation(path) {
	let coroutine = co.wrap(function* () {
		let info = { size: 0, count: 0 }
		try {
			info.size = yield new Promise((resolve, reject) => {
				exec(`du -b -d 0 ${path}`, function (err, stdout, stderr) {
					if (err) {
						return reject({ code: 'RESTORE_ERROR_GET_USB_FILE_SIZE' })
					}
					resolve(parseInt(stdout.split('\t')[0]))
				})
			})

			info.count = yield new Promise((resolve, reject) => {
				exec(`find ${path} -type f | wc -l`, function (err, stdout, stderr) {
					if (err) {
						return reject({ code: 'RESORE_ERROR_GET_USB_FILE_COUNTS' })
					}
					resolve(parseInt(stdout.split('\t')[0]))
				})
			})
		} catch (err) {
			console.log(err)
		}

		return info
	})

	return coroutine()
}

router.get('/usb/:devname/status', (req, res) => {
	co(function* () {

		if (!req.query.src) {
			throw { code: 'RESTORE_ERROR_GET_USB_NO_SRC_PARAM' }
		}
		console.log(req.query.src)

		let reqSrc = JSON.parse(req.query.src)
		if (typeof reqSrc !== 'object') {
			throw { code: 'RESTORE_ERROR_GET_USB_INVALID_SRC_PARAM' }
		}

		let devices = yield systeminfo.blockDevices()
		let usbparts = devices.filter(device => {
			return (device.type === 'part') && ~device.name.indexOf(req.params.devname) && device.mount
		})

		if (!usbparts.length) {
			throw { code: 'RESTORE_ERROR_GET_USB_NO_USB_DEVICE' }
		}

		const mountPoint = usbparts[0].mount

		let data = {
			config: null,
			packages: [],
			contents: []
		}

		try {
			let usbPath = path.join(mountPoint, meta.restoreConfigPath.split(usbScheme)[1])
			data.config = yield getDirectoryInformation(usbPath)
			data.config.src = meta.restoreConfigPath

			for (let [index, type] of meta.packageTypes.entries()) {
				usbPath = path.join(mountPoint, meta.restorePackagePath.split(usbScheme)[1], type)
				data.packages[index] = yield getDirectoryInformation(usbPath)
				data.packages[index].src = path.join(meta.restorePackagePath, type)
			}

			for (let [index, src] of reqSrc.contents.entries()) {
				usbPath = path.join(mountPoint, src.split(usbScheme)[1])
				data.contents[index] = yield getDirectoryInformation(usbPath)
				data.contents[index].src = src
			}	
		} catch (err) {
			console.log(err)
		}

		res.json(data)
	})
	.catch(err => {
		console.log(err)
		res.status(400).json({ code: 'RESTORE_ERROR_GET_CURRENT_STATUS' })
	})
})

router.post('/status', (req, res) => {
	co(function* () {
		let deviceStatus = yield fs.readFileAsync(meta.deviceSettingFilePath, { encoding: 'utf8' })
		deviceStatus = JSON.parse(deviceStatus)
		let params = req.body.runOnBoot
		for (let key of Object.keys(req.body)) {
			if (!allowedAttrs.includes(key)) {
				continue
			}

			console.log(req.body[key])
			if (typeof req.body[key] === 'object') {
				console.log('old: ', req.body[key])
				console.log('new: ', deviceStatus.apis.restore[key])
				let newStatus = Object.assign(deviceStatus.apis.restore[key], req.body[key])
				deviceStatus.apis.restore[key] = newStatus
			} else {
				deviceStatus.apis.restore[key] = req.body[key]
			}
		}

		let data = JSON.stringify(deviceStatus, null, 2)
		yield fs.writeFileAsync(meta.deviceSettingFilePath, data, { encoding: 'utf8' })

		res.json({ result: true })
	})
	.catch(err => {
		console.log(err)
		res.status(400).json({ code: 'RESTORE_ERROR_SET_CURRENT_STATUS' })
	})
})

module.exports = router
