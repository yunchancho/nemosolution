const co = require('co')
const fs = require('fs')
const express = require('express')
const systeminfo = require('systeminformation')
const usbmem = require('../lib/usbmem')
const { exec } = require('child_process')

let router = express.Router()

router.get('/pc', (req, res) => {
	systeminfo.system().then(result => res.json(result))
	.catch(err => res.status(400).json(err))
})

router.get('/cpu', (req, res) => {
	let coroutine = co.wrap(function* () {
		let result = {}
		try {
			result.cpu = yield systeminfo.cpu()
			result.cpuFlags = yield systeminfo.cpuFlags()
			result.cpuCache = yield systeminfo.cpuCache()
			result.cpuCurrentspeed = yield systeminfo.cpuCurrentspeed()
			result.cpuTemporature = yield systeminfo.cpuTemperature()
		} catch (err) {
			throw {
				code: 'SYSTEM_ERROR_GET_CPU'
			}
		}

		return result;

	})

	coroutine().then(result => res.json(result))
	.catch(err => res.status(400).json(err))
})

router.get('/memory', (req, res) => {
	systeminfo.mem().then(result => res.json(result))
	.catch(err => res.status(400).json({ code: 'SYSTEM_ERROR_GET_MEM' }))
})

router.get('/graphics', (req, res) => {
	systeminfo.graphics().then(result => res.json(result))
	.catch(err => res.status(400).json({ code: 'SYSTEM_ERROR_GET_GRAPHICS' }))
})

router.get('/fssize', (req, res) => {
	systeminfo.fsSize().then(result => res.json(result))
	.catch(err => res.status(400).json({ code: 'SYSTEM_ERROR_GET_FSSIZE' }))
})

router.get('/blockdevices', (req, res) => {
  (async () => {
    try {
      await usbmem.mountAll()
      let result = await systeminfo.blockDevices()
      console.log(result)
      res.json(result)
    } catch (err) {
      res.status(400).json({ code: 'SYSTEM_ERROR_GET_BLOCK_DEVICES' })
    }
  })()
})

router.post('/poweroff', (req, res) => {
  let timeoutMin = parseInt(req.body.timeoutMin) || 0
	controlPower('--poweroff', timeoutMin).then(() => res.json({ result: true }))
	.catch(err => res.status(400).json({ code: 'SYSTEM_ERROR_POWER_OFF' }))
})

router.post('/reboot', (req, res) => {
  let timeoutMin = parseInt(req.body.timeoutMin) || 0
	controlPower('--reboot', timeoutMin).then(() => res.json({ result: true }))
	.catch(err => {
    console.log(err)
    res.status(400).json({ code: 'SYSTEM_ERROR_REBOOT' })
  })
})

router.post('/powercancel', (req, res) => {
  exec('shutdown -c')
	res.json({ result: true })
})

router.delete('/usb/:devname', (req, res) => {
	// check devname is valid
	co(function* () {
		try {
			let devices = yield systeminfo.blockDevices()
			let usbparts = devices.filter(device => {
				return (device.type === 'part') && ~device.name.indexOf(req.params.devname) && device.mount
			})

			if (!usbparts.length) {
				return res.json({ result: false })
			}

			let result = yield usbmem.umountOne({ name: req.params.devname })
			fs.rmdirSync(usbparts[0].mount)
			res.json(result)
		} catch (err) {
			console.log(err.stack)
			res.status(400).json({ code: 'SYSTEM_ERROR_EJECT_USB_MEMORY' })
		}
	})
})

function controlPower(command, timeoutMin) {
	return new Promise((resolve, reject) => {
    console.log(`shutdown -t ${timeoutMin} ${command}`)
		exec(`shutdown -t ${timeoutMin} ${command}`)
		resolve()
	})
}

module.exports = router
