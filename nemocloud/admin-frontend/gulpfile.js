const gulp = require('gulp')
const jshint = require('gulp-jshint')
const webpack = require('webpack')
const WebpackDevServer = require('webpack-dev-server')
const { exec } = require('child_process')

const webpackConfigPath = {
	devel: './webpack.devel.js',
	prod: './webpack.prod.js'
}

gulp.task('jshint', () => {
	return gulp.src('./**/*.js')
	.pipe(jshint())
	.pipe(jshint.reporter('default'))
})

gulp.task('devel', [ 'clean:devel', 'webpack:build-devel', 'webpack:run-server' ])
gulp.task('prod', [ 'clean:prod', 'webpack:build-prod' ])
gulp.task('webpack:build-prod', () => {
	const config = require(webpackConfigPath.prod)
	webpack(config, () => console.log('done..'))
})
gulp.task('webpack:build-devel', () => {
	const config = require(webpackConfigPath.devel)
	webpack(config, () => console.log('done..'))
})
gulp.task('webpack:run-server', () => {
	const config = require(webpackConfigPath.devel)
	const server = new WebpackDevServer(webpack(config), { stats: { colors: true } })

	server.listen(config.devServer.port, 'localhost')
})
gulp.task('clean:devel', () => {
	const config = require(webpackConfigPath.devel)
	return new Promise((resolve, reject) => {
		exec(`rm -rf ${config.output.path}`, (err, stdout, stderr) => {
			if (err) {
				return reject(err)
			}
			console.log(stdout)
			resolve()
		})
	})
})
gulp.task('clean:prod', () => {
	const config = require(webpackConfigPath.prod)
	return new Promise((resolve, reject) => {
		exec(`rm -rf ${config.output.path}`, (err, stdout, stderr) => {
			if (err) {
				return reject(err)
			}
			console.log(stdout)
			resolve()
		})
	})
})

gulp.task('default', [ 'devel' ])
gulp.task('build', [ 'prod' ])
