const appConfig = function ($urlRouterProvider, $locationProvider) {

	"ngInject";

	$locationProvider.html5Mode(true).hashPrefix('!');
	$urlRouterProvider.otherwise('/home');
}

export default appConfig;
