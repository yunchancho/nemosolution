const appConfig = (toastrConfig) => {
  'ngInject'
  angular.extend(toastrConfig, {
    positionClass: 'toast-top-center',
    timeOut: 7000,
    allowHtml: true
  });
};

export default appConfig;