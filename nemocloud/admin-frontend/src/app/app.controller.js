class AppController {
	/// #if SNOW_MODE=="cloud"
	constructor($rootScope, $state, $transitions, authModal, modal, cognito, spinner) {
		'ngInject'
		this.cognito = cognito;
	/// #endif
	/// #if SNOW_MODE=="local"
	constructor($rootScope, $state, $transitions, authModal, modal, spinner, localApis) {
		'ngInject'
		this.localApis = localApis;
	/// #endif
	/// #if SNOW_MODE=="usb"
	constructor($rootScope, $state, $transitions, authModal, modal, spinner, localApis) {
		'ngInject'
		this.localApis = localApis;
	/// #endif
		this.name = 'app';
		this.$rootScope = $rootScope;
		this.$state = $state;
		this.$transitions = $transitions;
		this.authModal = authModal;
		this.modal = modal;
		this.spinner = spinner;
		this.isLogin = false;
		this.isConfirm = false;

		this.addOnLoginFunction();
		this.watchTransition();

		$(document).ready(function () {
			// Full height of sidebar
			function fix_height() {
				var heightWithoutNavbar = $("body > #wrapper").height() - 61;
				$(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

				var navbarHeigh = $('nav.navbar-default').height();
				var wrapperHeigh = $('#page-wrapper').height();

				if(navbarHeigh > wrapperHeigh){
					$('#page-wrapper').css("min-height", navbarHeigh + "px");
				}

				if(navbarHeigh < wrapperHeigh){
					$('#page-wrapper').css("min-height", $(window).height()  + "px");
				}

				if ($('body').hasClass('fixed-nav')) {
					if (navbarHeigh > wrapperHeigh) {
						$('#page-wrapper').css("min-height", navbarHeigh + "px");
					} else {
						$('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
					}
				}

			}

			$(window).bind("load resize scroll", function() {
				if(!$("body").hasClass('body-small')) {
					fix_height();
				}
			});

			// Move right sidebar top after scroll
			$(window).scroll(function(){
				if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav') ) {
					$('#right-sidebar').addClass('sidebar-top');
				} else {
					$('#right-sidebar').removeClass('sidebar-top');
				}
			});

			setTimeout(function(){
				fix_height();
			})
		});

		// Minimalize menu when screen is less than 768px
		$(() => {
			$(window).bind("load resize", () => {
				this.minimalizeMenu();
			})
		})
		this.minimalizeMenu()
	}

	minimalizeMenu() {
		if ($(document).width() < 769) {
			console.log("create body-small");
			$('body').addClass('body-small')
		} else {
			console.log("remove body-small");
			$('body').removeClass('body-small')
		}
	}

	addOnLoginFunction() {
		/// #if SNOW_MODE=="cloud"
		this.cognito.addOnLoginFunction(this.name, () => {
			this.isLogin = this.cognito.isLogin();
		})
		/// #endif
		/// #if SNOW_MODE=="local"
		this.localApis.addOnLoginFunction(this.name, () => {
			this.isLogin = this.localApis.isLogin();
		})
		/// #endif
		/// #if SNOW_MODE=="usb"
		this.localApis.addOnLoginFunction(this.name, () => {
			this.isLogin = this.localApis.isLogin();
		})
		/// #endif
	}
	/// #if SNOW_MODE=="local"
	// Check device is confirmed
	checkConfirm() {
		if (!this.localApis.isConfirm) {
			// $state.go('home')
			this.spinner.on()
			let confirm = false;
			return this.localApis.getDeviceStatus()
			.then(result => {
				confirm = (result !== 'initial')
			})
			.catch(err => {
				confirm = false;
				let params = {
					title: '상태 오류',
					content: '디바이스의 상태를 가져올 수 없습니다.',
					error: err.message
				}

				switch(err.message) {
					case 'Failed to fetch':
						params.content = '내부 서버와 통신할 수 없습니다.'
						break;
				}

				this.modal.error(params)
			})
			.then(() => this.spinner.off())
			.then(() => confirm)
		} else {
			return Promise.resolve(true);
		}
	}
	/// #endif
	/// #if SNOW_MODE=="usb"
	// Check device is confirmed
	checkConfirm() {
		if (!this.localApis.isConfirm) {
			// $state.go('home')
			this.spinner.on()
			let confirm = false;
			return this.localApis.getDeviceStatus()
			.then(result => {
				confirm = (result !== 'initial')
			})
			.catch(err => {
				confirm = false;
				let params = {
					title: '상태 오류',
					content: '디바이스의 상태를 가져올 수 없습니다.',
					error: err.message
				}

				switch(err.message) {
					case 'Failed to fetch':
						params.content = '내부 서버와 통신할 수 없습니다.'
						break;
				}

				this.modal.error(params)
			})
			.then(() => this.spinner.off())
			.then(() => confirm)
		} else {
			return Promise.resolve(true);
		}
	}
	/// #endif

	// Check user session token is expired and unauthenticated user attempt to access states that doesn't permitted
	// If session has problem, try to retrive user session and refresh token.
	// If token is refreshed well, move to destination states.
	// if not, stop transition and open login modal
	checkLogin($state) {
		/// #if SNOW_MODE=="local"
		// Check if device is confirmed
		return this.checkConfirm()
		.then(isConfirm => {
			if (!isConfirm) {
				this.isLogin = false;
				this.localApis.signOut()
				this.authModal.open('deviceauth');
				throw new Error('Confirm device')
			}
			// Check logged in
			if (!this.localApis.isLogin()) {
				if (!this.localApis.retrieveCurrentUser()) {
					this.authModal.open('login');
				}
				this.isLogin = this.localApis.isLogin()
			}
			return
		})
		/// #endif
		/// #if SNOW_MODE=="usb"
		// Check if device is confirmed
		return this.checkConfirm()
		.then(isConfirm => {
			if (!isConfirm) {
				this.isLogin = false;
				this.localApis.signOut()
				this.authModal.open('deviceauth');
				throw new Error('Confirm device')
			}
			// Check logged in
			if (!this.localApis.isLogin()) {
				if (!this.localApis.retrieveCurrentUser()) {
					this.authModal.open('login');
				}
				this.isLogin = this.localApis.isLogin()
			}
			return
		})
		/// #endif
		/// #if SNOW_MODE=="cloud"
		if (this.cognito.isLogin()) {
			return Promise.resolve();
		}

		// Session token expired
		return this.cognito.retrieveCurrentUser()
		.then(result => {
			this.authModal.close();
			return result;
		})
		.catch(err => {
			this.authModal.open('login');
			return err;
		})
		.then(result => {
			this.isLogin = this.cognito.isLogin();
      this.$rootScope.$$phase || this.$rootScope.$apply();
			console.log(`Refresh Token: ${result}`);
		})
		/// #endif
	}

	// Check login status on every state changes (include first loading)
	watchTransition() {
		this.$transitions.onStart({}, (trans) => {
			let $state = trans.router.stateService;
			return this.checkLogin($state);
		});
	}

}

export default AppController;
