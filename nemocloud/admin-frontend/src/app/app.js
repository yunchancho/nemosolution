import angular from 'angular';
import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import ngTranslate from 'angular-translate';
import ngSanitize from 'angular-sanitize';
import 'jquery-ui';
import 'jquery-ui/themes/base/core.css';
import 'jquery-ui/themes/base/theme.css';
import 'jquery-ui/themes/base/sortable.css';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widgets/sortable';
import 'angular-ui-sortable/dist/sortable.min.js';

import commons from '../commons/commons.module';
import components from '../components/components.module';

import appComponent from './app.component';
import appRouteConfig from './app.config.route';
import appTransConfig from './app.config.trans';
import appToastrConfig from './app.config.toastr';

console.log(`NODE_ENV..: ${process.env.NODE_ENV}`)
console.log(`SNOW_MODE..: ${process.env.SNOW_MODE}`)

// In case of development
if (process.env.NODE_ENV !== 'production') {
	if (module && module.hot) {
		module.hot.accept()
	}
}

if (process.env.NODE_ENV === 'test') {
	require('angular-mocks/angular-mocks');
	console.log('connect mocks!');
	mocha.setup({ timeout: 10000 });
	// remove console.error
	// console.error = () => {}
}

const name = 'app';

const appModule = angular.module(name, [
	uiRouter,
	uiBootstrap,
	ngTranslate,
	ngSanitize,
	commons,
	components
])
.config(appRouteConfig)
.config(appTransConfig)
.config(appToastrConfig)
.component(name, appComponent)
.run(initialize)

function initialize($rootScope, $state) {
	'ngInject';

	$rootScope.$state = $state;

	/// #if SNOW_MODE=="cloud"
	$rootScope.SNOW_MODE = 'cloud'
	/// #endif
	/// #if SNOW_MODE=="local"
	$rootScope.SNOW_MODE = 'local'
	/// #endif
	/// #if SNOW_MODE=="usb"
	$rootScope.SNOW_MODE = 'usb'
	/// #endif
	console.log("hello", $rootScope.SNOW_MODE);
}

/*
angular.element(document).ready(() => {
	angular.bootstrap(document, [name]);
})
*/

export default appModule;
