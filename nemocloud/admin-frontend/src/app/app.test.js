// import testModule from './app.js'
import testComponent from './app.component';
import testController from './app.controller';
import testTemplate from './app.html';

describe('app', () => {
  let makeController, $rootScope, $state, $transitions, authModal, cognito, spinner, modal;

  beforeEach(window.module('app'));
  beforeEach(inject((_$rootScope_, _$state_, _$transitions_, _authModal_, _cognito_, _spinner_, _modal_) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    $transitions = _$transitions_;
    authModal = _authModal_;
    cognito = _cognito_;
    spinner = _spinner_;
    modal = _modal_;
    makeController = () => {
      return new testController($rootScope, $state, $transitions, authModal, modal, cognito, spinner);
    };
  }));

  // describe('Module', () => {
  //   // top-level specs: i.e., routes, injection, naming
  // });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('isLogin');
      expect(controller).to.have.property('isConfirm');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(testTemplate).to.match(/cpu 실시간 사용량/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = testComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(testTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(testController);
    });
  });
});
