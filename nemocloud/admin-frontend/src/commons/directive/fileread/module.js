import angular from 'angular';
import Fileread from './directive'

const module = angular.module('app.commons.directive.fileread', [
  'app.commons.services.spinner'
])
.directive('fileread', ($injector) => {
  'ngInject'
  return new Fileread($injector)
})
.name;

export default module;
