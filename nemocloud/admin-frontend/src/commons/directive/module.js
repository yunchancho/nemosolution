import angular from 'angular';

import fileread from './fileread/module';

const module = angular.module('app.commons.directive', [
	fileread,
])
.name

export default module;
