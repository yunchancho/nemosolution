import angular from 'angular';

import bytes from './bytes/module'
import s3Directory from './s3Directory/module';
import pkg from './package/module';

const module = angular.module('app.commons.filters', [
	s3Directory,
	pkg,
	bytes
])
.name

export default module;
