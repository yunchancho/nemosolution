let packageFilter = () => {
  return (input, query, install) => {
    if (input === []) {
      return [];
    }
    
    if (query === '' && !install) {
      return input;
    }
    query = query.toLowerCase()
    
    //TODO: Check performance
    return input.filter(pkg => {
      if (install 
        && pkg.view.installed === 'not installed') {
        return false
      }
      
      if (pkg.Package.toLowerCase().includes(query)
        || pkg.Description.toLowerCase().includes(query)
        || pkg.Maintainer.toLowerCase().includes(query)) {
          return true;
      } else {
        return false
      } 
    })
  }
}

export default packageFilter;