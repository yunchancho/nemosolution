import angular from 'angular';
import filter from './filter'

const name = 'packageFilter'

const module = angular.module('app.commons.filters.package', [
])
.filter(name, filter)
.name;

export default module;
