import angular from 'angular';
import AuthModal from './service';

const module = angular.module('app.commons.services.authModal', [
  'ui.bootstrap'
])
.service('authModal', AuthModal)
.name;

export default module;
