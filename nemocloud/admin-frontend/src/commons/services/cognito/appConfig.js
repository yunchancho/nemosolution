export default {
  region: 'ap-northeast-2',
  IdentityPoolId: 'ap-northeast-2:e013a8fa-b20f-46ca-83e9-7f7503e4aff9',
  UserPoolId: 'ap-northeast-2_8rFlu47uh',
  ClientId: '542p4a9ngntdobcba6jbv48qcb',
  attributeNames: {
    customer: 'custom:customer',
    type: 'custom:type',
    permission: 'custom:permission'
  }
}
