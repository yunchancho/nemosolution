import AWS from 'aws-sdk';
import * as AWSCognito from 'amazon-cognito-identity-js';
import { CognitoSyncManager } from 'amazon-cognito-js';
import Promise from 'bluebird'
import appConfig from './appConfig'

const userPool = Promise.promisifyAll(
  new AWSCognito.CognitoUserPool({
    UserPoolId: appConfig.UserPoolId,
    ClientId: appConfig.ClientId,
  })
);

AWS.config.region = appConfig.region;
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: appConfig.IdentityPoolId,
});


class CognitoService {
  constructor() {
    this.cognitoUser;
    this.refreshTimer = null;
    this.type;
    this.onLoginFunctionMap = new Map();
    this.onLogoutFunctionMap = new Map();
  }


  // Register on login listener
  addOnLoginFunction(key, func) {
    this.onLoginFunctionMap.set(key, func);
  }

  // Register on logout listener
  addOnLogoutFunction(key, func) {
    this.onLogoutFunctionMap.set(key, func);
  }


  changePassword(oldPassword, newPassword) {
    if (!this.isLogin()) {
      return Promise.reject(new Error('Not logged in'));
    }
    return this.cognitoUser.changePasswordAsync(oldPassword, newPassword);
  }

  // Confirm user by emailed code
  // Code is sended automatically on signup
  // User can resend code by resendConfirmationCode()
  // this.cognitoUser must have initiated by login or signup
  confirmRegistration(code) {
    if (this.isLogin()) {
      return Promise.reject(new Error('Already logged in'));
    }

    if (!this.cognitoUser) {
      return Promise.reject(new Error('Invalid Access'));
    }

    return this.cognitoUser.confirmRegistrationAsync(code, true);
  }


  // Delete current logged in user (drop out)
  deleteUser() {
    if (!this.isLogin()) {
      return Promise.reject(new Error('Not logged in'));
    }
    return this.cognitoUser.deleteUserAsync()
    .then(result => {
      this.signOut();
      return result;
    })
  }

  getAccessKeyId() {
    if (!this.isLogin()) {
      console.error('Not logged in');
      return null;
    }
    return AWS.config.credentials.accessKeyId;
  }

  getAccessToken() {
    if (!this.isLogin()) {
      console.error('Not logged in');
      return null;
    }
    return this.cognitoUser.signInUserSession.accessToken;
  }

  getConfig() {
    return appConfig;
  }

  getCustomer() {
    if (!this.isLogin()) {
      console.error('Not logged in');
      return Promise.reject(new Error('Not logged in'));
    }
    return this.getUserAttribute(appConfig.attributeNames.customer);
  }

  getType() {
    if (!this.isLogin()) {
      console.error('Not logged in');
      return '';
    }
    return this.type;
  }

  // getSecretAccessKey() {
  //   if (!this.isLogin()) {
  //     console.error('Not logged in');
  //     return null;
  //   }
  //   return AWS.config.credentials.secretAccessKey;
  // }
  //
  // getSessionToken() {
  //   if (!this.isLogin()) {
  //     console.error('Not logged in');
  //     return null;
  //   }
  //   return AWS.config.credentials.sessionToken;
  // }

  // Get specfied attribute from AWS
  // Boolean attribute(email_verified, phone_number_verified, ...)
  // return string 'true' or 'false' (not boolean value)
  getUserAttribute(attributeName) {
    if (!this.isLogin()) {
      return Promise.reject(new Error('Not logged in'));
    }

    return this.cognitoUser.getUserAttributesAsync()
    .then(result => {
      for (let attribute of result) {
        if (attribute.getName() === attributeName) {
          return attribute.getValue();
        }
      }

      // User doesn't have specified attribute
      return '';
    })
  }


  // Return user attributes by Map object
  getUserAttributesMap() {
    return this.cognitoUser.getUserAttributesAsync()
    .then(result => {
      let attributesMap = new Map();
      for (let attribute of result) {
        attributesMap.set(attribute.getName(), attribute.getValue());
      }
      return attributesMap;
    })
  }


  // Doesn't have to be log in
  getUsername() {
    if (!this.cognitoUser) {
      return;
    }
    return this.cognitoUser.getUsername();
  }

  // Initiate this.cognitoUser (not login)
  initCognitoUser(username) {
    let userData = {
      Username : username,
      Pool : userPool
    };
    this.cognitoUser = Promise.promisifyAll(new AWSCognito.CognitoUser(userData));
  }

  // Checks the session to determine whether it is currently logged in
  // !cognitoUser: Not logged in
  // !signInUserSession: Login fail or after registering user
  // !isValid: Token expired (1 hour passed from login)
  isLogin() {
    if ( !this.cognitoUser
      || !this.cognitoUser.signInUserSession
      || !this.cognitoUser.signInUserSession.isValid()) {
      return false;
    }
    return true;
  }

  // Authenticate user by username and password
  // After authenticate success, call onLoginSuccess()
  // Open password challenge Page if user was registered by admin
  // and it is first login
  login(username, password) {
    return new Promise((resolve, reject) => {
      let authenticationData = {
        Username : username,
        Password : password,
      };
      let authenticationDetails = new AWSCognito.AuthenticationDetails(authenticationData);

      this.initCognitoUser(username);
      this.cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: (result) => {
          this.onLoginSuccess(result)
          .then(resolve, reject);
        },
        onFailure: (err) => {
          return reject(err);
        },
        newPasswordRequired: (userAttributes, requiredAttributes) => {
          let error = new Error('New password required');
          error.attributes = [userAttributes, requiredAttributes];
          return reject(error)
        }
      });
    });
  }


  newPasswordChallenge(newPassword, attributesData) {
    return new Promise((resolve, reject) => {
      let onloginParam = {
        onSuccess: (result) => {
          this.onLoginSuccess(result)
          .then(resolve, reject);
        },
        onFailure: (err) => {
          return reject(err);
        },
        newPasswordRequired: (userAttributes, requiredAttributes) => {
        }
      }

      this.cognitoUser.completeNewPasswordChallenge(newPassword, attributesData, onloginParam);
    });
  }

  // Handle login success and get session(refresh browser)
  // Set AWS credentials and get keys
  onLoginSuccess(result) {
		let credentialsParam = {
      IdentityPoolId : appConfig.IdentityPoolId,
			Logins: {}
		};
    let userPoolUrl = `cognito-idp.${appConfig.region}.amazonaws.com/${appConfig.UserPoolId}`;
		credentialsParam.Logins[userPoolUrl] = result.getIdToken().getJwtToken();

    AWS.config.credentials = new AWS.CognitoIdentityCredentials(credentialsParam);

    console.log('Logged in User: ', this.cognitoUser);
    console.log('AWS credentials: ', AWS.config.credentials);

    return new Promise((resolve, reject) => AWS.config.credentials.get(resolve))
    .then(() => this.getUserAttribute(appConfig.attributeNames.type))
    .then(type => {
      this.type = type;

      // Instantiate aws sdk service objects now that the credentials have been updated.
      // Or can register instantiate function on onLoginFunctionMap by addOnLoginFunction() function
      // example: let s3 = new AWS.S3();
      //      or: cognito.addOnLoginFunction(()=> {
      //            let s3 = new AWS.S3();
      //          })

      // Run registered on login function
      for (let [key, value] of this.onLoginFunctionMap) {
        console.log(`onLogin: ${key}`);
        value();
      }
      try {
        let refreshTime = AWS.config.credentials.expireTime.valueOf() - (new Date()).valueOf() + 3000
        this.refreshTimer = setTimeout(() => this.retrieveCurrentUser(), refreshTime)
      } catch(e) {
        console.error('setting refresh token error:', e.message)
      }

      return 'Login Success';
    })
    .catch(err => {
      this.signOut();
      throw err;
    })
  }

  removeOnLoginFunction(key) {
    this.onLoginFunctionMap.delete(key);
  }


  // Reset user password by verificationCode sended in sendFindPasswordCode()
  // User should verify email address before reset password
  resetPassword(verificationCode, newPassword) {
    return new Promise((resolve, reject) => {
      if (this.isLogin()) {
        return reject(new Error('Already logged in'));
      }
      if (!this.cognitoUser) {
        return reject(new Error('Send code first'));
      }

      let findPasswordHandler = {
        onSuccess: (result) => {
          return resolve(result);
        },
        onFailure: (err) => {
          return reject(err);
        },
        inputVerificationCode() {
          // Not used in here
        }
      }

      this.cognitoUser.confirmPassword(verificationCode, newPassword, findPasswordHandler);
    });
  }

  resendConfirmationCode() {
    if (this.isLogin()) {
      return Promise.reject(new Error('Already logged in'));
    }
    if (!this.cognitoUser) {
      return Promise.reject(new Error('Invalid Access'));
    }

    return this.cognitoUser.resendConfirmationCodeAsync();
  }

  // Retrieve the current user from local storage
  // If user logged out, cognitoUser is null
  //
  // If already retrieve the user, no-op
  retrieveCurrentUser() {
    if (this.isLogin()) {
      return Promise.reject(new Error('Already logged in'));
    }

    this.cognitoUser = userPool.getCurrentUser();
    if (this.cognitoUser === null) {
      return Promise.reject(new Error('No user'));
    }
    this.cognitoUser = Promise.promisifyAll(this.cognitoUser);

    return this.cognitoUser.getSessionAsync()
    .then(result => this.onLoginSuccess(result))
  }

  // Run registered on logout function
  runOnLogoutFunction() {
    for (let [key, func] of this.onLogoutFunctionMap) {
      console.log(`onLogout: ${key}`);
      func();
    }
  }

  sendFindPasswordCode(username) {
    return new Promise((resolve, reject) => {
      if (this.isLogin()) {
        return reject(new Error('Already logged in'));
      }

      this.initCognitoUser(username);

      let findPasswordHandler = {
        onSuccess: (result) => {
          // Not used in here
        },
        onFailure: (err) => {
          // Called when sending email is fail
          // ex) user email is not verified
          return reject(err);
        },
        inputVerificationCode() {
          // Called after sending verificate email
          return resolve('Code sended');
        }
      }

      this.cognitoUser.forgotPassword(findPasswordHandler);
    });
  }


  // Send verificate code by using sms message or email
  // Verificate is finished in verificateAttribute()
  sendAttributeVerificateCode(attribute) {
    return new Promise((resolve, reject) => {
      if (!this.isLogin()) {
        return reject(new Error('Not logged in'));
      }

      let phoneVerificateHandler = {
        onSuccess: (result) => {},
        onFailure: (err) => {
          // Called when sending message is fail
          return reject(err);
        },
        inputVerificationCode() {
          // Called after sending verificate message
          return resolve('Code sended');
        }
      }
      this.cognitoUser.getAttributeVerificationCode(attribute, phoneVerificateHandler);
    });
  }


  // Logout
  // Delete user session and aws credentials
  signOut() {
    if (!this.isLogin()) {
      console.log('Not logged in');
      return;
    }
    this.cognitoUser.signOut();
    this.type = '';

    AWS.config.credentials.clearCachedId();
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: appConfig.IdentityPoolId,
    });
    clearTimeout(this.refreshTimer)

    this.runOnLogoutFunction();

    console.log('signed out');
  }


  // Register user in userPool
  // userInfoMap: Map of user information pair (include username, password)
  registerUser(userInfoMap) {
    if (this.isLogin()) {
      return Promise.reject(new Error('Already Logged in'));
    }

    // Construct attribute list (user attributes except username, password)
    let attributeList = [];
    for (let [name, value] of userInfoMap) {
      if (name === 'username' || name === 'password') {
        continue;
      }
      attributeList.push(new AWSCognito.CognitoUserAttribute({
        Name: name,
        Value: value
      }));
    }

    // Send sign up request to AWS
    return userPool.signUpAsync(userInfoMap.get('username')
      , userInfoMap.get('password'), attributeList, null)
    .then(result => {
      this.initCognitoUser(userInfoMap.get('username'));
      return result;
    })
  }

  // Modify user information according to attributesMap
  updateUserAttributes(attributesMap) {
    if (!this.isLogin()) {
      return Promise.reject(new Error('Not logged in'));
    }
    let attributeList = [];
    for (let [Name, Value] of attributesMap) {
      let attribute = {
        Name,
        Value
      }
      attributeList.push(attribute);
    }

    return this.cognitoUser.updateAttributesAsync(attributeList);
  }

  // Verify user attribute by verificationCode sended in sendAttributeVerificateCode()
  verificateAttribute(attribute, verificationCode) {
    return new Promise((resolve, reject) => {
      if (!this.isLogin()) {
        return reject(new Error('Not logged in'));
      }

      let verificateHandler = {
        onSuccess: (result) => {
          return resolve(result);
        },
        onFailure: (err) => {
          return reject(err);
        },
        inputVerificationCode() {
          // Not used in here
        }
      }

      this.cognitoUser.verifyAttribute(attribute, verificationCode, verificateHandler);
    });
  }
}

export default CognitoService;
