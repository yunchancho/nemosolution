import JSZip from 'jszip'
import FileTransfer from '../../../lib/fileTransfer/index'

/// #if SNOW_MODE=='cloud'
import RpcManager from '../../../lib/rpcManager/index'
/// #endif

class Device {
  constructor(name, services) {
    this.name = name;
    /// #if SNOW_MODE=="cloud"
    this.$rootScope = services.$rootScope
    this.cognito = services.cognito
    this.rpcManager = null
    this.connected = false;
    /// #endif
    this.dynamoDB = services.dynamoDB;
    this.iot = services.iot;
    this.lambda = services.lambda;
    this.pkgConfig = [];
    this.attributes = {};
    this.bigdata = [];
    this.contentsync = [];
    this.mandatorypkgs = [];
    this.pkgindex = [];
    this.pkglist = [];
    this.thingTypeName = '';
  }

  /// #if SNOW_MODE=="cloud"

  backupConfig() {
    let params = {
      api: 'backupConfig',
      token: this.cognito.getAccessToken().jwtToken,
      swKey: this.name
    }
    return this.lambda.invoke('NemoSnowBackupDevice', params)
  }

  getScreenshot() {
    let params = {
      type: 'devinfo',
      command: 'listScreenshots'
    }

		return this.rpcManager.runDeviceCommand(params)
  }

  getIpAddress(){
    let params = {
      type:'devinfo',
      command:'listIpaddrs'
    }

    return this.rpcManager.runDeviceCommand(params)
  }

  restoreConfigFromCloud(prefix) {
    let params = {
      api: 'restoreConfig',
      swKey: this.name,
      token: this.cognito.getAccessToken().jwtToken,
      prefix
    }

    return this.lambda.invoke('NemoSnowBackupDevice', params)
  }

  restoreConfigFromLocal(items) {
    let params = {
      api: 'restoreConfigLocal',
      swKey: this.name,
      token: this.cognito.getAccessToken().jwtToken,
      items: this.dynamoDB.removeHashKey(items)
    }

    return this.lambda.invoke('NemoSnowBackupDevice', params)
  }

  runCommand(params) {
    return this.rpcManager.runDeviceCommand(params)
  }

  powerCancel() {
    let params = {
      type: 'powerctrl',
      command: 'powerCancel'
    }
    return this.rpcManager.runDeviceCommand(params)
  }

  shutdown(timeoutMin = 0) {
    let params = {
      type: 'powerctrl',
      command: 'powerOff',
      params: { timeoutMin }
    }
    return this.rpcManager.runDeviceCommand(params)
  }

  reboot(timeoutMin = 0) {
    let params = {
      type: 'powerctrl',
      command: 'reboot',
      params: { timeoutMin }
    }
    return this.rpcManager.runDeviceCommand(params)
  }
  /// #endif

  addContents(content) {
    // refresh device to reduce concurrency problem
    return this.fetch()
    .then(() => {
      if (this.hasContents(content.name)) {
        throw new Error('Already has that contents');
      }
    })
    .then(() => this.dynamoDB.addContentsSource(this.name, content))
    /// #if SNOW_MODE=="cloud"
    .then(() => this.fetch());
    /// #endif
  }

  applyTheme(themeId) {
    return this.dynamoDB.applyTheme(this.name, themeId)
    /// #if SNOW_MODE=="cloud"
    .then(() => this.fetch());
    /// #endif
  }

  applyShell(shellid) {
    return this.dynamoDB.applyShell(this.name, shellid)
    /// #if SNOW_MODE=="cloud"
    .then(() => this.fetch());
    /// #endif
  }

  // Not used in frontend
  // TODO: Concurrency problem on adding after nemo-snow add package with mongodb id
  addPackageIndex(pkgIndex) {
    // refresh device to reduce concurrency problem
    return this.fetch()
    .then((result) => {
      if (this.hasPkgIndex(pkgIndex)) {
        throw new Error('Already in package');
      }
    })
    .then(() => this.dynamoDB.addPackageIndex(this.name, pkgIndex.name))
    /// #if SNOW_MODE=="cloud"
    .then(() => this.fetch());
    /// #endif
  }

  // Decide status by dynamoDB items
  checkInstalled(pkg) {
    let pkgName = pkg.Package;
    for (let pkgItem of this.pkglist) {
      if (pkgItem.pkgname === pkgName) {
        for (let config of this.pkgConfig) {
          if (config.name === pkgName) {
              return 'installed'
          }
        }
        return 'installing'
      }
    }
    return 'not installed';
  }

  // Confirm install complete request (for installer)
  confirmInstall() {
    return this.iot.confirmInstall(this.name)
    /// #if SNOW_MODE=="cloud"
    .then(() => this.fetch());
    /// #endif
  }

  downloadConfigs() {
    let customer = ''
    return this.cognito.getCustomer()
    .then(result => {
      customer = result
      return this.dynamoDB.scanDevice(this.name)
    })
    .then(result => {
      let zip = new JSZip();
      for (let item of result) {
        zip.file(`${item.name}.json`, JSON.stringify(item, null, 2));
      }
      console.log('zip: ', zip)
      return zip.generateAsync({type: 'blob'})
    })
    .then(blob => {
    	let utc = new Date().getTime() + (9 * 60 * 60000) // Korea Standard Time
      let date = new Date(utc).toISOString().replace(/T/, '_').replace(/:/g, '-').replace(/\..+/g, '')
      FileTransfer.downloadBlob(blob, `${customer}_configs_${date}.zip`, null)
    })
  }

  // Scan device table items from dynamoDB
  // and store in device object
  fetch() {
    return this.dynamoDB.scanDevice(this.name)
    .then((result) => {
      let config = this.dynamoDB.getConfig();
      this.pkgConfig = [];
      for (let item of result) {
        let pkgConfigFlag = true;
        for (let defaultKey of Object.keys(config.deviceItems)) {
          if (item.name === config.deviceItems[defaultKey]) {
            // Remove pre&postfix '_'
            let length = config.deviceItems[defaultKey].length
            this[config.deviceItems[defaultKey].slice(1, length - 1)] = item.value;
            pkgConfigFlag = false;
            break;
          }
        }
        if (pkgConfigFlag) {
          this.pkgConfig.push(item);
        }
      }
      return null;
    })
    /// #if SNOW_MODE=="cloud"
    .then(() => this.iot.describeDevice(this))
    /// #endif
  }

  hasContents(name) {
    for (let content of this.contentsync) {
      if (content.name === name) {
        return true;
      }
    }

    return false;
  }

  initialize() {
    /// #if SNOW_MODE=="cloud"
    if (!!this.rpcManager) {
      return Promise.resolve()
    }
    let initPromise = this.dynamoDB.getIotRegistry()
    .then(registry => {
      let params = {
  			clientId: `${this.name}_frontend`,
  			thingName: `${this.name}`,
  			awsHost: registry.endpoints[0].host,
  			region: registry.endpoints[0].region,
  			keepAlive: registry.keepAlive,
  		}
      this.rpcManager = new RpcManager(params, (state) => {
        this.connected = (state.type === 'connected')
        console.log(`${this.name} state: ${state.type}` , this.connected)
        this.$rootScope.$$phase || this.$rootScope.$apply()
      })
    })
    .then(() => this.rpcManager.initialize())
    .catch(err => {
      console.error(`${this.name} initialize error`, err)
      throw err
    })

    initPromise.then(() => {
      let params = {
        type: 'powerctrl',
        command: 'echo',
        params: { knock: true }
      }
      return this.rpcManager.runDeviceCommand(params)
    })
    // TODO: Remove rootScope from here
    .then(result => {
      this.connected = !!result.knock
      this.$rootScope.$$phase || this.$rootScope.$apply()
    })
    .catch(err => {
      this.connected = false
      this.$rootScope.$$phase || this.$rootScope.$apply()
    })

    // initPromise.then(()=>{
    //   let params = {
    //     type:'devinfo',
    //     command:'listIpaddrs'
    //   }
    //
    //   return this.rpcManager.runDeviceCommand(params)
    // })
    // .then(result => {
    //   this.ipAddress = result
    //   this.$rootScope.$$phase || this.$rootScope.$apply()
    // })
    // .catch(err => {
    //   console.log(err);
    // })

    return initPromise;
    /// #endif
    /// #if SNOW_MODE=="local"
    return Promise.resolve()
    ///#endif
    /// #if SNOW_MODE=="usb"
    return Promise.resolve()
    ///#endif
  }

  // Install specified app to all devices in deviceList
  // After adding package name in dynamoDB, snow agent will install package and update db with app configuration
  // TODO: Concurrency problem on installing app while pending delete and
  // immediately after install
  installPackage(pkg, pkgIndex) {
    let pkgName = pkg.Package;
    // Refresh installed app list and check app status
    // by fetchDevice to reduce concurrency problem
    return this.fetch()
    .then(result => {
      // Check device has in app's package
      if (!this.hasPkgIndex(pkgIndex)) {
        throw new Error('Device is not in packages');
      }

      if (this.checkInstalled(pkg) !== 'not installed') {
        throw new Error('Already installed or pending');
      }
    })
    .then(() => this.dynamoDB.installPackage(this.name, pkgName))
    .then(() => this.fetch(this))
  }

  // Return device is in the package or not
  hasPkgIndex(pkgIndex) {
    for (let index of this.pkgindex) {
      if (index.dir === pkgIndex.name + '/') {
        return true;
      }
    }
    return false;
  }

  removeContents(name) {
    return this.fetch()
    .then(() => {
      if (!this.hasContents(name)) {
        throw new Error(`${this.name} doesn't have target contents`);
      }

      // Find package index
      let index = -1;
      for (let i = 0; i < this.contentsync.length; i++) {
        if (this.contentsync[i].name === name) {
          index = i;
          break;
        }
      }
      if (index === -1) {
        // Local data doesn't match with db.
        // Refresh(fetch) data
        this.fetch();
        throw new Error(`Contents list index error`);
      }

      return index;
    })
    .then(index => this.dynamoDB.removeContentsSource(this.name, name, index))
    .then(() => this.fetch())
  }

  removePkgIndex(pkgIndex) {
    return this.fetch()
    .then(() => {
      if (!this.hasPkgIndex(pkgIndex)) {
        throw new Error(`${this.name} doesn't have target package`);
      }

      // Find package index
      let index = -1;
      for (let i = 0; i < this.pkgindex.length; i++) {
        if (this.pkgindex[i].dir === pkgIndex.name + '/') {
          index = i;
          break;
        }
      }
      if (index === -1) {
        // Local data doesn't match with db.
        // Refresh(fetch) data
        this.fetch();
        throw new Error(`Package list index error`);
      }

      return index;
    })
    .then(index => this.dynamoDB.removePkgIndex(this.name, pkgIndex.name, index))
    .then(() => this.fetch())
  }

  setContentSync(contentSync) {
    return this.dynamoDB.setContentSync(this.name, contentSync)
    .then(() => this.fetch())
  }

  // Replace app configuration in dynamoDB
  setPkgConfig(pkg, config) {
    let pkgName = pkg.Package;
    return this.fetch()
    .then(() => {
      return this.dynamoDB.putPkgConfig(this.name, pkgName, config)
    })
    .then(() => this.fetch())
  }

  setPowerTimer(power) {
    return this.dynamoDB.updateDevicePower(this.name, power)
  }

  setTheme(value) {
    return this.dynamoDB.setDeviceThemes(this.name, value)
    /// #if SNOW_MODE=="cloud"
    .then(() => this.fetch());
    /// #endif
  }

  setShell(value) {
    return this.dynamoDB.setDeviceShells(this.name, value)
    /// #if SNOW_MODE=="cloud"
    .then(() => this.fetch());
    /// #endif
  }

  // updatePkgRegistry(pkg, registry) {
  //   let pkgName = pkg.Package;
  //   return this.fetch()
  //   .then(() => {
  //     if (this.checkInstalled(pkg) !== 'installed') {
  //       throw new Error(`Not installed`);
  //     }
  //
  //     // Find package index
  //     let index = -1;
  //     for (let i = 0; i < this.pkglist.length; i++) {
  //       if (this.pkglist[i].pkgname === pkgName) {
  //         index = i;
  //         break;
  //       }
  //     }
  //     if (index === -1) {
  //       // Local data doesn't match with db.
  //       // Refresh(fetch) data
  //       this.fetch();
  //       throw new Error(`Package list index error`);
  //     }
  //     return index;
  //   })
  //   .then(index => this.dynamoDB.updatePkgRegistry(this.name, pkgName, index, registry))
  //   .then(() => this.fetch())
  // }

  updateContents(content) {
    return this.fetch()
    .then(() => {
      if (!this.hasContents(content.name)) {
        throw new Error(`Doesn't have contents`);
      }

      // Find package index
      let index = -1;
      for (let i = 0; i < this.contentsync.length; i++) {
        if (this.contentsync[i].name === content.name) {
          index = i;
          break;
        }
      }
      if (index === -1) {
        // Local data doesn't match with db.
        // Refresh(fetch) data
        this.fetch();
        throw new Error(`Contents list index error`);
      }

      return index;
    })
    .then(index => this.dynamoDB.updateContentsSource(this.name, content, index))
    .then(() => this.fetch())
  }

  // Unnstall specified app on all devices in deviceList
  // After adding package name in dynamoDB, snow agent will install package and update db with app configuration
  uninstallPackage(pkg) {
    let pkgName = pkg.Package;
    return this.fetch()
    .then(() => {
      if (this.checkInstalled(pkg) === 'not installed') {
        throw new Error(`${this.name} is not installed`);
      }

      for (let mandatorypkg of this.mandatorypkgs) {
        if (pkgName === mandatorypkg) {
          throw new Error(`Can't remove mandatory package`)
        }
      }

      // Find package index
      let index = -1;
      for (let i = 0; i < this.pkglist.length; i++) {
        if (this.pkglist[i].pkgname === pkgName) {
          index = i;
          break;
        }
      }
      if (index === -1) {
        // Local data doesn't match with db.
        // Refresh(fetch) data
        this.fetch();
        throw new Error(`Package list index error`);
      }

      return index;
    })
    .then(index => this.dynamoDB.uninstallPackage(this.name, pkgName, index))
    /// #if SNOW_MODE=="cloud"
    .then(() => this.fetch());
    /// #endif
  }
}

export default Device;
