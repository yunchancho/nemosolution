import angular from 'angular';
import DeviceManager from './service';

const module = angular.module('app.commons.services.deviceManager', [
  /// #if SNOW_MODE=='cloud'
  'app.commons.services.cognito',
  'app.commons.services.lambda',
  'app.commons.services.iot',
  /// #endif
  /// #if SNOW_MODE=='local'
  'app.commons.services.localApis',
  'app.commons.services.lambda',
  /// #endif
  /// #if SNOW_MODE=='usb'
  'app.commons.services.localApis',
  'app.commons.services.lambda',
  /// #endif
  'app.commons.services.dynamoDB',
])
.service('deviceManager', DeviceManager)
.name;

export default module;
