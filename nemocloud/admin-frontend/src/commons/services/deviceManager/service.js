import Device from './device'
import Parallel from '../../../lib/parallel/index.js'
import appConfig from './appConfig';

//TODO: Makes only one deviceList exist
class DeviceManager {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, cognito, dynamoDB, packages, lambda, iot){
    'ngInject'
    this.cognito = cognito;
    this.packages = packages;
    this.connected = false;
    this.lambda = lambda;
    this.iot = iot;
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, dynamoDB, lambda, localApis){
    'ngInject'
    this.localApis = localApis;
    this.lambda = lambda;
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, dynamoDB, lambda, localApis){
    'ngInject'
    this.localApis = localApis;
    this.lambda = lambda;
  /// #endif
    this.$rootScope = $rootScope;
    this.dynamoDB = dynamoDB;
    this.deviceList = [];
    this.parallel = new Parallel();

    this.initOnLogin();
    this.destroyOnLogout();
  }

  addContents(content) {
    let requests = this.deviceList.map(device => device.addContents(content));
    return Promise.all(requests)
  }

  // TODO: Concurrency problem on adding after nemo-snow add package with mongodb id
  addPackageIndex(pkg) {
    let requests = this.deviceList.map(device => device.addPackageIndex(pkg));
    return Promise.all(requests)
  }

  applyTheme(themeId) {
    return this.deviceList[0].applyTheme(themeId)
    /// #if SNOW_MODE=='cloud'
    .then(result => this.syncDevice(this.deviceList[0]))
    /// #endif
    .then(() => { this.refresh() })
  }

  applyShell(shellId) {
    return this.deviceList[0].applyShell(shellId)
    /// #if SNOW_MODE=='cloud'
    .then(result => this.syncDevice(this.deviceList[0]))
    /// #endif
    .then(() => { this.refresh() })
  }

  checkInstalledAll(app) {
    return this.deviceList.map(device => {
      return {
        name: device.name,
        installed: device.checkInstalled(app)
      }
    })
  }

  /// #if SNOW_MODE=='cloud'
  initOnLogin() {
    this.cognito.addOnLoginFunction('deviceManager', () => {
      this.deviceList = [];
    })
  }

  destroyOnLogout() {
    this.cognito.addOnLogoutFunction('deviceManager', () => {
      this.deviceList = [];
    })
  }
  /// #endif

  /// #if SNOW_MODE=='local'
  initOnLogin() {
    this.localApis.addOnLoginFunction('deviceManager', () => {
      this.deviceList = [];
    })
  }

  destroyOnLogout() {
    this.localApis.addOnLogoutFunction('deviceManager', () => {
      this.deviceList = [];
    })
  }
  /// #endif

  /// #if SNOW_MODE=='usb'
  initOnLogin() {
    this.localApis.addOnLoginFunction('deviceManager', () => {
      this.deviceList = [];
    })
  }

  destroyOnLogout() {
    this.localApis.addOnLogoutFunction('deviceManager', () => {
      this.deviceList = [];
    })
  }
  /// #endif


  // Call fetchDevice function to each device concurrently
  fetchDevices() {
    let requests = this.deviceList.map(device => device.fetch());
    return Promise.all(requests)
    .then(result => this.deviceList)
  }

  // Fetch user accessible device list by lambda
  listDevices() {
    /// #if SNOW_MODE=='local'
    return this.dynamoDB.listTables()
    .then(deviceList => {
      // Remove non-device tables
      let tables = this.dynamoDB.getConfig().tables;
      for (let i = 0; i < deviceList.length; i++) {
        for (let key of Object.keys(tables)) {
          if (deviceList[i] === tables[key]) {
            deviceList.splice(i, 1);
            i--;
            continue;
          }
        }
      }
      return deviceList;
    })
    /// #endif
    /// #if SNOW_MODE=='usb'
    return this.dynamoDB.listTables()
    .then(deviceList => {
      // Remove non-device tables
      let tables = this.dynamoDB.getConfig().tables;
      for (let i = 0; i < deviceList.length; i++) {
        for (let key of Object.keys(tables)) {
          if (deviceList[i] === tables[key]) {
            deviceList.splice(i, 1);
            i--;
            continue;
          }
        }
      }
      return deviceList;
    })
    /// #endif

    /// #if SNOW_MODE=='cloud'
    let context = {
      api: 'listUserDevice',
      accessToken: this.cognito.getAccessToken().jwtToken
    }

    return Promise.resolve()
    .then(() => this.lambda.invoke('NemoSnowListUserData', context))
    .then(deviceList => deviceList.sort())
    /// #endif
  }

  // Return device list to caller(ex: controller)
  // If deviceList not initiated or empty, fetch list from dynamoDB
  getDeviceList() {
    return new Promise((resolve, reject) => {
      if (this.deviceList.length !== 0) {
        return resolve(this.deviceList);
      }

      let pass = this.parallel.p(() => {
        resolve(this.deviceList);
      })
      if (!pass) {
        // Wait until the first one completes fetching
        return;
      }

      console.log('Init deviceList');

      /// #if SNOW_MODE=='cloud'
      // set common mandatory packages
      let params = {
        api: 'listCommonMandatoryPackages'
      }
      this.lambda.invoke('NemoSnowListUserData', params)
      .then(result => {
        let mandatoryPkgs = result.map(item => item.pkgname)
        this.packages.setMandatoryPkgs(mandatoryPkgs)
        return this.listDevices()
      })
      /// #endif
      /// #if SNOW_MODE=='local'
      this.listDevices()
      /// #endif
      /// #if SNOW_MODE=='usb'
      this.listDevices()
      /// #endif
      .then((result) => {
        let services = {
          $rootScope: this.$rootScope,
          dynamoDB: this.dynamoDB,
          cognito: this.cognito,
          iot: this.iot,
          lambda: this.lambda
        }
        this.deviceList = result.map((deviceName) => {
          return new Device(deviceName, services);
        })

        return Promise.all(this.deviceList.map(device => device.initialize()))
      })
      .then(() => this.fetchDevices())
      .then(result => resolve(this.deviceList))
      .then(result => console.log('Device List:', this.deviceList))
      .catch(err => {
        this.deviceList = []
        reject(err)
      })
      .then(() => {
        // Return deviceList to awaiter even if error occured
        this.parallel.v();
      })
    });
  }

  hasContentsAll(name) {
    return this.deviceList.map(device => {
      return {
        name: device.name,
        hasContents: device.hasContents(name)
      }
    })
  }

  installPackage(device, pkg, pkgIndex) {
    return device.installPackage(pkg, pkgIndex)
    /// #if SNOW_MODE=='cloud'
    .then(result => this.syncDevice(device))
    /// #endif
    .then(() => { this.refresh() })
  }

  refresh() {
    return this.fetchDevices()
    .then(() => this.getDeviceList())
  }

  /// #if SNOW_MODE=='cloud'
  restoreConfigFromCloud(prefix) {
    return this.deviceList[0].restoreConfigFromCloud(prefix)
    .then(result => this.syncDevice(this.deviceList[0]))
  }

  restoreConfigFromLocal(items) {
    return this.deviceList[0].restoreConfigFromLocal(items)
    .then(result => this.syncDevice(this.deviceList[0]))
  }
  /// #endif

  removeContents(name) {
    let requests = this.deviceList.map(device => device.removeContents(name));
    return Promise.all(requests)
  }

  removePkgIndex(pkgIndex) {
    let requests = this.deviceList.map(device => device.removePkgIndex(pkgIndex));
    return Promise.all(requests)
  }

  setContentSync(contentSync) {
    return this.deviceList[0].setContentSync(contentSync)
    /// #if SNOW_MODE=='cloud'
    .then(result => this.syncDevice())
    /// #endif
    .then(() => { this.refresh() })
  }

  setPowerTimer(powerConfig) {
    return this.deviceList[0].setPowerTimer(powerConfig)
    /// #if SNOW_MODE=='cloud'
    .then(result => this.syncDevice())
    /// #endif
    .then(() => { this.refresh() })
  }

  setTheme(value) {
    return this.deviceList[0].setTheme(value)
    /// #if SNOW_MODE=='cloud'
    .then(result => this.syncDevice())
    /// #endif
    .then(() => { this.refresh() })
  }

  setShell(value){
    return this.deviceList[0].setShell(value)
    /// #if SNOW_MODE=='cloud'
    .then(result => this.syncDevice())
    /// #endif
    .then(() => { this.refresh() })
  }

  /// #if SNOW_MODE=='cloud'
  syncDevice(device = this.deviceList[0]) {
    let params = {
      api: 'syncDeviceTable',
      swKey: device.name,
      accessToken: this.cognito.getAccessToken().jwtToken
    }

    return this.lambda.invoke('NemoSnowSyncDeviceTable', params)
    .then(() => this.fetchDevices())
  }
  /// #endif

  // Replace app configuration in dynamoDB
  setPkgConfigAll(device, pkg, config) {
    return device.setPkgConfig(pkg, config)
    /// #if SNOW_MODE=='cloud'
    .then(result => this.syncDevice())
    /// #endif
    .then(() => { this.refresh() })
  }

  // Unnstall specified app on all devices in deviceList
  // After adding package name in dynamoDB, snow agent will install package and update db with app configuration
  uninstallPackage(device, pkg) {
    return device.uninstallPackage(pkg)
    /// #if SNOW_MODE=='cloud'
    .then(result => this.syncDevice(device))
    /// #endif
    .then(() => { this.refresh() })
  }

  updateContents(content) {
    let requests = this.deviceList.map(device => device.updateContents(content));
    return Promise.all(requests)
  }
}

export default DeviceManager;
