const appConfig = {
  tables: {
    customer: 'Customer',
    certificate: 'Certificate',
    registry: 'Registry'
  },
  deviceItems: {
    bigdata: '_bigdata_',
    contentSync: '_contentsync_',
    packageList: '_pkglist_',
    packageIndex: '_pkgindex_',
    mandatoryPackages: '_mandatorypkgs_',
    power: '_power_',
    theme: '_theme_',
    shell: '_shell_'
  },
  /// #if SNOW_MODE=='cloud'
  contentsSrcPrefix: 's3://nemosnow-contents',
  packageIndexHost: 's3://nemosnow-packages.s3-ap-northeast-1.amazonaws.com',
  initDynamoDBParams: {},
  /// #endif
  /// #if SNOW_MODE=='local'
  contentsSrcPrefix: '@usb/contents',
  packageIndexHost: 'file:/opt/pkgrepo',
  initDynamoDBParams: {
    endpoint: `http://${self.location.hostname}:8000`,
  },
  /// #endif
  /// #if SNOW_MODE=='usb'
  contentsSrcPrefix: '@usb/contents',
  packageIndexHost: 'file:/opt/pkgrepo',
  initDynamoDBParams: {
    endpoint: `http://${self.location.hostname}:8000`,
  },
  /// #endif
}

export default appConfig;
