import angular from 'angular';
import DynamoDB from './service';

const module = angular.module('app.commons.services.dynamoDB', [
  /// #if SNOW_MODE=='cloud'
  'app.commons.services.cognito',
  /// #endif
  /// #if SNOW_MODE=='local'
  'app.commons.services.localApis',
  /// #endif
  /// #if SNOW_MODE=='usb'
  'app.commons.services.localApis',
  /// #endif
])
.service('dynamoDB', DynamoDB)
.name;

export default module;
