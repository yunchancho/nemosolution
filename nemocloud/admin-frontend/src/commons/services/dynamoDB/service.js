import AWS from 'aws-sdk';
import Promise from 'bluebird'
import appConfig from './appConfig';

class DynamoDB {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, cognito){
    'ngInject'
    this.cognito = cognito;
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, localApis){
    'ngInject'
    this.localApis = localApis;
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, localApis){
    'ngInject'
    this.localApis = localApis
  /// #endif
    this.$rootScope = $rootScope;
    this.docClient;
    this.dynamoDB;

    this.init();
    this.registerInitFunc();
  }

  // TODO: Condition and concurrency problem with same name and different prefix
  addContentsSource(deviceName, content) {
    let src;
    if (content.src.startsWith('/')) {
      src = `${appConfig.contentsSrcPrefix}${content.src}`
    } else {
      src = `${appConfig.contentsSrcPrefix}/${content.src}`
    }
    let target = {
      name: content.name,
      dest: content.dest.startsWith('/')?content.dest:`/${content.dest}`,
      src
    };
    target = this.removeHashKey(target)

    if (target.name === '') {
      return Promise.reject(new Error('Input name'))
    }

    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.contentSync
      },
      ConditionExpression: `NOT contains(#v, :chk)`,
      UpdateExpression: `set #v = list_append(#v, :source)`,
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':source': [target],
        ':chk': target
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  // Add package index(group) to device
  // TODO: ConditionExpression doesn't apply to mongodb item(has _id attribute)
  addPackageIndex(deviceName, pkgName) {
    let target = {
      dir: pkgName + '/',
      host: `${appConfig.packageIndexHost}`,
      type: 'deb'
    };
    target = removeHashKey(target)

    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.packageIndex
      },
      ConditionExpression: `NOT contains(#v, :chk)`,
      UpdateExpression: 'set #v = list_append(#v, :pkg)',
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':pkg': [target],
        ':chk': target
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  applyTheme(deviceName, themeId) {
    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.theme
      },
      ConditionExpression: `attribute_exists(#v)`,
      UpdateExpression: 'set #v.#u = :t',
      ExpressionAttributeNames: {
        '#v': 'value',
        '#u': 'useDefaultId'
      },
      ExpressionAttributeValues: {
        ':t': themeId
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  applyShell(deviceName, shellId) {
    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.shell
      },
      ConditionExpression: `attribute_exists(#v)`,
      UpdateExpression: 'set #v.#u = :t',
      ExpressionAttributeNames: {
        '#v': 'value',
        '#u': 'useDefaultId'
      },
      ExpressionAttributeValues: {
        ':t': shellId
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  getConfig() {
    return appConfig;
  }

  getIotRegistry() {
    let params = {
      TableName: appConfig.tables.registry,
      Key: {
        "name": 'iot'
      }
    }

    return this.docClient.getAsync(params)
    .then(result => result.Item)
  }

  init() {
    /// #if SNOW_MODE=='cloud'
    this.dynamoDB = Promise.promisifyAll(new AWS.DynamoDB());
    /// #endif
    /// #if SNOW_MODE=='local'
    this.dynamoDB = Promise.promisifyAll(new AWS.DynamoDB(appConfig.initDynamoDBParams));
    /// #endif
    /// #if SNOW_MODE=='usb'
    this.dynamoDB = Promise.promisifyAll(new AWS.DynamoDB(appConfig.initDynamoDBParams));
    /// #endif
    let params = {service: this.dynamoDB};
    this.docClient = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient(params));
  }

  // Add package name in device package list
  // Update occurs only when package list doesn't
  // contain target package
  // ConditionExpression prevents installing app
  // to device on pending install
  installPackage(deviceName, pkgName) {
    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.packageList
      },
      ConditionExpression: `NOT contains(#v, :pkg)`,
      UpdateExpression: 'set #v = list_append(#v, :app)',
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':app': [{pkgname: pkgName}],
        ':pkg': {pkgname: pkgName}
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  /// #if SNOW_MODE=='local'
  listTables() {
    return this.dynamoDB.listTablesAsync({})
    .then(result => result.TableNames);
  }
  /// #endif
  /// #if SNOW_MODE=='usb'
  listTables() {
    return this.dynamoDB.listTablesAsync({})
    .then(result => result.TableNames);
  }
  /// #endif

  /// #if SNOW_MODE=='cloud'
  registerInitFunc() {
    this.cognito.addOnLoginFunction('dynamoDB', () => {
      this.init();
    })
    this.cognito.addOnLogoutFunction('dynamoDB', () => {
      this.init();
    })
  }
  /// #endif


  /// #if SNOW_MODE=='local'
  registerInitFunc() {
    this.localApis.addOnLoginFunction('dynamoDB', () => {
      this.init();
    })
    this.localApis.addOnLogoutFunction('dynamoDB', () => {
      this.init();
    })
  }
  /// #endif

  /// #if SNOW_MODE=='usb'
  registerInitFunc() {
    this.localApis.addOnLoginFunction('dynamoDB', () => {
      this.init();
    })
    this.localApis.addOnLogoutFunction('dynamoDB', () => {
      this.init();
    })
  }
  /// #endif


  // Remove package(index) from device
  removeContentsSource(deviceName, name, index) {
    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.contentSync
      },
      ConditionExpression: `#v[${index}].#n = :d`,
      UpdateExpression: `remove #v[${index}]`,
      ExpressionAttributeNames: {
        '#v': 'value',
        '#n': 'name'
      },
      ExpressionAttributeValues: {
        ':d': name
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  removeHashKey(object) {
    let removeHash = (current) => {
      if (typeof current === 'object') {
        delete current['$$hashKey']
        for (let key of Object.keys(current)) {
          if (typeof current[key] === 'string') {
            if (current[key] === '') {
              delete current[key];
            }
          } else if (current[key] === null){
            delete current[key];
          } else {
            removeHash(current[key])
          }
        }
      }
    }
    let clone = JSON.parse(JSON.stringify(object))
    removeHash(clone);
    return clone;
  }


  // Remove package index from device
  removePkgIndex(deviceName, pkgName, index) {
    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.packageIndex
      },
      ConditionExpression: `#v[${index}].dir = :dir`,
      UpdateExpression: `remove #v[${index}]`,
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':dir': pkgName + '/'
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  scanDevice(deviceName) {
    let params = {
      TableName: deviceName
    }

    return this.docClient.scanAsync(params)
      .then(data => data.Items)
  }

  setContentSync(deviceName, contentSync) {
    let value = this.removeHashKey(contentSync)

    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.contentSync
      },
      ConditionExpression: `attribute_exists(#v)`,
      UpdateExpression: `set #v = :value`,
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':value': value,
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  setDeviceItem(deviceName, key, value) {
    let item = this.removeHashKey(value)

    let params = {
      TableName: deviceName,
      Key: {
        name: key
      },
      ConditionExpression: `attribute_exists(#v)`,
      UpdateExpression: `set #v = :value`,
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':value': item,
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  setDeviceThemes(deviceName, value) {
    let item = this.removeHashKey(value)

    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.theme
      },
      ConditionExpression: `attribute_exists(#v)`,
      UpdateExpression: `set #v.#t = :value`,
      ExpressionAttributeNames: {
        '#v': 'value',
        '#t': 'configs'
      },
      ExpressionAttributeValues: {
        ':value': item,
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  setDeviceShells(deviceName, value) {
    let item = this.removeHashKey(value)

    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.shell
      },
      ConditionExpression: `attribute_exists(#v)`,
      UpdateExpression: `set #v.#t = :value`,
      ExpressionAttributeNames: {
        '#v': 'value',
        '#t': 'configs'
      },
      ExpressionAttributeValues: {
        ':value': item,
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  // Update device's package configuration
  putPkgConfig(deviceName, pkgname, config) {
    let params = {
      TableName: deviceName,
      Item: {
        name: pkgname,
        value: this.removeHashKey(config),
      }
    };

    return this.docClient.putAsync(params);
  }

  updateDevicePower(deviceName, power) {
    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.power
      },
      UpdateExpression: 'set #v = :p',
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':p': this.removeHashKey(power)
      },
      ReturnValues: 'UPDATED_NEW'
    };
    return this.docClient.updateAsync(params);
  }

  // Update device's app configuration
  updatePkgConfig(deviceName, pkgname, config) {
    let params = {
      TableName: deviceName,
      Key: {
        name: pkgname
      },
      ConditionExpression: 'attribute_exists(#v)',
      UpdateExpression: 'set #v = :config',
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':config': this.removeHashKey(config)
      },
      ReturnValues: 'UPDATED_NEW'
    };
    return this.docClient.updateAsync(params);
  }

  // Update device's app registry configuration
  updatePkgRegistry(deviceName, pkgname, index, registry) {
    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.packageList
      },
      ConditionExpression: `#v[${index}].#pn = :pn`,
      UpdateExpression: `set #v[${index}].#r = :registry`,
      ExpressionAttributeNames: {
        '#pn': 'pkgname',
        '#v': 'value',
        '#r': 'registry'
      },
      ExpressionAttributeValues: {
        ':pn': this.removeHashKey(pkgname),
        ':registry': this.removeHashKey(registry)
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  // Update contentsync item in device
  updateContentsSource(deviceName, content, index) {
    let src;
    if (content.src.startsWith('/')) {
      src = `${appConfig.contentsSrcPrefix}${content.src}`
    } else {
      src = `${appConfig.contentsSrcPrefix}/${content.src}`
    }

    let target = {
      name: content.name,
      dest: content.dest.startsWith('/')?content.dest:`/${content.dest}`,
      src
    };
    target = this.removeHashKey(target)

    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.contentSync
      },
      ConditionExpression: `#v[${index}].#n = :d`,
      UpdateExpression: `set #v[${index}] = :c`,
      ExpressionAttributeNames: {
        '#v': 'value',
        '#n': 'name'
      },
      ExpressionAttributeValues: {
        ':d': target.name,
        ':c': target
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }


  // Remove package from device package list
  // Removing uses index which is fetched to local store
  // If the index changes or doesn't match, throw error
  uninstallPackage(deviceName, pkgName, pkgindex) {
    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.packageList
      },
      ConditionExpression: `#v[${pkgindex}].pkgname = :p`,
      UpdateExpression: `remove #v[${pkgindex}]`,
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':p': pkgName
      },
      ReturnValues: 'UPDATED_NEW'
    };
    return this.docClient.updateAsync(params);
  }
}

export default DynamoDB;
