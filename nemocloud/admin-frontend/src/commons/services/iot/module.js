import angular from 'angular';
import Iot from './service';

const module = angular.module('app.commons.services.iot', [
  /// #if SNOW_MODE=='cloud'
  'app.commons.services.cognito',
  /// #endif
  /// #if SNOW_MODE=='local'
  'app.commons.services.localApis',
  /// #endif
  /// #if SNOW_MODE=='usb'
  'app.commons.services.localApis',
  /// #endif
  'app.commons.services.dynamoDB'
])
.service('iot', Iot)
.name;

export default module;
