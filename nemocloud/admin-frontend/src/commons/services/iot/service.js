import AWS from 'aws-sdk';
import Parallel from '../../../lib/parallel/index.js'
import Promise from 'bluebird'
import appConfig from './appConfig'


class Iot {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, cognito, dynamoDB){
    'ngInject'
    this.cognito = cognito;
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, localApis, dynamoDB){
    'ngInject'
    this.localApis = localApis;
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, localApis, dynamoDB){
    'ngInject'
    this.localApis = localApis;
  /// #endif
    this.$rootScope = $rootScope;
    this.dynamoDB = dynamoDB;
    this.iot;

    this.init();
    this.registerInitFunc();
  }

  confirmInstall(thingName) {
    let params = {thingName};

    return this.iot.describeThingAsync(params)
    .then(thing => {
      if (thing.attributes.status !== appConfig.status.waitConfirm) {
        throw new Error('Wrong Iot status');
      }

      thing.attributes.status = appConfig.status.activated;
      return thing;
    })
    .then(thing => this.updateThing(thing))
  }

  describeDevice(device) {
    /// #if SNOW_MODE=='local'
    return this.localApis.getDeviceStatus()
    .then(status => {
      device.attributes = {
        status
      }
      return device
    })
    /// #endif
    /// #if SNOW_MODE=='usb'
    return this.localApis.getDeviceStatus()
    .then(status => {
      device.attributes = {
        status
      }
      return device
    })
    /// #endif
    /// #if SNOW_MODE=='cloud'
    let params = {
      thingName: device.name
    }
    return this.iot.describeThingAsync(params)
    .then(thing => {
      device.attributes = thing.attributes;
      device.thingTypeName = thing.thingTypeName;
      return device;
    })
    /// #endif
  }

  getConfig() {
    return appConfig
  }

  init() {
    this.iot = Promise.promisifyAll(new AWS.Iot());
  }

  /// #if SNOW_MODE=='cloud'
  registerInitFunc() {
    this.cognito.addOnLoginFunction('iot', () => {
      this.init();
    })
    this.cognito.addOnLogoutFunction('iot', () => {
      this.init();
    })
  }
  /// #endif

  /// #if SNOW_MODE=='local'
  registerInitFunc() {
    this.localApis.addOnLoginFunction('iot', () => {
      this.init();
    })
    this.localApis.addOnLogoutFunction('iot', () => {
      this.init();
    })
  }
  /// #endif
  /// #if SNOW_MODE=='usb'
  registerInitFunc() {
    this.localApis.addOnLoginFunction('iot', () => {
      this.init();
    })
    this.localApis.addOnLogoutFunction('iot', () => {
      this.init();
    })
  }
  /// #endif

  // Update thing's attributes.
  // Thing's version will be added by 1
  updateThing(thing) {
    let params = {
      thingName: thing.thingName,
      attributePayload: {
        attributes: thing.attributes
      },
    }
    return this.iot.updateThingAsync(params);
  }
}

export default Iot;
