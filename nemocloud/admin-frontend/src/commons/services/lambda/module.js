import angular from 'angular';
import Lambda from './service';

const module = angular.module('app.commons.services.lambda', [
  /// #if SNOW_MODE=='cloud'
  'app.commons.services.cognito',
  /// #endif
  /// #if SNOW_MODE=='local'
  'app.commons.services.localApis',
  /// #endif
  /// #if SNOW_MODE=='usb'
  'app.commons.services.localApis',
  /// #endif
])
.service('lambda', Lambda)
.name;

export default module;
