import AWS from 'aws-sdk';

class Lambda {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, cognito){
    'ngInject'
    this.cognito = cognito;
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, localApis){
    'ngInject'
    this.localApis = localApis;
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, localApis){
    'ngInject'
    this.localApis = localApis;
  /// #endif
    this.$rootScope = $rootScope;
    this.lambda;

    this.init();
    this.registerInitFunc();
  }

  init() {
    this.lambda = new AWS.Lambda();
  }

  /// #if SNOW_MODE=='cloud'
  registerInitFunc() {
    this.cognito.addOnLoginFunction('lambda', () => {
      this.init();
    })
    this.cognito.addOnLogoutFunction('lambda', () => {
      this.init();
    })
  }
  /// #endif

  /// #if SNOW_MODE=='local'
  registerInitFunc() {
    this.localApis.addOnLoginFunction('lambda', () => {
      this.init();
    })
    this.localApis.addOnLogoutFunction('lambda', () => {
      this.init();
    })
  }
  /// #endif
  /// #if SNOW_MODE=='usb'
  registerInitFunc() {
    this.localApis.addOnLoginFunction('lambda', () => {
      this.init();
    })
    this.localApis.addOnLogoutFunction('lambda', () => {
      this.init();
    })
  }
  /// #endif
  /// #if SNOW_MODE=='cloud'
  invoke(functionName, context) {
    return new Promise((resolve, reject) => {
      if (!this.cognito.isLogin()) {
        reject(new Error('Not logged in'));
      }

      let b64Context = AWS.util.base64.encode(JSON.stringify(context))

      var params = {
        FunctionName: functionName,
        ClientContext: b64Context
      }

      this.lambda.invoke(params, (err, result) => {
        if (err) {
          // Lambda sysrem error(not invoked in lambda function callback parameter)
          // ex) function permission error
          return reject(err);
        }

        if (!!result.FunctionError) {
          // Error which is invoked by lambda function callback (user defined error)
          return reject(!!result.Payload && JSON.parse(result.Payload).errorMessage);
        }

        return resolve(!!result.Payload && JSON.parse(result.Payload));
      });
    });
  }
  /// #endif
}

export default Lambda;
