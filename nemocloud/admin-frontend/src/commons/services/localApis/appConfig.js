const appConfig = {
  endpoint: `http://${self.location.hostname}:80/`,
  contentsSrcPrefix: '/mnt/contents/',
  packageIndexHost: 'file:/opt/pkgrepo',
  backupBucket: 'nemosnow-backups'
}

export default appConfig;
