import angular from 'angular';
import LocalApis from './service';

const module = angular.module('app.commons.services.localApis', [
])
.service('localApis', LocalApis)
.name;

export default module;
