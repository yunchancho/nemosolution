import AWS from 'aws-sdk'
import request from 'request';
import appConfig from './appConfig';

// Init AWS config for local frontend(dummy)
AWS.config.region = 'ap-northeast-2';
AWS.config.credentials = new AWS.Credentials({
		accessKeyId: 'AAAAAAAAAAAAAAAAAAAA',
		secretAccessKey: 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
});

class LocalApis {
  constructor($rootScope){
    'ngInject'
    this.$rootScope = $rootScope;
    this.accessToken = null;
    this.isConfirm = false;
    this.mode = '';
    this.baseRequest = request.defaults({
      baseUrl: appConfig.endpoint,
      json: true
    })
    this.onLoginFunctionMap = new Map();
    this.onLogoutFunctionMap = new Map();
  }

  // Register on login listener
  addOnLoginFunction(key, func) {
    this.onLoginFunctionMap.set(key, func);
  }

  // Register on logout listener
  addOnLogoutFunction(key, func) {
    this.onLogoutFunctionMap.set(key, func);
  }

  isLogin() {
    return !!this.accessToken;
  }

  backup(target, dest) {
    let options = {
      url: `backup/${target}?token=${this.accessToken}`,
      body: {
        dest
      }
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.post(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body);
      })
    })
  }

  backupCloud(target, swkey) {
    let customer = swkey.slice(0, swkey.indexOf('_'))
    let dest = `s3://${appConfig.backupBucket}/${customer}/${swkey}`
    return this.backup(target, dest);
  }

  playSampleSound(sinkId) {
    let options = {
      url: `audio/sink/${sinkId}/play?token=${this.accessToken}`,
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.get(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body);
      })
    })
  }

  changeMode(config) {
    let options = {
      url: `device/mode?token=${this.accessToken}`,
      body: {
        mode: config.mode,
        keepConfig: config.keepConfig
      }
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.post(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }

        this.setMode(config.mode);
        return resolve(body);
      })
    })
  }

  changePassword(oldPasswd, newPasswd) {
    let options = {
      url: `auth/passwd?token=${this.accessToken}`,
      body: {
        oldPasswd,
        newPasswd,
      }
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.post(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body.result);
      })
    })
  }

  ejectUsb(usb) {
		if (usb.fs.length === 0) {
			return Promise.reject(new Error(`USB doesn't have partition`))
		}
		let fs = usb.fs[0].fs
    let devName = fs.slice(fs.lastIndexOf('/') + 1)
    let options = {
      url: `system/usb/${devName}?token=${this.accessToken}`,
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.delete(options, (error, response, body) => {
        if (error || response.statusCode !== 200) {
          return reject(error || body);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body);
      })
    })
  }

  getAccessToken() {
    return this.accessToken;
  }

  getBackupStatus() {
    let options = {
      url: `backup/status?token=${this.accessToken}`,
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.get(options, (error, response, body) => {
        if (error || response.statusCode !== 200) {
          return reject(error || body);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body);
      })
    })
  }

  getConfig() {
    return appConfig
  }

  getDeviceStatus() {
    let options = {
      url: `device/status`,
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.get(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body.status);
      })
    })
  }

  getMode() {
    let options = {
      url: `device/mode?token=${this.accessToken}`,
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.get(options, (error, response, body) => {
        if (error || response.statusCode !== 200) {
          return reject(error || body);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        this.setMode(body.mode);
        return resolve(body);
      })
    })
  }

  getPackageIndex(index) {
    let options = {
      url: `package/index?token=${this.accessToken}&host=${appConfig.packageIndexHost}&dir=${index}`,
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.get(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body.content);
      })
    })
  }

  getRestoreStatus() {
    let options = {
      url: `restore/status?token=${this.accessToken}`,
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.get(options, (error, response, body) => {
        if (error || response.statusCode !== 200) {
          return reject(error || body);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body);
      })
    })
  }

  getSinks() {
    let options = {
      url: `audio/sinks?token=${this.accessToken}`,
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.get(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body);
      })
    })
  }


  // target: [pc, cpu, memory, graphics, fssize, blockdevices]
  getSystemInfo(target) {
    let options = {
      url: `system/${target}?token=${this.accessToken}`,
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.get(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body);
      })
    })
  }

  getSwKey() {
    let options = {
      url: `device/swkey?token=${this.accessToken}`,
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.get(options, (error, response, body) => {
        if (error || response.statusCode !== 200) {
          return reject(error || body);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body.swKey);
      })
    })
  }

	// Return Usb and that's own fs list
	getUsbList() {
		let usbs = [];
		return this.getSystemInfo('blockdevices')
		.then(devices => {
			usbs = devices.filter(device => {
				return (device.removable && device.protocol === 'usb')
			})
			usbs.forEach(usb => {
				usb.fs = []
			})
		})
		.then(() => this.getSystemInfo('fssize'))
		.then(result => {
			for (let fs of result) {
				let name = fs.fs.slice(fs.fs.lastIndexOf('/') + 1)
				for (let usb of usbs) {
					if (name.startsWith(usb.name)) {
						usb.fs.push(fs)
						break;
					}
				}
			}
			usbs = usbs.filter(usb => {
				return usb.fs.length !== 0
			})
			console.log('usbs: ', usbs)
			return usbs;
		})
	}

  getUsbStatus(usb, contents) {
    let devName = usb.fs.slice(usb.fs.lastIndexOf('/') + 1);
    let src = {
      contents: contents.map(content => content.src)
    }
    let options = {
      url: `restore/usb/${devName}/status?token=${this.accessToken}&src=${JSON.stringify(src)}`,
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.get(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body);
      })
    })
  }

  login(id, passwd) {
    let options = {
      url: `auth/login`,
      body: {
        id,
        passwd
      }
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.post(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(this.onLoginSuccess(body))
      })
    })
  }

  mute(sink, value) {
    let options = {
      url: `audio/sink/${sink}/mute?token=${this.accessToken}`,
      body: {
        value: (value?0:1)
      }
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.post(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }

        return resolve(body);
      })
    })
  }

  onLoginSuccess(session) {
    this.accessToken = session.token;
    this.isConfirm = session.deviceVerified;

    if (this.isConfirm) {
      sessionStorage.setItem('token', session.token)
      sessionStorage.setItem('deviceVerified', session.deviceVerified)

      // Get mode and run onLoginFunction
      return this.getMode()
      .catch(err => {
        console.error('getMode fails on login success', err)
      })
      .then(() => {
        console.log('Login', session)
        return session.token;
      })
    } else {
      return Promise.resolve(false);
    }
  }

  powerOff() {
    let options = {
      url: `system/poweroff?token=${this.accessToken}`,
      body: {
				timeoutMin: 0
			}
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.post(options, (error, response, body) => {
        if (error || response.statusCode !== 200) {
          return reject(error || body);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }

        return resolve(body);
      })
    })
  }

  reboot() {
    let options = {
      url: `system/reboot?token=${this.accessToken}`,
      body: {
				timeoutMin: 0
			}
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.post(options, (error, response, body) => {
        if (error || response.statusCode !== 200) {
          return reject(error || body);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }

        return resolve(body);
      })
    })
  }

  retrieveCurrentUser() {
    let session = {
      token: sessionStorage.getItem('token'),
      deviceVerified: (sessionStorage.getItem('deviceVerified') === 'true')?true:false
    }

    if (session.token !== '' && session.deviceVerified) {
      return this.onLoginSuccess(session)
    } else {
      return false;
    }
  }

  requestConfirm(swKey) {
    let options = {
      url: `device/status`,
      body: {
        status: 'wait_confirm',
        swKey,
				timeoutSec: 5
      }
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.post(options, (error, response, body) => {
        if (error || response.statusCode !== 200) {
          return reject(error || body);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }

        this.isConfirm = true;
        sessionStorage.setItem('deviceVerified', this.isConfirm)
         return resolve(body);
      })
    })
  }

  // Run registered on login function
  runOnLoginFunction() {
    for (let [key, func] of this.onLoginFunctionMap) {
      console.log(`onLogin: ${key}`);
      func();
    }
  }

  // Run registered on logout function
  runOnLogoutFunction() {
    for (let [key, func] of this.onLogoutFunctionMap) {
      console.log(`onLogout: ${key}`);
      func();
    }
  }

  // Private method
  setMode(mode) {
    if (this.mode !== mode) {
      this.mode = mode;
      this.runOnLoginFunction()
    }
  }

  setRestoreStatus(runOnBoot) {
    let options = {
      url: `restore/status?token=${this.accessToken}`,
      body: {
        runOnBoot
      }
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.post(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }

        return resolve(body);
      })
    })
  }

  setVolume(sink, volume) {
    let options = {
      url: `audio/sink/${sink}/volume?token=${this.accessToken}`,
      body: {
        value: volume
      }
    }

    return new Promise((resolve, reject) => {
      this.baseRequest.post(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }

        return resolve(body);
      })
    })
  }

  signOut() {
    this.runOnLogoutFunction()
    sessionStorage.setItem('token', '')
    sessionStorage.setItem('deviceVerified', '')
    this.accessToken = null;
    this.isConfirm = false;
    this.mode = '';
  }
}

export default LocalApis;
