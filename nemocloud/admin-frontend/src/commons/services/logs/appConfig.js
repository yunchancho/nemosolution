export default {
  ops: {
    add: '추가',
    modify: '수정',
    remove: '제거',
    install: '설치',
    apply:'적용',
    uninstall: '삭제',
    upload: '업로드',
    create: '생성',
    backup: '백업',
    restore: '복구',
    reboot:'재시작',
    shutdown:'종료'
  }
}
