import angular from 'angular';
import Logs from './service';

const module = angular.module('app.commons.services.logs', [
])
.service('logs', Logs)
.name;

export default module;
