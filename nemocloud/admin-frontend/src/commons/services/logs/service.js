import appConfig from './appConfig'

class Logs {
  constructor(cognito, lambda){
    'ngInject'
    this.cognito = cognito;
    this.lambda = lambda;
  }

  success(message) {
    let log = {
        prefix: '',
        status: 'success',
        message
      }
    
    return this._put(log)
  }
  
  _put(log) {
    let exist = false
    for (let operator of Object.keys(appConfig.ops)) {
      if (operator === log.message.ops) {
        exist = true;
      }
    }
    if (!exist){
      return Promise.reject(new Error(`invalid ops: ${log.message.ops}`))
    }
    
    let params = {
      api: 'putLog',
      accessToken: this.cognito.getAccessToken().jwtToken,
      log
    }
    // log.permission will be inserted in lambda function
    return this.lambda.invoke('NemoSnowLogs', params)
  }
  
  getConfig() {
    return appConfig;
  }
  
  getLogs() {
    try {
      let params = {
        api: 'getLogs',
        accessToken: this.cognito.getAccessToken().jwtToken,
      }
      return this.lambda.invoke('NemoSnowLogs', params)
    } catch(e) {
      return Promise.reject(e)
    }
  }
}

export default Logs;
