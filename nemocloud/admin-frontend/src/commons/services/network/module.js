import angular from 'angular';
import Network from './service';

const module = angular.module('app.commons.services.network', [
  'app.commons.services.localApis'
])
.service('network', Network)
.name;

export default module;
