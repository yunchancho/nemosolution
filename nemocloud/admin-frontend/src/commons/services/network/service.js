import request from 'request';
import Parallel from '../../../lib/parallel/index.js'

let baseRequest;

class Network {
  constructor($rootScope, localApis){
    'ngInject'
    this.$rootScope = $rootScope;
    this.localApis = localApis;
    this.parallel = new Parallel();
    this.status = false;
    this.wifi = {
      ifces: []
    };
    this.ethernet = {
      ifces: []
    }
    
    
    baseRequest = request.defaults({
      baseUrl: localApis.getConfig().endpoint,
      json: true,
    })
  }
  
  connectAccesspoint(ifce, ssid, passwd) {
    let options = {
      url: `network/wifi/${ifce.device}/connection?token=${this.localApis.getAccessToken()}`,
      body: {
        ssid,
        passwd,
      }
    }
    
    return new Promise((resolve, reject) => {
      baseRequest.post(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body.result);
      })
    })
  }
  
  connectEthernet(ifce, config) {
    let options = {
      url: `network/ethernet/${ifce.device}/connection?token=${this.localApis.getAccessToken()}`,
      body: {
        dhcp: config.dhcp,
        ip: config.ip,
        gw: config.gw,
        dns: (config.autoDNS?[]:config.dns),
      }
    }
    
    console.log(options)
    return new Promise((resolve, reject) => {
      baseRequest.post(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body);
      })
    })
  }
  
  disconnectWifi(ifce) {
    let options = {
      url: `network/wifi/${ifce.device}/disconnection?token=${this.localApis.getAccessToken()}`,
      body: {}
    }
    
    return new Promise((resolve, reject) => {
      baseRequest.post(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        return resolve(body.result);
      })
    })
  }

  fetchNetwork(mode) {
    this.wifi = {
      ifces: []
    };
    this.ethernet = {
      ifces: []
    }
    return this.getNetwork(mode)
  }
  
  getAccesspoints(ifce) {
    let options = { 
      url: `network/wifi/${ifce.device}/accesspoints?token=${this.localApis.getAccessToken()}`,
    }
    
    return new Promise((resolve, reject) => {
      baseRequest.get(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        
        // Move connected Ap to first
        for (let i = 0; i < body.length; i++) {
          if (body[i].connected) {
            let connected = body[i];
            body.splice(i, 1);
            body.unshift(connected);
          }
        }
        
        ifce.accesspoints = body;
        
        return resolve(ifce.accesspoints);
      })
    })
  }
  
  
  // Init interfaces
  getInterfaces() {
    let options = { 
      url: `network/ifces?token=${this.localApis.getAccessToken()}`,
    }
    
    return new Promise((resolve, reject) => {
      baseRequest.get(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        this.wifi.ifces = []
        this.ethernet.ifces = []
        for (let ifce of body) {
          if (ifce.type === "wifi") {
            this.wifi.ifces.push(ifce);
          }
          if (ifce.type === "ethernet") {
            this.ethernet.ifces.push(ifce);
          }
        }
        return resolve(body);
      })
    })
  }
  
  getNetwork(mode) {
    return new Promise((resolve, reject) => {
      if (this.wifi.ifces.length !== 0 || this.ethernet.ifces.length !== 0) {
        return resolve((mode === 'wifi')?this.wifi:this.ethernet);
      }
      
      let pass = this.parallel.p(() => {
        return resolve((mode === 'wifi')?this.wifi:this.ethernet);
      })
      if (!pass) {
        // Wait until the first one completes fetching
        return;
      }
      
      console.log('Init network')
      this.getInterfaces()
      .then(() => this.refreshInterfaces())
      .then(() => {
        return resolve((mode === 'wifi')?this.wifi:this.ethernet);
      })
      .catch(reject)
      .then(() => {
        // Return deviceList to awaiter even if error occured
        this.parallel.v();
      })
    })
  }
  
  getStatus() {
    let options = {
      url: `network/status?token=${this.localApis.getAccessToken()}`,
    }
    
    return new Promise((resolve, reject) => {
      baseRequest.get(options, (error, response, body) => {
        if (error || response.statusCode !== 200) {
          return reject(error || body);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        
        this.status = body.internet;
        return resolve(body);
      })
    })
  }
  
  refreshInterfaces() {
    let requests = []
    for (let ifce of this.wifi.ifces) {
      requests.push(this.getAccesspoints(ifce))
    }
    return Promise.all(requests);
  }
  
  refreshNetwork() {
    this.wifi.ifces = [];
    this.ethernet.ifces = [];
    return this.getNetwork();
  }
  
  setStatus(on) {
    let options = {
      url: `network/status?token=${this.localApis.getAccessToken()}`,
      body: {
        status: (on?'enabled':'disabled'),
      }
    }
    
    return new Promise((resolve, reject) => {
      baseRequest.post(options, (error, response, body) => {
        if (error) {
          return reject(error);
        }
        if (body.code) {
          return reject(new Error(body.code));
        }
        
        if (!on) {
          this.wifi.ifces = []
          this.ethernet.ifces = []
        }
        
        return resolve(body);
      })
    })
  }
}

export default Network;