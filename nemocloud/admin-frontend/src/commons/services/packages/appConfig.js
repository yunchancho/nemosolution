const appConfig = {
  index: [
    'system',
    'shell',
    'application',
    'theme',
  ]
}

export default appConfig;
