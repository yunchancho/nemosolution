import angular from 'angular';
import Packages from './service';

const module = angular.module('app.commons.services.packages', [
  /// #if SNOW_MODE=='cloud'
  'app.commons.services.cognito',
  /// #endif
  /// #if SNOW_MODE=='local'
  'app.commons.services.localApis',
  /// #endif
  /// #if SNOW_MODE=='usb'
  'app.commons.services.localApis',
  /// #endif
  'app.commons.services.dynamoDB',
  'app.commons.services.s3',
])
.service('packages', Packages)
.name;

export default module;
