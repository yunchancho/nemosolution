import encoding from 'text-encoding';
/// #if SNOW_MODE=='usb'
import appConfig from './appConfig'
const zlib = require('zlib')
const co = require('co')
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))

const fileScheme = 'file:'
const indexFile = 'Packages.gz'
/// #endif

class Packages {
  constructor($rootScope, s3){
    'ngInject'
    this.$rootScope = $rootScope;
    this.s3 = s3;
    this.mandatoryPkgs = new Set();
  }

  setMandatoryPkgs(pkgs) {
    this.mandatoryPkgs = new Set(pkgs);
    console.log('mandatory Packages: ', this.mandatoryPkgs)
  }

  isMandatoryPkgs(pkgname) {
    return this.mandatoryPkgs.has(pkgname)
  }

  // TODO: get Customer specified index in customer folders
  /// #if SNOW_MODE=='cloud'
  getPackageIndex(index) {
    return this.s3.getPackage(`${index}/Packages.gz`)
    .then(result => new encoding.TextDecoder("utf-8").decode(result.Body))
  }
  /// #endif
  /// #if SNOW_MODE=='usb'
  getPackageIndex(index){
    const indexFilePath = `/opt/pkg-repo/${index}/${indexFile}`
    let tmpIndexFilePath = `/tmp/pkg-repo/${index}`.replace(/\//g, '_')
    let content = null;
    return new Promise((resolve, reject) => {
      if (fs.existsSync(indexFilePath)) {
        fs.createReadStream(indexFilePath)
          .pipe(zlib.createGunzip((err) => { console.log(err) }))
          .pipe(fs.createWriteStream(tmpIndexFilePath))
          .on('close',() => {
            content = fs.readFileSync(tmpIndexFilePath, { encoding: 'utf8' })
            return resolve(content)
          })
          .on('error', (error) => {
            console.log(error)
          })
      } else{
        return reject(new Error('Package is not existed'))
      }
    })
  }
  /// #endif

  // Parse package's property from Packages file
  parsePackageIndex(packages) {
    let appList = [];
    let appIndex = 0;
    let lineBegin = 0;
    let lineEnd = 0;
    let colon = 0;
    let name = '';
    let value = '';

    colon = packages.indexOf(':', lineBegin);
    if (colon !== -1) {
      appList[appIndex] = {};
    }
    while(colon !== -1) {
      lineEnd = packages.indexOf('\n', lineBegin);
      if (packages.charAt(lineBegin) === ' ' || packages.charAt(lineBegin) === '\t') {
        // previous item continues
        appList[appIndex][name] = `${appList[appIndex][name]}\n${packages.slice(lineBegin, lineEnd).trim()}`
      } else {
        // new item
        name = packages.slice(lineBegin, colon).trim();
        value = packages.slice(colon + 1, lineEnd).trim();

        appList[appIndex][name] = value;
      }

      lineBegin = lineEnd + 1;

      // Empty line means new app start
      if (packages.charAt(lineBegin) === '\n'
        && packages.indexOf(':', lineBegin + 1) !== -1) {
        lineBegin++;
        appIndex++;
        appList[appIndex] = {};
      }
      colon = packages.indexOf(':', lineBegin);
    }
    return appList;
  }
}

export default Packages;
