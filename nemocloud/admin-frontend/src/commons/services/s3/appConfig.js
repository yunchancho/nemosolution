export default {
  contentsBucket: 'nemosnow-contents',
  packagesBucket: 'nemosnow-packages',
  backupsBucket: 'nemosnow-backups',
  region: 'ap-northeast-2',
  directory: {
    backup: {
      screenshots: 'screenshots',
      configs: 'configs',
      logs: 'logs'
    }
  }
}