import angular from 'angular';
import S3 from './service';

const module = angular.module('app.commons.services.s3', [
  'app.commons.services.lambda',
  /// #if SNOW_MODE=='cloud'
  'app.commons.services.cognito',
  /// #endif
  /// #if SNOW_MODE=='local'
  'app.commons.services.localApis',
  /// #endif
  /// #if SNOW_MODE=='usb'
  'app.commons.services.localApis',
  /// #endif
])
.service('s3', S3)
.name;

export default module;
