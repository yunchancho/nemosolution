import AWS from 'aws-sdk';
import Promise from 'bluebird'
import encoding from 'text-encoding';
import appConfig from './appConfig'
import FileTransfer from '../../../lib/fileTransfer/index.js'


class S3 {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, cognito, lambda){
    'ngInject'
    this.cognito = cognito;
    this.lambda = lambda
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, localApis, lambda){
    'ngInject'
    this.localApis = localApis;
    this.lambda = lambda
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, localApis, lambda){
    'ngInject'
    this.localApis = localApis;
    this.lambda = lambda
  /// #endif
    this.$rootScope = $rootScope;
    this.s3;

    this.init();
    this.registerInitFunc();
  }

  copyContents(from, to) {
    let params = {
      Bucket: appConfig.contentsBucket,
      CopySource: `${appConfig.contentsBucket}/${from}`,
      Key: to
    }

    return this.s3.copyObjectAsync(params);
  }

  deleteObject(bucket, key) {
    if (key.charAt(key.length - 1) === '/') {
      // Remove all files in folder
      return this.deleteFolder(bucket, key)
    } else {
      return this.deleteFile(bucket, key)
    }
  }

  // Delete folder and inner objects
  deleteFolder(bucket, key) {
    return this.listObjects(bucket, key)
    .then(fileList => {
      let requests = [];
      for (let i = 0; i < fileList.length; i = i + 1000) {
        requests.push(fileList.slice(i, i + 1000));
      }
      requests.map(item => {
        return this.deleteFiles(bucket, item);
      })

      return Promise.all(requests);
    })
  }

  deleteFile(bucket, key) {
    let params = {
      Bucket: bucket,
      Key: key
    }

    return this.s3.deleteObjectAsync(params)
  }

  // TODO: Errors are in result[0].Errors
  deleteFiles(bucket, files) {
    let params = {
      Bucket: bucket,
      Delete: {
        Objects: files.map((file) => {
          return {Key: file.Key};
        })
      }
    }

    return this.s3.deleteObjectsAsync(params);
  }

  downloadFile(bucket, object) {
    // Download file using signed url
    // Url expires after one day
    let params = {
      Bucket: bucket,
      Key: object.Key,
      Expires: 900
    }

    return this.s3.getSignedUrlAsync('getObject', params)
    .then(url => {
      FileTransfer.downloadURL(url)
      return url
    })

    // // Download file using getObject
    // let params = {
    //   Bucket: bucket,
    //   Key: object.Key,
    //// Range: `bytes=0-100`
    // }

    // return this.s3.getObjectAsync(params)
    // .then(result => {
    //   console.log('s3 get Object', result)
    //   let filename = object.Key.slice(object.Key.lastIndexOf('/') + 1);
    //   FileTransfer.downloadBlob(result.Body, filename, result.ContentType);
    //   return result;
    // })
  }

  getConfig() {
    return appConfig
  }

  getObject(bucket, key) {
    let params = {
      Bucket: bucket,
      Key: key,
    }

    return this.s3.getObjectAsync(params)
  }

  getPackage(key) {
    let params = {
      Bucket: appConfig.packagesBucket,
      Key: key,
      ResponseCacheControl: 'no-cache'
    }

    return this.s3.getObjectAsync(params);
  }

  init() {
    this.s3 = new AWS.S3({
      region: appConfig.region
    });
    this.s3 = Promise.promisifyAll(this.s3);
  }

  /// #if SNOW_MODE=='cloud'
  registerInitFunc() {
    this.cognito.addOnLoginFunction('s3', () => {
      this.init();
    })
    this.cognito.addOnLogoutFunction('s3', () => {
      this.init();
    })
  }
  /// #endif

  /// #if SNOW_MODE=='local'
  registerInitFunc() {
    this.localApis.addOnLoginFunction('s3', () => {
      this.init();
    })
    this.localApis.addOnLogoutFunction('s3', () => {
      this.init();
    })
  }
  /// #endif
  /// #if SNOW_MODE=='usb'
  registerInitFunc() {
    this.localApis.addOnLoginFunction('s3', () => {
      this.init();
    })
    this.localApis.addOnLogoutFunction('s3', () => {
      this.init();
    })
  }
  /// #endif

  // List all objects in contents Bucket
  // Maximum listing Count is 1000. call listing api recusively
  listObjects(bucket, prefix) {
    // Function declaration
    let recursiveCall = (list, ContinuationToken, prefix) => {
      let params = {
        Bucket: bucket,
        ContinuationToken,
        Prefix: prefix
      }

      return this.s3.listObjectsV2Async(params)
      .then(result => {
        list = list.concat(result.Contents);

        if (result.KeyCount === 1000) {
          return recursiveCall(list, result.NextContinuationToken);
        } else {
          return list;
        }
      })
    }


    // Function call
    return recursiveCall([], null, prefix);
  }

  listBackupIndex() {
    let context = {
      api: 'listDeviceBackup',
      accessToken: this.cognito.getAccessToken().jwtToken
    }
    return this.lambda.invoke('NemoSnowListUserData', context)
  }

  listDirectoryObjects(bucket, prefix) {
    let params = {
      Bucket: bucket,
      Prefix: prefix,
      Delimiter: '/'
    }
    return this.s3.listObjectsV2Async(params)
    .then(data => {
      // Remove itself
      for (let i = 0; i < data.Contents.length; i++) {
        if (data.Contents[i].Key === prefix) {
          data.Contents.splice(i, 1);
        }
      }
      return data;
    })
  }

  // list 2-depth folders has prefix
  // ex) listIndex('packages', 'nemoux/') -> return folders in nemoux folder (in packages bucket)
  listIndex(bucket, prefix) {
    return this.listDirectoryObjects(bucket, prefix)
    .then(data => data.CommonPrefixes)
  }

  listPackages() {
    let params = {
      Bucket: appConfig.packagesBucket,
    };

    return this.s3.listObjectsV2Async(params);
  }

  // TODO: use multipart upload if file size > 100MB
  uploadFile(bucket, fileName, fileStream) {
    let params = {
      Bucket: bucket,
      Key: fileName,
      Body: fileStream
    }
    let options = {
      partSize: 10 * 1024 * 1024,
      queueSize: 1
    }

    return this.s3.uploadAsync(params, options)
  }

  uploadFileByUrl(bucket, fileName, fileStream) {
    let params = {
      Body: fileStream,
      Bucket: bucket,
      Key: fileName
    }

    let request = this.s3.putObject(params);
    request.on('httpUploadProgress', function (progress) {
      console.log(progress.loaded + " of " + progress.total + " bytes");
    });
    request.send();

    // return this.s3.createPresignedPostAsync(params)
    // .then(result => {
    //   console.log('url: ', result)
    //
    //   return new Promise((resolve, reject) => {
    //     let form = new FormData();
    //     for (let key of Object.keys(result.fields)) {
    //       form.append(key, result.fields[key])
    //     }
    //     form.append('file', new Blob([ fileStream ]));
    //
    //     let xhr = new XMLHttpRequest();
    //     // TODO: not worked in contents bucket
    //     xhr.open("POST", result.url, true);
    //     xhr.send(form);
    //     xhr.onload = () => {
    //       if (xhr.status.toString().charAt(0) === '2') {
    //         resolve(xhr.responseText);
    //       } else {
    //         reject(new Error(xhr.statusText))
    //       }
    //     };
    //     xhr.onerror = () => {
    //       reject(new Error(xhr.statusText))
    //     };
    //   })
    // })
  }

  uploadFiles(bucket, directory, files) {
    let results = [];
    return files.reduce((previousValue, currentValue) => {
      return previousValue.then(() => {
        let filename = directory + currentValue.file.name;
        console.log('Upload start:', filename);
        return this.uploadFile(bucket, filename, currentValue.fileStream)
      })
      .then(result => results.push(result))
    }, Promise.resolve())
  }
}

export default S3;
