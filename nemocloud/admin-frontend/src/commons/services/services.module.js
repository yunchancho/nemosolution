import angular from 'angular';

import authModal from './authModal/module';
import deviceManager from './deviceManager/module';
import dynamoDB from './dynamoDB/module';
import iot from './iot/module';
import packages from './packages/module';
import s3 from './s3/module';
import spinner from './spinner/module';
import modal from './modal/module';
import lambda from './lambda/module';


let subModules = [
	authModal,
	deviceManager,
	dynamoDB,
	iot,
	packages,
	s3,
	spinner,
	modal,
	lambda
]

/// #if SNOW_MODE=="cloud"
import cognito from './cognito/module';
import logs from './logs/module';
subModules = subModules.concat([
	cognito,
	logs
	])
/// #endif

/// #if SNOW_MODE=="local"
import localApis from './localApis/module';
import network from './network/module';
subModules = subModules.concat([
	localApis,
	network
	])
/// #endif

/// #if SNOW_MODE=="usb"
import localApis from './localApis/module';
import network from './network/module';
subModules = subModules.concat([
	localApis,
	network
	])
/// #endif

const module = angular.module('app.commons.services', subModules)
.name

export default module;
