import angular from 'angular';
import Spinner from './service';

const module = angular.module('app.commons.services.spinner', [
  'app.commons.services.modal'
])
.service('spinner', Spinner)
.name;

export default module;
