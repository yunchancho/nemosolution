import angular from 'angular';

import login from './login/module';

let subModules = [
	login
]

/// #if SNOW_MODE=="cloud"
import confirmEmail from './confirmEmail/module';
import forgotPassword from './forgotPassword/module';
import newPasswordRequired from './newPasswordRequired/module';
import register from './register/module';
import resetPassword from './resetPassword/module';

subModules = subModules.concat([
	confirmEmail,
	forgotPassword,
	newPasswordRequired,
	register,
	resetPassword
	])
/// #endif

/// #if SNOW_MODE=="local"
import deviceauth from './deviceauth/module';

subModules = subModules.concat([
	deviceauth
	])
/// #endif

const module = angular.module('app.commons.views.auth', subModules)
.name

export default module
