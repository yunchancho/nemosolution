class Controller {
  constructor($rootScope, $state, network, modal, spinner, localApis){
    'ngInject'
    this.name = 'requestConfirm';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.modal = modal;
    this.network = network;
    this.spinner = spinner;
    this.localApis = localApis;
    this.swKey = '';
    this.error = '';
    this.pending = false;
  }
  $onInit(){
    this.getNetworkStatus();
  }
  request() {
    this.error = '';
    this.pending = true;
    this.localApis.requestConfirm(this.swKey)
    .then(result => {
      console.log('Request Success:', result);
      let params = {
        type: 'reboot',
        time: 5,
        shutdown: false
      }

      this.modal.shutdown(params)
      this.close();
    })
    .catch((err) => {
      console.error('Request Confirm fails:', err);

      // err.code:
      // - ResourceNotFoundException
      // - ThrottlingException
      switch(err.code) {
        case 'ResourceNotFoundException':
        case 'DEVICE_ERROR_SET_STATUS_NOT_SW_KEY':
          this.error = '소프트웨어 키가 존재하지 않습니다.'
          break;
        case 'ERROR_LAMBDA_CONTROL_DEVICE_VERIFY_INVALID_HW_KEY':
          this.error = '장비와 소프트웨어 키가 일치하지 않습니다.'
          break;
        case 'ERROR_LAMBDA_CONTROL_DEVICE_VERIFY_ALREADY_DONE':
          this.error = '이미 인증이 완료된 장비입니다.'
          break;
        default:
          this.error = `에러: ${err.code}`
      }
    })
    .then(() => {
      this.pending = false;
      // this.$rootScope.$$phase || this.$rootScope.$apply();
    })
  }

  getNetworkStatus() {
    this.network.getStatus()
    .then(result => {
      // console.log('get network status:', result)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.log('get network status:', err)
      // Sign out and open login modal if token is invalid
      if (err.code === 'AUTH_ERROR_ACCESS_TOKEN_INVALID') {
        if (this.localApis.isLogin()) {
          this.localApis.signOut()
          this.$state.reload()
        }
      }
    })
  }
}

export default Controller;
