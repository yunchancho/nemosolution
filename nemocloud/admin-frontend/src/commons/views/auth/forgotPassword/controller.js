class Controller {
  constructor($rootScope, $state, authModal, cognito){
    'ngInject'
    this.name = 'forgotPassword';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.authModal = authModal;
    this.cognito = cognito;
    this.username = '';
    this.pending = false;
  }
  
  $onInit() {
    this.redirectHome();
  }
  
  loginModal() {
    this.authModal.open('login');
  }
  
  // Redirect to index page when user already logged in
  redirectHome() {
    if (this.cognito.isLogin()) {
      this.close();
    }
  }
  
  sendFindPasswordCode() {
    this.pending = true;
    this.cognito.sendFindPasswordCode(this.username)
    .then(result => {
      console.log(result);
      this.authModal.open('resetPassword');
      return result
    })
    .catch(err => {
      console.error(err)
      return err
    })
    .then(result => {
      this.pending = false;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
  }
}

export default Controller;
