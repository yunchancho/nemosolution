/// #if SNOW_MODE=="cloud"
class Controller {
  constructor($rootScope, $state, authModal, cognito, spinner){
    'ngInject'
    this.name = 'login'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.authModal = authModal;
    this.cognito = cognito;
    this.spinner = spinner;
    this.password = '';
    this.pendingLogin = false;
    this.username = '';
    this.errorMessage = '';
  }

  $onInit() {
    this.redirectHome();
  }

  forgotPassword() {
    this.authModal.open('forgotPassword');
  }

  // Redirect to index page when user already logged in
  redirectHome() {
    if (this.cognito.isLogin()) {
      this.close();
    }
  }

  login(){
    if (this.username === '' || this.password === '') {
      this.errorMessage = '아이디와 비밀번호를 입력하세요.'
      return Promise.resolve()
    }

    this.pendingLogin = true;
    this.spinner.on()
    return this.cognito.login(this.username, this.password)
    .then(result => {
      console.log('Login: ', result);
      this.$state.reload();
      this.close();
      return null;
    })
    .catch(err => {
      console.error('login:', err.message);
      switch(err.message) {
        case 'User does not exist.':
          this.errorMessage = '존재하지 않는 유저입니다.'
          break;
        case 'Incorrect username or password.':
          this.errorMessage = '아이디와 비밀번호가 일치하지 않습니다.'
          break;
        case 'Network Failure':
          this.errorMessage = '서버와 연결할 수 없습니다.'
          break;
        case 'Password attempts exceeded':
          this.errorMessage = '로그인 시도 횟수가 초과되었습니다. 잠시 후 다시 시도해주세요.';
          break;
        case 'User is not confirmed.':
          this.authModal.open('confirmEmail');
          break;
        case 'New password required':
          // User created by admin and it's first login
          let resolves = new Map();
          let [userAttributes, requiredAttributes] = err.attributes;
          resolves.set('userAttributes', userAttributes);
          resolves.set('requiredAttributes', requiredAttributes);
          this.authModal.open('newPasswordRequired', resolves);
          break;
        case 'Password reset required for the user':
          break;
        default:
          if (err.message.startsWith("The ambiguous role mapping rules for:")) {
            this.errorMessage = '고객사 정보를 확인할 수 없습니다. 관리자에게 문의해주세요.'
          }
      }
      return null;
    })
    .then(() => {
      this.pendingLogin = false;
      this.spinner.off()
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return;
    });
  }

  registerModal() {
    this.authModal.open('register');
  }
}
/// #endif

/// #if SNOW_MODE=="local"
class Controller {
  constructor($rootScope, $state, authModal, spinner, localApis){
    'ngInject'
    this.name = 'login'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.authModal = authModal;
    this.spinner = spinner;
    this.localApis = localApis;
    this.password = '';
    this.pendingLogin = false;
    this.username = '';
    this.errorMessage = '';
  }

  $onInit() {
    this.redirectHome();
  }

  forgotPassword() {
    this.authModal.open('forgotPassword');
  }

  // Redirect to index page when user already logged in
  redirectHome() {
    if (this.localApis.isLogin()) {
      this.close();
    }
  }

  login(){
    this.spinner.on()
    this.pendingLogin = true;
    return this.localApis.login(this.username, this.password)
    .then(result => {
      console.log('Login Success', result);
      this.$state.reload();
      this.close();
      return null
    })
    .catch(err => {
      console.error(`Login: ${err}`)
      switch(err.message) {
        case 'Failed to fetch':
          this.errorMessage = '서버와 연결할 수 없습니다.'
          break;
        case 'AUTH_ERROR_LOGIN_WRONG_ID':
        case 'AUTH_ERROR_LOGIN_WRONG_PASSWORD':
          this.errorMessage = '아이디 또는 비밀번호가 올바르지 않습니다.'
          break;
      }
      return null;
    })
    .then(() => {
      this.pendingLogin = false;
      this.spinner.off()
      this.$rootScope.$$phase || this.$rootScope.$apply();
    });
  }

  registerModal() {
    this.authModal.open('register');
  }
}
/// #endif


/// #if SNOW_MODE=="usb"
class Controller {
  constructor($rootScope, $state, authModal, spinner, localApis){
    'ngInject'
    this.name = 'login'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.authModal = authModal;
    this.spinner = spinner;
    this.localApis = localApis;
    this.password = '1234567890';
    this.pendingLogin = false;
    this.username = 'admin';
    this.errorMessage = '';
  }

  $onInit() {
  }


  // Redirect to index page when user already logged in
  redirectHome() {
    if (this.localApis.isLogin()) {
      this.close();
    }
  }

  login(){
    this.spinner.on()
    this.pendingLogin = true;
    return this.localApis.login(this.username, this.password)
    .then(result => {
      console.log('Login Success', result);
      this.$state.reload();
      this.close();
      return null
    })
    .catch(err => {
      console.error(`Login: ${err}`)
      switch(err.message) {
        case 'Failed to fetch':
          this.errorMessage = '서버와 연결할 수 없습니다.'
          break;
        case 'AUTH_ERROR_LOGIN_WRONG_ID':
        case 'AUTH_ERROR_LOGIN_WRONG_PASSWORD':
          this.errorMessage = '아이디 또는 비밀번호가 올바르지 않습니다.'
          break;
      }
      return null;
    })
    .then(() => {
      this.pendingLogin = false;
      this.spinner.off()
      this.$rootScope.$$phase || this.$rootScope.$apply();
    });
  }
}
/// #endif

export default Controller;
