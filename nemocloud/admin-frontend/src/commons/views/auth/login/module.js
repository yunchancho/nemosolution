import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'login';

let module = angular.module(name, [
	uiRouter,
	'app.commons.services.authModal',
	'app.commons.services.spinner',
	/// #if SNOW_MODE=='cloud'
  'app.commons.services.cognito',
  /// #endif
  /// #if SNOW_MODE=='local'
  'app.commons.services.localApis',
  /// #endif
	/// #if SNOW_MODE=='usb'
  'app.commons.services.localApis',
  /// #endif
	])
	.component(name, component)
	.name

export default module;
