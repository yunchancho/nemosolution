class Controller {
  constructor($state, authModal, cognito){
    'ngInject'
    this.name = 'newPasswordRequired';
    this.$state = $state;
    this.authModal = authModal;
    this.cognito = cognito;
    this.email = '';
    this.name = '';
    this.newPassword = '';
    
  }
  
  $onInit() {
    this.redirectHome();
  }
  
  login() {
    console.log(this.resolve.map)
    let attributeNames = this.cognito.getConfig().attributeNames;
    let attributesData = this.resolve.map.get('userAttributes');
    // attributesData.email = this.email;
    // attributesData.name = this.name;
    attributesData[attributeNames.type] = null;
    
    
    this.cognito.newPasswordChallenge(this.newPassword, attributesData)
    .then(result => {
      console.log('newPasswordChallange:', result);
      this.authModal.open('login');
      return null
    })
    .catch(err => console.error('newPasswordChallange:', err))
  }
  
  // Redirect to index page when user already logged in
  redirectHome() {
    if (this.cognito.isLogin()) {
      this.close();
    }
  }  
  
  resetPassword() {
    this.cognito.resetPassword(this.code, this.newPassword)
    .then(result => {
      console.log(result);
      this.authModal.open('login');
    })
    .then(err => console.error('reset password:', err))
  }
}

export default Controller;
