class Controller {
  constructor($state, authModal, cognito){
    'ngInject'
    this.name = 'register';
    this.$state = $state;
    this.authModal = authModal;
    this.cognito = cognito;
    this.email = '';
    this.fullname = '';
    this.password = '';
    this.username = '';
    this.policyCheck = false;
  }
  
  $onInit() {
    this.redirectHome();
  }
  
  loginModal() {
    this.authModal.open('login');
  }
  
  // Redirect to index page when user already logged in
  redirectHome() {
    if (this.cognito.isLogin()) {
      this.close();
    }
  }
  
  register() {
    if (!this.policyCheck) {
      console.log('Check policy');
      return;
    }
    
    let userInfoMap = new Map();
    userInfoMap.set('username', this.username);
    userInfoMap.set('password', this.password);
    userInfoMap.set('name', this.fullname);
    userInfoMap.set('email', this.email);
    
    this.cognito.registerUser(userInfoMap)
    .then(result => {
      console.log('register result: ', result);
      this.authModal.open('confirmEmail');
      return result;
    })
    .catch(console.error)
  }
}

export default Controller;
