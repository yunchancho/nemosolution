class Controller {
  constructor($rootScope, $state, authModal, cognito){
    'ngInject'
    this.name = 'resetPassword';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.authModal = authModal;
    this.cognito = cognito;
    this.code = '';
    this.newPassword = '';
    this.pending = false;
  }
  
  $onInit() {
    this.redirectHome();
  }
  
  
  // Redirect to index page when user already logged in
  redirectHome() {
    if (this.cognito.isLogin()) {
      this.close();
    }
  }  
  
  resetPassword() {
    this.pending = true;
    this.cognito.resetPassword(this.code, this.newPassword)
    .then(result => {
      this.authModal.open('login')
      return result;
    })
    .catch(err => console.error('resetPassword: ', err))
    .then(result => {
      this.pending = false;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
  }
}

export default Controller;
