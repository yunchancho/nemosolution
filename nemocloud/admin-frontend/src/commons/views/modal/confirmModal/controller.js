class Controller {
  constructor($state){
    'ngInject'
    this.$state = $state;
    this.title = '';
    this.content = '';
    this.warning = '';
    this.confirmButton = '';
    this.cancelButton = '';
  }
  
  $onInit() {
    this.onConfirm = () => {
      Promise.resolve(this.resolve.onConfirm())
      .then(() => this.close())
    }
    this.title = this.resolve.title;
    this.content = this.resolve.content;
    this.warning = this.resolve.warning;
    this.confirmButton = this.resolve.confirmButton;
    this.cancelButton = this.resolve.cancelButton;
  }
  
  close() {
    this.close();
  }
}

export default Controller;
