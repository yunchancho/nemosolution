class Controller {
  constructor($rootScope, $state, localApis, modal, spinner){
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state;
    this.localApis = localApis;
    this.modal = modal
    this.spinner = spinner;
    this.time = 5;
    this.type = 'shutdown';
    this.shutdown = true;
    this.counter = null;
  }

  $onInit() {
    this.time = this.resolve.time || 5;
    this.type = this.resolve.type;
    this.shutdown = this.resolve.shutdown || true
    
    this.counter = setInterval(() => {
      this.time = this.time - 1
      this.$rootScope.$$phase || this.$rootScope.$apply();
      if (this.time > 0) {
        return;
      }
      clearInterval(this.counter)
      
      if (!this.shutdown) {
        return;
      }
      
      let prom;
      this.spinner.on()
      if (this.type === 'shutdown') {
        prom = this.localApis.powerOff()
      } else {
        prom = this.localApis.reboot()
      }
      prom.then(() => {
        this.localApis.signOut()
        this.$rootScope.$$phase || this.$rootScope.$apply();
      })
      .catch(err => {
        this.close()
        let params = {
          title: '종료 실패',
          error: err.message
        }
        this.modal.error(params)
      })
      .then(() => this.spinner.off())
    }, 1000)
  }
  
  $onDestroy() {
    clearInterval(this.counter)
  }
}

export default Controller;
