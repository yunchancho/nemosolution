class NavbarController {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, cognito) {
    'ngInject'
    this.cognito = cognito;
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, localApis) {
    'ngInject'
    this.localApis = localApis;
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, localApis) {
    'ngInject'
    this.localApis = localApis;
  /// #endif
    this.name = 'navbar';
    this.$rootScope = $rootScope;
    this.type;
  }

  $onInit() {
    /// #if SNOW_MODE=='cloud'
    this.type = this.cognito.getType();
    // this.$rootScope.$$phase || this.$rootScope.$apply();
    /// #endif
  }
}

export default NavbarController;
