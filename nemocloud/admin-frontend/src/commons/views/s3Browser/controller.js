class Controller {
  constructor($rootScope, $uibModal, cognito, modal, s3, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.cognito = cognito;
    this.modal = modal;
    this.s3 = s3;
    this.spinner = spinner;
    this.indexList = [];
    this.directory = '';
    this.pickedFiles = [];
    // functions in s3FileList
    this.resolve = {
      refreshList: null,
      deleteFile: null,
      deleteFiles: null,
      downloadFiles: null,
    }
    this.selectedIndex;
    this.options = {
      bucket: '',
      indexPrefix: '',
      func: {
        refreshIndex: null,
        listIndex: null
      },
    }
    this.preogress;
  }

  $onInit() {
    this.options.func.refreshIndex = () => {
      return this.getIndexList()
    }
    this.spinner.on()
    this.getIndexList()
    .then(() => this.spinner.off())
  }

  createFolder() {
    let modal = this.$uibModal.open({
      component: 's3CreateFolder',
      resolve: {
        bucket: () => { return this.options.bucket },
        directory: () => { return this.directory },
        onComplete: () => { return () => this.resolve.refreshList() },
      },
      size: 'sm'
    });
  }

  deleteIndex(index) {
    console.log(index)
    let params = {
      title: '인덱스 삭제',
      content: '삭제하시겠습니까?\n',
      warning: '내부의 모든 파일이 삭제됩니다.',
      onConfirm: () => {
        this.resolve.deleteFile(index.Prefix)
        .then(() => this.getIndexList())
      }
    }
    this.modal.confirm(params)
  }

  getIndexList() {
    let prom;

    if (this.options.func.listIndex) {
      prom = this.options.func.listIndex(this.options.bucket, this.options.indexPrefix)
    } else {
      prom = this.s3.listIndex(this.options.bucket, this.options.indexPrefix)
    }

    return prom.then((result) => {
      this.indexList = result;
      try {
        this.selectedIndex = this.indexList[0].Prefix;
        this.$rootScope.$$phase || this.$rootScope.$apply();
      } catch (e) {
        console.error('no indexes', e)
      }
      console.log('index List', result);
      return this.indexList;
    })
    .catch((err) => console.error('getIndexList:', err))
  }

  select(index) {
    this.selectedIndex = index.Prefix;
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
  selectIndex(){
    console.log("????", this.selectIndex);
  }
  uploadFile() {
    if(this.pickedFiles === undefined){
      let params = {
        title: '업로드 실패',
        content: '업로드할 파일이 비어있습니다.',
      }
      this.modal.error(params)
      return;
    }
    this.spinner.on()
    return this.s3.uploadFiles(this.options.bucket, this.directory, this.pickedFiles)
    .then((result) => {
      this.resolve.refreshList();
      console.log('Upload finish', result);
      return result
      this.progress={
        loaded:0,
        total:0
      }
    })
    .catch((err) => {
      console.error('File upload error', err)
      let params = {
        title: '파일 업로드 실패',
        content: '파일 업로드 중 오류가 발생했습니다.',
        error: err.message
      }

      switch(err.message) {
        case 'Unsupported body payload object':
          params.content = '파일이 선택되지 않았거나 지원하지 않는 형식입니다.'
          params.error = '';
          break;
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
