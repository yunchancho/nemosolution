class Controller {
  constructor(s3, spinner){
    'ngInject'
    this.s3 = s3;
    this.spinner = spinner
    this.folderName = '';
    this.resolve;
    this.directory;
    this.bucket;
    this.error = ''
  }
  
  $onInit() {
    this.directory = this.resolve.directory;
    this.bucket = this.resolve.bucket;
  }
  
  create() {
    if (this.folderName.length === 0
      || this.folderName.indexOf('/') !== -1) {
        this.error = '잘못된 폴더명입니다.'
      return
    }
    
    let filename = this.directory + this.folderName + '/';
    let folder = new File([""], "folder");
    this.spinner.on()
    this.s3.uploadFile(this.bucket, filename, folder)
    .then((result) => {
      console.log(`Create Folder ${this.folderName}`);
      this.resolve.onComplete();
      this.close();
      return null
    })
    .catch((err) => console.error('Create Folder:', err))
    .then(() => this.spinner.off());
  }
  
  cancel() {
    this.close();
  }
  
}

export default Controller;
