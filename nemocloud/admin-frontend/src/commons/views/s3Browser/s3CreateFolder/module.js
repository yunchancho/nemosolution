import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 's3CreateFolder';

let module = angular.module(name, [ 
	uiRouter,
	'app.commons.services.s3'
	])
	.component(name, component)
	.name

export default module; 