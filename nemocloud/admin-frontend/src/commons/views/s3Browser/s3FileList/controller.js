import Promise from 'bluebird'
import appConfig from './appConfig';

class Controller {
  constructor($rootScope, $state, $uibModal, modal, s3, spinner, toastr){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.modal = modal;
    this.s3 = s3;
    this.spinner = spinner;
    this.toastr = toastr
    this.options;
    this.objects = [];
    this.resolve;
    this.index;
    this.directory;
  }

  $onInit() {
    this.resolve.refreshList = () => this.listObjects();
    this.resolve.deleteFile = (key) => this.deleteFile(key);
    this.resolve.deleteFiles = () => this.deleteFiles();
    this.resolve.downloadFiles = () => this.downloadFiles();
  }

  $onChanges(obj) {
    if (obj.index
      && obj.index.currentValue) {
      this.listObjects(this.index)
      .then(directory => {
        this.directory = directory
        this.$rootScope.$$phase || this.$rootScope.$apply()
      })
      .catch(console.error);
    }
  }

  _arrayBufferToBase64(buffer) {
    let binary = '';
    let bytes = new Uint8Array( buffer );
    for (let i = 0; i < bytes.byteLength; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  // Move to parent directory
  back() {
    let backIndex = this.directory.lastIndexOf('/', this.directory.length - 2);
    if (backIndex !== this.directory.indexOf('/')) {
      this.listObjects(this.directory.slice(0, backIndex + 1))
      .then(dir => {
        this.directory = dir;
        this.$rootScope.$$phase || this.$rootScope.$apply();
      });
    }
  }

  deleteFile(key) {
    this.spinner.on()

    return this.s3.deleteObject(this.options.bucket, key)
    .then((result) => {
      return this.listObjects();
    })
    .catch((err) => {
      console.error('delete Object: ', err);
    })
    .then(() => this.spinner.off())
  }

  deleteFiles() {
    this.spinner.on()
    let folders =[];
    let files = [];
    for (let folder of this.objects.CommonPrefixes) {
      if (folder.check) {
        folders.push(folder);
      }
    }

    for (let file of this.objects.Contents) {
      if (file.check) {
        files.push(file);
      }
    }

    let request1 = files.map(file => {
      return this.s3.deleteObject(this.options.bucket, file.Key)
    })
    let request2 = folders.map(folder =>{
      return this.s3.deleteFolder(this.options.bucket, folder.Prefix)
    })
    return Promise.all([...request1, ...request2])
    .then(() => {
      this.toastr.success('파일 삭제가 완료되었습니다..')
      return this.listObjects();
    })
    .catch(err => {
      console.error('download files', err);
      let params = {
        title: '파일 삭제 실패',
        content: '파일 삭제 중 오류가 발생했습니다.',
        error: err.message
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  downloadFile(file) {
    this.spinner.on()
    this.s3.downloadFile(this.options.bucket, file)
    .then((result) => {
      console.log('Download: ', result);
    })
    .catch((err) => console.error('downloadFile', err))
    .then(() => this.spinner.off());
  }

  downloadFiles() {
    this.spinner.on()

    let files = []
    for (let file of this.objects.Contents) {
      if (file.check) {
        files.push(file);
      }
    }

    let requests = files.map(file => {
      return this.s3.downloadFile(this.options.bucket, file)
    })

    return Promise.all(requests)
    .then(result => {
      this.toastr.success('파일 다운로드를 시작합니다..')
      console.log('download files: ', result)
    })
    .catch(err => {
      console.error('download files', err);
      let params = {
        title: '파일 다운로드 실패',
        content: '파일 다운로드 중 오류가 발생했습니다.',
        error: err.message
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  enterFolder(directory) {
    this.spinner.on()
    return this.listObjects(directory)
    .then((dir) => {
      this.directory = dir;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('listObjects error', err)
      let params = {
        title: '탐색 에러',
        content: '디렉토리 접근 중 에러가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off());
  }

  listObjects(directory) {
    if (!directory) {
      directory = this.directory;
    }
    this.spinner.on()
    return this.s3.listDirectoryObjects(this.options.bucket, directory)
    .then(result => {
      this.objects = result;
      return this.listObjectImage();
    })
    .catch((err) => console.error('listObjects', err))
    .then(() => {
      this.spinner.off()
      return directory
    })
  }

  listObjectImage(){
    if(Array.isArray(this.objects.Contents)){
      const promises = this.objects.Contents.map(item => {
        if(item.Key.indexOf('.png') > -1){
          return this.s3.getObject(appConfig.bucket.content, item.Key)
          .then(result => {
            item.icon = `data:image/PNG;base64,${this._arrayBufferToBase64(result.Body)}`;
          })
        }
        if(item.Key.indexOf('.jpg') > -1){
          return this.s3.getObject(appConfig.bucket.content, item.Key)
          .then(result => {
            item.icon = `data:image/jpeg;base64,${this._arrayBufferToBase64(result.Body)}`
          })
        }
        return item.icon;
      })

      return Promise.all(promises)
    }
  }
}

export default Controller;
