import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 's3FileList';

let module = angular.module(name, [
	uiRouter,
	'app.commons.services.s3',
	'app.commons.services.spinner',
	'app.commons.services.deviceManager',
	'app.commons.filters.s3Directory'
	
	])
	.component(name, component)
	.name

export default module;
