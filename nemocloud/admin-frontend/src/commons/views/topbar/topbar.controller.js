class TopbarController {
  /// #if SNOW_MODE=="cloud"
  constructor($rootScope, $state, deviceManager, cognito, modal, spinner){
    'ngInject'
    this.cognito = cognito;
    this.deviceManager = deviceManager;
    this.deviceList;
  /// #endif
  /// #if SNOW_MODE=="local"
  constructor($rootScope, $state, modal, spinner, localApis, network){
    'ngInject'
    this.localApis = localApis;
    this.network = network;
    this.networkRefresh;
  /// #endif
  /// #if SNOW_MODE=="usb"
  constructor($rootScope, $state, $uibModal, modal, spinner, toastr, localApis){
    'ngInject'
    this.localApis = localApis;
    this.$uibModal = $uibModal;
    this.toastr = toastr;
  /// #endif
    this.name = 'topbar';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.modal = modal;
    this.spinner = spinner;
    this.ipAddress=[];
  }

  /// #if SNOW_MODE=='local'
  $onInit() {
    this.getNetworkStatus();
    this.networkRefresh = setInterval(() => this.getNetworkStatus(), 10000)
    this.getMode()
  }

  $onDestroy() {
    clearInterval(this.networkRefresh)
  }

	// changeLanguage(langKey) {
	// 	this.$translate.use(langKey);
	// 	this.language = langKey;
	// }

  confirmReboot() {
    let params = {
      title: '장비 재시작',
      content: '장비가 재시작됩니다.\n계속 하시겠습니까?',
      onConfirm: () => this.reboot()
    }

    this.modal.confirm(params)
  }

  reboot() {
    let params = {
      type: 'reboot',
      time: 5
    }

    this.modal.shutdown(params)
  }

  confirmPowerOff() {
    let params = {
      title: '장비 종료',
      content: '장비가 종료됩니다.\n계속 하시겠습니까?',
      onConfirm: () => this.powerOff()
    }

    this.modal.confirm(params)
  }

  powerOff() {
    let params = {
      type: 'shutdown',
      time: 5
    }

    this.modal.shutdown(params)
  }

  getMode() {
    this.spinner.on()
    this.localApis.getMode()
    .then(result => {
      console.log('get mode:', result)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('get mode:', err)
    })
    .then(() => this.spinner.off())
  }

  // TODO: get real-time network status
  getNetworkStatus() {
    this.network.getStatus()
    .then(result => {
      // console.log('get network status:', result)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.log('get network status:', err)
      // Sign out and open login modal if token is invalid
      if (err.code === 'AUTH_ERROR_ACCESS_TOKEN_INVALID') {
        if (this.localApis.isLogin()) {
          this.localApis.signOut()
          this.$state.reload()
        }
      }
    })
  }
  /// #endif

  /// #if SNOW_MODE=='usb'
  $onInit() {
  }

  usbApply(){
    this.toastr.success('usb에 적용되었습니다.')
  }
  /// #endif


  logout() {
    /// #if SNOW_MODE=='cloud'
      this.cognito.signOut();
    /// #endif
    /// #if SNOW_MODE=='local'
      this.localApis.signOut();
    /// #endif
    /// #if SNOW_MODE=='usb'
      this.localApis.signOut();
    /// #endif
    if (this.$state.$current.name === 'home') {
      this.$state.reload();
    } else {
      this.$state.go('home')
    }
  }
}

export default TopbarController;
