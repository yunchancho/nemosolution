import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-toastr/dist/angular-toastr.min.css';
import 'angular-toastr/dist/angular-toastr.tpls.js';

import component from './topbar.component';

let subModules = [
  uiRouter,
  'app.commons.services.modal',
  'app.commons.services.spinner',

]

/// #if SNOW_MODE=='cloud'
subModules = subModules.concat([
  'app.commons.services.cognito',
])
/// #endif

/// #if SNOW_MODE=='local'
import topbarUsb from './topbarUsb/module'
subModules = subModules.concat([
  'app.commons.services.localApis',
  'app.commons.services.network',
  topbarUsb,
])
/// #endif

/// #if SNOW_MODE=='usb'
import topbarUsb from './topbarUsb/module'

subModules = subModules.concat([
  'app.commons.services.localApis',
  topbarUsb,
])
/// #endif

const name = 'topbar';

let module = angular.module(name, subModules)
.component(name, component)
.config(['$compileProvider', ($compileProvider) => {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(|blob|):/);
}])
.name;

export default module;
