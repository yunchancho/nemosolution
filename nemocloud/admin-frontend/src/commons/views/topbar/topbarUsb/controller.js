class Controller {
  constructor($rootScope, $state, modal, spinner, localApis){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.localApis = localApis;
    this.modal = modal;
    this.spinner = spinner;
    this.usbs = []
    this.error = false;
  }

  $onInit() {
    this.getUsbInfo()
  }

  confirmEject(usb) {
    let params = {
      title: 'USB 안전 제거',
      content: 'USB를 제거하시겠습니까?',
      onConfirm: () => this.eject(usb)
    }
    this.modal.confirm(params)
  }

  eject(usb) {
    this.spinner.on()
    return Promise.resolve()
    .then(() => {
      this.usbs = [];
    })
    .then(result => {
      console.log('eject USB:', result)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('eject USB fails:', err)
    })
    .then(() => this.spinner.off())
  }


  getUsbInfo() {
    this.error = false;
    return this.localApis.getUsbList()
    .then((usbs) => {
      this.usbs = usbs;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      this.error = true;
      console.error('Get fssize error: ', err)
    })
  }

}

export default Controller;
