import angular from 'angular';

import auth from './auth/auth.module'
import navbar from './navbar/navbar.module';
import topbar from './topbar/topbar.module';
import bottombar from './bottombar/bottombar.module';
import modal from './modal/module'

let subModules = [
  auth,
  navbar,
	topbar,
	bottombar,
  modal,
]
/// #if SNOW_MODE=='cloud'
import s3Browser from './s3Browser/module';
subModules = subModules.concat([
  s3Browser
])
/// #endif
/// #if SNOW_MODE=='local'
import denyCloudMode from './denyCloudMode/module';
subModules = subModules.concat([
  denyCloudMode
])
/// #endif
/// #if SNOW_MODE=='usb'
import denyCloudMode from './denyCloudMode/module';
subModules = subModules.concat([
  denyCloudMode
])
/// #endif

const module = angular.module('app.commons.views', subModules)
.name

export default module
