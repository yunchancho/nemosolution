import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import auxAudio from './auxAudio/module';
import auxNetwork from './auxNetwork/module';

const name = 'auxDevice';

let module = angular.module(name, [
	uiRouter,
	auxAudio,
	auxNetwork
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('auxDevice', {
			url: '/auxDevice',
			component: name
		});
}
