class Controller {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, $state, deviceManager, modal, spinner, toastr){
    'ngInject'
    this.deviceManager = deviceManager
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, $state, localApis, modal, spinner, toastr){
    'ngInject'
    this.localApis = localApis;
    /// #endif
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.modal = modal
    this.spinner = spinner
    this.toastr = toastr
    this.resolve;
    this.options;
  }

  $onInit() {
    this.resolve.setBreadcrumb('Data')
  }

  next() {
    /// #if SNOW_MODE=='local'
    if (this.localApis.mode === 'cloud'
      && this.options.target.logs) {
      let params = {
        title: '실패',
        content: '클라우드 모드에서는 로그 데이터 백업이 자동으로 진행됩니다.'
      }
      this.modal.error(params);
      return;
    }
    /// #endif
    this.$state.go("backup.2")
  }
}

export default Controller;
