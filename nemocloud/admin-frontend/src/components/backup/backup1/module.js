import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';


const name = 'backup1';

let module = angular.module(name, [
	uiRouter,
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('backup.1', {
			url: '/1',
			component: name
		});
}
export default module;
