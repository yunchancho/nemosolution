import backup1Module from './module'
import backup1Component from './component';
import backup1Controller from './controller';
import backup1Template from './template.html';

describe('backup1', () => {
  let $rootScope, $state, makeController;

  beforeEach(window.module(backup1Module));
  beforeEach(inject((_$rootScope_, _$state_) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    makeController = () => {
      return new backup1Controller($rootScope, $state);
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('deviceManager');
    });

    // it('next', () => {
    //   let controller = makeController();
    //   controller.next()
    //   console.log(JSON.stringify($state.current))
    //   expect($state.current.name).to.equal("backup.2");
    // });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(backup1Template).to.match(/백업하려는 데이터/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = backup1Component;

    it('includes the intended template',() => {
      expect(component.template).to.equal(backup1Template);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(backup1Controller);
    });
  });
});
