class Controller {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, $filter, $state, deviceManager, modal, spinner, toastr,logs){
    'ngInject'
    this.logs = logs
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, $filter, $state, deviceManager, modal, spinner, toastr, localApis, network){
    'ngInject'
    this.localApis = localApis;
    this.network = network;
  /// #endif
    this.$rootScope = $rootScope;
    this.$filter = $filter;
    this.$state = $state;
    this.deviceManager = deviceManager;
    this.modal = modal;
    this.spinner = spinner
    this.toastr = toastr;
    this.logSize = 0;
    this.resolve;
    this.options;
  }

  // TODO: Remove state transition error
  $onInit() {
    if (!this.options.target.configs
      && !this.options.target.logs) {
        this.$state.go('backup.1')
        return;
      }
    this.resolve.setBreadcrumb('Target')
  }

  // Backup to cloud
  backupCloud() {
    console.log('backup config: ', this.options)
    /// #if SNOW_MODE=='cloud'
    this.spinner.on()
    this.deviceManager.getDeviceList()
    .then(deviceList => {
      let requests = []
      if (this.options.target.configs) {
        requests = deviceList.map(device => {
          return device.backupConfig()
        })
      }
      if (this.options.target.logs) {
        // Automatically backup logs
      }

      return Promise.all(requests)
    })
    .then(result => {
      this.toastr.success('디바이스 백업이 완료되었습니다.')
    })
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '백업',
        target: '앱설정',
        ops: 'backup'
      }
      return this.logs.success(params)
    })
    /// #endif
    .catch(err => {
      let params = {
        title: '백업 실패',
        content: '백업 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
    /// #endif
    /// #if SNOW_MODE=='local'
    this.spinner.on()
    this.localApis.getSwKey()
    .then(swKey => {
      let requests = []
      let prom;
      if (this.options.target.configs) {
        prom = this.localApis.backupCloud('configs', swKey)
        requests.push(prom)
      }

      if (this.options.target.logs) {
        prom = this.localApis.backupCloud('logs', swKey)
        requests.push(prom)
      }

      return Promise.all(requests)
    })
    .then(result => {
      console.log('backup configs', result)
      this.toastr.success('백업이 완료되었습니다.')
    })
    .catch(err => {
      console.error('backup configs:', err);
      let params = {
        title: '백업 실패',
        content: '백업 도중 오류가 발생했습니다',
        error: err.message,
      }

      this.modal.error(params);
    })
    .then(() => this.spinner.off())
    /// #endif
  }

  /// #if SNOW_MODE=='cloud'
  backupLocal() {
    this.deviceManager.getDeviceList()
    .then(deviceList => deviceList[0].downloadConfigs())
    .then(result => {
      this.toastr.success('다운로드를 시작합니다...')
    })
    .catch(err => {
      let params = {
        title: '다운로드 실패',
        content: '백업 파일 다운로드 중 오류가 발생했습니다.',
        error: err.message
      }

      this.modal.error(params)
    })
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '백업',
        target: '앱설정',
        ops: 'backup'
      }
      return this.logs.success(params)
    })
    /// #endif
  }
  /// #endif

  confirmBackup() {
    /// #if SNOW_MODE=='cloud'
    let targets = []
    if (this.options.target.configs) {
      targets.push('앱 설정');
    }
    if (this.options.target.logs) {
      targets.push('로그 데이터');
    }
    let params = {
      title: '백업',
      content: `대상: ${targets}\n`
        + `위치: ${(this.options.dest === 'cloud')?'클라우드':'다운로드'}\n`
        // + `크기: ${Math.round(this.logSize / (1024 * 1024)) || '< 1'}MB\n`
        + `백업을 진행하시겠습니까?`,
      onConfirm: () => {
        if (this.options.dest === 'cloud') {
          return this.backupCloud()
        } else if (this.options.dest === 'local') {
          return this.backupLocal()
        }
      }
    }

    this.modal.confirm(params)
    /// #endif
    /// #if SNOW_MODE=='local'
    this.spinner.on()
    let prom;
    if (this.options.target.logs) {
      prom = this.getStatus()
        .then(() => this.getLogInfo())
        .catch(err => {
          console.error('get log information fails', err.message)
          if (err.message === 'no logs') {
            return;
          }

          let params = {
            title: '오류',
            content: '백업 정보를 가져올 수 없습니다.',
            error: err.message,
          }

          this.modal.error(params)
          throw new Error('Get Status Error')
        })
    } else {
      prom = Promise.resolve()
    }

    prom.then(() => {
      let targets = []
      if (this.options.target.configs) {
        targets.push('앱 설정');
      }
      if (this.options.target.logs) {
        targets.push('로그 데이터');
      }
      let params = {
        title: '백업',
        content: `대상: ${targets}\n`
          + `위치: 클라우드\n`
          + `크기: ${Math.round(this.logSize / (1024 * 1024)) || '< 1'}MB\n`
          + `백업을 진행하시겠습니까?`,
        onConfirm: () => this.backupCloud()
      }

      this.modal.confirm(params)
    })
    .catch(err => {
      if (err.message === 'Get Status Error') {
        // Already handled
        return;
      }

      let params = {
        title: '백업 실패',
        content: '백업 도중 오류가 발생했습니다.',
        error: err.message,
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
    /// #endif
  }

  /// #if SNOW_MODE=='local'
  getLogInfo() {
    return this.localApis.getBackupStatus()
    .then(info => {
      console.log('Log Info', info)
      this.logSize = info.log.totalSize
      return info;
    })
  }

  getStatus() {
    return this.network.getStatus()
    .then(result => {
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
  }
  /// #endif

}

export default Controller;
