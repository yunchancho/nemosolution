import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-toastr/dist/angular-toastr.min.css';
import 'angular-toastr/dist/angular-toastr.tpls.js';

import component from './component';


const name = 'backup2';

let module = angular.module(name, [
	uiRouter,
	'toastr'
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('backup.2', {
			url: '/2',
			component: name
		});
}

export default module;
