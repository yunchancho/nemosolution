import backup2Module from './module'
import backup2Component from './component';
import backup2Controller from './controller';
import backup2Template from './template.html';

describe('backup2', () => {
  let $rootScope, $filter, $state, makeController;

  beforeEach(window.module(backup2Module));
  beforeEach(inject((_$rootScope_, _$filter_, _$state_,) => {
    $rootScope = _$rootScope_;
    $filter = _$filter_;
    $state = _$state_;
    makeController = () => {
      return new backup2Controller($rootScope, $state);
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(backup2Template).to.match(/백업하려는 위치/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = backup2Component;

    it('includes the intended template',() => {
      expect(component.template).to.equal(backup2Template);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(backup2Controller);
    });
  });
});
