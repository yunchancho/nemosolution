class Controller {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, $filter, $state, deviceManager, modal, spinner, toastr,logs){
    'ngInject'
    this.logs = logs
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, $filter, $state, deviceManager, modal, spinner, toastr, localApis){
    'ngInject'
    this.localApis = localApis;
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, $filter, $state, deviceManager, modal, spinner, toastr, localApis){
    'ngInject'
    this.localApis = localApis;
  /// #endif
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.modal = modal;
    this.spinner = spinner;
    this.toastr = toastr;
    this.localApis = localApis;
    this.resolve;
    this.fs = null;
    this.options;
    this.logSize = 0;
    this.logSizeMB = 0;
    this.usbs = [];
    this.selected = null;
  }

  // TODO: Remove state transition error
  $onInit() {
    if (this.options.dest === '') {
      this.$state.go('backup.2')
    }
    this.resolve.setBreadcrumb('USB')
    this.spinner.on()

    let prom;
    if (this.options.target.logs) {
      prom = this.getBackupInfo()
    } else {
      prom = Promise.resolve()
    }

    prom.then(() => this.getUsbInfo())
    .catch(err => {
      let params = {
        title: '오류',
        content: '정보를 가져오던 도중 오류가 발생했습니다.',
        error: err.message,
        onClosed: () => window.history.back()
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  backup() {
    console.log('backup config: ', this.options)
    console.log('target: ', this.selected)

    // Check log and remain USB size
    if (!this.selected.view.enough) {
      console.error('USB has not enough space')
      return;
    }
    this.spinner.on()
    let requests = []
    let prom;
    if (this.options.target.configs) {
      prom = this.localApis.backup('configs', this.selected.mount)
      requests.push(prom)
    }

    if (this.options.target.logs) {
      prom = this.localApis.backup('logs', this.selected.mount)
      requests.push(prom)
    }

    return Promise.all(requests)
    .then(result => {
      console.log('backup configs', result)
      this.toastr.success('백업이 완료되었습니다.')
    })
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '백업',
        target: '앱설정',
        ops: 'backup'
      }
      return this.logs.success(params)
    })
    /// #endif
    .catch(err => {
      console.error('backup configs:', err);
      let params = {
        title: '백업 실패',
        content: '백업 도중 오류가 발생했습니다',
        error: err.message,
      }
      this.modal.error(params);
    })
    .then(() => this.spinner.off())
  }

  confirmBackup() {
    let targets = []
    if (this.options.target.configs) {
      targets.push('앱 설정');
    }
    if (this.options.target.logs) {
      targets.push('로그 데이터');
    }
    console.log(this.selected)
    let params = {
      title: '백업',
      content: `대상: ${targets}\n`
        + `위치: USB(${this.selected.model.trim()})\n`
        + `크기: ${this.logSizeMB || '< 1'}MB\n`
        + `백업을 진행하시겠습니까?`,
      onConfirm: () => this.backup()
    }

    this.modal.confirm(params)
  }

  getBackupInfo() {
    return this.localApis.getBackupStatus()
    .then(info => {
      console.log('Backup Info', info)
      this.logSize = info.log.totalSize
      this.logSizeMB = Math.round(info.log.totalSize / 1048576)
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return info;
    })
    .catch(err => {
      console.error('Get status error: ', err)
      this.logSize = 0;
      this.$rootScope.$$phase || this.$rootScope.$apply();
      throw err;
    })
  }

  getUsbInfo() {
    return this.localApis.getUsbList()
    .then(result => {
      this.usbs = [];
      for (let usb of result) {
        for (let fs of usb.fs) {
          fs.model = usb.model;
          fs.view = {
            total: Math.round(fs.size / 1048576),
            used: Math.round(fs.used / 1048576),
            remain: Math.round((fs.size - fs.used) / 1048576),
            enough: this.logSize < (fs.size - fs.used)
          }
          this.usbs.push(fs);
        }
      }
      this.selected = this.usbs[0]
      console.log('usbs: ', this.usbs)
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return;
    })
  }

}

export default Controller;
