import angular from 'angular';
import uiRouter from 'angular-ui-router';
import chartJs from 'angular-chart.js';
import 'angular-toastr/dist/angular-toastr.min.css';
import 'angular-toastr/dist/angular-toastr.tpls.js';

import component from './component';


const name = 'backup3';

let module = angular.module(name, [
	uiRouter,
	chartJs,
	'toastr'
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('backup.3', {
			url: '/3',
			component: name
		});
}

export default module;
