/// #if SNOW_MODE=='local'
import backup3Module from './module'
import backup3Component from './component';
import backup3Controller from './controller';
import backup3Template from './template.html';

describe('backup3', () => {
  let $rootScope, makeController;

  beforeEach(window.module(backup3Module));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new backup3Controller();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('localApis');
      expect(controller).to.have.property('logSize');
      expect(controller).to.have.property('logSizeMB');
      expect(controller).to.have.property('usbs');
      expect(controller).to.have.property('selected');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(backup3Template).to.match(/백업하려는 위치/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = backup3Component;

    it('includes the intended template',() => {
      expect(component.template).to.equal(backup3Template);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(backup3Controller);
    });
  });
});
/// #endif