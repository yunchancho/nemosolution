class Controller {
  constructor($rootScope, cognito, s3) {
    'ngInject'
    this.$rootScope = $rootScope;
    this.cognito = cognito;
    this.s3 = s3;
    this.options = {
      bucket: '',
      indexPrefix: '',
      func: {
        listIndex: () => this.s3.listBackupIndex()
      }
    }
  }

  $onInit() {
    this.options.bucket = this.s3.getConfig().backupsBucket
    this.cognito.getCustomer()
    .then(customer => {
      this.options.indexPrefix = `${customer}/`;
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return null;
    })
    .catch(err => {
      console.error(`can't get customer`, err)
    })
  }
}

export default Controller;
