class Controller {
  constructor($rootScope){
    'ngInject'
    this.$rootScope = $rootScope;
    this.breadcrumbs = []
    this.options = {
      target: {
        configs: false,
        logs: false
      },
      dest: '',
    }
    this.backupComp = true;
    this.contentBrowse = false;
    this.resolve = {
      setBreadcrumb: (title) => this.setBreadcrumb(title)
    }
    //TODO : work a round solution
    $("body").removeClass("mini-navbar");
    $(".collapse").removeClass("in");
    $(".nav > li").removeClass("active");
  }

  tabOpen(val) {
    if (val === 'backup'){
      this.contentBrowse = false;
      this.backupComp = true;
    } else if (val === 'content'){
      this.backupComp = false;
      this.contentBrowse = true;
    }
  }

  setBreadcrumb(title) {
    for (let i = 0; i < this.breadcrumbs.length; i++) {
      if (this.breadcrumbs[i] === title) {
        this.breadcrumbs.splice(i + 1, Number.MAX_SAFE_INTEGER);
        return;
      }
    }
    this.breadcrumbs.push(title);
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
}

export default Controller;
