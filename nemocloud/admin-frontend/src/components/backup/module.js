import angular from 'angular';
import uiRouter from 'angular-ui-router';
import chartJs from 'angular-chart.js';
import controller from './controller';

import component from './component';

import backup1 from './backup1/module'
import backup2 from './backup2/module'
let subModules = [
	uiRouter,
	chartJs,
	backup1,
	backup2,
]
/// #if SNOW_MODE=='cloud'
import backupData from './backupData/module'
subModules = subModules.concat([
	backupData,
])
/// #endif

/// #if SNOW_MODE=='local'
import backup3 from './backup3/module'
subModules = subModules.concat([
	backup3
])
/// #endif

const name = 'backup';

let module = angular.module(name, subModules)
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('backup', {
			url: '/backup',
			component: name
		});
}

export default module;
