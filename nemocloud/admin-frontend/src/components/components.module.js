import angular from 'angular';

import home from './home/module';
import content from './content/module';
import pkg from './pkg/module';
import nemoPolicy from './nemoPolicy/module';
import nemoIntro from './nemoIntro/module';
import theme from './theme/module';
import shell from './shell/module';

let subModules = [
  home,
  content,
  pkg,
	nemoPolicy,
  nemoIntro,
  theme,
  shell,
]

/// #if SNOW_MODE=="cloud"
import install from './install/module';
import monitoring from './monitoring/module';
import bigdata from './bigdata/module';
import backup from './backup/module';
import userInfo from './userInfo/module';
import restore from './restore/module'
import rpctest from './rpctest/module';
import auxDevice from './auxDevice/module';
import power from './power/module';

subModules = subModules.concat([
  install,
	monitoring,
  bigdata,
  backup,
  userInfo,
  restore,
  rpctest,
  auxDevice,
  power
	])
/// #endif

/// #if SNOW_MODE=="local"
import setting from './setting/module';
import info from './info/module';
import mode from './mode/module';
import update from './update/module';
import backup from './backup/module';

import localInfo from './localInfo/module'

subModules = subModules.concat([
	setting,
  info,
  backup,
  mode,
  update,
  localInfo
	])
/// #endif

/// #if SNOW_MODE=="usb"
import power from './power/module';

subModules = subModules.concat([
	power
	])
/// #endif

let module = angular.module('app.components', subModules)
.name;

export default module;
