import template from './template.html';
import controller from './controller';

let component = {
  restrict: 'E',
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
    // contents: '<',
    // deviceList: '<',
    // prefixFilter: '<',
    // onAdd: '&'
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default component;
