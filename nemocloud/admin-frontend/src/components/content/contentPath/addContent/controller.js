class Controller {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, cognito, deviceManager, logs, modal, s3, toastr, spinner) {
    'ngInject'
    this.cognito = cognito;
    this.s3 = s3;
    this.logs = logs;
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, deviceManager, modal, toastr, spinner) {
    'ngInject'
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, deviceManager, modal, toastr, spinner) {
    'ngInject'
  /// #endif
    this.$rootScope = $rootScope
    this.deviceManager = deviceManager;
    this.modal = modal;
    this.toastr = toastr;
    this.spinner = spinner
    this.resolve;
    this.contents;
    this.deviceList;
    this.content = {
      name: '',
      dest: '/',
      src: '/'
    };
  }

  $onInit() {
    this.contents = this.resolve.contents;
    this.deviceList = this.resolve.deviceList;
  }

  add() {
    if (this.content.name === '') {
      let params = {
        title: '컨텐츠 추가 실패',
        content: '컨텐츠 이름을 입력해야 합니다.'
      }
      this.close()
      this.modal.error(params)
      return Promise.reject(new Error('insert name'));
    }

    // Set dest
    if (!this.content.src.startsWith('/')) {
      this.content.src = `/${this.content.src}`
    }
    this.content.dest = this.content.src;
    // Push new content
    let value = JSON.parse(JSON.stringify(this.contents))
    let input = JSON.parse(JSON.stringify(this.content))

    return Promise.resolve()
    .then(() => {
      for (let content of value) {
        if (content.name === input.name) {
          throw new Error('Same name is already exist')
        }
      }

      this.close()
      this.spinner.on()
      value.push(input)
      return this.resolve.prefixFilter(value, false);
    })
    .then(() => this.deviceManager.setContentSync(value))
    .then((result) => console.log('addContents: ', result))
    .then(() => this.toastr.success(`새로운 컨텐츠 ${this.content.name}이(가) 목록이 추가되었습니다.`, '컨텐츠 추가 성공'))
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '컨텐츠',
        target: this.content.name,
        ops: 'add'
      }
      return this.logs.success(params)
    })
    /// #endif
    .catch((err) => {
      console.error('addContents: ', err);
      let params = {
        title: '컨텐츠 추가 실패',
        content: '컨텐츠 추가 도중 오류가 발생했습니다.',
      }

      switch(err.message) {
        case 'Same name is already exist':
          params.content = '이미 같은 이름이 존재합니다.'
          break;
        default:
          params.error = err.message
      }
      this.close();
      this.modal.error(params)
    })
    /// #if SNOW_MODE=='cloud'
    // Create new s3 contents folder
    .then(() => this.cognito.getCustomer())
    .then(customer => {
      let filename = `${customer}${input.dest}/`;
      let folder = new File([""], "folder");
      return this.s3.uploadFile(this.s3.getConfig().contentsBucket, filename, folder)
    })
    .then((result) => console.log(`Create Folder ${input.dest}`))
    .catch((err) => console.error('Create Folder:', err))
    /// #endif
    .then(() => this.resolve.onAdd())
    .catch(err => {
      console.error('refresh fails', err);
      let params = {
        title: '불러오기 실패',
        content: '목록 불러오기 도중 오류가 발생했습니다.\n새로고침을 눌러주세요',
        error: err.message
      }
      // this.modal.error(params)
    })
    .then(() => {
      this.close()
      this.spinner.off()
    })
  }

  cancel(){
    this.close();
  }

  // TODO: notify user about directory policy(must 1-depth folder)
  checkSlash(content) {
    if (!content.src) {
      content.src = '/'
    } else if (!content.src.startsWith('/')) {
      content.src = `/${content.src}`
    }

    if (content.src.split('/').length > 2) {
      content.src = content.src.slice(0, content.src.indexOf('/', 1))
    }
  }
}

export default Controller;
