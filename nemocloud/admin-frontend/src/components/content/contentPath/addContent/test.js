import testModule from './module'
import testComponent from './component';
import testController from './controller';
import testTemplate from './template.html';

function checkContentSync(contentSync, prefix) {
  let names = new Set();
  for (let content of contentSync) {
    if (names.has(content.name)) {
      throw new Error(`content sync name ${content.name} is duplicated`);
    } else {
      names.add(content.name);
    }
    
    let key = content.dest.slice(1);
    let srcExpected = `${prefix}/${meta.customer}/${key}`
    if (content.src !== srcExpected) {
      throw new Error(`${content.name}: src(${content.src}) doesn't match to dest(${content.dest})`)
    }
  }
}

describe('addContent', () => {
  let $rootScope, cognito, deviceManager, dynamoDB;
  let $componentController;
  let deviceList;
  let session;

  beforeEach(window.module('app'));
  beforeEach(inject((_$componentController_, _cognito_, _deviceManager_, _dynamoDB_) => {
    $componentController = _$componentController_;
    cognito = _cognito_;
    deviceManager = _deviceManager_;
    dynamoDB = _dynamoDB_;
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    let controller;
    let deviceList;
    
    beforeEach(function () {
      this.timeout(20000)
      let contentPathController = $componentController('contentPath', null, {})
      
      return cognito.login(meta.username, meta.password)
      .then(result => deviceManager.getDeviceList())
      .then(result => {
        deviceList = result;
        let bindings = {
          resolve: {
            contents: deviceList[0].contentsync,
            deviceList,
            prefixFilter: (list, on) => contentPathController.prefixFilter(list, on),
            onAdd: () => deviceList[0].fetch()
          },
          close: () => {}
        }
        
        controller = $componentController(testModule, null, bindings)
        controller.$onInit()
        
        return null;
      })
    });
    
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = $componentController(testModule, null, {})
      expect(controller).to.have.property('cognito');
      expect(controller).to.have.property('s3');
    });
    
    it('add content without name', () => { // erase if removing this.name from the controller
      let randomString = (Math.random() + 1).toString(36).substr(2, 7);
      
      controller.content = {
        name: '',
        src: `/${randomString}`,
        dest: '/'
      }
        
      return controller.add()
      .then(() => {
        throw new Error('add() must be rejected without name')
      })
      .catch(err => {
        if (err.message === 'add() must be rejected without name') {
          throw err
        }
        expect(err.message).to.equal('insert name');
        
        for (let contentsync of deviceList[0].contentsync) {
          expect(contentsync.name).to.not.equal('');
        }
        return null
      })
    })
    
    
    let randomString = (Math.random() + 1).toString(36).substr(2, 7);
    let tests = [
      {
        desc: 'add contents',
        name: randomString,
        src: `/${randomString}`
      },
      {
        desc: `add contents without '/'`,
        name: (Math.random() + 1).toString(36).substr(2, 7),
        src: (Math.random() + 1).toString(36).substr(2, 7)
      },
    ]
    
    tests.forEach(test => {
      it(test.desc, () => { // erase if removing this.name from the controller
        controller.content = {
          name: test.name,
          src: test.src,
          dest: '/'
        }
        
        return controller.add()
        .then(() => {
          checkContentSync(deviceList[0].contentsync, dynamoDB.getConfig().contentsSrcPrefix)
          
          let lastIndex = deviceList[0].contentsync.length - 1
          let destExpected = test.src.startsWith('/')?`${test.src}`:`/${test.src}`
          
          expect(deviceList[0].contentsync[lastIndex].name).to.equal(test.name);
          expect(deviceList[0].contentsync[lastIndex].src).to.equal(`${dynamoDB.getConfig().contentsSrcPrefix}/${meta.customer}${destExpected}`);
          expect(deviceList[0].contentsync[lastIndex].dest).to.equal(destExpected);
          
          // TODO: check s3 folder create
          return null
        })
      });
    })
    
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(testTemplate).to.match(/컨텐츠 이름/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = testComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(testTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(testController);
    });
  });
});
