import Promise from 'bluebird'

class Controller {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, $uibModal, cognito, deviceManager, dynamoDB, toastr, logs, modal, spinner) {
    'ngInject'
    this.cognito = cognito;
    this.logs = logs;
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, $uibModal, deviceManager, dynamoDB, toastr, modal, spinner) {
    'ngInject'
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, $uibModal, deviceManager, dynamoDB, toastr, modal, spinner) {
    'ngInject'
  /// #endif
    this.$rootScope = $rootScope
    this.deviceManager = deviceManager;
    this.dynamoDB = dynamoDB;
    this.$uibModal = $uibModal;
    this.toastr = toastr;
    this.modal = modal;
    this.spinner = spinner;
    this.contents = [];
    this.deviceList = [];
    this.selected = null;
    this.selectedOrigin = '';
  }

  $onInit() {
    this.prefixFilterResolve = (list, on) => this.prefixFilter(list, on)
    return this.getDeviceList()
  }

  add(){
    this.$uibModal.open({
      component: 'addContent',
      resolve: {
        contents: () => this.contents,
        deviceList: () => this.deviceList,
        prefixFilter: () => {
          return (list, on) => this.prefixFilter(list, on)
        },
        onAdd: () => {
          return () => this.refresh()
        }
      },
      size: 'lg',
    })
  }

  configUpdate(content) {
    if (this.selectedOrigin && this.selected) {
      this.selected.src = this.selectedOrigin
    }

    if(this.selected === content) {
      this.selected = null;
      this.selectedOrigin = '';
    } else {
      this.selected = content
      this.selectedOrigin = content.src;
    }
  }

  confirmRemove(content) {
    let params = {
      title: '컨텐츠 경로 삭제',
      content: '삭제하시겠습니까?',
      onConfirm: () => this.remove(content)
    }
    this.modal.confirm(params)
  }

  // TODO: notify user about directory policy(must 1-depth folder)
  checkSlash(content) {
    if (!content.src) {
      content.src = '/'
    } else if (!content.src.startsWith('/')) {
      content.src = `/${content.src}`
    }

    if (content.src.split('/').length > 2) {
      content.src = content.src.slice(0, content.src.indexOf('/', 1))
    }
  }

  getDeviceList() {
    this.spinner.on()
    return this.deviceManager.getDeviceList()
    .then(deviceList => {
      if (deviceList.length === 0) {
        throw new Error('No device')
      }
      this.deviceList = deviceList
      // Copy contents array
      let list = JSON.parse(JSON.stringify(deviceList[0].contentsync))
      // Hide prefix
      return this.prefixFilter(list, true)
    })
    .then(list => {
      this.contents = list
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('get device list fails', err);
      let params = {
        title: '오류',
        content: '목록을 가져올 수 없습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  prefixFilter(list, on) {
    let prefix = this.dynamoDB.getConfig().contentsSrcPrefix
    /// #if SNOW_MODE=='cloud'
    return this.cognito.getCustomer()
    .then(customer => {
      prefix = `${prefix}/${customer}`
    })
    /// #endif
    /// #if SNOW_MODE=='local'
    return Promise.resolve()
    /// #endif
    /// #if SNOW_MODE=='usb'
    return Promise.resolve()
    /// #endif
    .then(() => {
      if (on) {
        for (let content of list) {
          if (content.src.startsWith(prefix)) {
            content.src = content.src.slice(prefix.length)
          }
        }
      } else {
        for (let content of list) {
          if (!content.src.startsWith(prefix)) {
            if (content.src.startsWith('/')) {
              content.src = `${prefix}${content.src}`
            } else {
              content.src = `${prefix}/${content.src}`
            }
          }

          if (!content.dest.startsWith('/')) {
            content.dest = `/${content.dest}`
          }
        }
      }
      return list;
    })
  }

  refresh() {
    this.spinner.on()
    return this.deviceManager.fetchDevices()
    .then(() => this.getDeviceList())
    .then(() => this.onRefresh())
    .then(() => this.spinner.off())
  }

  remove(index) {
    this.spinner.on()
    let removed

    return Promise.resolve()
    .then(() => {
      let value = JSON.parse(JSON.stringify(this.deviceList[0].contentsync));
      removed = value.splice(index, 1)
      return this.prefixFilter(value, false);
      // To remove contentsync item in each device
      // this.deviceManager.removeContents(content.name)
    })
    .then(value => this.deviceManager.setContentSync(value))
    .then(result => console.log('removeContents', result))
    .then(() => this.toastr.success(`컨텐츠를 제거했습니다.`))
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '컨텐츠',
        target: removed[0].name,
        ops: 'remove'
      }
      return this.logs.success(params)
    })
    /// #endif
    .catch((err) => {
      console.error('removeContents', err);
      let params = {
        title: '컨텐츠 제거 실패',
        content: '컨텐츠 제거 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.refresh())
    .catch(err => {
      console.error('refresh fails', err);
      let params = {
        title: '불러오기 실패',
        content: '목록 불러오기 도중 오류가 발생했습니다.\n새로고침을 눌러주세요',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  update(index, content) {
    this.spinner.on()

    return Promise.resolve()
    .then(() => {
      content.dest = content.src;
      let value = JSON.parse(JSON.stringify(this.deviceList[0].contentsync))
      value[index] = JSON.parse(JSON.stringify(content));
      return this.prefixFilter(value, false);
    })
    .then(value => this.deviceManager.setContentSync(value))
    .then(result => console.log('updateContents', result))
    .then(() => this.toastr.success(`컨텐츠 ${content.name}을(를) 수정했습니다.`, '컨텐츠 수정 완료'))
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '컨텐츠',
        target: content.name,
        ops: 'modify'
      }
      return this.logs.success(params)
    })
    /// #endif
    .catch((err) => {
      console.error('updateContents', err);
      let params = {
        title: '컨텐츠 수정 실패',
        content: '컨텐츠 수정 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.refresh())
    .catch(err => {
      console.error('refresh fails', err);
      let params = {
        title: '불러오기 실패',
        content: '목록 불러오기 도중 오류가 발생했습니다.\n새로고침을 눌러주세요',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
