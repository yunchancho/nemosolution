import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-toastr/dist/angular-toastr.min.css';
import 'angular-toastr/dist/angular-toastr.tpls.js';

import component from './component';

import addContent from './addContent/module'

let subModules = [
	uiRouter,
	'toastr',
	// 'app.commons.services.deviceManager',
	// 'app.commons.services.dynamoDB',
	// 'app.commons.services.toastr',
	// 'app.commons.services.modal',
	// 'app.commons.services.spinner',
	addContent
]

/// #if SNOW_MODE=='cloud'
subModules = subModules.concat([
	'app.commons.services.cognito',
])
/// #endif 

const name = 'contentPath';
let module = angular.module(name, subModules)
	.component(name, component)
	.name

export default module; 
