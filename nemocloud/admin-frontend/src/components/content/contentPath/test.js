import testModule from './module'
import testComponent from './component';
import testController from './controller';
import testTemplate from './template.html';

function checkContentSync(contentSync, prefix) {
  let names = new Set();
  for (let content of contentSync) {
    if (names.has(content.name)) {
      throw new Error(`content sync name ${content.name} is duplicated`);
    } else {
      names.add(content.name);
    }
    
    let key = content.dest.slice(1);
    let srcExpected = `${prefix}/${meta.customer}/${key}`
    if (content.src !== srcExpected) {
      throw new Error(`${content.name}: src(${content.src}) doesn't match to dest(${content.dest})`)
    }
  }
}

describe('Content Path', () => {
  let $componentController, cognito, deviceManager, dynamoDB;

  beforeEach(window.module('app'));
  beforeEach(inject((_$componentController_, _cognito_, _deviceManager_, _dynamoDB_) => {
    $componentController = _$componentController_;
    cognito = _cognito_;
    deviceManager = _deviceManager_;
    dynamoDB = _dynamoDB_
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    let controller;
    
    beforeEach(function() {
      this.timeout(15000)
      let bindings = {
        onRefresh: () => {}
      }
      controller = $componentController(testModule, null, bindings);
      
      return controller.cognito.login(meta.username, meta.password)
      .then(() => controller.$onInit())
      .then(() => null)
    });
    
    it('has properties', () => { // erase if removing this.name from the controller
      expect(controller).to.have.property('selected');
      expect(controller).to.have.property('contents');
      expect(controller).to.have.property('deviceList');
    });
    
    it('has valid content sync', function() {
      expect(controller.deviceList.length).to.equal(meta.cloudDevice.number);
      for (let device of controller.deviceList) {
        checkContentSync(device.contentsync, dynamoDB.getConfig().contentsSrcPrefix)
      }
    })
    
    it('select content sync to edit', function() {
      if (controller.contents.length < 2) {
        this.skip()
      }
      
      let index = Math.floor(Math.random() * controller.contents.length);
      let firstSelected = controller.contents[index]
      // select content sync(click edit button)
      controller.configUpdate(firstSelected)
      expect(controller.selected).to.equal(firstSelected)
      controller.selected.src = 'modify';
      
      // select another content sync
      index = (index + 1) % controller.contents.length;
      controller.configUpdate(controller.contents[index])
      expect(firstSelected.src).to.not.equal('modify');
      expect(controller.selected).to.not.equal(firstSelected)
      expect(controller.selected).to.equal(controller.contents[index])
    })
    
    it('update content sync', function() {
      let index = Math.floor(Math.random() * controller.contents.length);
      let randomString = '/' + (Math.random() + 1).toString(36).substr(2, 7);
      let updatedItem = JSON.parse(JSON.stringify(controller.contents[index]));
      
      updatedItem.src = randomString;
      
      return controller.update(index, updatedItem)
      .then(() => {
        let contentsync = controller.deviceList[0].contentsync
        expect(contentsync[index].name).to.equal(updatedItem.name)
        expect(contentsync[index].src).to.equal(`${dynamoDB.getConfig().contentsSrcPrefix}/${meta.customer}${randomString}`)
        expect(contentsync[index].dest).to.equal(`${randomString}`)
      })
    })
    
    it('update content sync without "/"', function() {
      let index = Math.floor(Math.random() * controller.contents.length);
      let randomString = (Math.random() + 1).toString(36).substr(2, 7);
      let updatedItem = JSON.parse(JSON.stringify(controller.contents[index]));
      
      updatedItem.src = randomString;
      
      return controller.update(index, updatedItem)
      .then(() => {
        let contentsync = controller.deviceList[0].contentsync
        expect(contentsync[index].name).to.equal(updatedItem.name)
        expect(contentsync[index].src).to.equal(`${dynamoDB.getConfig().contentsSrcPrefix}/${meta.customer}/${randomString}`)
        expect(contentsync[index].dest).to.equal(`/${randomString}`)
      })
    })
    
    it('remove content sync', function() {
      let contentsync = controller.contents;
      let index = Math.floor(Math.random() * contentsync.length);
      let removedItem = JSON.parse(JSON.stringify(contentsync[index]));
      
      return controller.remove(index)
      .then(() => {
        for (let contentsync of controller.contents) {
          expect(contentsync).not.deep.to.equal(removedItem)
        }
      })
    })
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(testTemplate).to.match(/저장/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = testComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(testTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(testController);
    });
  });
});
