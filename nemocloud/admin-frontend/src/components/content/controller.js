class Controller {
  /// #if SNOW_MODE=="cloud"
  constructor($rootScope, cognito, s3) {
    'ngInject'
    this.cognito = cognito;
    this.s3 = s3;
  /// #endif
  /// #if SNOW_MODE=="local"
  constructor($rootScope, localApis) {
    'ngInject'
    this.localApis = localApis;
  /// #endif
  /// #if SNOW_MODE=="usb"
  constructor($rootScope, localApis) {
    'ngInject'
    this.localApis = localApis;
  /// #endif
    this.name = 'content';
    this.$rootScope = $rootScope
    this.options = {
      bucket: '',
      indexPrefix: '',
      func: {},
    }
    this.addIndex = true;
    this.contentBrowse = false;
    //TODO : work a round solution
    $("body").removeClass("mini-navbar");
    $(".collapse").removeClass("in");
    $(".nav > li").removeClass("active");
  }

  /// #if SNOW_MODE=="cloud"
  $onInit() {
    this.options.bucket = this.s3.getConfig().contentsBucket
    this.cognito.getCustomer()
    .then(customer => {
      this.options.indexPrefix = `${customer}/`
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return null;
    })
    .catch(err => {
      console.error(`can't get customer`, err)
    })
  }

  tabOpen(val) {
    if (val === 'index'){
      this.contentBrowse = false;
      this.addIndex = true;
    } else if (val === 'content'){
      this.addIndex = false;
      this.contentBrowse = true;
    }
  }

  refresh() {
    this.options.func.refreshIndex();
  }
  /// #endif
}

export default Controller;
