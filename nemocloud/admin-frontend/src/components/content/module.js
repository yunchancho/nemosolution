import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import contentPath from './contentPath/module';
/// #if SNOW_MODE=="usb"
import usbBrowser from './usbBrowser/module'
/// #endif
let subModule = [
	uiRouter,
	/// #if SNOW_MODE=="cloud"
	'app.commons.services.s3',
	/// #endif
	contentPath,
	/// #if SNOW_MODE=="usb"
	usbBrowser
	/// #endif
]

const name = 'content';

let module = angular.module(name, subModule)
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/content',
			component: name
		});
}
