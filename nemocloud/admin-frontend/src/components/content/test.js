/// #if SNOW_MODE=='cloud'
import testModule from './module'
import testComponent from './component';
import testController from './controller';
import testTemplate from './template.html';

describe('Content', () => {
  let $componentController;

  beforeEach(window.module('app'));
  beforeEach(inject((_$componentController_) => {
    $componentController = _$componentController_;
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
    let controller = $componentController(testModule, null, {});
      expect(controller).to.have.property('options');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(testTemplate).to.match(/컨텐츠/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = testComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(testTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(testController);
    });
  });
});
/// #endif