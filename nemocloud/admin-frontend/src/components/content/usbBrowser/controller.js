const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const homedir = require('os-homedir');
import dummyUsb from './dummyUsb';

class Controller {
  constructor($rootScope, $uibModal, modal, spinner, toastr){
    'ngInject'
    this.name="usbBrowser"
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.modal = modal;
    this.toastr = toastr;
    this.spinner = spinner;
    this.directory = '';
    this.selectedIndex = "";
    this.files=[];
    this.folders=[];
    this.usbs = [];
    this.homedir;
    this.message = "";
    this.pickFiles;
  }

  $onInit() {
    this.homedir = homedir();
    this.getUsbInfo();
  }

  back(){
    console.log("백하는 dir",this.selectedIndex);
    if(this.selectedIndex === ""){
      return false;
    }
    let num = this.directory.indexOf(this.selectedIndex)
    if(num < 0){
      return this.getUsbInfo();
    }
    console.log(num);
    this.selectedIndex = this.directory.slice(num)
    this.directory = this.directory.slice(0, num)
    console.log("back dir", this.directory);
    return this.listFiles();
  }

  createFolder() {
    let modal = this.$uibModal.open({
      component: 'usbCreateFolder',
      resolve: {
        directory: () => { return this.directory },
        onComplete: () => { return () => this.listFiles() }
      },
      size: 'sm'
    });
  }

  deleteFile(key) {
    this.spinner.on()
    console.log(this.directory+"/"+key);
    return fs.unlinkAsync(this.directory+"/"+key)
    .then((result) => {
      return this.listFiles();
    })
    .catch((err) => {
      console.error('delete Object: ', err);
    })
    .then(() => this.toastr.success(key+'가 삭제됬습니다.'))
    .then(() => this.spinner.off())
  }

  deleteFolder(key) {
    this.spinner.on()
    let files = fs.readdirSync(this.directory+"/"+key);
    let request1 = files.map(file => {
      return fs.unlinkAsync(this.directory+"/"+key+"/"+file)
    })
    let request2 = fs.rmdirAsync(this.directory+"/"+key)
    return Promise.all([...request1, request2])
    .then((result) => {
      return this.listFiles();
    })
    .catch((err) => {
      console.error('delete Object: ', err);
    })
    .then(() => this.toastr.success(key+'가 삭제됬습니다.'))
    .then(() => this.spinner.off())
  }

  deleteFiles() {
    this.spinner.on()
    let folders =[];
    let files = [];
    for (let folder of this.folders) {
      if (folder.check) {
        folders.push(folder);
      }
    }
    for (let file of this.files) {
      if (file.check) {
        files.push(file);
      }
    }

    let request1 = files.map(file => {
      return this.deleteFile(file.name)
    })
    let request2 = folders.map(folder =>{
      return this.deleteFolder(folder.name)
    })
    return Promise.all([...request1, ...request2])
    .then(() => {
      this.toastr.success('파일 삭제가 완료되었습니다..')
      return this.listFiles();
    })
    .catch(err => {
      console.error('download files', err);
      let params = {
        title: '파일 삭제 실패',
        content: '파일 삭제 중 오류가 발생했습니다.',
        error: err.message
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  downloadFile(key){
    return fs.readFileAsync(this.directory+"/"+key)
    .then(()=>{
      fs.writeFileAsync(this.homedir+"/"+key)
    })
    .then(() => {
      this.toastr.success(this.homedir+'/'+key+'다운 완료되었습니다.')
    })
  }

  downloadFiles(){
    let files=[];
    for (let file of this.files) {
      if (file.check) {
        files.push(file);
      }
    }
    files.map(file => {
      return fs.readFileAsync(this.directory+"/"+file.name)
      .then(() => {
        fs.writeFileAsync(this.homedir+"/" + file.name )
      })
      .then(() => {
        this.toastr.success('다운 완료되었습니다.')
      })
    })
  }

  listFiles() {
    console.log("mount path : ",this.directory);
    return fs.readdirAsync(this.directory)
    .then(result => {
      this.folders=[];
      this.files=[];
      result.map(file => {
        let obj = {
          name : file,
          check : false
        }
        if(file.indexOf('.') < 0){
          this.folders.push(obj);
        } else if(file.indexOf('.')>0){
          this.files.push(obj);
        }
      })
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      let params = {
        title: '실패',
        content: '파일 불러오기에 실패했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
  }

  getUsbInfo(){
    this.spinner.on()
    return Promise.resolve()
    .then(() => {
      this.usbs = dummyUsb
    })
    .then(() => {
      if(this.usbs.length > 0){
        this.directory = this.usbs[0].fs[0].mount
        this.listFiles();
        this.$rootScope.$$phase || this.$rootScope.$apply();
      } else if(this.usbs.length === 0){
        return this.message = "usb가 연결되어 있지 않습니다."
      }
    })
    .catch(err => {
      this.error = true;
      console.error('Get fssize error: ', err)
    })
    .then(() => this.spinner.off())
  }

  select(index) {
    this.selectedIndex = "/"+index.name;
    this.directory = this.directory + this.selectedIndex
    console.log("select dir",this.directory);
    return this.listFiles();
  }
  uploadFile(){
    if(this.pickFiles === undefined){
      let params = {
        title: '업로드 실패',
        content: '업로드할 파일이 비어있습니다.',
      }
      this.modal.error(params)
      return;
    }

    this.spinner.on()
    this.pickFiles.map(obj => {
      return fs.readFileAsync(obj.file.path)
      .then(result => {
        console.log(result);
        fs.writeFileAsync(this.directory+"/" + obj.file.name )
      })
      .then(()=>this.listFiles())
      .then(result => {this.toastr.success('업로드에 성공했습니다.')})
      .then(result => {
        console.log(result);
        this.spinner.off()
      })
    })
  }
}

export default Controller;
