import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import usbCreateFolder from './usbCreateFolder/module'

const name = 'usbBrowser';

let module = angular.module(name, [
	uiRouter,
	usbCreateFolder
	])
	.component(name, component)
	.name

export default module;
