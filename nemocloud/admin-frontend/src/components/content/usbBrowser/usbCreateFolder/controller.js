const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));

class Controller {
  constructor(spinner){
    'ngInject'
    this.spinner = spinner
    this.folderName = '';
    this.resolve;
    this.directory;
    this.bucket;
    this.error = ''
  }

  $onInit() {
    this.directory = this.resolve.directory;
  }

  cancel() {
    this.close();
  }

  create() {
    console.log("directory",this.directory);
    if (this.folderName.length === 0
      || this.folderName.indexOf('/') !== -1) {
        this.error = '잘못된 폴더명입니다.'
      return
    }

    let filename = this.directory + '/' + this.folderName;
    console.log("파일이름 : ",filename);
    this.spinner.on()
    return fs.mkdirAsync(filename)
    .then((result) => {
      this.resolve.onComplete();
      this.close();
      return null
    })
    .catch((err) => {
      this.error = err;
      console.error('Create Folder:', err)
    })
    .then(() => this.spinner.off());
  }
}

export default Controller;
