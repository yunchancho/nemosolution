class Controller {
  constructor($rootScope, logs, modal, spinner) {
    'ngInject'
    this.$rootScope = $rootScope;
    this.logs = logs;
    this.spinner = spinner;
    this.modal = modal;
    this.histories = []
  }

  $onInit() {
    // TODO: refresh logs
    this.fetch()
  }

  fetch() {
    this.spinner.on()
    return this.logs.getLogs()
    .then(logs => {
      this.histories = []
      logs.events.sort((a, b) => {
        // sort by time
        if (a.ingestionTime > b.ingestionTime) {
          return -1;
        }
        if (a.ingestionTime < b.ingestionTime) {
          return 1;
        }
        return 0;
      }).forEach(item => {
        let event = JSON.parse(item.message);
        let time = new Date(item.ingestionTime)
        let ops = '수정';
        for (let operator of Object.keys(this.logs.getConfig().ops)) {
          if (event.message.ops === operator) {
            ops = this.logs.getConfig().ops[operator];
            break;
          }
        }

        this.histories.push({
          time: `${time.toLocaleDateString()} ${time.toLocaleTimeString()}`,
          message: `[${event.status}]${event.username}: ${event.message.menu} ${event.message.target}을(를) ${ops}했습니다.`
        })
      })
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('fetch log error', err)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
