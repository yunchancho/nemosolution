class Controller {
  constructor($rootScope, $state){
    'ngInject'
    this.name='homeCloud'
    this.$rootScope = $rootScope;
    this.$state = $state;
  }

  $onInit() {
  }
}

export default Controller;
