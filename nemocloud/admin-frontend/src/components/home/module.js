import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';


/// #if SNOW_MODE=='cloud'
import history from './history/module';
import homeCloud from './homeCloud/module';
/// #endif

/// #if SNOW_MODE=='local'
import homeLocal from './homeLocal/module';
/// #endif

const name = 'home';

let module = angular.module(name, [
	 uiRouter,
	 /// #if SNOW_MODE=='cloud'
	 homeCloud,
	 history,
	 /// #endif
	 /// #if SNOW_MODE=='local'
	 homeLocal
	 /// #endif
 ])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/home',
			component: name
		});
}
