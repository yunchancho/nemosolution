class Controller {
  constructor($rootScope, deviceManager, modal, spinner, localApis){
    'ngInject'
    this.$rootScope = $rootScope;
    this.deviceManager = deviceManager;
    this.modal = modal;
    this.spinner = spinner;
    this.localApis = localApis;
    this.cpu = '';
    this.disk = {}
    this.memory = '';
    this.pc = '';
    this.ip = self.location.hostname;
    this.swKey = '';
    this.graph = {
      labels: ["로컬 용량",  "남은 용량"],
      data: [0, 0],
      options: {
      }
    }
  }
  
  $onInit() {
    this.getInfo()
  }
  
  // Get local disk info and apply graph
  getDiskInfo() {
    return this.localApis.getSystemInfo('fssize')
    .then(result => {
      for (let fs of result) {
        if (fs.mount === '/') {
          this.disk = fs;
        }
      }
      this.disk.view = {
        total: Math.round(this.disk.size / 1048576),
        used: Math.round(this.disk.used / 1048576),
        remain: Math.round((this.disk.size - this.disk.used) / 1048576),
      }
      console.log('disk', this.disk)
      
      this.graph.data = [
        this.disk.view.used,
        this.disk.view.remain,
      ]
      
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return;
    })
    .catch(err => {
      console.error('get disk info fails', err)
      throw err
    })
  }
  
  //TODO: add reject
  getIpAddress() {
    if (self.location.hostname !== 'localhost'
    && self.location.hostname !== '127.0.0.1') {
      return Promise.resolve(self.location.hostname)
    }
    
    return new Promise((resolve, reject) => {
      //compatibility for firefox and chrome
      let myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
      let pc = new myPeerConnection({iceServers: []});
      let localIPs = {};
      let ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g;
      let key;
      
      function ipIterate(ip) {
        if (!localIPs[ip]) {
          resolve(ip);
        } 
        localIPs[ip] = true;
      }
      pc.createDataChannel(""); //create a bogus data channel
      pc.createOffer((sdp) => {
        sdp.sdp.split('\n').forEach((line) => {
          if (line.indexOf('candidate') >= 0) {
            line.match(ipRegex).forEach(ipIterate);
          }
        });
        pc.setLocalDescription(sdp, ()=>{}, ()=>{});
      }, ()=>{}); // create offer and set local description
      pc.onicecandidate = (ice) => { //listen for candidate events
        if (ice && ice.candidate && ice.candidate.candidate && ice.candidate.candidate.match(ipRegex)){
          ice.candidate.candidate.match(ipRegex).forEach(ipIterate);
        } 
      };
    });
  }
  
  getInfo() {
    let requests = []
    let promise;
    this.spinner.on()
    
    promise = this.getIpAddress()
    .then(ip => {
      this.ip = ip
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('get IP error:', err)
      throw err;
    })
    requests.push(promise)
    
    promise = this.localApis.getSystemInfo('cpu')
    .then(cpu => {
      this.cpu = `${cpu.cpu.manufacturer} ${cpu.cpu.brand}`;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('get CPU error:', err)
      throw err;
    })
    requests.push(promise)
    
    promise = this.localApis.getSystemInfo('memory')
    .then(memory => {
      // this.memory = Math.round(memory.total / 1073741824 * 100) / 100
      this.memory = memory.total
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('get Memory error:', err)
      throw err;
    })
    requests.push(promise)
    
    promise = this.localApis.getSystemInfo('pc')
    .then(pc => {
      this.pc = `${pc.manufacturer} ${pc.model}` 
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('get PC error:', err)
      throw err;
    })
    requests.push(promise)
    
    promise = this.localApis.getSwKey()
    .then(swKey => {
      this.swKey = swKey;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('get swkey error:', err)
      throw err;
    })
    requests.push(promise)

    requests.push(this.getDiskInfo())
    
    Promise.all(requests)
    .catch(err => {
      let params = {
        title: '오류',
        content: '디바이스 정보를 가져오던 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
