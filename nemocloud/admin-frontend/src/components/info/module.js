import angular from 'angular';
import uiRouter from 'angular-ui-router';
import chartJs from 'angular-chart.js';

import component from './component';

const name = 'info';

let module = angular.module(name, [
	uiRouter,
	chartJs
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('info', {
			url: '/info',
			component: name
		});
}
