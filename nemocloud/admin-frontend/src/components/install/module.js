import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import userDeviceList from './userDeviceList/module';

const name = 'install';

let module = angular.module(name, [
	uiRouter,
	userDeviceList,
 ])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/install',
			component: name
		});
}
