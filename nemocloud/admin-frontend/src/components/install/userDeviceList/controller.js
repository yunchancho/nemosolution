class Controller {
  constructor($rootScope, $state, cognito, deviceManager, iot){
    'ngInject'
    this.name = 'userDeviceList';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.cognito = cognito;
    this.deviceManager = deviceManager;
    this.iot = iot
    
    this.deviceList = [];
    this.status;
  }
  
  $onInit() {
    this.status = this.iot.getConfig().status;
    this.getDeviceList();
  }
  
  confirm(device) {
    device.confirmInstall()
    .then((result) => {
      this.$rootScope.$$phase || this.$rootScope.$apply();
      console.log('checkInstalled Success');
    }).catch((err) => {
      console.error('checkInstalledThing:', err);
    });
  }
  
  getDeviceList() {
    this.deviceManager.getDeviceList()
    .then((result) => {
      this.deviceList = result;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch((err) => {
      console.error('listDevices', err);
    })
  }
  
  // Scan each device table items from dynamoDB
  refreshDevices() {
    this.deviceManager.fetchDevices()
    .then((result) => {
      this.$rootScope.$$phase || this.$rootScope.$apply();
      console.log('Device List:', result);
    })
    .catch((err) => {
      console.error('refreshDevices', err);
    })
  }
}

export default Controller;
