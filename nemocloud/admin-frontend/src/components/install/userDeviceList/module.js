import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'userDeviceList';

let module = angular.module(name, [ 
	uiRouter,
	'app.commons.services.cognito',
	'app.commons.services.dynamoDB'
	])
	.component(name, component)
	.name

export default module; 