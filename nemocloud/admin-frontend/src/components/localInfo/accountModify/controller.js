class Controller {
  constructor($rootScope, $state, $uibModal, modal, spinner, localApis){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.modal = modal
    this.spinner = spinner;
    this.localApis = localApis;
    this.oldPassword = '';
    this.newPassword = '';
    this.confirmPassword = '';
  }

  $onInit() {
    this.resolve.setSubtitle('Account')
  }

  //TODO: Apply newline
  changePassword() {
    this.spinner.on()
    if (this.newPassword !== this.confirmPassword) {
      console.error('confirm password doesn\'t match')
      let params = {
        title: '비밀번호 변경 실패',
        content: '비밀번호 확인이 일치하지 않습니다.'
      }
      this.modal.error(params)
      this.spinner.off()
      return;
    }

    this.localApis.changePassword(this.oldPassword, this.newPassword)
    .then(result => {
      console.log('Password is changed')
      let params = {
        title: '비밀번호 변경 성공',
        content: `변경이 완료되었습니다.\n다음에 로그인할 경우,\n새 비밀번호를 사용하세요.`,
        onClosed: () => {
          this.localApis.signOut()
          this.$state.reload()
        }
      }
      this.modal.success(params)
    })
    .catch(err => {
      console.error('Change Password error:', err)
      let params = {
        title: '비밀번호 변경 실패',
        content: '비밀번호 변경 실패.',
        error: err.message
      }
      switch(err.message) {
        case 'AUTH_ERROR_PASSWORD_NOT_PROPER':
          params.content = '현재 비밀번호가 일치하지 않습니다.'
          break;
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
