import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';


const name = 'accountModify';

let module = angular.module(name, [
	uiRouter,
	'app.commons.services.localApis'
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('localInfo.accountModify', {
			url: '/accountModify',
			component: name
		});
}
