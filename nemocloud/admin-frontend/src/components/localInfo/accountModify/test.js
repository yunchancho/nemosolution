/// #if SNOW_MODE=='local'
import accountModifyModule from './module'
import accountModifyComponent from './component';
import accountModifyController from './controller';
import accountModifyTemplate from './template.html';

describe('accountModify', () => {
  let $rootScope, makeController;

  beforeEach(window.module(accountModifyModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new accountModifyController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('$rootScope');
      expect(controller).to.have.property('$state');
      expect(controller).to.have.property('localApis');
      expect(controller).to.have.property('newPassword');
      expect(controller).to.have.property('oldPassword');
      expect(controller).to.have.property('confirmPassword');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(accountModifyTemplate).to.match(/비밀번호 변경/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = accountModifyComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(accountModifyTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(accountModifyController);
    });
  });
});
/// #endif
