class Controller {
  constructor($rootScope){
    'ngInject'
    this.$rootScope = $rootScope;
    this.breadcrumbs = ['정보']
    this.resolve = {
      setBreadcrumb: (title) => this.setBreadcrumb(title)
    }
  }
  
  setBreadcrumb(title) {
    for (let i = 0; i < this.breadcrumbs.length; i++) {
      if (this.breadcrumbs[i] === title) {
        this.breadcrumbs.splice(i + 1, Number.MAX_SAFE_INTEGER);
        return;
      }
    }
    this.breadcrumbs.push(title);
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
}

export default Controller;
