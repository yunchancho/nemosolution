import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import accountModify from './accountModify/module'

const name = 'localInfo';

let module = angular.module(name, [
	uiRouter,
	accountModify
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('localInfo', {
			url: '/localInfo',
			component: name
		});
}
