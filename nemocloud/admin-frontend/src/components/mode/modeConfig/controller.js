class Controller {
  constructor($rootScope, $state, modal, spinner, toastr, localApis, network){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.modal = modal;
    this.spinner = spinner;
    this.toastr = toastr;
    this.localApis = localApis;
    this.network = network;
    this.options;
  }

  $onInit() {
    this.options = this.resolve.options
    if (!this.options.mode) {
      this.$state.go('mode.select')
      return;
    }
    this.getStatus();
  }

  // TODO: move modal to front
  confirmChange(save) {
    let params = {
      title: '모드 변경',
      content: '모드를 변경합니다.\n계속하시겠습니까?',
      warning: '기기 내 모든 데이터가 삭제됩니다.',
      onConfirm: () => this.change(save)
    }
    this.close()
    this.modal.confirm(params)
  }

  change(save) {
    this.spinner.on()
    // change mode
    this.options.keepConfig = save;
    this.localApis.changeMode(this.options)
    .then(result => {
      console.log('mode change complete', result)
      return this.resolve.onChanges()
    })
    .then((result) => {
      this.toastr.success(`${(this.options.mode === 'cloud')?'클라우드':'로컬'} 모드로 변경되었습니다.`, '모드 변경 완료')
      this.close();
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      let params = {
        title: '모드 변경 실패',
        content: `모드 변경 중 오류가 발생했습니다.`,
        error: err.message,
        onConfirm: () => this.change(save)
      }

      switch(err.message) {
        case 'DEVICE_ERROR_SET_MODE_ALREADY_DONE':
          params.content = `이미 ${(this.options.mode === 'cloud')?'클라우드':'로컬'} 모드입니다.\n새로고침 후 다시 시도해 주세요.`
          break;
      }

      this.close();
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  getStatus() {
    this.spinner.on()
    this.network.getStatus()
    .then(result => {
      if (!result.internet) {
        this.$state.go('mode.disconnected')
      }
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('get network status', err)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
