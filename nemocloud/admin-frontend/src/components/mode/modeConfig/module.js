import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-toastr/dist/angular-toastr.min.css';
import 'angular-toastr/dist/angular-toastr.tpls.js';

import component from './component';

const name = 'modeConfig';

let module = angular.module(name, [
	uiRouter,
	'toastr',
	'app.commons.services.localApis',
	'app.commons.services.network',
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";
	
	$stateProvider
		.state('mode.config', {
			url: '/config',
			component: name
	});
}