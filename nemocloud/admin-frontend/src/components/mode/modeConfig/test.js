/// #if SNOW_MODE=='local'
import modeConfigModule from './module'
import modeConfigComponent from './component';
import modeConfigController from './controller';
import modeConfigTemplate from './template.html';

describe('modeConfig', () => {
  let $rootScope, $state, makeController;

  beforeEach(window.module(modeConfigModule));
  beforeEach(inject((_$rootScope_, _$state_) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    makeController = () => {
      return new modeConfigController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('network');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(modeConfigTemplate).to.match(/기존 설정 유지 여부/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = modeConfigComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(modeConfigTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(modeConfigController);
    });
  });
});
/// #endif