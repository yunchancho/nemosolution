class Controller {
  constructor($rootScope, $state, localApis, network){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.localApis = localApis;
    this.network = network;
  }
}

export default Controller;
