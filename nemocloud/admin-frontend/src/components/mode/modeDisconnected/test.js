/// #if SNOW_MODE=='local'
import modeDisconnectedModule from './module'
import modeDisconnectedComponent from './component';
import modeDisconnectedController from './controller';
import modeDisconnectedTemplate from './template.html';

describe('modeDisconnected', () => {
  let $rootScope, makeController;

  beforeEach(window.module(modeDisconnectedModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new modeDisconnectedController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('network');
      expect(controller).to.have.property('localApis');

    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(modeDisconnectedTemplate).to.match(/인터넷을 연결해주세요/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = modeDisconnectedComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(modeDisconnectedTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(modeDisconnectedController);
    });
  });
});
/// #endif