class Controller {
  constructor($rootScope, $state, $uibModal, modal, spinner, toastr, localApis, network){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state
    this.$uibModal = $uibModal
    this.modal = modal;
    this.spinner = spinner
    this.toastr = toastr;
    this.localApis = localApis;
    this.network = network;
    this.current = '';
    this.cloudAllowed = null;
  }
  
  $onInit() {
    this.spinner.on()
    this.getStatus()
    .then(() => this.getMode())
    .catch(err => {
      let params = {
        title: '오류',
        content: '모드 정보를 가져올 수 없습니다.',
        error: err.message
      }
      
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
  
  change() {
    this.spinner.on()
    // change mode
    let options = {
      mode: (this.current === 'cloud')?'local':'cloud',
      keepConfig: false
    }
    this.localApis.changeMode(options)
    .then(result => {
      console.log('mode change complete', result)
      return this.getMode()
    })
    .then((result) => {
      let params = {
        title: '모드 변경 완료',
        content: `${(options.mode === 'cloud')?'클라우드':'로컬'} 모드로 변경되었습니다.\n디바이스를 재시작합니다.`,
        onClosed: () => this.localApis.reboot()
      }
      this.modal.success(params)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      let params = {
        title: '모드 변경 실패',
        content: `모드 변경 중 오류가 발생했습니다.`,
        error: err.message,
      }

      switch(err.message) {
        case 'DEVICE_ERROR_SET_MODE_ALREADY_DONE':
        case 'ERROR_LAMBDA_CONTORL_DEVICE_SET_MODE_ALREADY_DONE':
          params.content = `이미 ${(options.mode === 'cloud')?'클라우드':'로컬'} 모드입니다.\n새로고침 후 다시 시도해 주세요.`
          break;
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
  
  toggleMode() {
    if (this.current === 'local' && this.cloudAllowed === 0) {
      console.error('cloud version is not allowed')
      let params = {
        title: '오류',
        content: '이 장비는 클라우드 모드를 사용할 수 없습니다.\n관리자에게 문의하세요.',
        error: err.message
      }
      
      this.modal.error(params)
      return;
    }
    let mode = (this.current === 'cloud')?'로컬':'클라우드'
    let params = {
      title: '모드 변경',
      content: `${mode}모드로 변경합니다.\n계속하시겠습니까?`,
      warning: '기기 내 모든 데이터가 삭제됩니다.\n변경 완료 후 디바이스가 재시작됩니다.',
      onConfirm: () => this.change()
    }
    this.modal.confirm(params)
  }
  
  // TODO: Can click current mode before fetch mode
  getMode() {
    return this.localApis.getMode()
    .then(result => {
      this.current = result.mode;
      this.cloudAllowed = result.cloudAllowed;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('get mode', err)
      throw err;
    })
  }
  
  getStatus() {
    return this.network.getStatus()
    .then(result => {
      if (!result.internet) {
        this.$state.go('mode.disconnected')
        this.spinner.off()
        throw new Error('network disconnected')
      }
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      throw err;
    })
  }
  
}

export default Controller;
