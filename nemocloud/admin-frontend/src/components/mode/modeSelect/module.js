import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'modeSelect';

let module = angular.module(name, [
	uiRouter,
	'app.commons.services.localApis',
	'app.commons.services.network',
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('mode.select', {
			url: '/select',
			component: name
		});
}