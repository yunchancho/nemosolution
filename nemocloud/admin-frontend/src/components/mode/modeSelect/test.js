/// #if SNOW_MODE=='local'
import modeSelectModule from './module'
import modeSelectComponent from './component';
import modeSelectController from './controller';
import modeSelectTemplate from './template.html';

describe('modeSelect', () => {
  let $rootScope, makeController;

  beforeEach(window.module(modeSelectModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new modeSelectController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('localApis');
      expect(controller).to.have.property('network');
      expect(controller).to.have.property('current');
      expect(controller).to.have.property('cloudAllowed');

    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(modeSelectTemplate).to.match(/장비 전환 모드/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = modeSelectComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(modeSelectTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(modeSelectController);
    });
  });
});
/// #endif