import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import modeDisconnected from './modeDisconnected/module';
import modeSelect from './modeSelect/module';

// Used after implements "mode change with keeping config"
// import modeConfig from './modeConfig/module';

const name = 'modeChange';

let module = angular.module(name, [
	uiRouter,
	// modeConfig,
	modeDisconnected,
	modeSelect,
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('mode', {
			url: '/mode',
			component: name
		});
}
