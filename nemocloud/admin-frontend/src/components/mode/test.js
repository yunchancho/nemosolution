/// #if SNOW_MODE=='local'
import modeModule from './module'
import modeComponent from './component';
import modeController from './controller';
import modeTemplate from './template.html';

describe('mode', () => {
  let $rootScope, makeController;

  beforeEach(window.module(modeModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new modeController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('options');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(modeTemplate).to.match(/장비 전환 모드/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = modeComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(modeTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(modeController);
    });
  });
});
/// #endif