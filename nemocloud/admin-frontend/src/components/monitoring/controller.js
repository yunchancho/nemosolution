class Controller {
  constructor($rootScope) {
    'ngInject'
    this.$rootScope = $rootScope
    this.breadcrumbs = []
    this.resolve = {
      swKey: '',
      device: null,
      setBreadcrumb: (title) => this.setBreadcrumb(title)
    }
    //TODO : work a round solution
    $("body").removeClass("mini-navbar");
    $(".collapse").removeClass("in");
    $(".nav > li").removeClass("active");
  }

  setBreadcrumb(title) {
    for (let i = 0; i < this.breadcrumbs.length; i++) {
      if (this.breadcrumbs[i] === title) {
        this.breadcrumbs.splice(i + 1, Number.MAX_SAFE_INTEGER);
        return;
      }
    }
    this.breadcrumbs.push(title);
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
}

export default Controller;
