import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import monitorDevice from './monitorDevice/module';
import monitorSelectDevice from './monitorSelectDevice/module'

const name = 'monitoring';

let module = angular.module(name, [
	uiRouter,
	monitorDevice,
	monitorSelectDevice
])
	.config(config)
	.component(name, component)
	.name

export default module; 

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/monitoring',
			component: name
		});
}
