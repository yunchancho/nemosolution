import graphs from './graphs'

class Controller {
  constructor($rootScope, $state){
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.resolve;
    this.graphs = []
  }

  $onInit() {
    if (!this.resolve.device) {
      this.$state.go('monitoring.selectDevice')
      return
    }
    this.resolve.setBreadcrumb('실시간 모니터링')
    this.setSwKey()
  }
  
  $onDestroy() {
    this.resolve.device = null
  }
  
  setSwKey() {
    this.graphs = JSON.parse(JSON.stringify(graphs)).map(item => {
      item.params.push({ key: 'swKey', value: this.resolve.device.name })
      return item
    })
  }
}

export default Controller;
