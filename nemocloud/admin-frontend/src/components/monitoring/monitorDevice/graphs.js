// To use dynamic parameter, insert "__key__" inside link and
// insert {key, value} object in params
// ex) link: 'base.com/?swKey=__swKey__'
//     params: [{key: 'swKey', value: 'device'}]
//     result url: 'base.com/?swKey=device'
export default [
  {
    title: 'CPU코어 갯수 대비 실시간 사용 평균',
    content: 'CPU 코어 갯수에 비례하여 현재 CPU사용비중이 얼마인지 나타냅니다.',
    tooltip: 'CPU 코어 1개당 CPU 사용비중은 1을 넘어서면 위험합니다.',
    params: [],
    link: `https://search-nemosnow-monitor-xpxg3folghbwv4id6kvvr7xorq.ap-northeast-2.es.amazonaws.com/_plugin/kibana/app/kibana#/visualize/edit/realtime_cpu_avg_load?embed=true&_g=()&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'*')),uiState:(),vis:(aggs:!((enabled:!t,id:'2',params:(customInterval:'2h',extended_bounds:(),field:time,interval:m,min_doc_count:1),schema:segment,type:date_histogram),(enabled:!t,id:'3',params:(field:cpuCoreNum),schema:metric,type:max),(enabled:!t,id:'4',params:(filters:!(('$$hashKey':'object:1082',input:(query:(query_string:(analyze_wildcard:!t,query:'swKey:__swKey__'))),label:''))),schema:group,type:filters),(enabled:!t,id:'1',params:(field:cpuAvgLoad),schema:metric,type:max)),listeners:(),params:(addLegend:!t,addTimeMarker:!t,addTooltip:!t,defaultYExtents:!f,interpolate:linear,legendPosition:bottom,mode:overlap,scale:linear,setYExtents:!t,shareYAxis:!t,smoothLines:!f,times:!(),yAxis:(max:5,min:0)),title:realtime_cpu_avg_load,type:area))`
  },
  {
    title: '기기 실시간 온도 체크',
    content: '기기의 온도를 체크합니다.',
    tooltip: '기기가 너무 뜨거워지면 문제가 생길수있습니다.',
    params: [],
    link: `https://search-nemosnow-monitor-xpxg3folghbwv4id6kvvr7xorq.ap-northeast-2.es.amazonaws.com/_plugin/kibana/app/kibana#/visualize/edit/realtime_cpu_temperature?embed=true&_g=()&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'*')),uiState:(),vis:(aggs:!((enabled:!t,id:'2',params:(customInterval:'2h',extended_bounds:(),field:time,interval:m,min_doc_count:1),schema:segment,type:date_histogram),(enabled:!t,id:'3',params:(field:cpuMaxTemp),schema:metric,type:max),(enabled:!t,id:'1',params:(field:cpuMainTemp),schema:metric,type:max),(enabled:!t,id:'4',params:(filters:!(('$$hashKey':'object:1671',input:(query:(query_string:(analyze_wildcard:!t,query:'swKey:__swKey__'))),label:''))),schema:group,type:filters)),listeners:(),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,defaultYExtents:!f,interpolate:linear,legendPosition:bottom,mode:overlap,scale:linear,setYExtents:!t,shareYAxis:!t,smoothLines:!f,times:!(),yAxis:(max:100,min:0)),title:realtime_cpu_temperature,type:area))`
  },
  {
    title: '실시간 디스크 사용량',
    content: '실시간 디스크 사용량이 얼마인지 나타냅니다.',
    tooltip: 'diskWioSec는 초당 디스크를 얼마나 썼는지를, diskRioSec는 초당 디스크를 얼마나 읽었는지를 나타냅니다.',
    params: [],
    link: `https://search-nemosnow-monitor-xpxg3folghbwv4id6kvvr7xorq.ap-northeast-2.es.amazonaws.com/_plugin/kibana/app/kibana#/visualize/edit/realtime_disk_load?embed=true&_g=()&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'*')),uiState:(),vis:(aggs:!((enabled:!t,id:'1',params:(field:diskRioSec),schema:metric,type:max),(enabled:!t,id:'2',params:(customInterval:'2h',extended_bounds:(),field:time,interval:m,min_doc_count:1),schema:segment,type:date_histogram),(enabled:!t,id:'3',params:(field:diskWioSec),schema:metric,type:max),(enabled:!t,id:'4',params:(filters:!(('$$hashKey':'object:2072',input:(query:(query_string:(analyze_wildcard:!t,query:'swKey:__swKey__'))),label:''))),schema:group,type:filters)),listeners:(),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,defaultYExtents:!f,drawLinesBetweenPoints:!t,interpolate:linear,legendPosition:bottom,radiusRatio:9,scale:linear,setYExtents:!f,shareYAxis:!t,showCircles:!t,smoothLines:!f,times:!(),yAxis:()),title:realtime_disk_load,type:line))`
  },
  {
    title: '실시간 메모리 사용량',
    content: '메모리 총량과 메모리 사용량을 표시합니다.',
    tooltip: '메모리 총량과 비교하여 관리하십시오',
    params: [],
    link: `https://search-nemosnow-monitor-xpxg3folghbwv4id6kvvr7xorq.ap-northeast-2.es.amazonaws.com/_plugin/kibana/app/kibana#/visualize/edit/realtime_mem_load?embed=true&_g=()&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'*')),uiState:(),vis:(aggs:!((enabled:!t,id:'3',params:(customInterval:'2h',extended_bounds:(),field:time,interval:auto,min_doc_count:1),schema:segment,type:date_histogram),(enabled:!t,id:'4',params:(filters:!(('$$hashKey':'object:2576',input:(query:(query_string:(analyze_wildcard:!t,query:'swKey:__swKey__'))),label:''))),schema:group,type:filters),(enabled:!t,id:'1',params:(field:memTotal,percents:!(50)),schema:metric,type:median),(enabled:!t,id:'2',params:(field:memUsed,percents:!(50)),schema:metric,type:median)),listeners:(),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,defaultYExtents:!f,interpolate:linear,legendPosition:bottom,mode:overlap,orderBucketsBySum:!f,scale:linear,setYExtents:!f,shareYAxis:!t,smoothLines:!f,times:!(),yAxis:()),title:realtime_mem_load,type:area))`
  },

  {
    title: '실시간 네트워크 사용량',
    content: '현재 네트워크를 얼마나 많이 사용하는지 표시합니다.',
    tooltip: 'networkRxSec는 ... networkTxSec는 ...',
    params: [],
    link: `https://search-nemosnow-monitor-xpxg3folghbwv4id6kvvr7xorq.ap-northeast-2.es.amazonaws.com/_plugin/kibana/app/kibana#/visualize/edit/realtime_network?embed=true&_g=()&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'*')),uiState:(),vis:(aggs:!((enabled:!t,id:'1',params:(field:networkRxSec),schema:metric,type:max),(enabled:!t,id:'2',params:(customInterval:'2h',extended_bounds:(),field:time,interval:m,min_doc_count:1),schema:segment,type:date_histogram),(enabled:!t,id:'3',params:(field:networkTxSec),schema:metric,type:max),(enabled:!t,id:'4',params:(filters:!(('$$hashKey':'object:2956',input:(query:(query_string:(analyze_wildcard:!t,query:'swKey:__swKey__'))),label:''))),schema:group,type:filters)),listeners:(),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,defaultYExtents:!f,drawLinesBetweenPoints:!t,interpolate:linear,legendPosition:bottom,radiusRatio:9,scale:linear,setYExtents:!f,shareYAxis:!t,showCircles:!t,smoothLines:!f,times:!(),yAxis:()),title:realtime_network,type:line))`
  }
]
