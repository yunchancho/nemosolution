import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import monitorFrame from './monitorFrame/module'
import monitorInfo from './monitorInfo/module'

const name = 'monitorDevice';

let module = angular.module(name, [ 
	uiRouter,
	monitorFrame,
	monitorInfo
	])
	.config(config)
	.component(name, component)
	.name

export default module; 

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('monitoring.device', {
			url: '/device',
			component: name
		});
}
