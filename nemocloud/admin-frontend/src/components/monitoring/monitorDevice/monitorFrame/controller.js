class Controller {
  constructor($rootScope, $sce, $state){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$sce = $sce;
    this.$state = $state;
    this.options = {
      link: '',
      title: '',
      content: '',
      tooltip: '',
      params: [] // {key, value} array
    }
    this.url = ''
  }

  $onInit() {
    this.setParams();
  }

  setParams() {
    let url = this.options.link
    for (let param of this.options.params) {
      url = url.replace(`__${param.key}__`, param.value)
    }
    //// remove remain parameter
    // url = url.replace(/__[a-zA-z]+__/, '')
    this.url = url
    console.log('url', {url: this.url})
  }

  trust(url) {
    return this.$sce.trustAsResourceUrl(url);
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
}

export default Controller;
