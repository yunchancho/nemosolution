import monitorFrameModule from './module'
import monitorFrameComponent from './component';
import monitorFrameController from './controller';
import monitorFrameTemplate from './template.html';

describe('monitorFrame', () => {
  let $rootScope, $sce, $state, makeController;

  beforeEach(window.module(monitorFrameModule));
  beforeEach(inject((_$rootScope_,_$sce_, _$state_) => {
    $rootScope = _$rootScope_;
    $sce = _$sce_;
    $state = _$state_;
    makeController = () => {
      return new monitorFrameController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller.options).to.have.property('link');
      expect(controller.options).to.have.property('title');
      expect(controller.options).to.have.property('content');
      expect(controller).to.have.property('url');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(monitorFrameTemplate).to.match(/기존 설정 유지 여부/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = monitorFrameComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(monitorFrameTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(monitorFrameController);
    });
  });
});
