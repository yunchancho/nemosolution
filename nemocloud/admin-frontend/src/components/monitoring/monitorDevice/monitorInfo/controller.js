import FileTransfer from '../../../../lib/fileTransfer/index'

class Controller {
  constructor($rootScope, $state, $uibModal, cognito, s3, spinner, modal){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.cognito = cognito;
    this.s3 = s3;
    this.spinner = spinner;
    this.modal = modal;
    this.resolve;
    this.canvasStyle = {}
    this.device;
    this.errorMessage = '이미지를 가져오는 중입니다.'
    // this.imageObject
    // this.image
    // this.date
    this.screenshots = [];
  }

  $onInit() {
    this.device = this.resolve.device;
    if (this.device) {
      this.fetch()
    }

    $(window).bind("resize", () => this.calculate())
  }

  $onDestroy() {
    $(window).unbind("resize", () => this.calculate())
  }

  _arrayBufferToBase64(buffer) {
    let binary = '';
    let bytes = new Uint8Array( buffer );
    for (let i = 0; i < bytes.byteLength; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  calculate() {
    let width = 0;
    let height = 0;
    let canvasWidth = angular.element(document.getElementById('images'))[0].clientWidth;

    if (this.screenshots.length === 0) {
      this.canvasStyle.height = 0;
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return;
    }

    // get canvas size
    for (let screenshot of this.screenshots) {
      let thumbnail = screenshot.thumbnail;
      if (width < thumbnail.x + thumbnail.width) {
        width = thumbnail.x + thumbnail.width;
      }
      if (height < thumbnail.y + thumbnail.height) {
        height = thumbnail.y + thumbnail.height;
      }
    }

    this.canvasStyle.height = height

    let ratio = 1
    if (width > canvasWidth) {
      ratio = canvasWidth / width;
    }

    for (let screenshot of this.screenshots) {
      let thumbnail = screenshot.thumbnail;
      thumbnail.style.position = 'absolute';
      thumbnail.style.width = `${thumbnail.width * ratio}px`;
      thumbnail.style.left = `${thumbnail.x * ratio}px`;
      thumbnail.style.top = `${thumbnail.y * ratio}px`;
    }

    this.$rootScope.$$phase || this.$rootScope.$apply();
    // console.log(angular.element(document.getElementById('ibox')))
    // console.log(angular.element(document.getElementById('ibox'))[0].clientWidth)
    // console.log('thumbnail', angular.element(document.getElementById('images'))[0].children)
  }

  _parseUrl(url) {
    // https://s3.ap-northeast-2.amazonaws.com/<bucket>/<customer>/<swkey>/screenshots/<file>
    let bucket = url.split('/')[3];
    let key = url.slice(url.indexOf('/', url.indexOf(bucket) + 1) + 1)
    return [bucket, key];
  }

  // bigImage() {
  //   let modal = this.$uibModal.open({
  //     component: 'screenshotModal',
  //     resolve: {
  //       download: () => {
  //         return () => this.download()
  //       },
  //       image: () => this.image
  //     },
  //     size: 'lg',
  //   })
  //
  //   return modal
  // }

  // download() {
  //   if (!this.imageObject) {
  //     return;
  //   }
  //   this.s3.downloadFile(this.s3.getConfig().backupsBucket, this.imageObject)
  // }

  // fetch() {
  //   let bucket = this.s3.getConfig().backupsBucket;
  //   let prefix = ''
  //
  //   this.spinner.on()
  //   return this.cognito.getCustomer()
  //   .then(customer => {
  //     prefix = `${customer}/${this.device.name}/${this.s3.getConfig().directory.backup.screenshots}/`
  //     console.log('screenshot prefix', prefix)
  //     return this.s3.listDirectoryObjects(bucket, prefix)
  //   })
  //   .catch()
  //   .then(result => {
  //     console.log('screenshots', result)
  //     if (result.Contents.length === 0) {
  //       throw new Error('no screenshots');
  //     }
  //
  //     let latest;
  //     let latestDate = '0'
  //     let contentDate;
  //     for (let content of result.Contents) {
  //       // Remove prefix, file extention('.png') and special charactors
  //       // contentDate: 14 charactors date string (ex: 19700101120000)
  //       contentDate = content.Key
  //                     .slice(content.Key.lastIndexOf('_') + 1, content.Key.lastIndexOf('.'))
  //                     .replace('-', '')
  //       if (parseInt(contentDate, 10) > parseInt(latestDate, 10)) {
  //         latest = content
  //         latestDate = contentDate
  //       }
  //     }
  //     this.date = latestDate
  //     this.imageObject = latest
  //     console.log('screenshot key', latest.Key)
  //     return this.s3.getObject(bucket, latest.Key)
  //   })
  //   .then(result => {
  //     this.image = `data:image/PNG;base64,${this._arrayBufferToBase64(result.Body)}`
  //     this.$rootScope.$$phase || this.$rootScope.$apply();
  //   })
  //   .catch(err => {
  //     this.imageObject = null
  //     this.image = null;
  //     this.date = ''
  //     if (err.message === 'no screenshots') {
  //       return;
  //     }
  //     console.error('get screenshot error', err)
  //     let params = {
  //       title: '스크린샷 오류',
  //       content: '디바이스 화면을 가져오던 도중 오류가 발생했습니다.',
  //       error: err.message
  //     }
  //     this.modal.error(params)
  //   })
  //   .then(() => this.spinner.off())
  // }

  fetch() {
    this.device.getScreenshot()
    .then(result => {
      this.screenshots = result;

      // For test
      this.screenshots[0].screenshot.url = 'https://s3.ap-northeast-2.amazonaws.com/nemosnow-backups/nemoux/nemoux_aaa_000001/screenshots/screen0_20170412-092011.png'
      this.screenshots[0].thumbnail.url = 'https://s3.ap-northeast-2.amazonaws.com/nemosnow-backups/nemoux/nemoux_aaa_000001/screenshots/thumbnails/screen0_20170412-092011.png'
      this.screenshots[1].screenshot.url = 'https://s3.ap-northeast-2.amazonaws.com/nemosnow-backups/nemoux/nemoux_aaa_000001/screenshots/screen1_20170412-092011.png'
      this.screenshots[1].thumbnail.url = 'https://s3.ap-northeast-2.amazonaws.com/nemosnow-backups/nemoux/nemoux_aaa_000001/screenshots/thumbnails/screen1_20170412-092011.png'
      this.screenshots[1].screenshot.url = 'https://s3.ap-northeast-2.amazonaws.com/nemosnow-backups/nemoux/nemoux_aaa_000001/screenshots/screen1_20170412-092011.png'
      this.screenshots[1].thumbnail.url = 'https://s3.ap-northeast-2.amazonaws.com/nemosnow-backups/nemoux/nemoux_aaa_000001/screenshots/thumbnails/screen1_20170412-092011.png'
      this.screenshots.push(JSON.parse(JSON.stringify(this.screenshots[0])))
      this.screenshots[2].thumbnail.x = 192;
      this.screenshots[2].thumbnail.y = 108;
      this.screenshots.push(JSON.parse(JSON.stringify(this.screenshots[0])))
      this.screenshots[3].thumbnail.x = 96;
      this.screenshots[3].thumbnail.y = 216;
      this.screenshots[3].thumbnail.width = 240;
      this.screenshots[3].thumbnail.height = 135;
      this.screenshots.push(JSON.parse(JSON.stringify(this.screenshots[0])))
      this.screenshots[4].thumbnail.x = 0;
      this.screenshots[4].thumbnail.y = 108;
      this.screenshots[4].thumbnail.width = 156;
      this.screenshots[4].thumbnail.height = 81;


      console.log('screenshot: ', this.screenshots)
      let requests = []
      for (let screenshot of this.screenshots) {
        let bucket, key;
        [bucket, key] = this._parseUrl(screenshot.thumbnail.url)
        console.log(bucket, key)
        let prom = this.s3.getObject(bucket, key)
        .then(result => {
          screenshot.thumbnail.image = `data:image/PNG;base64,${this._arrayBufferToBase64(result.Body)}`
        })
        requests.push(prom)
      }

      return Promise.all(requests)
    })
    .then(result => {
      console.log('screenshot result', this.screenshots)
      this.calculate()
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('screenshot: ', err)
      this.screenshots = []
      this.calculate()
      this.errorMessage = '이미지를 가져올 수 없습니다.';
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
  }

  openImage(screenshot) {
    // Open clicked image in new tab
    let prom;
    console.log('selected screenshot', screenshot)
    if (screenshot.image) {
      prom = Promise.resolve()
    } else {
      let bucket, key;
      [bucket, key] = this._parseUrl(screenshot.url)
      prom = this.s3.getObject(bucket, key)
      .then(result => {
        screenshot.image = `data:image/PNG;base64,${this._arrayBufferToBase64(result.Body)}`
        // return new Promise((resolve, reject) => setTimeout(resolve, 1000))
      })
    }

    prom.then(() => {
      let image = new Image();
      image.src = screenshot.image;
      let w = window.open("",'Image')
      w.document.write(image.outerHTML)
      // window.open(screenshot.image,'Image','resizable=1');
    })
    .catch(err => {
      console.error('get original image error', err)
    })
  }

}

export default Controller;
