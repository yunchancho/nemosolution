import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import screenshotModal from './screenshotModal/module'

const name = 'monitorInfo';

let module = angular.module(name, [ 
	uiRouter,
	screenshotModal
	])
	.component(name, component)
	.name

export default module; 