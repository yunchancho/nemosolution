class Controller {
  constructor($state){
    'ngInject'
    this.$state = $state;
    this.resolve;
    this.image;
  }

  $onInit() {
    this.image = this.resolve.image
  }

  download() {
    this.resolve.download()
  }
}

export default Controller;
