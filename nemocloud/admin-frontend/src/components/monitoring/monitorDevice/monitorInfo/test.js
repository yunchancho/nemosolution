import monitorInfoModule from './module'
import monitorInfoComponent from './component';
import monitorInfoController from './controller';
import monitorInfoTemplate from './template.html';

describe('monitorInfo', () => {
  let $rootScope, $state, makeController;

  beforeEach(window.module(monitorInfoModule));
  beforeEach(inject((_$rootScope_,_$state_) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    makeController = () => {
      return new monitorInfoController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(monitorInfoTemplate).to.match(/cpu 실시간 사용량/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = monitorInfoComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(monitorInfoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(monitorInfoController);
    });
  });
});
