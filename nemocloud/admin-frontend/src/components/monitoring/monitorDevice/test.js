import monitorDeviceModule from './module'
import monitorDeviceComponent from './component';
import monitorDeviceController from './controller';
import monitorDeviceTemplate from './template.html';

describe('monitorDevice', () => {
  let $rootScope, $state, makeController;

  beforeEach(window.module(monitorDeviceModule));
  beforeEach(inject((_$rootScope_,_$state_) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    makeController = () => {
      return new monitorDeviceController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('graphs');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(monitorDeviceTemplate).to.match(/cpu 실시간 사용량/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = monitorDeviceComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(monitorDeviceTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(monitorDeviceController);
    });
  });
});
