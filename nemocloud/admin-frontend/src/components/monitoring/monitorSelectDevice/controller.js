class Controller {
  constructor($rootScope, $state, deviceManager, modal, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state
    this.deviceManager = deviceManager;
    this.modal = modal;
    this.spinner = spinner;
    this.deviceList = []
    this.resolve;
  }

  $onInit() {
    this.getDeviceList()
    this.resolve.setBreadcrumb('모니터링 기기 선택')
  }

  select(device) {
    console.log(device)
    this.resolve.device = device;
    this.$state.go('monitoring.device')
  }

  getDeviceList() {
    this.spinner.on()
    this.deviceManager.getDeviceList()
    .then(deviceList => {
      this.deviceList = deviceList;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      let params = {
        title: '불러오기 실패',
        content: '디바이스 목록을 가져오던 도중 오류가 발생했습니다.',
        error : err.message
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
