import monitorSelectDeviceModule from './module'
import monitorSelectDeviceComponent from './component';
import monitorSelectDeviceController from './controller';
import monitorSelectDeviceTemplate from './template.html';

describe('monitorSelectDevice', () => {
  let $rootScope, $state,makeController;

  beforeEach(window.module(monitorSelectDeviceModule));
  beforeEach(inject((_$rootScope_,_$state_) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    makeController = () => {
      return new monitorSelectDeviceController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('deviceList');
      expect(controller).to.have.property('deviceManager');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(monitorSelectDeviceTemplate).to.match(/기기 선택/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = monitorSelectDeviceComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(monitorSelectDeviceTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(monitorSelectDeviceController);
    });
  });
});
