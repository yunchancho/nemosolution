class Controller {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, $state){
    'ngInject'
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, $state, localApis,){
    'ngInject'
    this.localApis = localApis;
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, $state, localApis ){
    'ngInject'
    this.localApis = localApis;
  /// #endif
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.breadcrumbs = []
    this.resolve = {
      mode: '',
      index: '',
      pkg: {},
      setBreadcrumb: (title, set) => this.setBreadcrumb(title, set),
      listPackages: null
    }
    //TODO : work a round solution
    $("body").removeClass("mini-navbar");
    $(".collapse").removeClass("in");
    $(".nav > li").removeClass("active");
  }
  $onInIt(){
    this.message = this.resolve.message;
  }
  setBreadcrumb(title, set=false) {
    if (set) {
      this.breadcrumbs = [title]
    }
    for (let i = 0; i < this.breadcrumbs.length; i++) {
      if (this.breadcrumbs[i] === title) {
        this.breadcrumbs.splice(i + 1, Number.MAX_SAFE_INTEGER);
        return;
      }
    }
    this.breadcrumbs.push(title);
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
}

export default Controller;
