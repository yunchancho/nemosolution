import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import pkgDetail from './pkgDetail/module';
import pkgList from './pkgList/module';
import pkgSelectIndex from './pkgSelectIndex/module';
import updatePkgConfig from './updatePkgConfig/module'
/// #if SNOW_MODE=='usb'
import pkgUpload from './pkgUpload/module'
/// #endif
let subModule = [
	uiRouter,
	pkgDetail,
	pkgList,
	pkgSelectIndex,
	updatePkgConfig,
	/// #if SNOW_MODE=='usb'
	pkgUpload
	/// #endif
]

const name = 'pkg';

let module = angular.module(name, subModule)
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/pkg',
			component: name
		});
}
