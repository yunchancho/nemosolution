class Controller {
  constructor($rootScope, $state, packages){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.packages = packages;
    this.pkg;
    this.slides=[];
    this.active = 0;
  }

  $onInit() {
    for(let screenshot of this.pkg.Screenshots){
      let req ={}
      req.src = screenshot
      this.slides.push(req);
    }

    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
}

export default Controller;
