import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';


const name = 'pkgDetail';

let module = angular.module(name, [ 
	uiRouter,
	'app.commons.services.packages',
	])
	.component(name, component)
	.name

export default module;