import testModule from './module'
import testComponent from './component';
import testController from './controller';
import testTemplate from './template.html';

describe('Package Detail', () => {
  let $componentController;

  beforeEach(window.module('app'));
  beforeEach(inject((_$componentController_) => {
    $componentController = _$componentController_;
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    let controller;
    
    beforeEach(function() {
      this.timeout(15000)
      let bindings = {
        pkg: meta.pkg
      }
      controller = $componentController(testModule, null, bindings);
    });
    
    it('has properties', () => {
      expect(controller).to.have.property('$rootScope');
      expect(controller).to.have.property('$state');
      expect(controller).to.have.property('packages');
      expect(controller).to.have.property('pkg');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(testTemplate).to.match(/기기 현황/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = testComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(testTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(testController);
    });
  });
});
