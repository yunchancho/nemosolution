class Controller {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, $state, $uibModal, s3, deviceManager, modal, logs, spinner, toastr, packages){
    'ngInject'
    this.logs = logs
    this.s3 = s3;
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, $state, $uibModal, deviceManager, modal, spinner, toastr, packages, localApis){
    'ngInject'
    this.localApis = localApis;
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, $state, $uibModal, deviceManager, dynamoDB, modal, spinner, toastr, packages, localApis ){
    'ngInject'
    this.localApis = localApis;
    this.dynamoDB = dynamoDB;
  /// #endif
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.deviceManager = deviceManager;
    this.modal = modal;
    this.spinner = spinner;
    this.toastr = toastr;
    this.packages = packages;
    this.resolve;
    this.deviceList = [];
    this.filterInstall = false;
    this.pkgs = [];
    this.query = '';
    this.metadataPrefix=[];
  }

  $onInit() {
    this.resolve.listPackages = () => this.listPackages();
  }

  _arrayBufferToBase64(buffer) {
    let binary = '';
    let bytes = new Uint8Array( buffer );
    for (let i = 0; i < bytes.byteLength; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  checkInstalled(pkg) {
    pkg.view.installed = this.deviceList[0].checkInstalled(pkg)
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }

  checkInstalledAll() {
    for (let pkg of this.pkgs) {
      this.checkInstalled(pkg)
    }
  }

  confirmUninstall(pkg) {
    let params = {
      title: '패키지 삭제',
      content: '패키지를 삭제하시겠습니까?\n장비 재시작 후 삭제가 완료됩니다.',
      onConfirm: () => this.uninstall(pkg)
    }

    this.modal.confirm(params)
  }

  getDevice() {
    return this.deviceManager.getDeviceList()
    .then(deviceList => {
      this.deviceList = deviceList;
      console.log('Devices:', this.deviceList)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('Get device fails', err);
      throw err
    })
  }

  getIndex() {
    /// #if SNOW_MODE=='cloud'
    return this.packages.getPackageIndex(this.resolve.index)
    /// #endif
    /// #if SNOW_MODE=='local'
    return this.localApis.getPackageIndex(this.resolve.index)
    /// #endif
    /// #if SNOW_MODE=='usb'
    return this.packages.getPackageIndex(this.resolve.index)
    /// #endif
    .then(result => {
      this.pkgs = this.packages.parsePackageIndex(result);
      console.log("this.pkgs : ",this.pkgs);
      // TODO: Move to service
      for (let pkg of this.pkgs) {
        pkg.view = {
          installed: '',
          mandatory: false,
          maintainer: {
            name: pkg.Maintainer.slice(0, pkg.Maintainer.indexOf('<') - 1),
          }
        }

        let pkgName = pkg.Package.toLowerCase()
        // check common mandatory package
        if (this.packages.isMandatoryPkgs(pkgName)) {
          pkg.view.mandatory = true;
        }
        // check each device's mandatory package
        for (let mandatory of this.deviceList[0].mandatorypkgs) {
          if (mandatory.pkgname === pkgName) {
            pkg.view.mandatory = true;
            break;
          }
        }
      }
      console.log('Packages:', this.pkgs)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .then(() => { this.checkInstalledAll() })
  }

  getMetadataIdex(){
    return this.pkgs.map( obj => {
      obj.Screenshots =[];
      obj.icon = `https://s3.ap-northeast-2.amazonaws.com/nemosnow-packages/metadata/${obj.Package}/icons/icon_128x128.png`
      obj.Screenshots.push(`https://s3.ap-northeast-2.amazonaws.com/nemosnow-packages/metadata/${obj.Package}/screenshots/screenshot_01.jpg`)
      obj.Screenshots.push(`https://s3.ap-northeast-2.amazonaws.com/nemosnow-packages/metadata/${obj.Package}/screenshots/screenshot_02.jpg`)
      obj.Screenshots.push(`https://s3.ap-northeast-2.amazonaws.com/nemosnow-packages/metadata/${obj.Package}/screenshots/screenshot_03.jpg`)
    })
  }

  install(pkg) {
    this.spinner.on()
    return this.deviceManager.installPackage(this.deviceList[0], pkg, {name: this.resolve.index})
    .then((result) => {
      pkg.view.installed = "installing"
      this.checkInstalled(pkg);
      this.toastr.success('패키지 설치 설정이 완료되었습니다.</br>설치는 장비 재시작 후 완료됩니다.')
      console.log('install package', result);
    })
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '패키지',
        target: pkg.Package,
        ops: 'install'
      }
      return this.logs.success(params)
    })
    /// #endif
    .catch((err) => {
      console.error('install package fail', err);
      let params = {
        title: '패키지 설치 실패',
        content: '패키지 설치 중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => {
      this.spinner.off()
    })
  }

  listPackages() {
    this.spinner.on()
    let prom;
    if (this.deviceList.length === 0) {
      prom = this.getDevice()
    } else {
      prom = Promise.resolve()
    }
    return prom
    .then(() => this.getIndex())
    .then(()=> this.getMetadataIdex())
    .catch(err => {
      console.error('get package fails', err.message)
      let params = {
        title: '패키지',
        content: '패키지 리스트를 불러올 수 없습니다.',
        error: err.message
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  select(pkg) {
    if (this.selected === pkg) {
      this.selected = null;
    } else {
      this.selected = pkg
    }
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }

  updatePackageConfig(pkg) {
    this.resolve.pkg = pkg;
    this.$state.go('pkg.config')
  }

  uninstall(pkg) {
    this.spinner.on()
    return this.deviceManager.uninstallPackage(this.deviceList[0], pkg)
    .then((result) => {
      pkg.view.installed = "not installed"
      this.checkInstalled(pkg);
      this.toastr.success('패키지 삭제 설정이 완료되었습니다.</br>삭제는 장비 재시작 후 완료됩니다.')
      console.log('uninstall Package', result);
    })
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '패키지',
        target: pkg.Package,
        ops: 'uninstall'
      }
      return this.logs.success(params)
    })
    /// #endif
    .catch((err) => {
      console.error('uninstall Package', err);
      let params = {
        title: '패키지 삭제 실패',
        content: '패키지 삭제 중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => {
      this.spinner.off()
    })
  }
}

export default Controller;
