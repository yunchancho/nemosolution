import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-toastr/dist/angular-toastr.min.css';
import 'angular-toastr/dist/angular-toastr.tpls.js';

import component from './component';

const name = 'pkgList';

let module = angular.module(name, [
	uiRouter,
	'toastr',
	'app.commons.services.packages',
	'app.commons.filters.package',
	])
	.component(name, component)
	.name

export default module;
