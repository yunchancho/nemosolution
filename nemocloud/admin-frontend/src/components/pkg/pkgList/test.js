import testModule from './module'
import testComponent from './component';
import testController from './controller';
import testTemplate from './template.html';

describe('Package List', () => {
  let $componentController, cognito;

  beforeEach(window.module('app'));
  beforeEach(inject((_$componentController_, _cognito_) => {
    $componentController = _$componentController_;
    cognito = _cognito_;
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    let controller;
    
    beforeEach(function() {
      this.timeout(20000)
      let bindings = {
        resolve: {
          mode: 'management',
          index: 'system',
          pkg: {},
          setBreadcrumb: (title, set) => {}
          // setBreadcrumb: (title, set) => this.setBreadcrumb(title, set)
        }
      }
      controller = $componentController(testModule, null, bindings);
      return cognito.login(meta.username, meta.password)
      .then(() => controller.deviceManager.getDeviceList())
      .then(deviceList => {
        controller.deviceList = deviceList;
      })
      .then(() => controller.$onInit())
      .then(() => null)
    });
    
    it('has properties', () => {
      expect(controller).to.have.property('resolve');
      expect(controller).to.have.property('deviceList');
      expect(controller).to.have.property('filterInstall');
      expect(controller).to.have.property('pkgs');
      expect(controller).to.have.property('query');
      
      expect(controller.deviceList).to.have.lengthOf(meta.cloudDevice.number);
    });
    
    it('list packages', () => {
      return controller.listPackages()
      .then(() => {
        expect(controller.pkgs).to.have.length.above(0);
        for (let pkg of controller.pkgs) {
          expect(pkg).to.have.property('Package')
          expect(pkg).to.have.property('Size')
          expect(pkg).to.have.property('Maintainer')
          expect(pkg).to.have.property('Version')
        }
      })
    })
    
    it('select package', () => {
      controller.pkgs[0] = meta.pkg;
      
      controller.select(controller.pkgs[0]);
      expect(controller.selected).to.equal(controller.pkgs[0]);
      
      controller.select(controller.pkgs[0]);
      expect(controller.selected).to.be.null;
    })
    
    it('install package', () => {
      controller.resolve.mode = 'store';
      return controller.install(meta.pkg)
      .then(() => {
        let hasPackage = false;
        for (let pkg of controller.deviceList[0].pkglist) {
          expect(pkg).to.have.property('pkgname');
          if (pkg.pkgname === meta.pkg.Package) {
            hasPackage = true;
          }
        }
        expect(hasPackage).to.equal(true);
        let pkg = JSON.parse(JSON.stringify(meta.pkg))
        controller.checkInstalled(pkg)
        expect(pkg.view.installed).to.equal('installed');
      })
    });
    
    it('uninstall package', function() {
      controller.resolve.mode = 'management';
      let hasTestPackage = false;
      for (let pkg of controller.deviceList[0].pkglist) {
        if (pkg.pkgname === meta.pkg.Package) {
          hasTestPackage = true
        }
      }
      if (!hasTestPackage) {
        // test package is not installed. must complete install test first
        this.skip()
        return;
      }
      
      return controller.uninstall(meta.pkg)
      .then(() => {
        for (let pkg of controller.deviceList[0].pkglist) {
          if (pkg.pkgname === meta.pkg.Package) {
            expect(pkg.pkgname).to.not.equal(meta.pkg.Package);
          }
        }
        let pkg = JSON.parse(JSON.stringify(meta.pkg))
        controller.checkInstalled(pkg)
        expect(pkg.view.installed).to.equal('not installed');
      })
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(testTemplate).to.match(/개발자/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = testComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(testTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(testController);
    });
  });
});
