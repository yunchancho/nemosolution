class Controller {
  constructor($rootScope, $state){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.resolve;
    this.active = 0;
    this.tabs = [
      { title:'플랫폼', value:'shell', index: 0},
      { title:'테마', value:'theme', index: 1},
      { title:'앱', value:'application', index: 2},
    ]
  }

  $onInit() {
    this.resolve.mode = this.$state.current.name.split('.')[1];
    this.resolve.setBreadcrumb(this.resolve.mode === 'store'?'스토어':'관리', true)

    // retrieve selected index or initiate
    if (this.resolve.index) {
      for (let i = 0; i < this.tabs.length; i++) {
        if (this.resolve.index === this.tabs[i].value) {
          this.active = i;
        }
      }
    } else {
      this.resolve.index = this.tabs[0].value
    }
  }

  select(index) {
    this.resolve.index = index;
    if (this.resolve.listPackages) {
      return this.resolve.listPackages()
    } else {
      return Promise.resolve()
    }
  }
}

export default Controller;
