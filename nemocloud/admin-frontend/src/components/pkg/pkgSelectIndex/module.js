import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'pkgSelectIndex';

let module = angular.module(name, [
	uiRouter,
])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('pkg.management', {
			url: '/management',
			component: name
		});
	$stateProvider
		.state('pkg.store', {
			url: '/store',
			component: name
		});
}
