import testModule from './module'
import testComponent from './component';
import testController from './controller';
import testTemplate from './template.html';

import pkgModule from '../module';
import pkgListModule from '../pkgList/module';

describe('Package Select Index', () => {
  let $componentController, $state, $timeout, cognito;

  beforeEach(window.module('app'));
  beforeEach(inject((_$componentController_, _$state_, _$timeout_, _cognito_) => {
    $componentController = _$componentController_;
    $state = _$state_;
    $timeout = _$timeout_;
    cognito = _cognito_;
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    let controller, pkgController, pkgListController;
    
    beforeEach(function() {
      this.timeout(15000)
      let bindings = {
        resolve: {
          mode: 'management',
          index: '',
          pkg: {},
          setBreadcrumb: null,
          listPackages: null
        }
      }
      // bind same object to each controller
      controller = $componentController(testModule, null, bindings)
      pkgController = $componentController(pkgModule, null, bindings)
      pkgListController = $componentController(pkgListModule, null, bindings)
      
      // binding other modules function
      bindings.resolve.setBreadcrumb = (title, set) => pkgController.setBreadcrumb(title, set);
      bindings.resolve.listPackages = () => pkgListController.listPackages();
      
      return cognito.login(meta.username, meta.password)
    });
    
    it('has properties', () => {
      expect(controller).to.have.property('$rootScope');
      expect(controller).to.have.property('$state');
      expect(controller).to.have.property('resolve');
      expect(controller).to.have.property('active');
      expect(controller).to.have.property('tabs');
    });
    
    it('init controller', () => {
      controller.resolve.mode = 'management'
      controller.resolve.index = null
      controller.$onInit()
      expect(controller.resolve.index).to.equal(controller.tabs[0].value);
      expect(pkgController.breadcrumbs).to.deep.equal(['관리']);
      
      controller.resolve.mode = 'store'
      controller.resolve.index = controller.tabs[0].value
      controller.$onInit()
      expect(controller.active).to.equal(controller.tabs[0].index);
      // TODO: test breadcrumbs after set $state.current.name
      // expect(pkgController.breadcrumbs).to.deep.equal(['스토어']);
    });
    
    it('select index tab', function() {
      let check = (index) => {
        return controller.select(controller.tabs[index].value)
        .then(() => {
          expect(pkgListController.pkgs).to.have.length.above(0);
          
          index++;
          if (index < controller.tabs.length) {
            // Test next tab
            return check(index);
          } else {
            // Test ends
            return null;
          }
        })
      }
      
      return check(0);
    })
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(testTemplate).to.match(/패키지 카테고리/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = testComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(testTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(testController);
    });
  });
});
