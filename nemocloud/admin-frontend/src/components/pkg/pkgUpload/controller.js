const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))

class Controller {
  constructor($rootScope, $state, $uibModal, modal, spinner, toastr, localApis){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.modal = modal;
    this.spinner = spinner;
    this.localApis = localApis;
    this.toastr = toastr
    this.pickedFiles;
    this.dest;
    this.directory;
    this.usbs=[]
    this.message='';
  }

  $onInit() {
    this.getUsbInfo()
  }

  getUsbInfo(){
    this.spinner.on()
    return this.localApis.getUsbList()
    .then(usbs => {
      this.usbs = usbs;
    })
    .then(() => {
      if(this.usbs.length > 0){
        this.directory = this.usbs[0].fs[0].mount
        this.$rootScope.$$phase || this.$rootScope.$apply();
      } else if(this.usbs.length === 0){
        return this.resolve.message = "usb가 연결되어 있지 않습니다."
      }
    })
    .catch(err => {
      this.error = true;
      console.error('Get fssize error: ', err)
    })
    .then(() => this.spinner.off())
  }

  upload(){
    this.spinner.on()
    this.pickedFiles.map(obj => {
      return fs.readFileAsync(obj.file.path)
      .then(result => {
        console.log(result);
        fs.writeFileAsync(this.directory+"/packages/"+this.dest+"/" + obj.file.name )
      })
      .then(result => {this.toastr.success('업로드에 성공했습니다.')})
      .then(result => {
        console.log(result);
        this.spinner.off()
      })
    })
  }
}

export default Controller;
