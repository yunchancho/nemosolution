import testModule from './module'
import testComponent from './component';
import testController from './controller';
import testTemplate from './template.html';

describe('pkg', () => {
  let $componentController;

  beforeEach(window.module('app'));
  beforeEach(inject((_$componentController_) => {
    $componentController = _$componentController_;
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    let controller;
    
    beforeEach(function() {
      this.timeout(15000)
      let bindings = {}
      controller = $componentController(testModule, null, bindings);
    });
    
    it('has properties', () => { // erase if removing this.name from the controller
      expect(controller).to.have.property('$rootScope');
      expect(controller).to.have.property('$state');
      expect(controller).to.have.property('breadcrumbs');
      expect(controller).to.have.property('resolve');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(testTemplate).to.match(/패키지/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = testComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(testTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(testController);
    });
  });
});
