class Controller {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, $state, $transitions, deviceManager, modal, logs, spinner, toastr){
    'ngInject'
    this.logs = logs;
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, $state, $transitions, deviceManager, modal, spinner, toastr){
    'ngInject'
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, $state, $transitions, deviceManager, modal, spinner, toastr){
    'ngInject'
  /// #endif
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$transitions = $transitions;
    this.deviceManager = deviceManager;
    this.modal = modal
    this.spinner = spinner;
    this.toastr = toastr;
    this.pkg;
    this.backFlag = false;
    this.backModalOpened = false;
    this.pkgConfig = {};
    this.options = {
      modes: ['tree', 'text']
    }
    this.deviceList = [];
    this.pickedFile = [];
    this.heading = {
      config: '설정',
      registry: '레지스트리',
      graph: '그래프'
    }
    this.onFileSelect = () => this.confirmLoadConfigFile();
  }

  $onInit() {
    if (!this.resolve.index
      || this.resolve.mode === 'store') {
      this.$state.go('pkg.management')
      return
    }
    this.resolve.setBreadcrumb('설정')
    // Listen browser back button click
    this.$transitions.onStart({from: 'pkg.config'}, (trans) => this.checkTransition(trans));
    this.pkg = this.resolve.pkg;
    return this.fetch();
  }

  _arrayBufferToString(buf) {
    return String.fromCharCode.apply(null, new Uint16Array(buf));
  }

  back() {
    window.history.back()
  }

  checkTransition(trans) {
    return new Promise((resolve, reject) => {
      if (this.backFlag) {
        return resolve();
      }
      if (this.backModalOpened) {
        return reject()
      }
      if (this.isChanged()) {
        let params = {
          title: '앱 설정',
          content: '변경사항을 취소하고 나가시겠습니까?',
          confirmButton: '나가기',
          cancelButton: '계속 수정',
          onConfirm: () => {
            this.backFlag = true;
            this.$state.go('pkg.management')
            reject()
          },
          onClosed: () => {
            this.backModalOpened = false;
            reject()
          }
        }

        this.backModalOpened = true;
        this.modal.confirm(params)
      } else {
        return resolve()
      }

    });
  }

  confirmLoadConfigFile() {
    let params = {
      title: '설정 불러오기',
      content: '파일을 불러오시겠습니까?',
      warning: '적용 시 기존 설정이 초기화됩니다.',
      onConfirm: () => this.loadConfigFile()
    }

    this.modal.confirm(params)
  }

  fetch() {
    this.spinner.on()
    return this.deviceManager.getDeviceList()
    .then(deviceList => {
      this.deviceList = deviceList;
      // for (let device of deviceList) {
      //   if (device.checkInstalled(this.pkg) === 'installed') {
      //     this.deviceList.push(device);
      //   }
      // }
      // console.log('Installed Device', this.deviceList);
      //
      // if (this.deviceList.length === 0) {
      //   console.log('There is no installed device');
      // }
      // this.$rootScope.$$phase || this.$rootScope.$apply();
      return null;
    })
    .then(() => this.getConfigure())
    .then(() => this.spinner.off())
  }

  getConfigure() {
    if (this.deviceList.length === 0) {
      let params = {
        title: '설정 실패',
        content: '설치된 디바이스가 없습니다.\n페이지를 새로고침 해 주세요.'
      }
      this.modal.error(params)
      return;
    }
    let pkgName = this.pkg.Package.toLowerCase();
    // default config
    this.pkgConfig = {
      registry: {},
      config: {},
      graph: {}
    };
    for (let pkg of this.deviceList[0].pkgConfig) {
      if (pkg.name === pkgName){
        // Copy configuration
        this.pkgConfig = JSON.parse(JSON.stringify(pkg.value));
        break;
      }
    }
    console.log('config:', this.pkgConfig)
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }

  isChanged() {
    if (this.deviceList.length === 0) {
      return false;
    }
    let pkgName = this.pkg.Package;
    let origin = {
      name: pkgName
    }
    for (let pkg of this.deviceList[0].pkgConfig) {
      if (pkg.name === pkgName){
        // Copy configuration
        origin = pkg;
        break;
      }
    }
    console.log("패키지 컨피그 : ",this.pkgConfig);
    console.log("오리진 : ",origin);
    // TODO: JSON.stringify can't compare same object with different key order
    if (JSON.stringify(this.pkgConfig) === JSON.stringify(origin.value)) {
      console.log('거짓');
      return false;
    } else {
      console.log('진실');
      return true;
    }
  }

  loadConfigFile() {
    this.spinner.on()
    return Promise.resolve()
    .then(() => {
      if (!this.pickedFile[0].file.name.endsWith('.json')) {
        throw new Error('Select .json file')
      }
    })
    .then(() => {
      this.pkgConfig = JSON.parse(new TextDecoder('utf-8').decode(this.pickedFile[0].fileStream)).value
      // this.pkgConfig.name = this.pkg.Package
      console.log('config:', this.pkgConfig)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('load config file fails', err)
      let params = {
        title: '읽기 실패',
        content: '파일을 읽어오는 도중 오류가 발생했습니다.',
        error: ''
      }

      if (err.message.startsWith(`Can't find end of central directory`)) {
        params.content = '잘못된 파일 형식입니다.';
      } else if (err.message === 'Select .json file') {
        params.content = 'json 형식의 파일을 선택해야 합니다.';
      } else if (err.message === 'Unexpected end of JSON input') {
        params.content = '설정 형식이 올바르지 않습니다.';
      } else {
        params.error = err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  update() {
    this.spinner.on()
    console.log('Package Config', this.pkgConfig)
    return this.deviceManager.setPkgConfigAll(this.deviceList[0], this.pkg, this.pkgConfig)
    .then((result) => {
      console.log('update Package Config', result);
      this.toastr.success('패키지 설정이 변경되었습니다.')
    })
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '패키지',
        target: this.pkg.Package,
        ops: 'modify'
      }
      return this.logs.success(params)
    })
    /// #endif
    .catch((err) => {
      console.error('update Package Config fails', err);
      let params = {
        title: '패키지 설정 실패',
        content: '패키지 설정 중 오류가 발생했습니다.',
        error: err.message
      }

      this.modal.error(params)
    })
    .then(() => this.fetch())
    .then(() => this.spinner.off())
  }
}

export default Controller;
