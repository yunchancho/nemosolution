import testModule from './module'
import testComponent from './component';
import testController from './controller';
import testTemplate from './template.html';

import pkgModule from '../module';
import pkgSelectIndexModule from '../pkgSelectIndex/module'

describe('Update Package Config', () => {
  let $componentController, cognito;

  beforeEach(window.module('app'));
  beforeEach(inject((_$componentController_, _cognito_) => {
    $componentController = _$componentController_;
    cognito = _cognito_;
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    let controller, pkgController, pkgSelectIndexController
    beforeEach(function() {
      this.timeout(15000)
      
      let bindings = {
        resolve: {
          mode: 'management',
          index: 'system',
          pkg: meta.pkg,
          setBreadcrumb: null,
          listPackages: null // not used in this component
        }
      }
      controller = $componentController(testModule, null, bindings);
      pkgController = $componentController(pkgModule, null, bindings)
      pkgSelectIndexController = $componentController(pkgSelectIndexModule, null, bindings)
      
      bindings.resolve.setBreadcrumb = (title, set) => pkgController.setBreadcrumb(title, set);
      
      return cognito.login(meta.username, meta.password)
      .then(() => null)
    });
    
    it('has properties', () => {
      expect(controller).to.have.property('$transitions');
      expect(controller).to.have.property('deviceManager');
      expect(controller).to.have.property('modal');
      expect(controller).to.have.property('spinner');
      expect(controller).to.have.property('backFlag');
      expect(controller).to.have.property('backModalOpened');
      expect(controller).to.have.property('pkgConfig');
    });
    
    it('init controller', () => {
      return Promise.resolve()
      .then(() => pkgSelectIndexController.$onInit())
      .then(() => controller.$onInit())
      .then(() => {
        expect(pkgController.breadcrumbs).to.deep.equal(['관리', '설정']);
        expect(controller.deviceList).to.have.lengthOf(meta.cloudDevice.number);
      })
    });
    
    it.skip('prevent move back if changes exists', function() {
      //TODO: implements
    })
    
    it('load local config file', function () {
      let pkgConfig = JSON.parse(JSON.stringify(meta.pkgConfig));
      pkgConfig.config.randomValue = (Math.random() + 1).toString(36).substr(2, 7);
      pkgConfig.bigdata.randomValue = (Math.random() + 1).toString(36).substr(2, 7);
      pkgConfig.graph.randomValue = (Math.random() + 1).toString(36).substr(2, 7);
      
      controller.pickedFile = {
        name: 'test.json',
        fileStream: new TextEncoder('utf-8').encode(JSON.stringify(pkgConfig))
      }
      
      return controller.loadConfigFile()
      .then(() => {
        expect(controller.pkgConfig.name).to.equal(meta.pkg.Package)
        expect(controller.pkgConfig.config).to.deep.equal(pkgConfig.config)
        expect(controller.pkgConfig.bigdata).to.deep.equal(pkgConfig.bigdata)
        expect(controller.pkgConfig.graph).to.deep.equal(pkgConfig.graph)
      })
      
    })
    
    it('update package config', () => {
      // Cloned config object with random new value
      let pkgConfig = JSON.parse(JSON.stringify(meta.pkgConfig));
      pkgConfig.config.randomValue = (Math.random() + 1).toString(36).substr(2, 7);
      pkgConfig.bigdata.randomValue = (Math.random() + 1).toString(36).substr(2, 7);
      pkgConfig.graph.randomValue = (Math.random() + 1).toString(36).substr(2, 7);
      
      return controller.$onInit()
      .then(() => {
        controller.pkgConfig = pkgConfig;
        return controller.update()
      })
      .then(() => {
        let hasConfig = false;
        for (let pkgConfig of controller.deviceList[0].pkgConfig) {
          if (pkgConfig.name === meta.pkg.Package) {
            hasConfig = true;
            expect(pkgConfig.config).to.deep.equal(pkgConfig.config)
          }
        }
        expect(hasConfig).to.be.true;
      })
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(testTemplate).to.match(/설정/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = testComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(testTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(testController);
    });
  });
});
