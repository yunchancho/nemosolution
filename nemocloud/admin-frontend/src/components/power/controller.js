class Controller {
    /// #if SNOW_MODE=='cloud'
  constructor($rootScope, deviceManager, spinner, toastr, modal){
    'ngInject'
    /// #endif
    /// #if SNOW_MODE=='local'
  constructor($rootScope, deviceManager, spinner, toastr, modal, localApis){
    'ngInject'
    this.localApis = localApis;
    /// #endif
    /// #if SNOW_MODE=='usb'
  constructor($rootScope, deviceManager, spinner, toastr, modal, localApis){
    'ngInject'
    this.localApis = localApis;
    /// #endif
    this.$rootScope = $rootScope;
    this.deviceManager = deviceManager;
    this.spinner = spinner;
    this.toastr = toastr
    this.modal = modal
    this.resolve = {
      setSubtitle: null
    }
    this.subtitle;
    this.deviceList = []
    //TODO : work a round solution
    $("body").removeClass("mini-navbar");
    $(".collapse").removeClass("in");
    $(".nav > li").removeClass("active");
  }

  $onInit() {
    this.resolve.setSubtitle = (subtitle) => this.setSubtitle(subtitle)
  }

  setSubtitle(subtitle) {
    return this.subtitle = subtitle;
  }
}

export default Controller;
