class Controller {
  constructor($rootScope, deviceManager, spinner, toastr, modal){
    'ngInject'
    this.$rootScope = $rootScope;
    this.deviceManager = deviceManager;
    this.spinner = spinner;
    this.toastr = toastr;
    this.modal = modal;
    this.resolve;
    this.deviceList = []
  }

  $onInit() {
    this.resolve.setSubtitle('IP 설정')
    return this.getDeviceList()
  }

  getDeviceList() {
    this.spinner.on()
    return this.deviceManager.getDeviceList()
    .then(deviceList => {
      this.deviceList = deviceList
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      let params = {
        title: '불러오기 실패',
        content: '디바이스 목록을 가져오던 도중 오류가 발생했습니다.',
        error : err.message
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

}

export default Controller;
