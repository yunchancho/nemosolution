import angular from 'angular';
import uiRouter from 'angular-ui-router';
import controller from './controller';

import component from './component';

const name = 'deviceIp';

let module = angular.module(name, [
	uiRouter,
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('power.ip', {
			url: '/ip',
			component: name
		});
}

export default module;
