import angular from 'angular';
import uiRouter from 'angular-ui-router';
import controller from './controller';

import component from './component';

import powerControl from './powerControl/module';
import powerTimer from './powerTimer/module';
import terminal from './terminal/module';
import deviceIp from './deviceIp/module';

const name = 'power';

let module = angular.module(name, [
	uiRouter,
	powerControl,
	powerTimer,
	terminal,
	deviceIp
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('power', {
			url: '/power',
			component: name
		});
}

export default module;
