class Controller {
  constructor($rootScope, $uibModal, deviceManager, spinner, toastr, modal, logs){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.deviceManager = deviceManager;
    this.spinner = spinner;
    this.toastr = toastr
    this.modal = modal;
    this.logs = logs
    this.deviceList = []
    this.all = false;

  }

  $onInit() {
    this.resolve.setSubtitle('전원관리')
    this.getDeviceList()
  }

  boot() {
    let devices = this.filterChecked()
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }

  // Currently useless
  cancel(devices) {
    let requests = devices.map(device => device.powerCancel())

    this.spinner.on()
    return Promise.all(requests)
    .then(result => {
      this.toastr.success('명령을 취소했습니다.')
    })
    .catch(err => {
      let params = {
        title: '실패',
        content: '종료 명령 취소 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  confirmCancel() {
    let devices = this.filterChecked()
    if (devices.length === 0) {
      this.toastr.warning('선택된 디바이스가 없습니다.');
      return;
    }

    devices = devices.filter(device => device.connected);
    if (devices.length === 0) {
      this.toastr.warning('선택된 디바이스 중 연결된 디바이스가 없습니다.');
      return;
    }

    let params = {
      title: '종료 취소',
      content: '종료 타이머를 취소하시겠습니까?',
      onConfirm: () => this.cancel(devices)
    }

    this.modal.confirm(params)
  }

  confirmReboot() {
    let devices = this.filterChecked()
    if (devices.length === 0) {
      this.toastr.warning('선택된 디바이스가 없습니다.');
      return;
    }

    devices = devices.filter(device => device.connected);
    if (devices.length === 0) {
      this.toastr.warning('선택된 디바이스 중 연결된 디바이스가 없습니다.');
      return;
    }

    this.$uibModal.open({
      component: 'powerCountdown',
      backdrop: false,
      resolve: {
        type: () => '재시작',
        ops: () => { return () => this.reboot(devices) }
      },
      size: 'sm',
    })
  }

  confirmShutdown() {
    let devices = this.filterChecked()
    if (devices.length === 0) {
      this.toastr.warning('선택된 디바이스가 없습니다.');
      return;
    }

    devices = devices.filter(device => device.connected);
    if (devices.length === 0) {
      this.toastr.warning('선택된 디바이스 중 연결된 디바이스가 없습니다.');
      return;
    }

    this.$uibModal.open({
      component: 'powerCountdown',
      backdrop: false,
      resolve: {
        type: () => '종료',
        ops: () => { return () => this.shutdown(devices) }
      },
      size: 'sm',
    })
  }

  filterChecked() {
    return this.deviceList.filter(device => {
      return device.check
    })
    .map(device => {
      return device.device
    })
  }

  reboot(devices) {
    console.log('!!!',devices);
    let requests = devices.map(device => device.reboot())

    return Promise.all(requests)
    .then(result => {
      this.toastr.success('디바이스를 재시작합니다.')
    })
    // .catch(err => {
    //   let params = {
    //     title: '재시작 실패',
    //     content: '디바이스 재시작 도중 오류가 발생했습니다.',
    //     error: err.message
    //   }
    //   this.modal.error(params)
    // })
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '전원',
        target: devices,
        ops: 'reboot'
      }
      return this.logs.success(params)
    })
    /// #endif
  }

  selectAll() {
    for (let device of this.deviceList) {
      device.check = this.all
    }
  }

  shutdown(devices) {
    let requests = devices.map(device => device.shutdown())

    return Promise.all(requests)
    .then(result => {
      this.toastr.success('디바이스를 종료합니다.')
    })
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '전원',
        target: devices,
        ops: 'shutdown'
      }
      return this.logs.success(params)
    })
    /// #endif
    // .catch(err => {
    //   let params = {
    //     title: '종료 실패',
    //     content: '디바이스 종료 도중 오류가 발생했습니다.',
    //     error: err.message
    //   }
    //   this.modal.error(params)
    // })
  }

  getDeviceList() {
    this.spinner.on()
    this.deviceManager.getDeviceList()
    .then(deviceList => {
      this.deviceList = deviceList.map(device => {
        return {
          device,
          check: false
        }
      });
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      let params = {
        title: '불러오기 실패',
        content: '디바이스 목록을 가져오던 도중 오류가 발생했습니다.',
        error : err.message
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
