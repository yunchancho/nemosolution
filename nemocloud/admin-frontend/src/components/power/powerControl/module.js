import angular from 'angular';
import uiRouter from 'angular-ui-router';
import controller from './controller';

import component from './component';

import powerCountdown from './powerCountdown/module'

const name = 'powerControl';

let module = angular.module(name, [
	uiRouter,
	powerCountdown
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('power.control', {
			url: '/control',
			component: name
		});
}

export default module;
