class Controller {
  constructor($rootScope){
    'ngInject'
    this.$rootScope = $rootScope;
    this.time = 5; // wait time
    this.type;
    this.resolve;
    this.interval;
  }

  $onInit() {
    this.type = this.resolve.type;
    this.countdown()
  }

  countdown() {
    this.interval = setInterval(() => {
      this.time--;
      console.log('interval', this.time)
      if (this.time > 0) {
        this.$rootScope.$$phase || this.$rootScope.$apply();
        return;
      }

      this.resolve.ops()
      clearTimeout(this.interval)
      this.close()
      this.$rootScope.$$phase || this.$rootScope.$apply();
    }, 1000)
  }

  cancel() {
    clearTimeout(this.interval)
    this.close();
  }
}

export default Controller;
