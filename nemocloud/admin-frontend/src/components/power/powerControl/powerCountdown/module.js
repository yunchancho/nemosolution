import angular from 'angular';
import uiRouter from 'angular-ui-router';
import controller from './controller';

import component from './component';

const name = 'powerCountdown';

let module = angular.module(name, [
	uiRouter,
	])
	.component(name, component)
	.name

export default module;
