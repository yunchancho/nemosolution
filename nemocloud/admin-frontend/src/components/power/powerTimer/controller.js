class Controller {
  constructor($rootScope, deviceManager, spinner, toastr, modal){
    'ngInject'
    this.$rootScope = $rootScope;
    this.deviceManager = deviceManager;
    this.spinner = spinner;
    this.toastr = toastr;
    this.modal = modal;
    this.resolve;
    this.deviceList = []
    this.timers = []
    this.weekdays = ['일', '월', '화', '수', '목', '금', '토'].map(week => {
      return { name: week, value: false }
    });

    
  }

  $onInit() {
    this.resolve.setSubtitle('타이머 설정')
    return this.getDeviceList()
    .then(() => this.getTimer())
  }

  addWeekTimer() {
    let timer = {
      week: [false, false, false, false, false, false, false],
      powerOn: this.parseTime('0:00'),
      powerOff: this.parseTime('0:00'),
      reboot: this.parseTime('0:00')
    }

    this.timers.push(timer)
  }

  addDateTimer() {
    let today = new Date()
    let month = today.getMonth() + 1;
    let day = today.getDate()

    if (month.length < 2) {
      month = `0${month}`;
    }
    if (day.length < 2) {
      day = `0${day}`;
    }

    let timer = {
      year: today.getFullYear(),
      month,
      day,
      powerOn: this.parseTime('0:00'),
      powerOff: this.parseTime('0:00'),
      reboot: this.parseTime('0:00')
    }

    this.timers.push(timer)
  }

  apply() {
    let days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
    let power = days.map(day => {
      return { day }
    })
    for (let timer of this.timers) {
      if (timer.week) {
        // timer using week
        for (let i = 0; i < timer.week.length; i++) {
          if (timer.week[i]) {
            power[i].powerOn = this.stringifyTime(timer.powerOn);
            power[i].powerOff = this.stringifyTime(timer.powerOff);
            power[i].reboot = this.stringifyTime(timer.reboot);
          }
        }
      } else {
        // timer using date
        // TODO: filter wrong date string
        let dateTimer = {
          date: `${timer.year}-${(timer.month.length >= 2)?timer.month:('0' + timer.month)}-${(timer.day.length >= 2)?timer.day:('0' + timer.day)}`,
          powerOn: this.stringifyTime(timer.powerOn),
          powerOff: this.stringifyTime(timer.powerOff),
          reboot: this.stringifyTime(timer.reboot),
        }
        power.push(dateTimer)
      }
    }

    console.log('apply power timer: ', power)

    this.spinner.on()
    return this.deviceManager.setPowerTimer(power)
    .then(() => this.deviceManager.refresh())
    .then(() => this.getDeviceList())
    .then(() => this.getTimer())
    .then(() => {
      this.toastr.success('전원 타이머 설정이 완료되었습니다.')
    })
    .catch(err => {
      console.error(err)
      let params = {
        title: '타이머 설정 실패',
        content: '설정 도중 오류가 발생했습니다.',
        error: err.message
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  getDeviceList() {
    this.spinner.on()
    return this.deviceManager.getDeviceList()
    .then(deviceList => {
      this.deviceList = deviceList
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      let params = {
        title: '불러오기 실패',
        content: '디바이스 목록을 가져오던 도중 오류가 발생했습니다.',
        error : err.message
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  // Parse power timer setting
  getTimer() {
    let deviceTimers = this.deviceList[0].power || []
    this.timers = [];

    for (let i = 0; i < deviceTimers.length; i++) {
      let deviceTimer = {
        powerOn: this.parseTime(deviceTimers[i].powerOn),
        powerOff: this.parseTime(deviceTimers[i].powerOff),
        reboot: this.parseTime(deviceTimers[i].reboot)
      }
      if (!deviceTimer.powerOn && !deviceTimer.powerOff && !deviceTimer.reboot) {
        // Timer doesn't set on this day
        continue;
      }

      if (deviceTimers[i].day) {
        // Timer using weekdays
        let add = true;
        for (let timer of this.timers) {
          if (this.toSafeTimeString(deviceTimer.powerOn) === this.toSafeTimeString(timer.powerOn)
          && this.toSafeTimeString(deviceTimer.powerOff) === this.toSafeTimeString(timer.powerOff)
          && this.toSafeTimeString(deviceTimer.reboot) === this.toSafeTimeString(timer.reboot)) {
            // Same timer is already added
            timer.week[i] = true;
            this.weekdays[i].value = true;
            add = false
            continue;
          }
        }
        if (add) {
          // Add new timer
          deviceTimer.week = [false, false, false, false, false, false, false]
          deviceTimer.week[i] = true;
          this.weekdays[i].value = true;
          this.timers.push(deviceTimer)
        }
      } else if (deviceTimers[i].date) {
        // timer using date
        [deviceTimer.year, deviceTimer.month, deviceTimer.day] = deviceTimers[i].date.split('-');
        this.timers.push(deviceTimer)
      }
      this.timers.sort((a, b) => {
        if (!!a.week === !!b.week) {
          return 0;
        } else if (a.week) {
          return -1;
        } else {
          return 1
        }
      })
    }
    console.log('timer: ', this.timers)
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }

  compareTimer(a, b) {
    if (a.powerOn === b.powerOn
    &&  a.powerOff === b.powerOff
    &&  a.reboot === b.reboot) {
      return true;
    } else {
      return false;
    }
  }

  // timeString: 00:00 ~ 23:59
  parseTime(timeString) {
    let hour, min;
    let time = new Date();

    if (!timeString) {
      // time.setHours(0, 0, 0)
      return null
    } else {
      [hour, min] = timeString.split(':');
      time.setHours(hour, min, 0)
      return time;
    }
  }

  removeTimer(index) {
    if (!!this.timers[index].week) {
      for (let i = 0; i < this.timers[index].week.length; i++) {
        if (this.timers[index].week[i]) {
          this.weekdays[i].value = false;
        }
      }
    }
    this.timers.splice(index, 1);
  }

  setCheckbox(index, value) {
    this.weekdays[index].value = value
  }

  stringifyTime(time) {
    if (!time) {
      return undefined;
    }
    let hour = `${time.getHours()}`;
    while (hour.length < 2) {
      hour = `0${hour}`;
    }
    let min = `${time.getMinutes()}`;
    while (min.length < 2) {
      min = `0${min}`;
    }
    return `${hour}:${min}`
  }

  toSafeTimeString(time) {
    if (time) {
      return time.toTimeString()
    } else {
      return undefined;
    }
  }

  // timer : { timer: Object.Date }
  toggleTimer(timer, type) {
    if (timer[type]) {
      timer[type] = undefined;
    } else {
      timer[type] = this.parseTime('0:00')
    }
  }
}

export default Controller;
