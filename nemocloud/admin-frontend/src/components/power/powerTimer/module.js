import angular from 'angular';
import uiRouter from 'angular-ui-router';
import controller from './controller';

import component from './component';

const name = 'powerTimer';

let module = angular.module(name, [
	uiRouter,
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('power.timer', {
			url: '/timer',
			component: name
		});
}

export default module;
