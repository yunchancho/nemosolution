import testModule from './module'
import testComponent from './component';
import testController from './controller';
import testTemplate from './template.html';

import powerModule from '../module';

describe('power timer', () => {
  let $componentController, cognito;

  beforeEach(window.module('app'));
  beforeEach(inject((_$componentController_, _cognito_) => {
    $componentController = _$componentController_;
    cognito = _cognito_;
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    let controller, powerController;
    
    beforeEach(function() {
      this.timeout(15000)
      let bindings = {
        resolve: {
          setSubtitle: null
        }
      }
      powerController = $componentController(powerModule, null, bindings)
      controller = $componentController(testModule, null, bindings);
      bindings.resolve.setSubtitle = (subtitle) => powerController.setSubtitle(subtitle);
      
      
      return cognito.login(meta.username, meta.password)
      .then(() => controller.$onInit())
      .then(() => null)
    });
    
    it('has properties', () => {
      expect(controller).to.have.property('deviceList');
      expect(controller).to.have.property('timers');
      expect(controller).to.have.property('weekdays');
    });
    
    it('init controller', function() {
      expect(controller.deviceList).to.have.lengthOf(meta.cloudDevice.number);
      expect(powerController.subtitle).to.equal('타이머 설정');
    })
    
    it('stringify & parse time', function() {
      let hour = `${Math.floor((Math.random() * 24))}`;
      let minute = `${Math.floor((Math.random() * 60))}`;
      let timeString = `${(hour.length < 2)?'0'+hour:hour}:${(minute.length < 2)?'0'+minute:minute}`;
      let date = controller.parseTime(timeString);
      
      expect(date.getMinutes()).to.equal(parseInt(minute));
      expect(date.getHours()).to.equal(parseInt(hour));
      
      expect(timeString).to.equal(controller.stringifyTime(date));
      
    })
    
    it('add week timer', function() {
      let count = controller.timers.length
      controller.addWeekTimer();
      let added = controller.timers[controller.timers.length - 1];
      expect(controller.timers.length).to.equal(count + 1);
      expect(added).to.have.property('week')
      
      // check weekday of new timer
      let weekIndex = -1;
      for (let i = 0; i < controller.weekdays.length; i++) {
        if (!controller.weekdays[i].value) {
          weekIndex = i;
          added.week[i] = true
          controller.setCheckbox(i, true);
          break;
        }
      }
      expect(weekIndex).to.not.equal(-1);
      
      return controller.apply()
      .then(() => {
        expect(controller.weekdays[weekIndex].value).to.be.true;
        for (let timer of controller.timers) {
          if (timer.week && timer.week[weekIndex]) {
            expect(added.powerOn.toString()).to.equal(timer.powerOn.toString())
            expect(added.powerOff.toString()).to.equal(timer.powerOff.toString())
            expect(added.reboot.toString()).to.equal(timer.reboot.toString())
            return null;
          }
        }
      })
    });
    
    it('add date timer', function() {
      let count = controller.timers.length
      controller.addDateTimer();
      let added = controller.timers[controller.timers.length - 1];
      let month = `${Math.floor((Math.random() * 12 + 1))}`;
      let day = `${Math.floor((Math.random() * 28 + 1))}`;
      
      added.year = '2017';
      added.month = month;
      added.day = day;
      
      expect(controller.timers.length).to.equal(count + 1);
      expect(controller.timers[controller.timers.length - 1]).to.have.property('year')
      expect(controller.timers[controller.timers.length - 1]).to.have.property('month')
      expect(controller.timers[controller.timers.length - 1]).to.have.property('day')
      
      return controller.apply()
      .then(() => {
        let exist = false;
        for (let timer of controller.timers) {
          if (timer.year === '2017'
            && parseInt(timer.month) === parseInt(added.month)
            && parseInt(timer.day) === parseInt(added.day)) {
            exist = true;
            break;
          }
        }
        expect(exist).to.be.true;
      })
      
    });
    
    it('update week timer', function() {
      let updatedTimer;
      let updatedWeek;
      let powerOn = controller.parseTime(`${Math.floor(Math.random() * 24)}:00`);
      let powerOff = controller.parseTime(`${Math.floor(Math.random() * 24)}:00`);
      
      for (let timer of controller.timers) {
        if (timer.week) {
          updatedTimer = timer
        }
      }
      if (!updatedTimer) {
        this.skip()
      }
      
      let weekIndex = -1;
      for (let i = 0; i < controller.weekdays.length; i++) {
        if (!controller.weekdays[i].value) {
          weekIndex = i;
          updatedTimer.week[i] = true
          controller.setCheckbox(i, true);
          break;
        }
      }
      if (weekIndex === -1) {
        this.skip()
        return;
      }
      updatedWeek = JSON.parse(JSON.stringify(updatedTimer.week))
      
      updatedTimer.powerOn = powerOn
      updatedTimer.powerOff = powerOff
      // off reset timer
      controller.toggleTimer(updatedTimer, 'reboot')
      
      return controller.apply()
      .then(() => {
        for (let timer of controller.timers) {
          if (timer.week[weekIndex]) {
            expect(timer.week).to.deep.equal(updatedWeek);
            expect(timer.powerOn.toString()).to.equal(updatedTimer.powerOn.toString());
            expect(timer.powerOff.toString()).to.equal(updatedTimer.powerOff.toString());
            expect(timer.reset).to.deep.equal(updatedTimer.reset);
            return
          }
        }
      })
      
    });
    
    it('update date timer', function() {
      let updatedTimer;
      let month = `${Math.floor((Math.random() * 12 + 1))}`;
      let day = `${Math.floor((Math.random() * 28 + 1))}`;
      let powerOn = controller.parseTime(`${Math.floor((Math.random() * 24))}:00`);
      let powerOff = controller.parseTime(`${Math.floor((Math.random() * 24))}:00`);
      
      for (let timer of controller.timers) {
        if (!timer.week) {
          updatedTimer = timer
        }
      }
      if (!updatedTimer) {
        this.skip()
      }
      
      updatedTimer.year = '2017';
      updatedTimer.month = month;
      updatedTimer.day = day;
      updatedTimer.powerOn = powerOn
      updatedTimer.powerOff = powerOff
      // off reset timer
      controller.toggleTimer(updatedTimer, 'reboot')
      
      return controller.apply()
      .then(() => {
        for (let timer of controller.timers) {
          if (!!timer.week) {
            continue;
          }
          if (timer.year === '2017' && timer.month === updatedTimer.month && timer.day === updatedTimer.day) {
            expect(timer.powerOn.toString()).to.equal(updatedTimer.powerOn.toString());
            expect(timer.powerOff.toString()).to.equal(updatedTimer.powerOff.toString());
            expect(timer.reset).to.deep.equal(updatedTimer.reset);
            return
          }
        }
      })
    });
    
    it('delete week timer', function() {
      let deleted;
      for (let i = 0; i < controller.timers.length; i++) {
        if (controller.timers[i].week) {
          deleted = JSON.parse(JSON.stringify(controller.timers[i]));
          controller.removeTimer(i);
          break;
        }
      }
      
      if (!deleted) {
        // Nothing to delete
        this.skip()
      }
      
      for (let i = 0; i < deleted.week.length; i++) {
        if (deleted.week[i]) {
          expect(controller.weekdays[i].value).to.be.false;
        }
      }
      
      return controller.apply()
      .then(() => {
        for (let i = 0; i < deleted.week.length; i++) {
          if (deleted.week[i]) {
            expect(controller.weekdays[i].value).to.be.false;
          }
        }
      })
      
    });
    
    it('delete date timer', function() {
      let deleted;
      for (let i = 0; i < controller.timers.length; i++) {
        if (!!controller.timers[i].week) {
          continue;
        }
        deleted = JSON.parse(JSON.stringify(controller.timers[i]));
        controller.removeTimer(i);
        break;
      }
      
      return controller.apply()
      .then(() => {
        for (let timer of controller.timers) {
          if (timer.year === deleted.year
          && timer.month === deleted.month
          && timer.day === deleted.day) {
            expect(timer.powerOn.toString()).to.not.equal(deleted.powerOn.toString());
            expect(timer.powerOff.toString()).to.not.equal(deleted.powerOff.toString());
            expect(timer.reset).to.not.equal(deleted.reset.toString());
            return
          }
        }
      })
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = testComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(testTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(testController);
    });
  });
});
