/// #if SNOW_MODE=='cloud'
import RpcManager from '../../../lib/rpcManager/index'
/// #endif

class Controller {
  constructor($rootScope, spinner, deviceManager,modal){
    'ngInject'
    this.$rootScope = $rootScope;
    this.deviceManager = deviceManager;
    this.deviceList=[];
    this.device;
    this.modal = modal
    this.spinner = spinner;
    this.resMsg=""
    this.cmdResult
    this.commands=[];
    
  }

  $onInit() {
    this.resolve.setSubtitle('원격 프롬프트')
    this.getDeviceList();
  }

  clear(){
    this.commands = [];
  }

  getDeviceList() {
    this.spinner.on()
    this.deviceManager.getDeviceList()
    .then(deviceList => {
      this.deviceList = deviceList
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      let params = {
        title: '불러오기 실패',
        content: '디바이스 목록을 가져오던 도중 오류가 발생했습니다.',
        error : err.message
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  runCommand(){
    let request = {};
    request.resMsg = this.resMsg;
    let params = {
      type: 'devinfo',
      command: 'runCustomCommand',
      params: { customCommand: this.resMsg }
    }
    this.spinner.on()
    return this.device.runCommand(params)
    .then(result => {
      request.cmdResult = result.output;
      return this.commands.push(request);
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      let params = {
        title: '불러오기 실패',
        content: '터미널 명령어를 불러오지 못했습니다.',
        error : err.message
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  selectDevice(){
    console.log('select',this.device);
  }
}

export default Controller;
