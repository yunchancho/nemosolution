/// #if SNOW_MODE=='local'
import infoModule from './module'
import infoComponent from './component';
import infoController from './controller';
import infoTemplate from './template.html';

describe('info', () => {
  let $rootScope, makeController;

  beforeEach(window.module(infoModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new infoController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('cpu');
      expect(controller).to.have.property('disk');
      expect(controller).to.have.property('memory');
      expect(controller).to.have.property('pc');
      expect(controller).to.have.property('ip');
      expect(controller).to.have.property('swKey');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(infoTemplate).to.match(/정보/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = infoComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(infoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(infoController);
    });
  });
});
/// #endif