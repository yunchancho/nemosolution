import angular from 'angular';
import uiRouter from 'angular-ui-router';
import controller from './controller';

import component from './component';

import restoreLocal from './restoreLocal/module'
import restoreSelectDevice from './restoreSelectDevice/module'
import restoreSelectFolder from './restoreSelectFolder/module'
import restoreSelectSrc from './restoreSelectSrc/module'

const name = 'restore';

let module = angular.module(name, [
	uiRouter,
	restoreLocal,
	restoreSelectDevice,
	restoreSelectFolder,
	restoreSelectSrc
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('restore', {
			url: '/restore',
			component: name
		});
}

export default module;
