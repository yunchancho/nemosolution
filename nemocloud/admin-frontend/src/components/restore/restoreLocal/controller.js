import JSZip from 'jszip'

class Controller {
  constructor($rootScope, $state, deviceManager, s3, spinner, toastr, modal, logs){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.logs = logs;
    this.deviceManager = deviceManager;
    this.s3 = s3;
    this.spinner = spinner;
    this.toastr = toastr;
    this.modal = modal;
    this.resolve;
    this.pickedFile = [];
    this.items = []
    this.onFileSelect = () => this.readFile()
  }

  $onInit() {
    if (!this.resolve.target.src) {
      this.$state.go('restore.source')
      return;
    }
    this.resolve.setBreadcrumb('백업 데이터')
  }

  back() {
    window.history.back()
  }

  confirmRestore() {
    let params = {
      title: '백업 복원',
      content: '설정을 복원하시겠습니까?',
      warning: '기존 설정이 삭제됩니다.',
      onConfirm: () => this.restore()
    }
    this.modal.confirm(params)
  }

  readFile() {
    this.spinner.on()
    return Promise.resolve()
    .then(() => {
      if (!this.pickedFile[0].file.name.endsWith('.zip')) {
        throw new Error('Select .zip file')
      }
    })
    .then(() => JSZip.loadAsync(this.pickedFile[0].fileStream))
    .then(zip => {
      console.log('Selected zip:', zip)
      let requests = []
      for (let key of Object.keys(zip.files)) {
        if (key.indexOf('/') !== -1) {
          throw new Error('exclude folder')
        }
        if (!key.endsWith('.json')) {
          throw new Error('Input .json file only')
        }
        requests.push(zip.file(key).async('string'))
      }
      return Promise.all(requests)
    })
    .then(files => {
      this.items = []
      for (let file of files) {
        this.items.push(JSON.parse(file))
      }
      console.log('zip items:', this.items)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      this.items = []
      let params = {
        title: '읽기 실패',
        content: '파일을 읽어오는 도중 오류가 발생했습니다.',
        error: ''
      }

      if (err.message.startsWith(`Can't find end of central directory`)) {
        params.content = '잘못된 파일 형식입니다.';
      } else if (err.message === 'Select .zip file') {
        params.content = 'zip 형식의 파일을 선택해야 합니다.';
      } else if (err.message === 'Input .json file only') {
        params.content = '설정은 json 파일 형식으로 입력해야 합니다.';
      } else if (err.message === 'exclude folder') {
        params.content = 'zip 파일은 폴더를 포함하지 않아야 합니다.';
      } else if (err.message === 'Unexpected end of JSON input') {
        params.content = '설정 형식이 올바르지 않습니다.';
      } else {
        params.error = err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  restore() {
    this.spinner.on()
    return this.deviceManager.getDeviceList()
    .then(deviceList => this.deviceManager.restoreConfigFromLocal(this.items))
    .then(result => {
      this.toastr.success('설정 복원이 완료되었습니다.')
    })
    .catch(err => {
      let params = {
        title: '복원 실패',
        content: '설정 복원 중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '복원',
        target: '앱설정',
        ops: 'restore'
      }
      return this.logs.success(params)
    })
    /// #endif
  }

}

export default Controller;
