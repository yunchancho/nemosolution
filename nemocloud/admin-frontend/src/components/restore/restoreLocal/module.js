import angular from 'angular';
import uiRouter from 'angular-ui-router';
import controller from './controller';

import component from './component';

const name = 'restoreLocal';

let module = angular.module(name, [
	uiRouter,
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('restore.local', {
			url: '/local',
			component: name
		});
}

export default module;
