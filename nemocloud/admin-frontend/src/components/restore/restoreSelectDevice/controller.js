class Controller {
  constructor($rootScope, $state, s3, spinner, modal){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.s3 = s3;
    this.spinner = spinner;
    this.modal = modal;
    this.resolve;
    this.backupList = null
  }
  
  $onInit() {
    if (!this.resolve.target.src) {
      this.$state.go('restore.source')
      return;
    }
    this.resolve.setBreadcrumb('디바이스')
    this.getIndexList();
  }
  
  back() {
    window.history.back()
  }
  
  select(backupIndex) {
    this.resolve.target.prefix = `${backupIndex.Prefix}configs/`;
    this.$state.go('restore.folder')
  }
  
  getIndexList() {
    this.spinner.on()
    this.s3.listBackupIndex()
    .then(result => {
      this.backupList = result.map(folder => {
        let start = folder.Prefix.lastIndexOf('/', folder.Prefix.length - 2)
        folder.name = folder.Prefix.slice(start + 1, folder.Prefix.length - 1)
        return folder;
      });
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('get device list fails', err)
      let params = {
        title: '불러오기 실패',
        content: '디바이스 목록을 가져오던 중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
