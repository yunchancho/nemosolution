import restoreSelectDeviceModule from './module'
import restoreSelectDeviceComponent from './component';
import restoreSelectDeviceController from './controller';
import restoreSelectDeviceTemplate from './template.html';

describe('restoreSelectDevice', () => {
  let $rootScope, makeController;

  beforeEach(window.module(restoreSelectDeviceModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new restoreSelectDeviceController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('$rootScope');
      expect(controller).to.have.property('$state');
      expect(controller).to.have.property('s3');
      
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(restoreSelectDeviceTemplate).to.match(/기기 선택/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = restoreSelectDeviceComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(restoreSelectDeviceTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(restoreSelectDeviceController);
    });
  });
});
