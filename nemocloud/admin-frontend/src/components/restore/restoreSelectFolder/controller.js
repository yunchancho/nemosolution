class Controller {
  constructor($rootScope, $state, deviceManager, s3, spinner, toastr, modal, logs){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.deviceManager = deviceManager;
    this.s3 = s3;
    this.logs = logs;
    this.spinner = spinner;
    this.toastr = toastr;
    this.modal = modal;
    this.resolve;
    this.backupList = null
  }

  $onInit() {
    if (!this.resolve.target.src
      || !this.resolve.target.prefix) {
      this.$state.go('restore.source')
      return;
    }
    this.resolve.setBreadcrumb('복원 데이터')
    this.getBackupList()
  }

  back() {
    window.history.back()
  }

  restore(backupIndex) {
    console.log("...",backupIndex);
    this.spinner.on()
    return this.deviceManager.refresh()
    .then(() => this.deviceManager.restoreConfigFromCloud(backupIndex.Prefix))
    .then(result => {
      console.log('restore result', result)
      this.toastr.success('디바이스 복원이 완료되었습니다.')
    })
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '복원',
        target: backupIndex.Prefix,
        ops: 'restore'
      }
      return this.logs.success(params)
    })
    /// #endif
    .catch(err => {
      console.error('restore fails', err)
      let params = {
        title: '복원 실패',
        content: '디바이스 복원 중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

  select(backupIndex) {
    let params = {
      title: '백업 복원',
      content: '설정을 복원하시겠습니까?',
      warning: '기존 설정이 삭제됩니다.',
      onConfirm: () => this.restore(backupIndex)
    }
    this.modal.confirm(params)
  }

  getBackupList() {
    this.spinner.on()
    this.s3.listDirectoryObjects(this.s3.getConfig().backupsBucket, this.resolve.target.prefix)
    .then(result => {
      console.log(result)
      this.backupList = result.CommonPrefixes.map(folder => {
        let start = folder.Prefix.lastIndexOf('/', folder.Prefix.length - 2)
        folder.name = folder.Prefix.slice(start + 1, folder.Prefix.length - 1)
        return folder;
      })
    })
    .catch(err => {
      console.error('get backup folder list fails', err)
      let params = {
        title: '불러오기 실패',
        content: '백업 목록을 가져오던 중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }

}

export default Controller;
