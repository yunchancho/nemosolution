import restoreSelectFolderModule from './module'
import restoreSelectFolderComponent from './component';
import restoreSelectFolderController from './controller';
import restoreSelectFolderTemplate from './template.html';

describe('restoreSelectFolder', () => {
  let $rootScope, makeController;

  beforeEach(window.module(restoreSelectFolderModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new restoreSelectFolderController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('$rootScope');
      expect(controller).to.have.property('$state');
      expect(controller).to.have.property('s3');
      expect(controller).to.have.property('backupList');

    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(restoreSelectFolderTemplate).to.match(/폴더 선택/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = restoreSelectFolderComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(restoreSelectFolderTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(restoreSelectFolderController);
    });
  });
});
