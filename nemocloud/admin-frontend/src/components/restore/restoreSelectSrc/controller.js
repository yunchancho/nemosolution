class Controller {
  constructor($rootScope, $state, modal){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.modal = modal;
    this.resolve;
  }
  
  $onInit() {
    this.resolve.setBreadcrumb('위치 선택')
  }
  
  select(src) {
    this.resolve.target.src = src;
    if (src === 'cloud') {
      this.$state.go('restore.device')
    } else if (src === 'local'){
      this.$state.go('restore.local')
    }
  }

}

export default Controller;
