import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-toastr/dist/angular-toastr.min.css';
import 'angular-toastr/dist/angular-toastr.tpls.js';

import component from './component';


const name = 'restoreSelectSrc';

let module = angular.module(name, [
	uiRouter,
	'toastr',
	'app.commons.services.modal'
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('restore.source', {
			url: '/source',
			component: name
		});
}

export default module;
