import restoreSelectSrcModule from './module'
import restoreSelectSrcComponent from './component';
import restoreSelectSrcController from './controller';
import restoreSelectSrcTemplate from './template.html';

describe('restoreSelectSrc', () => {
  let $rootScope, $state, modal, makeController;

  beforeEach(window.module(restoreSelectSrcModule));
  beforeEach(inject((_$rootScope_,_$state_, modal) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    modal = modal;
    makeController = () => {
      return new restoreSelectSrcController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('$rootScope');
      expect(controller).to.have.property('$state');
      expect(controller).to.have.property('modal');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(restoreSelectSrcTemplate).to.match(/설정 복원/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = restoreSelectSrcComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(restoreSelectSrcTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(restoreSelectSrcController);
    });
  });
});
