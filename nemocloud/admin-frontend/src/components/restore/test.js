import restoreModule from './module'
import restoreComponent from './component';
import restoreController from './controller';
import restoreTemplate from './template.html';

describe('restore', () => {
  let $rootScope, makeController;

  beforeEach(window.module(restoreModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new restoreController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('resolve');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(restoreTemplate).to.match(/복원/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = restoreComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(restoreTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(restoreController);
    });
  });
});
