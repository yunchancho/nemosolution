import co from 'co'
import RpcManager from '../../lib/rpcManager'

// TODO this should be set to keys by cognito
// *.json is only used for test

class Controller {
  constructor() {
		'ngInject';

    this.status = 'unknown'
		this.name = "rpc test component"
    this.customCommand = ''
    this.customCommandResult = ''

    lifecycleCallback = lifecycleCallback.bind(this)

		// TODO we need to replace this json to one of proper aws iot 
		this.rpcManager = new RpcManager(require('./remote.json'), lifecycleCallback)
		this.rpcManager.initialize()
		.then(result => console.log(result))
		.catch(err => console.log(err))
  }

	getDevInfo(command) {
		this.rpcManager.runDeviceCommand({ type: 'devinfo', command })
		.then(result => console.log(result))
		.catch(err => console.log(err))
	}

  runCommand() {
		this.rpcManager.runDeviceCommand({ type: 'devinfo', command: 'runCustomCommand', params: { customCommand: this.customCommand } })
		.then(result => {
      console.log(result)
      this.customCommandResult = result.output
    })
		.catch(err => console.log(err))
  }

  controlPower(command) {
		this.rpcManager.runDeviceCommand({ type: 'powerctrl', command, params: { timeoutMin: 1 } })
		.then(result => console.log(result))
		.catch(err => console.log(err))
	}

  echo(str) {
		this.rpcManager.runDeviceCommand({ type: 'powerctrl', command: 'echo', params: { test: str } })
		.then(result => console.log(result))
		.catch(err => console.log(err))
  }
}

function lifecycleCallback(message) {
  this.status = message.type
  console.log(this)
  console.log(message)
}

export default Controller;
