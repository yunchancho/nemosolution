class Controller {
  constructor($rootScope, localApis, modal){
    'ngInject'
    this.$rootScope = $rootScope;
    this.localApis = localApis;
    this.modal = modal;
    this.edit = false;
    this.sinks = [];
  }

  $onInit() {
    this.resolve.setSubtitle('Audio')
    this.getVolume();
  }

  beep(id) {
    this.localApis.playSampleSound(id)
    .then(result => console.log)
    .catch(err => {
      console.error('beep fails', err);
      throw err
    });
  }

  getVolume() {
    let initSink = (sink) => {
      sink.slider = {
        value: parseInt(sink.volume),
        minValue: 20,
        maxValue: 80,
        options: {
          id: sink.sink,
          floor: 0,
          ceil: 100,
          disabled: sink.mute,
          step:10,
          showSelectionBar: true,
          showTicks: true, // just to show the disabled style
          draggableRange: true, // just to show the disabled style
          onEnd: (id, value) => this.setVolume(id, value)
        }
      }
      sink.editing = false;
      return sink;
    }

    return this.localApis.getSinks()
    .then(sinks => {
      this.sinks = sinks.map(sink => {
        return initSink(sink)
      })
      console.log('sinks: ', this.sinks)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      let params = {
        title: '오류',
        content: '오디오 설정을 가져올 수 없습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
  }

  setVolume(id , value) {
    this.localApis.setVolume(id, value)
    .then(result => this.getVolume())
    .then(result => this.beep(id))
    .catch(err => {
      // Rollback slider
      for (let sink of this.sinks) {
        if (sink.sink === id) {
          sink.slider.value = parseInt(sink.volume)
          this.$rootScope.$$phase || this.$rootScope.$apply();
          break;
        }
      }
      console.error('Set volume error:', err);
      let params = {
        title: '볼륨 조절 실패',
        content: '볼륨 조절 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
  }

  toggleEdit(sink) {
    sink.editing = !sink.editing;
  };

  toggleMute(sink) {
    this.localApis.mute(sink.sink, sink.slider.options.disabled)
    .then(result => {
      this.getVolume();
    })
    .catch(err => {
      console.error('Mute error:', err);
      let params = {
        title: '음소거 실패',
        content: '음소거 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    });
  }

}

export default Controller;
