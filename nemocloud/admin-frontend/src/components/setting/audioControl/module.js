import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angularjs-slider/dist/rzslider.css'
import ngSlider from 'angularjs-slider/dist/rzslider.min.js';

import component from './component';

const name = 'audioControl';

let module = angular.module(name, [
	uiRouter,
	ngSlider,
	'app.commons.services.localApis'
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('setting.audioControl', {
			url: '/audioControl',
			component: name
		});
}

export default module;
