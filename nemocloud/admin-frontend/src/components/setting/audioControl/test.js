/// #if SNOW_MODE=='local'
import audioControlModule from './module'
import audioControlComponent from './component';
import audioControlController from './controller';
import audioControlTemplate from './template.html';

describe('audioControl', () => {
  let $rootScope, makeController;

  beforeEach(window.module(audioControlModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new audioControlController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('edit');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(audioControlTemplate).to.match(/비밀번호 변경/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = audioControlComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(audioControlTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(audioControlController);
    });
  });
});
/// #endif