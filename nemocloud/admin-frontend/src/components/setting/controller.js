class Controller {
  constructor($rootScope, $state) {
    'ngInject'
    this.name = 'setting';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.subtitle = '';
    this.resolve = {};
    //TODO : work a round solution
    $("body").removeClass("mini-navbar");
  }

  $onInit() {
    this.resolve.setSubtitle = (subtitle) => {
      return this.subtitle = subtitle;
    }
  }
}

export default Controller;
