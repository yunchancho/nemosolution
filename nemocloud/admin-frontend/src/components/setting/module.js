import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import audioControl from './audioControl/module';
import network from './network/module';

const name = 'setting';

let module = angular.module(name, [
	uiRouter,
	audioControl,
	network,
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/setting',
			component: name
		});
}
