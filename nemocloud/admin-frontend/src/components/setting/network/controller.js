class Controller {
  // on: use to on/off components
  // onSwitch: use to show checkbox(switch) button's move imediately
  constructor($rootScope, modal, spinner, network){
    'ngInject'
    this.$rootScope = $rootScope;
    this.modal = modal;
    this.spinner = spinner;
    this.network = network;
    this.on = false;
    this.onSwitch = false;
    this.mode = '';
    this.selectedWifi = null;
    this.selectedWifiIfce = null;
    this.selectedEthernet = null;
    this.ifces = {
      ethernet: [],
      wifi: []
    }
    this.fetchInterval;
    this.refreshing = false;
  }

  // TODO: Show selected network on init
  $onInit() {
    this.resolve.setSubtitle('Network')

    this.spinner.on()
    this.getStatus()
    .catch(err => {
      // TODO: error modal open repeatly
      if (err.message === 'network disabled') {
        console.log('network disabled')
        return;
      }
      console.error('get network status', err)
      let params = {
        title: '오류',
        content: '네트워크 설정을 가져오던 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())

    this.toggleRefresh(true)
  }

  $onDestroy() {
    this.toggleRefresh(false)
  }

  getStatus() {
    let status = null;

    return this.network.getStatus()
    .then(result => {
      status = result;
      console.log('network status: ', result)
      if (result.status !== 'enabled') {
        this.on = false;
        this.onSwitch = false;
        throw new Error('network disabled')
      } else {
        this.on = true;
        this.onSwitch = true;
      }
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .then(() => this.network.fetchNetwork())
    .then(() => this.network.getNetwork('wifi'))
    .then(wifi => {
      this.ifces.wifi = wifi.ifces;
      console.log('wifi', this.ifces.wifi)
      if (!this.mode) {
        for (let ifce of wifi.ifces) {
          if (ifce.device === status.ifce) {
            this.mode = ifce.type;
          }
        }
      }
    })
    .then(() => this.network.getNetwork('ethernet'))
    .then(ethernet => {
      this.ifces.ethernet = ethernet.ifces;
      console.log('ethernet', this.ifces.ethernet)
      if (!this.mode) {
        for (let ifce of ethernet.ifces) {
          if (ifce.device === status.ifce) {
            this.mode = ifce.type;
          }
        }
      }
    })
    .then(() => {
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('get network status fails', err)
      throw err
    })
  }

  setMode(mode) {
    this.mode = mode;
  }

  toggleNetwork() {
    this.spinner.on();
    if (!this.onSwitch) {
      // this.toggleRefresh(false)
      this.selectedWifi = null;
      this.selectedWifiIfce = null;
      this.selectedEthernet = null;
    }

    this.network.setStatus(this.onSwitch)
    .then(() => {
      // Wait for wifi searching
      if (this.onSwitch) {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            return resolve()
          }, 5000)
        })
      } else {
        return;
      }
    })
    .then(result => this.getStatus())
    .then(result => {
      this.on = this.onSwitch
      // this.toggleRefresh(this.on)
    })
    .catch(err => {
      this.onSwitch = this.on
      if (!this.on && err.message === 'network disabled') {
        return;
      }
      console.error('set network error', err)

      let params = {
        title: '오류',
        content: '네트워크 설정 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => {
      this.spinner.off()
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
  }

  toggleRefresh(on) {
    if (on) {
      this.fetchInterval = setInterval(() => {
        if (!this.refreshing) {
          this.refreshing = true;
          this.getStatus()
          .then(() => {
            this.refreshing = false;
          })
        }
      }, 5000)
    } else {
      clearInterval(this.fetchInterval)
    }
  }
}

export default Controller;
