class Controller {
  constructor($rootScope, spinner, modal, network, toastr){
    'ngInject'
    this.$rootScope = $rootScope;
    this.spinner = spinner;
    this.modal = modal;
    this.network = network
    this.toastr = toastr
    this.detail = false;
    this.ifce;
    this.view = {
      dhcp: true,
      autoDNS: true,
      ip: '',
      subnet: '',
      gw: '',
      dns: ['', '']
    }
  }
  
  $onChanges(obj) {
    if (obj.ifce
      && obj.ifce.currentValue
      && (!obj.ifce.previousValue 
      || obj.ifce.previousValue.name !== obj.ifce.currentValue.name)) {
      this.initView(obj.ifce.currentValue)
    }
  }
  
  $onInit() {
    this.initView(this.ifce)
  }

  // TODO: autoDNS to be on after setting dhcp on
  confirm() {
    this.spinner.on();
    this.network.connectEthernet(this.ifce, this.view)
    .catch(err => {
      // Handle setting dhcp on offline ethernet interface 
      if (err.message === 'NETWORK_ERROR_ETHERNET_CONNECT_NOT_ACTIVATED') {
        return true;
      }
      throw err
    })
    .then(result => {
      console.log('connect ethernet', result)
      return this.network.refreshNetwork()
    })
    .then(result => {
      // Select refreshed interface
      // Caution: It doesn't change 'ehternetList.selected' value
      let old = this.ifce;
      this.ifce = null;
      for (let ifce of this.network.ethernet.ifces) {
        if (ifce.device === old.device) {
          this.ifce = ifce;
        }
      }
      this.initView(this.ifce)
      this.toastr.success('이더넷 설정이 완료되었습니다.')
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.log('connectiong ethernet', err)
      let params = {
        title: '오류',
        content: '이더넷 설정 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
  
  initView(ifce) {
    if (!ifce) {
      return;
    }
    this.view = {
      dhcp: true,
      autoDNS: true,
      ip: '',
      subnet: '',
      gw: '',
      dns: ['', '']
    }
    if (ifce.dhcp === undefined) {
      this.view.dhcp = true
    } else {
      this.view.dhcp = ifce.dhcp
    }
    
    if (!ifce.dhcp) {
      this.view.ip = ifce.ip;
      this.view.subnet = '';
      this.view.gw = ifce.gw
    }
    if (ifce.dns && ifce.dns.length !== 0) {
      this.view.autoDNS = false;
      this.view.dns[0] = ifce.dns[0] || ''
      this.view.dns[1] = ifce.dns[1] || ''
    }
  }
}

export default Controller;
