/// #if SNOW_MODE=='local'
import ethernetDetailModule from './module'
import ethernetDetailComponent from './component';
import ethernetDetailController from './controller';
import ethernetDetailTemplate from './template.html';

describe('ethernetDetail', () => {
  let $rootScope, makeController;

  beforeEach(window.module(ethernetDetailModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new ethernetDetailController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('spinner');
      expect(controller).to.have.property('modal');
      expect(controller).to.have.property('network');
      expect(controller).to.have.property('toastr');
      expect(controller).to.have.property('detail');
      expect(controller).to.have.property('view');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(ethernetDetailTemplate).to.match(/고급설정/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = ethernetDetailComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(ethernetDetailTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(ethernetDetailController);
    });
  });
});
/// #endif