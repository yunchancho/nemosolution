class Controller {
  constructor($rootScope, spinner, modal, network){
    'ngInject'
    this.$rootScope = $rootScope;
    this.spinner = spinner;
    this.modal = modal;
    this.network = network;
    this.selected;
    this.ifces;
  }

  $onChanges(obj) {
    if (obj.ifces
      && obj.ifces.currentValue) {
      this.handleChanges()
    }
  }

  handleChanges() {
    if (!this.selected) {
      try {
        this.selectIfce(this.ifces[0])
      } catch(err) {
        console.error(err)
      }
    } else {
      for (let ifce of this.ifces) {
        if (ifce.name === this.selected.name) {
          this.selected = ifce;
          return
        }
      }
      this.selected = null;
    }
  }
  
  selectIfce(ifce) {
    this.selected = ifce;
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
}

export default Controller;
