/// #if SNOW_MODE=='local'
import ethernetListModule from './module'
import ethernetListComponent from './component';
import ethernetListController from './controller';
import ethernetListTemplate from './template.html';

describe('ethernetList', () => {
  let $rootScope, makeController;

  beforeEach(window.module(ethernetListModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new ethernetListController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('spinner');
      expect(controller).to.have.property('modal');
      expect(controller).to.have.property('network');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(ethernetListTemplate).to.match(/고급설정/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = ethernetListComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(ethernetListTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(ethernetListController);
    });
  });
});
/// #endif