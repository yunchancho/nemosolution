import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import ethernetDetail from './ethernetDetail/module'
import ethernetList from './ethernetList/module'
import wifiDetail from './wifiDetail/module'
import wifiList from './wifiList/module'


const name = 'network';

let module = angular.module(name, [
	uiRouter,
	ethernetDetail,
	ethernetList,
	wifiDetail,
	wifiList,
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('setting.network', {
			url: '/network',
			component: name
		});
}
