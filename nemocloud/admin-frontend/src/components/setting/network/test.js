/// #if SNOW_MODE=='local'
import networkModule from './module'
import networkComponent from './component';
import networkController from './controller';
import networkTemplate from './template.html';

describe('network', () => {
  let $rootScope, makeController;

  beforeEach(window.module(networkModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new networkController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('network');
      expect(controller).to.have.property('on');
      expect(controller).to.have.property('onSwitch');
      expect(controller).to.have.property('selectedWifi');
      expect(controller).to.have.property('selectedEthernet');
      expect(controller).to.have.property('selectedWifiIfce');
      expect(controller).to.have.property('ifces');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(networkTemplate).to.match(/네트워크/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = networkComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(networkTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(networkController);
    });
  });
});
/// #endif