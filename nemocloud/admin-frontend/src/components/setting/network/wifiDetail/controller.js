class Controller {
  constructor($rootScope, spinner, modal, network, toastr){
    'ngInject'
    this.$rootScope = $rootScope;
    this.spinner = spinner;
    this.modal = modal;
    this.network = network;
    this.toastr = toastr
    this.wifi;
    this.ifce;
    this.password = '';
  }
  
  $onChanges(obj) {
    if (obj.wifi
      && obj.wifi.previousValue
      && obj.wifi.currentValue
      && obj.wifi.previousValue.name !== obj.wifi.currentValue.name) {
      this.password = ''
    }
  }

  // If connect is true, connect to selected AP.
  // Else, disconnect selected interface
  connect(connect) {
    this.spinner.on()
    let prom;
    if (connect) {
      prom = this.network.connectAccesspoint(this.ifce, this.wifi.name, this.password)
    } else {
      prom = this.network.disconnectWifi(this.ifce)
    }
    
    prom.then(result => {
      console.log(`Wifi ${connect?'':'dis'}connected`, result)
      this.toastr.success(`WiFi가 연결${connect?'':' 종료'}되었습니다.`)
      return this.network.getAccesspoints(this.ifce);
    })
    .then(aps => {
      for (let ap of aps) {
        if (ap.name === this.wifi.name) {
          this.wifi = ap;
        }
      }
      // TODO: Refresh after wifi state changed
      this.ifce.state = connect?'connected':'disconnected'
    })
    .catch(err => {
      console.error(`Wifi ${connect?'dis':''}connection error`, err);
      let params = {
        title: '오류',
        content: '와이파이 설정 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
