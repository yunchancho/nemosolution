import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-toastr/dist/angular-toastr.min.css';
import 'angular-toastr/dist/angular-toastr.tpls.js';

import component from './component';


const name = 'wifiDetail';

let module = angular.module(name, [
	uiRouter,
	'toastr',
	'app.commons.services.localApis'
	])
	.component(name, component)
	.name

export default module;