/// #if SNOW_MODE=='local'
import wifiDetailModule from './module'
import wifiDetailComponent from './component';
import wifiDetailController from './controller';
import wifiDetailTemplate from './template.html';

describe('wifiDetail', () => {
  let $rootScope, makeController;

  beforeEach(window.module(wifiDetailModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new wifiDetailController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('spinner');
      expect(controller).to.have.property('modal');
      expect(controller).to.have.property('network');
      expect(controller).to.have.property('toastr');
      expect(controller).to.have.property('password');

    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(wifiDetailTemplate).to.match(/신호 강도/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = wifiDetailComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(wifiDetailTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(wifiDetailController);
    });
  });
});
/// #endif