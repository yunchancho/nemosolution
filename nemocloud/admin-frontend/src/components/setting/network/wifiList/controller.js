class Controller {
  constructor($rootScope, spinner, localApis, modal, network){
    'ngInject'
    this.$rootScope = $rootScope;
    this.spinner = spinner;
    this.localApis = localApis;
    this.modal = modal;
    this.network = network;
    this.tab;
    this.selectedIfce;
    this.selectedWifi;
    this.ifces;
    this.interval;
  }

  $onChanges(obj) {
    if (obj.ifces
      && obj.ifces.currentValue) {
      this.handleChanges()
    }
  }

  // Remember selected interface and accesspoint
  handleChanges() {
    if (!this.selectedIfce) {
      this.tab = this.ifces[0].device;
      this.tabSelected(this.ifces[0])
      return;
    }
    for (let ifce of this.ifces) {
      if (ifce.device === this.selectedIfce.device) {
        this.tab = ifce.device
        this.selectedIfce = ifce;
        if (this.selectedWifi) {
          let find = false;
          for (let ap of ifce.accesspoints) {
            if (ap.name === this.selectedWifi.name) {
              this.selectedWifi = ap;
              find = true;
              break;
            }
          }
          if (!find) {
            this.selectedWifi = null;
          }
        } else {
          this.selectedWifi = ifce.accesspoints[0]
        }
        break;
      }
    }
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
  
  tabSelected(ifce) {
    this.selectedIfce = ifce;
    try {
      this.selectedWifi = ifce.accesspoints[0]
    } catch(err) {
      console.error(err)
    }
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }

  selectWifi(ifce, wifi) {
    this.selectedIfce = ifce;
    this.selectedWifi = wifi;
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
};
export default Controller;
