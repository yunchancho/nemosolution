import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';


const name = 'wifiList';

let module = angular.module(name, [
	uiRouter,
	'app.commons.services.localApis'
	])
	.component(name, component)
	.name

export default module;