/// #if SNOW_MODE=='local'
import wifiListModule from './module'
import wifiListComponent from './component';
import wifiListController from './controller';
import wifiListTemplate from './template.html';

describe('wifiList', () => {
  let $rootScope, makeController;

  beforeEach(window.module(wifiListModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new wifiListController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('$rootScope');
      expect(controller).to.have.property('spinner');
      expect(controller).to.have.property('localApis');
      expect(controller).to.have.property('modal');
      expect(controller).to.have.property('network')
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(wifiListTemplate).to.match(/신호 강도/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = wifiListComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(wifiListTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(wifiListController);
    });
  });
});
/// #endif