/// #if SNOW_MODE=='local'
import settingModule from './module'
import settingComponent from './component';
import settingController from './controller';
import settingTemplate from './template.html';

describe('setting', () => {
  let $rootScope,$state, makeController;

  beforeEach(window.module(settingModule));
  beforeEach(inject((_$rootScope_, _$state_) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    makeController = () => {
      return new settingController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('subtitle');
      expect(controller).to.have.property('resolve');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(settingTemplate).to.match(/세팅/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = settingComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(settingTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(settingController);
    });
  });
});
/// #endif