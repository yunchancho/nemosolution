class Controller {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, $state) {
    'ngInject'
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, $state, localApis,){
    'ngInject'
    this.localApis = localApis;
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, $state, localApis,){
    'ngInject'
    this.localApis = localApis;
  /// #endif
    this.name = 'shell';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.breadcrumbs = []
    this.resolve = {
      setBreadcrumb: (title) => this.setBreadcrumb(title)
    }
    this.shell;
    $("body").removeClass("mini-navbar");
    $(".collapse").removeClass("in");
    $(".nav > li").removeClass("active");
  }

  $onInit() {
    this.$state.go("shell.shellList")
  }
  setBreadcrumb(title) {
    for (let i = 0; i < this.breadcrumbs.length; i++) {
      if (this.breadcrumbs[i] === title) {
        this.breadcrumbs.splice(i + 1, Number.MAX_SAFE_INTEGER);
        return;
      }
    }
    this.breadcrumbs.push(title);
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
}

export default Controller;
