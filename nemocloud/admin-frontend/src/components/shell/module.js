import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import shellList from './shellList/module'
import shellDetail from './shellDetail/module'
import addShell from './addShell/module'
import uploadShell from './uploadShell/module'
import 'angular-jsoneditor'

const name = 'shell';

let module = angular.module(name, [
	 uiRouter,
	 shellList,
	 shellDetail,
	 addShell,
	 uploadShell,
	 'angular-jsoneditor'
 ])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/shell',
			component: name
		});
}
