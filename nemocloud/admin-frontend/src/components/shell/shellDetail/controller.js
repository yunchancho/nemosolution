class Controller {
  constructor($rootScope, $state, deviceManager, modal, spinner, toastr) {
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.resolve;
    this.deviceManager = deviceManager;
    this.modal = modal;
    this.spinner = spinner;
    this.toastr = toastr;
    this.shell={};
    this.configs=[];
    this.options = {
      modes: ['tree', 'text']
    };
    this.daemon={
      id:"",
      path:""
    };
    this.input={
      nodeid:"",
      screenid:""
    }
    this.fullscreen={
      id:"",
      type:""
    };
    this.screens={
      nodeid:"",
      screenid:""
    };
    this.tooltips={
      daemons:{
        id:"백그라운드 프로그램의 식별자",
        path:"프로그램 실행파일의 절대경로"
      },
      screens:{
        nodeid:"기기 식별자이며 보통 0 으로 설정",
        screenid:"디스플레이 식별자",
        x:"시작할 x좌표",
        y:"시작할 y좌표",
        width:"디스플레이의 가로 해상도",
        height:"디스플레이의 세로 해상도",
        refresh: "디스플레이 재생빈도",
        scope:"스코프입니다."
      },
      inputs:{
        nodeid: "기기 식별자이며 보통 0 으로 설정",
        screenid: "디스플레이 식별자"
      },
      fullscreens:{
        id: "풀스크린 식별자",
        type: '총 3가지 타입 존재 (normal: "기본 타입", pick: "핀치줌 제스처", pitch: "던지는 제스처")',
        focus: "설명 필요..",
        fixed: "설명 필요",
        source: "디스플레이 내 적용 영역",
        destination: "디스플레이 내 결과 영역"
      },
      windows: {
         minimum: "최소 창 크기 정의",
         maximum: "최대 창 크기 정의",
         width: "가로 길이",
         height: "세로 길이"
      },
      screenshot:{
        interval : "스크린샷을 촬영하는 시간간격, 단위는 ms이다. (1초 = 1000ms)"
      },
      screensaver:{
        timeout : "일정 시간동안 아무런 활동이 없으면 스크린 세이버가 작동, 단위는 ms이다. (1초 = 1000ms)"
      }
    }
    this.index;
    this.shell_json;
  }

  $onInit() {
    this.resolve.setBreadcrumb('화면설정 수정')
    this.shell = JSON.parse(JSON.stringify(this.resolve.shell));
    this.shell_json = JSON.parse(JSON.stringify(this.resolve.shell));
    this.configs = this.resolve.configs;
    this.index = this.resolve.index;
  }

  addDaemon(){
    let input = JSON.parse(JSON.stringify(this.daemon));

    if(input.id === "" || input.path === ""){
      let params = {
          title: "쉘 추가 실패",
          content: "값을 입력해주세요."
      }
      this.modal.error(params);
      return;
    }

    for (let item of this.shell.daemons) {
        if (item.id === input.id || item.path === input.path) {
            let params = {
                title: '백그라운드 추가 실패',
                content: '이미 입력되어진 이름이 존재합니다.'
            }
            this.modal.error(params)
            return;
        }
    }
    this.shell.daemons.push(input)
  }

  addInput(){
    let item = JSON.parse(JSON.stringify(this.input));
    if(item.nodeid === "" || item.screenid===""){
      let params = {
          title: "쉘 추가 실패",
          content: "값을 입력해주세요."
      }
      this.modal.error(params);
      return;
    }
    for (let input of this.shell.inputs) {
        if (input.screenid === item.screenid) {
            let params = {
                title: '백그라운드 추가 실패',
                content: '이미 입력되어진 이름이 존재합니다.'
            }
            this.modal.error(params)
            return;
        }
    }
    this.shell.inputs.push(item)
  }
  addFullscreen(){
    let item = JSON.parse(JSON.stringify(this.fullscreen));
    if(item.id === "" || item.type===""){
      let params = {
          title: "쉘 추가 실패",
          content: "값을 입력해주세요."
      }
      this.modal.error(params);
      return;
    }
    for (let fullscreen of this.shell.fullscreens) {
        if (fullscreen.id === item.id) {
            let params = {
                title: '백그라운드 추가 실패',
                content: '이미 입력되어진 이름이 존재합니다.'
            }
            this.modal.error(params)
            return;
        }
    }

    this.shell.fullscreens.push(item)
  }
  addScreen(){
    let item = JSON.parse(JSON.stringify(this.screen));
    if(item.nodeid === "" || item.screenid===""){
      let params = {
          title: "쉘 추가 실패",
          content: "값을 입력해주세요."
      }
      this.modal.error(params);
      return;
    }
    for (let screen of this.shell.screens) {
        if (screen.screenid === item.screenid) {
            let params = {
                title: '백그라운드 추가 실패',
                content: '이미 입력되어진 이름이 존재합니다.'
            }
            this.modal.error(params)
            return;
        }
    }
    this.shell.screens.push(item)
  }

  apply(){
    console.log("인덱스 : ",this.index);
    let configs = [...this.configs]
    configs[this.index] = this.shell;
    this.spinner.on();

    return this.deviceManager.setShell(configs).then(result => {
        this.toastr.success("테마 설정이 완료 되었습니다.")
        this.$state.go("shell.shellList");
    }).catch(error => {
        let params = {
            title: "쉘 변경 실패",
            content: "쉘 변경에 실패하였습니다.",
            error: error.message
        }
        this.modal.error(params);
    }).then(() => this.spinner.off());
  }

  updateShell_json(){
    let configs = [...this.configs]
    configs[this.index] = this.shell_json;
    this.spinner.on();
    return this.deviceManager.setShell(configs).then(result => {
      this.toastr.success("테마 설정이 완료 되었습니다.")
      this.$state.go("shell.shellList");
    }).catch(error => {
      let params = {
        title: "쉘 변경 실패",
        content: "쉘 변경에 실패하였습니다.",
        error: error.message
      }
      this.modal.error(params);
    }).then(() => this.spinner.off());
  }

  back(){
    this.$state.go("shell.shellList")
  }

  confirmRemoveDaemon(index) {
    let params = {
      title: '컨텐츠 경로 삭제',
      content: '삭제하시겠습니까?',
      onConfirm: () => this.removeDaemon(index)
    }
    this.modal.confirm(params)
  }
  confirmRemoveInput(index) {
    let params = {
      title: '컨텐츠 경로 삭제',
      content: '삭제하시겠습니까?',
      onConfirm: () => this.removeInput(index)
    }
    this.modal.confirm(params)
  }
  confirmRemoveFullscreen(index) {
    let params = {
      title: '컨텐츠 경로 삭제',
      content: '삭제하시겠습니까?',
      onConfirm: () => this.removeFullscreen(index)
    }
    this.modal.confirm(params)
  }
  confirmRemoveScreen(index) {
    let params = {
      title: '컨텐츠 경로 삭제',
      content: '삭제하시겠습니까?',
      onConfirm: () => this.removeScreen(index)
    }
    this.modal.confirm(params)
  }
  refresh(){
    this.shell = JSON.parse(JSON.stringify(this.resolve.shell));
  }
  removeDaemon(index){
    this.shell.daemons.splice(index,1);
  }
  removeInput(index){
    this.shell.inputs.splice(index,1);
  }
  removeFullscreen(index){
    this.shell.fullscreens.splice(index,1);
  }
  removeScreen(index){
    this.shell.screens.splice(index,1);
  }
}

export default Controller;
