class Controller {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, $state, deviceManager, modal, spinner, toastr, logs) {
    this.logs = logs;
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, $state, deviceManager, modal, spinner, toastr) {
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, $state, deviceManager, modal, spinner, toastr) {
  /// #endif

    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.deviceManager = deviceManager;
    this.resolve;
    this.modal = modal;
    this.spinner = spinner;
    this.toastr = toastr;
    this.shell={};
    this.configs = [];
    this.deviceList;
    this.useDefaultId;
  }

  $onInit() {
    this.resolve.setBreadcrumb('화면설정 리스트')
    this.getDeviceList();
  }

  addShell() {
      this.resolve.configs = this.configs;
      this.$state.go("shell.addShell")
  }

  applyShell(index) {
      let params = {
          title: '화면설정 적용',
          content: `화면설정을 적용하시겠습니까?`,
          onConfirm: () => this.apply(index)
      }
      this.modal.confirm(params)
  }

  apply(shell){
    console.log(shell)
    return Promise.resolve()
    .then(() => {
      return this.deviceManager.applyShell(shell.id)
    })
    .then(() => this.toastr.success(`${shell.id}이 화면설정으로 적용됬습니다..`, '화면 설정 적용 성공'))
    .then(() => {
      this.useDefaultId = shell.id
      return this.getDeviceList()
    })
    /// #if SNOW_MODE=='cloud'
    .then(() => {
      let params = {
        menu: '화면설정',
        target: this.useDefaultId,
        ops: 'apply'
      }
      return this.logs.success(params)
    })
    /// #endif
    .catch(err => {
      console.error(err)
    })
    .then(() => {
      this.$rootScope.$$phase || this.$rootScope.$apply();
      this.spinner.off()
      return this.useDefaultId;
    })
  }

  changeIndex(){
    let configs = [...this.configs]
    this.spinner.on();
    return this.deviceManager.setShell(configs)
    .then(result => {console.log(result)})
    .catch(error =>{console.log(err)})
    .then(() => this.spinner.off())
  }

  getDeviceList() {
      this.spinner.on()
      return Promise.resolve()
      .then(() => {
        return this.deviceManager.getDeviceList()
      })
      .then(deviceList => {
          if (deviceList.length === 0) {
              throw new Error('No device')
          }
          this.deviceList = deviceList
          this.useDefaultId = deviceList[0].shell.useDefaultId
          // Copy shells array
          if (!deviceList[0].shell || !deviceList[0].shell.configs) {
            this.configs = [];
          } else {
            this.configs = [...deviceList[0].shell.configs]
          }
          this.$rootScope.$$phase || this.$rootScope.$apply();
          console.log("쉘",this.configs);
      }).then(()=>{
        return this.getShellImage()
      }).catch(err => {
          let params = {
              title: '오류',
              content: '목록을 가져올 수 없습니다.',
              error: err.message
          }
          this.modal.error(params)
      }).then(() => {

        this.spinner.off()
        return this.configs;
      })
  }

  getShellImage(){
    for(let shell of this.configs){
      let canvas = document.getElementById(shell.id);
      shell.screens.map(obj => {
        let ctx = canvas.getContext("2d");
        let x = (obj.x/50) + 100;
        let y = (obj.y/50) + 100;
        let width = (obj.width/50) ;
        let height = (obj.height/50) ;
        console.log("좌표 ", x, y, width, height);
        ctx.rect(x, y, width, height);
        ctx.stroke();
        if(obj.width === 1920) {
          ctx.fillText("1K",x+10,y+15);
        } else if(obj.width===3840){
          ctx.fillText("4K",x+10,y+25);
        } else {
          ctx.fillText(obj.width,x+10,y+15);
        }
      })
    }
  }

  removeShell(index) {
      let params = {
          title: '설정 삭제',
          content: `설정을 삭제하시겠습니까?`,
          onConfirm: () => this.remove(index)
      }
      this.modal.confirm(params)
  }

  remove(index) {

    if(this.deviceList[0].shell.useDefaultId === this.deviceList[0].shell.configs[index].id){
      let params = {
          title: '삭제 할 수 없습니다.',
          content: '현재 사용중인 쉘입니다.'
      }
      this.modal.error(params)
      return;
    }

    this.spinner.on()
    return Promise.resolve()
    .then(() => {
      let configs = [...this.deviceList[0].shell.configs]
      configs.splice(index, 1)
      return configs
    })
    .then(configs => this.deviceManager.setShell(configs))
    .then(result => console.log('removeShell', result))
    .then(() => { return this.getDeviceList() })
    .then(() => this.toastr.success(`쉘을 제거했습니다.`))
    .catch(err => {
      let params = {
        title: '쉘 제거 실패',
        content: '쉘 제거 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() =>{
      this.$rootScope.$$phase || this.$rootScope.$apply();
      this.spinner.off()
    })
  }
  uploadShell(){
    this.resolve.configs = this.configs;
    this.$state.go("shell.uploadShell")
  }

  updateShell(index) {
    this.resolve.configs = this.configs
    this.resolve.shell = this.configs[index];
    this.resolve.index = index;
    this.$state.go("shell.shellDetail");
  }
}

export default Controller;
