import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'uploadShell';

let module = angular.module(name, [
	 uiRouter,
 ])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('shell.uploadShell', {
			url: '/uploadShell',
			component: name
		});
}
