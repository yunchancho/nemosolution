class Controller {
    /// #if SNOW_MODE=='cloud'
    constructor($rootScope, $state, deviceManager, $uibModal, modal, spinner, toastr, packages) {
        'ngInject'
    /// #endif
    /// #if SNOW_MODE=='local'
    constructor($rootScope, $state, deviceManager, $uibModal, modal, spinner, toastr, packages, localApis) {
        'ngInject'
        this.localApis = localApis;
    /// #endif
    /// #if SNOW_MODE=='usb'
    constructor($rootScope, $state, deviceManager, $uibModal, modal, spinner, toastr, packages) {
        'ngInject'
    /// #endif
        this.packages = packages;
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.$uibModal = $uibModal;
        this.modal = modal;
        this.spinner = spinner;
        this.toastr = toastr;
        this.layers;
        this.services = [];
        this.service = {
            group:"",
            pkgname: "",
            width: 0,
            height: 0,
            x: 0,
            y: 0,
            layer: "",
            param: {}
        }
        this.params=[];
        this.deviceManager = deviceManager;
        this.deviceList = [];
        this.pkgList =[];
        this.pkgTheme=[];
        this.pkgs=[];
    }

    $onInit() {
      this.getDevice();
      this.listPackages();
    }

    addParams() {
      let param = {
        key: '',
        value: ''
      }
      this.params.push(param);
    }

    addBackgrounds(service) {
        if (service.group ==='' || service.pkgname === '' || service.layerId === '' || service.width === "" || service.height === "" || service.x === "" || service.y === "") {
            let params = {
                title: '백그라운드 추가 실패',
                content: '백그라운드 필수입력사항들을 입력해야 합니다.'
            }
            this.modal.error(params)
            return;
        }

        let value = JSON.parse(JSON.stringify(this.services));
        let input = JSON.parse(JSON.stringify(this.service));

        let keys = new Set();
        for (let param of this.params) {
            if (keys.has(param.key)) {
              let params = {
                  title: '백그라운드 추가 실패',
                  content: '이미 입력되어진 이름이 존재합니다.'
              }
              this.modal.error(params)
              return;
            } else {
              keys.add(param.key)
            }
        }
        //params의 key값이 중복되는지 체크메소드 추가

        let params = this.service.param

        for (let param of this.params) {
          params[param.key] = param.value;
        }

        this.service.param = params;
        this.services.push(this.service)
        this.service = {
            group:"",
            pkgname: "",
            width: 0,
            height: 0,
            x: 0,
            y: 0,
            layerId: (this.layers.length !== 0)
                ? this.layers[0].id
                : "",
            param: {}
        }
    }

    checkInstalled(pkg) {
      pkg.view.installed = this.deviceList[0].checkInstalled(pkg)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    }

    checkInstalledAll() {
      for (let pkg of this.pkgs) {
        this.checkInstalled(pkg)
      }
    }

    delParams(index) {
        this.params.splice(index, 1);
    }

    getDevice() {
      return this.deviceManager.getDeviceList()
      .then(deviceList => {
        this.deviceList = deviceList;
        console.log('Devices:', this.deviceList)
        this.$rootScope.$$phase || this.$rootScope.$apply();
      })
      .catch(err => {
        console.error('Get device fails', err);
        throw err
      })
    }

    getIndex() {
      /// #if SNOW_MODE=='cloud'
      return this.packages.getPackageIndex('theme')
      /// #endif
      /// #if SNOW_MODE=='local'
      return this.localApis.getPackageIndex('theme')
      /// #endif
      /// #if SNOW_MODE=='usb'
      return this.localApis.getPackageIndex('theme')
      /// #endif
      .then(result => {
        this.pkgs = this.packages.parsePackageIndex(result);
        console.log("this.pkgs : ",this.pkgs);
        // TODO: Move to service
        for (let pkg of this.pkgs) {
          pkg.view = {
            installed: '',
            mandatory: false,
            maintainer: {
              name: pkg.Maintainer.slice(0, pkg.Maintainer.indexOf('<') - 1),
            }
          }

          let pkgName = pkg.Package.toLowerCase()
        }
        this.$rootScope.$$phase || this.$rootScope.$apply();
      })
    }

    listPackages() {
      this.spinner.on()
      let prom;
      if (this.deviceList.length === 0) {
        prom = this.getDevice()
      } else {
        prom = Promise.resolve()
      }
      return prom.then(() => this.getIndex())
      .then(() => this.checkInstalledAll())
      .catch(err => {
        console.error('get package fails', err.message)
        let params = {
          title: '패키지',
          content: '패키지 리스트를 불러올 수 없습니다.',
          error: err.message
        }

        this.modal.error(params)
      })
      .then(() => this.spinner.off())
    }
}

export default Controller;
