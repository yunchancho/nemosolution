class Controller {
  constructor($rootScope, $state, $uibModal, modal, spinner, toastr){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.modal = modal;
    this.spinner = spinner;
    this.toastr = toastr;
    this.layers =[];
    this.layer = {
      id : ""
    }
  }

  $onInit() {

  }

  addLayer() {
    if (this.layer.id === '') {
      let params = {
        title: '레이어 추가 실패',
        content: '레이어 이름을 입력해야 합니다.'
      }
      this.modal.error(params)
      return;
    }
    let value = JSON.parse(JSON.stringify(this.layers));
    let input = JSON.parse(JSON.stringify(this.layer));

    for (let layer of value) {
      if (layer.id === input.id) {
        let params = {
          title: '백그라운드 추가 실패',
          content: '이미 입력되어진 이름이 존재합니다.'
        }
        this.modal.error(params)
        return;
      }
    }

    this.layers.push(this.layer);

    this.layer = {
      id : ""
    };
  }


}

export default Controller;
