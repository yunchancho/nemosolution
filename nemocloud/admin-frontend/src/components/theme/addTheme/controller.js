class Controller {
    constructor($rootScope, $state, deviceManager, $uibModal, modal, spinner, toastr) {
        'ngInject'
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.$uibModal = $uibModal;
        this.modal = modal;
        this.deviceManager = deviceManager;
        this.deviceList;
        this.spinner = spinner;
        this.toastr = toastr;
        this.id;
        this.layers = [];
        this.layer;
        this.services = [];
        this.service;
        this.options = {
          modes: ['tree', 'text']
        }
        this.configs;
        this.theme={
            "id": "",
            "services": [],
            "defaultLayerId": "",
            "fullscreenLayerId": "",
            "layers": []
          }
        this.sortableOptions = {
            update: (e, ui) => console.log(e, ui),
            axis: 'x'
        };
        this.fullscreenLayerId;
        this.defaultLayerId;
        this.pickedFile = {};
        this.onFileSelect = () => this.readFile()
        this.theme_json={
            "id": "",
            "services": [],
            "defaultLayerId": "",
            "fullscreenLayerId": "",
            "layers": []
          }
        this.mobileTabLayer = true;
        this.mobileTabService = false;
        this.mobileTabFullscreen = false;
        this.mobileTabDefault =false;
    }

    $onInit() {
        this.deviceList = this.resolve.deviceList;
        this.configs = this.resolve.configs;
        this.resolve.setBreadcrumb('새테마 만들기')
    }


    editLayer(index) {
        //전체 레이어중에 디폴트속성과 풀스크린 속성이 존재하는지 체크하고 만약 있으면, 더 추가하지 못하게 막아야함
        this.$uibModal.open({
            component: 'editLayerModal',
            resolve: {
                title: () => "레이어 수정",
                layers : () => this.layers,
                index : () => index
            },
            size: 'md'
        })
    }

    editBg(index) {
        this.$uibModal.open({
            component: 'editBgModal',
            resolve: {
                title: () => "레이어 수정",
                services:()=> this.services,
                layers : () => this.layers,
                index: ()=> index

            },
            size: 'md'
        })
    }

    tabOpen(val) {
      if (val === 'layer'){
        this.mobileTabService = false;
        this.mobileTabFullscreen = false;
        this.mobileTabDefault =false;
        this.mobileTabLayer = true;
      } else if (val === 'service'){
        this.mobileTabLayer = false;
        this.mobileTabDefault =false;
        this.mobileTabFullscreen = false;
        this.mobileTabService = true;
      } else if (val === 'default') {
        this.mobileTabLayer = false;
        this.mobileTabService = false;
        this.mobileTabFullscreen = false;
        this.mobileTabDefault =true;
      } else if (val === 'fullscreen') {
        this.mobileTabLayer = false;
        this.mobileTabService = false;
        this.mobileTabDefault =false;
        this.mobileTabFullscreen = true;
      }
    }

    removeLayer(index) {
        this.layers.splice(index, 1);
    }

    removeBg(index) {
        this.services.splice(index, 1);
    }

    addTheme() {
        if(this.id === "" || this.layers.length === 0 || this.services.length ===0) {
          let params = {
              title: '테마 추가 실패',
              content: '이름과 레이어 백그라운드를 추가해주세요.'
          }
          this.modal.error(params)
          return;
        }

        for(let item of this.configs){
          if(this.id === item.id){
            let params = {
                title: '테마 추가 실패',
                content: '같은 이름의 테마가 존재합니다.'
            }
            this.modal.error(params)
            return;
          }
        }

        let theme = {
            id: this.id,
            services: this.services,
            layers: this.layers,
            defaultLayerId:this.defaultLayerId,
            fullscreenLayerId:this.fullscreenLayerId
        }

        let configs = [...this.configs]

        configs.push(theme);
        this.spinner.on();
        return this.deviceManager.setTheme(configs).then(result => {
            this.toastr.success("테마 설정이 완료 되었습니다.")
            this.$state.go("theme.themeList");
        }).catch(error => {
            let params = {
                title: "테마 추가 실패",
                content: "테마 추가에 실패하였습니다.",
                error: error.message
            }
            this.modal.error(params);
        }).then(() => this.spinner.off());
    }
    addTheme_json(){
      if(this.theme_json.id === "" || this.theme_json.layers.length === 0 || this.theme_json.services.length ===0) {
        let params = {
            title: '테마 추가 실패',
            content: '이름과 레이어 백그라운드를 추가해주세요.'
        }
        this.modal.error(params)
        return;
      }
      let configs = [...this.configs]

      configs.push(this.theme_json);
      this.spinner.on();
      return this.deviceManager.setTheme(configs)
      .then(() => {
        this.deviceManager.refresh()
      })
      .then(result => {
          this.toastr.success("테마 설정이 완료 되었습니다.")
          this.$state.go("theme.themeList");
      }).catch(error => {
          let params = {
              title: "테마 추가 실패",
              content: "테마 추가에 실패하였습니다.",
              error: error.message
          }
          this.modal.error(params);
      }).then(() => this.spinner.off());
    }
}

export default Controller;
