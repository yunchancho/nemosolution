import angular from 'angular';
import uiRouter from 'angular-ui-router';
import addThemeAddLayer from './addThemeAddLayer/module';
import addThemeAddBg from './addThemeAddBg/module';
import 'angular-jsoneditor';
import 'angular-toastr/dist/angular-toastr.min.css';
import 'angular-toastr/dist/angular-toastr.tpls.js';

import component from './component';

const name = 'addTheme';

let module = angular.module(name, [
	uiRouter,
	addThemeAddLayer,
	addThemeAddBg,
	'angular-jsoneditor',
	'ui.sortable',
	'toastr'
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('theme.addTheme', {
			url: '/addTheme',
			component: name
		});
}
