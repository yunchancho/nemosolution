class Controller {
    /// #if SNOW_MODE=='cloud'
    constructor($rootScope, $state, deviceManager, modal, packages) {
        'ngInject'
    /// #endif
    /// #if SNOW_MODE=='local'
    constructor($rootScope, $state, deviceManager, modal, packages, localApis) {
        'ngInject'
        this.localApis = localApis;
    /// #endif
    /// #if SNOW_MODE=='usb'
    constructor($rootScope, $state, deviceManager, modal, packages) {
        'ngInject'
    /// #endif
        this.$rootScope = $rootScope
        this.$state = $state;
        this.title;
        this.modal = modal;
        this.packages = packages;
        this.content;
        this.layers;
        this.services;
        this.editBg = {
            group:"",
            pkgname: "",
            layerId: "",
            width: "",
            height: "",
            x: "",
            y: "",
            param: {}
        }
        this.params = [];
        this.errorMessage ="";
        this.deviceManager = deviceManager;
        this.deviceList = [];
        this.pkgList =[];
        this.pkgs=[]
    }

    $onInit() {
        this.getDevice();
        this.listPackages();
        this.title = this.resolve.title;
        this.services = this.resolve.services;
        this.layers = this.resolve.layers;
        this.index = this.resolve.index;

        this.editBg = JSON.parse(JSON.stringify(this.services[this.index]));
        console.log("에디트비쥐 : ",this.editBg);
        for (let key of Object.keys(this.editBg.param)) {
          this.params.push({ key, value: this.editBg.param[key] });
        }

        console.log("백그라운드 ", this.services[this.index]);
    }

    addParams() {
        this.params.push({ name: '', value: '' });
    }

    close() {
      this.close();
    }

    checkInstalled(pkg) {
      pkg.view.installed = this.deviceList[0].checkInstalled(pkg)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    }

    checkInstalledAll() {
      for (let pkg of this.pkgs) {
        this.checkInstalled(pkg)
      }
    }

    edit() {
        let keys = new Set();
        for (let param of this.params) {
            if (keys.has(param.key)) {
                this.errorMessage = "중복된 파람이 있습니다.";
                return;
            } else {
                keys.add(param.key)
            }
        }

        for (let param of this.params) {
            this.editBg.param[param.key] = param.value;
        }

        this.services[this.index] = this.editBg;


        this.close()
    }


    delParams(index) {
        console.log('!!!',this.params[index]);
        delete this.editBg.param[this.params[index].key];
        this.params.splice(index, 1);
    }

    getDevice() {
      return this.deviceManager.getDeviceList()
      .then(deviceList => {
        this.deviceList = deviceList;
        console.log('Devices:', this.deviceList)
        this.$rootScope.$$phase || this.$rootScope.$apply();
      })
      .catch(err => {
        console.error('Get device fails', err);
        throw err
      })
    }

    getIndex() {
      /// #if SNOW_MODE=='cloud'
      return this.packages.getPackageIndex('theme')
      /// #endif
      /// #if SNOW_MODE=='local'
      return this.localApis.getPackageIndex('theme')
      /// #endif
      /// #if SNOW_MODE=='usb'
      return this.localApis.getPackageIndex('theme')
      /// #endif
      .then(result => {
        this.pkgs = this.packages.parsePackageIndex(result);
        console.log("this.pkgs : ",this.pkgs);
        // TODO: Move to service
        for (let pkg of this.pkgs) {
          pkg.view = {
            installed: '',
            mandatory: false,
            maintainer: {
              name: pkg.Maintainer.slice(0, pkg.Maintainer.indexOf('<') - 1),
            }
          }

          let pkgName = pkg.Package.toLowerCase()
        }
        this.$rootScope.$$phase || this.$rootScope.$apply();
      })
    }

    listPackages() {
      let prom;
      if (this.deviceList.length === 0) {
        prom = this.getDevice()
      } else {
        prom = Promise.resolve()
      }
      return prom.then(() => this.getIndex())
      .then(() => this.checkInstalledAll())
      .catch(err => {
        console.error('get package fails', err.message)
        let params = {
          title: '패키지',
          content: '패키지 리스트를 불러올 수 없습니다.',
          error: err.message
        }

        this.modal.error(params)
      })
    }
}

export default Controller;
