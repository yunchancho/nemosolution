class Controller {
  constructor($state, modal){
    'ngInject'
    this.$state = $state;
    this.modal = modal;
    this.title;
    this.content;
    this.editLayer={
      id:""
    }
    this.errorMessage=""
  }

  $onInit() {
    this.title = this.resolve.title;
    this.layers = this.resolve.layers;
    this.index = this.resolve.index;
    this.editLayer = JSON.parse(JSON.stringify(this.layers[this.index]));
  }
  edit(){
    for (let layer of this.layers) {
        if (layer.id === this.editLayer.id && layer !== this.layers[this.index]) {
            this.errorMessage = "존재하는 레이어 이름입니다.";
            return;
        }
    }
    this.layers[this.index].id = this.editLayer.id;
  }

  close() {
    this.modal.close();
  }
}

export default Controller;
