import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'editLayerModal';

let module = angular.module(name, [
	uiRouter,
	])
	.component(name, component)
	.name

export default module;
