import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import themeList from './themeList/module';
import addTheme from './addTheme/module';
import uploadTheme from './uploadTheme/module';
import updateTheme from './updateTheme/module';
import editLayerModal from './editLayerModal/module';
import editBgModal from './editBgModal/module';

const name = 'theme';

let module = angular.module(name, [
	 uiRouter,
	 themeList,
	 addTheme,
	 uploadTheme,
	 updateTheme,
	 editLayerModal,
	 editBgModal,
 ])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/theme',
			component: name
		});
}
