import appConfig from './appConfig'

class Controller {
  /// #if SNOW_MODE=='cloud'
  constructor($rootScope, s3, $state, deviceManager, modal, spinner, toastr, logs) {
    this.logs = logs
    this.s3 = s3;
  /// #endif
  /// #if SNOW_MODE=='local'
  constructor($rootScope, $state, deviceManager, modal, spinner, toastr) {
  /// #endif
  /// #if SNOW_MODE=='usb'
  constructor($rootScope, $state, deviceManager, modal, spinner, toastr) {
  /// #endif
      'ngInject'
      this.$rootScope = $rootScope;
      this.$state = $state;
      this.modal = modal;
      this.spinner = spinner;
      this.toastr = toastr;
      this.resolve;
      this.theme;
      this.configs = [];
      this.deviceManager = deviceManager;
      this.deviceList = [];
      this.useDefaultId="";
      this.thumbnail;
    }

    $onInit() {
        this.resolve.setBreadcrumb('테마 리스트')
        this.getDeviceList();
    }

    addTheme() {
        this.resolve.configs = this.configs;
        this.resolve.deviceList = this.deviceList;
        this.$state.go("theme.addTheme")
    }

    uploadTheme() {
        this.resolve.configs = this.configs;
        this.resolve.deviceList = this.deviceList;
        this.$state.go("theme.uploadTheme")
    }

    applyTheme(theme) {
        let params = {
            title: '테마 적용',
            content: `테마를 적용하시겠습니까?`,
            onConfirm: () => this.apply(theme)
        }
        this.modal.confirm(params)
    }

    apply(theme){
      console.log(theme)
      this.spinner.on()
      return Promise.resolve()
      .then(() => {
        return this.deviceManager.applyTheme(theme.id)
      })
      .then(() => this.toastr.success(`${theme.id}이(가) 테마로 적용됬습니다..`, '테마 적용 성공'))
      .then(() => {
        this.useDefaultId = shell.id
        return this.getDeviceList()
      })
      /// #if SNOW_MODE=='cloud'
      .then(() => {
        let params = {
          menu: '테마',
          target: this.useDefaultId,
          ops: 'apply'
        }
        return this.logs.success(params)
      })
      /// #endif
      .catch(err => {
        console.error(err)
      })
      .then(() => {
        this.$rootScope.$$phase || this.$rootScope.$apply();
        this.spinner.off()
        return this.useDefaultId;
      })
    }

    changeIndex(){
      let configs = [...this.configs]
      this.spinner.on();
      return this.deviceManager.setTheme(configs)
      .then(() => {console.log("changeIndex")})
      .catch(error =>{console.log(err)})
      .then(() => this.spinner.off());
    }

    getDeviceList() {
        this.spinner.on()
        return Promise.resolve()
        .then(() => {
          return this.deviceManager.getDeviceList()
        })
        .then(deviceList => {
            if (deviceList.length === 0) {
                throw new Error('No device')
            }
            this.deviceList = deviceList
            this.useDefaultId = deviceList[0].theme.useDefaultId
            // Copy configs array
            if (!deviceList[0].theme || !deviceList[0].theme.configs) {
              this.configs = []
            } else {
              this.configs = [...deviceList[0].theme.configs]
            }
            this.$rootScope.$$phase || this.$rootScope.$apply();
            console.log("컨피그 : ",this.configs);
        }).then(()=>{
          return this.getThemeThumbnail();
        }).catch(err => {
            let params = {
                title: '오류',
                content: '목록을 가져올 수 없습니다.',
                error: err.message
            }
            this.modal.error(params)
        }).then(() => {
          this.spinner.off()
          return this.configs;
        })
    }
    getThemeThumbnail(){
      return this.configs.map(obj => {
        let keyword = obj.id.slice(0,obj.id.indexOf('-'));
        switch (keyword) {
          case 'nemoyoyo':
            return obj.thumbnail = "/assets/img/yoyo.jpg"
            break;
          case 'hyundai':
            return obj.thumbnail = "/assets/img/hyundai.jpg"
            break;
          case 'karim':
            return obj.thumbnail = "/assets/img/karim.jpg"
            break;
          case 'munhak':
            return obj.thumbnail = "/assets/img/munhak.jpg"
            break;
          case 'sge':
            return obj.thumbnail = "/assets/img/sge.jpg"
            break;
          case 'olympus':
            return obj.thumbnail = "/assets/img/olympus.jpg"
            break;
          case 'lotte':
            return obj.thumbnail = "/assets/img/lotte.jpg"
            break;
          default:
            return obj.thumbnail = "/assets/img/contents.png"
        }
      })
    }

    removeTheme(index) {
        let params = {
            title: '테마 삭제',
            content: `테마를 삭제하시겠습니까?`,
            onConfirm: () => this.remove(index)
        }
        this.modal.confirm(params)
    }

    remove(index) {

      if(this.deviceList[0].theme.useDefaultId === this.deviceList[0].theme.configs[index].id){
        let params = {
            title: '삭제 할 수 없습니다.',
            content: '현재 사용중인 테마입니다.'
        }
        this.modal.error(params)
        return;
      }

      this.spinner.on()
      return Promise.resolve()
      .then(() => {
        this.configs.splice(index, 1)
        let configs = this.configs
        return configs
      })
      .then(configs => this.deviceManager.setTheme(configs))
      .then(result => console.log('removeTheme', result))
      .then(() => { return this.getDeviceList() })
      .then(() => this.toastr.success(`테마를 제거했습니다.`))
      .catch(err => {
        let params = {
          title: '테마 제거 실패',
          content: '테마 제거 도중 오류가 발생했습니다.',
          error: err.message
        }
        this.modal.error(params)
      })
      .then(() =>{
        this.$rootScope.$$phase || this.$rootScope.$apply();
        this.spinner.off()

      })
    }

    updateTheme(index) {
      this.resolve.configs = this.configs;
      this.resolve.theme = this.configs[index];
      this.resolve.index = index;
      this.resolve.deviceList = this.deviceList;
      this.$state.go("theme.updateTheme");
    }

}

export default Controller;
