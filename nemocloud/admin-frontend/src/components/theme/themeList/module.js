import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-toastr/dist/angular-toastr.min.css';
import 'angular-toastr/dist/angular-toastr.tpls.js';

import component from './component';

const name = 'themeList';

let module = angular.module(name, [
	uiRouter,
	'toastr',
	/// #if SNOW_MODE=='usb'
	'app.commons.services.dynamoDB'
	/// #endif
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('theme.themeList', {
			url: '/themeList',
			component: name
		});
}
