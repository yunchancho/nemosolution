class Controller {
    constructor($rootScope, $state, deviceManager, $uibModal, modal, spinner, toastr) {
        'ngInject'
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.$uibModal = $uibModal;
        this.modal = modal;
        this.deviceManager = deviceManager;
        this.spinner = spinner;
        this.toastr = toastr;
        this.deviceList;
        this.configs;
        this.theme;
        this.options = {
          modes: ['tree', 'text']
        }
        this.theme_json;
        this.sortableOptions = {
            update: (e, ui) => console.log(e, ui),
            axis: 'x'
        };
        this.mobileTabService = false;
        this.mobileTabFullscreen = false;
        this.mobileTabDefault =false;
        this.mobileTabLayer = true;
    }

    $onInit() {
        this.deviceList = this.resolve.deviceList;
        this.configs = this.resolve.configs;
        this.theme = this.resolve.theme;
        this.index = this.resolve.index;
        console.log("테마 __ ",this.index);
        this.theme_json = JSON.parse(JSON.stringify(this.theme))
        this.resolve.setBreadcrumb('테마 수정')
    }

    editLayer(index) {
        this.$uibModal.open({
            component: 'editLayerModal',
            resolve: {
                title: () => "레이어 수정",
                layers : () => this.theme.layers,
                index: () => index
            },
            size: 'md'
        })
    }

    editBg(index) {
        this.$uibModal.open({
            component: 'editBgModal',
            resolve: {
                title: () => "레이어 수정",
                service : () => this.theme.services[index],
                services:()=> this.theme.services,
                layers : () => this.theme.layers,
                index: () => index
            },
            size: 'md'
        })
    }

    tabOpen(val) {
      if (val === 'layer'){
        this.mobileTabService = false;
        this.mobileTabFullscreen = false;
        this.mobileTabDefault =false;
        this.mobileTabLayer = true;
      } else if (val === 'service'){
        this.mobileTabLayer = false;
        this.mobileTabDefault =false;
        this.mobileTabFullscreen = false;
        this.mobileTabService = true;
      } else if (val === 'default') {
        this.mobileTabLayer = false;
        this.mobileTabService = false;
        this.mobileTabFullscreen = false;
        this.mobileTabDefault =true;
      } else if (val === 'fullscreen') {
        this.mobileTabLayer = false;
        this.mobileTabService = false;
        this.mobileTabDefault =false;
        this.mobileTabFullscreen = true;
      }
    }

    removeLayer(index) {
        this.theme.layers.splice(index, 1);
    }

    removeBg(index) {
        this.theme.services.splice(index, 1);
    }

    updateTheme() {
        if(this.theme.id ==="" || this.theme.layers.length === 0 || this.theme.services ===0) {
          let params = {
              title: '테마 추가 실패',
              content: '이름과 레이어 백그라운드를 추가해주세요.'
          }
          this.modal.error(params)
          return;
        }

        let configs = [...this.configs]

        configs[this.index] = this.theme;
        this.spinner.on();
        return this.deviceManager.setTheme(configs).then(result => {
            this.toastr.success("테마 설정이 완료 되었습니다.")
            this.$state.go("theme.themeList");
        }).catch(error => {
            let params = {
                title: "테마 추가 실패",
                content: "테마 추가에 실패하였습니다.",
                error: error.message
            }
            this.modal.error(params);
        }).then(() => this.spinner.off());
    }

    updateTheme_json(){
      if(this.theme_json.id ==="" || this.theme_json.layers.length === 0 || this.theme_json.services ===0) {
        let params = {
            title: '테마 추가 실패',
            content: '이름과 레이어 백그라운드를 추가해주세요.'
        }
        this.modal.error(params)
        return;
      }
      let configs = [...this.configs]
      configs[this.index] = this.theme_json;
      this.spinner.on();
      return this.deviceManager.setTheme(configs).then(result => {
          this.toastr.success("테마 설정이 완료 되었습니다.")
          this.$state.go("theme.themeList");
      }).catch(error => {
          let params = {
              title: "테마 추가 실패",
              content: "테마 추가에 실패하였습니다.",
              error: error.message
          }
          this.modal.error(params);
      }).then(() => this.spinner.off());
    }
}

export default Controller;
