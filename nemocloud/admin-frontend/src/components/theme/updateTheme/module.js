import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-jsoneditor'
import 'angular-toastr/dist/angular-toastr.min.css';
import 'angular-toastr/dist/angular-toastr.tpls.js';

import component from './component';

const name = 'updateTheme';

let module = angular.module(name, [
	uiRouter,
	'ui.sortable',
	'angular-jsoneditor',
	'toastr'
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('theme.updateTheme', {
			url: '/updateTheme',
			component: name
		});
}
