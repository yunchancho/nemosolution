class Controller {
    constructor($rootScope, $state, deviceManager, $uibModal, modal, spinner, toastr) {
        'ngInject'
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.$uibModal = $uibModal;
        this.modal = modal;
        this.deviceManager = deviceManager;
        this.deviceList;
        this.spinner = spinner;
        this.toastr = toastr;
        this.id;
        this.layers = [];
        this.layer;
        this.services = [];
        this.service;
        this.configs;
        this.theme;
        this.sortableOptions = {
            update: (e, ui) => console.log(e, ui),
            axis: 'x'
        };
        this.files=[];
        this.fullscreenLayerId;
        this.defaultLayerId;
        this.pickedFile;
        this.onFileSelect = () => this.readFile()
    }

    $onInit() {
        this.resolve.setBreadcrumb('새테마 만들기')
        this.deviceList = this.resolve.deviceList;
        this.configs = this.resolve.configs;
        this.theme_json = JSON.parse(JSON.stringify(this.theme))
    }

    _arrayBufferToString(buffer){
        let arr = new Uint8Array(buffer);
        let str = String.fromCharCode.apply(String, arr);
        if( /[\u0080-\uffff]/.test(str) ){
            throw new Error("this string seems to contain (still encoded) multibytes");
        }
        return str;
    }

    addTheme() {

        for(let theme of this.files){
          if(theme.id === "" || theme.layers.length === 0 || theme.services.length ===0) {
            let params = {
                title: '테마 추가 실패',
                content: '이름과 레이어 백그라운드를 추가해주세요.'
            }
            this.modal.error(params)
            return;
          }
          for(let item of this.configs){
            if(theme.id === item.id){
              let params = {
                title: '테마 추가 실패',
                content: '같은 이름의 테마가 존재합니다.'
              }
              this.modal.error(params)
              return;
            }
          }
          let configs = [...this.configs]
          configs.push(theme)
        }

        this.spinner.on();
        return this.deviceManager.setTheme(this.configs).then(result => {
            this.toastr.success("테마 설정이 완료 되었습니다.")
            this.$state.go("theme.themeList");
        }).catch(error => {
          console.error(error)
            let params = {
                title: "테마 추가 실패",
                content: "테마 추가에 실패하였습니다.",
                error: error.message
            }
            this.modal.error(params);
        }).then(() => this.spinner.off());
    }

    addTheme_json(){
      if(this.files.id === "" || this.files.layers.length === 0 || this.files.services.length ===0) {
        let params = {
            title: '테마 추가 실패',
            content: '이름과 레이어 백그라운드를 추가해주세요.'
        }
        this.modal.error(params)
        return;
      }
      let configs = [...this.configs]

      configs.push(this.theme);
      this.spinner.on();
      return this.deviceManager.setTheme(configs).then(result => {
          this.toastr.success("테마 설정이 완료 되었습니다.")
          this.$state.go("theme.themeList");
      }).catch(error => {
          let params = {
              title: "테마 추가 실패",
              content: "테마 추가에 실패하였습니다.",
              error: error.message
          }
          this.modal.error(params);
      }).then(() => this.spinner.off());
    }


    editLayer(index) {
        //전체 레이어중에 디폴트속성과 풀스크린 속성이 존재하는지 체크하고 만약 있으면, 더 추가하지 못하게 막아야함
        this.$uibModal.open({
            component: 'editLayerModal',
            resolve: {
                title: () => "레이어 수정",
                layers : () => this.layers,
                index : () => index
            },
            size: 'md'
        })
    }

    editBg(index) {
        this.$uibModal.open({
            component: 'editBgModal',
            resolve: {
                title: () => "레이어 수정",
                services:()=> this.services,
                layers : () => this.layers,
                index: ()=> index

            },
            size: 'md'
        })
    }

    readFile() {
      this.spinner.on()
      return Promise.resolve()
      .then(() => {
        //타입 구분
        let files = JSON.parse(this._arrayBufferToString(this.pickedFile[0].fileStream))
        if(Object.prototype.toString.call(files) === "[object Array]"){
          this.files = files
        } else if(Object.prototype.toString.call(files) === "[object Object]"){
          if(files.hasOwnProperty("useDefaultId")){
            this.files = files.configs
          } else{
            this.files.push(files)
          }

        }
        this.$rootScope.$$phase || this.$rootScope.$apply();
      })
      .catch(err => {
        console.error(err);
        this.theme = {}
        let params = {
          title: '읽기 실패',
          content: '파일을 읽어오는 도중 오류가 발생했습니다.',
          error: ''
        }
        this.modal.error(params)
      })
      .then(() => this.spinner.off())
    }

    removeLayer(index) {
        this.layers.splice(index, 1);
    }

    removeBg(index) {
        this.services.splice(index, 1);
    }


}

export default Controller;
