import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-toastr/dist/angular-toastr.min.css';
import 'angular-toastr/dist/angular-toastr.tpls.js';

import component from './component';

const name = 'uploadTheme';

let module = angular.module(name, [
	uiRouter,
	'ui.sortable',
	'toastr'
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('theme.uploadTheme', {
			url: '/uploadTheme',
			component: name
		});
}
