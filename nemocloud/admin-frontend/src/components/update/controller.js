class Controller {
  constructor($rootScope, localApis){
    'ngInject'
    this.$rootScope = $rootScope;
    this.localApis = localApis
  }
}

export default Controller;
