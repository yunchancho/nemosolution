import angular from 'angular';
import uiRouter from 'angular-ui-router';
import chartJs from 'angular-chart.js';
import controller from './controller';

import component from './component';

import updateUsb from './updateUsb/module'

const name = 'update';

let module = angular.module(name, [
	uiRouter,
	updateUsb,
	chartJs
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('update', {
			url: '/update',
			component: name
		});
}

export default module;
