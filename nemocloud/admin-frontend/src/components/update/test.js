/// #if SNOW_MODE=='local'
import updateModule from './module'
import updateComponent from './component';
import updateController from './controller';
import updateTemplate from './template.html';

describe('update', () => {
  let $rootScope, makeController;

  beforeEach(window.module(updateModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new updateController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();

      
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(updateTemplate).to.match(/업데이트/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = updateComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(updateTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(updateController);
    });
  });
});
/// #endif