class Controller {
  constructor($rootScope, $filter, $transitions, $uibModal, deviceManager, modal, spinner, localApis){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$filter = $filter;
    this.$transitions = $transitions;
    this.$uibModal = $uibModal
    this.deviceManager = deviceManager;
    this.modal = modal
    this.spinner = spinner;
    this.localApis = localApis;
    this.device = null;
    this.fetchInterval;
    this.runOnBoot = {
      content: false,
      package: false,
      config: false
    };
    this.graph = {
      labels: ["로컬 용량", "컨텐츠 용량", "패키지 용량", "설정 파일 용량", "남는 용량"],
      data: [0, 0, 0, 0, 0],
      options: {
      }
    },
    this.disk = {};
    this.usbs = []
    this.usbStatus = {
      config: [],
      contents: [],
      packages: [],
      view: {}
    }
  }

  $onInit() {
    this.getStatus()

    this.spinner.on()
    this.checkUsb()
    .then(() => this.spinner.off())
    this.fetchInterval = setInterval(() => this.checkUsb(), 5000)
  }

  $onDestroy() {
    clearInterval(this.fetchInterval)
  }

  checkSize() {
    let remain = this.disk.view.remain;
    let data = 0;

    if (this.runOnBoot.content) {
      data += this.usbStatus.view.contents.size
    }
    if (this.runOnBoot.package) {
      data += this.usbStatus.view.packages.size
    }
    if (this.runOnBoot.config) {
      data += this.usbStatus.view.config.size
    }
    
    return remain > data
  }

  checkUsb() {
    return this.getDeviceList()
    .then(() => this.getUsbStatus())
    .catch(err => {
      return true
    })
    .then(noUsb => this.getDiskInfo(noUsb))
    .catch(err => {
      let params = {
        title: '오류',
        content: '장치 정보를 가져올 수 없습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
  }

  getStatus() {
    this.spinner.on()
    this.localApis.getRestoreStatus()
    .then(result => {
      this.runOnBoot = result.runOnBoot;
      console.log('restore config: ', this.runOnBoot);
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('get status fails', err)
    })
    .then(() => this.spinner.off())
  }
  
  getDeviceList() {
    return this.deviceManager.getDeviceList()
    .then(deviceList => {
      this.device = deviceList[0];
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
  }
  
  // Get local disk info and apply graph
  getDiskInfo(noUsb) {
    return this.localApis.getSystemInfo('fssize')
    .then(result => {
      for (let fs of result) {
        if (fs.mount === '/') {
          this.disk = fs;
        }
      }
      
      this.disk.view = {
        total: Math.round(this.disk.size / 1048576),
        used: Math.round(this.disk.used / 1048576),
        remain: Math.round((this.disk.size - this.disk.used) / 1048576),
        enough: false
      }
      console.log('disk', this.disk)
      if (noUsb) {
        // Set graph datas
        this.graph.labels = ["로컬 용량", "남는 용량"];
        this.graph.data = [
          this.disk.view.used,
          this.disk.view.remain
        ]
      } else {
        // Set graph datas (include USB datas)
        let usbView = this.usbStatus.view;
        let size = usbView.config.size + usbView.contents.size + usbView.packages.size
        this.disk.view.enough = size < (this.disk.size - this.disk.used)
        this.graph.labels = ["로컬 용량", "컨텐츠 용량", "패키지 용량", "설정 파일 용량", "남는 용량"];
        this.graph.data = [
          this.disk.view.used,
          this.usbStatus.view.contents.size,
          this.usbStatus.view.packages.size,
          this.usbStatus.view.config.size,
          Math.round(this.disk.view.remain - this.usbStatus.view.contents.size - this.usbStatus.view.packages.size - this.usbStatus.view.config.size)
        ]
      }
      
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return false;
    })
    .catch(err => {
      console.error('get disk info fails', err)
      throw err;
    })
  }
  
  getUsbStatus() {
    return this.localApis.getUsbList('fssize')
    .then(result => {
      this.usbs = result
      if (this.usbs.length === 0) {
        throw new Error('no usbs')
      }
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return;
    })
    .then(() => this.localApis.getUsbStatus(this.usbs[0].fs[0], this.device.contentsync))
    .then(result => {
      this.usbStatus = result;
      this.usbStatus.view = {}
      this.usbStatus.view.config = this.usbStatus.config
      this.usbStatus.view.config.size = this.$filter('bytesFilter')(this.usbStatus.view.config.size, 'MB', 0)
      this.usbStatus.view.contents = this.reduceStatus(this.usbStatus.contents)
      this.usbStatus.view.packages = this.reduceStatus(this.usbStatus.packages)
      
      console.log('USB status', this.usbStatus)
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return;
    })
    .catch(err => {
      // init usbStatus
      this.usbStatus = {
        view: {
          config: {
            size: 0,
            count: 0
          },
          contents: {
            size: 0,
            count: 0
          },
          packages: {
            size: 0,
            count: 0
          },
        }
      }
      console.error('get usb status fails', err)
      throw err
    })
  }
  
  reboot() {
    this.spinner.on()
    this.localApis.reboot()
    .then(console.log('reboot...'))
    .catch(err => {
      console.error('reboot err', err)
      let params = {
        title: '재시작 실패',
        content: '장비 재시작 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(this.spinner.off())
  }
  
  reduceStatus(status) {
    let total = status.reduce((previous, current) => {
      return {
        size: previous.size + current.size,
        count: previous.count + current.count
      }
    })
    total.size = Math.round(total.size / 1048576)
    return total;
  }

  setStatus() {
    if (!this.checkSize()) {
      let params = {
        title: '업데이트 실패',
        content: '기기 저장공간이 부족합니다.'
      }

      this.modal.error(params);
      return;
    }

    this.spinner.on()
    this.localApis.setRestoreStatus(this.runOnBoot)
    .then(result => {
      console.log('setting restore config', result)
      let params = {
        title: '설정 완료',
        content: '업데이트는 재부팅 후에 적용됩니다.\n지금 재부팅하시겠습니까?',
        warning: '재부팅 완료까지 USB가 연결되어 있어야 합니다.',
        confirmButton: '지금 재시작',
        cancelButton: '나중에 재시작',
        onConfirm: () => this.reboot(),
      }
      this.modal.confirm(params)
      
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('set status fails', err)
    })
    .then(() => this.spinner.off())
  }

}

export default Controller;
