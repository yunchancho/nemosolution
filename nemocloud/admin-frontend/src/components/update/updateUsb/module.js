import angular from 'angular';
import uiRouter from 'angular-ui-router';
import chartJs from 'angular-chart.js';

import component from './component';

const name = 'updateUsb';

let module = angular.module(name, [
	uiRouter,
	chartJs,
	])
	.component(name, component)
	.name

export default module;
