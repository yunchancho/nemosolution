/// #if SNOW_MODE=='local'
import updateUSBModule from './module'
import updateUSBComponent from './component';
import updateUSBController from './controller';
import updateUSBTemplate from './template.html';

describe('updateUSB', () => {
  let $rootScope, makeController;

  beforeEach(window.module(updateUSBModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new updateUSBController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('$filter');
      expect(controller).to.have.property('$transitions');
      expect(controller).to.have.property('$uibModal');
      expect(controller).to.have.property('deviceManager');
      expect(controller).to.have.property('modal');
      expect(controller).to.have.property('spinner');
      expect(controller).to.have.property('localApis');
      expect(controller).to.have.property('device');
      expect(controller).to.have.property('runOnBoot');
      expect(controller).to.have.property('graph');
      expect(controller).to.have.property('disk');
      expect(controller).to.have.property('usbs');
      expect(controller).to.have.property('usbStatus');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(updateUSBTemplate).to.match(/업데이트/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = updateUSBComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(updateUSBTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(updateUSBController);
    });
  });
});
/// #endif