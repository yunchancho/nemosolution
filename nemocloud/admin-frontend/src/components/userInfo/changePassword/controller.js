class Controller {
  constructor($rootScope, $state, cognito, toastr, modal){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.cognito = cognito;
    this.toastr = toastr;
    this.modal = modal;
    this.newPassword = '';
    this.oldPassword = '';
    this.confirmPassword = '';

  }

  changePassword() {
    if (!this.checkNewPassword()) {
      return;
    }

    this.cognito.changePassword(this.oldPassword, this.newPassword)
    .then(result => {
      this.toastr.success("계정 정보를 수정했습니다.")
      this.newPassword = '';
      this.oldPassword = '';
      this.confirmPassword = '';
    })
    .catch(err => {
      let params = {
        title: '비밀번호 변경 실패',
        content: '비밀번호 변경에 실패했습니다.'
      }

      this.modal.error(params)
    })
  }

  checkNewPassword() {
    if (this.newPassword !== this.confirmPassword) {
      let params = {
        title: '비밀번호 변경 실패',
        content: '새 비밀번호와 비밀번호 확인이 다릅니다.',
      }

      this.modal.error(params)
      this.newPassword = '';
      this.oldPassword = '';
      this.confirmPassword = '';
    }

    return true;
  }
}

export default Controller;
