class Controller {
  constructor($rootScope, $state, $uibModal, cognito){
    'ngInject'
    this.name = 'dropOut';
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.cognito = cognito;
  }
  
  openDropOutModal() {
    this.$uibModal.open({
      component: 'dropOutModal',
      windowClass: 'animated flipInY'
    });
  }
  
}

export default Controller;
