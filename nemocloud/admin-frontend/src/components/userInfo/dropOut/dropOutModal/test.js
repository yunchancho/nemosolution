import dropOutModalModule from './module'
import dropOutModalComponent from './component';
import dropOutModalController from './controller';
import dropOutModalTemplate from './template.html';

describe('dropOutModal', () => {
  let $rootScope, makeController;

  beforeEach(window.module(dropOutModalModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new dropOutModalController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('$rootScope');
      expect(controller).to.have.property('name');
      expect(controller).to.have.property('cognito');
      expect(controller).to.have.property('confirm');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(dropOutModalTemplate).to.match(/계정 삭제/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = dropOutModalComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(dropOutModalTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(dropOutModalController);
    });
  });
});
