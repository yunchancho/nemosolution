import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import dropOutModal from './dropOutModal/module';

const name = 'dropOut';

let module = angular.module(name, [ 
	uiRouter,
	'app.commons.services.cognito',
	dropOutModal
	])
	.component(name, component)
	.name

export default module; 