import dropOutModule from './module'
import dropOutComponent from './component';
import dropOutController from './controller';
import dropOutTemplate from './template.html';

describe('dropOut', () => {
  let $rootScope, $state, makeController;

  beforeEach(window.module(dropOutModule));
  beforeEach(inject((_$rootScope_,_$state_) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    makeController = () => {
      return new dropOutController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('$rootScope');
      expect(controller).to.have.property('name');
      expect(controller).to.have.property('cognito');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(dropOutTemplate).to.match(/계정삭제/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = dropOutComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(dropOutTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(dropOutController);
    });
  });
});
