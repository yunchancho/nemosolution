class Controller {
  constructor($rootScope, $state, cognito, toastr, modal){
    'ngInject'
    this.name = 'updateAttributes';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.cognito = cognito;
    this.toastr = toastr;
    this.modal = modal;
    this.attributes = [];
    this.attributesNames = ['name', 'email', 'phone_number'];
    this.oldAttributes = [];
  }
  
  $onInit() {
    this.printAttributes();
  }
  
  copyAttributes(from) {
    let to = [];
    for (let key of this.attributesNames) {
      to[key] = from[key];
    }
    return to;
  }
  
  // Filter unchanged Attributes
  deferenceAttributes() {
    let changedAttributes = new Map;
    for (let key of this.attributesNames){
      if(this.oldAttributes[key] !== this.attributes[key]) {
        changedAttributes.set(key, this.attributes[key]);
      }
    }
    return changedAttributes;
  }
  
  printAttributes() {
    this.cognito.getUserAttributesMap()
    .then(attributesMap => {
      for (let key of this.attributesNames) {
        this.oldAttributes[key] = attributesMap.get(key);        
      }
      
      this.resetAttributes();
    })
    .catch(err => console.error('printAttributes error: ', err))
  }
  
  resetAttributes() {
    this.attributes = this.copyAttributes(this.oldAttributes);
    
    console.log('Attributes: ', this.attributes);
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
  
  
  // Call cognito to update attributes with the Map of changed attributes
  // If user update email or phone_number, email (or sms) including
  // verification code will be sended 
  updateAttributes() {
    let changedAttributes = this.deferenceAttributes();
    
    if (changedAttributes.size === 0) {
      console.log('Nothing changed');
      this.toastr.warning('수정사항이 없습니다.')
      return;
    }
    
    this.cognito.updateUserAttributes(changedAttributes)
    .then(result => {
      console.log('Update result: ' + result);
      this.oldAttributes = this.copyAttributes(this.attributes);
      
      this.toastr.success("계정 정보를 수정했습니다.")
      // TODO: Reload pages that using user attributes
      //       ex) common/view/user
      return result;
    })
    .catch(err => {
      let params = {
        title: '설정 실패',
        content: '계정 정보 수정 중 오류가 발생했습니다.',
        error: err.message
      }
      
      this.modal.error(params)
    })
  }
}

export default Controller;
