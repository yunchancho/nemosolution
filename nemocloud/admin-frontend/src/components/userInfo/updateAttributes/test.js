import updateAttributesModule from './module'
import updateAttributesComponent from './component';
import updateAttributesController from './controller';
import updateAttributesTemplate from './template.html';

describe('updateAttributes', () => {
  let $rootScope, $state, cognito, makeController;

  beforeEach(window.module(updateAttributesModule));
  beforeEach(inject((_$rootScope_, _$state_, cognito) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    cognito = cognito;
    makeController = () => {
      return new updateAttributesController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('$rootScope');
      expect(controller).to.have.property('$state');
      expect(controller).to.have.property('cognito');
      expect(controller).to.have.property('attributes');
      expect(controller).to.have.property('attributesNames');
      expect(controller).to.have.property('oldAttributes');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(updateAttributesTemplate).to.match(/정보 수정/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = updateAttributesComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(updateAttributesTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(updateAttributesController);
    });
  });
});
