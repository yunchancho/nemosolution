import verifyEmailModule from './module'
import verifyEmailComponent from './component';
import verifyEmailController from './controller';
import verifyEmailTemplate from './template.html';

describe('verifyEmail', () => {
  let $rootScope, $state, cognito, makeController;

  beforeEach(window.module(verifyEmailModule));
  beforeEach(inject((_$rootScope_, _$state_,_cognito_) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    cognito = _cognito_;
    makeController = () => {
      return new verifyEmailController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('cognito');
      expect(controller).to.have.property('code');
      expect(controller).to.have.property('emailVerified');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(verifyEmailTemplate).to.match(/이메일 인증/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = verifyEmailComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(verifyEmailTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(verifyEmailController);
    });
  });
});
