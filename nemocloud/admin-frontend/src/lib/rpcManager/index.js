import co from 'co';
import _ from 'underscore';
import Promise from 'bluebird';
import aws from 'aws-sdk';
import { thingShadow } from 'aws-iot-device-sdk';
import awsConfig from './aws-configuration';

const operationTimeout = 10000;
const topicTimeout = 5000;

class RpcManager {
  constructor(args, lifecycleCb) {
    this.members = {
      args: args,
      thingShadow: {},
      thingName: args.thingName,
      topicFrontendId: `${args.thingName}_frontend`,
      topicDeviceId: `${args.thingName}_device`,
      publishedTopicStack: [],
      reqStack: [],
      timeoutHandle: null,
      topics: [],
      lifecycleCb: lifecycleCb,
      lifecycleTopics: [
        `$aws/events/presence/connected/${args.thingName}_device`,
        `$aws/events/presence/disconnected/${args.thingName}_device`,
      ]
    }
  }

  initialize() {
    const initCoroutine = co.wrap(function* (self) { 
      let result = null;
      try {
        yield _setMembers(self);
        yield _updateAwsCredentials(self);
        yield _connectToAws(self);
        yield _registerShadowHandlers(self);
      } catch (err) {
        throw 'failed to execute generator: ' + err.stack;
      }

      return true;
    });

    return initCoroutine(this);
  }

  get config() {
    let promise = new Promise((resolve, reject) => {
      _commitOperationToAws(this, "get", {}, { resolve, reject })
    });

    return promise;
  }

  set config(config) {
    let promise = new Promise((resolve, reject) => {
      let data = {
        state: {
          desired: {}
        }
      };

      data.state.desired = config;
      _commitOperationToAws(this, "update", data, { resolve, reject })
    });

    return promise;
  }

  runDeviceCommand(payload) {
    let members = this.members
    let topic = _.findWhere(members.topics, { type: payload.type })

    let coroutine = co.wrap(function* () {
      if (!topic) {
        return Promise.reject('unknown payload type')
      }

      console.log('request payload: ', payload)

      // set unique request id using timestamp 
      payload.__reqid__ = Date.now()

      return new Promise((resolve, reject) => {
        members.thingShadow.publish(topic.pub,	JSON.stringify(payload))
        let timeout = setTimeout(() => {
          let foundIndex = members.publishedTopicStack.findIndex((item) => item.timeout === timeout)
          if (foundIndex < 0) {
            console.log('published topic not exists')
            return
          }

          members.publishedTopicStack[foundIndex].promise.reject(new Error('This request has been timeouted'))
          members.publishedTopicStack.splice(foundIndex, 1)
        }, topicTimeout)

        members.publishedTopicStack.push({ __reqid__: payload.__reqid__, pubTopic: topic.pub, promise: { resolve, reject }, timeout });
      })
    })

    return coroutine()
  }
}

function _setMembers(self)
{
  let members = self.members

  // defines specific topics 
  members.topics.push({ 
    type: 'devinfo',
    sub: members.thingName + '-devinfo-response',
    pub: members.thingName + '-devinfo-request',
    commands: [ 'listSounds', 'listTouches', 'listDisplayResolutions', 'listScreenshots', 'listIpaddrs', 'runCustomCommand' ]
  }, {
    type: 'powerctrl',
    sub: members.thingName + '-powerctrl-response',
    pub: members.thingName + '-powerctrl-request',
    commands: [ 'powerOn', 'powerOff', 'reboot', 'echo' ]
  });

  if (!_createShadow(self)) {
    return Promise.reject('failed to create shadow');
  }

  return Promise.resolve('success to create shadow');
}

function _registerShadowHandlers(self)
{
  let members = self.members
  let shadow = members.thingShadow;

  shadow.on('connect', function() {
    console.log('success to connect aws iot');
  });

  shadow.on('close', function() {
    console.log('close');
    shadow.unregister(members.thingName);
  });

  shadow.on('reconnect', function() {
    console.log('success to reconnect aws iot');
  });

  shadow.on('offline', function() {
    if (members.timeoutHandle !== null) {
      clearTimeout(members.timeoutHandle);
      members.timeoutHandle = null;
    }

    while (members.reqStack.length) {
      members.reqStack.pop();
    }
    console.log('offline');
  });

  shadow.on('error', function(error) {
    console.log('error: ', error);
  });

  shadow.on('status', function(thingName, stat, token, config) {
    let lastReq = members.reqStack.pop();
    if (lastReq.token === token) {
      console.log(thingName + '\'s ' + lastReq.op + ' stat: ' + stat);
      lastReq.promise.resolve(config);
    } else {
      console.log(thingName + '\'s received token is not mismatched (status)');
      lastReq.promise.reject("Oops, token mistmatched");
    }
    //console.log(JSON.stringify(config, null, '\t'));
  });

  shadow.on('timeout', function(thingName, clientToken) {
    let lastReq = members.reqStack.pop();
    if (lastReq.token === clientToken) {
      console.log(thingName + '\'s ' + lastReq.op + ' request is timeout');
    } else {
      console.log(thingName + '\'s received token is not mismatched (timeout)');
    }
  });

  shadow.on('message', function(topic, payload) {
    // check if this topic is reserved one for device connect/disconnect
    if (members.lifecycleTopics.includes(topic)) {
      if (!members.lifecycleCb) {
        return 
      }

      let res = JSON.parse(payload.toString())
      return members.lifecycleCb({
        timestamp: res.timestamp,
        type: res.eventType
      })
    }

    let found = _.findWhere(members.topics, { sub: topic });
    if (!found) {
      console.log('invalid topic: ', topic);
      return;
    }

    // TODO we need to resolve or reject it for promise created on topic request
    // pubTopic should include specific id per request
    // currently we remove last request of the topic

    const result = JSON.parse(payload.toString())
    const origRequest = members.publishedTopicStack.filter(i => (i.pubTopic == found.pub) && (i.__reqid__ == result.__reqid__))[0]
    console.log('responsed data: ', result)
    origRequest.promise.resolve(result)
    clearTimeout(origRequest.timeout)

    members.publishedTopicStack = _.without(members.publishedTopicStack, origRequest);
  });

  return Promise.resolve();
}

function _createShadow(self)
{
  let members = self.members
  let args = members.args;
  let option = {
    clientId: members.topicFrontendId,
    region: args.region,
    protocol: 'wss',
    keepalive: args.keepAlive,
    host: args.awsHost,
    debug: true,
    accessKeyId: '',
    secretKey: '',
    sessionToken: '',
    will: {
      topic: 'faketopic',
      paylaod: 'disconnected',
      qos: 0,
      retain: false
    }
  }

  try {
    members.thingShadow = thingShadow(option);
  } catch (err) {
    console.log('failed to make aws thingshadow: ', err);
    return false;
  }

  return true;
}

function _updateAwsCredentials(self)
{
  let members = self.members

  aws.config.region = awsConfig.region;
  aws.config.credentials = new aws.CognitoIdentityCredentials({
    IdentityPoolId: awsConfig.poolId
  });


  let cognitoIdentity = new aws.CognitoIdentity();
  return new Promise(function (resolve, reject) {
    aws.config.credentials.get(function(err, data) {
      if (!err) {
        console.log('retrieved identity: ' + aws.config.credentials.identityId);
        let params = {
          IdentityId: aws.config.credentials.identityId
        };
        cognitoIdentity.getCredentialsForIdentity(params, function(err, data) {
          if (!err) {
            //
            // Update our latest AWS credentials; the MQTT client will use these
            // during its next reconnect attempt.
            //
            members.thingShadow.updateWebSocketCredentials(
              data.Credentials.AccessKeyId,
              data.Credentials.SecretKey,
              data.Credentials.SessionToken);
            resolve("success");
          } else {
            console.log('error retrieving credentials: ' + err);
            reject(err);
          }
        });
      } else {
        console.log('error retrieving identity:' + err);
        reject(err);
      }
    });
  });
}

function _connectToAws(self)
{
  let members = self.members

  return new Promise(function (resolve, reject) {
    members.thingShadow.register(members.thingName, { 
      ignoreDeltas: true,
      operationTimeout: operationTimeout
    }, function (err, failedTopics) {
      if (!err && !failedTopics) {
        console.log('thing registered as aws iot');
        // subscribe specific topics
        console.log('topics: ', members.topics)
        for (let topic of members.topics) {
          members.thingShadow.subscribe(topic.sub);
        }
        for (let topic of members.lifecycleTopics) {
          members.thingShadow.subscribe(topic);
        }
        resolve(self);
      } else {
        reject(err);
      }
    });
  });
}

function _commitOperationToAws(self, operation, data = {}, promise = {})
{
  let members = self.members

  let token;
  if (operation === 'get' || operation == 'delete') {
    token = members.thingShadow[operation](members.thingName);
  } else if (operation === 'update') {
    token = members.thingShadow[operation](members.thingName, data);
  } else {
    return Promise.reject('unknown operation: ', operation);
  }

  if (token === null) {
    if (members.timeoutHandle !== null) {
      console.log('other operation not finished. This will be retried.. ');
      members.timeoutHandle = setTimeout(() =>
        _commitOperationToAws(self, operation, data),
        operationTimeout * 2);
    }	
  } else {
    members.reqStack.push({ op: operation, token: token, promise: promise });
  }

  return Promise.resolve();
}

module.exports = RpcManager;
