const path = require('path')
const webpack = require('webpack')
const ChunkManifestPlugin = require('chunk-manifest-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const InlineManifestWebpackPlugin = require('inline-manifest-webpack-plugin')
const WebpackChunkHash = require('webpack-chunk-hash')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
	node: {
		//console: 'empty',
		fs: 'empty',
		net: 'empty',
		tls: 'empty'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: [/node_modules/],
				use: [
					{
						loader: 'ng-annotate-loader'
					},
					{
						loader: 'babel-loader',
						options: {
							plugins: ['transform-runtime'],
  						presets: ['es2015']
						}
					}
				]
			},
			{
				test: /\.html$/,
				use: [{
					loader: 'html-loader',
					options: {
						attrs: ['img:src', 'img:ng-src']
					}
				}]
			},
      {
			 	test: /\.css$/,
				loaders: [ 'style-loader', 'to-string-loader', 'css-loader' ]
		 	},
			{
				test: /\.(png|jpg|gif)$/,
				loader: 'file-loader',
			},
			{
				test: /\.(woff|woff2|eot|ttf|svg)$/,
				use: [{
					loader: 'url-loader',
					options: {
						limit: 100000
					}
				}]
			}
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				// default is 'cloud'
				'SNOW_MODE': process.env.SNOW_MODE? JSON.stringify(process.env.SNOW_MODE): JSON.stringify('cloud')
			}
		}),
		// new webpack.optimize.CommonsChunkPlugin({
		// 	names: 'vendor',
		// 	minChunks: (module) => {
		// 		return module.context && !!~module.context.indexOf('node_modules')
		// 	}
		// }),
		new webpack.HashedModuleIdsPlugin(),
		/* if we use chunk hash plugin, we need not to consider manifest inlining for long term caching
		 */
		// new WebpackChunkHash(),
		new HtmlWebpackPlugin({
			template: './src/index.html',
			chunksSortMode: 'dependency'
		}),
		new CopyWebpackPlugin([{
			from: 'src/assets/',
			to: 'assets'
		}]),
		new webpack.ProvidePlugin({
			'window.jQuery': 'jquery',
			'window.$': 'jquery',
			$: 'jquery',
			jQuery: 'jquery'
		})
	]
}
