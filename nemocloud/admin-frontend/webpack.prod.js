const path = require('path')
const querystring = require('querystring')
const webpack = require('webpack')
const webpackMerge = require('webpack-merge')
const baseConfig = require('./webpack.common.js')

let ifdefOpt = {
	DEBUG: true,
	SNOW_MODE: process.env.SNOW_MODE || 'cloud'
}
let ifdefQuery = querystring.encode({json: JSON.stringify(ifdefOpt)})

let prodConfig = {
	devtool: 'cheap-module-source-map',
	entry: {
		main: './src/app/app.js'
	},
	output: {
		path: path.resolve(__dirname, 'prod'),
		filename: '[name].[chunkhash].bundle.js',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: [/node_modules/],
				use: [
					{
						loader: `ifdef-loader?${ifdefQuery}`
					}
				]
			}
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		}),
		new webpack.LoaderOptionsPlugin({
			minimize: true,
			debug: false
		}),
		/*
		 * UglifyJsPlugin doesn't work on ES6, so we need to add rules for babel
		 * this plugin execution is exclusive with source map creation
		 */
		new webpack.optimize.UglifyJsPlugin({
			beautify: false,
			mangle: {
				screw_ie8: true,
				keep_fnames: true,
				/*
				 * You can specify all variables that should not be mangled.
				 * For example if your vendor dependency doesn't use modules
				 * and relies on global variables. Most of angular modules relies on
				 * angular global variable, so we should keep it unchanged
				 */
				 //except: ['$super', '$', 'exports', 'require', 'angular']
			},
			comments: false
		})

		/*
		 * TODO we need to extract text for css files 
		 */
	]
}

module.exports = webpackMerge(baseConfig, prodConfig)
