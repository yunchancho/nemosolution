const path = require('path')
const webpack = require('webpack')
const ChunkManifestPlugin = require('chunk-manifest-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const InlineManifestWebpackPlugin = require('inline-manifest-webpack-plugin')
const WebpackChunkHash = require('webpack-chunk-hash')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const querystring = require('querystring')

const serverPort = 8080;

let meta = {
  username: 'test1',
  password: 'rnw3ign2eko41fkek',
  customer: 'test',
	cloudDevice: {
		number: 2
	},
  pkg: {
    "Architecture": "amd64",
    "Depends": "network-manager, openssh-server, curl",
    "Description": "nemoux's profile environment",
    "Filename": "shell/nemoprofile_0.0.1_amd64.deb",
    "Homepage": "http://www.nemoux.net",
    "Installed-Size": "38836",
    "MD5sum": "f2e40b97cd2b56bd3c90a32d9bf874ed",
    "Maintainer": "Taehwan Kim <taehwan@nemoux.net>,",
    "Package": "test",
    "Priority": "optional",
    "SHA1": "bbae19c2ce8540a2a806c4c57c858ac0866e68ab",
    "SHA256": "f55bdcd2ccc613aadb8940ead69b536aebffad21500e412fc2bcc1c7af003bc8",
    "Section": "graphics",
    "Size": "23186022",
    "Version": "0.0.1",
    "view": {
      "installed": "not installed",
      "maintainer": {
        "name": "Taehwan Kim",
      },
      "mandatory": false,
    }
  },
  pkgConfig: {
    name: 'test',
    config: {
      value1: '1',
      value2: '2',
      value3: '3',
    },
    bigdata: {
      use: false
    },
    graph: {
      value1: '1'
    }
  }
}

let ifdefOpt = {
	DEBUG: false,
	SNOW_MODE: process.env.SNOW_MODE || 'cloud'
}

let ifdefQuery = querystring.encode({json: JSON.stringify(ifdefOpt)})


module.exports = {
  node: {
		//console: 'empty',
		fs: 'empty',
		net: 'empty',
		tls: 'empty'
	},
  devtool: 'source-map',
	entry: ['./src/app/app.js'],
  output: {
		path: path.resolve(__dirname, 'test'),
		filename: '[name].bundle.js',
		publicPath: '/'
	},
  devServer: {
		hot: false,
		port: serverPort,
		compress: true,
		historyApiFallback: true,
		contentBase: path.resolve(__dirname, 'test'),
		publicPath: '/'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: [/node_modules/],
				use: [
					{
						loader: 'babel-loader',
						options: {
							plugins: ['transform-runtime'],
  						presets: ['es2015']
						}
					},
          {
            loader: `ifdef-loader?${ifdefQuery}`
          }
				]
			},
			{
				test: /\.html$/,
				use: [{
					loader: 'html-loader',
					options: {
						attrs: ['img:src', 'img:ng-src']
					}
				}]
			},
      {
			 	test: /\.css$/,
				loaders: [ 'style-loader', 'to-string-loader', 'css-loader' ]
		 	},
			{
				test: /\.(png|jpg|gif)$/,
				loader: 'file-loader',
			},
			{
				test: /\.(woff|woff2|eot|ttf|svg)$/,
				use: [{
					loader: 'url-loader',
					options: {
						limit: 100000
					}
				}]
			}
		]
	},
	plugins: [
    new webpack.DefinePlugin({
			'meta': JSON.stringify(meta),
      'SNOW_MODE': process.env.SNOW_MODE? JSON.stringify(process.env.SNOW_MODE): JSON.stringify('cloud'),
			'process.env': {
				'NODE_ENV': JSON.stringify('test'),
				'SNOW_MODE': JSON.stringify(process.env.SNOW_MODE || 'cloud')
			}
		}),
    // new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin(),
		new webpack.HashedModuleIdsPlugin(),
		/* if we use chunk hash plugin, we need not to consider manifest inlining for long term caching
		 */
		new HtmlWebpackPlugin({
			template: './src/index.html',
			chunksSortMode: 'dependency'
		}),
		new CopyWebpackPlugin([{
			from: 'src/assets/',
			to: 'assets'
		}]),
		new webpack.ProvidePlugin({
			'window.jQuery': 'jquery',
			'window.$': 'jquery',
			$: 'jquery',
			jQuery: 'jquery'
		})
	]
}
