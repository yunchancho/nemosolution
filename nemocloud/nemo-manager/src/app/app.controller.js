class AppController {
	constructor($rootScope, $state, $transitions, authModal, cognito, spinner) {
		'ngInject'
		this.name = 'app';
		this.$rootScope = $rootScope;
		this.$state = $state;
		this.$transitions = $transitions;
		this.authModal = authModal;
		this.cognito = cognito;
		this.spinner = spinner;
		this.isLogin = false;
		
		console.log("init app");
		
		this.addOnLoginFunction();
		this.watchTransition();
		
		$(document).ready(function () {
			// Full height of sidebar
			function fix_height() {
				var heightWithoutNavbar = $("body > #wrapper").height() - 61;
				$(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

				var navbarHeigh = $('nav.navbar-default').height();
				var wrapperHeigh = $('#page-wrapper').height();

				if(navbarHeigh > wrapperHeigh){
					$('#page-wrapper').css("min-height", navbarHeigh + "px");
				}

				if(navbarHeigh < wrapperHeigh){
					$('#page-wrapper').css("min-height", $(window).height()  + "px");
				}

				if ($('body').hasClass('fixed-nav')) {
					if (navbarHeigh > wrapperHeigh) {
						$('#page-wrapper').css("min-height", navbarHeigh + "px");
					} else {
						$('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
					}
				}

			}

			$(window).bind("load resize scroll", function() {
				if(!$("body").hasClass('body-small')) {
					fix_height();
				}
			});

			// Move right sidebar top after scroll
			$(window).scroll(function(){
				if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav') ) {
					$('#right-sidebar').addClass('sidebar-top');
				} else {
					$('#right-sidebar').removeClass('sidebar-top');
				}
			});

			setTimeout(function(){
				fix_height();
			})
		});

		// Minimalize menu when screen is less than 768px
		$(() => {
			$(window).bind("load resize", () => {
				this.minimalizeMenu();
			})
		})
		this.minimalizeMenu()
	}
	
	minimalizeMenu() {
		if ($(document).width() < 769) {
			console.log("create body-small");
			$('body').addClass('body-small')
		} else {
			console.log("remove body-small");
			$('body').removeClass('body-small')
		}
	}
	
	addOnLoginFunction() {
		this.cognito.addOnLoginFunction(this.name, () => {
			this.isLogin = true;
		})
	}
	
	// Check user session token is expired and unauthenticated user attempt to access states that doesn't permitted
	// If session has problem, try to retrive user session and refresh token.
	// If token is refreshed well, move to destination states.
	// if not, stop transition and open login modal
	checkLogin($state) {
		if (this.cognito.isLogin()) {
			return Promise.resolve();
		}
		
		// Session token expired
		return this.cognito.retrieveCurrentUser()
		.then(result => {
			this.authModal.close();
			return result;
		})
		.catch(err => {
			this.authModal.open('login');
			return err;
		})
		.then(result => {
			this.isLogin = this.cognito.isLogin();
			console.log(`Refresh Token: ${result}`);
		})
	}
	
	// Check login status on every state changes (include first loading)
	watchTransition() {
		this.$transitions.onStart({}, (trans) => {
			let $state = trans.router.stateService;
			// console.log($state.$current.name + '->', $state.params)
			return this.checkLogin($state);
		});
	}
	
}

export default AppController;
