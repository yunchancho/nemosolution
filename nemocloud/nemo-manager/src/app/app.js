import angular from 'angular';
import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import ngTranslate from 'angular-translate';
import ngSanitize from 'angular-sanitize';

import commons from '../commons/commons.module';
import components from '../components/components.module';

import appComponent from './app.component';
import appRouteConfig from './app.config.route';
import appTransConfig from './app.config.trans';

// In case of development
if (process.env.NODE_ENV !== 'production') {
	if (module.hot) {
		module.hot.accept();
	}
	// TODO how to monitor index.html on dev server
	//import './index.html';
}

const name = 'app';

angular.module(name, [
	uiRouter, uiBootstrap, ngTranslate, ngSanitize, commons, components
])
.config(appRouteConfig)
.config(appTransConfig)
.component(name, appComponent)
.run(initialize)

function initialize($rootScope, $state) {
	'ngInject';

	console.log("hello");
	$rootScope.$state = $state;
}

/*
angular.element(document).ready(() => {
	angular.bootstrap(document, [name]);
})
*/
