import angular from 'angular';
import attributes from './attributes.directive.js';

let module = angular.module('app.commons.attributes', []);

for (let attr of attributes) {
	module.directive(attr, attributes[attr]);
}
  
export default module.name;
