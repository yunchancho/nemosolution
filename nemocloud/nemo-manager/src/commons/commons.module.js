import angular from 'angular';

import views from './views/views.module';
import services from './services/services.module';
import attributes from './attributes/attributes.module';
import directive from './directive/module';
import filters from './filters/module';

let commons = angular.module('app.commons', [
	views,
	services,
	attributes,
	directive,
	filters
])
  
.name;

export default commons;
