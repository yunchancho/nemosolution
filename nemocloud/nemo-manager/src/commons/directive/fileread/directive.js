class Fileread {
  constructor($injector) {
    'ngInject'
    this.scope = {
      fileread: "=",
      onRead: "=",
    }
    this.spinner = $injector.get('spinner');
  }

  link(scope, element, attributes) {
    element.bind("change", (changeEvent) => {
      scope.fileread = [];
      if (changeEvent.target.files.length === 0) {
        return [];
      }
      this.spinner.on();
      for (let file of changeEvent.target.files) {
        let reader = new FileReader();
        reader.onload = (loadEvent) => {
          scope.$apply(() => {
            scope.fileread.push({
              file,
              fileStream: loadEvent.target.result
            })

            // check reading every file is finished
            if (scope.fileread.length === changeEvent.target.files.length) {
              if (scope.onRead) {
                scope.onRead();
              }
              console.log('Selected file:', scope.fileread)
              this.spinner.off()
            }
          });
        }

        reader.readAsArrayBuffer(file);
      }
    });
  }
}

export default Fileread;
