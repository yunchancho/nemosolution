import angular from 'angular';

import s3Directory from './s3Directory/module';

const module = angular.module('app.commons.filters', [
	s3Directory,
])
.name

export default module;
