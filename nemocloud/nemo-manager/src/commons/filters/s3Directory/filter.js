// Return S3 files in current directory
let directoryFilter = () => {
  return (input, directory) => {
    let out;
    
    if (!input || directory === undefined) {
      return '';
    }
    
    if (input.charAt(input.length - 1) === '/') {
      // directory
      out = input.slice(directory.length, input.length - 1)
    } else {
      // file
      out = input.slice(directory.length)
    }
    return out
  }
}

export default directoryFilter;