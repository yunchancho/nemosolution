import angular from 'angular';
import filter from './filter'

const name = 'directoryFilter'

const module = angular.module('app.commons.filters.s3Directory', [
])
.filter(name, filter)
.name;

export default module;
