import angular from 'angular';
import AuthModal from './service';

const module = angular.module('app.commons.services.authModal', [
])
.service('authModal', AuthModal)
.name;

export default module;
