// TODO: Keyboard covers input form in smartphone
class AuthModal {
  constructor($rootScope, $uibModal){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.currentModal;
  }
  
  close() {
    if (!!this.currentModal) {
      this.currentModal.close();
    }
  }
  
  // Open or replace component based modal
  // and return closed promise
  open(component, resolveMap) {
    if (this.currentModal) {
      this.currentModal.close();
    }
    
    let resolve = {
      map: () => {return resolveMap}
    };
    
    this.currentModal = this.$uibModal.open({
      backdrop: false,
      backdropClass: 'gray-bg',
      openedClass: 'gray-bg',
      component,
      resolve,
      keyboard: false,
      windowClass: 'gray-bg modal-scroll'
    });
    
    return this.currentModal;
  }
}

export default AuthModal;