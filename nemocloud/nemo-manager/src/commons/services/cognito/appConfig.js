export default {
  region: 'ap-northeast-2',
  IdentityPoolId: 'ap-northeast-2:51e24413-8dbd-4259-95a6-233d2faa9ece',
  UserPoolId: 'ap-northeast-2_63OCojYdy',
  ClientId: '3ihg0ao0sutf1q7m9662ef1h5v',
  attributeNames: {
    customer: 'custom:customer',
    permission: 'custom:permission',
    type: 'custom:type',
  }
}
