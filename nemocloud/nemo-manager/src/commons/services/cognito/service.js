import 'aws-sdk';
import * as AWSCognito from 'amazon-cognito-identity-js';
import { CognitoSyncManager } from 'amazon-cognito-js';
import Promise from 'bluebird'
import appConfig from './appConfig'

const userPool = Promise.promisifyAll(
  new AWSCognito.CognitoUserPool({
    UserPoolId: appConfig.UserPoolId,
    ClientId: appConfig.ClientId,
  })
);

AWS.config.region = appConfig.region;
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: appConfig.IdentityPoolId,
});


class CognitoService {
  constructor() {
    this.cognitoUser;
    this.refreshTimer = null
    this.onLoginFunctionMap = new Map();
    this.onLogoutFunctionMap = new Map();

  }


  // Register on login listener
  addOnLoginFunction(key, func) {
    this.onLoginFunctionMap.set(key, func);
  }

  // Register on logout listener
  addOnLogoutFunction(key, func) {
    this.onLogoutFunctionMap.set(key, func);
  }


  changePassword(oldPassword, newPassword) {
    if (!this.isLogin()) {
      return Promise.reject(new Error('Not logged in'));
    }
    return this.cognitoUser.changePasswordAsync(oldPassword, newPassword);
  }


  // Delete current logged in user (drop out)
  deleteUser() {
    if (!this.isLogin()) {
      return Promise.reject(new Error('Not logged in'));
    }
    return this.cognitoUser.deleteUserAsync()
    .then(result => {
      this.signOut();
      return result;
    })
  }
  
  getConfig() {
    return appConfig;
  }

  // Get specfied attribute from AWS
  // Boolean attribute(email_verified, phone_number_verified, ...)
  // return string 'true' or 'false' (not boolean value)
  getUserAttribute(attributeName) {
    if (!this.isLogin()) {
      return Promise.reject(new Error('Not logged in'));
    }

    return this.cognitoUser.getUserAttributesAsync()
    .then(result => {
      for (let attribute of result) {
        if (attribute.getName() === attributeName) {
          return attribute.getValue();
        }
      }
      
      // User doesn't have specified attribute
      return '';
    })
  }


  // Return user attributes by Map object
  getUserAttributesMap() {
    return this.cognitoUser.getUserAttributesAsync()
    .then(result => {
      let attributesMap = new Map();
      for (let attribute of result) {
        attributesMap.set(attribute.getName(), attribute.getValue());
      }
      return attributesMap;
    })
  }


  // Doesn't have to be log in
  getUsername() {
    if (!this.cognitoUser) {
      return;
    }
    return this.cognitoUser.getUsername();
  }

  // Initiate this.cognitoUser (not login)
  initCognitoUser(username) {
    let userData = {
      Username : username,
      Pool : userPool
    };
    this.cognitoUser = Promise.promisifyAll(new AWSCognito.CognitoUser(userData));
  }

  // Checks the session to determine whether it is currently logged in
  // !cognitoUser: Not logged in
  // !signInUserSession: Login fail or after registering user
  // !isValid: Token expired (1 hour passed from login)
  isLogin() {
    if ( !this.cognitoUser
      || !this.cognitoUser.signInUserSession
      || !this.cognitoUser.signInUserSession.isValid()) {
      return false;
    }
    return true;
  }

  // Authenticate user by username and password
  // After authenticate success, call onLoginSuccess()
  // Open password challenge Page if user was registered by admin
  // and it is first login
  login(username, password) {
    return new Promise((resolve, reject) => {
      let authenticationData = {
        Username : username,
        Password : password,
      };
      let authenticationDetails = new AWSCognito.AuthenticationDetails(authenticationData);
      
      this.initCognitoUser(username);
      this.cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: (result) => {
          this.onLoginSuccess(result)
          .then(resolve, reject);
        },
        onFailure: (err) => {
          return reject(err);
        },
        newPasswordRequired: (userAttributes, requiredAttributes) => {
          let error = new Error('New password required');
          error.attributes = [userAttributes, requiredAttributes];
          return reject(error)
        }
      });
    });
  }
  
  newPasswordChallenge(newPassword, attributesData) {
    return new Promise((resolve, reject) => {
      let onloginParam = {
        onSuccess: (result) => {
          this.onLoginSuccess(result)
          .then(resolve, reject);
        },
        onFailure: (err) => {
          return reject(err);
        },
        newPasswordRequired: (userAttributes, requiredAttributes) => {
        }
      }
      
      this.cognitoUser.completeNewPasswordChallenge(newPassword, attributesData, onloginParam);
    });
  }

  // Handle login success and get session(refresh browser)
  // Set AWS credentials and get keys
  onLoginSuccess(result) {
			let credentialsParam = {
        IdentityPoolId : appConfig.IdentityPoolId,
				Logins: {}
			};
      let userPoolUrl = `cognito-idp.${appConfig.region}.amazonaws.com/${appConfig.UserPoolId}`;
			credentialsParam.Logins[userPoolUrl] = result.getIdToken().getJwtToken();

      AWS.config.credentials = Promise.promisifyAll(new AWS.CognitoIdentityCredentials(credentialsParam));

      console.log('Logged in User: ', this.cognitoUser);
      console.log('AWS credentials: ', AWS.config.credentials);

      return AWS.config.credentials.getAsync()
      .then(() => {
        // Instantiate aws sdk service objects now that the credentials have been updated.
        // Or can register instantiate function on onLoginFunctionMap by addOnLoginFunction() function
        // example: let s3 = new AWS.S3();
        //      or: cognito.addOnLoginFunction(()=> {
        //            let s3 = new AWS.S3();
        //          })

        // Run registered on login function
        for (let [key, value] of this.onLoginFunctionMap) {
          console.log(`onLogin: ${key}`);
          value();
        }
        
        let refreshTime = AWS.config.credentials.expireTime.valueOf() - (new Date()).valueOf() + 3000
        this.refreshTimer = setTimeout(() => this.retrieveCurrentUser(), refreshTime)

        return 'Login Success';
      })
      .catch(err => {
        this.signOut();
        throw err;
      })
    }

  removeOnLoginFunction(key) {
    this.onLoginFunctionMap.delete(key);
  }

  // Retrieve the current user from local storage
  // If user logged out, cognitoUser is null
  //
  // If already retrieve the user, no-op
  retrieveCurrentUser() {
    if (this.isLogin()) {
      return Promise.reject(new Error('Already logged in'));
    }

    this.cognitoUser = userPool.getCurrentUser();
    if (this.cognitoUser === null) {
      return Promise.reject(new Error('No user'));
    }
    this.cognitoUser = Promise.promisifyAll(this.cognitoUser);

    return this.cognitoUser.getSessionAsync()
    .then(result => this.onLoginSuccess(result))
  }

  // Run registered on logout function
  runOnLogoutFunction() {
    for (let [key, func] of this.onLogoutFunctionMap) {
      console.log(`onLogout: ${key}`);
      func();
    }
  }


  // Send verificate code by using sms message or email
  // Verificate is finished in verificateAttribute()
  sendAttributeVerificateCode(attribute) {
    return new Promise((resolve, reject) => {
      if (!this.isLogin()) {
        return reject(new Error('Not logged in'));
      }
      
      let phoneVerificateHandler = {
        onSuccess: (result) => {},
        onFailure: (err) => {
          // Called when sending message is fail
          return reject(err);
        },
        inputVerificationCode() {
          // Called after sending verificate message
          return resolve('Code sended');
        }
      }
      this.cognitoUser.getAttributeVerificationCode(attribute, phoneVerificateHandler);
    });
  }


  // Logout
  // Delete user session and aws credentials
  signOut() {
    if (!this.isLogin()) {
      console.log('Not logged in');
      return;
    }
    this.cognitoUser.signOut();
    this.permisson = null;

    AWS.config.credentials.clearCachedId();
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: appConfig.IdentityPoolId,
    });
    clearTimeout(this.refreshTimer)

    this.runOnLogoutFunction();

    console.log('signed out');
  }


  // Modify user information according to attributesMap
  updateUserAttributes(attributesMap) {
    if (!this.isLogin()) {
      return Promise.reject(new Error('Not logged in'));
    }
    let attributeList = [];
    for (let [Name, Value] of attributesMap) {
      let attribute = {
        Name,
        Value
      }
      attributeList.push(attribute);
    }

    return this.cognitoUser.updateAttributesAsync(attributeList);
  }

  // Verify user attribute by verificationCode sended in sendAttributeVerificateCode()
  verificateAttribute(attribute, verificationCode) {
    return new Promise((resolve, reject) => {
      if (!this.isLogin()) {
        return reject(new Error('Not logged in'));
      }
      
      let verificateHandler = {
        onSuccess: (result) => {
          return resolve(result);
        },
        onFailure: (err) => {
          return reject(err);
        },
        inputVerificationCode() {
          // Not used in here
        }
      }
      
      this.cognitoUser.verifyAttribute(attribute, verificationCode, verificateHandler);
    });
  }
}

export default CognitoService;
