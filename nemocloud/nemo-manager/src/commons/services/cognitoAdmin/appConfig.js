export default {
  userPoolId: 'ap-northeast-2_8rFlu47uh',
  managerPoolId: 'ap-northeast-2_63OCojYdy',
  attributes: [
    'name',
    'email',
    'custom:customer',
    'custom:permission',
    'custom:type'
  ],
  types: [
    'user',
    'installer'
  ]
}
