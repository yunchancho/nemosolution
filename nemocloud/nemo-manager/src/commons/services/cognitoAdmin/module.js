import angular from 'angular';
import CognitoAdminService from './service';

const module = angular.module('app.commons.services.cognitoAdmin', [
  'app.commons.services.cognito'
])
.service('cognitoAdmin', CognitoAdminService)
.name;

export default module;
