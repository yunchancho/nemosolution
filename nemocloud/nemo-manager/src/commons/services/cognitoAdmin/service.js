import 'aws-sdk';
import Promise from 'bluebird';
import appConfig from './appConfig';
import Parallel from '../../../lib/parallel/index.js'


// Module for top layer admin
// Manages cognito user pool
class CognitoAdminService {
  constructor(cognito) {
    'ngInject'
    this.cognito = cognito;
    this.serviceProvider;
    this.userPoolId = appConfig.userPoolId;

    this.init();
    this.initOnLogin();
  }
  
  // Run functions on manager user pool
  setManagerPool(manager) {
    if (manager) {
      this.userPoolId = appConfig.managerPoolId;
    } else {
      this.userPoolId = appConfig.userPoolId;
    }
  }

  confirmUser(user) {
    let params = {
      UserPoolId: this.userPoolId,
      Username: user.Username
    }

    return this.serviceProvider.adminConfirmSignUpAsync(params)
  }

  confirmUsers(userList) {
    return Parallel.asyncAllPromise(userList, (user) => {
      return this.confirmUser(user)
    });
  }

  createUser(username, password, attributes) {
    let params = {
      UserPoolId: this.userPoolId,
      Username: username,
      TemporaryPassword: password,
      UserAttributes: attributes
    }

    return this.serviceProvider.adminCreateUserAsync(params)
  }

  deleteUser(user) {
    let params = {
      UserPoolId: this.userPoolId,
      Username: user.Username
    }

    return this.serviceProvider.adminDeleteUserAsync(params)
  }

  deleteUsers(userList) {
    return Parallel.asyncAllPromise(userList, (user) => {
      return this.deleteUser(user)
    });
  }

  disableUser(user) {
    let params = {
      UserPoolId: this.userPoolId,
      Username: user.Username
    }

    return this.serviceProvider.adminDisableUserAsync(params)
  }

  disableUsers(userList) {
    return Parallel.asyncAllPromise(userList, (user) => {
      return this.disableUser(user)
    });
  }

  enableUser(user) {
    let params = {
      UserPoolId: this.userPoolId,
      Username: user.Username
    }

    return this.serviceProvider.adminEnableUserAsync(params);
  }

  enableUsers(userList) {
    return Parallel.asyncAllPromise(userList, (user) => {
      return this.enableUser(user)
    });
  }
  
  getConfig() {
    return appConfig
  }

  getModifiableAttributes() {
    return appConfig.attributes;
  }

  init() {
    this.serviceProvider = Promise.promisifyAll(new AWS.CognitoIdentityServiceProvider());
  }

  initOnLogin() {
    this.cognito.addOnLoginFunction('cognitoAdmin', () => {
      this.init();
    });
  }

  // Get user's login device list
  listDevices(user) {
    let params = {
      UserPoolId: this.userPoolId,
      Username: user.Username,
      Limit: 0
    }

    return this.serviceProvider.adminListDevicesAsync(params)
      
  }


  // Fetch user (with attributes) list from server
  listUsers() {
    let params = {
      UserPoolId: this.userPoolId
    };
    return this.serviceProvider.listUsersAsync(params)
  }

  resetUserPassword(user) {
    let params = {
      UserPoolId: this.userPoolId,
      Username: user.Username
    };

    return this.serviceProvider.adminResetUserPasswordAsync(params)
  }


  resetUsersPassword(userList) {
    return Parallel.asyncAllPromise(userList, (user) => {
      return this.resetUserPassword(user)
    });
  }

  updateUserAttribute(user) {
    let params = {
      UserAttributes: user.Attributes,
      UserPoolId: this.userPoolId,
      Username: user.Username
    }
    console.log(params)
    return this.serviceProvider.adminUpdateUserAttributesAsync(params)
  }
}

export default CognitoAdminService;
