import angular from 'angular';
import DbManager from './service';

const module = angular.module('app.commons.services.dbManager', [
  'app.commons.services.cognito'
])
.service('dbManager', DbManager)
.name;

export default module;
