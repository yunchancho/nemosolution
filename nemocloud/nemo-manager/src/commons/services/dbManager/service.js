class DbManager {
  constructor($rootScope, dynamoDB){
    'ngInject'
    this.$rootScope = $rootScope;
    this.dynamoDB = dynamoDB;
  }

  // Return list of checked item
  filterChecked(list) {
    let checkedList = [];
    for (let item of list) {
      if (item.check) {
        checkedList.push(item);
      }
    }
    return checkedList;
  }
}

export default DbManager;
