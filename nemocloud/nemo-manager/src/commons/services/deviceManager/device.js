class Device {
  constructor(name, services) {
    this.name = name;
    this.dynamoDB = services.dynamoDB;
    this.iot = services.iot;
    this.pkgConfig = [];
    this.attributes = {};
    this.bigdata = [];
    this.contentsync = [];
    this.mandatorypkgs = [];
    this.pkgindex = [];
    this.pkglist = [];
    this.thingTypeName = '';
  }
  
  addContents(content) {
    // refresh device to reduce concurrency problem
    return this.fetch()
    .then(() => {
      if (this.hasContents(content.name)) {
        throw new Error('Already has that contents');
      }
    })
    .then(() => this.dynamoDB.addContentsSource(this.name, content))
    .then(() => this.fetch())
  }
  
  // Not used in frontend
  // TODO: Concurrency problem on adding after nemo-snow add package with mongodb id
  addPackageIndex(pkgIndex) {
    // refresh device to reduce concurrency problem
    return this.fetch()
    .then((result) => {
      if (this.hasPkgIndex(pkgIndex)) {
        throw new Error('Already in package');
      }
    })
    .then(() => this.dynamoDB.addPackageIndex(this.name, pkgIndex.name))
    .then(() => this.fetch())
  }
  
  // Decide status by dynamoDB items
  checkInstalled(app) {
    let appName = app.Package.toLowerCase();
    for (let pkgItem of this.pkglist) {
      if (pkgItem.pkgname === appName) {
        return 'installed';
      }
    }
    return 'not installed';
  }
  
  // Scan device table items from dynamoDB
  // and store in device object
  fetch() {
    return this.dynamoDB.scanDevice(this.name)
    .then((result) => {
      let config = this.dynamoDB.getConfig();
      this.pkgConfig = [];
      for (let item of result) {
        let pkgConfigFlag = true;
        for (let defaultKey of Object.keys(config.deviceItems)) {
          if (item.name === config.deviceItems[defaultKey]) {
            let length = config.deviceItems[defaultKey].length
            this[config.deviceItems[defaultKey].slice(1, length - 1)] = item.value;
            pkgConfigFlag = false;
            break;
          }
        }
        if (pkgConfigFlag) {
          this.pkgConfig.push(item);
        }
      }

      return null;
    })
    .then(() => this.iot.describeDevice(this))
  }
  
  hasContents(name) {
    for (let content of this.contentsync) {
      if (content.name === name) {
        return true;
      }
    }

    return false;
  }
  
  // Install specified app to all devices in deviceList
  // After adding package name in dynamoDB, snow agent will install package and update db with app configuration
  // TODO: Concurrency problem on installing app while pending delete and
  // immediately after install
  installPackage(pkg, pkgIndex) {
    let pkgName = pkg.Package.toLowerCase();
    // Refresh installed app list and check app status
    // by fetchDevice to reduce concurrency problem
    return this.fetch()
    .then(result => {
      // Check device has in app's package
      if(!this.hasPkgIndex(pkgIndex)) {
        throw new Error('Device is not in packages');
      }

      if (this.checkInstalled(pkg) !== 'not installed') {
        throw new Error('Already installed or pending');
      }
    })
    .then(() => this.dynamoDB.installPackage(this.name, pkgName))
    .then(() => this.fetch(this))
  }
  
  // Return device is in the package or not
  hasPkgIndex(pkgIndex) {
    for (let index of this.pkgindex) {
      if (index.dir === pkgIndex.name + '/') {
        return true;
      }
    }
    return false;
  }
  
  removeContents(name) {
    return this.fetch()
    .then(() => {
      if (!this.hasContents(name)) {
        throw new Error(`${this.name} doesn't have target contents`);
      }

      // Find package index
      let index = -1;
      for (let i = 0; i < this.contentsync.length; i++) {
        if (this.contentsync[i].name === name) {
          index = i;
          break;
        }
      }
      if (index === -1) {
        // Local data doesn't match with db.
        // Refresh(fetch) data
        this.fetch();
        throw new Error(`Contents list index error`);
      }

      return index;
    })
    .then(index => this.dynamoDB.removeContentsSource(this.name, name, index))
    .then(() => this.fetch())
  }
  
  removePkgIndex(pkgIndex) {
    return this.fetch()
    .then(() => {
      if (!this.hasPkgIndex(pkgIndex)) {
        throw new Error(`${this.name} doesn't have target package`);
      }

      // Find package index
      let index = -1;
      for (let i = 0; i < this.pkgindex.length; i++) {
        if (this.pkgindex[i].dir === pkgIndex.name + '/') {
          index = i;
          break;
        }
      }
      if (index === -1) {
        // Local data doesn't match with db.
        // Refresh(fetch) data
        this.fetch();
        throw new Error(`Package list index error`);
      }

      return index;
    })
    .then(index => this.dynamoDB.removePkgIndex(this.name, pkgIndex.name, index))
    .then(() => this.fetch())
  }
  
  // Replace app configuration in dynamoDB
  setPkgConfig(pkg, config) {
    let pkgName = pkg.Package.toLowerCase();
    return this.fetch()
    .then(() => {
      if (this.checkInstalled(pkg) !== 'installed') {
        throw new Error(`Not installed`)
      }
    })
    .then(() => this.dynamoDB.updatePkgConfig(this.name, pkgName, config))
    .then(() => this.fetch())
  }
  
  updatePkgRegistry(pkg, registry) {
    let pkgName = pkg.Package.toLowerCase();
    return this.fetch()
    .then(() => {
      if (this.checkInstalled(pkg) !== 'installed') {
        throw new Error(`Not installed`);
      }

      // Find package index
      let index = -1;
      for (let i = 0; i < this.pkglist.length; i++) {
        if (this.pkglist[i].pkgname === pkgName) {
          index = i;
          break;
        }
      }
      if (index === -1) {
        // Local data doesn't match with db.
        // Refresh(fetch) data
        this.fetch();
        throw new Error(`Package list index error`);
      }
      return index;
    })
    .then(index => this.dynamoDB.updatePkgRegistry(this.name, pkgName, index, registry))
    .then(() => this.fetch())
  }
  
  updateContents(content) {
    return this.fetch()
    .then(() => {
      if (!this.hasContents(content.name)) {
        throw new Error(`Doesn't have contents`);
      }

      // Find package index
      let index = -1;
      for (let i = 0; i < this.contentsync.length; i++) {
        if (this.contentsync[i].name === content.name) {
          index = i;
          break;
        }
      }
      if (index === -1) {
        // Local data doesn't match with db.
        // Refresh(fetch) data
        this.fetch();
        throw new Error(`Contents list index error`);
      }

      return index;
    })
    .then(index => this.dynamoDB.updateContentsSource(this.name, content, index))
    .then(() => this.fetch())
  }
  
  // Unnstall specified app on all devices in deviceList
  // After adding package name in dynamoDB, snow agent will install package and update db with app configuration
  uninstallPackage(pkg) {
    let pkgName = pkg.Package.toLowerCase();
    return this.fetch()
    .then(() => {
      if (this.checkInstalled(pkg) !== 'installed') {
        throw new Error(`${this.name} is not installed`);
      }
      
      // Find package index
      let index = -1;
      for (let i = 0; i < this.pkglist.length; i++) {
        if (this.pkglist[i].pkgname === pkgName) {
          index = i;
          break;
        }
      }
      if (index === -1) {
        // Local data doesn't match with db.
        // Refresh(fetch) data
        this.fetch();
        throw new Error(`Package list index error`);
      }

      return index;
    })
    .then(index => this.dynamoDB.uninstallPackage(this.name, pkgName, index))  
    .then(() => this.fetch())
  }
}

export default Device;