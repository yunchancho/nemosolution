import angular from 'angular';
import DeviceManager from './service';

const module = angular.module('app.commons.services.deviceManager', [
  'app.commons.services.cognito',
  'app.commons.services.dynamoDB',
  'app.commons.services.lambda',
])
.service('deviceManager', DeviceManager)
.name;

export default module;
