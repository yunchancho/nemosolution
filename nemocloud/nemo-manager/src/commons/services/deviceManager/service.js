import Device from './device';
import Parallel from '../../../lib/parallel/index.js'

//TODO: Makes only one deviceList exist
class DeviceManager {
  constructor($rootScope, cognito, dynamoDB, lambda, iot){
    'ngInject'
    this.$rootScope = $rootScope;
    this.cognito = cognito;
    this.dynamoDB = dynamoDB;
    this.lambda = lambda;
    this.iot = iot;
    this.deviceList = [];
    this.parallel = new Parallel();

    this.destroyOnLogout();
  }

  // TODO: Concurrency problem on adding after nemo-snow add package with mongodb id
  addPackageIndex(pkg) {
    return Parallel.asyncAllPromise(this.deviceList, (device) => {
      return device.addPackageIndex(pkg);
    });
  }

  destroyOnLogout() {
    this.cognito.addOnLogoutFunction('deviceManager', () => {
      this.deviceList = [];
    })
  }

  // Call fetchDevice function to each device concurrently
  fetchDevices() {
    return Parallel.asyncAllPromise(this.deviceList, (device) => {
      return device.fetch();
    })
    .then(result => this.deviceList)
  }

  // Fetch user accessible device list by lambda
  listDevices() {
    return this.dynamoDB.listTables()
    .then(tableNames => {
      let nonDeviceSet = new Set();
      let nonDeviceTables = this.dynamoDB.getTableNames();
      for (let key in nonDeviceTables) {
        nonDeviceSet.add(nonDeviceTables[key]);
      }

      for (let i = 0; i < tableNames.length; i++) {
        if (nonDeviceSet.has(tableNames[i])) {
          tableNames.splice(i, 1);
          i--;
        }
      }

      return tableNames;
    })
  }

  // Return device list to caller(ex: controller)
  // If deviceList not initiated or empty, fetch list from dynamoDB
  getDeviceList() {
    return new Promise((resolve, reject) => {
      if (this.deviceList.length !== 0) {
        return resolve(this.deviceList);
      }

      let pass = this.parallel.p(() => {
        resolve(this.deviceList);
      })
      if (!pass) {
        // Wait until the first one completes fetching
        return;
      }

      console.log('Init deviceList');
      this.listDevices()
      .then((result) => {
        let services = {
          dynamoDB: this.dynamoDB,
          iot: this.iot
        }
        this.deviceList = result.map((deviceName) => {
          return new Device(deviceName, services);
        })
      })
      .then(() => this.fetchDevices())
      .then(result => resolve(this.deviceList))
      .then(result => console.log('Device List:', this.deviceList))
      .catch(err => {
        this.deviceList = []
        reject(err)
      })
      .then(() => {
        // Return deviceList to awaiter even if error occured
        this.parallel.v();
      })
    });
  }

  hasPkgIndexAll(pkgIndex) {
    return this.deviceList.map(device => {
      return {
        name: device.name,
        hasPkgIndex: device.hasPkgIndex(pkgIndex)
      }
    })
  }
  
  refresh() {
    this.deviceList = [];
    return this.getDeviceList();
  }

  removePkgIndex(pkgIndex) {
    return Parallel.asyncAllPromise(this.deviceList, (device) => {
      return device.removePkgIndex(pkgIndex);
    });
  }
}

export default DeviceManager;
