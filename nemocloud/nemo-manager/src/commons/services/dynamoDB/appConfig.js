export default {
  tables: {
    customer: 'Customer',
    certificate: 'Certificate',
    registry: 'Registry'
  },
  customerItems: {
    prefixMeta: 'prefixmeta'
  },
  deviceItems: {
    bigdata: '_bigdata_',
    contentSync: '_contentsync_',
    packageList: '_pkglist_',
    packageIndex: '_pkgindex_',
    mandatoryPackages: '_mandatorypkgs_',
    power: '_power_',
    theme: '_theme_',
    shell: '_shell_',
  },
  localEndpoint: `http://${self.location.hostname}:8000`
}
