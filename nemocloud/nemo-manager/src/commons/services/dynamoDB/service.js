import 'aws-sdk';
import '../cognito/module';
import Promise from 'bluebird';
import appConfig from './appConfig';

class DynamoDB {
  constructor($rootScope, cognito){
    'ngInject'
    this.$rootScope = $rootScope;
    this.cognito = cognito;
    this.docClient;
    this.dynamoDB;
    this.mode = 'cloud';

    this.init();
    this.initOnLogin();
  }

  // TODO: Condition and concurrency problem with same name and different prefix
  addContentsSource(deviceName, content) {
    let src;
    if (content.src.startsWith('/')) {
      src = `${appConfig.contentsSrcPrefix}${content.src}`
    } else {
      src = `${appConfig.contentsSrcPrefix}/${content.src}`
    }
    let target = {
      name: content.name,
      dest: content.dest.startsWith('/')?content.dest:`/${content.dest}`,
      src
    };

    if (target.name === '') {
      return Promise.reject(new Error('Input name'))
    }

    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.contentSync
      },
      ConditionExpression: `NOT contains(#v, :chk)`,
      UpdateExpression: `set #v = list_append(#v, :source)`,
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':source': [target],
        ':chk': target
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  // Add package index(group) to device
  // TODO: ConditionExpression doesn't apply to mongodb item(has _id attribute)
  addPackageIndex(deviceName, pkgName) {
    let target = {
      dir: pkgName + '/',
      host: 's3://nemosnow-packages.s3-ap-northeast-1.amazonaws.com',
      type: 'deb'
    };

    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.packageIndex
      },
      ConditionExpression: `NOT contains(#v, :chk)`,
      UpdateExpression: 'set #v = list_append(#v, :pkg)',
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':pkg': [target],
        ':chk': target
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  createCustomer(customer) {
    let params = {
      TableName: appConfig.tables.customer,
      ConditionExpression: 'attribute_not_exists(#n)',
      ExpressionAttributeNames: {
        '#n': 'name'
      },
      Item: {
        name: customer.name,
        nickname: customer.nickname,
        description: customer.description,
      }
    }

    return this.docClient.putAsync(params);
  }

  deleteCustomer(customer) {
    let params = {
      TableName: appConfig.tables.customer,
      Key: {
        'name': customer.name
      }
    };

    return this.docClient.deleteAsync(params);
  }

  deleteCustomers(customerList) {
    if (customerList.length === 0) {
      return Promise.resolve();
    }

    let params = {
      RequestItems: {}
    };
    params.RequestItems[appConfig.tables.customer] = [];

    for (let customer of customerList) {
      params.RequestItems[appConfig.tables.customer].push({
        DeleteRequest: {
          Key: {
            name: customer.name
          }
        }
      })
    }

    return this.docClient.batchWriteAsync(params)
  }

  describeTable(tableName) {
    let params = {
      TableName: tableName
    };

    return this.dynamoDB.describeTableAsync(params);
  }
  
  getConfig() {
    return appConfig;
  }

  getCustomer(name) {
    let params = {
      TableName: appConfig.tables.customer,
      Key: {
        name
      }
    }

    return this.docClient.getAsync(params)
    .then(data => data.Item)
  }

  // Return non-device table names array
  getTableNames() {
    return appConfig.tables;
  }

  init() {
    if (this.mode === 'cloud') {
      // Show cloud database(default)
      this.dynamoDB = Promise.promisifyAll(new AWS.DynamoDB());
    } else if (this.mode === 'local') {
      // Show local database
      this.dynamoDB = Promise.promisifyAll(new AWS.DynamoDB({endpoint: 'http://localhost:8000'}));
    }
    
    let params = {service: this.dynamoDB};
    this.docClient = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient(params));
  }

  initOnLogin() {
    this.cognito.addOnLoginFunction('dynamoDB', () => {
      this.init();
    })
  }


  // Add package name in device package list
  // Update occurs only when package list doesn't
  // contain target package
  // ConditionExpression prevents installing app
  // to device on pending install
  installApp(deviceName, pkgName) {
    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.packageList
      },
      ConditionExpression: `NOT contains(#v, :pkg)`,
      UpdateExpression: 'set #v = list_append(#v, :app)',
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':app': [{pkgname: pkgName}],
        ':pkg': {pkgname: pkgName}
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  listTables() {
    return this.dynamoDB.listTablesAsync({})
    .then(result => result.TableNames)
  }

  // Remove package(index) from device
  removeContentsSource(deviceName, dest, index) {
    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.contentSync
      },
      ConditionExpression: `#v[${index}].dest = :d`,
      UpdateExpression: `remove #v[${index}]`,
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':d': dest
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }
  
  removeHashKey(object) {
    let removeHash = (current) => {
      if (typeof current === 'object') {
        delete current['$$hashKey']
        for (let key in current) {
          removeHash(current[key])
        }
      }
    }
    console.log('dynamodb', object)
    let clone = JSON.parse(JSON.stringify(object))
    removeHash(clone);
    return clone;
  }

  // Remove package(index) from device
  removePkgIndex(deviceName, pkgName, pkgIndex) {
    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.packageIndex
      },
      ConditionExpression: `#v[${pkgIndex}].dir = :dir`,
      UpdateExpression: `remove #v[${pkgIndex}]`,
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':dir': pkgName + '/'
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }


  // Return all companies in database
  scanCustomer() {
    let params = {
      TableName: appConfig.tables.customer
    }

    return this.docClient.scanAsync(params)
    .then(data => data.Items)
  }

  scanDevice(deviceName) {
    let params = {
      TableName: deviceName
    }

    return this.docClient.scanAsync(params)
    .then(data => data.Items)
  }
  
  setMode(mode) {
    this.mode = mode
    this.init()
  }

  // Implement after customer having attribute
  updateCustomer(customer) {
    let params = {
      TableName: appConfig.tables.customer,
      Key: {
        name: customer.name
      },
      ConditionExpression: 'attribute_exists(#n)',
      UpdateExpression: 'set #d = :d, #nn = :nn',
      ExpressionAttributeNames: {
        '#d': 'description',
        '#n': 'name',
        '#nn': 'nickname'
      },
      ExpressionAttributeValues: {
        ':d': customer.description,
        ':nn': customer.nickname,
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params)
  }
  
  updateItem(deviceName, key, item, value) {
    let names = item.split('.')
    let attributeNames = {};
    let updateNames = ''
    for (let i = 0; i < names.length; i++) {
      attributeNames[`#n${i}`] = names[i];
      updateNames = `${updateNames}.#n${i}`
    }
    updateNames = updateNames.slice(1)
    
    let params = {
      TableName: deviceName,
      Key: {
        name: key
      },
      UpdateExpression: `set ${updateNames} = :v`,
      ExpressionAttributeNames: attributeNames,
      ExpressionAttributeValues: {
        ':v': this.removeHashKey(value),
      },
      ReturnValues: 'UPDATED_NEW'
    };
    
    console.log(params)
    return this.docClient.updateAsync(params)
  }
      
    

  // Update device's package configuration
  updatePkgConfig(deviceName, pkgname, config) {
    let params = {
      TableName: deviceName,
      Key: {
        name: pkgname
      },
      ConditionExpression: 'attribute_exists(#v)',
      UpdateExpression: 'set #v = :config',
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':config': config
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }

  // Remove package from device package list
  // Removing uses index which is fetched to local store
  // If the index changes or doesn't match, throw error
  uninstallApp(deviceName, pkgName, pkgIndex) {
    let params = {
      TableName: deviceName,
      Key: {
        name: appConfig.deviceItems.packageList
      },
      ConditionExpression: `#v[${pkgIndex}].pkgname = :p`,
      UpdateExpression: `remove #v[${pkgIndex}]`,
      ExpressionAttributeNames: {
        '#v': 'value'
      },
      ExpressionAttributeValues: {
        ':p': pkgName
      },
      ReturnValues: 'UPDATED_NEW'
    };

    return this.docClient.updateAsync(params);
  }
}

export default DynamoDB;
