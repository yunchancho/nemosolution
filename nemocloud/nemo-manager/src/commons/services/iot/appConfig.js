export default {
  status: {
    initial: 'initial',
    deactivated: 'deactivated',
    activated: 'activated', 
    waitConfirm: 'wait_confirm'
  },
  mode: {
    local: 'local',
    cloud: 'cloud'
  },
  policy: 'NemoSnowBasicIoT',
  deviceCodeLength: 6
}
