import angular from 'angular';
import Iot from './service';

const module = angular.module('app.commons.services.iot', [
  'app.commons.services.cognito',
  'app.commons.services.dynamoDB'
])
.service('iot', Iot)
.name;

export default module;
