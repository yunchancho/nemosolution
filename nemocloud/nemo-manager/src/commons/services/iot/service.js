import 'aws-sdk';
import Promise from 'bluebird'
import appConfig from './appConfig'
import Parallel from '../../../lib/parallel/index.js'


class Iot {
  constructor($rootScope, cognito, dynamoDB, lambda, s3){
    'ngInject'
    this.$rootScope = $rootScope;
    this.cognito = cognito;
    this.dynamoDB = dynamoDB;
    this.lambda = lambda;
    this.s3 = s3;
    this.iot;

    this.init();
    this.initOnLogin();
  }

  // Create number of devices
  // Device name is consisted of customerName_prefix_index
  // Index starts from last index of prefix
  //
  // TODO: Error: Subscriber limit exceeded: Only 10 tables can be created, updated, or deleted simultaneously
  createDevices(customerName, prefix, mode, number) {
    let controlParams = {
      api: 'createDevices',
      customerName,
      prefix,
      mode,
      number
    }

    return this.lambda.invoke('NemoSnowControlDeviceResource', controlParams)
    .then(newDevices => {
      let syncParams = {
        api: 'copyDeviceTableForNewDevices',
        swKeyPrefix: `${customerName}_${prefix}`,
        newDevices
      }
      return this.lambda.invoke('NemoSnowSyncDeviceTable', syncParams)
    })
    .then(() => newDevices)
  }
  
  deleteDevices(thingList) {
    let params = {
      api: 'deleteDevices',
      thingList,
    }
    console.log('delete things:', thingList)
    return this.lambda.invoke('NemoSnowControlDeviceResource', params)
  }

  // Return thing's information
  // If specified thing name doesn't exist, reject 404 error
  describeThing(thingName) {
    let params = {
      thingName
    }
    return this.iot.describeThingAsync(params)
  }
  
  // TODO: Get Network Failure when describe multiple thing at once
  describeDevice(device) {
    let params = {
      thingName: device.name
    }
    return this.iot.describeThingAsync(params)
    .then(thing => {
      device.attributes = thing.attributes;
      device.thingTypeName = thing.thingTypeName;
      return device;
    })
  }
  
  getConfig() {
    return appConfig;
  }

  init() {
    this.iot = Promise.promisifyAll(new AWS.Iot());
  }

  initOnLogin() {
    this.cognito.addOnLoginFunction('iot', () => {
      this.init();
    })
  }

  // Get list of all things
  listThings() {
    let params = {
      maxResults: 100
    }
    return this.iot.listThingsAsync(params)
  }

  // Update thing's attributes.
  // Thing's version will be added by 1
  updateThing(thing) {
    let params = {
      thingName: thing.thingName,
      attributePayload: {
        attributes: thing.attributes
      },
    }
    return this.iot.updateThingAsync(params)
  }

  updateThings(thingList) {
    return Parallel.asyncAllPromise(thingList, (thing) => {
      return this.updateThing(thing);
    });
  }
}

export default Iot;
