import 'aws-sdk';

class Lambda {
  constructor($rootScope, cognito){
    'ngInject'
    this.$rootScope = $rootScope;
    this.cognito = cognito;
    this.lambda;
    
    this.init();
    this.initLambdaOnLogin();
  }
  
  init() {
    this.lambda = new AWS.Lambda();    
  }
  
  initLambdaOnLogin() {
    this.cognito.addOnLoginFunction('lambda', () => {
      this.init();
    })
  }
  
  invoke(functionName, context) {
    return new Promise((resolve, reject) => {
      if (!this.cognito.isLogin()) {
        reject(new Error('Not logged in'));
      }
      
      let params = {
        FunctionName: functionName,
        ClientContext: AWS.util.base64.encode(JSON.stringify(context))
      }
      
      this.lambda.invoke(params, (err, result) => {
        if (err) {
          // Lambda system error(not invoked in lambda function callback parameter)
          // ex) function permission error
          return reject(err);
        }
        
        if (!!result.FunctionError) {
          // Error which is invoked by lambda function callback (user defined error)
          return reject(!!result.Payload && JSON.parse(result.Payload).errorMessage);
        }
        
        return resolve(!!result.Payload && JSON.parse(result.Payload));
      });
    });
  }
}

export default Lambda;