import angular from 'angular';
import Modal from './service';

const module = angular.module('app.commons.services.modal', [
])
.service('modal', Modal)
.name;

export default module;
