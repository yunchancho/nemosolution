class Modal {
  constructor($rootScope, $uibModal){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
  }

  // params: title, content, onClosed
  success(params) {
    let modal = this.$uibModal.open({
      component: 'successModal',
      resolve: {
        title: () => {return params.title},
        content: () => {return params.content},
      },
      size: 'sm',
    })
    .closed
    .then(params.onClosed);

    return modal
  }

  // params: title, content, warning, confirmButton, cancelButton, onClosed, onConfirm
  confirm(params) {
    let modal = this.$uibModal.open({
      component: 'confirmModal',
      resolve: {
        title: () => {return params.title},
        content: () => {return params.content},
        warning: () => {return params.warning},
        confirmButton: () => {return params.confirmButton},
        cancelButton: () => {return params.cancelButton},
        onConfirm: () => {return params.onConfirm},
      },
      size: 'sm',
    })
    .closed
    .then(params.onClosed);

    return modal
  }

  // params: title, content, error, onClosed
  error(params) {
    let modal = this.$uibModal.open({
      component: 'errorModal',
      resolve: {
        title: () => {return params.title},
        content: () => {return params.content},
        error: () => {return params.error}
      },
      size: 'sm',
    })
    .closed
    .then(params.onClosed);

    return modal
  }
}

export default Modal;
