import angular from 'angular';
import Packages from './service';

const module = angular.module('app.commons.services.packages', [
  'app.commons.services.cognito',
  'app.commons.services.dynamoDB',
  'app.commons.services.s3',
])
.service('packages', Packages)
.name;

export default module;
