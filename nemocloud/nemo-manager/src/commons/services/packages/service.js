import encoding from 'text-encoding';
import Parallel from '../../../lib/parallel/index.js'

class Packages {
  constructor($rootScope, cognito, dynamoDB, s3){
    'ngInject'
    this.$rootScope = $rootScope;
    this.cognito = cognito;
    this.dynamoDB = dynamoDB;
    this.s3 = s3;
    this.bucket = s3.getConfig().packagesBucket;
    this.packageList = [];
    this.parallel = new Parallel();

    this.destroyOnLogout();
  }

  delete(pkg) {
    return this.s3.deleteObject(this.bucket, pkg.Filename)
  }

  destroyOnLogout() {
    this.cognito.addOnLogoutFunction('packages', () => {
      this.packageList = [];
    })
  }

  listPackages() {
    return new Promise((resolve, reject) => {
      this.s3.listPackages()
      .then((result) => {
        this.packageList = [];

        // Filter 1-depth folder
        for (let file of result.Contents) {
          let slashIndex = file.Key.indexOf('/');
          if (file.Size === 0
            && slashIndex === file.Key.length - 1) {
              this.packageList.push({name: file.Key.slice(0, slashIndex)});
          }
        }
      })
      .then(() => {
        return Parallel.asyncAllPromise(this.packageList, (pkg) => {
          return this.getPackageIndex(pkg);
        });
      })
      .then((result) => resolve(this.packageList))
      .catch(reject)
    });
  }

  getPackageIndex(pkg) {
    return this.s3.getPackage(`${pkg.name}/Packages.gz`)
    .then((result) => {
      let packageIndex = new encoding.TextDecoder("utf-8").decode(result.Body);
      pkg.appList = this.parsePackageIndex(packageIndex);
      return pkg;
    })
  }


  // Return package list to caller(ex: controller)
  // If packageList not initiated or empty, fetch list
  getPackageList() {
    return new Promise((resolve, reject) => {
      if (this.packageList.length !== 0) {
        return resolve(this.packageList);
      }

      // Wait other caller completes fetching data
      // Resolve on v()
      let pass = this.parallel.p(() => {
        resolve(this.packageList);
      })
      if (!pass) {
        return;
      }

      console.log('Init packageList');
      this.listPackages()
      .then((result) => resolve(this.packageList))
      .catch(reject)
      .then(() => this.parallel.v());
    });
  }

  // Parse package's property from Packages file
  parsePackageIndex(packages) {
    let appList = [];
    let appIndex = 0;
    let lineBegin = 0;
    let lineEnd = 0;
    let colon = 0;
    let name = '';
    let value = '';

    colon = packages.indexOf(':', lineBegin);
    if (colon !== -1) {
      appList[appIndex] = {};
    }
    while(colon !== -1) {
      lineEnd = packages.indexOf('\n', colon);
      name = packages.slice(lineBegin, colon).trim();
      value = packages.slice(colon + 1, lineEnd).trim();

      appList[appIndex][name] = value;

      lineBegin = lineEnd + 1;

      // Empty line means new app start
      if (packages.charAt(lineBegin) === '\n'
        && packages.indexOf(':', lineBegin + 1) !== -1) {
        lineBegin++;
        appIndex++;
        appList[appIndex] = {};
      }
      colon = packages.indexOf(':', lineBegin);
    }
    return appList;
  }
}

export default Packages;
