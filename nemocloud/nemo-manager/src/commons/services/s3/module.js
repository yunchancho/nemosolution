import angular from 'angular';
import S3 from './service';

const module = angular.module('app.commons.services.s3', [
  'app.commons.services.cognito'
])
.service('s3', S3)
.name;

export default module;
