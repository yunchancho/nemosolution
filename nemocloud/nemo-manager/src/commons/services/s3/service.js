import 'aws-sdk';
import Promise from 'bluebird'
import appConfig from './appConfig'
import FileTransfer from '../../../lib/fileTransfer/index.js'


class S3 {
  constructor($rootScope, cognito){
    'ngInject'
    this.$rootScope = $rootScope;
    this.cognito = cognito;
    this.s3;

    this.init();
    this.registerInitFunc();
  }

  deleteDeviceBackup(deviceName) {
    let prefix = `${deviceName.slice(0, deviceName.indexOf('_'))}/${deviceName}/`
    return this.listBackups(prefix)
    .then(files => {
      let baseParams = {
        Bucket: appConfig.backupsBucket,
        Delete: {
          Objects: []
        }
      }
      let requests = []
      console.log('s3', baseParams)
      let params = JSON.parse(JSON.stringify(baseParams));
      // Devide delete request
      for (let i = 0; i < files.length; i++) {
        params.Delete.Objects.push({ Key: files[i].Key })
        if (i % 1000 === 999) {
          console.log('push', params)
          requests.push(this.s3.deleteObjectsAsync(params))
          params = JSON.parse(JSON.stringify(baseParams));
        }
      }
      // push remain objects
      if (params.Delete.Objects.length !== 0) {
        console.log('remain', params)
        requests.push(this.s3.deleteObjectsAsync(params))
      }
      return Promise.all(requests)
    })
  }

  deleteObject(bucket, key) {
    if (key.charAt(key.length - 1) === '/') {
      // Remove all files in folder
      return this.deleteFolder(bucket, key)
    } else {
      return this.deleteFile(bucket, key)
    }
  }

  // Delete folder and inner objects
  deleteFolder(bucket, key) {
    return this.listObjects(bucket, key)
    .then(fileList => {
      let requests = [];
      for (let i = 0; i < fileList.length; i = i + 1000) {
        requests.push(fileList.slice(i, i + 1000));
      }
      requests.map(item => {
        return this.deleteFiles(bucket, item);
      })

      return Promise.all(requests);
    })
  }

  deleteFile(bucket, key) {
    let params = {
      Bucket: bucket,
      Key: key
    }

    console.log(params)
    return this.s3.deleteObjectAsync(params)
    .then(result => {
      console.log(result)
      return result;
    })
  }

  // TODO: Errors are in result[0].Errors
  deleteFiles(bucket, files) {
    let params = {
      Bucket: bucket,
      Delete: {
        Objects: files.map((file) => {
          return {Key: file.Key};
        })
      }
    }

    return this.s3.deleteObjectsAsync(params);
  }

  downloadFile(bucket, object) {
    // Download file using signed url
    // Url expires after one day
    let params = {
      Bucket: bucket,
      Key: object.Key,
      Expires: 900
    }

    return this.s3.getSignedUrlAsync('getObject', params)
    .then(url => {
      FileTransfer.downloadURL(url)
      return url
    })

    // // Download file using getObject
    // let params = {
    //   Bucket: bucket,
    //   Key: object.Key,
    //// Range: `bytes=0-100`
    // }

    // return this.s3.getObjectAsync(params)
    // .then(result => {
    //   console.log('s3 get Object', result)
    //   let filename = object.Key.slice(object.Key.lastIndexOf('/') + 1);
    //   FileTransfer.downloadBlob(result.Body, filename, result.ContentType);
    //   return result;
    // })
  }

  getConfig() {
    return appConfig
  }

  getPackage(key) {
    let params = {
      Bucket: appConfig.packagesBucket,
      Key: key,
      ResponseCacheControl: 'no-cache'
    }

    return this.s3.getObjectAsync(params);
  }

  init() {
    this.s3 = new AWS.S3({
      region: appConfig.region
    });
    this.s3 = Promise.promisifyAll(this.s3);
  }

  registerInitFunc() {
    this.cognito.addOnLoginFunction('s3', () => {
      this.init();
    })
    this.cognito.addOnLogoutFunction('s3', () => {
      this.init();
    })
  }

  // List all objects in contents Bucket
  // Maximum listing Count is 1000. call listing api recusively
  listObjects(bucket, prefix) {
    // Function declaration
    let recursiveCall = (list, ContinuationToken, prefix) => {
      let params = {
        Bucket: bucket,
        ContinuationToken,
        Prefix: prefix
      }

      return this.s3.listObjectsV2Async(params)
      .then(result => {
        list = list.concat(result.Contents);

        if (result.KeyCount === 1000) {
          return recursiveCall(list, result.NextContinuationToken);
        } else {
          return list;
        }
      })
    }

    // Function call
    return recursiveCall([], null, prefix);
  }

  // List all objects in backups Bucket
  // Maximum listing Count is 1000. call listing api recusively
  listBackups(prefix) {
    // Function declaration
    let recursiveCall = (list, ContinuationToken, prefix) => {
      let params = {
        Bucket: appConfig.backupsBucket,
        ContinuationToken,
        Prefix: prefix
      }

      return this.s3.listObjectsV2Async(params)
      .then(result => {
        list = list.concat(result.Contents);

        if (result.KeyCount === 1000) {
          return recursiveCall(list, result.NextContinuationToken);
        } else {
          return list;
        }
      })
    }

    // Function call
    return recursiveCall([], null, prefix);
  }

  // List all objects in contents Bucket
  // Maximum listing Count is 1000. call listing api recusively
  listContents(prefix) {
    // Function declaration
    let recursiveCall = (list, ContinuationToken, prefix) => {
      let params = {
        Bucket: appConfig.contentsBucket,
        ContinuationToken,
        Prefix: prefix
      }

      return this.s3.listObjectsV2Async(params)
      .then(result => {
        list = list.concat(result.Contents);

        if (result.KeyCount === 1000) {
          return recursiveCall(list, result.NextContinuationToken);
        } else {
          return list;
        }
      })
    }

    // Function call
    return recursiveCall([], null, prefix);
  }

  listDirectoryObjects(bucket, prefix) {
    let params = {
      Bucket: bucket,
      Prefix: prefix,
      Delimiter: '/'
    }

    return this.s3.listObjectsV2Async(params)
    .then(data => {
      // Remove itself
      for (let i = 0; i < data.Contents.length; i++) {
        if (data.Contents[i].Key === prefix) {
          data.Contents.splice(i, 1);
        }
      }
      return data;
    })
  }

  // list 2-depth folders has prefix
  // ex) listIndex('packages', 'nemoux/') -> return folders in nemoux folder (in packages bucket)
  listIndex(bucket, prefix) {
    console.log(bucket, prefix)
    return this.listDirectoryObjects(bucket, prefix)
    .then(data => {
      console.log(data)
      return data.CommonPrefixes
    })
    // .then(data => data.CommonPrefixes)
  }

  listPackages() {
    let params = {
      Bucket: appConfig.packagesBucket,
    };

    return this.s3.listObjectsV2Async(params);
  }

  uploadBackupFile(fileName, fileStream) {
    let params = {
      Bucket: appConfig.backupsBucket,
      Key: fileName,
      Body: fileStream
    }
    let options = {
      partSize: 10 * 1024 * 1024,
      queueSize: 1
    }

    return this.s3.uploadAsync(params, options)
  }

  // TODO: use multipart upload if file size > 100MB
  uploadFile(bucket, fileName, fileStream) {
    let params = {
      Bucket: bucket,
      Key: fileName,
      Body: fileStream
    }
    let options = {
      partSize: 10 * 1024 * 1024,
      queueSize: 1
    }

    return this.s3.uploadAsync(params, options)
  }

  uploadFileByUrl(bucket, fileName, fileStream) {
    let params = {
      Bucket: bucket,
      Fields: {
        key: fileName
      }
    }

    return this.s3.createPresignedPostAsync(params)
    .then(result => {
      console.log('url: ', result)

      return new Promise((resolve, reject) => {
        let form = new FormData();
        for (let key of Object.keys(result.fields)) {
          form.append(key, result.fields[key])
        }
        form.append('file', new Blob([ fileStream ]));
        let xhr = new XMLHttpRequest();
        // TODO: not worked in contents bucket
        xhr.open("POST", result.url);
        xhr.send(form);
        xhr.onload = () => {
          if (xhr.status.toString().charAt(0) === '2') {
            resolve(xhr.responseText);
          } else {
            reject(new Error(xhr.statusText))
          }
        };
        xhr.onerror = () => {
          reject(new Error(xhr.statusText))
        };
      })
    })
  }

  uploadFiles(bucket, directory, files) {
    let results = [];

    return files.reduce((previousValue, currentValue) => {
      return previousValue.then(() => {
        let filename = directory + currentValue.file.name;
        console.log('Upload start:', filename);
        return this.uploadFileByUrl(bucket, filename, currentValue.fileStream)
      })
      .then(result => results.push(result))
    }, Promise.resolve())
  }
}

export default S3;
