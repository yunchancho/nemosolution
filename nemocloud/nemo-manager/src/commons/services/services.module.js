import angular from 'angular';

import authModal from './authModal/module';
import cognito from './cognito/module';
import cognitoAdmin from './cognitoAdmin/module';
import dbManager from './dbManager/module';
import deviceManager from './deviceManager/module';
import dynamoDB from './dynamoDB/module';
import iot from './iot/module';
import lambda from './lambda/module';
import packages from './packages/module';
import s3 from './s3/module';
import spinner from './spinner/module';
import modal from './modal/module';

const module = angular.module('app.commons.services', [
	authModal,
	cognito,
	cognitoAdmin,
	dbManager,
	deviceManager,
	dynamoDB,
	iot,
	lambda,
	packages,
	s3,
	spinner,
	modal
])
.name

export default module;
