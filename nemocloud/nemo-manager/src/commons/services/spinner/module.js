import angular from 'angular';
import Spinner from './service';

const module = angular.module('app.commons.services.spinner', [
])
.service('spinner', Spinner)
.name;

export default module;
