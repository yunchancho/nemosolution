import angular from 'angular';

import login from './login/module';
import newPasswordRequired from './newPasswordRequired/module';

const module = angular.module('app.commons.views.auth', [
	login,
	newPasswordRequired
])
.name

export default module 
