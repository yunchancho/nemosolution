class Controller {
  constructor($rootScope, $state, authModal, cognito, spinner){
    'ngInject'
    this.name = 'login'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.authModal = authModal;
    this.cognito = cognito;
    this.spinner = spinner;
    this.password = '';
    this.pendingLogin = false;
    this.username = '';
  }
  
  $onInit() {
    this.redirectHome();
  }
  
  // Redirect to index page when user already logged in
  redirectHome() {
    if (this.cognito.isLogin()) {
      this.close();
    }
  }  
  
  login(){
    this.pendingLogin = true;
    this.spinner.on()
    this.cognito.login(this.username, this.password)
    .then(result => {
      console.log('Login: ', result);
      this.$state.reload();
      this.close();
      return null;
    })
    .catch(err => {
        console.error('login:', err);
        switch(err.message) {
          case 'New password required': 
            // User created by admin and it's first login
            let resolves = new Map();
            let [userAttributes, requiredAttributes] = err.attributes;
            resolves.set('userAttributes', userAttributes);
            resolves.set('requiredAttributes', requiredAttributes);
            this.authModal.open('newPasswordRequired', resolves);
            break;
          case 'Password reset required for the user':
            break;
        }
        return null;
    })
    .then(() => {
      this.spinner.off()
      this.pendingLogin = false;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    });
  }
}

export default Controller;
