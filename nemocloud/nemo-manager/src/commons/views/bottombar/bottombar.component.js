import template from './bottombar.html';
import controller from './bottombar.controller';

let component = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default component;
