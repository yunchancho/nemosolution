class Controller {
  constructor($rootScope, $state, dynamoDB, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.dynamoDB = dynamoDB
    this.spinner = spinner;
    this.data;
    this.table;
    this.key;
    this.exp;
    this.title;
    this.subTitle;
    this.options = {
      modes: ['tree', 'text']
    }
  }

  $onInit() {
    try {
      this.data = JSON.parse(JSON.stringify(this.resolve.data));
    } catch (e) {
      this.data = {}
    }
    this.table = this.resolve.table;
    this.key = this.resolve.key;
    this.exp = this.resolve.exp;
    this.title = this.resolve.title;
    this.subTitle = this.resolve.subTitle;
  }
  
  update() {
    this.spinner.on()
    return this.dynamoDB.updateItem(this.table, this.key, this.exp, this.data)
    .then(result => console.log('update DB', result))
    .then(() => this.close())
    .catch(err => console.error('update DB fails', err))
    .then(() => this.spinner.off())
  }
}

export default Controller;
