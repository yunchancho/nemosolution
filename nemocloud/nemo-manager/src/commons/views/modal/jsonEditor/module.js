import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-jsoneditor'

import component from './component';

const name = 'jsonEditor';

let module = angular.module(name, [ 
	uiRouter,
	'angular-jsoneditor',
	])
	.component(name, component)
	.name

export default module; 