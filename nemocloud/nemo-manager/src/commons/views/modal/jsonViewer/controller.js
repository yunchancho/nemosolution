class Controller {
  constructor($rootScope, $state){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.data;
    this.title;
    this.subTitle;
    this.options = {
      mode: 'view'
    }
  }

  $onInit() {
    try {
      this.data = JSON.parse(JSON.stringify(this.resolve.data));
    } catch (e) {
      this.data = {}
    }
    this.title = this.resolve.title;
    this.subTitle = this.resolve.subTitle;
  }
}

export default Controller;
