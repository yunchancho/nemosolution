import angular from 'angular';

import jsonEditor from './jsonEditor/module';
import jsonViewer from './jsonViewer/module';

const module = angular.module('app.commons.views.modal', [
	jsonEditor,
	jsonViewer,
])
.name

export default module 
