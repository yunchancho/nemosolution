import template from './rightbar.html';
import controller from './rightbar.controller';

let component = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default component;
