import angular from 'angular';
import uiRouter from 'angular-ui-router';
import component from './rightbar.component';

const name = 'rightbar';

let module = angular.module(name, [])
.component(name, component)
.name;

export default module;
