import template from './template.html';
import controller from './controller';

let component = {
  restrict: 'E',
  bindings: {
    bucket: '<',
    indexPrefix: '<',
    func: '<'
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default component;
