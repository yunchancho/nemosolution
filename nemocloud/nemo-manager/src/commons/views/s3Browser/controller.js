class Controller {
  constructor($rootScope, $uibModal, cognito, s3, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.cognito = cognito;
    this.s3 = s3;
    this.spinner = spinner;
    this.bucket;
    this.func;
    this.indexList = [];
    this.indexPrefix = '';
    this.directory = '';
    this.pickedFiles = [];
    this.selectedIndex;

    //function from s3FileList
    this.refreshList;
    this.downloadFiles;
  }

  $onInit() {
    this.spinner.on()
    this.getIndexList()
    .then(() => this.spinner.off())
  }

  createFolder() {
    let modal = this.$uibModal.open({
      component: 's3CreateFolder',
      resolve: {
        bucket: () => { return this.bucket },
        directory: () => { return this.directory },
        onComplete: () => { return () => this.refreshList() },
      },
      size: 'sm'
    });
  }

  getIndexList() {
    let prom;

    if (this.listIndex) {
      prom = this.func.listIndex(this.bucket, this.indexPrefix)
    } else {
      prom = this.s3.listIndex(this.bucket, this.indexPrefix)
    }

    return prom.then((result) => {
      this.indexList = result;
      try {
        this.selectedIndex = this.indexList[0].Prefix;
        this.$rootScope.$$phase || this.$rootScope.$apply();
      } catch (e) {
        console.error('no contents.', e)
      }
      console.log('contentsList', result);
      return this.indexList;
    })
    .catch((err) => console.error('getIndexList:', err))
  }

  select(index) {
    this.selectedIndex = index.Prefix;
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }

  // TODO: Enhance file selector (Dropzone.js?)
  uploadFile() {
    this.spinner.on()
    return this.s3.uploadFiles(this.bucket, this.directory, this.pickedFiles)
    .then((result) => {
      this.refreshList();
      console.log('Upload finish', result);
      return result
    })
    .catch((err) => {
      console.error('File upload error', err)
      let params = {
        title: '파일 업로드 실패',
        content: '파일 업로드 중 오류가 발생했습니다.',
        error: err.message
      }

      switch(err.message) {
        case 'Unsupported body payload object':
          params.content = '파일이 선택되지 않았거나 지원하지 않는 형식입니다.'
          params.error = '';
          break;
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
