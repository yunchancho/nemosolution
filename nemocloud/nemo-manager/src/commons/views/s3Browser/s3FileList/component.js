import template from './template.html';
import controller from './controller';

let component = {
  restrict: 'E',
  bindings: {
    bucket: '<',
    index: '<',
    indexPrefix: '<',
    directory: '=',
    refresh: '=',
    downloadCheckedFiles: '='
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default component;
