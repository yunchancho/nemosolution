class Controller {
  constructor($rootScope, $state, $uibModal, modal, s3, spinner, toastr){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.modal = modal;
    this.s3 = s3;
    this.spinner = spinner;
    this.toastr = toastr
    this.bucket;
    this.objects = [];
    this.index;
    this.indexPrefix;
    this.directory;
  }
  
  $onInit() {
    this.refresh = () => {
      this.listObjects();
    }
    this.downloadCheckedFiles = () => {
      this.downloadFiles();
    }
  }
  
  $onChanges(obj) {
    if (obj.index
      && obj.index.currentValue) {
      this.listObjects(this.index)
      .then(directory => {
        this.directory = directory
        this.$rootScope.$$phase || this.$rootScope.$apply();
      })
      .catch(console.error);
    }
  }
  
  // Move to parent directory
  back() {
    let backIndex = this.directory.lastIndexOf('/', this.directory.length - 2);
    this.listObjects(this.directory.slice(0, backIndex + 1))
    .then(dir => {
      this.directory = dir;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    });
  }
  
  deleteFile(key) {
    console.log(key)
    this.spinner.on()
    this.s3.deleteObject(this.bucket, key)
    .then((result) => {
      console.log('delete Object:', result);
      this.listObjects();
      return result;
    })
    .catch((err) => {
      console.error('delete Object', err);
    })
    .then(() => this.spinner.off())
  }
  
  downloadFile(file) {
    this.spinner.on()
    this.s3.downloadFile(this.bucket, file)
    .then((result) => {
      console.log('Download: ', result);
    })
    .catch((err) => console.error('downloadFile', err))
    .then(() => this.spinner.off());
  }

  downloadFiles() {
    this.spinner.on()

    let files = []
    for (let file of this.objects.Contents) {
      if (file.check) {
        files.push(file);
      }
    }

    let requests = files.map(file => {
      return this.s3.downloadFile(this.bucket, file)
    })

    return Promise.all(requests)
    .then(result => {
      this.toastr.success('파일 다운로드를 시작합니다..')
      console.log('download files: ', result)
    })
    .catch(err => {
      console.error('download files', err);
      let params = {
        title: '파일 다운로드 실패',
        content: '파일 다운로드 중 오류가 발생했습니다.',
        error: err.message
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
  
  enterFolder(directory) {
    this.spinner.on()
    this.listObjects(directory)
    .then((dir) => {
      this.directory = dir;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('listObjects error', err)
      let params = {
        title: '탐색 에러',
        content: '디렉토리 접근 중 에러가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off());
  }
  
  listObjects(directory) {
    if (!directory) {
      directory = this.directory;
    }
    this.spinner.on()
    return this.s3.listDirectoryObjects(this.bucket, directory)
    .then(result => {
      this.objects = result;
      console.log('file in directory: ', this.objects)
      this.$rootScope.$$phase || this.$rootScope.$apply();
      
      return directory;
    })
    .catch((err) => console.error('listObjects', err))
    .then(() => {
      this.spinner.off()
      return directory
    })
  }
}

export default Controller;
