import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-toastr/dist/angular-toastr.min.css'
import 'angular-toastr/dist/angular-toastr.tpls.min.js'

import component from './component';

const name = 's3FileList';

let module = angular.module(name, [ 
	uiRouter,
	'toastr',
	'app.commons.services.s3',
	'app.commons.services.spinner',
	'app.commons.services.deviceManager',
	'app.commons.filters.s3Directory'
	])
	.component(name, component)
	.name

export default module;