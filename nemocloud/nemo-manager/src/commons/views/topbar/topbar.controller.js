class TopbarController {
  constructor($translate, $state, cognito, dynamoDB) {
		'ngInject';
    this.name = 'topbar';
		this.$translate = $translate;
    this.$state = $state;
    this.cognito = cognito;
    this.dynamoDB = dynamoDB;
  }

	changeLanguage(langKey) {
		this.$translate.use(langKey);
		this.language = langKey;
	}

  toggleMode() {
    this.dynamoDB.setMode(this.localMode?'local':'cloud')
  }
  
  logout() {
    this.cognito.signOut();
    if (this.$state.$current.name === 'home') {
      this.$state.reload();
    } else {
      this.$state.go('home')
    }
  }
}

export default TopbarController;
