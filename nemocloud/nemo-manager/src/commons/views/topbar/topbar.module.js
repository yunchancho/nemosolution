import angular from 'angular';
import component from './topbar.component';

const name = 'topbar';

let module = angular.module(name, ['app.commons.services.cognito']) 
.component(name, component) 
.name;

export default module;
