import template from './user.html';
import controller from './user.controller';

let component = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default component;
