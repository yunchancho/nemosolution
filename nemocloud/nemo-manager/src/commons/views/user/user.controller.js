class UserController {
  constructor($rootScope, $state, $transitions, cognito) {
    'ngInject';
    this.name = 'user';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$transitions = $transitions;
    this.cognito = cognito;
    this.fullname = '';
    
    this.getUserFullname();
  }
  
  // Get username on refresh
  getUserFullname() {
    this.cognito.getUserAttribute('name')
    .then(result => {
      this.fullname = result;
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return this.fullname;
    })
    .catch(err => console.error('get name error: ', err))
  }
  
  logout() {
    this.cognito.signOut();
    if (this.$state.$current.name === 'home') {
      this.$state.reload();
    } else {
      this.$state.go('home')
    }
  }
  
  profile() {
    console.log('Logged in User: ', this.cognito.cognitoUser);
    console.log('AWS credentials: ', AWS.config.credentials);
  }
}

export default UserController;
