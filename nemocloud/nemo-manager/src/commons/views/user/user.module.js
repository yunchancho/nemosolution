import angular from 'angular';
import component from './user.component';

const name = 'user';

let module = angular.module(name, [])
.component(name, component)
.name;

export default module;
