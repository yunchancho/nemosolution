import angular from 'angular';

import auth from './auth/auth.module'
import modal from './modal/modal.module';
import navbar from './navbar/navbar.module';
import topbar from './topbar/topbar.module';
import bottombar from './bottombar/bottombar.module';
import rightbar from './rightbar/rightbar.module';
import user from './user/user.module';
import s3Browser from './s3Browser/module';

const module = angular.module('app.commons.views', [
  auth,
  modal,
  navbar,
	topbar,
	bottombar,
	rightbar,
	user,
	s3Browser
])
.name

export default module 
