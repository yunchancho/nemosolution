import angular from 'angular';

import home from './home/module';
import customer from './customer/module';
import devices from './devices/module';
import things from './things/module';
import users from './users/module';
import mypage from './mypage/module';
import packages from './packages/module';

let module = angular.module('app.components', [
  home,
  customer,
  devices,
  mypage,
  things,
  users,
  packages
])
.name;

export default module;
