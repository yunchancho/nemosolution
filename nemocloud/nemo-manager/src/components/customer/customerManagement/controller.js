class Controller {
  constructor($rootScope, $state, $uibModal, dbManager, dynamoDB, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.dbManager = dbManager;
    this.dynamoDB = dynamoDB;
    this.spinner = spinner;
    this.customerList = [];
  }
  
  $onInit() {
    this.scanCustomer();
  }
  
  scanCustomer() {
    this.spinner.on();
    return this.dynamoDB.scanCustomer()
    .then(data => {
      this.customerList = data;
      console.log('Customer: ', this.customerList);
      this.$rootScope.$$phase || this.$rootScope.$apply(); 
      return this.customerList;     
    })
    .catch(err => console.error('scanCustomer:', err))
    .then(() => this.spinner.off())
  }
  
  openDeleteCustomerModal() {
    let resolve = {
      checkedCustomer: () => {
        return this.dbManager.filterChecked(this.customerList);
      }
    }
    
    this.openModal('deleteCustomer', resolve);
  }
  

  openModal(component, resolve = {}, size = 'sm') {
    resolve.refreshList = () => {
      return () => this.scanCustomer();
    }
    let modal = this.$uibModal.open({
      component,
      resolve,
      size
    });
  }
}

export default Controller;
