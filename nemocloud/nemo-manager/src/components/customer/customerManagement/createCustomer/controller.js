class Controller {
  constructor($rootScope, $state, dynamoDB, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.dynamoDB = dynamoDB;
    this.spinner = spinner
    this.customer = {
      name: '',
      nickname: '',
      description: ''
    }
  }
  
  create() {
    this.spinner.on()
    this.dynamoDB.createCustomer(this.customer)
    .then(result => {
      console.log('create:', result);
      this.resolve.refreshList()
      this.close();
      return result;
    })
    .catch(err => console.error('create:', err))
    .then(() => this.spinner.off())
  }
  
  cancel() {
    this.close();
  }
  
}

export default Controller;
