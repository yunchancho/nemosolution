class Controller {
  constructor($rootScope, $state, $uibModal, dynamoDB, DTOptionsBuilder, DTColumnDefBuilder){
    'ngInject'
    this.name = 'customerList';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.dynamoDB = dynamoDB
    this.checkAll = false;
    this.customerList;
    
    this.dtOptions = DTOptionsBuilder.newOptions()
      .withPaginationType('full_numbers')
      .withDisplayLength(100)
      .withOption('responsive', true)
      .withOption('retrieve', true);
    this.dtColumnDefs = [
      DTColumnDefBuilder.newColumnDef(0).notSortable(),
      DTColumnDefBuilder.newColumnDef(1),
      DTColumnDefBuilder.newColumnDef(2)
    ];
  }
  
  // TODO: Check displayed(filtered) items only
  applyCheckAll() {
    for (let customer of this.customerList) {
      customer.check = this.checkAll;
    }
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
  
  edit(customer) {
    let modal = this.$uibModal.open({
      component: 'editCustomer',
      resolve: {
        customer: () => {return customer},
        refreshList: () => {return () => this.onChanged()}
      },
      size: 'sm'
    });
  }
  
  openEditModal(title, customer, exp) {
    let resolve = {
      title: () => title,
      subTitle: () => `${customer.name}`,
      table: () => this.dynamoDB.getConfig().tables.customer,
      data: () => customer[exp],
      key: () => customer.name,
      exp: () => exp,
    }
    
    let modal = this.$uibModal.open({
      component: 'jsonEditor',
      resolve,
      size: 'lg'
    })
    .closed
    .then(() => this.onChanged())
  }
  
  viewModal(title, device, data) {
    let resolve = {
      title: () => title,
      subTitle: () => `${device.name}`,
      data: () => data,
      refreshList: () => {
        return () => this.onChanged();
      }
    }
    
    let modal = this.$uibModal.open({
      component: 'jsonViewer',
      resolve,
      size: 'lg'
    });
  }
}

export default Controller;
