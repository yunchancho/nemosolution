class Controller {
  constructor($rootScope, $state, deviceManager, dynamoDB, iot, spinner){
    'ngInject'
    this.name = 'deleteCustomer';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.deviceManager = deviceManager;
    this.dynamoDB = dynamoDB;
    this.iot = iot;
    this.spinner = spinner
    this.confirm = '';
    this.checkedCustomer;
  }

  $onInit() {
    this.checkedCustomer = this.resolve.checkedCustomer;
  }
  
  delete() {
    if (this.confirm !== 'delete') {
      console.log('Type "delete"');
      return;
    }

    this.spinner.on()
    return this.deviceManager.getDeviceList()
    .then(deviceList => {
      let customerDevices = deviceList.filter(device => {
        for (let customer of this.checkedCustomer) {
          if (device.name.startsWith(`${customer.name}_`)) {
            return true;
          }
        }
        return false;
      })

      return this.iot.deleteDevices(customerDevices.map(device => {
        return { thingName: device.name }
      }))
    })
    .then(() => this.dynamoDB.deleteCustomers(this.checkedCustomer))
    .then(() => this.resolve.refreshList())
    .then(() => this.close())
    .catch((err) => console.error('deleteCustomer err:', err))
    .then(() => this.spinner.off())
  }
  
  cancel() {
    this.close();
    return null;
  }  
}

export default Controller;
