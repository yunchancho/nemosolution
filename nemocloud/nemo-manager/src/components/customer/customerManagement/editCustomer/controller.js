class Controller {
  constructor($rootScope, $state, dynamoDB, spinner){
    'ngInject'
    this.name = 'editCustomer';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.dynamoDB = dynamoDB;
    this.spinner = spinner
    this.oldCustomer;
    this.customer = {
      name: '',
      nickname: '',
      description: ''
    };
  }

  $onInit() {
    this.oldCustomer = this.resolve.customer;
    this.customer = {
      name: this.oldCustomer.name,
      nickname: this.oldCustomer.nickname,
      description: this.oldCustomer.description
    };
  }

  update() {
    this.spinner.on()
    return this.dynamoDB.updateCustomer(this.customer)
    .then(() => this.resolve.refreshList())
    .then(() => this.close())
    .catch((err) => console.error('editCustomer:', err))
    .then(() => this.spinner.off())
  }

  cancel() {
    this.close();
    return null;
  }
}

export default Controller;
