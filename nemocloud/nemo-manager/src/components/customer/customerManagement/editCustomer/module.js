import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'editCustomer';

let module = angular.module(name, [ 
	uiRouter,
	'app.commons.services.dbManager',
	])
	.component(name, component)
	.name

export default module; 