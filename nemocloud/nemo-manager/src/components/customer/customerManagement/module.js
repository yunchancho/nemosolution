import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import customerList from './customerList/module';
import deleteCustomer from './deleteCustomer/module';
import editCustomer from './editCustomer/module';
import createCustomer from './createCustomer/module';

const name = 'customerManagement';

let module = angular.module(name, [ 
	uiRouter,
	customerList,
	deleteCustomer,
	editCustomer,
	createCustomer
	])
	.component(name, component)
	.name

export default module; 