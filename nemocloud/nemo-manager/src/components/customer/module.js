import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import customerManagement from './customerManagement/module';

const name = 'customer';

let module = angular.module(name, [
	uiRouter,
	customerManagement
	])
	.config(config)
	.component(name, component)
	.name

export default module; 

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/customer',
			component: name
		});
}
