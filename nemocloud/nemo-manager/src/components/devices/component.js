import template from './template.html';
import controller from './controller';

let component = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default component;
