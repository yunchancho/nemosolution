class Controller {
  constructor($rootScope, $state, $uibModal, deviceManager, dynamoDB, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.deviceManager = deviceManager;
    this.dynamoDB = dynamoDB;
    this.spinner = spinner
    this.deviceList = [];
    this.localMode = false;
  }

  $onInit() {
    this.getDeviceList();
  }

  getDeviceList() {
    this.deviceManager.getDeviceList()
    .then(result => {
      this.deviceList = result;
      console.log('Device List', this.deviceList)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => console.error('getDeviceList', err))
  }

  refresh() {
    this.spinner.on()
    this.deviceManager.refresh()
    .then(() => this.getDeviceList())
    .catch(console.err)
    .then(() => this.spinner.off())

  }


  openModal(component, resolve, size = 'sm') {
    let modal = this.$uibModal.open({
      component,
      resolve,
      size
    });

    modal.closed.then(() => {
      this.scanCustomer();
    })
  }
}

export default Controller;
