class Controller {
  constructor($rootScope, $state, $uibModal, dynamoDB, DTOptionsBuilder, DTColumnDefBuilder){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.dynamoDB = dynamoDB;
    this.checkAll = false;
    this.deviceList = [];
    this.dbConfigs = dynamoDB.getConfig()
    
    this.dtOptions = DTOptionsBuilder.newOptions()
      .withPaginationType('full_numbers')
      .withDisplayLength(100)
      .withOption('retrieve', true);
    this.dtColumnDefs = [
      DTColumnDefBuilder.newColumnDef(0).notSortable(),
      DTColumnDefBuilder.newColumnDef(1),
      DTColumnDefBuilder.newColumnDef(2)
    ];
  }
  
  applyCheckAll() {
    for (let device of this.deviceList) {
      device.check = this.checkAll;
    }
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
  
  editPkgConfig(device, pkgConfig) {
    let resolve = {
      title: () => 'Package Config',
      subTitle: () => `${device.name}:${pkgConfig.name}`,
      table: () => device.name,
      data: () => pkgConfig.value,
      key: () => pkgConfig.name,
      exp: () => 'value',
    }
    
    this.openEditModal(device, resolve);
  }
  
  editItem(device, key) {
    let resolve = {
      title: () => key,
      subTitle: () => `${device.name}`,
      table: () => device.name,
      data: () => device[key],
      key: () => key,
      exp: () => 'value',
    }
    
    this.openEditModal(device, resolve);
  }
  
  openEditModal(device, resolve) {
    let modal = this.$uibModal.open({
      component: 'jsonEditor',
      resolve,
      size: 'lg'
    })
    .closed
    .then(() => device.fetch())
    .then(() => {
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
  }
  
  viewModal(title, device, data) {
    let resolve = {
      title: () => title,
      subTitle: () => `${device.name}`,
      data: () => data,
      refreshList: () => {
        return () => this.onChanged();
      }
    }
    
    let modal = this.$uibModal.open({
      component: 'jsonViewer',
      resolve,
      size: 'lg'
    });
  }
}

export default Controller;
