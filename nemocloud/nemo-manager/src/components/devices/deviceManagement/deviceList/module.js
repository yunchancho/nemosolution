import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'datatables/media/css/jquery.dataTables.min.css';
import 'datatables';
import 'angular-datatables/dist/css/angular-datatables.min.css';
import 'angular-datatables';

import component from './component';

const name = 'deviceList';

let module = angular.module(name, [ 
	uiRouter,
	'datatables',
	'app.commons.services.cognito',
	'app.commons.services.dbManager',
	])
	.component(name, component)
	.name

export default module; 