import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import deviceList from './deviceList/module';

const name = 'deviceManagement';

let module = angular.module(name, [ 
	uiRouter,
	deviceList,
	])
	.component(name, component)
	.name

export default module; 