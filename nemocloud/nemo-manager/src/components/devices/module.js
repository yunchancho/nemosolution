import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import deviceManagement from './deviceManagement/module';
import pkgIndexManagement from './pkgIndexManagement/module';

const name = 'devices';

let module = angular.module(name, [
	uiRouter,
	'app.commons.services.deviceManager',
	'app.commons.services.packages',
	deviceManagement,
	pkgIndexManagement
	])
	.config(config)
	.component(name, component)
	.name

export default module; 

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/devices',
			component: name
		});
}
