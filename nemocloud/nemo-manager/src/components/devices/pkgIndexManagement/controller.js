class Controller {
  constructor($rootScope, $state, $uibModal, packages, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.spinner = spinner
    this.packages = packages;
    this.pkgIndexList = [];
    this.selectedPkgIndex = null;
    
  }
  
  $onInit() {
    this.getPackages();
  }

  getPackages() {
    this.spinner.on()
    this.packages.getPackageList()
    .then((result) => {
      this.pkgIndexList = result;
      console.log('Packages:', this.pkgIndexList);
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch((err) => {
      console.error('getPackageList', err);
    })
    .then(() => this.spinner.off())
  }
  
  selectPackage(pkgIndex) {
    this.selectedPkgIndex = pkgIndex;
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
}

export default Controller;
