import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import pkgIndexDetail from './pkgIndexDetail/module';

const name = 'pkgIndexManagement';

let module = angular.module(name, [ 
	uiRouter,
	'app.commons.services.packages',
	pkgIndexDetail,
	])
	.component(name, component)
	.name

export default module; 