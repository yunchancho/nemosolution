import template from './template.html';
import controller from './controller';

let component = {
  restrict: 'E',
  bindings: {
    pkgIndex: '<',
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default component;
