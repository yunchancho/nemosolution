class Controller {
  constructor($rootScope, $state, $uibModal, deviceManager, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.deviceManager = deviceManager;
    this.spinner = spinner
    this.app;
    this.pkgIndex;
    this.devicePackageStatus = [];
  }
  
  $onInit() {
    this.getDeviceList();
  }
  
  $onChanges(obj) {
    if (obj.pkgIndex
      && obj.pkgIndex.currentValue) {
      this.checkInstalled(this.pkgIndex);
    }
  }
  
  checkInstalled(pkgIndex) {
    if (pkgIndex) {
      this.devicePackageStatus = this.deviceManager.hasPkgIndexAll(pkgIndex);
      this.$rootScope.$$phase || this.$rootScope.$apply();
    }
  }
  
  // Scan each device table items from dynamoDB
  refreshDevices() {
    this.deviceManager.fetchDevices()
    .then((result) => {
      this.checkInstalled(this.pkgIndex);
      console.log('Device List:', this.deviceManager.deviceList);
    })
    .catch((err) => {
      console.error('fetchDevices', err);
    })
  }
  
  getDeviceList() {
    this.deviceManager.getDeviceList()
    .then((result) => {
      this.checkInstalled(this.pkgIndex);
      console.log('Device List:', this.deviceManager.deviceList);
    })
    .catch((err) => {
      console.error('getDeviceList:', err);
    })
  }
  
  add() {
    this.deviceManager.addPackageIndex(this.pkgIndex)
    .then((result) => {
      this.checkInstalled(this.pkgIndex);
      console.log('addPackageIndex', result);
    })
    .catch((err) => {
      console.error('addPackageIndex', err);
    })
  }
  
  remove() {
    this.deviceManager.removePkgIndex(this.pkgIndex)
    .then((result) => {
      this.checkInstalled(this.pkgIndex);
      console.log('removePkgIndex', result);
    })
    .catch((err) => {
      console.error('removePkgIndex', err);
    })
  }
}

export default Controller;
