import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

// import updateAppConfig from './updateAppConfig/module';

const name = 'pkgIndexDetail';

let module = angular.module(name, [ 
	uiRouter,
	// updateAppConfig
	])
	.component(name, component)
	.name

export default module; 