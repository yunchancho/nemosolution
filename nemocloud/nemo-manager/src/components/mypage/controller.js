class Controller {
  constructor($rootScope) {
    'ngInject'
    this.name = 'mypage';
    this.$rootScope = $rootScope
    this.subtitle = '';
    this.resolve = {};
  }
  
  $onInit() {
    this.resolve.setSubtitle = (subtitle) => {
      return this.subtitle = subtitle;
    }
  }
}

export default Controller;
