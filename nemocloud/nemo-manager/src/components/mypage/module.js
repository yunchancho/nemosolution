import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import userInfo from './userInfo/module';

const name = 'mypage';

let module = angular.module(name, [ 
	uiRouter,
	userInfo
	])
	.config(config)
	.component(name, component)
	.name

export default module; 

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/mypage',
			component: name
		});
}
