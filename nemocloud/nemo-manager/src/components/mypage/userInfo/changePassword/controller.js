class Controller {
  constructor($state, cognito, spinner){
    'ngInject'
    this.$state = $state;
    this.cognito = cognito;
    this.spinner = spinner;
    this.newPassword = '';
    this.oldPassword = '';
    this.confirmPassword = '';
    
  }
  
  changePassword() {
    if (!this.checkNewPassword()) {
      return;
    }
    this.spinner.on()
    this.cognito.changePassword(this.oldPassword, this.newPassword)
    .then(result => {
      console.log('Password Change: ', result);
    })
    .catch(err => console.error(err))
    .then(() => this.spinner.off())
  }
  
  checkNewPassword() {
    if (this.newPassword !== this.confirmPassword) {
      console.log('confirm password is not same');
      return false;
    }
    
    return true;
  }
}

export default Controller;
