class Controller {
  constructor($rootScope, $state, cognito, spinner){
    'ngInject'
    this.name = 'dropOutModal';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.cognito = cognito;
    this.spinner = spinner;
  }
  
  cancel() {
    this.close();
  }
  
  delete() {
    this.spinner.on()
    this.cognito.deleteUser()
    .then(result => {
      this.close();
      this.$state.reload();
      return null;
    })
    .catch(err => console.error('deleteUser: ', err))
    .then(() => this.spinner.off())
  }
  
}

export default Controller;
