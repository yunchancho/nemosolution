import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import changePassword from './changePassword/module';
import dropOut from './dropOut/module';
import updateAttributes from './updateAttributes/module';
import verifyEmail from './verifyEmail/module';

const name = 'userInfo';

let module = angular.module(name, [ 
	uiRouter,
	changePassword,
	dropOut,
	updateAttributes,
	verifyEmail
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('mypage.userInfo', {
			url: '/userInfo',
			component: name
		});
}