class Controller {
  constructor($rootScope, $state, cognito, spinner){
    'ngInject'
    this.name = 'updateAttributes';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.cognito = cognito;
    this.spinner = spinner;
    this.attributes = [];
    this.attributesNames = ['name', 'email', 'phone_number'];
    this.oldAttributes = [];
  }
  
  $onInit() {
    this.printAttributes();
  }
  
  copyAttributes(from) {
    let to = [];
    for (let key of this.attributesNames) {
      to[key] = from[key];
    }
    return to;
  }
  
  // Filter unchanged Attributes
  deferenceAttributes() {
    let changedAttributes = new Map;
    for (let key of this.attributesNames){
      if(this.oldAttributes[key] !== this.attributes[key]) {
        changedAttributes.set(key, this.attributes[key]);
      }
    }
    return changedAttributes;
  }
  
  printAttributes() {
    this.spinner.on()
    this.cognito.getUserAttributesMap()
    .then(attributesMap => {
      for (let key of this.attributesNames) {
        this.oldAttributes[key] = attributesMap.get(key);        
      }
      
      this.resetAttributes();
    })
    .catch(err => console.error('printAttributes error: ', err))
    .then(() => this.spinner.off())
  }
  
  resetAttributes() {
    this.attributes = this.copyAttributes(this.oldAttributes);
    
    console.log('Attributes: ', this.attributes);
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
  
  
  // Call cognito to update attributes with the Map of changed attributes
  // If user update email or phone_number, email (or sms) including
  // verification code will be sended 
  updateAttributes() {
    let changedAttributes = this.deferenceAttributes();
    
    if (changedAttributes.size === 0) {
      console.log('Nothing changed');
      return;
    }

    this.spinner.on()
    this.cognito.updateUserAttributes(changedAttributes)
    .then(result => {
      console.log('Update result: ' + result);
      this.oldAttributes = this.copyAttributes(this.attributes);
      // TODO: Reload pages that using user attributes
      //       ex) common/view/user
      return result;
    })
    .catch(console.error)
    .then(() => this.spinner.off())
  }
}

export default Controller;
