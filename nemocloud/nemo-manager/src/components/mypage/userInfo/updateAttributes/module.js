import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'updateAttributes';

let module = angular.module(name, [ 
	uiRouter,
	'app.commons.services.cognito'
	])
	.component(name, component)
	.name

export default module; 