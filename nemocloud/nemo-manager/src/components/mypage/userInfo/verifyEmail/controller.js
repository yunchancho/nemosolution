class Controller {
  constructor($rootScope, $state, cognito, spinner){
    'ngInject'
    this.name = 'verifyEmail';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.cognito = cognito;
    this.spinner = spinner;
    this.code = '';
    this.emailVerified = false;
  }
  
  $onInit() {
    this.getEmailVerified();
  }
  
  getEmailVerified() {
    this.spinner.on()
    return this.cognito.getUserAttribute('email_verified')
    .then(result => {
      this.emailVerified = (result === 'true');
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => console.error('get email verified error: ', err))
    .then(() => this.spinner.off())
  }
  
  sendVerificationCode() {
    this.spinner.on()
    this.cognito.sendAttributeVerificateCode('email')
    .then(console.log)
    .catch(console.error)
    .then(() => this.spinner.off())
  }
  
  verifyEmail() {
    this.spinner.on()
    return this.cognito.verificateAttribute('email', this.code)
    .then(result => {
      console.log(result);
      return this.getEmailVerified();
    })
    .catch(console.error)
    .then(() => this.spinner.off())
  }
}

export default Controller;
