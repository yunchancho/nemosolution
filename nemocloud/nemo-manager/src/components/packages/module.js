import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import packagesManagement from './packagesManagement/module';
import packagesList from './packagesList/module';
import packagesUpdate from './packagesUpdate/module';
import pkgBrowser from './pkgBrowser/module';

const name = 'packages';

let module = angular.module(name, [
	uiRouter,
	packagesManagement,
	pkgBrowser,
	packagesList,
	packagesUpdate
	])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/packages',
			component: name
		});
}
