class Controller {
  constructor($rootScope, $state, $uibModal, s3, spinner, packages, DTOptionsBuilder, DTColumnDefBuilder){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.s3 = s3;
    this.spinner = spinner
    this.packages = packages;
    this.bucket = this.s3.getConfig().packagesBucket
    this.packageList = [];
    this.pickedFile;
    this.index = 'system';
    this.indexList = ['system', 'shell', 'application', 'theme'];
    this.dtOptions = DTOptionsBuilder.newOptions()
                                     .withPaginationType('full_numbers')
                                     .withDisplayLength(100)
                                     .withOption('retrieve', true);
    this.dtColumnDefs = [
      DTColumnDefBuilder.newColumnDef(0).notSortable(),
      DTColumnDefBuilder.newColumnDef(1),
      DTColumnDefBuilder.newColumnDef(2)
    ];
    this.resolve;
  }

  $onInit() {
    this.getPackageList();
  }

  getPackageList() {
    this.spinner.on()
    this.packages.getPackageIndex({name: this.index})
    .then(result => {
      this.packageList = result.appList;
      console.log(`Packages in ${this.index}`, this.packageList)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('list packages fail', err)
    })
    .then(() => this.spinner.off())
  }

  applyCheckAll() {
    for (let device of this.packageList) {
      device.check = this.checkAll;
    }
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }

  update(index) {
    this.resolve.package = this.packageList[index]
    console.log("패키지 : ",this.resolve.package)
    this.$state.go("packages.packagesUpdate")
  }

  delete(index) {
    this.spinner.on()
    this.packages.delete(this.packageList[index])
    .then(result => {
      console.log('Delete:', result)
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return;
    })
    .catch(err => {
      console.error('Delete package fails:', err)
    })
    .then(() => this.spinner.off())
  }

  selectIndex(index) {
    this.index = index;
    this.getPackageList()
  }
}

export default Controller;
