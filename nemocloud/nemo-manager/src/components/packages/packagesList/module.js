import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'datatables/media/css/jquery.dataTables.min.css';
import 'datatables';
import 'angular-datatables/dist/css/angular-datatables.min.css';
import 'angular-datatables';

import component from './component';

const name = 'packagesList';

let module = angular.module(name, [
	uiRouter,
	'app.commons.directive.fileread',
	'datatables',
	'app.commons.services.cognito',
	'app.commons.services.dbManager',
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('packages.packagesList', {
			url: '/packagesList',
			component: name
		});
}

export default module;
