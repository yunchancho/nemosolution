class Controller {
  constructor($rootScope, $state, $uibModal, s3, spinner, packages){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.s3 = s3;
    this.spinner = spinner
    this.packages = packages;
    this.package;
    this.bucket = this.s3.getConfig().packagesBucket
    this.packageList = [];
    this.pickedFile;
    this.index = 'system/';
    this.indexList = ['system/', 'shell/', 'application/', 'theme/'];
    this.icon;
    this.image;
  }

  $onInit() {

  }

  // TODO: Enhance file selector (Dropzone.js?)
  uploadFile() {
    this.spinner.on()
    return this.s3.uploadFiles(this.bucket, this.index, this.pickedFiles)
    .then((result) => {
      console.log('Upload finish', result);
      return result
    })
    .catch((err) => {
      console.error('File upload error', err)
      let params = {
        title: '파일 업로드 실패',
        content: '파일 업로드 중 오류가 발생했습니다.',
        error: err.message
      }

      switch(err.message) {
        case 'Unsupported body payload object':
          params.content = '파일이 선택되지 않았거나 지원하지 않는 형식입니다.'
          params.error = '';
          break;
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
