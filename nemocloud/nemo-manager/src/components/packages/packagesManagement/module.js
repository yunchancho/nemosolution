import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'packagesManagement';

let module = angular.module(name, [
	uiRouter,
	'app.commons.directive.fileread'
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('packages.packagesManagement', {
			url: '/packagesManagement',
			component: name
		});
}

export default module;
