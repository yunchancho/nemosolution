class Controller {
  constructor($rootScope, $state, $uibModal, s3, spinner, packages){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.s3 = s3;
    this.spinner = spinner
    this.packages = packages;
    this.package;
    this.bucket = this.s3.getConfig().packagesBucket
    this.packageList = [];
    this.pickedFile;
    this.icon;
    this.image;
    this.package;
    this.index={
      icon:"metadata/icon/",
      images:"metadata/images/"
    }
  }

  $onInit() {
    this.package = this.resolve.package;
    this.getPackageList();
  }

  getPackageList() {
    this.spinner.on()
    this.packages.getPackageIndex({name: this.index})
    .then(result => {
      this.packageList = result.appList;
      console.log(`Packages in ${this.index}`, this.packageList)
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => {
      console.error('list packages fail', err)
    })
    .then(() => this.spinner.off())
  }

  selectIndex(index) {
    this.index = index;
    this.getPackageList()
  }

  // TODO: Enhance file selector (Dropzone.js?)
  uploadFile() {
    this.spinner.on()
    return this.s3.uploadFiles(this.bucket, this.index.icon, this.icon)
    .then(() => this.s3.uploadFiles(this.bucket, this.index.images, this.image) )
    .then((result) => {
      console.log('Upload finish', result);
      return result
    })
    .catch((err) => {
      console.error('File upload error', err)
      let params = {
        title: '파일 업로드 실패',
        content: '파일 업로드 중 오류가 발생했습니다.',
        error: err.message
      }
      switch(err.message) {
        case 'Unsupported body payload object':
          params.content = '파일이 선택되지 않았거나 지원하지 않는 형식입니다.'
          params.error = '';
          break;
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }
}

export default Controller;
