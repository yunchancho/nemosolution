import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'packagesUpdate';

let module = angular.module(name, [
	uiRouter,
	'app.commons.directive.fileread'
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('packages.packagesUpdate', {
			url: '/packagesUpdate',
			component: name
		});
}

export default module;
