class Controller {
  constructor(s3) {
    'ngInject'
    this.name = 'pkgBrowser';
    this.s3 = s3;
    this.bucket = ''
    this.prefix = ''
  }

  $onInit(){
    this.bucket = this.s3.getConfig().packagesBucket;
  }
}

export default Controller;
