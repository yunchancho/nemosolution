import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'pkgBrowser';

let module = angular.module(name, [
	uiRouter,
	])
	.config(config)
	.component(name, component)
	.name

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('packages.pkgBrowser', {
			url: '/pkgBrowser',
			component: name
		});
}

export default module;
