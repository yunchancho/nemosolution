import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import thingManagement from './thingManagement/module';

const name = 'things';

let module = angular.module(name, [
	uiRouter,
	thingManagement
 ])
	.config(config)
	.component(name, component)
	.name

export default module; 

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/things',
			component: name
		});
}
