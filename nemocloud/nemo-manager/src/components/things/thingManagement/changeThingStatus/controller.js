class Controller {
  constructor($rootScope, $state, dbManager, iot, spinner){
    'ngInject'
    this.name = 'changeThingStatus';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.dbManager = dbManager;
    this.iot = iot;
    this.spinner = spinner
    this.thingList;
    this.statusList = [];
  }
  
  $onInit() {
    this.getStatus();
  }
  
  getStatus() {
    this.statusList = [];
    let status = this.iot.getConfig().status
    for (let key in status) {
      this.statusList.push(status[key])
    }
  }
  
  changeThingStatus(status) {
    let checkedThing = this.dbManager.filterChecked(this.thingList);
    
    for (let thing of checkedThing) {
      thing.attributes.status = status;
    }
    
    this.spinner.on()
    this.iot.updateThings(checkedThing)
    .then(() => this.onChanged())
    .catch(err => console.error('updateThings err:', err))
    .then(() => this.spinner.off())
  }
  
}

export default Controller;
