import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'changeThingStatus';

let module = angular.module(name, [ 
	uiRouter,
	'app.commons.services.dbManager',
	'app.commons.services.iot',
	])
	.component(name, component)
	.name

export default module; 