class Controller {
  constructor($rootScope, $state, $uibModal, dbManager, iot, spinner){
    'ngInject'
    this.name = 'thingManagement';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.dbManager = dbManager;
    this.iot = iot;
    this.spinner = spinner
    this.thingList = [];
  }
  
  $onInit() {
    this.listThings();
  }
  
  listThings() {
    this.spinner.on()
    return this.iot.listThings()
    .then((result) => {
      this.thingList = result.things;
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return this.thingList;
    })
    .catch(err => console.error('listThings err:', err))
    .then(() => this.spinner.off())
  }
  
  openDeleteModal() {
    let resolve = {
      checkedThing: () => {
        return this.dbManager.filterChecked(this.thingList)
      }
    }
    
    this.openModal('deleteThing', resolve);
  }
  
  
  openModal(component, resolve = {}, size = 'sm') {
    resolve.refreshList = () => {
      return () => this.listThings();
    }
    let modal = this.$uibModal.open({
      component,
      resolve,
      size
    });
  }
}

export default Controller;
