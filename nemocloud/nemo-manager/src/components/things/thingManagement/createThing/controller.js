class Controller {
  constructor($rootScope, $state, dynamoDB, iot, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.dynamoDB = dynamoDB;
    this.iot = iot;
    this.spinner = spinner
    this.customer;
    this.customerList = [];
    this.mode = '';
    this.modes = []
    this.number = 1;
    this.prefix = '';
  }
  
  $onInit() {
    this.getMode();
    this.spinner.on()
    this.dynamoDB.scanCustomer()
    .then(list => {
      this.customerList = list;
      this.customer = list[0].name;
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return null
    })
    .catch(err => console.error('scanCustomer Err', err))
    .then(() => this.spinner.off())
  }
  
  create() {
    this.spinner.on()
    this.iot.createDevices(this.customer, this.prefix, this.mode, this.number)
    .then(result => console.log('createDevice:', result))
    .then(() => this.resolve.refreshList())
    .then(() => this.close())
    .catch((err) => {
      console.error('createDevice:', err);
    })
    .then(() => this.spinner.off())
  }
  
  cancel() {
    this.close();
  }
  
  getMode() {
    this.modes = []
    let modes = this.iot.getConfig().mode
    for (let mode in modes) {
      this.modes.push(mode)
    }
    this.mode = this.modes[0]
  }
  
}

export default Controller;
