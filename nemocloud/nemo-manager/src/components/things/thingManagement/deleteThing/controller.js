class Controller {
  constructor($rootScope, $state, iot, spinner){
    'ngInject'
    this.name = 'deleteThing';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.iot = iot;
    this.spinner = spinner
    this.confirm = 'delete';
    this.checkedThing;
  }

  $onInit() {
    this.checkedThing = this.resolve.checkedThing;
  }
  
  delete() {
    if (this.confirm !== 'delete') {
      console.log('Type "delete"');
      return;
    }
    
    this.spinner.on()
    this.iot.deleteDevices(this.checkedThing)
    .then(result => console.log('deleteDevices: ', result))
    .then(() => this.resolve.refreshList())
    .then(() => this.close())
    .catch(err => {
      console.error('deleteDevices err:', err);
    })
    .then(() => this.spinner.off())
  }
  
  cancel() {
    this.close();
  }  
}

export default Controller;
