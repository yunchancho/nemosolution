class Controller {
  constructor($rootScope, $state, iot, spinner){
    'ngInject'
    this.name = 'editThing';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.iot = iot;
    this.spinner = spinner
    this.statusList = [];
    this.oldThing;
    this.thing = {
      thingName: '',
      attributes: {
        status: '',
        mode: '',
        hwKey: '',
        cloudAllowed: '',
      }
    };
  }
  
  $onInit() {
    this.oldThing = this.resolve.thing;
    this.thing = {
      thingName: this.oldThing.thingName,
      attributes: {
        status: this.oldThing.attributes.status,
        mode: this.oldThing.attributes.mode,
        hwKey: this.oldThing.attributes.hwKey,
        cloudAllowed: this.oldThing.attributes.cloudAllowed,
      }
    };

    this.getStatus();
  }
  
  getStatus() {
    this.statusList = [];
    let status = this.iot.getConfig().status
    for (let key in status) {
      this.statusList.push(status[key])
    }
  }
  
  update() {
    // if (this.oldThing.attributes.status === this.thing.attributes.status) {
    //     console.log('No changes');
    //     this.close();
    //     return;
    //   }
    this.spinner.on()
    this.iot.updateThing(this.thing)
    .then(() => this.resolve.refreshList())
    .then(() => this.close())
    .catch((err) => {
      console.error('updateThing:', err)
    })
    .then(() => this.spinner.off())
  }
  
  cancel() {
    this.close();
  }  
}

export default Controller;
