import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import changeThingStatus from './changeThingStatus/module';
import createThing from './createThing/module';
import deleteThing from './deleteThing/module';
import editThing from './editThing/module';
import thingList from './thingList/module';

const name = 'thingManagement';

let module = angular.module(name, [
	uiRouter,
	changeThingStatus,
	createThing,
	deleteThing,
	thingList,
	editThing,
 ])
	.component(name, component)
	.name

export default module;
