class Controller {
  constructor($rootScope, $state, $uibModal, DTOptionsBuilder, DTColumnDefBuilder){
    'ngInject'
    this.name = 'list';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.checkAll = false;
    this.thingList = [];
    
    this.dtOptions = DTOptionsBuilder.newOptions()
      .withPaginationType('full_numbers')
      .withDisplayLength(100)
      .withOption('retrieve', true);
    this.dtColumnDefs = [
      DTColumnDefBuilder.newColumnDef(0).notSortable(),
      DTColumnDefBuilder.newColumnDef(1),
      DTColumnDefBuilder.newColumnDef(2),
      DTColumnDefBuilder.newColumnDef(3),
      DTColumnDefBuilder.newColumnDef(4)
    ];
  }
  
  applyCheckAll() {
    for (let thing of this.thingList) {
      thing.check = this.checkAll;
    }
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
  
  edit(thing) {
    let modal = this.$uibModal.open({
      component: 'editThing',
      resolve: {
        thing: () => {return thing},
        refreshList: () => {return () => this.onChanged()}
      },
      size: 'sm'
    });
  }
  

}

export default Controller;
