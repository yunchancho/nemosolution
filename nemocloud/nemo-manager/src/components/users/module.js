import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import userManagement from './userManagement/module';

const name = 'users';

let module = angular.module(name, [
	uiRouter,
	userManagement
	])
	.config(config)
	.component(name, component)
	.name

export default module; 

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/users',
			component: name
		});
}
