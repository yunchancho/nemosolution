class Controller {
  constructor($rootScope, $state, $uibModal, cognito, cognitoAdmin, dbManager, spinner){
    'ngInject'
    this.name = 'userManagement';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.cognito = cognito;
    this.cognitoAdmin = cognitoAdmin;
    this.dbManager = dbManager;
    this.spinner = spinner
    this.manager = false;
    this.userList = [];
  }
  
  $onInit() {
    this.cognitoAdmin.setManagerPool(this.manager);
    this.listUsers();
  }
  
  confirmUser() {
    let checkedUser = this.dbManager.filterChecked(this.userList);
    this.spinner.on()
    this.cognitoAdmin.confirmUsers(checkedUser)
    .then(result => this.listUsers())
    .catch((err) => console.error('disableUsers', err))
    .then(() => this.spinner.off())
  }
  
  disableUser() {
    let checkedUser = this.dbManager.filterChecked(this.userList);
    this.spinner.on()
    this.cognitoAdmin.disableUsers(checkedUser)
    .then(result => this.listUsers())
    .catch((err) => console.error('disableUsers', err))
    .then(() => this.spinner.off())
  }
  
  enableUser() {
    let checkedUser = this.dbManager.filterChecked(this.userList);
    this.spinner.on()
    this.cognitoAdmin.enableUsers(checkedUser)
    .then(result => this.listUsers())
    .catch(err => console.error('enableUsers', err))
    .then(() => this.spinner.off())
  }
  
  listUsers() {
    this.spinner.on()
    return this.cognitoAdmin.listUsers()
    .then(data => {
      this.userList = data.Users;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => console.error('listUsers: ', err))
    .then(() => this.spinner.off())
  }
  
  openCreateUserModal() {
    let resolve = {
      manager: () => {
        return this.manager;
      }
    }
    
    this.openModal('createUser', resolve, 'lg');
  }
  
  
  openDeleteUserModal() {
    let resolve = {
      checkedUser: () => {
        return this.dbManager.filterChecked(this.userList)
      }
    }
    
    this.openModal('deleteUser', resolve);
  }
  
  
  openModal(component, resolve = {}, size = 'sm') {
    resolve.refreshList = () => {
      return () => this.listUsers();
    }
    
    let modal = this.$uibModal.open({
      component,
      resolve,
      size
    });
  }
  
  togglePool() {
    this.manager = !this.manager;
    this.cognitoAdmin.setManagerPool(this.manager);
    this.listUsers();
  }
  
  resetPassword() {
    let checkedUser = this.dbManager.filterChecked(this.userList);
    
    this.spinner.on()
    this.cognitoAdmin.resetUsersPassword(checkedUser)
    .then(result => {
      console.log('resetUsersPassword', result)
      this.listUsers();
    })
    .catch(err => console.error('resetUsersPassword', err))
    .then(() => this.spinner.off())
  }
}

export default Controller;
