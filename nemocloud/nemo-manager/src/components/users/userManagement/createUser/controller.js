class Controller {
  constructor($rootScope, $state, cognitoAdmin, dynamoDB, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.cognitoAdmin = cognitoAdmin;
    this.dynamoDB = dynamoDB;
    this.spinner = spinner
    this.manager;
    this.customerList = [];
    this.username = '';
    this.password = '';
    this.attributes = {
      customer: '',
      email: '',
      name: '',
      permission: '',
      type: '',
    }
    this.typeList = [];
  }
  
  $onInit() {
    this.manager = this.resolve.manager;
    this.typeList = this.cognitoAdmin.getConfig().types;
    this.attributes.type = this.typeList[0];
    this.fetchCustomerList();
  }
  
  createUser() {
    if (this.manager) {
      this.attributes.customer = 'admin';
      this.attributes.permission = '';
    }
    
    // Set attributes array
    let attributesParam = [];
    for (let key in this.attributes) {
      switch(key) {
        case 'customer':
        case 'type':
          attributesParam.push({
            Name: `custom:${key}`,
            Value: this.attributes[key]
          });
          break;
        case 'permission':
          let item = {
            Name: `custom:${key}`,
            Value: `${this.attributes.customer}_${this.attributes[key]}`
          }
          attributesParam.push(item);
          break;
        default:
          attributesParam.push({Name: key, Value: this.attributes[key]});
      }
    }
    this.spinner.on()
    this.cognitoAdmin.createUser(this.username, this.password, attributesParam)
    .then(() => this.resolve.refreshList())
    .then(() => this.close())
    .catch((err) => console.error('createUser:', err))
    .then(() => this.spinner.off())
  }
  
  fetchCustomerList() {
    this.spinner.on()
    this.dynamoDB.scanCustomer()
    .then(list => {
      this.customerList = list;
      this.attributes.customer = this.customerList[0].name;
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => console.error('fetchCustomerList:', err))
    .then(() => this.spinner.off())
  }
  
  cancel() {
    this.close();
  }  
}

export default Controller;
