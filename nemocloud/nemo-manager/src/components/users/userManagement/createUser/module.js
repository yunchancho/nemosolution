import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'createUser';

let module = angular.module(name, [ 
	uiRouter,
	'app.commons.services.cognitoAdmin',
	'app.commons.services.dynamoDB',
	])
	.component(name, component)
	.name

export default module; 