class Controller {
  constructor($rootScope, $state, cognitoAdmin, spinner){
    'ngInject'
    this.name = 'deleteUser';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.cognitoAdmin = cognitoAdmin;
    this.spinner = spinner
    this.confirm = '';
    this.checkedUser;
  }

  $onInit() {
    this.checkedUser = this.resolve.checkedUser;
  }
  
  delete() {
    if (this.confirm !== 'delete') {
      console.log('Type "delete"');
      return;
    }
    
    this.spinner.on()
    this.cognitoAdmin.deleteUsers(this.checkedUser)
    .then(() => this.resolve.refreshList())
    .then(() => this.close())
    .catch(err => console.error('deleteUser err:', err))
    .then(() => this.spinner.off())
  }
  
  cancel() {
    this.close();
  }  
}

export default Controller;
