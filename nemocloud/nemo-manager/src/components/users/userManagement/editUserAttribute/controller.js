class Controller {
  constructor($rootScope, $state, cognito, cognitoAdmin, dynamoDB, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.cognito = cognito;
    this.cognitoAdmin = cognitoAdmin;
    this.dynamoDB = dynamoDB;
    this.spinner = spinner
    this.customer;
    this.originUser;
    this.user = {
      Username: '',
      Attributes: []
    };
  }

  $onInit() {
    this.originUser = this.resolve.user;
    this.user.Username = this.originUser.Username;

    this.linkAttribute();
    this.fetchCustomerList();
  }
  
  fetchCustomerList() {
    this.spinner.on()
    this.dynamoDB.scanCustomer()
    .then(list => {
      this.customerList = list;
      if (this.customerList.length !== 0
        && !this.customer.Value) {
        this.customer.Value = this.customerList[0].name;
      }
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => console.error('fetchCustomerList:', err))
    .then(() => this.spinner.off())
  }

  // TODO: Needs refactor
  linkAttribute() {
    this.user.Attributes = [];
    let modifiableAttributes = new Set(this.cognitoAdmin.getModifiableAttributes());
    let names = this.cognito.getConfig().attributeNames;

    // Init user.Attribute
    modifiableAttributes.forEach(item => {
      let attribute = {
        Name: item,
        Value: ''
      }
      switch(attribute.Name) {
        case names.customer:
          this.customer = attribute;
          break;
      }
      this.user.Attributes.push(attribute);
    })

    // Assign orignal value to new array(user.Attribute)
    for (let attribute of this.user.Attributes) {
      for (let origin of this.originUser.Attributes) {
        if (attribute.Name === origin.Name) {
          attribute.Value = origin.Value;
        }
      }
    }
  }

  update() {
    let value = JSON.parse(JSON.stringify(this.user))
    for (let i = 0; i < value.Attributes.length; i++) {
      if (value.Attributes[i].Name === this.cognito.getConfig().attributeNames.type) {
        value.Attributes.splice(i, 1);
      }
    }
    this.spinner.on()
    this.cognitoAdmin.updateUserAttribute(value)
    .then(() => this.resolve.refreshList())
    .then(() => this.close())
    .catch(err => console.error('updateUserAttribute:', err))
    .then(() => this.spinner.off())
  }

  cancel() {
    this.close();
  }
}

export default Controller;
