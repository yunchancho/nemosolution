import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import createUser from './createUser/module';
import deleteUser from './deleteUser/module';
import editUserAttribute from './editUserAttribute/module';
import userList from './userList/module';

const name = 'userManagement';

let module = angular.module(name, [ 
	uiRouter,
	createUser,
	deleteUser,
	editUserAttribute,
	userList
	])
	.component(name, component)
	.name

export default module; 