import template from './template.html';
import controller from './controller';

let component = {
  restrict: 'E',
  bindings: {
    userList: '<',
    onChanged: '&'
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default component;
