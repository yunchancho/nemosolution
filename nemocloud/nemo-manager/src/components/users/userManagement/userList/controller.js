class Controller {
  constructor($rootScope, $state, $uibModal, DTOptionsBuilder, DTColumnDefBuilder){
    'ngInject'
    this.name = 'userList';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.checkAll = false;
    this.userList = [];
    
    this.dtOptions = DTOptionsBuilder.newOptions()
      .withPaginationType('full_numbers')
      .withDisplayLength(100)
      .withOption('retrieve', true);
    this.dtColumnDefs = [
      DTColumnDefBuilder.newColumnDef(0).notSortable(),
      DTColumnDefBuilder.newColumnDef(1),
      DTColumnDefBuilder.newColumnDef(2),
      DTColumnDefBuilder.newColumnDef(3),
      DTColumnDefBuilder.newColumnDef(4),
      DTColumnDefBuilder.newColumnDef(5),
      DTColumnDefBuilder.newColumnDef(6).notSortable()
    ];
  }
  
  applyCheckAll() {
    for (let user of this.userList) {
      user.check = this.checkAll;
    }
    
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
  
  edit(user) {
    let modal = this.$uibModal.open({
      component: 'editUserAttribute',
      resolve: {
        user: () => {return user},
        refreshList: () => {return () => this.onChanged()}
      },
      size: 'lg'
    });
  }
  
}

export default Controller;
