//import LocalConfigManager from './local_config_manager'
import co from 'co';
import RemoteConfigManager from './remote_config_manager'

// private members
const privateMembers = new WeakMap();

class DeviceConfig {
	constructor(args) {
		'ngInject'

		let manager;
	 	if (process.env.RUN_TARGET == 'local') {
			//manager = new LocalConfigManager(args);
		} else {
			manager = new RemoteConfigManager(args);
		}

		privateMembers.set(this, {
			manager
		});

	}

	initialize() {
		let members = privateMembers.get(this);
		const initCoroutine = co.wrap(function* () {
			yield members.manager.initialize();
		});

		return initCoroutine();
	}

	get() {
		// return promise
		let members = privateMembers.get(this);
		return members.manager.config;
	}

	set(config) {
		// return promise
		let members = privateMembers.get(this);
		return members.manager.config(config);
	}

	getDevInfo(type) {
		// return promise
		let members = privateMembers.get(this);
		return members.manager.getDevInfo(type);
	}
}

export default DeviceConfig
