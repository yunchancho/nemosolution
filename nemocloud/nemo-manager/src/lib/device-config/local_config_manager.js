import co from 'co';
import Promise from 'bluebird';
import Mongo from 'mongodb';

const MongoClient = Promise.promisifyAll(Mongo).MongoClient;
const mongoScheme = "mongodb://";
const configKey = "_path";
const specialValues = {
	deletion: "@__delete__"
}

// private members
const privateMembers = new WeakMap();

function createDeviceConfigFromDocs(shellItems, stebackItems)
{
	let result = {
		shell: {},
		steback: {}
	};
	// we need to remove _path key from docs 
	// and set the key's value to a key
	for (let item of shellItems) {
		let key = item._path;
		delete item._path;
		result.shell[key] = item;
	}

	for (let item of stebackItems) {
		let key = item._path;
		delete item._path;
		result.steback[key] = item;
	}

	return result;
}

function createDocsFromDeviceConfig(config)
{
	let result = {
		shell: [],
		steback: []
	}

	for (let key in config.shell) {
		let value = key;
		config.shell[key][configKey] = value;
		result.shell.push(config.shell[key]);
	}

	for (let key in config.steback) {
		let value = key;
		config.steback[key][configKey] = value;
		result.steback.push(config.steback[key]);
	}

	return result;
}

function updateDocs(collection, items)
{
	return co(function* () {
		for (let item of items) {
			yield collection.updateAsync({ _path: item._path }, { $set: item }, { w: 1 });

			// TODO we would need to handle serveral depth of json data
			// but here we only handle 1 depth.
			for (let prop in item) {
				if (item[prop] === specialValues.deletion) {
					console.log("changed item: ", item);
					let target = {};
					target[prop] = "";
					yield collection.updateAsync({ _path: item._path }, { $unset: target });
				} 
			}
		}
	});
}

class DataManager {
	constructor(args) {
		privateMembers.set(this, {
			args: args,
			shell: null,
			steback: null
		});
		this.specials = specialValues;
	}

	initialize() {
		let members = privateMembers.get(this);
		const initCoroutine = co.wrap(function* () {
			try {
				const db = yield MongoClient.connectAsync(mongoScheme + members.args.dbPath);
				members.shell = db.collection(members.args.shellCollection);
				members.steback = db.collection(members.args.stebackCollection);
			} catch (err) {
				console.log("err: ", err.stack);
				throw "failed to initialize db: " + err.stack;
			}
			return true;
		});

		return initCoroutine(this);
	}

	get config() {
		let members = privateMembers.get(this);
		const configCoroutine = co.wrap(function* (self) {
			let deviceConfig = null;
			try {
				const shellConfig = yield members.shell.find({}, { _id: 0 }).toArrayAsync();
				const stebackConfig = yield members.steback.find({}, { _id: 0 }).toArrayAsync();
				deviceConfig = yield createDeviceConfigFromDocs(shellConfig, stebackConfig);
			} catch (err) {
				console.log("err: ", err.stack);
				throw "failed to get config: " + err.stack;
			}
			return deviceConfig;
		});

		return configCoroutine(this);
	}

	set config(config) {
		let members = privateMembers.get(this);
		const configCoroutine = co.wrap(function* (self) {
			try {
				let copiedConfig = JSON.parse(JSON.stringify(config));
				let change = yield createDocsFromDeviceConfig(copiedConfig);
				console.log("change: ", change);
				yield updateDocs(members.shell, change.shell);
				yield updateDocs(members.steback, change.steback);
			} catch (err) {
				console.log("err: ", err.stack);
				throw "failed to set config: " + err.stack;
			}
		});

		return configCoroutine(this);
	}
}

module.exports = LocalConfigManager;
