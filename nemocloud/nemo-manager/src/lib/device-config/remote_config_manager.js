import co from 'co';
import _ from 'underscore';
import Promise from 'bluebird';
import aws from 'aws-sdk';
import { thingShadow } from 'aws-iot-device-sdk';
import awsConfig from './aws-configuration';

const operationTimeout = 10000;

// private members
const privateMembers = new WeakMap();

class RemoteConfigManager {
	constructor(args) {
		privateMembers.set(this, {
			args: args,
			thingShadow: {},
			thingName: args.thingName,
			publishedTopicStack: [],
			reqStack: [],
			timeoutHandle: null,
			topics: []
		});
	}

	initialize() {
		const initCoroutine = co.wrap(function* (self) { 
			let result = null;
			try {
				yield _setMembers(self);
				yield _updateAwsCredentials(self);
				yield _connectToAws(self);
				yield _registerShadowHandlers(self);
			} catch (err) {
				throw 'failed to execute generator: ' + err.stack;
			}

			let members = privateMembers.get(self);
			return true;
		});

		return initCoroutine(this);
	}

	get config() {
		let members = privateMembers.get(this);
		let promise = new Promise((resolve, reject) => {
			_commitOperationToAws(this, "get", {}, { resolve, reject })
		});

		return promise;
	}

	set config(config) {
		let members = privateMembers.get(this);
		let promise = new Promise((resolve, reject) => {
			let data = {
				state: {
					desired: {}
				}
			};

			data.state.desired = config;
			_commitOperationToAws(this, "update", data, { resolve, reject })
		});

		return promise;
	}

	getDevInfo(type) {
		let members = privateMembers.get(this);
		let topic = _.findWhere(members.topics, { pub: members.thingName + '-devinfo-request' });
		if (!_.contains(topic.types, type)) {
			return Promise.reject('Unsupported devinfo type');
		}

		let payload = JSON.stringify({ type, message: 'please give me info :)' });
		return new Promise((resolve, reject) => {
			let pubTopic = members.thingName + '-devinfo-request';
			members.thingShadow.publish(pubTopic,	payload);
			members.publishedTopicStack.push({ pubTopic, promise: { resolve, reject } });
		});
	}
}


function _setMembers(self)
{
	let members = privateMembers.get(self);

	// defines specific topics 
	members.topics.push({ 
		sub: members.thingName + '-devinfo-response',
		pub: members.thingName + '-devinfo-request',
		types: [
			'touch', 'sound', 'resolution'
		]
 	});

	if (!_createShadow(self)) {
		return Promise.reject('failed to create shadow');
	}

	return Promise.resolve('success to create shadow');
}

function _registerShadowHandlers(self)
{
	let members = privateMembers.get(self);
	let shadow = members.thingShadow;

  shadow.on('connect', function() {
     console.log('success to connect aws iot');
  });

  shadow.on('close', function() {
     console.log('close');
     shadow.unregister(members.thingName);
  });

  shadow.on('reconnect', function() {
     console.log('success to reconnect aws iot');
  });

	shadow.on('offline', function() {
	  if (members.timeoutHandle !== null) {
	 	 clearTimeout(members.timeoutHandle);
	 	 members.timeoutHandle = null;
	  }

	  while (members.reqStack.length) {
	 	 members.reqStack.pop();
	  }
	  console.log('offline');
  });

  shadow.on('error', function(error) {
	  console.log('error: ', error);
  });

  shadow.on('status', function(thingName, stat, token, config) {
		let lastReq = members.reqStack.pop();
		if (lastReq.token === token) {
			console.log(thingName + '\'s ' + lastReq.op + ' stat: ' + stat);
			lastReq.promise.resolve(config);
		} else {
			console.log(thingName + '\'s received token is not mismatched (status)');
			lastReq.promise.reject("Oops, token mistmatched");
		}
		//console.log(JSON.stringify(config, null, '\t'));
  });

  shadow.on('timeout', function(thingName, clientToken) {
		let lastReq = members.reqStack.pop();
		if (lastReq.token === clientToken) {
			console.log(thingName + '\'s ' + lastReq.op + ' request is timeout');
		} else {
			console.log(thingName + '\'s received token is not mismatched (timeout)');
		}
  });

	shadow.on('message', function(topic, payload) {
	  console.log("message: ", topic, payload.toString());
		let found = _.findWhere(members.topics, { sub: topic });
		if (!found) {
			console.log('invalid topic: ', topic);
			return;
		}

		// TODO we need to resolve or reject it for promise created on topic request
		// pubTopic should include specific id per request
		// currently we remove last request of the topic
		//
		let requests = _.where(members.publishedTopicStack, { pubTopic: found.pub });
		let lastRequest = requests.pop();
		lastRequest.promise.resolve(JSON.parse(payload.toString()));
		members.publishedTopicStack = _.without(members.publishedTopicStack, lastRequest);
	});

	return Promise.resolve();
}

function _createShadow(self)
{
	let members = privateMembers.get(self);
	let args = members.args;
	let option = {
		clientId: args.clientId,
		region: args.region,
		protocol: 'wss',
		keepalive: args.keepAlive,
		host: args.awsHost,
		debug: true,
		accessKeyId: '',
		secretKey: '',
		sessionToken: ''
	};

	try {
		members.thingShadow = thingShadow(option);
	} catch (err) {
		console.log('failed to make aws thingshadow: ', err);
		return false;
	}

	return true;
}

function _updateAwsCredentials(self)
{
	let members = privateMembers.get(self);

	aws.config.region = awsConfig.region;
	aws.config.credentials = new aws.CognitoIdentityCredentials({
		 IdentityPoolId: awsConfig.poolId
	});


	let cognitoIdentity = new aws.CognitoIdentity();
	console.log('credentials: ', cognitoIdentity);

	return new Promise(function (resolve, reject) {
		aws.config.credentials.get(function(err, data) {
			if (!err) {
				console.log('retrieved identity: ' + aws.config.credentials.identityId);
				let params = {
					IdentityId: aws.config.credentials.identityId
				};
				cognitoIdentity.getCredentialsForIdentity(params, function(err, data) {
					if (!err) {
						//
						// Update our latest AWS credentials; the MQTT client will use these
						// during its next reconnect attempt.
						//
						members.thingShadow.updateWebSocketCredentials(
								data.Credentials.AccessKeyId,
								data.Credentials.SecretKey,
								data.Credentials.SessionToken);
						resolve("success");
					} else {
						console.log('error retrieving credentials: ' + err);
						reject(err);
					}
				});
			} else {
				console.log('error retrieving identity:' + err);
				reject(err);
			}
		});
	});


}

function _connectToAws(self)
{
	let members = privateMembers.get(self);

	return new Promise(function (resolve, reject) {
		members.thingShadow.register(members.thingName, { 
			ignoreDeltas: true,
			operationTimeout: operationTimeout
		}, function (err, failedTopics) {
			if (!err && !failedTopics) {
				console.log('thing registered as aws iot');
				// subscribe specific topics
				for (let topic of members.topics) {
					members.thingShadow.subscribe(topic.sub);
				}
				resolve(self);
			} else {
				reject(err);
			}
		});
	});
}

function _commitOperationToAws(self, operation, data = {}, promise = {})
{
	let members = privateMembers.get(self);

	let token;
  if (operation === 'get' || operation == 'delete') {
		token = members.thingShadow[operation](members.thingName);
	} else if (operation === 'update') {
		token = members.thingShadow[operation](members.thingName, data);
	} else {
		return Promise.reject('unknown operation: ', operation);
	}

	if (token === null) {
		if (members.timeoutHandle !== null) {
			console.log('other operation not finished. This will be retried.. ');
			members.timeoutHandle = setTimeout(() =>
				 	_commitOperationToAws(self, operation, data),
				 operationTimeout * 2);
		}	
	} else {
		members.reqStack.push({ op: operation, token: token, promise: promise });
	}

	return Promise.resolve();
}

function replaceObjectValue(object, src, dest)
{
	// TODO we would need to handle serveral depth of json data
	// but here we only handle 1 depth.
	/*
	for (let key in object) {
		if (object[key] === src)	{
			object[key] = null;
		}
	}
	*/

	// TODO this is workaround to replace object's value
	// we need to use more flexible way for replacement.
	let changedString = JSON.stringify(object).replace('\'' + src + '\'', dest);
	return JSON.parse(changedString);
}

module.exports = RemoteConfigManager;
