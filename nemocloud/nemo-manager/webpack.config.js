var path    = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  devtool: 'inline-source-map',
  entry: {},
	node: {
		//console: 'empty',
		fs: 'empty',
		net: 'empty',
		tls: 'empty'
	},
  module: {
    loaders: [
       { test: /\.js$/, exclude: [/node_modules/], loader: 'ng-annotate!babel' },
       { test: /\.json$/, loader: 'json' },
       { test: /\.html$/, loader: 'html?' + JSON.stringify({ attrs: ['img:src', 'img:ng-src'] }) }, 
       { test: /\.styl$/, loader: 'style!css!stylus' },
       { test: /\.css$/, loader: 'style!css' },
			 { test: /\.(png|jpg)$/, loader: 'file' },
			 {
				 test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
				 loader: "url?limit=10000&mimetype=application/font-woff"
			 }, {
				 test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
				 loader: "url?limit=10000&mimetype=application/font-woff"
			 }, {
				 test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
				 loader: "url?limit=10000&mimetype=application/octet-stream"
			 }, {
				 test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
				 loader: "file"
			 }, {
				 test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
				 loader: "url?limit=10000&mimetype=image/svg+xml"
			 }
    ]
  },
  plugins: [
    // Injects bundles in your index.html instead of wiring all manually.
    // It also adds hash to all injected assets so we don't have problems
    // with cache purging during deployment.
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      inject: 'body',
      hash: true
    }),

    // Automatically move all modules defined outside of application directory to vendor bundle.
    // If you are using more complicated project structure, consider to specify common chunks manually.
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module, count) {
        return module.resource && module.resource.indexOf(path.resolve(__dirname, 'src')) === -1;
      }
    }),

		new webpack.ProvidePlugin({
			'window.jQuery': 'jquery',
			'window.$': 'jquery',
			$: 'jquery',
			jQuery: 'jquery'
		})
  ]
};
