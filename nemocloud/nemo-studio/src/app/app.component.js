// declare dependancies used by app-wide
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import 'animate.css/animate.css';

import './app.css';
import template from './app.html';
import controller from './app.controller';

let appComponent = {
  restrict: 'E',
  template,
	controller,
  controllerAs: 'vm'
};

export default appComponent;
