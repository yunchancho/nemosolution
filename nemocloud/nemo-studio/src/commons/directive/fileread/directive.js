class Fileread {
  constructor() {
    'ngInject'
    this.scope = {
      fileread: "=",
      onRead: "="
    }
  }
  
  link(scope, element, attributes) {
    element.bind("change", (changeEvent) => {
      var reader = new FileReader();
      reader.onload = (loadEvent) => {
        scope.$apply(() => {
          scope.fileread = changeEvent.target.files[0];
          scope.fileread.fileStream = loadEvent.target.result;
          console.log('Selected file:', scope.fileread)
          if (scope.onRead) {
            scope.onRead();
          }
        });
      }
      if (!!changeEvent.target.files[0]) {
        reader.readAsArrayBuffer(changeEvent.target.files[0]);
      }
    });
  }
}

export default Fileread;