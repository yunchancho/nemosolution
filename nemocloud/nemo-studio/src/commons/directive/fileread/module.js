import angular from 'angular';
import Fileread from './directive'

const module = angular.module('app.commons.directive.fileread', [
])
.directive('fileread', () => new Fileread)
.name;

export default module;
