let packageFilter = () => {
  // measure: 'KB', 'MB', 'GB'
  // point: number of numbers under point  
  return (input, measure, point) => {
    if (input === '' || !['KB', 'MB', 'GB'].includes(measure)) {
      return '';
    }

    let devider = 0;
    switch(measure) {
      case 'KB':
        devider = 1024;
        break;
      case 'MB':
        devider = 1024 * 1024;
        break;
      case 'GB':
        devider = 1024 * 1024 * 1024;
        break;
    }

    let precision = 1;
    for (let i = 0; i < point; i++) {
      precision *= 10;
    }
    
    return Math.round((input / devider) * precision) / precision;
  }
}

export default packageFilter;