import angular from 'angular';
import filter from './filter'

const name = 'bytesFilter'

const module = angular.module('app.commons.filters.bytes', [
])
.filter(name, filter)
.name;

export default module;
