import angular from 'angular';

import bytes from './bytes/module'
import s3Directory from './s3Directory/module';

const module = angular.module('app.commons.filters', [
	s3Directory,
	bytes
])
.name

export default module;
