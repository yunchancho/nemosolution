// TODO: Keyboard covers input form in smartphone
class AuthModal {
  constructor($rootScope, $uibModal){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.currentModal;
  }

  close() {
    if (!!this.currentModal) {
      this.currentModal.close();
    }
  }

  // Open or replace component based modal
  // and return closed promise
  open(component, resolveMap) {
    if (this.currentModal) {
      this.currentModal.close();
    }

    let resolve = {
      map: () => {return resolveMap}
    };

    this.currentModal = this.$uibModal.open({
      backdrop: false,
      openedClass: 'image-bg',
      component,
      resolve,
      keyboard: false,
      windowClass: 'modal-scroll'
    });

    return this.currentModal;
  }
}

export default AuthModal;
