import angular from 'angular';
import CognitoService from './service';

const module = angular.module('app.commons.services.cognito', [])
.service('cognito', CognitoService)
.name;

export default module;
