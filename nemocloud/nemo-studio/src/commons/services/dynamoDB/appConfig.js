const appConfig = {
  tables: {
    customer: 'Customer',
    certificate: 'Certificate',
    registry: 'Registry'
  },
  deviceItems: {
    bigdata: '_bigdata_',
    contentSync: '_contentsync_',
    packageList: '_pkglist_',
    packageIndex: '_pkgindex_',
    mandatoryPackages: '_mandatorypkgs_',
    power: '_power_',
    theme: '_theme_'
  },
  contentsSrcPrefix: 's3://nemosnow-contents',
  packageIndexHost: 's3://nemosnow-packages.s3-ap-northeast-1.amazonaws.com',
  initDynamoDBParams: {},
}

export default appConfig;
