import angular from 'angular';
import DynamoDB from './service';

const module = angular.module('app.commons.services.dynamoDB', [
  'app.commons.services.cognito',
])
.service('dynamoDB', DynamoDB)
.name;

export default module;
