import 'aws-sdk';
import Promise from 'bluebird'
import appConfig from './appConfig';

class DynamoDB {
  constructor($rootScope, cognito){
    'ngInject'
    this.cognito = cognito;
    this.$rootScope = $rootScope;
    this.docClient;
    this.dynamoDB;

    this.init();
    this.registerInitFunc();
  }

  getConfig() {
    return appConfig;
  }

  init() {
    this.dynamoDB = Promise.promisifyAll(new AWS.DynamoDB());
    this.docClient = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient({ service: this.dynamoDB }));
  }

  registerInitFunc() {
    this.cognito.addOnLoginFunction('dynamoDB', () => {
      this.init();
    })
    this.cognito.addOnLogoutFunction('dynamoDB', () => {
      this.init();
    })
  }
  
  removeHashKey(object) {
    let removeHash = (current) => {
      if (typeof current === 'object') {
        delete current['$$hashKey']
        for (let key in current) {
          if (typeof current[key] === 'string') {
            if (current[key] === '') {
              delete current[key];
            }
          } else {
            removeHash(current[key])
          }
        }
      }
    }
    let clone = JSON.parse(JSON.stringify(object))
    removeHash(clone);
    return clone;
  }
}

export default DynamoDB;
