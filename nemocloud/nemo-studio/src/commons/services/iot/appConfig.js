export default {
  status: {
    initial: 'initial',
    deactivated: 'deactivated',
    activated: 'activated', 
    waitConfirm: 'wait_confirm'
  }
}
