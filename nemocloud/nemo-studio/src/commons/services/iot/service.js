import 'aws-sdk';
import Parallel from '../../../lib/parallel/index.js'
import Promise from 'bluebird'
import appConfig from './appConfig'


class Iot {
  constructor($rootScope, cognito, dynamoDB){
    'ngInject'
    this.cognito = cognito;
    this.$rootScope = $rootScope;
    this.dynamoDB = dynamoDB;
    this.iot;
    
    this.init();
    this.registerInitFunc();
  }
  
  getConfig() {
    return appConfig
  }
  
  init() {
    this.iot = Promise.promisifyAll(new AWS.Iot());
  }
  
  registerInitFunc() {
    this.cognito.addOnLoginFunction('iot', () => {
      this.init();
    })
    this.cognito.addOnLogoutFunction('iot', () => {
      this.init();
    })
  }
}

export default Iot;