import angular from 'angular';
import Lambda from './service';

const module = angular.module('app.commons.services.lambda', [
  'app.commons.services.cognito',
])
.service('lambda', Lambda)
.name;

export default module;
