import 'aws-sdk';

class Lambda {
  constructor($rootScope, cognito){
    'ngInject'
    this.cognito = cognito;
    this.$rootScope = $rootScope;
    this.lambda;
    
    this.init();
    this.registerInitFunc();
  }
  
  init() {
    this.lambda = new AWS.Lambda();
  }
  
  registerInitFunc() {
    this.cognito.addOnLoginFunction('lambda', () => {
      this.init();
    })
    this.cognito.addOnLogoutFunction('lambda', () => {
      this.init();
    })
  }
  
  invoke(functionName, context) {
    return new Promise((resolve, reject) => {
      if (!this.cognito.isLogin()) {
        reject(new Error('Not logged in'));
      }
      
      let b64Context = AWS.util.base64.encode(JSON.stringify(context))
      
      var params = {
        FunctionName: functionName,
        ClientContext: b64Context
      }
      
      this.lambda.invoke(params, (err, result) => {
        if (err) {
          // Lambda sysrem error(not invoked in lambda function callback parameter)
          // ex) function permission error
          return reject(err);
        }
        
        if (!!result.FunctionError) {
          // Error which is invoked by lambda function callback (user defined error)
          return reject(!!result.Payload && JSON.parse(result.Payload).errorMessage);
        }
        
        return resolve(!!result.Payload && JSON.parse(result.Payload));
      });
    });
  }
}

export default Lambda;