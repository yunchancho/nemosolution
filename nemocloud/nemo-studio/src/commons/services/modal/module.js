import angular from 'angular';
import Modal from './service';

const module = angular.module('app.commons.services.modal', [
  'ui.bootstrap'
])
.service('modal', Modal)
.name;

export default module;
