import angular from 'angular';

import authModal from './authModal/module';
import cognito from './cognito/module';
import dynamoDB from './dynamoDB/module';
import iot from './iot/module';
import s3 from './s3/module';
import spinner from './spinner/module';
import modal from './modal/module';
import lambda from './lambda/module';

const module = angular.module('app.commons.services', [
	authModal,
	cognito,
	dynamoDB,
	iot,
	s3,
	spinner,
	modal,
	lambda
])
.name

export default module;
