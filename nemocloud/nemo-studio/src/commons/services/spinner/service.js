class Spinner {
  constructor($rootScope, modal){
    'ngInject'
    this.$rootScope = $rootScope;
    this.modal = modal;
    this.show = 0;
    this.timer;
  }
  
  on() {
    this.show = this.show + 1;
    this.timer = setTimeout(() => {
      if (this.show !== 0) {
        this.show = 0;
        // TODO: Move error message code to out of service module
        let params = {
          title: '연결 오류',
          content: '응답 시간이 초과되었습니다.',
        }
        this.modal.error(params)
      }
    }, 60 * 1000)
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
  
  off() {
    this.show = this.show - 1;
    if (this.show < 0) {
      this.show = 0
    }
    if (this.show === 0) {
      clearTimeout(this.timer)
    }
    this.$rootScope.$$phase || this.$rootScope.$apply();
  }
  
}

export default Spinner;