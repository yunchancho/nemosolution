import angular from 'angular';

import login from './login/module';
import confirmEmail from './confirmEmail/module';
import forgotPassword from './forgotPassword/module';
import newPasswordRequired from './newPasswordRequired/module';
import register from './register/module';
import resetPassword from './resetPassword/module';


const module = angular.module('app.commons.views.auth', [
	login,
	confirmEmail,
	forgotPassword,
	newPasswordRequired,
	register,
	resetPassword
	])
.name

export default module 
