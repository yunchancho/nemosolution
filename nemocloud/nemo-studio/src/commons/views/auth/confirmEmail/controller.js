class Controller {
  constructor($state, authModal, cognito){
    'ngInject'
    this.name = 'confirmEmail';
    this.$state = $state;
    this.authModal = authModal;
    this.cognito = cognito;
    this.code = '';
    this.username = this.cognito.getUsername();
    this.usernameDisable = !!this.username;
    
  }
  
  $onInit() {
    this.redirectHome();
  }
  
  confirm() {
    this.cognito.confirmRegistration(this.code)
    .then(result => {
      console.log('Confirm email', result);
      this.authModal.open('login');
      return null;
    })
    .catch(err => console.log('Confirm email', err))
  }
  
  
  // Redirect to index page when user already logged in
  redirectHome() {
    if (this.cognito.isLogin()) {
      this.close();
    }
  }
  
  
  resendConfirmationCode() {
    this.cognito.initCognitoUser(this.username);
    this.cognito.resendConfirmationCode()
    .then(result => {
      console.log("confirmation code sended", result);
    })
    .catch(err => {
      console.error("sending confirmation code failed", err)
    })
  }
}

export default Controller;
