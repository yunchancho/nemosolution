import testModule from './module'
import testComponent from './component';
import testController from './controller';
import testTemplate from './template.html';


describe('login', () => {
  let $componentController, cognito;

  beforeEach(window.module('app'));
  beforeEach(inject((_$componentController_, _cognito_) => {
    $componentController = _$componentController_;
    cognito = _cognito_;
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    let controller;
    beforeEach(() => {
      let bindings = {
        close: () => {}
      }
      controller = $componentController(testModule, null, bindings);
    }) 
    
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      expect(controller).to.have.property('password');
      expect(controller).to.have.property('pendingLogin');
      expect(controller).to.have.property('username');
      expect(controller).to.have.property('errorMessage');
    });
    
    it('login with empty username', () => {
      controller.username = '';
      controller.password = meta.password;
      return controller.login()
      .then(() => {
        expect(controller.errorMessage).to.equal('아이디와 비밀번호를 입력하세요.');
        expect(cognito.isLogin()).to.equal(false);
        return null;
      })
    })
    
    it('login with wrong username', () => {
      controller.username = 'gaerbzbeagdfg';
      controller.password = meta.password;
      return controller.login()
      .then(() => {
        expect(controller.errorMessage).to.equal('존재하지 않는 유저입니다.');
        expect(cognito.isLogin()).to.equal(false);
        return null;
      })
    })
    
    // TODO: Test wrong password after resolve login attempt count exceed error
    it.skip('login with empty password', () => {
      controller.username = meta.username;
      controller.password = '';
      return controller.login()
      .then(() => {
        // expect(controller.errorMessage).to.equal('존재하지 않는 유저입니다.');
        expect(cognito.isLogin()).to.equal(false);
        return null;
      })
    })
    
    it.skip('login with wrong password', () => {
      controller.username = meta.username;
      controller.password = 'aaa';
      return controller.login()
      .then(() => {
        expect(controller.errorMessage).to.equal('아이디와 비밀번호가 일치하지 않습니다.');
        expect(cognito.isLogin()).to.equal(false);
        return null;
      })
    })
    
    it('login with correct password', () => {
      controller.username = meta.username;
      controller.password = meta.password;
      return controller.login()
      .then(() => {
        expect(cognito.isLogin()).to.equal(true);
        expect(cognito.type).to.equal('user');
        return null;
      })
    })
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(testTemplate).to.match(/cpu 실시간 사용량/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = testComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(testTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(testController);
    });
  });
});
