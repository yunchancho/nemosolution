import angular from 'angular';
import uiRouter from 'angular-ui-router';
import component from './bottombar.component';

const name = 'bottombar';

let module = angular.module(name, [])
.component(name, component)
.name;

export default module;
