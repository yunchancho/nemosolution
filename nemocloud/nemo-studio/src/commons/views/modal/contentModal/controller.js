class Controller {
  constructor($state){
    'ngInject'
    this.$state = $state;
    this.title;
    this.content;
  }
  
  $onInit() {
    this.title = this.resolve.title;
    this.content = this.resolve.content;
  }
  
  close() {
    this.close();
  }
}

export default Controller;
