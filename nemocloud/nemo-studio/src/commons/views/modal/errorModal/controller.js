class Controller {
  constructor($state){
    'ngInject'
    this.$state = $state;
    this.title;
    this.content;
    this.error;
  }
  
  $onInit() {
    this.title = this.resolve.title;
    this.content = this.resolve.content;
    this.error = this.resolve.error;
  }
  
  close() {
    this.close();
  }
}

export default Controller;
