import angular from 'angular';

import successModal from './successModal/module';
import confirmModal from './confirmModal/module';
import errorModal from './errorModal/module';
import contentModal from './contentModal/module';

const module = angular.module('app.commons.views.modal', [
  successModal,
  confirmModal,
  errorModal,
  contentModal,
])
.name

export default module
