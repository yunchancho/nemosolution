import template from './navbar.html';
import controller from './navbar.controller';

let component = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default component;
