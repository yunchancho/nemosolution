class NavbarController {
  constructor($rootScope, cognito) {
    'ngInject'
    this.cognito = cognito;
    this.name = 'navbar';
    this.$rootScope = $rootScope;
    this.type;
  }
  
  $onInit() {
    this.type = this.cognito.getType();
  }
}

export default NavbarController;
