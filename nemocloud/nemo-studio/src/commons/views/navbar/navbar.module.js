import angular from 'angular';
import uiRouter from 'angular-ui-router';
import component from './navbar.component';

const name = 'navbar';

let module = angular.module(name, [])
.component(name, component)
.name;

export default module;
