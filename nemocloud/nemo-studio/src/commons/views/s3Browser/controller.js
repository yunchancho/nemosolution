class Controller {
  constructor($rootScope, $uibModal, cognito, modal, s3, spinner){
    'ngInject'
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.cognito = cognito;
    this.modal = modal;
    this.s3 = s3;
    this.spinner = spinner;
    this.indexList = [];
    this.directory = '';
    this.pickedFile = {};
    // functions in s3FileList
    this.resolve = {
      checkCounter: 0,
      refreshList: null,
      deleteFile: null,
      deleteFiles: null,
      detailFile:null,
      downloadFiles: null,
      fileCheck:null
    }
    this.selectedIndex;
    this.options = {
      bucket: '',
      indexPrefix: '',
      func: {
        refreshIndex: null,
        listIndex: null
      },
    }
  }

  $onInit() {
    this.options.func.refreshIndex = () => {
      return this.getIndexList()
    }
    this.spinner.on()
    this.getIndexList()
    .then(() => this.spinner.off())
  }

  createFolder() {
    let modal = this.$uibModal.open({
      component: 's3CreateFolder',
      resolve: {
        bucket: () => { return this.options.bucket },
        directory: () => { return this.directory },
        onComplete: () => { return () => this.resolve.refreshList() },
      },
      size: 'sm'
    });
  }

  deleteIndex(index) {
    console.log(index)
    let params = {
      title: '인덱스 삭제',
      content: '삭제하시겠습니까?\n',
      warning: '내부의 모든 파일이 삭제됩니다.',
      onConfirm: () => {
        this.resolve.deleteFile(index.Prefix)
        .then(() => this.getIndexList())
      }
    }
    this.modal.confirm(params)
  }

  getIndexList() {
    let prom;

    if (this.options.func.listIndex) {
      prom = this.options.func.listIndex(this.options.bucket, this.options.indexPrefix)
    } else {
      prom = this.s3.listIndex(this.options.bucket, this.options.indexPrefix)
    }

    return prom.then((result) => {
      this.indexList = result;
      try {
        this.selectedIndex = this.indexList[0].Prefix;
        console.log('인덱스 리스트 : ', this.indexList);
        this.$rootScope.$$phase || this.$rootScope.$apply();
      } catch (e) {
        console.error('no indexes', e)
      }
      console.log('index List', result);
      return this.indexList;
    })
    .catch((err) => console.error('getIndexList:', err))
  }

  uploadModal(){
    let modal = this.$uibModal.open({
      component: 'uploadModal',
      resolve: {
        options: () => this.options,
        refreshList: () => { return () => this.resolve.refreshList() },
        directory : () => this.directory,
      },
      size: 'md'
    });
  }
}

export default Controller;
