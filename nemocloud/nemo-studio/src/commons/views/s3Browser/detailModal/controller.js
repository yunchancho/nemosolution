class Controller {
  constructor($state, cognito, s3, spinner) {
    'ngInject'
    this.$state = $state;
    this.s3 = s3;
    this.cognito = cognito;
    this.spinner = spinner;
    this.pickedFile;
    this.directory;
    this.options;
    this.file;
  }

  $onInit() {
    this.file = this.resolve.file;
    console.log(this.file);
  }

  close() {
    this.close();
  }

}

export default Controller;
