import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import s3CreateFolder from './s3CreateFolder/module';
import s3FileList from './s3FileList/module';
import uploadModal from './uploadModal/module';

const name = 's3Browser';

let module = angular.module(name, [
	uiRouter,
	'app.commons.services.s3',
	'app.commons.services.spinner',
	'app.commons.filters.s3Directory',
	s3CreateFolder,
	s3FileList,
	uploadModal
	])
	.component(name, component)
	.name

export default module;
