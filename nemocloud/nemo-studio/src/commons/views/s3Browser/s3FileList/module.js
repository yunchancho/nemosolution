import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import detailModal from '../detailModal/module'

const name = 's3FileList';

let module = angular.module(name, [
	uiRouter,
	'app.commons.services.s3',
	'app.commons.services.spinner',
	'app.commons.filters.s3Directory',
	detailModal
	])
	.component(name, component)
	.name

export default module;
