class Controller {
  constructor($state, cognito, s3, spinner) {
    'ngInject'
    this.$state = $state;
    this.s3 = s3;
    this.cognito = cognito;
    this.spinner = spinner;
    this.pickedFile;
    this.directory;
    this.options;

  }

  $onInit() {
    this.directory = this.resolve.directory
    this.options = this.resolve.options
  }

  close() {
    this.close();
  }

  uploadFile() {
    let filename = this.directory + this.pickedFile.name;
    console.log('Upload start:', filename);
    this.spinner.on()
    this.s3.uploadFile(this.options.bucket, filename, this.pickedFile)
    .then((result) => {
      this.resolve.refreshList();
      console.log('Upload finish', result);
      return result
    })
    .catch((err) => {
      console.error('File upload error', err)
      let params = {
        title: '파일 업로드 실패',
        content: '파일 업로드 중 오류가 발생했습니다.',
        error: err.message
      }

      switch(err.message) {
        case 'Unsupported body payload object':
        params.content = '파일이 선택되지 않았거나 지원하지 않는 형식입니다.'
        params.error = '';
        break;
      }

      this.modal.error(params)
    })
    .then(() => this.spinner.off())
    .then(() => this.close())
  }
}

export default Controller;
