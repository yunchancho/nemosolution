import testModule from './topbar.module'
import testComponent from './topbar.component';
import testController from './topbar.controller';
import testTemplate from './topbar.html';

describe('topbar', () => {
  let makeController, $rootScope, $state, cognito, modal, spinner;

  beforeEach(window.module(testModule));
  beforeEach(inject((_$rootScope_, _$state_, _cognito_, _modal_, _spinner_) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    cognito = _cognito_;
    modal = _modal_;
    spinner = _spinner_;
    makeController = () => {
      return new testController($rootScope, $state, cognito, modal, spinner);
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
    });
    
    it('logout', (done) => {
      let controller = makeController(done);
      cognito.login(meta.username, meta.password)
      .then(() => {
        expect(cognito.isLogin()).to.equal(true);
        controller.logout();
        expect(cognito.isLogin()).to.equal(false);
      })
      .then(done, done)
    })
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(testTemplate).to.match(/cpu 실시간 사용량/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = testComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(testTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(testController);
    });
  });
});
