import template from './topbar.html';
import controller from './topbar.controller';

let component = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default component;
