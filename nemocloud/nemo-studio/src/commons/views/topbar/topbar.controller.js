class TopbarController {
  constructor($rootScope, $state, cognito, modal, spinner){
    'ngInject'
    this.cognito = cognito;
    this.name = 'topbar';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.modal = modal;
    this.spinner = spinner;
  }
  
  logout() {
      this.cognito.signOut();
    if (this.$state.$current.name === 'home') {
      this.$state.reload();
    } else {
      this.$state.go('home')
    }
  }
}

export default TopbarController;
