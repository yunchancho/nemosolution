import angular from 'angular';

import auth from './auth/auth.module'
import navbar from './navbar/navbar.module';
import topbar from './topbar/topbar.module';
import bottombar from './bottombar/bottombar.module';
import modal from './modal/module'
import s3Browser from './s3Browser/module';

const module = angular.module('app.commons.views', [
  auth,
  navbar,
	topbar,
	bottombar,
  modal,
  s3Browser
])
.name

export default module
