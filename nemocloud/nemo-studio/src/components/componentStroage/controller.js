class Controller {
  constructor($rootScope, s3, cognito) {
    'ngInject'
    this.name = 'componentStroage';
    this.cognito = cognito;
    this.$rootScope = $rootScope;
    this.s3 = s3;
    this.options = {
      bucket: '',
      indexPrefix: '',
      func: {
        refreshIndex: null,
        listIndex: () => this.s3.listBackupIndex()
      },
    }
  }

  $onInit() {
    this.options.bucket = this.s3.getConfig().backupsBucket
    this.cognito.getCustomer()
    .then(customer => {
      this.options.indexPrefix = `${customer}/`;
      this.$rootScope.$$phase || this.$rootScope.$apply();
      return null;
    })
    .catch(err => {
      console.error(`can't get customer`, err)
    })
  }
}

export default Controller;
