import angular from 'angular';

import home from './home/module';
import nemoPolicy from './nemoPolicy/module';
import nemoIntro from './nemoIntro/module';
import userInfo from './userInfo/module';
import scene from './scene/module';
import nemoList from './nemoList/module';
import componentStroage from './componentStroage/module'

let module = angular.module('app.components', [
  home,
  nemoPolicy,
  nemoIntro,
  userInfo,
  scene,
  nemoList,
  componentStroage
])
.name;

export default module;
