class Controller {
  constructor($rootScope) {
    'ngInject'
    this.name = 'home';
    this.$rootScope = $rootScope;
  }
}

export default Controller;
