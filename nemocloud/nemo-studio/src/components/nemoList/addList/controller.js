class Controller {
  constructor($rootScope) {
    'ngInject'
    this.name = 'addList';
    this.lists = [
      {
        name: "",
        radio: false
      },
      {
        name: "widget1",
        radio: false
      },
      {
        name: "widget2",
        radio: false
      },
      {
        name: "widget3",
        radio: false
      },
      {
        name: "widget4",
        radio: false
      },
      {
        name: "widget5",
        radio: false
      },
      {
        name: "widget6",
        radio: false
      },
      {
        name: "widget7",
        radio: false
      },
      {
        name: "widget8",
        radio: false
      },
      {
        name: "widget9",
        radio: false
      },
      {
        name: "widget10",
        radio: false
      },
      {
        name: "widget11",
        radio: false
      },
      {
        name: "widget12",
        radio: false
      },
      {
        name: "widget13",
        radio: false
      },
      {
        name: "widget14",
        radio: false
      },
      {
        name: "widget15",
        radio: false
      }
    ];
    this.values = [
      {
        name: "Apple Device",
        src: "p3.jpg"
      }, {
        name: "Samsung Device",
        src: "p1.jpg"
      }, {
        name: "DELL Device",
        src: "p2.jpg"
      }
    ];
    this.scenes = [
      {
        check: false,
        scene: {
          name: "nemo1",
          src: "p1.jpg"
        }
      }, {
        check: false,
        scene: {
          name: "nemo2",
          src: "p2.jpg"
        }
      }, {
        check: false,
        scene: {
          name: "nemo3",
          src: "p3.jpg"
        }
      }
    ]
    this.sortableOptions = {
      update: (e, ui) => console.log(e, ui),
      axis: 'x'
    };
    this.$rootScope = $rootScope;
    this.screen = {};
  }

  addSceneList() {
    for (let scene of this.scenes) {
      if (scene.check === true) {
        let item = JSON.stringify(scene.scene)
        this.values.push(JSON.parse(item))
      }
    }
    console.log("values :  ", this.values);
  }
  removeList(index) {
    alert("제거!")
  }
  radioClass(index) {
    let i = 0;
    for (i = 0; i < this.lists.length; i++) {
      this.lists[i].radio = false;
    }
    this.lists[index].radio = true;

    this.screen.name = this.lists[index].name;

  }

}

export default Controller;
