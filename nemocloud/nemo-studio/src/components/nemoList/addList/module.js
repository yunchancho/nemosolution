import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

const name = 'addList';

let module = angular.module(name, [
	 uiRouter,
	 'ui.sortable'
 ])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('nemoList.addList', {
			url: '/addList',
			component: name
		});
}
