class Controller {
  constructor($rootScope) {
    'ngInject'
    this.name = 'nemoList';
    this.$rootScope = $rootScope;
  }
}

export default Controller;
