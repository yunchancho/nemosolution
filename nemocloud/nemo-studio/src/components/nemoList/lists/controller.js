class Controller {
  constructor($rootScope, $state, modal, toastr, spinner) {
    'ngInject'
    this.$rootScope = $rootScope;
    this.state = $state;
    this.modal = modal;
    this.toastr = toastr;
    this.spinner = spinner;
    this.lists = [
      {
        name : "NEMOLIST1",
        length:5,
        productDate:"2017.01.02",
        updateDate:"2017.03.05",
        size:"1GB",
        runtime : "00:30:00"
      },
      {
        name : "NEMOLIST2",
        length:5,
        productDate:"2017.01.02",
        updateDate:"2017.03.05",
        size:"1GB",
        runtime : "00:30:00"
      },
      {
        name : "NEMOLIST3",
        length:5,
        productDate:"2017.01.02",
        updateDate:"2017.03.05",
        size:"1GB",
        runtime : "00:30:00"
      }
    ] ;
  }
  confirmRemove(list){
    let params = {
      title: '리스트 삭제',
      content: '삭제하시겠습니까?',
      onConfirm: () => this.remove(list)
    }
    this.modal.confirm(params)
  }
  listUpdate(){
    this.state.go('nemoList.addList')
  }
  remove(index){
    this.spinner.on()

    return Promise.resolve()
    .then(() => {
        this.lists.splice(index, 1)
    })
    .then(() => this.toastr.success(`컨텐츠를 제거했습니다.`))
    .catch((err) => {
      console.error('removeContents', err);
      let params = {
        title: '컨텐츠 제거 실패',
        content: '컨텐츠 제거 도중 오류가 발생했습니다.',
        error: err.message
      }
      this.modal.error(params)
    })
    .then(() => this.spinner.off())
  }


}

export default Controller;
