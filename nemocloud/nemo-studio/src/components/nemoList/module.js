import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import lists from './lists/module'
import addList from './addList/module'

const name = 'nemoList';

let module = angular.module(name, [
	 uiRouter,
	 lists,
	 addList
 ])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/nemoList',
			component: name
		});
}
