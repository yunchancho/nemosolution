import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import templates from './templates/module'
import prod from './prod/module'

const name = 'add';

let module = angular.module(name, [
	 uiRouter,
	 templates,
	 prod
 ])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('scene.add', {
			url: '/add',
			component: name
		});
}
