class Controller {
  constructor($rootScope, $uibModal) {
    'ngInject'
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
  }
  add(){
    this.$uibModal.open({
        component: 'addModal',
        resolve: {
            title: () => "상세정보",
            content:()=>"정보"
        },
        size: 'lg'
    })
  }
  preview(){
    this.$uibModal.open({
        component: 'previewModal',
        resolve: {
            title: () => "미리보기"
        },
        size: 'md'
    })
  }
}

export default Controller;
