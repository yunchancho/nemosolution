import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'textangular';
import 'textangular/dist/textAngular-sanitize.min'
import angularSanitize from 'angular-sanitize';

import component from './component';

import addModal from './addModal/module'
import previewModal from './previewModal/module'

const name = 'prod';

let module = angular.module(name, [
	 uiRouter,
	 addModal,
	 previewModal,
	 angularSanitize,
	 'textAngular',
 ])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider, $provide) {
	"ngInject";

	$stateProvider
		.state('scene.add.prod', {
			url: '/prod',
			component: name
		});

	$provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions) { // $delegate is the taOptions we are decorating
			taOptions.toolbar= [
      ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
      ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent']
  ];
			return taOptions;
	}]);
}
