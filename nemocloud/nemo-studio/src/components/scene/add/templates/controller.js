class Controller {
  constructor($rootScope,$state) {
    'ngInject'
    this.$rootScope = $rootScope;
    this.$state = $state;
  }

  product(){
    this.$state.go('scene.add.prod')
  }
}

export default Controller;
