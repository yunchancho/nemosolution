class Controller {
  constructor($rootScope) {
    'ngInject'
    this.name = 'scene';
    this.$rootScope = $rootScope;
  }
}

export default Controller;
