import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';
import sceneList from './sceneList/module';
import add from './add/module';

const name = 'scene';

let module = angular.module(name, [
	 uiRouter,
	 sceneList,
	 add,
 ])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state(name, {
			url: '/scene',
			component: name
		});
}
