class Controller {
  constructor($rootScope, $uibModal) {
    'ngInject'
    this.name = 'sceneList';
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
  }

  detail(){
    this.$uibModal.open({
        component: 'sceneListModal',
        resolve: {
            title: () => "상세정보",
            content:()=>"정보"
        },
        size: 'lg'
    })
  }
}

export default Controller;
