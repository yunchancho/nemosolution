import angular from 'angular';
import uiRouter from 'angular-ui-router';

import component from './component';

import sceneListModal from './sceneListModal/module'

const name = 'sceneList';

let module = angular.module(name, [
	 uiRouter,
	 sceneListModal
 ])
	.config(config)
	.component(name, component)
	.name

export default module;

function config($urlRouterProvider, $stateProvider) {
	"ngInject";

	$stateProvider
		.state('scene.sceneList', {
			url: '/sceneList',
			component: name
		});
}
