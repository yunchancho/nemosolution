import changePasswordModule from './module'
import changePasswordComponent from './component';
import changePasswordController from './controller';
import changePasswordTemplate from './template.html';

describe('changePassword', () => {
  let $rootScope, makeController;

  beforeEach(window.module(changePasswordModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new changePasswordController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('$state');
      expect(controller).to.have.property('cognito');
      expect(controller).to.have.property('newPassword');
      expect(controller).to.have.property('oldPassword');
      expect(controller).to.have.property('confirmPassword');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template', () => {
      expect(changePasswordTemplate).to.match(/변경/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = changePasswordComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(changePasswordTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(changePasswordController);
    });
  });
});
