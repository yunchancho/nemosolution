import template from './template.html';
import controller from './controller';

let component = {
  restrict: 'E',
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default component;
