class Controller {
  constructor($rootScope, $state, cognito){
    'ngInject'
    this.name = 'dropOutModal';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.cognito = cognito;
    this.confirm = '';
  }
  
  cancel() {
    this.close();
  }
  
  delete() {
    if (this.confirm !== 'delete') {
      console.log("input 'delete'")
      return;
    }
    this.cognito.deleteUser()
    .then(result => {
      this.close();
      this.$state.reload();
      return null;
    })
    .catch(err => console.error('deleteUser: ', err))
  }
  
}

export default Controller;
