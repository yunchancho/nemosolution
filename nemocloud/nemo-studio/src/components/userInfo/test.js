import userInfoModule from './module'
import userInfoComponent from './component';
import userInfoController from './controller';
import userInfoTemplate from './template.html';

describe('userInfo', () => {
  let $rootScope, makeController;

  beforeEach(window.module(userInfoModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new userInfoController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has properties', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    // it('has name in template', () => {
    //   expect(userInfoTemplate).to.match(/기기 선택/g);
    // });
  });

  describe('Component', () => {
    // component/directive specs
    let component = userInfoComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(userInfoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(userInfoController);
    });
  });
});
