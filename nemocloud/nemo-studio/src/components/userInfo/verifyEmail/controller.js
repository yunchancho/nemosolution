class Controller {
  constructor($rootScope, $state, cognito, toastr, modal){
    'ngInject'
    this.name = 'verifyEmail';
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.cognito = cognito;
    this.toastr = toastr;
    this.modal = modal;
    this.code = '';
    this.emailVerified = false;
  }
  
  $onInit() {
    this.getEmailVerified();
  }
  
  getEmailVerified() {
    return this.cognito.getUserAttribute('email_verified')
    .then(result => {
      this.emailVerified = (result === 'true');
      this.$rootScope.$$phase || this.$rootScope.$apply();
    })
    .catch(err => console.error('get email verified error: ', err))
  }
  
  sendVerificationCode() {
    this.cognito.sendAttributeVerificateCode('email')
    .then(() => {
      this.toastr.success('인증 코드를 전송했습니다.')
    })
    .catch(err => {
      let params = {
        title: '전송 실패',
        content: '인증 코드 전송 중 오류가 발생했습니다.',
        error: err.message
      }
      
      this.modal.error(params)
    })
  }
  
  verifyEmail() {
    this.cognito.verificateAttribute('email', this.code)
    .then(result => {
      console.log(result);
      return this.getEmailVerified();
    })
    .then(() => {
      this.toastr.success('인증 완료')
    })
    .catch(err => {
      console.error('verificate fails', err)
      let params = {
        title: '인증 실패',
        content: '인증 중 오류가 발생했습니다.',
        error: err.message
      }
      
      this.modal.error(params)
    })
  }
}

export default Controller;
