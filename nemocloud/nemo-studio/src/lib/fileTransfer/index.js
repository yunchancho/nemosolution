class FileTransfer {
  static downloadBlob(data, fileName, mimeType) {
    let blob, url;
    blob = new Blob([data], {
      type: mimeType
    });
    url = window.URL.createObjectURL(blob);
    this.downloadURL(url, fileName, mimeType);
    setTimeout(() => {
      return window.URL.revokeObjectURL(url);
    }, 1000);
  };
  
  static downloadURL(data, fileName) {
    let a;
    a = document.createElement('a');
    a.href = data;
    a.download = fileName;
    document.body.appendChild(a);
    a.style = 'display: none';
    a.click();
    a.remove();
  };
}

export default FileTransfer;