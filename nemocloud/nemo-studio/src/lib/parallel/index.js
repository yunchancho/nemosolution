class Parallel {
  constructor() {
    this.fetching = false;
    this.list = [];
  }
  
  // Manage concurrent function call (likes semaphore)
  // Run registered functions after first caller fetch something
  p(func) {
    if (this.fetching) {
      this.list.push(func);
      return false
    } else {
      this.fetching = true;
      return true;
    }
  }
  
  v() {
    for (let func of this.list) {
      func();
    }
    this.list = [];
    this.fetching = false;
  }
  
  // Call function for each item in list
  // Sequence is not guaranted
  static asyncCallFunc(list, func, callback) {
    if (list.length === 0) {
      // Empty list
      return callback(null, null);
    }
    
    let requests = list.map((item) => {
      return new Promise((resolve, reject) => {
        func(item, (err, result) => {
          if (err) {
            reject(err);
          }
          resolve(result);
        });
      });
    })
    
    Promise.all(requests.map(p => p.catch(e => e)))
    .then((value) => {
      return callback(null, value);
    })
    .catch((reason) => {
      return callback(reason);
    });
  }
  
  // Return promise that call functions(callback) for each item in list
  // Sequence is not guaranted
  // Transform all rejects to resolves
  static asyncCallPromise(list, func) {
    return new Promise((resolve, reject) => {
      if (list.length === 0) {
        // Empty list
        resolve();
      }
      
      let requests = list.map((item) => {
        return new Promise((resolve, reject) => {
          func(item, (err, result) => {
            if (err) {
              reject(err);
            }
            resolve(result);
          });
        });
      })
      
      Promise.all(requests.map(p => p.catch(e => e)))
      .then(resolve)
      .catch(reject);
    });
  }
  
  // Return promise that call promises for each item in list
  // Sequence is not guaranted
  static asyncAllPromise(list, promise) {
    return new Promise((resolve, reject) => {
      if (list.length === 0) {
        // Empty list
        resolve();
      }
      
      let requests = list.map((item) => {
        return promise(item).catch(e => e)
      })
      Promise.all(requests)
      .then(resolve)
      .catch(reject);
    });
  }
}

export default Parallel;