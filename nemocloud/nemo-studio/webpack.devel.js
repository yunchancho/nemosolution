const path = require('path')
const querystring = require('querystring')
const webpack = require('webpack')
const webpackMerge = require('webpack-merge')
const baseConfig = require('./webpack.common.js')

const serverPort = 8088

let ifdefOpt = {
	DEBUG: false,
	SNOW_MODE: process.env.SNOW_MODE || 'cloud'
}
let ifdefQuery = querystring.encode({json: JSON.stringify(ifdefOpt)})

let develConfig = {
	devtool: 'source-map',
	entry: [
		'./src/app/app.js'
	],
	output: {
		path: path.resolve(__dirname, 'devel'),
		filename: '[name].bundle.js',
		publicPath: '/'
	},
	devServer: {
		hot: true,
		port: serverPort,
		compress: true,
		historyApiFallback: true,
		contentBase: path.resolve(__dirname, 'devel'),
		publicPath: '/'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: [/node_modules/],
				use: [
					{
						loader: `ifdef-loader?${ifdefQuery}`
					}
				]
			}
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('development')
			}
		}),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin()
	]
}

module.exports = webpackMerge(baseConfig, develConfig)
