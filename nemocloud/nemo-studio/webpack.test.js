const path = require('path')
const webpack = require('webpack')
const ChunkManifestPlugin = require('chunk-manifest-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const InlineManifestWebpackPlugin = require('inline-manifest-webpack-plugin')
const WebpackChunkHash = require('webpack-chunk-hash')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const querystring = require('querystring')

const serverPort = 8080;

let meta = {
  username: 'test1',
  password: 'rnw3ign2eko41fkek',
  customer: 'test',
	cloudDevice: {
		number: 2
	},
}

let ifdefOpt = {
	DEBUG: false,
	SNOW_MODE: process.env.SNOW_MODE || 'cloud'
}

let ifdefQuery = querystring.encode({json: JSON.stringify(ifdefOpt)})


module.exports = {
  node: {
		//console: 'empty',
		fs: 'empty',
		net: 'empty',
		tls: 'empty'
	},
  devtool: 'source-map',
	entry: ['./src/app/app.js'],
  output: {
		path: path.resolve(__dirname, 'test'),
		filename: '[name].bundle.js',
		publicPath: '/'
	},
  devServer: {
		hot: false,
		port: serverPort,
		compress: true,
		historyApiFallback: true,
		contentBase: path.resolve(__dirname, 'test'),
		publicPath: '/'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: [/node_modules/],
				use: [
					{
						loader: 'babel-loader',
						options: {
							plugins: ['transform-runtime'],
  						presets: ['es2015']
						}
					},
          {
            loader: `ifdef-loader?${ifdefQuery}`
          }
				]
			},
			{
				test: /\.html$/,
				use: [{
					loader: 'html-loader',
					options: {
						attrs: ['img:src', 'img:ng-src']
					}
				}]
			},
      {
			 	test: /\.css$/,
				loaders: [ 'style-loader', 'to-string-loader', 'css-loader' ]
		 	},
			{
				test: /\.(png|jpg|gif)$/,
				loader: 'file-loader',
			},
			{
				test: /\.(woff|woff2|eot|ttf|svg)$/,
				use: [{
					loader: 'url-loader',
					options: {
						limit: 100000
					}
				}]
			}
		]
	},
	plugins: [
    new webpack.DefinePlugin({
			'meta': JSON.stringify(meta),
      'SNOW_MODE': process.env.SNOW_MODE? JSON.stringify(process.env.SNOW_MODE): JSON.stringify('cloud'),
			'process.env': {
				'NODE_ENV': JSON.stringify('test'),
				'SNOW_MODE': JSON.stringify(process.env.SNOW_MODE || 'cloud')
			}
		}),
    // new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin(),
		new webpack.HashedModuleIdsPlugin(),
		/* if we use chunk hash plugin, we need not to consider manifest inlining for long term caching
		 */
		new HtmlWebpackPlugin({
			template: './src/index.html',
			chunksSortMode: 'dependency'
		}),
		new CopyWebpackPlugin([{
			from: 'src/assets/',
			to: 'assets'
		}]),
		new webpack.ProvidePlugin({
			'window.jQuery': 'jquery',
			'window.$': 'jquery',
			$: 'jquery',
			jQuery: 'jquery'
		})
	]
}
