import 'babel-polyfill'
import _ from 'lodash'
import moment from 'moment'

function component() {
	let element = document.createElement('div')
	element.innerHTML = _.join(['hello', 'webpack'])

	console.log(moment().format())
	return element
}

document.body.appendChild(component())
