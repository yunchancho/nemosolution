const path = require('path')
const webpack = require('webpack')
const webpackMerge = require('webpack-merge')
const baseConfig = require('./webpack.config.js')

const serverPort = 8080

let develConfig = {
	devtool: 'source-map',
	entry: {
		main: './app/index.js'
	},
	output: {
		path: path.resolve(__dirname, 'devel'),
		filename: '[name].bundle.js',
		publicPath: '/'
	},
	devServer: {
		hot: true,
		port: serverPort,
		compress: true,
		historyApiFallback: true,
		contentBase: path.resolve(__dirname, 'devel'),
		publicPath: '/'
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin()
	]
}

module.exports = webpackMerge(baseConfig, develConfig)
