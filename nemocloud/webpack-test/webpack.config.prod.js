const path = require('path')
const webpack = require('webpack')
const webpackMerge = require('webpack-merge')
const baseConfig = require('./webpack.config.js')

let prodConfig = {
	devtool: 'cheap-module-source-map',
	entry: {
		main: './app/index.js'
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].[chunkhash].bundle.js',
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		}),
		new webpack.LoaderOptionsPlugin({
			minimize: true,
			debug: false
		}),
		/*
		 * UglifyJsPlugin doesn't work on ES6, so we need to add rules for babel
		 * this plugin execution is exclusive with source map creation
		 */
		new webpack.optimize.UglifyJsPlugin({
			beautify: false,
			mangle: {
				screw_ie8: true,
				keep_fnames: true
			},
			comments: false
		})

		/*
		 * TODO we need to extract text for css files 
		 */
	]
}

module.exports = webpackMerge(baseConfig, prodConfig)
