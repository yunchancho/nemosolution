# stand alone daemon program to broadcast sensor data using TUIO
ADD_SUBDIRECTORY(beam_daemon)

# nemo plugin to send sensor data to main thread using nemochannel/nemoqueue 
#ADD_SUBDIRECTORY(signage_realsense_plugin)

ADD_SUBDIRECTORY(karim_daemon)
