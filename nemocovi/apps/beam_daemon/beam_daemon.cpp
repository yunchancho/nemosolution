#include <vector>

#include "nemocovi.h"

#include "nemocovi.hpp"
#include "beam_daemon.hpp"

namespace nemocovi {

BeamDaemon::BeamDaemon(NemoCoviDeviceType device)
	: m_deviceType(device)
{
}

BeamDaemon::~BeamDaemon() 
{
}

bool BeamDaemon::initialize(std::string &daemonName, std::string &messageRouterAddr, int messageRouterPort) 
{
	// TODO check if device number is valid using each device Inspector class
	m_nemoCovi = NemoCovi::create(NEMOCOVI_APP_BEAM, m_deviceType, daemonName, messageRouterAddr, messageRouterPort);

	if (!m_nemoCovi) {
		return false;
	}

	if (!m_nemoCovi->initialize()) {
		return false;
	}

	return true;
}

void BeamDaemon::run()
{
	std::vector<NemoCoviClassId> classes = {
		NEMOCOVI_CLASSID_PALM_TOUCH,
		NEMOCOVI_CLASSID_HAND_SWIPE,
		NEMOCOVI_CLASSID_OBJECT_CIRCLE
	};

	while(m_nemoCovi->tryNextFrame()) {
		m_nemoCovi->sendEventsToTuioServer(NEMOCOVI_EVENT_PALM, classes);
		m_nemoCovi->sendEventsToTuioServer(NEMOCOVI_EVENT_HAND, classes);
		m_nemoCovi->sendEventsToTuioServer(NEMOCOVI_EVENT_OBJECT, classes);
	}
}

bool BeamDaemon::deinitialize()
{
	return true;
}

} // namespace nemocovi
