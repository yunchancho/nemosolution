#ifndef BEAM_DAEMON_H
#define BEAM_DAEMON_H

#include <nemocovi.h>
#include <i_nemocovi.hpp>

namespace nemocovi {

class BeamDaemon {
	public:
		//BeamBaemon(const BeamDaemon&) = delete; // set as noncopyable class

		bool initialize(std::string &daemonName, std::string &messageRouterAddr, int messageRouterPort);
		bool deinitialize();
		void run();

		explicit BeamDaemon(NemoCoviDeviceType device);
		~BeamDaemon();

  private:
		INemoCoviPtr m_nemoCovi;
		NemoCoviDeviceType m_deviceType;
};

} // namespace nemocovi

#endif// BEAM_DAEMON_H
