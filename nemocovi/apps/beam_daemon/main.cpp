#include <string>
#include <iostream>

#include <nemocovi.h>
#include "beam_daemon.hpp"

static std::string g_daemonName("nemocovid");
static std::string g_messageRouterAddr("localhost");
static int g_messageRouterPort = 3334;

int main (int argc, char *argv[])
{
	nemocovi::BeamDaemon daemon(NEMOCOVI_DEVICE_REALSENSE);

	if (!daemon.initialize(g_daemonName, g_messageRouterAddr, g_messageRouterPort)) {
#ifdef SHOW_DEBUG_MESSAGE
		std::cout << "failed to initialize daemon instance" << std::endl;
#endif
		return -1;
	}

	daemon.run();
	daemon.deinitialize();

	return 0;
}
