#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <json-c/json.h>
#include <nemocovi.h>
#include <nemocovi.hpp>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <nemokits/nemobus.h>

#define BUFF_SIZE 1024

static int idle_count = 0;
static int active_play = 0;

static const char *karim_busid = "/r200karimd";
static const char *art_busid = "/nemoart-karim";
static const char *art_mode = "repeat_all";
static const char *metadata_path = "/etc/nemocovi/karim/meta.json";

static struct nemobus *bus;

void play(char *url) {
  printf("play!\n");
  struct busmsg *msg;

  msg = nemobus_msg_create();
  nemobus_msg_set_name(msg, "clear");
  nemobus_send_msg(bus, karim_busid, art_busid, msg);
  nemobus_msg_destroy(msg);

  msg = nemobus_msg_create();
  nemobus_msg_set_name(msg, "play");
  nemobus_msg_set_attr(msg, "url", url);
  nemobus_msg_set_attr(msg, "mode", art_mode);
  nemobus_send_msg(bus, karim_busid, art_busid, msg);
  nemobus_msg_destroy(msg);
}

int play_active(struct nemocovi_object *events, int count, int active_threshold_depth, char *play_url)
{
  int result = -1; 
	for (int i = 0; i < count; i++) {
		if (events[i].depth > active_threshold_depth) {
			continue;
		} 

    play(play_url);
		printf("person object depth: %d\n", events[i].depth );

    result = 0;
	}

  return result;
}

int send_objects(struct nemocovi_object *events, int count, int stay_threshold_second)
{
	for (int i = 0; i < count; i++) {
		if (events[i].aid != NEMOCOVI_ACTIONID_LEAVE) {
			continue;
		}

		if (events[i].duration < stay_threshold_second) {
			continue;
		} 

    //TODO send this object log to fluentd
    printf("person leave: sid %d, depth: %d, duration: %lu\n", events[i].sid, events[i].depth, events[i].duration);
  }
}

struct covi_option {
	int depth_boundaries[10];
	int depth_boundaries_count;
	int tracking_distance;
	int depth_avg_range;
	int framerate;
  char *idle_video_path;
  char *active_video_path;
  int active_threshold_depth;
  int idle_enter_wait_second;
  int stay_threshold_second;
};

static void set_nemocovi_option(const char* json_path, struct covi_option *option)
{
	int i, fd;
	char buff[BUFF_SIZE];
	json_object *jobj, *tmp;

	fd = open(json_path, O_RDONLY);
	if (fd > 0) {
		printf("set meta file (.json)\n");
		read(fd, buff, BUFF_SIZE);
		jobj = json_tokener_parse(buff);

		json_object_object_foreach(jobj, key, val) {
			if (!strcmp(key, "depthBoundaries")) {
				for (i = 0; i < json_object_array_length(val); i++) {
					tmp = json_object_array_get_idx(val, i);
					option->depth_boundaries[i] = json_object_get_int(tmp); 
					printf("set depth boundaries[%d]: %d\n", i, option->depth_boundaries[i]);
				}
				option->depth_boundaries_count = json_object_array_length(val);
			} else if(!strcmp(key, "trackingMaxDistance")) {
				option->tracking_distance = json_object_get_int(val);
				printf("set tracking max distance: %d\n", option->tracking_distance);
			} else if(!strcmp(key, "personCenterRange")) {
				option->depth_avg_range = json_object_get_int(val);
				printf("set person center range: %d\n", option->depth_avg_range);
			} else if(!strcmp(key, "framerate")) {
				option->framerate = json_object_get_int(val);
				printf("set frame rate: %d\n", option->framerate);
			} else if (!strcmp(key, "idleVideoPath")) {
				option->idle_video_path = strdup(json_object_get_string(val));
				printf("set idle video path: %s\n", option->idle_video_path);
			} else if (!strcmp(key, "activeVideoPath")) {
				option->active_video_path = strdup(json_object_get_string(val));
				printf("set active video path: %s\n", option->active_video_path);
			} else if (!strcmp(key, "activeThresholdDepth")) {
				option->active_threshold_depth = json_object_get_int(val);
				printf("set active threshold depth: %d\n", option->active_threshold_depth);
			} else if (!strcmp(key, "idleEnterWaitSecond")) {
				option->idle_enter_wait_second = json_object_get_int(val);
				printf("set idle enter wait second: %d\n", option->idle_enter_wait_second);
			} else if (!strcmp(key, "stayThresholdSecond")) {
				option->stay_threshold_second = json_object_get_int(val);
				printf("set stay threshold second: %d\n", option->stay_threshold_second);
      }
		}
		close(fd);
	} 
}

int main(int argc, char *argv[])
{
	struct covi_option option;

	struct nemocovi_object *objects = NULL;
	struct nemocovi_usedclass usedclass; 
	int objects_count;

  set_nemocovi_option(metadata_path, &option);

  bus = nemobus_create();
  nemobus_connect(bus, NULL);
  nemobus_advertise(bus, "set", karim_busid);

	nemocovi_handle *handle = nemocovi_create(NEMOCOVI_APP_SIGNAGE, NEMOCOVI_DEVICE_REALSENSE);

	if (!handle) {
		printf("failed to get nemocovi handle\n");
	}

	usedclass.classes[0] = NEMOCOVI_CLASSID_OBJECT_PERSON;

	// set options
	nemocovi_set_depth_boundary(handle, NEMOCOVI_CLASSID_OBJECT_PERSON, option.depth_boundaries, option.depth_boundaries_count);
	nemocovi_set_event_tracking_distance(handle, NEMOCOVI_CLASSID_OBJECT_PERSON, option.tracking_distance);
	nemocovi_set_depth_avg_range(handle, NEMOCOVI_CLASSID_OBJECT_PERSON, option.depth_avg_range);
	nemocovi_set_frame_rate(handle, option.framerate);

	while(nemocovi_try_next_frame(handle) >= 0) {
		nemocovi_free_object_events(objects);

		objects = nemocovi_get_object_events(handle, &usedclass, &objects_count);
    if (!objects_count) {
      //printf("idle count: %d\n", idle_count);
      idle_count++;
      if (idle_count > (option.framerate * option.idle_enter_wait_second) && active_play) {

        printf("\n\nstop active play");
        play(option.idle_video_path);
        printf("start idle play\n\n");
        idle_count = 0;
        active_play = 0;
      }
      continue;
    }

    idle_count = 0;

    if (!active_play) {
      int result = play_active(objects, objects_count, option.active_threshold_depth, option.active_video_path);
      if (result == 0) {
        printf("\n\nstop idle play");
        printf("start active play\n\n");
        active_play = 1;
      }
    } else {
      send_objects(objects, objects_count, option.stay_threshold_second);
    }
	}

	return 0;
}
