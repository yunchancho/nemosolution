#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <inttypes.h>
#include <fcntl.h>
#include <json-c/json.h>
#include <nemotool/nemotool.h>
#include <nemotool/nemoqueue.h>
#include <nemotool/nemochannel.h>

#include <nemocovi.h>

//#include "person_plugin.h"

#define BUFF_SIZE 1024

struct plugin_context {
	const char *args;
	struct nemochannel *channel;
	struct nemoqueue *queue;
};

struct covi_option {
	int depth_boundaries[10];
	int depth_boundaries_count;
	int tracking_distance;
	int depth_avg_range;
	int framerate;
};


static struct plugin_context g_ctx;

void print_objects(struct nemocovi_object *events, int count)
{
	printf("------------------------------------\n");
	for (int i = 0; i < count; i++) {
		printf("OBJECT\n");
		printf("sid: %d, cid: %d, aid: %d\n", events[i].sid, events[i].cid, events[i].aid);
		printf("norm_x: %.3f(%d), norm_y: %.3f(%d)\n", events[i].norm_x, events[i].x, events[i].norm_y, events[i].y);
		printf("norm_w: %.3f(%d), norm_h: %.3f(%d)\n", events[i].norm_width,  events[i].width, events[i].norm_height,  events[i].height);
		printf("depth: %d\n", events[i].depth);

	}
	printf("------------------------------------\n\n\n");
}

static void sendEventsToNemocore(struct nemocovi_object *objects, int count, int64_t serial)
{
	struct eventone *one;

	// set specific event for start sign
	one = nemoqueue_one_create(2, 0);
	nemoqueue_one_seti(one, 0, serial);
	nemoqueue_one_seti(one, 1, 0);
	nemoqueue_enqueue_one(g_ctx.queue, one);

	if (!objects || !count) {
		nemochannel_dispatch(g_ctx.channel, serial);
		return;
	}

	for (int i = 0; i < count; i++) {
		one = nemoqueue_one_create(3, 4);
		nemoqueue_one_seti(one, 0, serial);
		nemoqueue_one_seti(one, 1, objects[i].sid);
		nemoqueue_one_seti(one, 2, objects[i].depth);
		nemoqueue_one_setf(one, 0, objects[i].norm_x);
		nemoqueue_one_setf(one, 1, objects[i].norm_y);
		nemoqueue_one_setf(one, 2, objects[i].norm_width);
		nemoqueue_one_setf(one, 3, objects[i].norm_height);
		nemoqueue_enqueue_one(g_ctx.queue, one);
	}

	nemochannel_dispatch(g_ctx.channel, serial);
}

static void set_nemocovi_option(struct covi_option *option)
{
	int i, fd;
	char buff[BUFF_SIZE];
	json_object *jobj, *tmp;

	fd = open(META_JSON_PATH, O_RDONLY);
	if (fd > 0) {
		printf("set meta file (.json)\n");
		read(fd, buff, BUFF_SIZE);
		jobj = json_tokener_parse(buff);

		json_object_object_foreach(jobj, key, val) {
			if (!strcmp(key, "depthBoundaries")) {
				for (i = 0; i < json_object_array_length(val); i++) {
					tmp = json_object_array_get_idx(val, i);
					option->depth_boundaries[i] = json_object_get_int(tmp); 
					printf("set depth boundaries[%d]: %d\n", i, option->depth_boundaries[i]);
				}
				option->depth_boundaries_count = json_object_array_length(val);
			} else if(!strcmp(key, "trackingMaxDistance")) {
				option->tracking_distance = json_object_get_int(val);
				printf("set tracking max distance: %d\n", option->tracking_distance);
			} else if(!strcmp(key, "personCenterRange")) {
				option->depth_avg_range = json_object_get_int(val);
				printf("set person center range: %d\n", option->depth_avg_range);
			} else if(!strcmp(key, "framerate")) {
				option->framerate = json_object_get_int(val);
				printf("set frame rate: %d\n", option->framerate);
			}
		}
		close(fd);
	} 
}

static void* run_nemocovi(void *userdata)
{
	struct covi_option option = {
		{300, 1000, 3500}, 3, 120, 20, 30
	};

	static uint64_t serial = 0;

	struct nemocovi_object *objects = NULL;
	struct nemocovi_usedclass usedclass; 
	int objects_count;

	set_nemocovi_option(&option);
	nemocovi_handle *handle = nemocovi_create(NEMOCOVI_APP_SIGNAGE, NEMOCOVI_DEVICE_REALSENSE);

	if (!handle) {
		printf("failed to get nemocovi handle\n");
	}

	usedclass.classes[0] = NEMOCOVI_CLASSID_OBJECT_PERSON;

	// set options
	nemocovi_set_depth_boundary(handle, NEMOCOVI_CLASSID_OBJECT_PERSON, option.depth_boundaries, option.depth_boundaries_count);
	nemocovi_set_event_tracking_distance(handle, NEMOCOVI_CLASSID_OBJECT_PERSON, option.tracking_distance);
	nemocovi_set_depth_avg_range(handle, NEMOCOVI_CLASSID_OBJECT_PERSON, option.depth_avg_range);
	nemocovi_set_frame_rate(handle, option.framerate);

	while(nemocovi_try_next_frame(handle) >= 0) {
		nemocovi_free_object_events(objects);

		objects = nemocovi_get_object_events(handle, &usedclass, &objects_count);
#ifdef SHOW_DEBUG_MESSAGE
		print_objects(objects, objects_count);
#endif

		sendEventsToNemocore(objects, objects_count, serial++);
		objects_count = 0;
	}

	return 0;
}

int nemoplugin_init(struct nemotool *tool, const char *args, struct nemochannel *chan, struct nemoqueue *queue)
{
	pthread_t tid;

	g_ctx.args = args;
	g_ctx.channel = chan;
	g_ctx.queue = queue;

	pthread_create(&tid, NULL, run_nemocovi, (void *) args);

	return 0;
}
