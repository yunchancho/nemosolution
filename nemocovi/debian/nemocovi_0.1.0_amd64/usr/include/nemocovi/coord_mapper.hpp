#ifndef COORD_MAPPER_HPP
#define COORD_MAPPER_HPP

#include <vector>
#include <opencv2/core/core.hpp>

#include "i_coord_mapper.hpp"

namespace nemocovi {

class ICalibrator;
class ISensor;

typedef std::shared_ptr<ICalibrator> ICalibratorPtr;
typedef std::shared_ptr<ISensor> ISensorPtr;

class CoordMapper : public ICoordMapper {
	public: 
		static ICoordMapperPtr create(ICalibratorPtr calibrator, ISensorPtr sensor) {
			ICoordMapperPtr mapper(new CoordMapper(calibrator, sensor));
			mapper->recalibrate();
			return mapper;
		};	

		virtual cv::Point2f normalizeByScreenCoord(cv::Point& src) override;
		virtual cv::Vec2f normalizeByScreenSize(cv::Vec2i &src) override;
		virtual void recalibrate() override;
		virtual void showTransformedScreen() override;

		~CoordMapper();

	private:
		explicit CoordMapper(ICalibratorPtr calibrator, ISensorPtr sensor);

		std::vector<cv::Point> m_screenPoints;
		cv::Matx33f m_transformMatrix;
		uint32_t m_screenWidthPixel; // pixel with in color sensor, not real screen resolution
		uint32_t m_screenHeightPixel; // pixel with in color sensor, not real screen resolution
		ICalibratorPtr m_calibrator;
		ISensorPtr m_sensor;
};

} // namespace nemocovi

#endif //COORD_MAPPER_HPP
