#ifndef I_COORD_MAPPER_HPP
#define I_COORD_MAPPER_HPP

#include <memory>
#include <opencv2/core/core.hpp>

namespace nemocovi {

class ICoordMapper {
	public:
		virtual cv::Point2f normalizeByScreenCoord(cv::Point& src) = 0;
		virtual cv::Vec2f normalizeByScreenSize(cv::Vec2i &src) = 0;
		virtual void recalibrate() = 0;
		virtual void showTransformedScreen() = 0;

		virtual ~ICoordMapper() {};
};

typedef std::shared_ptr<ICoordMapper> ICoordMapperPtr;

} // namespace nemocovi

#endif //I_COORD_MAPPER_HPP

