#ifndef I_MESSANGER_HPP
#define I_MESSANGER_HPP

#include <memory>

namespace nemocovi {

class IMessanger {
	public: 
		virtual void send(std::vector<PalmPtr> &sets) = 0;
		virtual void send(std::vector<HandPtr> &sets) = 0;
		virtual void send(std::vector<ObjectPtr> &sets) = 0;
		virtual uint32_t generateSessionId() = 0;

		virtual ~IMessanger() {};
};

typedef std::shared_ptr<IMessanger> IMessangerPtr;

} // namespace nemocovi

#endif //I_MESSANGER_HPP
