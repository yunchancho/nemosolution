#ifndef I_SENSOR_HPP
#define I_SENSOR_HPP

#include <memory>
#include <nemocovi.h>

namespace nemocovi {

enum class FrameType {
	PALM,
	HAND,
	OBJECT
};

class ISensor {
	public:
		virtual bool initialize() = 0;
		virtual NemoCoviAppType getAppType() = 0;
		virtual int getFrameRate() = 0;
		virtual bool setFrameRate(int fps) = 0;
		virtual void* getDepthRawFrame() = 0;
		virtual void* getColorRawFrame() = 0;
		virtual void* getDepthDenoisyFrame(FrameType type, int minDepth, int maxDepth) = 0;
		virtual void* getColorDenoisyFrame(FrameType type) = 0;
		virtual bool tryNextFrame() = 0;
		virtual void getDepthFrameSize(int &width, int &height) = 0;
		virtual void getColorFrameSize(int &width, int &height) = 0;
		virtual uint16_t getAvgDepthValue(uint16_t *rawFrame, int cx, int cy, int range) = 0;

		virtual ~ISensor() {};
};

typedef std::shared_ptr<ISensor> ISensorPtr;

} // namespace nemocovi

#endif // I_SENSOR_HPP
