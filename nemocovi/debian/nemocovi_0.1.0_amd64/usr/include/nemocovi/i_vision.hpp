#ifndef I_VISION_HPP
#define I_VISION_HPP

#include <memory>
#include <vector>

#include "nemocovi.hpp"

namespace nemocovi {

class IVision {
	public:
		virtual std::vector<PalmPtr> getPalms(std::vector<NemoCoviClassId> &classes) = 0;
		virtual std::vector<HandPtr> getHands(std::vector<NemoCoviClassId> &classes) = 0;
		virtual std::vector<ObjectPtr> getObjects(std::vector<NemoCoviClassId> &classes) = 0;
		virtual cv::Mat getRawImage(NemoCoviRawDataType type) = 0;
		virtual bool tryNextFrame() = 0;

		// option for detail control
		virtual bool setDepthBoundary(NemoCoviClassId classId, std::vector<int> depthes) = 0;
		virtual bool setEventTrackingDistance(NemoCoviClassId classId, int distance) = 0;
		virtual bool setDepthAvgRange(NemoCoviClassId classId, int range) = 0;

		virtual ~IVision() {};
};

typedef std::shared_ptr<IVision> IVisionPtr;

} // namespace nemocovi

#endif // I_VISION_HPP
