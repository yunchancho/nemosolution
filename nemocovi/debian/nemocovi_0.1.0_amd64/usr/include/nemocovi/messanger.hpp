#ifndef MESSANGER_HPP
#define MESSANGER_HPP

#include <memory>
#include <string>

#include "i_messanger.hpp"

static const std::string g_palmPath("/tuio/2Dcur");
static const std::string g_handPath("/tuio/2Dhand");
static const std::string g_objectPath("/tuio/2Dobject");

namespace lo {

class Address;

};

namespace nemocovi {

typedef std::shared_ptr<lo::Address> LoAddressPtr;

class Messanger : public IMessanger {
	public: 
		static IMessangerPtr create() 
		{
			return IMessangerPtr(new Messanger());
		};
		static IMessangerPtr create(std::string &appName, std::string &routerAddr, int routerPort)
		{
			return IMessangerPtr(new Messanger(appName, routerAddr, routerPort));
		};	

		void send(std::vector<PalmPtr> &sets) override;
		void send(std::vector<HandPtr> &sets) override;
		void send(std::vector<ObjectPtr> &sets) override;
		uint32_t generateSessionId() override;

		~Messanger();

	private:
		explicit Messanger();
		explicit Messanger(std::string &appName, std::string &routerAddr, int routerPort);

		std::string m_appName;
		std::string m_routerAddr;
		int m_routerPort;
		LoAddressPtr m_client;
		uint32_t m_palmFseq;
		uint32_t m_handFseq;
		uint32_t m_objectFseq;
		static uint32_t s_currentSid;
};

} // namespace nemocovi

#endif //MESSANGER_HPP
