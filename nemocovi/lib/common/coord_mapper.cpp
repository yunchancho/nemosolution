#include <cassert>
#include <memory>
#include <vector>
#include <iostream>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include "i_calibrator.hpp"
#include "i_sensor.hpp"
#include "coord_mapper.hpp"

using namespace std;

namespace nemocovi {

CoordMapper::CoordMapper(ICalibratorPtr calibrator, ISensorPtr sensor)
	: m_calibrator(calibrator)
	, m_sensor(sensor)
{
}

CoordMapper::~CoordMapper()
{
}

cv::Point2f CoordMapper::normalizeByScreenCoord(cv::Point& src)
{
	assert(m_screenWidthPixel);
	assert(m_screenHeightPixel);

	cv::Point3f dst;
	dst = m_transformMatrix * cv::Point3f(src.x, src.y, 1);

	return cv::Point2f(dst.x / m_screenWidthPixel, dst.y / m_screenHeightPixel);
}

cv::Vec2f CoordMapper::normalizeByScreenSize(cv::Vec2i &src)
{
	assert(m_screenWidthPixel);
	assert(m_screenHeightPixel);

	return cv::Vec2f((float) src[0] / m_screenWidthPixel, (float) src[1] / m_screenHeightPixel);
}


void CoordMapper::recalibrate()
{
	m_screenPoints = m_calibrator->getScreenRectPoints();
#ifdef SHOW_DEBUG_MESSAGE
	std::cout << "screen points: " << m_screenPoints << std::endl;
#endif

	if (m_screenPoints.size() != 4) {
#ifdef SHOW_DEBUG_MESSAGE
		std::cout << "failed to get screen rect points" << std::endl;
#endif
		return;
	}

	cv::Point2f srcPoints[4];
	cv::Point2f destPoints[4];

	// 1. get start point from apporx points
	int startIdx = 0;
	uint32_t minDistance = 9999999;
	for (int i = 0; i < m_screenPoints.size(); i++) {
		int distance = sqrt(m_screenPoints[i].x * m_screenPoints[i].x + m_screenPoints[i].y * m_screenPoints[i].y);
		if (distance < minDistance) {
			minDistance = distance;
			startIdx = i;
		}
	}

	uint32_t dx, dy, line1, line2, preIdx, nextIdx;
	if (startIdx == 0) {
		preIdx = m_screenPoints.size() - 1;
		nextIdx = startIdx + 1;
		srcPoints[0] = m_screenPoints[startIdx]; // top-left
		srcPoints[1] = m_screenPoints[preIdx]; // top-right
		srcPoints[2] = m_screenPoints[nextIdx]; // bottom-left
		srcPoints[3] = m_screenPoints[nextIdx + 1]; // bottom-right
	} else if (startIdx == (m_screenPoints.size() - 1)) {
		preIdx = startIdx - 1;
		nextIdx = 0;
		srcPoints[0] = m_screenPoints[startIdx]; // top-left
		srcPoints[1] = m_screenPoints[preIdx]; // top-right
		srcPoints[2] = m_screenPoints[nextIdx]; // bottom-left
		srcPoints[3] = m_screenPoints[nextIdx + 1]; // bottom-right
	} else {
		preIdx = startIdx - 1;
		nextIdx = startIdx + 1;
		srcPoints[0] = m_screenPoints[startIdx]; // top-left
		srcPoints[1] = m_screenPoints[preIdx]; // top-right
		srcPoints[2] = m_screenPoints[nextIdx]; // bottom-left
		if (startIdx == 1) {
			srcPoints[3] = m_screenPoints[nextIdx + 1]; // bottom-right
		} else if (startIdx == 2) {
			srcPoints[3] = m_screenPoints[preIdx - 1]; // bottom-right
		}
	}

	dx = m_screenPoints[startIdx].x - m_screenPoints[preIdx].x;
	dy = m_screenPoints[startIdx].y - m_screenPoints[preIdx].y;
	line1 = sqrt(dx * dx + dy * dy);

	dx = m_screenPoints[startIdx].x - m_screenPoints[nextIdx].x;
	dy = m_screenPoints[startIdx].y - m_screenPoints[nextIdx].y;
	line2 = sqrt(dx * dx + dy * dy);

	if (line1 > line2) {
		m_screenWidthPixel = line1;
		m_screenHeightPixel = line2;
	} else {
		m_screenWidthPixel = line2;
		m_screenHeightPixel = line1;
	}
#ifdef SHOW_DEBUG_MESSAGE
	cout << "start point idx: " << startIdx << ", width: " << m_screenWidthPixel << ", height: " << m_screenHeightPixel << endl;
#endif

	destPoints[0] = cv::Point(0, 0);
	destPoints[1] = cv::Point(m_screenWidthPixel, 0);
	destPoints[2] = cv::Point(0, m_screenHeightPixel);
	destPoints[3] = cv::Point(m_screenWidthPixel, m_screenHeightPixel);

	m_transformMatrix = cv::getPerspectiveTransform(srcPoints, destPoints);

#ifdef SHOW_DEBUG_MESSAGE
	cout << "transform matrix: " << m_transformMatrix << endl;

	//cv::Point3f transPoint = transformMatrix * cv::Point3f(186, 165, 1);
	for (int i = 0; i < 4; i++) {
		cv::Point3f transPoint = m_transformMatrix * cv::Point3f(srcPoints[i].x, srcPoints[i].y, 1);
		cout << srcPoints[i] << "->" << transPoint << endl;
	}
#endif
}

void CoordMapper::showTransformedScreen()
{
	int width, height;
	m_sensor->getColorFrameSize(width, height);

	uint8_t *frame = static_cast<uint8_t *>(m_sensor->getColorRawFrame());
	cv::Mat src(height, width, CV_8UC3, (void *) frame);
	cv::Mat dst(height, width, CV_8UC3);
	cv::warpPerspective(src, dst, m_transformMatrix, src.size());

	cv::imshow("Original Screen", src);
	cv::imshow("Transformed Screen", dst);
	cv::waitKey(1);
}

} // namespace nemocovi
