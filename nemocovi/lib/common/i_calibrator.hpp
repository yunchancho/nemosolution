#ifndef I_CALIBRATOR_HPP
#define I_CALIBRATOR_HPP

#include <memory>
#include <opencv2/core/core.hpp>

namespace nemocovi {

class ICalibrator {
	public:
		virtual std::vector<cv::Point> getScreenRectPoints() = 0;

		virtual ~ICalibrator() {};
};

typedef std::shared_ptr<ICalibrator> ICalibratorPtr;

} // namespace nemocovi

#endif //I_CALIBRATOR_HPP
