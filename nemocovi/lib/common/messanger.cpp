#include <string>
#include <vector>
#include <lo/lo_cpp.h>

#include "../nemocovi.hpp"

#include "messanger.hpp"

namespace nemocovi {

namespace {

uint32_t g_currentSid = 0;

}

Messanger::Messanger(std::string &appName, std::string &routerAddr, int routerPort)
	: m_appName(appName)
	, m_routerAddr(routerAddr)
	, m_routerPort(routerPort)
	, m_palmFseq(0)
	, m_handFseq(0)
	, m_objectFseq(0)
{
	try {
		m_client = LoAddressPtr(new lo::Address(lo::string_type(m_routerAddr), lo::num_string_type(m_routerPort)));
	} catch (...) {
		throw;
	}
}

Messanger::Messanger()
	: m_client(nullptr)
	, m_appName()
	, m_routerAddr()
	, m_routerPort(0)
	, m_palmFseq(0)
	, m_handFseq(0)
	, m_objectFseq(0)
{
}

Messanger::~Messanger()
{
}

void Messanger::send(std::vector<PalmPtr> &sets)
{
	if (!m_client) {
		return;
	}

	lo::Bundle bundle;

	lo::Message source, alive, fseq;
	source.add_string(m_appName);
	alive.add_string("alive");
	fseq.add_string("fseq");

	// tuio source add	
	bundle.add(g_palmPath, source);

	// tuio alive add
	for (int i = 0; i < sets.size(); i++) {
		alive.add_int32(sets[i]->sid);
	}
	bundle.add(g_palmPath, alive);

	// tuio set add
	for (int i = 0; i < sets.size(); i++) {
		lo::Message set;
		set.add_string("set");
		set.add_int32(sets[i]->sid); // session id
		set.add_int32(static_cast<int>(sets[i]->cid)); // class id
		set.add_float(sets[i]->normX); // [0-1] normalized coord
		set.add_float(sets[i]->normY); // [0-1] normalized coord
		set.add_float(0.0);
		set.add_float(0.0);
		set.add_float(0.0);
		bundle.add(g_palmPath, set);
	}

	// tuio fseq add
	fseq.add_int32(m_palmFseq++);
	bundle.add(g_palmPath, fseq);
#ifdef SHOW_DEBUG_MESSAGE
	bundle.print();
#endif

	m_client->send(bundle);
}

void Messanger::send(std::vector<HandPtr> &sets)
{
	if (!m_client) {
		return;
	}

	lo::Bundle bundle;

	lo::Message source, alive, fseq;
	source.add_string(m_appName);
	alive.add_string("alive");
	fseq.add_string("fseq");

	// tuio source add	
	bundle.add(g_handPath, source);

	// tuio alive add
	for (int i = 0; i < sets.size(); i++) {
		alive.add_int32(sets[i]->sid);
	}
	bundle.add(g_handPath, alive);

	// tuio set add
	for (int i = 0; i < sets.size(); i++) {
		lo::Message set;
		set.add_string("set");
		set.add_int32(sets[i]->sid);
		set.add_int32(static_cast<int>(sets[i]->cid)); // class id
		set.add_float(sets[i]->normX); // [0-1] normalized coord
		set.add_float(sets[i]->normY); // [0-1] normalized coord
		set.add_float(sets[i]->normX1); // [0-1] normalized coord
		set.add_float(sets[i]->normY1); // [0-1] normalized coord
		bundle.add(g_handPath, set);
	}

	// tuio fseq add
	fseq.add_int32(m_handFseq++);
	bundle.add(g_handPath, fseq);
#ifdef SHOW_DEBUG_MESSAGE
	bundle.print();
#endif

	m_client->send(bundle);
}

void Messanger::send(std::vector<ObjectPtr> &sets)
{
	if (!m_client) {
		return;
	}

	lo::Bundle bundle;

	lo::Message source, alive, fseq;
	source.add_string(m_appName);
	alive.add_string("alive");
	fseq.add_string("fseq");

	// tuio source add	
	bundle.add(g_objectPath, source);

	// tuio alive add
	for (int i = 0; i < sets.size(); i++) {
		alive.add_int32(sets[i]->sid);
	}
	bundle.add(g_objectPath, alive);

	// tuio set add
	for (int i = 0; i < sets.size(); i++) {
		lo::Message set;
		set.add_string("set");
		set.add_int32(sets[i]->sid);
		set.add_int32(static_cast<int>(sets[i]->cid)); // class id
		set.add_float(sets[i]->normX); // [0-1] normalized coord
		set.add_float(sets[i]->normY); // [0-1] normalized coord
		set.add_float(sets[i]->normWidth); // [0-1] normalized  
		set.add_float(sets[i]->normHeight); // [0-1] normalized
		set.add_float(sets[i]->normAngle); // [0-2PI] 
		bundle.add(g_objectPath, set);
	}

	// tuio fseq add
	fseq.add_int32(m_objectFseq++);
	bundle.add(g_objectPath, fseq);
#ifdef SHOW_DEBUG_MESSAGE
	bundle.print();
#endif

	m_client->send(bundle);
}

uint32_t Messanger::generateSessionId()
{
	g_currentSid++;
	return g_currentSid;
}

} // namespace nemocovi
