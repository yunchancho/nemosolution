#ifndef I_NEMOCOVI_HPP
#define I_NEMOCOVI_HPP

#include <memory>
#include <vector>
#include <opencv2/core/core.hpp>

#include "nemocovi.h"

namespace nemocovi {

struct Palm {
	uint32_t sid;
	NemoCoviClassId cid;
	NemoCoviActionId aid;
	bool isChanged;

	cv::Point coord;

	// normalized data..
	float normX;
	float normY;
};

struct Hand {
	uint32_t sid;
	NemoCoviClassId cid;
	NemoCoviActionId aid;
	bool isChanged;

	cv::Point coord;
	cv::Point coord1;

	// normalized data..
	float normX;
	float normY;
	float normX1;
	float normY1;
};

struct Object {
	uint32_t sid;
	NemoCoviClassId cid;
	NemoCoviActionId aid;
	bool isChanged;

	uint64_t startTime;
	uint64_t duration;

	cv::Point coord;
	float angle;
	uint32_t radius;
	uint32_t width;
	uint32_t height;
	uint16_t depth;

	// normalized data..
	float normX;
	float normY;
	float normWidth;
	float normHeight;
	float normDepth;
	float normAngle; // check this
};

typedef std::shared_ptr<Palm> PalmPtr;
typedef std::shared_ptr<Hand> HandPtr;
typedef std::shared_ptr<Object> ObjectPtr;


class INemoCovi {
	public:
		virtual bool initialize() = 0;
		virtual bool tryNextFrame() = 0;
		virtual bool sendEventsToTuioServer(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes) = 0;

		virtual	void getEvents(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes, std::vector<PalmPtr> &events) = 0;
		virtual	void getEvents(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes, std::vector<HandPtr> &events) = 0;
		virtual	void getEvents(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes, std::vector<ObjectPtr> &events) = 0;
		virtual cv::Mat getRawImage(NemoCoviRawDataType type) = 0;

		// options for detail control 
		virtual bool setFrameRate(int fps) = 0;
		virtual bool setDepthBoundary(NemoCoviClassId classId, std::vector<int> depthes) = 0;
 		virtual bool setEventTrackingDistance(NemoCoviClassId classId, int distance) = 0;
 		virtual bool setDepthAvgRange(NemoCoviClassId classId, int range) = 0;

		virtual ~INemoCovi() {};
};

typedef std::shared_ptr<INemoCovi> INemoCoviPtr;

} // namespace nemocovi

#endif //I_NEMOCOVI_HPP
