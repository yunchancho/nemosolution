#include <vector>
#include <iostream>

#include "nemocovi.h"

#include "realsense/realsense.hpp"
#include "realsense/realsense_vision.hpp"
#include "realsense/realsense_calibrator.hpp"

//TODO if new devices are added, include their headers here
//#include "kinect/kinect.hpp"
//#include "kinect/kinect_vision.hpp"
//#include "kinect/kinect_calibrator.hpp"

#include "coord_mapper.hpp"
#include "messanger.hpp"
#include "nemocovi.hpp"

namespace nemocovi {

NemoCovi::NemoCovi(NemoCoviAppType appType, NemoCoviDeviceType deviceType, std::string appName, std::string address, int port)
	: m_appName(appName)
	, m_deviceType(deviceType)
	, m_messageRouterAddr(address)
	, m_messageRouterPort(port)
	, m_appType(appType)
{
}

NemoCovi::NemoCovi(NemoCoviAppType appType, NemoCoviDeviceType deviceType)
	: m_appName()
	, m_deviceType(deviceType)
	, m_messageRouterAddr()
	, m_messageRouterPort()
	, m_appType(appType)
{
}

NemoCovi::~NemoCovi()
{
}

bool NemoCovi::initialize()
{
	if (m_messageRouterAddr.empty()) {
		m_messanger = Messanger::create();
	} else {
		m_messanger = Messanger::create(m_appName, m_messageRouterAddr, m_messageRouterPort);
	}

	if (!m_messanger) {
		return false;
	}

	if (m_deviceType == NEMOCOVI_DEVICE_REALSENSE) {
		m_sensor = device::RealSense::create(0, m_appType);
		if (m_sensor->initialize()) {
			m_vision = device::RealSenseVision::create(m_sensor, m_messanger);
			m_calibrator = device::RealSenseCalibrator::create(m_sensor);
		} else {
			m_vision = nullptr;
			m_calibrator = nullptr;
		}
	} else if (m_deviceType == NEMOCOVI_DEVICE_KINECT) {
		//m_sensor = Kinect::create();
		//m_vision = KinectVision::create(m_sensor, m_messanger);
		//m_calibrator = KinectCalibrator::create(m_sensor);
	} else {
		m_sensor = nullptr;
		m_vision = nullptr;
		m_calibrator = nullptr;
	}

	if (!m_sensor || !m_vision || !m_calibrator) { 
		return false;
	}

	m_mapper = CoordMapper::create(m_calibrator, m_sensor);
	if (!m_mapper) {
		return false;
	}

	return true;
}

bool NemoCovi::tryNextFrame()
{
	m_palms.clear();
	m_hands.clear();
	m_objects.clear();

	if (!m_sensor->tryNextFrame()) {
		return false;
	}

#ifdef SHOW_OPENCV_WINDOW
	//m_mapper->showTransformedScreen();
#endif

	return true;
}

bool NemoCovi::sendEventsToTuioServer(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes)
{
	if (m_messageRouterAddr.empty()) {
		return false;
	}

	// first of all, we need to set events
	setEvents(type, classes);

	if (type == NEMOCOVI_EVENT_PALM) {
		if (m_palms.size()) {
			m_messanger->send(m_palms);
		}
	} else if (type == NEMOCOVI_EVENT_HAND) {
		if (m_hands.size()) {
			m_messanger->send(m_hands);
		}
	} else if (type == NEMOCOVI_EVENT_OBJECT) {
		if (m_objects.size()) {
			m_messanger->send(m_objects);
		}
	} else {
#ifdef SHOW_DEBUG_MESSAGE
		std::cout << "Unsupported event type" << std::endl;
#endif
		return false;
	}

	return true;
}

void NemoCovi::getEvents(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes, std::vector<PalmPtr> &events)
{
	if (type != NEMOCOVI_EVENT_PALM) {
		return;
	}
	setEvents(type, classes);
	events = m_palms;
}

void NemoCovi::getEvents(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes, std::vector<HandPtr> &events)
{
	if (type != NEMOCOVI_EVENT_HAND) {
		return;
	}
	setEvents(type, classes);
	events = m_hands;
}

void NemoCovi::getEvents(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes, std::vector<ObjectPtr> &events)
{
	if (type != NEMOCOVI_EVENT_OBJECT) {
		return;
	}
	setEvents(type, classes);
	events = m_objects;
}

cv::Mat NemoCovi::getRawImage(NemoCoviRawDataType type)
{
	cv::Mat mat = m_vision->getRawImage(type);
	//std::cout << __FUNCTION__ << ": raw image ref: " << mat.u->refcount << std::endl;
	return mat;
}

bool NemoCovi::setFrameRate(int fps)
{
	if (!m_sensor->setFrameRate(fps)) {
		return false;
	}

	return true;
}

bool NemoCovi::setDepthBoundary(NemoCoviClassId classId, std::vector<int> depthes)
{
	if (!m_vision->setDepthBoundary(classId, depthes)) {
		return false;
	}

	return true;
}

bool NemoCovi::setEventTrackingDistance(NemoCoviClassId classId, int distance)
{
	if (!m_vision->setEventTrackingDistance(classId, distance)) {
		return false;
	}

	return true;
}

bool NemoCovi::setDepthAvgRange(NemoCoviClassId classId, int range)
{
	if (classId <= NEMOCOVI_CLASSID_OBJECT && classId >= NEMOCOVI_CLASSID_OBJECT_END) {
		return false;
	}

	if (!m_vision->setDepthAvgRange(classId, range)) {
		return false;
	}

	return true;
}

void NemoCovi::setEvents(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes)
{
	if ((type == NEMOCOVI_EVENT_PALM) && (m_palms.size() == 0)) {
		m_palms = m_vision->getPalms(classes);
		normalizeData(m_palms);
	} else if ((type == NEMOCOVI_EVENT_HAND) && (m_hands.size() == 0)) {
		m_hands = m_vision->getHands(classes);
		normalizeData(m_hands);
	} else if ((type == NEMOCOVI_EVENT_OBJECT) && (m_objects.size() == 0)) {
		m_objects = m_vision->getObjects(classes);
		normalizeData(m_objects);
	} else {
		// invalid type or already set events for current frame
	}
}

void NemoCovi::normalizeData(std::vector<PalmPtr> &events)
{
	for (int i = 0; i < events.size(); i++) {
		cv::Point2f coord = m_mapper->normalizeByScreenCoord(events[i]->coord);
		events[i]->normX = coord.x;
		events[i]->normY = coord.y;
	}
}

void NemoCovi::normalizeData(std::vector<HandPtr> &events)
{
	for (int i = 0; i < events.size(); i++) {
		cv::Point2f coord = m_mapper->normalizeByScreenCoord(events[i]->coord);
		cv::Point2f coord1 = m_mapper->normalizeByScreenCoord(events[i]->coord1);

		events[i]->normX = coord.x;
		events[i]->normY = coord.y;
		events[i]->normX1 = coord1.x;
		events[i]->normY1 = coord1.y;
	}
}

void NemoCovi::normalizeData(std::vector<ObjectPtr> &events)
{
	for (int i = 0; i < events.size(); i++) {
		cv::Point2f coord = m_mapper->normalizeByScreenCoord(events[i]->coord);
		events[i]->normX = coord.x;
		events[i]->normY = coord.y;

		cv::Vec2i tmp(events[i]->width, events[i]->height);
		cv::Vec2f size = m_mapper->normalizeByScreenSize(tmp);
		events[i]->normWidth = size[0];
		events[i]->normHeight = size[1];

#ifdef SHOW_DEBUG_MESSAGE
		std::cout << "sid: " << events[i]->sid << ", normalize coord(" << coord.x << ", " << coord.y << "), ";
		std::cout << "size (" << size[0] << ", " << size[1] << ")" << std::endl << std::endl;
#endif
	}
}

}
