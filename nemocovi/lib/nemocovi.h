
#ifndef NEMOCOVI_H
#define NEMOCOVI_H

#include <stdint.h>
#include <opencv/cv.h>

#ifndef EXPORT_API
#define EXPORT_API	__attribute__((visibility("default")))
#endif

typedef enum {
	NEMOCOVI_APP_BEAM = 0, // type with bottom side on which beam projects
	NEMOCOVI_APP_SIGNAGE
} NemoCoviAppType;

typedef enum {
	NEMOCOVI_DEVICE_REALSENSE = 0, // r200, sr300, f200
	NEMOCOVI_DEVICE_KINECT
} NemoCoviDeviceType;

typedef enum {
	NEMOCOVI_RAWDATA_DEPTH = 0,
	NEMOCOVI_RAWDATA_COLOR,
} NemoCoviRawDataType;

typedef enum {
	NEMOCOVI_EVENT_PALM = 0,
	NEMOCOVI_EVENT_HAND,
	NEMOCOVI_EVENT_OBJECT,
} NemoCoviEventType;

typedef enum {
	NEMOCOVI_CLASSID_PALM = 100,
	NEMOCOVI_CLASSID_PALM_TOUCH,
	NEMOCOVI_CLASSID_PALM_END,
	NEMOCOVI_CLASSID_HAND = 200,
	NEMOCOVI_CLASSID_HAND_SWIPE,
	NEMOCOVI_CLASSID_HAND_GRAB,
	NEMOCOVI_CLASSID_HAND_END,
	NEMOCOVI_CLASSID_OBJECT = 300,
	NEMOCOVI_CLASSID_OBJECT_CIRCLE,
	NEMOCOVI_CLASSID_OBJECT_RECTANGLE,
	NEMOCOVI_CLASSID_OBJECT_PENTAGON,
	NEMOCOVI_CLASSID_OBJECT_HEXAGON,
	NEMOCOVI_CLASSID_OBJECT_TRIANGLE,
	NEMOCOVI_CLASSID_OBJECT_PERSON,
	NEMOCOVI_CLASSID_OBJECT_END,
	NEMOCOVI_CLASSID_END
} NemoCoviClassId;

typedef enum {
	NEMOCOVI_ACTIONID_NONE = 0,
	NEMOCOVI_ACTIONID_DOWN,
	NEMOCOVI_ACTIONID_UP,
	NEMOCOVI_ACTIONID_ENTER,
	NEMOCOVI_ACTIONID_LEAVE,
	NEMOCOVI_ACTIONID_MOTION
} NemoCoviActionId;

struct nemocovi_palm {
	uint32_t sid;
	NemoCoviClassId cid;
	NemoCoviActionId aid;

	// normalized data..
	float norm_x;
	float norm_y;
};

struct nemocovi_hand {
	uint32_t sid;
	NemoCoviClassId cid;
	NemoCoviActionId aid;

	// normalized data..
	float norm_x;
	float norm_y;
	float norm_x1;
	float norm_y1;
};

struct nemocovi_object {
	uint32_t sid;
	NemoCoviClassId cid;
	NemoCoviActionId aid;

	int depth;

	int x, y;
	int width, height;

	uint64_t start_time, duration;

	// normalized data..
	float norm_x;
	float norm_y;
	float norm_width;
	float norm_height;
	float norm_angle; // check this
};

struct nemocovi_usedclass {
	NemoCoviClassId classes[20];
};

typedef void nemocovi_handle;

#ifdef __cplusplus
extern "C" {
#endif

EXPORT_API nemocovi_handle* nemocovi_create_with_tuio(NemoCoviAppType app, NemoCoviDeviceType type, const char *app_name, const char *tuio_address, int tuio_port);
EXPORT_API nemocovi_handle* nemocovi_create(NemoCoviAppType app, NemoCoviDeviceType type);
EXPORT_API int nemocovi_try_next_frame(nemocovi_handle *handle);

// options
EXPORT_API int nemocovi_set_frame_rate(nemocovi_handle *handle, int fps);
EXPORT_API int nemocovi_set_depth_boundary(nemocovi_handle *handle, NemoCoviClassId classId, int depthes[], int count);
EXPORT_API int nemocovi_set_event_tracking_distance(nemocovi_handle *handle, NemoCoviClassId classId, int distance);
EXPORT_API int nemocovi_set_depth_avg_range(nemocovi_handle *handle, NemoCoviClassId classId, int range);


EXPORT_API int nemocovi_send_palm_events_to_tuio_server(nemocovi_handle *handle, struct nemocovi_usedclass *usedclass);
EXPORT_API int nemocovi_send_hand_events_to_tuio_server(nemocovi_handle *handle, struct nemocovi_usedclass *usedclass);
EXPORT_API int nemocovi_send_object_events_to_tuio_server(nemocovi_handle *handle, struct nemocovi_usedclass *usedclass);

EXPORT_API struct nemocovi_palm* nemocovi_get_palm_events(nemocovi_handle *handle, struct nemocovi_usedclass *usedclass, int *count);
EXPORT_API struct nemocovi_hand* nemocovi_get_hand_events(nemocovi_handle *handle, struct nemocovi_usedclass *usedclass, int *count);
EXPORT_API struct nemocovi_object* nemocovi_get_object_events(nemocovi_handle *handle, struct nemocovi_usedclass *usedclass, int *count);

EXPORT_API void nemocovi_free_palm_events(struct nemocovi_palm *events);
EXPORT_API void nemocovi_free_hand_events(struct nemocovi_hand *events);
EXPORT_API void nemocovi_free_object_events(struct nemocovi_object *events);

EXPORT_API CvMat nemocovi_get_opencv_matrix(nemocovi_handle *handle, NemoCoviRawDataType type);
EXPORT_API void nemocovi_free_opencv_matrix(CvMat **mat);

EXPORT_API void nemocovi_destroy(nemocovi_handle *handle);

#ifdef __cplusplus
}
#endif

#endif //NEMOCOVI_H
