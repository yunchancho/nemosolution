#ifndef NEMOCOVI_HPP
#define NEMOCOVI_HPP

#include <string>
#include <memory>
#include <opencv2/core/core.hpp>

#include "nemocovi.h"
#include "i_nemocovi.hpp"

namespace nemocovi {

//forward declaration
class IVision;
class ISensor;
class ICoordMapper;
class ICalibrator;
class IMessanger;

typedef std::shared_ptr<IVision> IVisionPtr;
typedef std::shared_ptr<ISensor> ISensorPtr;
typedef std::shared_ptr<ICoordMapper> ICoordMapperPtr;
typedef std::shared_ptr<ICalibrator> ICalibratorPtr;
typedef std::shared_ptr<IMessanger> IMessangerPtr;

class NemoCovi : public INemoCovi {
	public:
		static INemoCoviPtr create(NemoCoviAppType app, NemoCoviDeviceType device, std::string appName, std::string address, int port) {
			return INemoCoviPtr(new NemoCovi(app, device, appName, address, port));
		};	

		static INemoCoviPtr create(NemoCoviAppType app, NemoCoviDeviceType device) {
			return INemoCoviPtr(new NemoCovi(app, device));
		};	

		bool initialize() override;
		bool tryNextFrame() override;
		bool sendEventsToTuioServer(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes) override;

		void getEvents(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes, std::vector<PalmPtr> &events) override;
		void getEvents(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes, std::vector<HandPtr> &events) override;
		void getEvents(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes, std::vector<ObjectPtr> &events) override;
		cv::Mat getRawImage(NemoCoviRawDataType type) override;

		bool setFrameRate(int fps) override;
		bool setDepthBoundary(NemoCoviClassId classId, std::vector<int> depthes) override;
 		bool setEventTrackingDistance(NemoCoviClassId classId, int distance) override;
 		bool setDepthAvgRange(NemoCoviClassId classId, int range) override;

		~NemoCovi();

	private:
		void setEvents(NemoCoviEventType type, std::vector<NemoCoviClassId> &classes);

		void normalizeData(std::vector<PalmPtr> &events);
		void normalizeData(std::vector<HandPtr> &events);
		void normalizeData(std::vector<ObjectPtr> &events);

		explicit NemoCovi(NemoCoviAppType app, NemoCoviDeviceType device, std::string appName, std::string address, int port);
		explicit NemoCovi(NemoCoviAppType app, NemoCoviDeviceType device);

		std::string m_appName;
		NemoCoviAppType m_appType;
		std::string m_messageRouterAddr;
		int m_messageRouterPort;
		NemoCoviDeviceType m_deviceType;
		ISensorPtr m_sensor;
		IVisionPtr m_vision;
		ICoordMapperPtr m_mapper;
		ICalibratorPtr m_calibrator;
		IMessangerPtr m_messanger;

		std::vector<PalmPtr> m_palms;
		std::vector<HandPtr> m_hands;
		std::vector<ObjectPtr> m_objects;
};

static cv::Mat getOpenCvMatrix(nemocovi_handle *handle, NemoCoviRawDataType type) 
{
	CvMat tmp = nemocovi_get_opencv_matrix(handle, NEMOCOVI_RAWDATA_COLOR);
	return cv::cvarrToMat(&tmp);
}

} // namespace nemocovi

#endif //NEMOCOVI_HPP
