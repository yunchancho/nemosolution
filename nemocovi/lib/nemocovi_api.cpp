#include <vector>

#include "nemocovi.h"

#include "i_nemocovi.hpp"
#include "nemocovi.hpp"

using namespace nemocovi;

namespace {

std::vector<INemoCoviPtr> g_handles;

static INemoCoviPtr nemocovi_find_core_handle(nemocovi_handle *handle)
{
	INemoCoviPtr found = nullptr;
	for (auto it = g_handles.begin(); it != g_handles.end(); it++) {
		if ((*it).get() == handle) {
			found = (*it);
			break;
		}
	}

	return found;
}

static int nemocovi_send_events_to_tuio_server(nemocovi_handle *handle, NemoCoviEventType type, struct nemocovi_usedclass *usedclass)
{
	INemoCoviPtr found = nemocovi_find_core_handle(handle);
	if (!found) {
		return -1;
	}

	if (!usedclass) {
		return -1;
	}

	std::vector<NemoCoviClassId> classes;
	for (int i = 0; i < 10; i++) {
		classes.push_back(usedclass->classes[i]);
	}

	if (classes.size() == 0) {
		return -1;
	}

	if (!found->sendEventsToTuioServer(type, classes)) {
		return -1;
	}

	return 0;
}

} // namespace 

nemocovi_handle* nemocovi_create_with_tuio(NemoCoviAppType app, NemoCoviDeviceType device, const char *app_name, const char *tuio_address, int tuio_port)
{
	INemoCoviPtr handle = NemoCovi::create(app, device, app_name, tuio_address, tuio_port);
	if (!handle) {
		return NULL;
	}

	if (!handle->initialize()) {
		return NULL;
	}

	g_handles.push_back(handle);

	// return handle's pointer
	return static_cast<nemocovi_handle *>(handle.get());
}

nemocovi_handle* nemocovi_create(NemoCoviAppType app, NemoCoviDeviceType device)
{
	INemoCoviPtr handle = NemoCovi::create(app, device);
	if (!handle) {
		return NULL;
	}

	if (!handle->initialize()) {
		return NULL;
	}

	g_handles.push_back(handle);

	// return handle's pointer
	return static_cast<nemocovi_handle *>(handle.get());
}

int nemocovi_try_next_frame(nemocovi_handle *handle)
{
	INemoCoviPtr found = nemocovi_find_core_handle(handle);
	if (!found) {
		return -1;
	}

	if (found->tryNextFrame() == false) {
		return -1;
	}

	return 0;
}

int nemocovi_set_frame_rate(nemocovi_handle *handle, int fps)
{
	INemoCoviPtr found = nemocovi_find_core_handle(handle);
	if (!found) {
		return -1;
	}

	if (!found->setFrameRate(fps)) {
		return -1;
	}

	return 0;
}

int nemocovi_set_depth_boundary(nemocovi_handle *handle, NemoCoviClassId classId, int depthes[], int count)
{
	INemoCoviPtr found = nemocovi_find_core_handle(handle);
	if (!found) {
		return -1;
	}
	
	std::vector<int> boundaries;
	for (int i = 0; i < count; i++) {
		boundaries.push_back(depthes[i]);
	}

	if (!found->setDepthBoundary(classId, boundaries)) {
		return -1;
	}

	return 0;
}

int nemocovi_set_event_tracking_distance(nemocovi_handle *handle, NemoCoviClassId classId, int distance)
{
	INemoCoviPtr found = nemocovi_find_core_handle(handle);
	if (!found) {
		return -1;
	}
	
	if (!found->setEventTrackingDistance(classId, distance)) {
		return -1;
	}

	return 0;
}

int nemocovi_set_depth_avg_range(nemocovi_handle *handle, NemoCoviClassId classId, int range)
{
	INemoCoviPtr found = nemocovi_find_core_handle(handle);
	if (!found) {
		return -1;
	}
	
	if (!found->setDepthAvgRange(classId, range)) {
		return -1;
	}

	return 0;
}

int nemocovi_send_palm_events_to_tuio_server(nemocovi_handle *handle, struct nemocovi_usedclass *usedclass)
{
	return nemocovi_send_events_to_tuio_server(handle, NEMOCOVI_EVENT_PALM, usedclass);
}

int nemocovi_send_hand_events_to_tuio_server(nemocovi_handle *handle, struct nemocovi_usedclass *usedclass)
{
	return nemocovi_send_events_to_tuio_server(handle, NEMOCOVI_EVENT_HAND, usedclass);
}

int nemocovi_send_object_events_to_tuio_server(nemocovi_handle *handle, struct nemocovi_usedclass *usedclass)
{
	return nemocovi_send_events_to_tuio_server(handle, NEMOCOVI_EVENT_OBJECT, usedclass);
}

struct nemocovi_palm* nemocovi_get_palm_events(nemocovi_handle *handle, struct nemocovi_usedclass *usedclass, int *count)
{
	INemoCoviPtr found = nemocovi_find_core_handle(handle);
	if (!found) {
		return NULL;
	}

	std::vector<PalmPtr> events;
	std::vector<NemoCoviClassId> classes;

	// check valid event classes are requested
	for (int i = 0; i < 10; i++) {
		if ((usedclass->classes[i] > NEMOCOVI_CLASSID_PALM) &&
			 	(usedclass->classes[i] < NEMOCOVI_CLASSID_PALM_END))
	 	{
			classes.push_back(usedclass->classes[i]);
		}
	}

	if (classes.size() == 0) {
		return NULL;
	}

	found->getEvents(NEMOCOVI_EVENT_PALM, classes, events);

	// filter changed events from all events
	/* TODO we should provide interface to get only changed events to user
	for (auto it = events.begin(); it != events.end();) {
		if ((*it)->isChanged == false) {
			it = events.erase(it);
		} else {
			it++;
		}
	}
	*/

	int valid_count = *count = events.size();
	if (valid_count == 0) {
		return NULL;
	}

	struct nemocovi_palm *palms = (struct nemocovi_palm *) malloc(sizeof(struct nemocovi_palm) * valid_count);
	for (int i = 0; i < valid_count; i++) {
		palms[i].sid = events[i]->sid;
		palms[i].cid = events[i]->cid;
		palms[i].aid = events[i]->aid;
		palms[i].aid = events[i]->aid;
		palms[i].norm_x = events[i]->normX;
		palms[i].norm_y = events[i]->normY;
	}

	return palms;
}

struct nemocovi_hand* nemocovi_get_hand_events(nemocovi_handle *handle, struct nemocovi_usedclass *usedclass, int *count)
{
	INemoCoviPtr found = nemocovi_find_core_handle(handle);
	if (!found) {
		return NULL;
	}

	std::vector<HandPtr> events;
	std::vector<NemoCoviClassId> classes;

	// check valid event classes are requested
	for (int i = 0; i < 10; i++) {
		if ((usedclass->classes[i] > NEMOCOVI_CLASSID_HAND) &&
			 	(usedclass->classes[i] < NEMOCOVI_CLASSID_HAND_END))
	 	{
			classes.push_back(usedclass->classes[i]);
		}
	}

	if (classes.size() == 0) {
		return NULL;
	}
	found->getEvents(NEMOCOVI_EVENT_HAND, classes, events);

	// filter changed events from all events
	/* TODO we should provide interface to get only changed events to user
	for (auto it = events.begin(); it != events.end();) {
		if ((*it)->isChanged == false) {
			it = events.erase(it);
		} else {
			it++;
		}
	}
	*/

	int valid_count = *count = events.size();
	if (valid_count == 0) {
		return NULL;
	}

	nemocovi_hand *hands = (struct nemocovi_hand *) malloc(sizeof(struct nemocovi_hand) * valid_count);
	for (int i = 0; i < valid_count; i++) {
		hands[i].sid = events[i]->sid;
		hands[i].cid = events[i]->cid;
		hands[i].aid = events[i]->aid;
		hands[i].aid = events[i]->aid;
		hands[i].norm_x = events[i]->normX;
		hands[i].norm_y = events[i]->normY;
		hands[i].norm_x1 = events[i]->normX1;
		hands[i].norm_y1 = events[i]->normY1;
	}

	return hands;
}

struct nemocovi_object* nemocovi_get_object_events(nemocovi_handle *handle, struct nemocovi_usedclass *usedclass, int *count)
{
	INemoCoviPtr found = nemocovi_find_core_handle(handle);
	if (!found) {
		return NULL;
	}

	std::vector<ObjectPtr> events;
	std::vector<NemoCoviClassId> classes;

	// check valid event classes are requested
	for (int i = 0; i < 10; i++) {
		if ((usedclass->classes[i] > NEMOCOVI_CLASSID_OBJECT) &&
			 	(usedclass->classes[i] < NEMOCOVI_CLASSID_OBJECT_END))
	 	{
			classes.push_back(usedclass->classes[i]);
		}
	}

	if (classes.size() == 0) {
		return NULL;
	}

	found->getEvents(NEMOCOVI_EVENT_OBJECT, classes, events);

	// filter changed events from all events
	/* TODO we should provide interface to get only changed events to user
	for (auto it = events.begin(); it != events.end();) {
		if ((*it)->isChanged == false) {
			it = events.erase(it);
		} else {
			it++;
		}
	}
	*/

	int valid_count = *count = events.size();
	if (valid_count == 0) {
		return NULL;
	}

	nemocovi_object *objects = (struct nemocovi_object *) malloc(sizeof(struct nemocovi_object) * valid_count);
	for (int i = 0; i < valid_count; i++) {
		objects[i].sid = events[i]->sid;
		objects[i].cid = events[i]->cid;
		objects[i].aid = events[i]->aid;
		objects[i].aid = events[i]->aid;
		objects[i].depth = events[i]->depth;
		objects[i].norm_x = events[i]->normX;
		objects[i].norm_y = events[i]->normY;
		objects[i].norm_width = events[i]->normWidth;
		objects[i].norm_height = events[i]->normHeight;
		objects[i].norm_angle = events[i]->normAngle;


		objects[i].width = events[i]->width;
		objects[i].height = events[i]->height;
		objects[i].x = events[i]->coord.x;
		objects[i].y = events[i]->coord.y;
		objects[i].start_time = events[i]->startTime;
		objects[i].duration = events[i]->duration;
	}

	return objects;
}

void nemocovi_free_palm_events(struct nemocovi_palm *events)
{
	if (!events) {
		return;
	}

	free(events);
}

void nemocovi_free_hand_events(struct nemocovi_hand *events)
{
	if (!events) {
		return;
	}

	free(events);
}

void nemocovi_free_object_events(struct nemocovi_object *events)
{
	if (!events) {
		return;
	}

	free(events);
}

CvMat nemocovi_get_opencv_matrix(nemocovi_handle *handle, NemoCoviRawDataType type)
{
	INemoCoviPtr found = nemocovi_find_core_handle(handle);
	if (!found) {
		return cvMat(0, 0, 0, NULL);
	}

	return found->getRawImage(type);
}

void nemocovi_free_opencv_matrix(CvMat **mat)
{
}

void nemocovi_destroy(nemocovi_handle *handle)
{
	if (handle) {
		return;
	}

	for (auto it = g_handles.begin(); it != g_handles.end(); it++) {
		if ((*it).get() == handle) {
			g_handles.erase(it);
			break;
		}
	}
}
