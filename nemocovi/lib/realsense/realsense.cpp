#include <cassert>
#include <memory>
#include <iostream>

#include <librealsense/rs.hpp>
#include <nemocovi.h>

#include "realsense_inspector.hpp"
#include "realsense.hpp"

static int g_default_fps = 30;

using namespace std;

namespace nemocovi {
namespace device {

RealSense::RealSense(int devIndex, NemoCoviAppType type)
	: m_devIndex(devIndex)
	, m_fps(g_default_fps)
	, m_depthDenoisyPalmFrame(NULL)
	, m_depthDenoisyHandFrame(NULL)
	, m_depthDenoisyObjectFrame(NULL)
	, m_colorDenoisyObjectFrame(NULL)
	, m_appType(type)
{
	try {
		m_handle = RealSenseInspector::Instance()->createHandle(m_context, m_devIndex);

		if (!m_handle) {
			// TODO throw exception for constructor creation failure
		}

	} catch (...) {
		throw;
	}
}

RealSense::~RealSense()
{
	RealSenseInspector::Instance()->removeHandle(m_devIndex);
}

bool RealSense::initialize()
{
	rs::log_to_console(rs::log_severity::warn);
#ifdef SHOW_DEBUG_MESSAGE
	printf("\nUsing device 0, an %s\n", m_handle->get_name());
	printf("    Serial number: %s\n", m_handle->get_serial());
	printf("    Firmware version: %s\n", m_handle->get_firmware_version());
#endif

	rs::apply_depth_control_preset(m_handle, 1);

	m_handle->set_option(rs::option::r200_lr_gain, 100);
	m_handle->set_option(rs::option::r200_lr_exposure, 164);

	m_handle->set_option(rs::option::r200_emitter_enabled, 1);

	m_handle->set_option(rs::option::r200_depth_control_estimate_median_decrement, 0);
	m_handle->set_option(rs::option::r200_depth_control_estimate_median_increment, 0);
	m_handle->set_option(rs::option::r200_depth_control_median_threshold, 0);

	m_handle->set_option(rs::option::r200_depth_control_score_minimum_threshold, 0);
	m_handle->set_option(rs::option::r200_depth_control_score_maximum_threshold, 1023);
	m_handle->set_option(rs::option::r200_depth_control_texture_count_threshold, 0);
	m_handle->set_option(rs::option::r200_depth_control_texture_difference_threshold, 0);
	m_handle->set_option(rs::option::r200_depth_control_second_peak_threshold, 0);
	m_handle->set_option(rs::option::r200_depth_control_neighbor_threshold, 0);
	m_handle->set_option(rs::option::r200_depth_control_lr_threshold, 2047);

	m_handle->enable_stream(rs::stream::depth, 480, 360, rs::format::z16, g_default_fps);
	m_handle->enable_stream(rs::stream::color, 640, 480, rs::format::rgb8, g_default_fps);
	//m_handle->enable_stream(rs::stream::depth, rs::preset::best_quality);
	//m_handle->enable_stream(rs::stream::color, rs::preset::best_quality);

	m_handle->start();

	for (int i = 0; i < 30; ++i) {
		m_handle->wait_for_frames();
	}

	m_depthIntrin = m_handle->get_stream_intrinsics(rs::stream::depth_aligned_to_color);
	m_colorIntrin = m_handle->get_stream_intrinsics(rs::stream::color);

	if (m_appType == NEMOCOVI_APP_BEAM) {
		m_depthBaselineFrame = new uint16_t[m_depthIntrin.width * m_depthIntrin.height];
		setDepthBaselineFrame();
	}

	return true;
}

bool RealSense::tryNextFrame()
{
	assert(m_handle);

	try {
		for (int i = 0; i < (g_default_fps / m_fps); i++) {
			m_handle->wait_for_frames();
		}

		m_depthRawFrame = (uint16_t *) m_handle->get_frame_data(rs::stream::depth_aligned_to_color);
		m_colorRawFrame = (uint8_t *) m_handle->get_frame_data(rs::stream::color);

		// free existing denoisy frames
		delete m_depthDenoisyPalmFrame;
		delete m_depthDenoisyHandFrame;
		delete m_depthDenoisyObjectFrame;
		delete m_colorDenoisyObjectFrame;

		for (auto tmpDepth : m_tmpDepthFrames) {
			delete tmpDepth;
		}
		m_tmpDepthFrames.clear();

		m_depthDenoisyPalmFrame = m_depthDenoisyHandFrame = m_depthDenoisyObjectFrame = NULL;
	 	m_colorDenoisyObjectFrame = NULL;

		return true;
	} catch (...) {
		return false;
	}
}

bool RealSense::setFrameRate(int fps)
{
	if (fps > g_default_fps || fps < 1 ) {
		return false;
	}

	m_fps = fps;
	return true;
}

void RealSense::getDepthFrameSize(int &width, int &height)
{
	width = m_depthIntrin.width;
	height = m_depthIntrin.height;
}

void RealSense::getColorFrameSize(int &width, int &height)
{
	width = m_colorIntrin.width;
	height = m_colorIntrin.height;
}

void* RealSense::getDepthRawFrame()
{
	return static_cast<void *>(m_depthRawFrame);
}


void* RealSense::getColorRawFrame()
{
	return static_cast<void *>(m_colorRawFrame);
}

void RealSense::setDepthBaselineFrame()
{
	uint16_t *baselines[g_baselineFrameCount];
	uint16_t *frame = (uint16_t *) m_handle->get_frame_data(rs::stream::depth_aligned_to_color);

	m_baselineAvg = getAvgDepthValue(frame, m_depthIntrin.width / 2, m_depthIntrin.height / 2, 10);

	for (int i = 0; i < g_baselineFrameCount; ++i) {
		m_handle->wait_for_frames();
		baselines[i] = (uint16_t *) m_handle->get_frame_data(rs::stream::depth_aligned_to_color);
	}

	for(int dy = 0; dy < m_depthIntrin.height; dy++) {
		for(int dx = 0; dx < m_depthIntrin.width; dx++) {
			uint16_t min = 65535;
			std::ostringstream oss;
			oss << "min: " << min;
			for (int i = 0; i < g_baselineFrameCount; ++i) {
				if (baselines[i][m_depthIntrin.width * dy + dx] > 0) {
					if (baselines[i][m_depthIntrin.width * dy + dx] < min) {
						min = baselines[i][m_depthIntrin.width * dy + dx];
						oss << " -> " << min;
					}
				}
			}

			if (min > m_baselineAvg + 10) {
				min = m_baselineAvg;
				oss << " -> " << min << " (above bottom case)";
			}

			m_depthBaselineFrame[dy * m_depthIntrin.width + dx] = min;
#ifdef SHOW_DEBUG_MESSAGE
			//std::cout << "dx: " << dx << ", dy: " << dy << ", depth: " << oss.str() << endl;
#endif
		}
	}

#ifdef SHOW_DEBUG_MESSAGE
	std::cout << "realsense height: " << m_baselineAvg << endl;
#endif
}


uint16_t RealSense::getAvgDepthValue(uint16_t *rawFrame, int cx, int cy, int range)
{
	uint16_t avg = 0;
	uint32_t sum = 0, count = 0;
	int size = range / 2;

	for (int dy = cy - size; dy < cy + size; dy++) {
		for (int dx = cx - size; dx < cx + size; dx++) {
			if (rawFrame[dy * m_depthIntrin.width + dx]) {
				uint16_t depthValue = rawFrame[dy * m_depthIntrin.width + dx];
				sum += depthValue;
				count++;
			}
		}
	}

	if (count) {
		avg = sum / count;
	}

#ifdef SHOW_DEBUG_MESSAGE
	std::cout << "bottom depth average: " << avg << ", count: " << count << endl;;
#endif
	
	return avg;
}

void* RealSense::getDepthDenoisyFrame(FrameType type, int minDepth, int maxDepth)
{
	uint16_t *frame;

	if (type == FrameType::PALM) {
		if (!m_depthDenoisyPalmFrame) {
			m_depthDenoisyPalmFrame = denoiseDepthRawPalmFrame(m_depthRawFrame);
		}
		frame = m_depthDenoisyPalmFrame;
	} else if (type == FrameType::HAND) {
		if (!m_depthDenoisyHandFrame) {
			m_depthDenoisyHandFrame = denoiseDepthRawHandFrame(m_depthRawFrame);
		}
		frame = m_depthDenoisyHandFrame;
	} else if (type == FrameType::OBJECT)  {

		if (minDepth == 0 || maxDepth == 0) {
			if (!m_depthDenoisyObjectFrame) {
					m_depthDenoisyObjectFrame = denoiseDepthRawObjectFrame(m_depthRawFrame);
			}
			frame = m_depthDenoisyObjectFrame;
		} else {
			frame = denoiseDepthRawObjectFrame(m_depthRawFrame, minDepth, maxDepth);
			m_tmpDepthFrames.push_back(frame);
		}
	} else {
		frame = NULL;
	}

	return static_cast<void *>(frame);
}

void* RealSense::getColorDenoisyFrame(FrameType type)
{
	uint8_t *frame;

	if (type == FrameType::OBJECT) {
		if (!m_colorDenoisyObjectFrame) {
			m_colorDenoisyObjectFrame = denoiseColorRawObjectFrame(m_colorRawFrame);
		}
		frame = m_colorDenoisyObjectFrame;
	} else {
		frame = NULL;
	}

	return static_cast<void *>(frame);
}

uint16_t* RealSense::denoiseDepthRawPalmFrame(uint16_t* rawFrame)
{
	assert(rawFrame);

	std::size_t memsize = m_depthIntrin.height * m_depthIntrin.width * sizeof(uint16_t);

	uint16_t *denoisyFrame = (uint16_t *) malloc(memsize);
	std::memcpy(denoisyFrame, rawFrame, memsize);

	for(int dy = 0; dy < m_depthIntrin.height; dy++) {
		for(int dx = 0; dx < m_depthIntrin.width; dx++) {
			if (m_depthBaselineFrame[dy * m_depthIntrin.width + dx] - denoisyFrame[dy * m_depthIntrin.width + dx] - g_depthDeviate * 2 <= 0) {
				denoisyFrame[dy * m_depthIntrin.width + dx] = 0x0000;
			} else if (m_depthBaselineFrame[dy * m_depthIntrin.width + dx] - denoisyFrame[dy * m_depthIntrin.width + dx] - g_depthDeviate * 4 <= 0)  { 
				//denoisyFrame[dy * m_depthIntrin.width + dx] = 0x0000;
			} else {
				denoisyFrame[dy * m_depthIntrin.width + dx] = 0x0000;
			}
		}
	}

	return denoisyFrame;
}

uint16_t* RealSense::denoiseDepthRawHandFrame(uint16_t* rawFrame)
{
	assert(rawFrame);

	std::size_t memsize = m_depthIntrin.height * m_depthIntrin.width * sizeof(uint16_t);

	uint16_t *denoisyFrame = (uint16_t *) malloc(memsize);
	std::memcpy(denoisyFrame, rawFrame, memsize);

	for(int dy = 0; dy < m_depthIntrin.height; dy++) {
		for(int dx = 0; dx < m_depthIntrin.width; dx++) {
			if (m_depthBaselineFrame[dy * m_depthIntrin.width + dx] - denoisyFrame[dy * m_depthIntrin.width + dx] - g_handBottomDepth <= 0) {
				denoisyFrame[dy * m_depthIntrin.width + dx] = 0x0000;
			}
		}
	}

	return denoisyFrame;
}

uint16_t* RealSense::denoiseDepthRawObjectFrame(uint16_t* rawFrame, int minDepth, int maxDepth)
{
	assert(rawFrame);

	std::size_t memsize = m_depthIntrin.height * m_depthIntrin.width * sizeof(uint16_t);

	uint16_t *denoisyFrame = (uint16_t *) malloc(memsize);
	std::memcpy(denoisyFrame, rawFrame, memsize);

	// if needed, do something to denoise 
	for(int dy = 0; dy < m_depthIntrin.height; dy++) {
		for(int dx = 0; dx < m_depthIntrin.width; dx++) {
			if (denoisyFrame[dy * m_depthIntrin.width + dx] > maxDepth) {
				denoisyFrame[dy * m_depthIntrin.width + dx] = 0;
			}
			
			if (denoisyFrame[dy * m_depthIntrin.width + dx] < minDepth) {
				denoisyFrame[dy * m_depthIntrin.width + dx] = 0;
			}
		}
	}

	return denoisyFrame;
}

uint8_t* RealSense::denoiseColorRawObjectFrame(uint8_t* rawFrame)
{
	assert(rawFrame);

	rs::intrinsics m_colorIntrin = m_handle->get_stream_intrinsics(rs::stream::color);
	std::size_t memsize = m_colorIntrin.height * m_colorIntrin.width * sizeof(uint8_t) * 3;

	uint8_t *denoisyFrame = (uint8_t *) malloc(memsize);
	std::memcpy(denoisyFrame, rawFrame, memsize);

	for(int dy = 0; dy < m_colorIntrin.height; dy++) {
		for(int dx = 0; dx < m_colorIntrin.width; dx++) {
			// if we need to handle, write here
		}
	}

	return denoisyFrame;
}

} // namespace device
} // namespace nemocovi
