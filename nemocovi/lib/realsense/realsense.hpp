#ifndef REALSENSE_HPP
#define REALSENSE_HPP

#include <librealsense/rs.hpp>

#include <nemocovi.h>
#include <i_sensor.hpp>
#include "realsense_inspector.hpp"

static int g_depthDeviate = 5; // millimeter
static int g_handBottomDepth = 200; // millimeter
static int g_baselineFrameCount = 10;
static int g_defaultDepthAverage = 1003; // millimeter (this is for test)

namespace nemocovi {
namespace device {

class RealSense: public ISensor {
	public:
		static ISensorPtr create(int devIndex = 0, NemoCoviAppType appType = NEMOCOVI_APP_BEAM) {
			return ISensorPtr(new RealSense(devIndex, appType));
		};

		bool initialize() override;
		NemoCoviAppType getAppType() override { return m_appType; };
		int getFrameRate() override { return m_fps; };
		bool setFrameRate(int fps) override;
		void* getDepthRawFrame() override;
		void* getColorRawFrame() override;
		void* getDepthDenoisyFrame(FrameType type, int minDepth, int maxDepth) override;
		void* getColorDenoisyFrame(FrameType type) override;
		bool tryNextFrame() override;
		void getDepthFrameSize (int &width, int &height) override;
		void getColorFrameSize (int &width, int &height) override;
		uint16_t getAvgDepthValue(uint16_t *rawFrame, int x, int y, int range) override;

		~RealSense();

	private:
		void setDepthBaselineFrame();
		uint16_t* denoiseDepthRawPalmFrame(uint16_t* rawFrame);
		uint16_t* denoiseDepthRawHandFrame(uint16_t* rawFrame);
		uint16_t* denoiseDepthRawObjectFrame(uint16_t* rawFrame, int minDepth = 300, int maxDepth = 3500);
		uint8_t* denoiseColorRawObjectFrame(uint8_t* rawFrame);

		explicit RealSense(int devIndex, NemoCoviAppType appType);

		int m_devIndex;
		int m_fps; // max is 30 fps
		NemoCoviAppType m_appType;
		rs::context m_context; 
		rs::device *m_handle; // rs::device is noncopyable
		rs::intrinsics m_depthIntrin; 
		rs::intrinsics m_colorIntrin; 
		uint16_t m_baselineAvg;
		uint16_t *m_depthBaselineFrame;
		uint16_t *m_depthRawFrame;
		uint16_t *m_depthDenoisyPalmFrame;
		uint16_t *m_depthDenoisyHandFrame;
		uint16_t *m_depthDenoisyObjectFrame;
		uint8_t *m_colorRawFrame;
		uint8_t *m_colorDenoisyObjectFrame;
		std::vector<uint16_t *> m_tmpDepthFrames;
};

} // namespace device
} // namespace nemocovi

#endif //REALSENSE_HPP
