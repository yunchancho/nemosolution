#include <cassert>
#include <memory>
#include <vector>
#include <iostream>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include "realsense.hpp"
#include "realsense_calibrator.hpp"

namespace nemocovi {
namespace device {

namespace {

int g_calibrateFrameCount = 10;

} // namespace

RealSenseCalibrator::RealSenseCalibrator(ISensorPtr sensor)
	: m_sensor(sensor)
{
}

RealSenseCalibrator::~RealSenseCalibrator()
{
}

std::vector<cv::Point> RealSenseCalibrator::getScreenRectPoints()
{
	std::vector<std::vector<cv::Point> > samples;

	int width, height;
	m_sensor->getColorFrameSize(width, height);

	if (m_sensor->getAppType() == NEMOCOVI_APP_SIGNAGE) {
		// this case doesn't need projection screen rect
		std::vector<cv::Point> defaultPoints;
		defaultPoints.push_back(cv::Point(0, 0));
		defaultPoints.push_back(cv::Point(0, width));
		defaultPoints.push_back(cv::Point(width, height));
		defaultPoints.push_back(cv::Point(height, 0));
		return defaultPoints;
	} if (m_sensor->getAppType() == NEMOCOVI_APP_BEAM) {
		// prject a picture fitted to screen size
		projectScreenPicture(); 

		for (int i = 0; i < g_calibrateFrameCount; i++) {
			// check if calling tryNextFrame doesn't affect any other parts
			m_sensor->tryNextFrame();
			uint8_t *frame = static_cast<uint8_t *>(m_sensor->getColorRawFrame());
			cv::Mat filtered = filterScreenFrame(frame);
			std::vector<cv::Point> screenPoints = detectScreenObject(filtered);

			// TODO we should filter blank points
			if (screenPoints.size() == 4) { 
				samples.push_back(screenPoints);
			}
		}

		if (!samples.size()) {
			// as default, use color sensor's size
			std::vector<cv::Point> defaultPoints;
			defaultPoints.push_back(cv::Point(0, 0));
			defaultPoints.push_back(cv::Point(0, width));
			defaultPoints.push_back(cv::Point(width, height));
			defaultPoints.push_back(cv::Point(height, 0));
			return defaultPoints;
		}

		// TODO get sampling result from samples
		//

		return samples[0];
	} else {

	}
}

void RealSenseCalibrator::projectScreenPicture()
{
}

cv::Mat RealSenseCalibrator::filterScreenFrame(uint8_t *colorFrame)
{
	int width, height;
	m_sensor->getColorFrameSize(width, height);

	cv::Mat src(height, width, CV_8UC3, (void *)colorFrame);
	cv::cvtColor(src, src, CV_BGR2RGB);
	cv::medianBlur(src, src, 5);
	cv::Mat gray;
	cv::Mat output, tmp;
	cv::cvtColor(src, gray, CV_BGR2GRAY);
	cv::bilateralFilter(gray, output, 15, 10, 10);
	cv::Canny(output, output, 0, 250, 5);

#ifdef SHOW_OPENCV_WINDOW
	cv::imshow("filtered object image", output);
	cv::waitKey(1);
#endif

	return output.clone();
}

double RealSenseCalibrator::getAngle(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
	double dx1 = pt1.x - pt0.x;
	double dy1 = pt1.y - pt0.y;
	double dx2 = pt2.x - pt0.x;
	double dy2 = pt2.y - pt0.y;

	return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

std::vector<cv::Point> RealSenseCalibrator::detectScreenObject(cv::Mat &matrix)
{
	std::vector<std::vector<cv::Point> > contours;
	int width, height;

	m_sensor->getColorFrameSize(width, height);
	cv::Mat dst = cv::Mat(height, width, CV_8UC3, (void *) m_sensor->getColorRawFrame());

	cv::findContours(matrix.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	std::vector<cv::Point> approx;
	std::vector<cv::Point> maxRect;
	uint32_t maxRectArea = 0;

	for (int i = 0; i < contours.size(); i++)
	{
		// Approximate contour with accuracy proportional
		// to the contour perimeter
		cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.02, true);

		// Skip small or non-convex objects 
		uint32_t curContourArea = cv::contourArea(contours[i]);
		if (curContourArea < 300) {
			continue;
		}
		 
		std::vector<std::vector<cv::Point> > hulls(1);
		std::vector<std::vector<cv::Point> > tcontours;
		tcontours.push_back(contours[i]);
		cv::convexHull(cv::Mat(tcontours[0]), hulls[0], false);

		if (!cv::isContourConvex(approx)) {
			//cv::drawContours(dst, hulls, -1, cv::Scalar(0, 0, 255), 2);
			continue;
		} else {
			//cv::drawContours(dst, hulls, -1, cv::Scalar(0, 255, 0), 2);
		}

		//drawContours(dst, tcontours, -1, cv::Scalar(255, 0, 0),2);

		if (approx.size() == 3) {
			//setLabel(dst, "TRI", contours[i]);    // Triangles
		} else if (approx.size() >= 4 && approx.size() <= 6) {
			// Number of vertices of polygonal curve
			int vtc = approx.size();

			// Get the cosines of all corners
			std::vector<double> cos;
			for (int j = 2; j < vtc+1; j++)
				cos.push_back(getAngle(approx[j%vtc], approx[j-2], approx[j-1]));

			// Sort ascending the cosine values
			std::sort(cos.begin(), cos.end());

			// Get the lowest and the highest cosine
			double mincos = cos.front();
			double maxcos = cos.back();

			// Use the degrees obtained above and the number of vertices
			// to determine the object of the contour
			if (vtc == 4 && mincos >= -0.1 && maxcos <= 0.3)
			{
				//setLabel(dst, "RECT", contours[i]);
				if (maxRectArea < curContourArea) {
					maxRectArea = curContourArea;
					maxRect = approx;
				}
			} else if (vtc == 5 && mincos >= -0.34 && maxcos <= -0.27) {
				//setLabel(dst, "PENTA", contours[i]);
			} else if (vtc == 6 && mincos >= -0.55 && maxcos <= -0.45) {
				//setLabel(dst, "HEXA", contours[i]);
			}
		} else {
			// Detect and label circles
			double area = cv::contourArea(contours[i]);
			cv::Rect r = cv::boundingRect(contours[i]);
			int radius = r.width / 2;

			if (std::abs(1 - ((double)r.width / r.height)) <= 0.5 &&
			    std::abs(1 - (area / (CV_PI * std::pow(radius, 2)))) <= 0.5)
			{
				//setLabel(dst, "CIR", contours[i]);
			}
		}
	}

	return maxRect;
}

} // namespace device
} // namespace nemocovi
