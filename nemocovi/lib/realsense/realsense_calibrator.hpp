#ifndef REALSENSE_CALIBRATOR_HPP
#define REALSENSE_CALIBRATOR_HPP

#include <memory>
#include <vector>
#include <opencv/cv.hpp>

#include "i_calibrator.hpp"

namespace nemocovi {

class ISensor;
typedef std::shared_ptr<ISensor> ISensorPtr;

namespace device {

class RealSenseCalibrator: public ICalibrator {
	public:
		static ICalibratorPtr create(ISensorPtr sensor) {
			return ICalibratorPtr(new RealSenseCalibrator(sensor));
		};	

		std::vector<cv::Point> getScreenRectPoints() override;

		~RealSenseCalibrator();

	private:
		void projectScreenPicture();
		cv::Mat filterScreenFrame(uint8_t *colorFrame);
		double getAngle(cv::Point pt1, cv::Point pt2, cv::Point pt0);
		std::vector<cv::Point> detectScreenObject(cv::Mat &matrix);

		explicit RealSenseCalibrator(ISensorPtr sensor);	

		ISensorPtr m_sensor;
};

} // namespace device
} // namespace nemocovi

#endif //REALSENSE_CALIBRATOR_HPP
