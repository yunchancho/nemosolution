#include <iostream>
#include <librealsense/rs.hpp>

#include "realsense_inspector.hpp"

namespace nemocovi {
namespace device {

RealSenseInspector* RealSenseInspector::s_instance = NULL;

RealSenseInspector::RealSenseInspector()
	: m_devIndexes()
{
}

RealSenseInspector::~RealSenseInspector()
{
}

RealSenseInspector* RealSenseInspector::Instance()
{
	if (!s_instance) {
			s_instance = new RealSenseInspector();
	}

	return s_instance;
}

rs::device* RealSenseInspector::createHandle(rs::context &context, int devIndex)
{
	// TODO check if devIndex is already included in m_devIndexes
	// if it is, create new realsense handle
	//
	rs::device *handle = context.get_device(devIndex);
	
	return handle;
}

bool RealSenseInspector::removeHandle(int devIndex)
{
	return true;
}

} // namespace device
} // namespace nemocovi
