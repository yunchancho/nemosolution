#ifndef REALSENSE_INSPECTOR_HPP
#define REALSENSE_INSPECTOR_HPP

#include <vector>
#include <memory>

#include <librealsense/rs.hpp>

namespace nemocovi {
namespace device {

class RealSenseInspector {
	public:
		static RealSenseInspector* Instance();
		rs::device* createHandle(rs::context &context, int devIndex);
		bool removeHandle(int devIndex);

	private:
		RealSenseInspector();
		~RealSenseInspector();

		std::vector<int> m_devIndexes;
		static RealSenseInspector* s_instance;
};

} // namespace device
} // namespace nemocovi

#endif //REALSENSE_INSPECTOR_HPP
