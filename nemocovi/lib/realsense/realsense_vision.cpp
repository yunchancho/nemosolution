#include <cassert>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include "nemocovi.hpp"
#include "realsense_vision.hpp"

using namespace std;

namespace nemocovi {
namespace device {

#ifdef SHOW_OPENCV_WINDOW
static cv::RNG g_rng(12345);
#endif

RealSenseVision::RealSenseVision(ISensorPtr sensor, IMessangerPtr messanger)
	: m_sensor(sensor)
	, m_messanger(messanger)
	, m_circleSamplingCount(0)
{
	try {
		if (m_sensor == nullptr || m_messanger == nullptr) {
			// TODO throw exception for constructor creation failure
		}
		initialize();
	} catch (...) {
		throw;
	}
}

RealSenseVision::~RealSenseVision()
{
}

bool RealSenseVision::initialize()
{
	// set person default option values
	m_depthBoundaryMap[NEMOCOVI_CLASSID_OBJECT_PERSON] = { 300, 1000, 3500 };
	m_eventTrackingDistanceMap[NEMOCOVI_CLASSID_OBJECT_PERSON] = 120;
	m_depthAvgRangeMap[NEMOCOVI_CLASSID_OBJECT_PERSON] = 20;
}

std::vector<PalmPtr> RealSenseVision::getPalms(std::vector<NemoCoviClassId> &classes)
{
	std::vector<PalmPtr> palms;
	auto found = std::find(classes.begin(), classes.end(), NEMOCOVI_CLASSID_PALM_TOUCH);
	if (found != classes.end()) {
		setPalmTouch(palms);
	}

	return palms;
}

std::vector<HandPtr> RealSenseVision::getHands(std::vector<NemoCoviClassId> &classes)
{
	std::vector<HandPtr> hands;

	auto found = std::find(classes.begin(), classes.end(), NEMOCOVI_CLASSID_HAND_SWIPE);
	if (found != classes.end()) {
		setHandSwipes(hands);
	}

	//setHandGrabs(hands);
	
	return hands;
}

std::vector<ObjectPtr> RealSenseVision::getObjects(std::vector<NemoCoviClassId> &classes)
{
	std::vector<ObjectPtr> objects;

	auto found = std::find(classes.begin(), classes.end(), NEMOCOVI_CLASSID_OBJECT_CIRCLE);
	if (found != classes.end()) {
		setObjectCircles(objects);
	}

	//setObjectRectangles(objects);
	//

	found = std::find(classes.begin(), classes.end(), NEMOCOVI_CLASSID_OBJECT_PERSON);
	if (found != classes.end()) {
		setObjectPersons(objects);
	}
	
	return objects;
}

cv::Mat RealSenseVision::getRawImage(NemoCoviRawDataType type)
{
	if (type == NEMOCOVI_RAWDATA_DEPTH) {
		if (m_depthRawMatrix.empty()) {
			setRawImage(type);
		}
		return m_depthRawMatrix;
	}

	if (type == NEMOCOVI_RAWDATA_COLOR) {
		if (m_colorRawMatrix.empty()) {
			setRawImage(type);
		}

		//std::cout << ": raw image ref: " << m_colorRawMatrix.u->refcount << std::endl;
		return m_colorRawMatrix;
	}

	return cv::Mat();
}

void RealSenseVision::setRawImage(NemoCoviRawDataType type)
{
	int width, height;

	if (type == NEMOCOVI_RAWDATA_DEPTH) {
		uint16_t *frame = static_cast<uint16_t*>(m_sensor->getDepthRawFrame());
		m_sensor->getDepthFrameSize(width, height);
		m_depthRawMatrix = cv::Mat(height, width, CV_16U, (void *) frame);

	} else if (type == NEMOCOVI_RAWDATA_COLOR) {
		uint8_t *frame = static_cast<uint8_t *>(m_sensor->getColorRawFrame());
		m_sensor->getColorFrameSize(width, height);
		m_colorRawMatrix = cv::Mat(height, width, CV_8UC3, (void *) frame);
	}
}

bool RealSenseVision::tryNextFrame()
{
	m_depthRawMatrix = cv::Mat();
	m_colorRawMatrix = cv::Mat();

	return m_sensor->tryNextFrame();
}

bool RealSenseVision::setDepthBoundary(NemoCoviClassId classId, std::vector<int> depthes)
{
	m_depthBoundaryMap[classId] = depthes;
	return true;
}

bool RealSenseVision::setEventTrackingDistance(NemoCoviClassId classId, int distance)
{
	m_eventTrackingDistanceMap[classId] = distance;
	return true;
}

bool RealSenseVision::setDepthAvgRange(NemoCoviClassId classId, int range)
{
	m_depthAvgRangeMap[classId] = range;
	return true;
}

cv::Mat RealSenseVision::filterPalmFrame(uint16_t *frame)
{
	int width, height;
	cv::Mat dst;

	assert(frame);

	m_sensor->getDepthFrameSize(width, height);
	cv::Mat src(height, width, CV_16U, (void *) frame);

	src.convertTo(dst, CV_8UC1);

#ifdef SHOW_OPENCV_WINDOW
	cv::imshow("filtered palm image", dst);
	cv::waitKey(1);
#endif

	return dst.clone();
}

cv::Mat RealSenseVision::filterHandFrame(uint16_t *frame)
{
	int width, height;
	cv::Mat dst;

	m_sensor->getDepthFrameSize(width, height);
	cv::Mat src(height, width, CV_16U, (void *) frame);

	src.convertTo(dst, CV_8UC1);
	cv::Mat element1(9, 9, CV_8U, cv::Scalar(1));
	cv::morphologyEx(dst, dst, cv::MORPH_CLOSE, element1);

#ifdef SHOW_OPENCV_WINDOW
	cv::imshow("filtered hand image", dst);
	cv::waitKey(1);
#endif

	return dst.clone();
}

cv::Mat RealSenseVision::filterObjectCircleFrame(uint8_t *frame)
{
	int width, height;
	cv::Mat dst;

	m_sensor->getColorFrameSize(width, height);
	cv::Mat src(height, width, CV_8UC3, (void *) frame);

	cv::cvtColor(src, src, CV_BGR2RGB);
	cv::medianBlur(src, src, 5);
	cv::cvtColor(src, dst, CV_BGR2GRAY);

#ifdef SHOW_OPENCV_WINDOW
	cv::imshow("filtered object image", dst);
	cv::waitKey(1);
#endif

	return dst.clone();
}

cv::Mat RealSenseVision::filterObjectPersonDepthFrame(uint16_t *frame)
{
	int width, height;
	cv::Mat dst, dst2;

	assert(frame);

	m_sensor->getDepthFrameSize(width, height);
	cv::Mat src(height, width, CV_16U, (void *) frame);
	src.convertTo(dst, CV_8UC1);
#ifdef SHOW_OPENCV_WINDOW
	cv::imshow("convert to cv 8u1", dst);
#endif
	cv::Mat element1(9, 9, CV_8U, cv::Scalar(1));
	cv::morphologyEx(dst, dst, cv::MORPH_OPEN, element1);

	cv::Mat element2(9, 9, CV_8U, cv::Scalar(1));
	cv::morphologyEx(dst, dst, cv::MORPH_CLOSE, element2);
	cv::medianBlur(dst, dst, 3);

#ifdef SHOW_OPENCV_WINDOW
	cv::imshow("filtered person image", dst);
	cv::waitKey(1);
#endif

	return dst.clone();
}

cv::Mat RealSenseVision::filterObjectPersonColorFrame(uint8_t *frame)
{
	int width, height;
	cv::Mat dst;

	m_sensor->getColorFrameSize(width, height);
	cv::Mat src(height, width, CV_8UC3, (void *) frame);
	cv::cvtColor(src, dst, CV_BGR2GRAY);

	return src.clone();
}

void RealSenseVision::setPalmTouch(std::vector<PalmPtr> &palms)
{
	uint16_t *frame = static_cast<uint16_t*>(m_sensor->getDepthDenoisyFrame(FrameType::PALM, 0, 0));
	cv::Mat matrix = filterPalmFrame(frame);
	std::vector<cv::Point> centroids = findCentroids(matrix, 300);

	int fps = m_sensor->getFrameRate();

	if (centroids.size() > 0) {
		for (auto it = m_palmContexts.begin(); it != m_palmContexts.end(); ) {
			std::vector<cv::Point> &events = (*it)->events;

			cv::Point newPoint = findContinuousEvents(centroids, events.front());

			int dummyCount = 0;
			if ((newPoint.x == -1) && (newPoint.y == -1)) {
#ifdef SHOW_DEBUG_MESSAGE
				std::ostringstream oss;
				for (auto cit = centroids.begin(); cit != centroids.end(); cit++) {
					oss << *cit << ", ";
				}
				cout << "centroid not found: " << oss.str() << endl;
				cout << "compared point: " << events.front() << endl;
#endif

				events.push_back(cv::Point(-1, -1));
				dummyCount = getDummyEventsCount(events);
			} else {
#ifdef SHOW_DEBUG_MESSAGE
				cout << "continuous centroid found: " << newPoint << endl;
#endif
				removeDummyEvents(events);
				int dx = newPoint.x - events.front().x;
				int dy = newPoint.y - events.front().y;

				// check if this is move event 
				if (sqrt(dx * dx + dy * dy) > 25) {
					events.push_back(newPoint);
					events.front() = newPoint;
					addEvents(palms, *it, NEMOCOVI_CLASSID_PALM, NEMOCOVI_ACTIONID_MOTION);
				} else {
					addEvents(palms, *it, NEMOCOVI_CLASSID_PALM, NEMOCOVI_ACTIONID_MOTION, false);
				}
			}

			if (dummyCount == ceil(fps / 3)) {
				addEvents(palms, *it, NEMOCOVI_CLASSID_PALM, NEMOCOVI_ACTIONID_UP);
				it = m_palmContexts.erase(it);
			} else {
				it++;
			}
		}
	} else {
		for (auto it = m_palmContexts.begin(); it != m_palmContexts.end(); ) {
			std::vector<cv::Point> &events = (*it)->events;
			events.push_back(cv::Point(-1, -1));

			// check if this is for mouse up
			int dummyCount = getDummyEventsCount(events);
			if (dummyCount == ceil(fps / 3)) {
				addEvents(palms, *it, NEMOCOVI_CLASSID_PALM, NEMOCOVI_ACTIONID_UP);
				it = m_palmContexts.erase(it);
			} else {
				it++;
			}
		}
	}

	// add new event vector if there is unhandled centroids
	if (centroids.size() > 0) {
		for (auto cit = centroids.begin(); cit != centroids.end(); cit++) {
#ifdef SHOW_DEBUG_MESSAGE
			cout << "palm down: " << *cit << endl;
#endif
			PalmContextPtr newPalmContext(new struct PalmContext());
			newPalmContext->sid = m_messanger->generateSessionId();
			newPalmContext->events.push_back(*cit);
#ifdef SHOW_OPENCV_WINDOW	
			newPalmContext->color = cv::Scalar(g_rng.uniform(0, 255), g_rng.uniform(0, 255), g_rng.uniform(0, 255));
#endif
			m_palmContexts.push_back(newPalmContext);

			addEvents(palms, newPalmContext, NEMOCOVI_CLASSID_PALM, NEMOCOVI_ACTIONID_DOWN);
		}
	}

#ifdef SHOW_OPENCV_WINDOW
	showPalmResult(palms);
#endif
}

void RealSenseVision::setHandSwipes(std::vector<HandPtr> &hands)
{
	int width, height;
	cv::Mat matrix, dst;
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	std::vector<HandPtr> palms;
	cv::RNG rng(12345);

	m_sensor->getDepthFrameSize(width, height);

	uint16_t *frame = static_cast<uint16_t*>(m_sensor->getDepthDenoisyFrame(FrameType::HAND, 0, 0));
	matrix = filterHandFrame(frame);
	dst = cv::Mat::zeros(matrix.size(), CV_8UC3);
	cv::findContours(matrix.clone(), contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));

	std::vector<std::vector<cv::Point> > tips;
	std::vector<StartEdge> tipEdges;
	StartEdge edge = StartEdge::Invalid;

	int fps = m_sensor->getFrameRate();

	for (int i = 0; i < contours.size(); i++) {
		if (cv::contourArea(contours[i]) >= 3000) {

			std::vector<std::vector<cv::Point> > tcontours;
			tcontours.push_back(contours[i]);
			cv::drawContours(dst, tcontours, -1, cv::Scalar(0, 0, 255), 2);

			std::vector<std::vector<cv::Point> > hulls(1);
			std::vector<std::vector<int> > hullsI(1);
			cv::convexHull(cv::Mat(tcontours[0]), hulls[0], false);
			cv::convexHull(cv::Mat(tcontours[0]), hullsI[0], false);

			std::vector<std::vector<cv::Vec4i> > convexDefects(contours.size());
			cv::convexityDefects(tcontours[0], hullsI[0], convexDefects[i]);

			std::vector<cv::Point> hullContour;
			cv::approxPolyDP(cv::Mat(hulls[0]), hullContour, 0.001, true);

			if (std::fabs(contourArea(cv::Mat(hullContour))) > 3000) {
				cv::Scalar color(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
				//cv::drawContours(dst, contours, (int) i, color, CV_FILLED, 8, hierarchy, 0, cv::Point());
				cv::drawContours(dst, hulls, -1, cv::Scalar(0, 255, 0), 2);
				std::vector<cv::Point> &hull = hulls[0];

				cv::Point bottom = cv::Point(-1, -1);

				for (int i = 0; i < hull.size(); i++) {
					//cout << "hull[i]: " << hull[i] << endl;
					if ((hull[i].x >= 0) && (hull[i].x <= 35)) {
						for (int j = i + 1; j < hull.size(); j++) {
							if (hull[j].x >= (hull[i].x - 5) && hull[j].x <= (hull[i].x + 5)) {
								bottom = cv::Point((hull[i].x + hull[j].x) / 2, (hull[i].y + hull[j].y) / 2);
								edge = StartEdge::Left;
								break;
							}
						}
						break;
					} else if (((hull[i].x >= (width - 70)) && (hull[i].x <= width))) {
						for (int j = i + 1; j < hull.size(); j++) {
							if (hull[j].x >= (hull[i].x - 5) && hull[j].x <= (hull[i].x + 5)) {
								bottom = cv::Point((hull[i].x + hull[j].x) / 2, (hull[i].y + hull[j].y) / 2);
								edge = StartEdge::Right;
								break;
							}
						}
						break;
					} else if ((hull[i].y >= 0) && (hull[i].y <= 10)) {
						for (int j = i + 1; j < hull.size(); j++) {
							if (hull[j].y >= (hull[i].y - 5) && hull[j].y <= (hull[i].y + 5)) {
								bottom = cv::Point((hull[i].x + hull[j].x) / 2, (hull[i].y + hull[j].y) / 2);
								edge = StartEdge::Top;
								break;
							}
						}
						break;
					} else if ((hull[i].y >= (height - 10)) && (hull[i].y <= height)) {
						for (int j = i + 1; j < hull.size(); j++) {
							if (hull[j].y >= (hull[i].y - 5) && hull[j].y <= (hull[i].y + 5)) {
								bottom = cv::Point((hull[i].x + hull[j].x) / 2, (hull[i].y + hull[j].y) / 2);
								edge = StartEdge::Bottom;
								break;
							}
						}
						break;
					}
				}

				if (bottom == cv::Point(-1, -1)) {
					continue;
				} 

				int max_distance = 0;
				cv::Point point;

				for (auto it = hull.begin(); it != hull.end(); it++) {
					int dx = bottom.x - (*it).x;
					int dy = bottom.y - (*it).y;

					int distance = sqrt(dx * dx + dy * dy);
					if (distance > max_distance) {
						point = *it;
						max_distance = distance;
					}
				}

#ifdef SHOW_OPENCV_WINDOW
				cv::circle(dst, point, 5, cv::Scalar(255, 255, 255), -1);
#endif
				tips.push_back({point, bottom});
				tipEdges.push_back(edge);
			}
		}
	}

	// recognize tips(end finger tip) is for hand
	// 0. if there is no the tips point, invalid point (-1,-1) is added into every swipe histories. 
	// 1. find vertical points 
	// 2. get angle and compare direction to existing one to be same
	// 3. check if angle is in threshold angle, or not.
	// . if so, add new point into events, and emit swipe event (move)
	// . if not so, emit swipe event (up) and remove existing events
	// . we also should check if the point is on 4 side edge. if so, we should do the step above.
	// 4. if any tips is not include in this swipe events, add invalid point (-1,-1) 
	// 5. check if this swipe events is ended. if so, emit swipe up event and remove the events
	// 6. if vertical point is new, create new swipe events, and emit swipe down event
	if (tips.size() > 0) {
		for (auto it = m_swipeContexts.begin(); it != m_swipeContexts.end(); ) {
#ifdef SHOW_DEBUG_MESSAGE
			cout << "swipe vector size: " << m_swipeContexts.size() << endl;
#endif
			HandContextPtr context = *it;
			
			int point_count = 0;
			cv::Point point = cv::Point(-1, -1);
			for (auto hit = context->events.cbegin(); hit != context->events.cend(); hit++) {
				cv::Point tmp = (*hit)[0];
				if ((tmp.x == -1) && (tmp.y == -1)) {
					continue;
				}

				point = (*hit)[0];
				point_count++;
			}

			bool found = false;
			cv::Point mc;

			auto tit = tips.begin();
		 	auto eit = tipEdges.begin();
			while(tit != tips.end()) {
				mc = (*tit)[0];

				int dx = mc.x - point.x;
				int dy = mc.y - point.y;
				int angle;
				int distance = sqrt(dx * dx + dy * dy);

				if ((distance > 0) && (distance < 180)) {
					cv::Point vpoint;
					if ((context->edge == StartEdge::Top) || (context->edge == StartEdge::Bottom)) {
						vpoint.x = mc.x;
						vpoint.y = point.y;
						angle = getDefectSwipeAngle(mc, vpoint, point);
					} else {
						vpoint.x = mc.x;
						vpoint.y = point.y;
						angle = getDefectSwipeAngle(point, vpoint, mc);
					}

					if (std::abs(angle) < 30) {
						if (!found) {
							context->events.push_back(*tit);
							context->events.front() = *tit;

							if (point_count > 1) {
								//TODO we should send enter event first 
								// swipe motion 
								addEvents(hands, context, NEMOCOVI_CLASSID_HAND_SWIPE, NEMOCOVI_ACTIONID_MOTION);
							} else {
								// swipe enter
								addEvents(hands, context, NEMOCOVI_CLASSID_HAND_SWIPE, NEMOCOVI_ACTIONID_ENTER);
							}

							found = true;
						} else {
							// remove noise near already handled point, but this is weak solution..
							// on current data condition, this routine can be not entered here.
							//
						}
					} else if (distance == 0) {
						// current point and latest point is same, remove current one
					}	else {
						// angle dosen't satisfy in proper range
						//cout << "angle problem: " << mc << ", angle: " << angle << ", distance: " << distance << endl;
					}
					tit = tips.erase(tit);
					eit = tipEdges.erase(eit);
					continue;
				} 
				tit++;
				eit++;
			}

			if (!found) {
				// satisfied tips not found. this is ignored.
				context->events.push_back({cv::Point(-1, -1), cv::Point(-1, -1)});

				// check this triggeres up event 
				int dummyCount = 0;
				for (auto eit = context->events.crbegin(); eit != context->events.crend(); eit++) {
					cv::Point point = (*eit)[0];
					if ((point.x == -1) && (point.y == -1)) {
						dummyCount++;
						continue;
					}
					break;
				}

				int validCount = 0;
				for (auto eit = context->events.crbegin(); eit != context->events.crend(); eit++) {
					cv::Point point = (*eit)[0];
					if ((point.x == -1) && (point.y == -1)) {
						continue;
					}
					validCount++;
				}

				if (dummyCount >= ceil(fps / 3)) {
					if (validCount > 1) {
						//dummy point is fullfilled
						//swipe leave
						addEvents(hands, context, NEMOCOVI_CLASSID_HAND_SWIPE, NEMOCOVI_ACTIONID_LEAVE);
					} 

					it = m_swipeContexts.erase(it);

				} else {
					it++;
				}
			} else {
				if (((mc.x >= 0) && (mc.x <= 35)) ||
						((mc.x >= (width - 10)) && (mc.x <= width)) ||
						((mc.y >= 0) && (mc.y <= 10)) ||
						((mc.y >= (height - 10)) && (mc.y <= height))) {

					it = m_swipeContexts.erase(it);
					// last point is on edge
					// swipe leave
					addEvents(hands, context, NEMOCOVI_CLASSID_HAND_SWIPE, NEMOCOVI_ACTIONID_LEAVE);
				} else {
					it++;
				}
			}

			//break;
		}
	} else {
#ifdef SHOW_DEBUG_MESSAGE
		cout << "tips not founds.." << endl;
#endif
		for (auto it = m_swipeContexts.begin(); it != m_swipeContexts.end(); ) {
			HandContextPtr context = *it;
			it = m_swipeContexts.erase(it);

			int point_count = 0;
			for (auto hit = context->events.cbegin(); hit != context->events.cend(); hit++) {
				cv::Point tmp = (*hit)[0];
				if ((tmp.x == -1) && (tmp.y == -1)) {
					continue;
				}
				point_count++;
			}

			// the contexts not triggerred by enter should be not added as events
			// these might be generated by noise
			if (point_count > 1) {
				addEvents(hands, context, NEMOCOVI_CLASSID_HAND_SWIPE, NEMOCOVI_ACTIONID_LEAVE);
			}
		}
	}	

	// add new event vector if there is unhandled centroids
	if (tips.size() > 0) {
		for (int i = 0; i < tips.size(); i++) {
			HandContextPtr newHandContext(new struct HandContext());
			newHandContext->sid = m_messanger->generateSessionId();
			newHandContext->edge = tipEdges[i];
			newHandContext->events.push_back(tips[i]);
			newHandContext->color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));

			// we don't recognize this to swipe enter, because this can be noisy
			m_swipeContexts.push_back(newHandContext);
		}
	}

#ifdef SHOW_OPENCV_WINDOW
	showHandSwipeResult(hands);
#endif
}

void RealSenseVision::setHandGrabs(std::vector<HandPtr> &hands)
{
}

void RealSenseVision::setObjectCircles(std::vector<ObjectPtr> &objects)
{
	std::vector<cv::Vec3f> circles, newFoundCircles;

	int width, height;
	m_sensor->getColorFrameSize(width, height);
	uint8_t *frame = static_cast<uint8_t *>(m_sensor->getColorDenoisyFrame(FrameType::OBJECT));
	cv::Mat matrix = filterObjectCircleFrame(frame);
	cv::HoughCircles(matrix, circles, CV_HOUGH_GRADIENT, 2, matrix.rows/16, 300, 90, 0, width * 0.25); 

	// remove false alram circle using depth info
	/*
	for (auto it = newFoundCircles.begin(); it != newFoundCircles.end(); ) {
		cv::Vec3f circle = *it;
	}
	*/

	int fps = m_sensor->getFrameRate();
	m_circleSamples.push_back(circles);

	// TODO we need to do sampling for several frames before handling found circles 
	if (m_circleSamplingCount < 0) {
		for (auto it = m_circleContexts.begin(); it != m_circleContexts.end(); it++) {
			addEvents(objects, *it, NEMOCOVI_CLASSID_OBJECT_CIRCLE, NEMOCOVI_ACTIONID_MOTION, false);
		}
		m_circleSamplingCount++;
		return;
	}
 
	for (int i = 0; i < m_circleSamples.size(); i++) {
		for (int j = 0; j < m_circleSamples[i].size(); j++) {
			newFoundCircles.push_back(m_circleSamples[i][j]);
		}
	}

	m_circleSamplingCount = 0;
	m_circleSamples.clear();

	if (newFoundCircles.size() > 0) {
		for (auto it = m_circleContexts.begin(); it != m_circleContexts.end(); ) {
			std::vector<cv::Point> &events = (*it)->events;

			cv::Vec3f newPoint = findContinuousEvents(newFoundCircles, events.front(), 30);

			int dummyCount = 0;
			if ((newPoint[0] == -1) && (newPoint[1] == -1)) {
#ifdef SHOW_DEBUG_MESSAGE
				std::ostringstream oss;
				for (auto cit = newFoundCircles.begin(); cit != newFoundCircles.end(); cit++) {
					oss << *cit << ", ";
				}
				cout << "continuous circle not found: " << oss.str() << endl;
				cout << "compared point: " << events.front() << endl;
#endif

				events.push_back(cv::Point(-1, -1));
				dummyCount = getDummyEventsCount(events);
			} else {
#ifdef SHOW_DEBUG_MESSAGE
				cout << "continuous centroid found: " << newPoint << endl;
#endif
				removeDummyEvents(events);
				int dx = newPoint[0] - events.front().x;
				int dy = newPoint[1] - events.front().y;

				// check if this is move event 
				if (sqrt(dx * dx + dy * dy) > 15) {
					cv::Point p(newPoint[0], newPoint[1]);
					events.push_back(p);
					events.front() = p;
					(*it)->radius = newPoint[2];
					addEvents(objects, *it, NEMOCOVI_CLASSID_OBJECT_CIRCLE, NEMOCOVI_ACTIONID_MOTION);
				} else {
					addEvents(objects, *it, NEMOCOVI_CLASSID_OBJECT_CIRCLE, NEMOCOVI_ACTIONID_MOTION, false);
				}
			}

			if (dummyCount == fps) {
#ifdef SHOW_DEBUG_MESSAGE
				cout << "object up: " << events.front() << endl;
#endif
				addEvents(objects, *it, NEMOCOVI_CLASSID_OBJECT_CIRCLE, NEMOCOVI_ACTIONID_LEAVE);
				it = m_circleContexts.erase(it);
			} else {
				it++;
			}
		}
	} else {
		for (auto it = m_circleContexts.begin(); it != m_circleContexts.end(); ) {
			std::vector<cv::Point> &events = (*it)->events;
			events.push_back(cv::Point(-1, -1));

			// check if this is for mouse up
			int dummyCount = getDummyEventsCount(events);
			if (dummyCount == fps) {
#ifdef SHOW_DEBUG_MESSAGE
				cout << "object up: " << events.front() << endl;
#endif
				addEvents(objects, *it, NEMOCOVI_CLASSID_OBJECT_CIRCLE, NEMOCOVI_ACTIONID_LEAVE);
				it = m_circleContexts.erase(it);
			} else {
				it++;
			}
		}
	}

	// add new event vector if there is unhandled centroids
	if (newFoundCircles.size() > 0) {
		for (auto cit = newFoundCircles.begin(); cit != newFoundCircles.end(); cit++) {
#ifdef SHOW_DEBUG_MESSAGE
			cout << "object down: " << *cit << endl;
#endif
			cv::Vec3f circle = *cit;
			ObjectContextPtr newObjectContext(new struct ObjectContext());
			newObjectContext->sid = m_messanger->generateSessionId();
			newObjectContext->events.push_back(cv::Point(circle[0], circle[1]));
			newObjectContext->radius = circle[2];
#ifdef SHOW_OPENCV_WINDOW	
			newObjectContext->color = cv::Scalar(g_rng.uniform(0, 255), g_rng.uniform(0, 255), g_rng.uniform(0, 255));
#endif
			m_circleContexts.push_back(newObjectContext);

			addEvents(objects, newObjectContext, NEMOCOVI_CLASSID_OBJECT_CIRCLE, NEMOCOVI_ACTIONID_ENTER);
		}
	}

#ifdef SHOW_OPENCV_WINDOW
	showObjectCircleResult(objects);
#endif
}

void RealSenseVision::setObjectRectangles(std::vector<ObjectPtr> &objects)
{
}

void RealSenseVision::setObjectPersons(std::vector<ObjectPtr> &objects)
{
	int frameWidth, frameHeight; 
	m_sensor->getDepthFrameSize(frameWidth, frameHeight);
	uint16_t *frame = static_cast<uint16_t*>(m_sensor->getDepthDenoisyFrame(FrameType::OBJECT, 0, 0));

	std::vector<cv::Point> origCentroids, centroids;
	std::vector<cv::Rect> boundings;
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	std::vector<int> contourIdxs;

	int fps = m_sensor->getFrameRate();

	setPersonDetectionData(origCentroids, boundings, contourIdxs, contours, hierarchy);
	centroids = origCentroids;

	if (centroids.size() > 0) {
		for (auto it = m_personContexts.begin(); it != m_personContexts.end(); ) {
			std::vector<cv::Point> &events = (*it)->events;

			cv::Point newPoint = findContinuousEvents(centroids, events.front(), m_eventTrackingDistanceMap[NEMOCOVI_CLASSID_OBJECT_PERSON]);

			int dummyCount = 0;
			if ((newPoint.x == -1) && (newPoint.y == -1)) {
#ifdef SHOW_DEBUG_MESSAGE
				std::ostringstream oss;
				for (auto cit = centroids.begin(); cit != centroids.end(); cit++) {
					oss << *cit << ", ";
				}
				cout << "centroid not found: " << oss.str() << endl;
				cout << "compared point: " << events.front() << endl;
#endif

				events.push_back(cv::Point(-1, -1));
				dummyCount = getDummyEventsCount(events);
			} else {
#ifdef SHOW_DEBUG_MESSAGE
				cout << "continuous centroid found: " << newPoint << endl;
#endif
				// set meta data of person
				auto found = find(origCentroids.begin(), origCentroids.end(), newPoint);
				auto index = std::distance(origCentroids.begin(), found);
				cv::Rect boundingBox = boundings[index];
				(*it)->width = boundingBox.br().x - boundingBox.tl().x;
				(*it)->height = boundingBox.br().y - boundingBox.tl().y;
				(*it)->depth = m_sensor->getAvgDepthValue(frame, newPoint.x, newPoint.y, 20);
				(*it)->bounding = boundingBox;
				(*it)->contours = contours;
				(*it)->contourHierarchy= hierarchy;
				(*it)->contourIdx = contourIdxs[index];

				removeDummyEvents(events);
				int dx = newPoint.x - events.front().x;
				int dy = newPoint.y - events.front().y;

				// check if this is move event 
				if (sqrt(dx * dx + dy * dy) > 25) {
					events.push_back(newPoint);
					events.front() = newPoint;
					addEvents(objects, *it, NEMOCOVI_CLASSID_OBJECT_PERSON, NEMOCOVI_ACTIONID_MOTION);
				} else {
					addEvents(objects, *it, NEMOCOVI_CLASSID_OBJECT_PERSON, NEMOCOVI_ACTIONID_MOTION, false);
				}
			}

			if (dummyCount == ceil(fps / 3)) {
				(*it)->duration = getUnixTimestampNow() - (*it)->startTime;
				addEvents(objects, *it, NEMOCOVI_CLASSID_OBJECT_PERSON, NEMOCOVI_ACTIONID_LEAVE);
				it = m_personContexts.erase(it);
			} else {
				it++;
			}
		}
	} else {
		for (auto it = m_personContexts.begin(); it != m_personContexts.end(); ) {
			std::vector<cv::Point> &events = (*it)->events;
			events.push_back(cv::Point(-1, -1));

			// check if this is for person leave
			int dummyCount = getDummyEventsCount(events);
			if (dummyCount == ceil(fps / 3)) {
				(*it)->duration = getUnixTimestampNow() - (*it)->startTime;
				addEvents(objects, *it, NEMOCOVI_CLASSID_OBJECT_PERSON, NEMOCOVI_ACTIONID_LEAVE);
				it = m_personContexts.erase(it);
			} else {
				it++;
			}
		}
	}

	// add new event vector if there is unhandled centroids
	if (centroids.size() > 0) {
		int depthAvgRange = m_depthAvgRangeMap[NEMOCOVI_CLASSID_OBJECT_PERSON];
		for (auto cit = centroids.begin(); cit != centroids.end(); cit++) {
#ifdef SHOW_DEBUG_MESSAGE
			cout << "palm down: " << *cit << endl;
#endif
			ObjectContextPtr newObjectContext(new struct ObjectContext());
			newObjectContext->sid = m_messanger->generateSessionId();
			newObjectContext->events.push_back(*cit);
#ifdef SHOW_OPENCV_WINDOW	
			newObjectContext->color = cv::Scalar(g_rng.uniform(0, 255), g_rng.uniform(0, 255), g_rng.uniform(0, 255));
#endif
			// set meta data of person
			auto found = find(origCentroids.begin(), origCentroids.end(), (*cit));
			auto index = std::distance(origCentroids.begin(), found);
			cv::Rect boundingBox = boundings[index];
			newObjectContext->width = boundingBox.br().x - boundingBox.tl().x;
			newObjectContext->height = boundingBox.br().y - boundingBox.tl().y;
			newObjectContext->depth = m_sensor->getAvgDepthValue(frame, cit->x, cit->y, depthAvgRange);
			newObjectContext->bounding = boundingBox;
			newObjectContext->contours = contours;
			newObjectContext->contourHierarchy= hierarchy;
			newObjectContext->contourIdx = contourIdxs[index];
			newObjectContext->startTime = getUnixTimestampNow();

			m_personContexts.push_back(newObjectContext);
			addEvents(objects, newObjectContext, NEMOCOVI_CLASSID_OBJECT_PERSON, NEMOCOVI_ACTIONID_ENTER);
		}
	}

#ifdef SHOW_OPENCV_WINDOW
	showObjectPersonResult(objects);
#endif
}

void RealSenseVision::setPersonDetectionData(std::vector<cv::Point> &centroids, std::vector<cv::Rect> &boundings, std::vector<int> &contourIdxs, std::vector<std::vector<cv::Point> > &contours, std::vector<cv::Vec4i> &hierarchy) 
{
	uint32_t personAreaMinLimit = 10000;
	for (int i = 0; i < m_depthBoundaryMap[NEMOCOVI_CLASSID_OBJECT_PERSON].size() - 1; i++) {
		int start = m_depthBoundaryMap[NEMOCOVI_CLASSID_OBJECT_PERSON][i];
		int end = m_depthBoundaryMap[NEMOCOVI_CLASSID_OBJECT_PERSON][i + 1];

		uint16_t *frame = static_cast<uint16_t*>(m_sensor->getDepthDenoisyFrame(FrameType::OBJECT, start, end));
		cv::Mat matrix = filterObjectPersonDepthFrame(frame);
		cv::findContours(matrix, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));

		int idx = 0;
		for (auto it = contours.begin(); it != contours.end(); it++, idx++) {
			if (cv::contourArea(*it) >= personAreaMinLimit) {
				std::vector<cv::Point> hullContour;
				cv::approxPolyDP(cv::Mat(*it), hullContour, 0.001, true);
				cv::Rect bounding = cv::boundingRect(cv::Mat(hullContour));
				boundings.push_back(bounding);
				contourIdxs.push_back(idx);

				// get cetnroid of contour
				cv::Moments mu = cv::moments(*it, false);
				cv::Point mc = cv::Point(mu.m10/mu.m00, mu.m01/mu.m00);
				centroids.push_back(mc);
			}
		}
	}
}

int RealSenseVision::getDummyEventsCount(std::vector<cv::Point> &events)
{
	int dummyCount = 0;
	for (auto eit = events.crbegin(); eit != events.crend(); eit++) {
		cv::Point point = *eit;
		if ((point.x == -1) && (point.y == -1)) {
			dummyCount++;
			continue;
		}
		break;
	}

	return dummyCount;
}

void RealSenseVision::removeDummyEvents(std::vector<cv::Point> &events)
{
	for (auto eit = events.begin(); eit != events.end(); ) {
		cv::Point point = *eit;
		if ((point.x == -1) && (point.y == -1)) {
			eit = events.erase(eit);
		} else {
			eit++;
		}
	}
}

std::vector<cv::Point> RealSenseVision::findCentroids(cv::Mat &matrix, int approxAreaLimit)
{
	//cv::RNG rng(12345);
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	cv::findContours(matrix.clone(), contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));
	std::vector<cv::Point> centroids;

	for (int i = 0; i < contours.size(); i++) {
		if (cv::contourArea(contours[i]) >= 50 && cv::contourArea(contours[i]) <= 1000) {
			//cv::Scalar color(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));

			std::vector<std::vector<cv::Point> > tcontours;
			tcontours.push_back(contours[i]);
			//cv::drawContours(dst, tcontours, -1, cv::Scalar(0, 0, 255), 2);

			std::vector<std::vector<cv::Point> > hulls(1);
			cv::convexHull(cv::Mat(tcontours[0]), hulls[0], false);
			std::vector<cv::Point> hullContour;
			cv::approxPolyDP(cv::Mat(hulls[0]), hullContour, 0.001, true);

			if (std::fabs(contourArea(cv::Mat(hullContour))) > approxAreaLimit) {
				//cv::drawContours(dst, contours, (int) i, color, CV_FILLED, 8, hierarchy, 0, cv::Point());
				//cv::drawContours(dst, hulls, -1, cv::Scalar(0, 255, 0), 2);

				// get cetnroid of contour
				cv::Moments mu = cv::moments(contours[i], false);
				cv::Point mc = cv::Point(mu.m10/mu.m00, mu.m01/mu.m00);
				centroids.push_back(mc);
			}
		}
	}

	return centroids;
}

cv::Point RealSenseVision::findContinuousEvents(std::vector<cv::Point> &newFoundPoints, cv::Point target, uint32_t threshold)
{
	bool found = false;

	cv::Point newPoint;
	cv::Point continuousPoint;
	for (auto it = newFoundPoints.begin(); it != newFoundPoints.end(); ) {
		newPoint = *it;
		int dx = newPoint.x - target.x;
		int dy = newPoint.y - target.y;

		// check if this is in validPoint's boundary
		if (sqrt(dx * dx + dy * dy) < threshold) {
			if (!found) {
				found = true;
				continuousPoint = newPoint;
			} 
			it = newFoundPoints.erase(it);
		} else { 
			it++;
		}
	}

	if (!found) {
		continuousPoint = cv::Point(-1, -1);
	}

	return continuousPoint;
}

cv::Vec3f RealSenseVision::findContinuousEvents(std::vector<cv::Vec3f> &newFoundPoints, cv::Point target, uint32_t threshold)
{
	bool found = false;

	cv::Vec3f newPoint;
	cv::Vec3f continuousPoint;
	for (auto it = newFoundPoints.begin(); it != newFoundPoints.end(); ) {
		newPoint = *it;
		int dx = newPoint[0] - target.x;
		int dy = newPoint[1] - target.y;

		// check if this is in validPoint's boundary
		if (sqrt(dx * dx + dy * dy) < threshold) {
			if (!found) {
				found = true;
				continuousPoint = newPoint;
			} 
			it = newFoundPoints.erase(it);
		} else { 
			it++;
		}
	}

	if (!found) {
		continuousPoint = cv::Vec3f(-1.0f, -1.0f, -1.0f);
	}

	return continuousPoint;
}

void RealSenseVision::addEvents(std::vector<PalmPtr> &palms, PalmContextPtr context, NemoCoviClassId cid, NemoCoviActionId aid, bool changed)
{	
	PalmPtr palm(new struct Palm());
	palm->sid = context->sid;
	palm->cid = cid;
	palm->aid = aid;
	palm->isChanged = changed;
	palm->coord = context->events.front();;

	palms.push_back(palm);
}	

void RealSenseVision::addEvents(std::vector<HandPtr> &hands, HandContextPtr context, NemoCoviClassId cid, NemoCoviActionId aid, bool changed)
{
	HandPtr hand(new struct Hand());
	hand->sid = context->sid;
	hand->cid = cid;
	hand->aid = aid;
	hand->isChanged = changed;
	
	std::vector<cv::Point> pair = context->events.front();
	hand->coord = pair[0];
	hand->coord1 = pair[1];

	hands.push_back(hand);

}

void RealSenseVision::addEvents(std::vector<ObjectPtr> &objects, ObjectContextPtr context, NemoCoviClassId cid, NemoCoviActionId aid, bool changed)
{
	ObjectPtr object(new struct Object());
	object->sid = context->sid;
	object->cid = cid;
	object->aid = aid;
	object->isChanged = changed;
	object->startTime = context->startTime;
	object->duration = context->duration;

	if (cid == NEMOCOVI_CLASSID_OBJECT_CIRCLE)
	{
		object->coord = context->events.front(); // object center
		object->width = context->radius * 2;
		object->height = context->radius * 2;
	}

	if (cid == NEMOCOVI_CLASSID_OBJECT_PERSON)
	{
		object->coord = context->bounding.tl(); // object top-left
		object->width = context->width;
		object->height = context->height;
		object->depth = context->depth;
	}

	objects.push_back(object);
}

int RealSenseVision::getDefectSwipeAngle(cv::Point start, cv::Point end, cv::Point center)
{
	float angle;

	cv::Point cs = center - start;
	cv::Point ce = center - end;

	float angcs = atan2(cs.y, cs.x);
	float angce = atan2(ce.y, ce.x);
	float diff = angcs - angce;

	angle = ((diff * 180) / CV_PI);
	return angle;
}

void RealSenseVision::showPalmResult(std::vector<PalmPtr> &palms)
{
#ifdef SHOW_DEBUG_MESSAGE
	int row = 0;
	for (auto it = m_palmContexts.begin(); it != m_palmContexts.end(); it++) {
		ostringstream oss;
		std::vector<cv::Point> &events = (*it)->events;
		oss << "-------------------------" << endl;
		oss << "[" << row << "] ";
		for (auto eit  = events.begin(); eit != events.end(); eit++) {
			oss << *eit << ", ";
		}
		oss << endl;
	 	oss << "-------------------------" << endl;
		cout << oss.str() << endl;
		row++;
	}
#endif

	int width, height;
	m_sensor->getDepthFrameSize(width, height);
	cv::Mat dst = cv::Mat::zeros(height, width, CV_8UC3);

	for (auto it = m_palmContexts.begin(); it != m_palmContexts.end(); it++) {
		std::vector<cv::Point> &events = (*it)->events;
		//cv::Point coord = events.front();
		for (auto eit = events.begin(); eit != events.end(); eit++) {
			if ((*eit).x != -1 && (*eit).y != -1) {
				cv::circle(dst, *eit, 3, (*it)->color, -1);
			}
		}
		// draw lastest available point info
		ostringstream oss;
		cv::Point last = events.front();

		oss << "(" << last.x << ", " << last.y << ")";
		last.y = last.y - 10;
		cv::putText(dst, oss.str(), last, 1, 1.0, cv::Scalar::all(255));
	}


	for (auto it = palms.begin(); it != palms.end(); it++) {
		PalmPtr palm = *it;
		cv::Point coord = palm->coord;

		cv::circle(dst, coord, 3, cv::Scalar(0, 255, 255), -1);

		ostringstream oss;
		oss << "(" << coord.x << ", " << coord.y << ")";

		coord.y = coord.y - 10;
		cv::putText(dst, oss.str(), coord, 1, 1.0, cv::Scalar::all(255));

		oss.str("");
		oss.clear();
		oss << "palm ";
		if (palm->aid == NEMOCOVI_ACTIONID_DOWN) {
			oss << "down";
		} else if (palm->aid == NEMOCOVI_ACTIONID_UP) {
			oss << "up";
		} else if (palm->aid == NEMOCOVI_ACTIONID_MOTION) {
			oss << "motion";
		} else {
			oss << "invalid";
		}

		coord.y = coord.y + 30;
		cv::putText(dst, oss.str(), coord, 1, 1.0, cv::Scalar::all(255));
	}

	ostringstream oss;
	oss << "current alive palm count: " << m_palmContexts.size();
	cv::putText(dst, oss.str(), cv::Point(10, 10), 1, 1.0, cv::Scalar::all(255));

	cv::imshow("Depth Palm Result", dst);
}

void RealSenseVision::showHandSwipeResult(std::vector<HandPtr> &hands)
{
#ifdef SHOW_DEBUG_MESSAGE
	int row = 0;
	for (auto it = m_swipeContexts.begin(); it != m_swipeContexts.end(); it++) {
		std::ostringstream oss;
		HandContextPtr context = *it;
		oss << "-------------------------" << endl;
		oss << "[" << row << "] ";
		for (auto eit  = context->events.begin(); eit != context->events.end(); eit++) {
			oss << *eit << ", ";
		}
		oss << endl;
	 	oss << "-------------------------" << endl;
		cout << oss.str() << endl;
		row++;
	}
#endif
}

void RealSenseVision::showObjectCircleResult(std::vector<ObjectPtr> &objects)
{
	int width, height;
	m_sensor->getColorFrameSize(width, height);
	cv::Mat dst(height, width, CV_8UC3, (void *) m_sensor->getColorRawFrame());
	cv::cvtColor(dst, dst, CV_BGR2RGB);

#ifdef SHOW_DEBUG_MESSAGE
	int row = 0;
	for (auto it = m_circleContexts.begin(); it != m_circleContexts.end(); it++) {
		ostringstream oss;
		std::vector<cv::Point> &events = (*it)->events;
		oss << "-------------------------" << endl;
		oss << "[" << row << "] ";
		for (auto eit  = events.begin(); eit != events.end(); eit++) {
			oss << *eit << ", ";
		}
		oss << endl;
	 	oss << "-------------------------" << endl;
		cout << oss.str() << endl;
		row++;
	}
#endif

	for (auto it = m_circleContexts.begin(); it != m_circleContexts.end(); it++) {
		std::vector<cv::Point> &events = (*it)->events;
		for (auto eit = events.begin(); eit != events.end(); eit++) {
			if ((*eit).x != -1 && (*eit).y != -1) {
				cv::circle(dst, *eit, 3, (*it)->color, -1);
			}
		}

		int dummyCount = getDummyEventsCount(events);
		// draw lastest available point info
		ostringstream oss;
		cv::Point last = events.front();
		cv::circle(dst, last, (*it)->radius, cv::Scalar(0, 255, 0), 3, CV_AA);
		oss << "(" << last.x << ", " << last.y << ") / " <<  dummyCount;
		last.y = last.y - 10;
		cv::putText(dst, oss.str(), last, 1, 1.0, cv::Scalar::all(255));
	}

	for (auto it = objects.begin(); it != objects.end(); it++) {
		ObjectPtr object = *it;
		if (object->cid != NEMOCOVI_CLASSID_OBJECT_CIRCLE) {
			continue;
		}

		cv::Point coord = object->coord;

		//cv::circle(dst, coord, 2, cv::Scalar(0, 255, 0), 3, CV_AA);
		//cv::circle(dst, coord, object->radius, cv::Scalar(0, 255, 0), 3, CV_AA);

		ostringstream oss;
		oss << "(" << coord.x << ", " << coord.y << ")";

		coord.y = coord.y - 10;
		cv::putText(dst, oss.str(), coord, 1, 1.0, cv::Scalar::all(255));

		oss.str("");
		oss.clear();
		oss << "circle ";
		if (object->aid == NEMOCOVI_ACTIONID_ENTER) {
			oss << "enter";
		} else if (object->aid == NEMOCOVI_ACTIONID_LEAVE) {
			oss << "leave";
		} else if (object->aid == NEMOCOVI_ACTIONID_MOTION) {
			oss << "motion";
		} else {
			oss << "invalid";
		}

		coord.y = coord.y + 20;
		cv::putText(dst, oss.str(), coord, 1, 1.0, cv::Scalar::all(255));

	}

	ostringstream oss;
	oss << "current alive circle count: " << m_circleContexts.size();
	cv::putText(dst, oss.str(), cv::Point(10, 10), 1, 1.0, cv::Scalar::all(255));

	cv::imshow("Color Circle Result", dst);
}

void RealSenseVision::showObjectPersonResult(std::vector<ObjectPtr> &objects)
{
#ifdef SHOW_DEBUG_MESSAGE
	int row = 0;
	for (auto it = m_personContexts.begin(); it != m_personContexts.end(); it++) {
		ostringstream oss;
		std::vector<cv::Point> &events = (*it)->events;
		oss << "-------------------------" << endl;
		oss << "[" << row << "] ";
		for (auto eit  = events.begin(); eit != events.end(); eit++) {
			oss << *eit << ", ";
		}
		oss << endl;
	 	oss << "-------------------------" << endl;
		cout << oss.str() << endl;
		row++;
	}
#endif

	int width, height;
	m_sensor->getDepthFrameSize(width, height);

	cv::Mat colorDst(height, width, CV_8UC3, (void *) m_sensor->getColorRawFrame());
	cv::cvtColor(colorDst, colorDst, CV_BGR2RGB);
	cv::Mat depthDst = cv::Mat::zeros(height, width, CV_8UC3);

	int idx = 0;
	for (auto it = m_personContexts.begin(); it != m_personContexts.end(); it++, idx++) {
		//cv::Scalar randColor(g_rng.uniform(0, 255), g_rng.uniform(0, 255), g_rng.uniform(0, 255));
		//cv::drawContours(depthDst, (*it)->contours, (*it)->contourIdx, randColor, CV_FILLED, 8, (*it)->contourHierarchy, 0, cv::Point());
		cv::rectangle(depthDst, (*it)->bounding.tl(), (*it)->bounding.br(), (*it)->color, 3, 8, 0);
		cv::rectangle(colorDst, (*it)->bounding.tl(), (*it)->bounding.br(), (*it)->color, 3, 8, 0);

		cv::Point coord = (*it)->bounding.tl();
		coord.x = coord.x + 30;
		coord.y = coord.y + 30;

		ostringstream oss;
		oss << "(x: " << (*it)->bounding.tl().x << ", y: " << (*it)->bounding.tl().y << ")";
		cv::putText(depthDst, oss.str(), coord, 1, 1.0, cv::Scalar::all(255));
		cv::putText(colorDst, oss.str(), coord, 1, 1.0, cv::Scalar::all(255));

		oss.str("");
		oss.clear();
		coord.y = coord.y + 25;
		oss << "(w: " << (*it)->width << ", h: " << (*it)->height << ", d: " << (*it)->depth << ")";
		cv::putText(depthDst, oss.str(), coord, 1, 1.0, cv::Scalar::all(255));
		cv::putText(colorDst, oss.str(), coord, 1, 1.0, cv::Scalar::all(255));
	}

	ostringstream oss;
	oss << "current alive person count: " << m_personContexts.size();

	cv::putText(depthDst, oss.str(), cv::Point(10, 10), 1, 1.0, cv::Scalar::all(255));
	cv::putText(colorDst, oss.str(), cv::Point(10, 10), 1, 1.0, cv::Scalar::all(255));

	cv::imshow("Person Detection Result - Depth", depthDst);
	cv::imshow("Person Detection Result - Color", colorDst);


	cv::waitKey(1);
}

uint64_t RealSenseVision::getUnixTimestampNow()
{
	time_t t = std::time(0);
	return static_cast<uint64_t>(t);
}

} // namespace device
} // namespace nemocovi
