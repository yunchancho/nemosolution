#ifndef REALSENSE_VISION_H
#define REALSENSE_VISION_H

#include <vector>
#include <opencv/cv.hpp>

#include "i_vision.hpp"
#include "i_sensor.hpp"
#include "i_messanger.hpp"

#include "nemocovi.hpp"

namespace nemocovi {
namespace device {

enum class StartEdge {
	Invalid = -1,
	Top = 0,
	Bottom,
	Left,
	Right
};

struct PalmContext {
	uint32_t sid; // session unique id
	std::vector<cv::Point> events;
	cv::Scalar color;
};

struct HandContext {
	uint32_t sid; // unique id
	std::vector<std::vector<cv::Point> > events;
	StartEdge edge; // 0: top, 1: bottom, 2: left, 3: right
	cv::Scalar color;
};

struct ObjectContext {
	uint32_t sid; // session unique id
	std::vector<cv::Point> events;
	uint32_t radius;
	float angle;
	uint32_t width;
	uint32_t height;
	uint32_t depth;
	uint64_t startTime;
	uint64_t duration;
	cv::Scalar color;

	// for debugging
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> contourHierarchy;
	int contourIdx;
	cv::Rect bounding;
};

typedef std::shared_ptr<struct HandContext> HandContextPtr;
typedef std::shared_ptr<struct PalmContext> PalmContextPtr;
typedef std::shared_ptr<struct ObjectContext> ObjectContextPtr;

class RealSenseVision : public IVision {
	public:
		static IVisionPtr create(ISensorPtr sensor, IMessangerPtr messanger) {
			return IVisionPtr(new RealSenseVision(sensor, messanger));
		};

		std::vector<PalmPtr> getPalms(std::vector<NemoCoviClassId> &classes) override;
		std::vector<HandPtr> getHands(std::vector<NemoCoviClassId> &classes) override;
		std::vector<ObjectPtr> getObjects(std::vector<NemoCoviClassId> &classes) override;
		cv::Mat getRawImage(NemoCoviRawDataType type) override;
		bool tryNextFrame() override;

		// option for detail control
		bool setDepthBoundary(NemoCoviClassId classId, std::vector<int> depthes) override;
		bool setEventTrackingDistance(NemoCoviClassId classId, int distance) override;
		bool setDepthAvgRange(NemoCoviClassId classId, int range) override;

		~RealSenseVision();

	private:
		bool initialize();
		cv::Mat filterPalmFrame(uint16_t *frame);
		cv::Mat filterHandFrame(uint16_t *frame);
		cv::Mat filterObjectCircleFrame(uint8_t *frame);
		cv::Mat filterObjectPersonDepthFrame(uint16_t *frame);
		cv::Mat filterObjectPersonColorFrame(uint8_t *frame);
		void setRawImage(NemoCoviRawDataType type);
		void setPalmTouch(std::vector<PalmPtr> &palms);
		void setHandSwipes(std::vector<HandPtr> &hands);
		void setHandGrabs(std::vector<HandPtr> &hands);
		void setObjectCircles(std::vector<ObjectPtr> &objects);
		void setObjectRectangles(std::vector<ObjectPtr> &objects);
		void setObjectPersons(std::vector<ObjectPtr> &objects);
		//void setPersonDetectionData(std::vector<cv::Point> &centroids, std::vector<cv::Rect> &boundings); 
		void setPersonDetectionData(std::vector<cv::Point> &centroids, std::vector<cv::Rect> &boundings, std::vector<int> &contourIdxs, std::vector<std::vector<cv::Point> > &contours, std::vector<cv::Vec4i> &hierarchy); 

		void addEvents(std::vector<PalmPtr> &palms, PalmContextPtr context, NemoCoviClassId cid, NemoCoviActionId aid, bool changed = true);
		void addEvents(std::vector<HandPtr> &hands, HandContextPtr context, NemoCoviClassId cid, NemoCoviActionId aid, bool changed = true);
		void addEvents(std::vector<ObjectPtr> &objects, ObjectContextPtr context, NemoCoviClassId cid, NemoCoviActionId aid, bool changed = true);

		cv::Point findContinuousEvents(std::vector<cv::Point> &newFoundPoints, cv::Point target, uint32_t threshold = 50);
		cv::Vec3f findContinuousEvents(std::vector<cv::Vec3f> &newFoundPoints, cv::Point target, uint32_t threshold = 50);

		std::vector<cv::Point> findCentroids(cv::Mat &matrix, int approxAreaLimit);
		int getDummyEventsCount(std::vector<cv::Point> &events);
		void removeDummyEvents(std::vector<cv::Point> &events);
		int getDefectSwipeAngle(cv::Point start, cv::Point end, cv::Point center);

		void showPalmResult(std::vector<PalmPtr> &palms);
		void showHandSwipeResult(std::vector<HandPtr> &hands);
		void showObjectCircleResult(std::vector<ObjectPtr> &objects);
		void showObjectPersonResult(std::vector<ObjectPtr> &objects);

		uint64_t getUnixTimestampNow();

		explicit RealSenseVision(ISensorPtr sensor, IMessangerPtr messanger);

		std::vector<PalmContextPtr> m_palmContexts;
		std::vector<HandContextPtr> m_swipeContexts;
		std::vector<ObjectContextPtr> m_circleContexts;
		std::vector<ObjectContextPtr> m_personContexts;
		std::vector<std::vector<cv::Vec3f> > m_circleSamples;
		cv::Mat m_depthRawMatrix;
		cv::Mat m_colorRawMatrix;

		int m_circleSamplingCount;
		ISensorPtr m_sensor;
		IMessangerPtr m_messanger;

		std::map<NemoCoviClassId, std::vector<int> > m_depthBoundaryMap;
		std::map<NemoCoviClassId, int> m_eventTrackingDistanceMap;
		std::map<NemoCoviClassId, int> m_depthAvgRangeMap;

};

} // namespace devicee
} // namespace nemocovi

#endif //REALSENSE_VISION_H
