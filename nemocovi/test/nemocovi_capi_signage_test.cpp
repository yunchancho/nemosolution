#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <json-c/json.h>
#include <nemocovi.h>
#include <nemocovi.hpp>
#include <opencv/cv.h>
#include <opencv/highgui.h>

#define BUFF_SIZE 1024

void print_objects(struct nemocovi_object *events, int count)
{
	for (int i = 0; i < count; i++) {
		if (events[i].aid != NEMOCOVI_ACTIONID_LEAVE) {
			continue;
		}

		if (events[i].depth > 1000) {
			continue;
		} 

		if (events[i].duration < 10) {
			continue;
		} 

		printf("Person Object\n");
		printf("sid: %d, cid: %d, aid: %d\n", events[i].sid, events[i].cid, events[i].aid);
		printf("norm_x: %.3f, norm_y: %.3f\n", events[i].norm_x, events[i].norm_y);
		printf("norm_w: %.3f, norm_h: %.3f, depth: %d\n", events[i].norm_width, events[i].norm_height, events[i].depth );
		printf("start_time: %lu, duration: %lu\n", events[i].start_time, events[i].duration);
	}
}

struct covi_option {
	int depth_boundaries[10];
	int depth_boundaries_count;
	int tracking_distance;
	int depth_avg_range;
	int framerate;
};

static void set_nemocovi_option(const char* json_path, struct covi_option *option)
{
	int i, fd;
	char buff[BUFF_SIZE];
	json_object *jobj, *tmp;

	fd = open(json_path, O_RDONLY);
	if (fd > 0) {
		printf("set meta file (.json)\n");
		read(fd, buff, BUFF_SIZE);
		jobj = json_tokener_parse(buff);

		json_object_object_foreach(jobj, key, val) {
			if (!strcmp(key, "depthBoundaries")) {
				for (i = 0; i < json_object_array_length(val); i++) {
					tmp = json_object_array_get_idx(val, i);
					option->depth_boundaries[i] = json_object_get_int(tmp); 
					printf("set depth boundaries[%d]: %d\n", i, option->depth_boundaries[i]);
				}
				option->depth_boundaries_count = json_object_array_length(val);
			} else if(!strcmp(key, "trackingMaxDistance")) {
				option->tracking_distance = json_object_get_int(val);
				printf("set tracking max distance: %d\n", option->tracking_distance);
			} else if(!strcmp(key, "personCenterRange")) {
				option->depth_avg_range = json_object_get_int(val);
				printf("set person center range: %d\n", option->depth_avg_range);
			} else if(!strcmp(key, "framerate")) {
				option->framerate = json_object_get_int(val);
				printf("set frame rate: %d\n", option->framerate);
			}
		}
		close(fd);
	} 
}

int main(int argc, char *argv[])
{
	struct covi_option option = {
		{300, 1000, 3500}, 3, 120, 20, 30
	};

	struct nemocovi_object *objects = NULL;
	struct nemocovi_usedclass usedclass; 
	int objects_count;

	if (argc == 2) {
		set_nemocovi_option(argv[1], &option);
	}

	nemocovi_handle *handle = nemocovi_create(NEMOCOVI_APP_SIGNAGE, NEMOCOVI_DEVICE_REALSENSE);

	if (!handle) {
		printf("failed to get nemocovi handle\n");
	}

	usedclass.classes[0] = NEMOCOVI_CLASSID_OBJECT_PERSON;

	// set options
	nemocovi_set_depth_boundary(handle, NEMOCOVI_CLASSID_OBJECT_PERSON, option.depth_boundaries, option.depth_boundaries_count);
	nemocovi_set_event_tracking_distance(handle, NEMOCOVI_CLASSID_OBJECT_PERSON, option.tracking_distance);
	nemocovi_set_depth_avg_range(handle, NEMOCOVI_CLASSID_OBJECT_PERSON, option.depth_avg_range);
	nemocovi_set_frame_rate(handle, option.framerate);

	while(nemocovi_try_next_frame(handle) >= 0) {
		nemocovi_free_object_events(objects);

		objects = nemocovi_get_object_events(handle, &usedclass, &objects_count);

		printf("------------------------------------\n");
		print_objects(objects, objects_count);
		printf("------------------------------------\n\n\n");

		CvMat tmp = nemocovi_get_opencv_matrix(handle, NEMOCOVI_RAWDATA_COLOR);
		cv::Mat tmp2 = nemocovi::getOpenCvMatrix(handle, NEMOCOVI_RAWDATA_COLOR); 

		//cv::imshow("Raw Color Data (C ver)", cv::cvarrToMat(&tmp));
		//cv::imshow("Raw Color Data (C++ ver)", tmp2);
		//cv::waitKey(1);
	}

	return 0;
}
