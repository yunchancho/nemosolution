#include <stdio.h>
#include <stdlib.h>
#include <nemocovi.h>
#include <nemocovi.hpp>
#include <opencv/cv.h>
#include <opencv/highgui.h>

void print_palms(struct nemocovi_palm *events, int count)
{
	for (int i = 0; i < count; i++) {
		printf("PALM\n");
		printf("sid: %d, cid: %d, aid: %d\n", events[i].sid, events[i].cid, events[i].aid);
		printf("norm_x: %.2f, norm_y: %.2f\n", events[i].norm_x, events[i].norm_y);
	}
}

void print_hands(struct nemocovi_hand *events, int count)
{
	for (int i = 0; i < count; i++) {
		printf("HAND\n");
		printf("sid: %d, cid: %d, aid: %d\n", events[i].sid, events[i].cid, events[i].aid);
		printf("norm_x: %.2f, norm_y: %.2f\n", events[i].norm_x, events[i].norm_y);
		printf("norm_x1: %.2f, norm_y1: %.2f\n", events[i].norm_x1, events[i].norm_y1);
	}
}

void print_objects(struct nemocovi_object *events, int count)
{
	for (int i = 0; i < count; i++) {
		printf("OBJECT\n");
		printf("sid: %d, cid: %d, aid: %d\n", events[i].sid, events[i].cid, events[i].aid);
		printf("norm_x: %.2f, norm_y: %.2f\n", events[i].norm_x, events[i].norm_y);
		printf("norm_w: %.2f, norm_h: %.2f\n", events[i].norm_width, events[i].norm_height);
	}
}

int main(int argc, char *argv[])
{
	struct nemocovi_palm *palms = NULL;
	struct nemocovi_hand *hands = NULL;
	struct nemocovi_object *objects = NULL;
	struct nemocovi_usedclass usedclass;

	int palms_count, hands_count, objects_count;

	nemocovi_handle *handle = nemocovi_create(NEMOCOVI_APP_BEAM, NEMOCOVI_DEVICE_REALSENSE);

	if (!handle) {
		printf("failed to get nemocovi handle\n");
	}

	usedclass.classes[0] = NEMOCOVI_CLASSID_PALM_TOUCH;
	usedclass.classes[1] = NEMOCOVI_CLASSID_HAND_SWIPE;
	usedclass.classes[2] = NEMOCOVI_CLASSID_OBJECT_CIRCLE;
	usedclass.classes[3] = NEMOCOVI_CLASSID_OBJECT_PERSON;

	while(nemocovi_try_next_frame(handle) >= 0) {
		nemocovi_free_palm_events(palms);
		nemocovi_free_hand_events(hands);
		nemocovi_free_object_events(objects);

		palms = nemocovi_get_palm_events(handle, &usedclass, &palms_count);
		hands = nemocovi_get_hand_events(handle, &usedclass, &hands_count);
		objects = nemocovi_get_object_events(handle, &usedclass, &objects_count);

		printf("------------------------------------\n");
		print_palms(palms, palms_count);
		printf("------------------------------------\n");
		print_hands(hands, hands_count);
		printf("------------------------------------\n");
		print_objects(objects, objects_count);
		printf("------------------------------------\n\n\n");

		CvMat tmp = nemocovi_get_opencv_matrix(handle, NEMOCOVI_RAWDATA_COLOR);
		cv::Mat tmp2 = nemocovi::getOpenCvMatrix(handle, NEMOCOVI_RAWDATA_COLOR); 

		cv::imshow("Raw Color Data (C ver)", cv::cvarrToMat(&tmp));
		cv::imshow("Raw Color Data (C++ ver)", tmp2);
		cv::waitKey(1);
	}

	return 0;
}
