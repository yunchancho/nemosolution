#include <cstdio>
#include <stdint.h>
#include <list>
#include <vector>
#include <limits>
#include <string>
#include <iostream>
#include <fstream>

#include <librealsense/rs.hpp>
#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace std;
using namespace cv;

struct swipe_context;

static int thresh = 100;
static int max_thresh = 255;
static int touch_gap = 15; // millimeter
static int depth_deviate = 5; // millimeter
static int centroid_neighbor_size = 10; // millimeter
static cv::RNG rng(12345);
static uint16_t *baselines[10];
static std::list<uint16_t *> used_frame_list;
static std::list<uint16_t *> recent_frame_list;
static std::vector<std::vector<cv::Point> > touch_vector;
static std::vector<struct swipe_context *> swipe_vector;
static ostringstream touchup_oss;
static ostringstream touchdown_oss;
static ostringstream touchmove_oss;

static ostringstream swipeenter_oss;
static ostringstream swipeleave_oss;
static ostringstream swipemove_oss;

extern void detectFingerTip(Mat &frame, vector<vector<cv::Point> > &contours);

enum class SwipeEdge {
	Invalid = -1,
	Top = 0,
	Bottom,
	Left,
	Right
};

struct app_context {
	rs::device *rsdev;
	uint16_t bottom_depth;
	uint16_t baseline[360][480];
	std::list<uint16_t *> used_frame_list;
	std::list<uint16_t *> recent_frame_list;
};

struct swipe_context {
	uint32_t dir; // 0: left, 1: right
	uint32_t id; // unique id
	std::vector<cv::Point> history;
	SwipeEdge edge; // 0: top, 1: bottom, 2: left, 3: right
	cv::Scalar color;
};

static uint16_t beam_get_average_bottom_depth(struct app_context *app, uint16_t *depth_image)
{
	rs::device *dev = app->rsdev;
  rs::intrinsics depth_intrin = dev->get_stream_intrinsics(rs::stream::depth);

	int cx = depth_intrin.width / 2;
	int cy = depth_intrin.height / 2;


	uint32_t sum = 0, count = 0;

	for (int dy = cy - 5; dy < cy + 5; dy++) {
		for (int dx = cx - 5; dx < cx + 5; dx++) {
			if (depth_image[dy * depth_intrin.width + dx]) {
				uint16_t depth_value = depth_image[dy * depth_intrin.width + dx];
				sum += depth_value;
				count++;
			}
		}
	}

	uint16_t avg = 1003;

	if (count) {
		avg = sum / count;
	}

	cout << "bottom depth average: " << avg << ", count: " << count << endl;;
	
	return avg;
}

int rs_init(struct app_context *app)
{
	rs::log_to_console(rs::log_severity::warn);

	rs::device *dev = app->rsdev;
	printf("\nUsing device 0, an %s\n", dev->get_name());
	printf("    Serial number: %s\n", dev->get_serial());
	printf("    Firmware version: %s\n", dev->get_firmware_version());

	rs::apply_depth_control_preset(dev, 1);

	dev->set_option(rs::option::r200_lr_gain, 100);
	dev->set_option(rs::option::r200_lr_exposure, 164);

	dev->set_option(rs::option::r200_emitter_enabled, 1);


	//dev->set_option(rs::option::r200_auto_exposure_mean_intensity_set_point, 512);
	//dev->set_option(rs::option::r200_auto_exposure_bright_ratio_set_point, 0.2);
	//dev->set_option(rs::option::r200_auto_exposure_kp_gain, 0.0025);
	//dev->set_option(rs::option::r200_auto_exposure_kp_exposure, 0.0025);
	//dev->set_option(rs::option::r200_auto_exposure_kp_dark_threshold, 10);
	//dev->set_option(rs::option::r200_auto_exposure_top_edge, 0);
	//dev->set_option(rs::option::r200_auto_exposure_bottom_edge, 65535);
	//dev->set_option(rs::option::r200_auto_exposure_left_edge, 0);
	//dev->set_option(rs::option::r200_auto_exposure_right_edge, 65535);

	dev->set_option(rs::option::r200_depth_control_estimate_median_decrement, 0);
	dev->set_option(rs::option::r200_depth_control_estimate_median_increment, 0);
	dev->set_option(rs::option::r200_depth_control_median_threshold, 0);

	dev->set_option(rs::option::r200_depth_control_score_minimum_threshold, 0);
	dev->set_option(rs::option::r200_depth_control_score_maximum_threshold, 1023);
	dev->set_option(rs::option::r200_depth_control_texture_count_threshold, 0);
	dev->set_option(rs::option::r200_depth_control_texture_difference_threshold, 0);
	dev->set_option(rs::option::r200_depth_control_second_peak_threshold, 0);
	dev->set_option(rs::option::r200_depth_control_neighbor_threshold, 0);
	dev->set_option(rs::option::r200_depth_control_lr_threshold, 2047);

	dev->enable_stream(rs::stream::depth, 480, 360, rs::format::z16, 30);
	dev->enable_stream(rs::stream::color, 640, 480, rs::format::rgb8, 30);

  dev->start();

	// discard initial some frames, which might be filled with dummy value
	for (int i = 0; i < 30; ++i) {
		dev->wait_for_frames();
	}

	return 0;
}

int opencv_init(struct app_context *app)
{
	return 0;
}

void drawOrigin(const uint16_t *depth_image, const uint8_t *color_image)
{
	cv::Mat depth16(360, 480, CV_16U, (void *)depth_image);
	cv::Mat depthM;
	depth16.convertTo(depthM, CV_8UC1);
	cv::threshold(depthM, depthM, thresh, 255, THRESH_BINARY);
	cv::medianBlur(depthM, depthM, 9);

	/*
	unsigned short min = 0.5, max = 2.5;
	cv::Mat img0 = cv::Mat::zeros(depthM.size().height, depthM.size().width, CV_8UC1);
	cv::Mat depth_show;
	double scale_ = 255.0 / (max - min);
	depthM.convertTo(img0, CV_8UC1, scale_);
	*/

	cv::imshow("Depth", depthM);
	int key = cv::waitKey(1);
}

void beam_update_used_frame_history(struct app_context *app, uint16_t *depth_image)
{
	if (!used_frame_list.empty()) {
		used_frame_list.pop_back();
	}
	used_frame_list.push_front(depth_image);
}

uint16_t beam_get_used_frame_depth(struct app_context *app, int dx, int dy)
{
	rs::device *dev = app->rsdev;
	rs::intrinsics depth_intrin = dev->get_stream_intrinsics(rs::stream::depth);
	uint16_t depth = 0;

	for (list<uint16_t*>::iterator it = used_frame_list.begin(); it != used_frame_list.end(); it++) {
		uint16_t *tmp = *it;
		if (!tmp[dy * depth_intrin.width + dx]) {
			continue;
		}
		depth = tmp[dy * depth_intrin.width + dx];
		break;
	}

	return depth;
}

int beam_get_defect_angle(cv::Point start, cv::Point end, cv::Point center)
{
	float angle;

	cv::Point cs = center - start;
	cv::Point ce = center - end;

	float angcs = atan2(cs.y, cs.x);
	float angce = atan2(ce.y, ce.x);
	float diff = angcs - angce;

	angle = ((diff * 180) / CV_PI);
	return angle > 0 ? angle : 360 + angle;
}

int beam_get_defect_swipe_angle(cv::Point start, cv::Point end, cv::Point center)
{
	float angle;

	cv::Point cs = center - start;
	cv::Point ce = center - end;

	float angcs = atan2(cs.y, cs.x);
	float angce = atan2(ce.y, ce.x);
	float diff = angcs - angce;

	angle = ((diff * 180) / CV_PI);
	return angle;
}

uint16_t* beam_get_noise_removed_touch_frame(struct app_context *app, uint16_t *orig_frame)
{
	rs::device *dev = app->rsdev;

	rs::intrinsics depth_intrin = dev->get_stream_intrinsics(rs::stream::depth);
	std::size_t memsize = depth_intrin.height * depth_intrin.width * sizeof(uint16_t);

	uint16_t *depth_image = (uint16_t *) malloc(memsize);
	std::memcpy(depth_image, orig_frame, memsize);

	for(int dy = 0; dy < depth_intrin.height; dy++) {
		for(int dx = 0; dx < depth_intrin.width; dx++) {
			if (app->baseline[dy][dx] - depth_image[dy * depth_intrin.width + dx] - depth_deviate * 2 <= 0) {
				//printf("depth_point.z: %f\n", depth_point.z);
				depth_image[dy * depth_intrin.width + dx] = 0x0000;
			} else if (app->baseline[dy][dx] - depth_image[dy * depth_intrin.width + dx] - depth_deviate * 4 <= 0)  { 
				//depth_image[dy * depth_intrin.width + dx] = 0x0000;
			} else {
				depth_image[dy * depth_intrin.width + dx] = 0x0000;
			}
		}
	}

	return depth_image;
}

uint16_t* beam_get_noise_removed_gesture_frame(struct app_context *app, uint16_t *orig_frame)
{
	rs::device *dev = app->rsdev;

	rs::intrinsics depth_intrin = dev->get_stream_intrinsics(rs::stream::depth);
	std::size_t memsize = depth_intrin.height * depth_intrin.width * sizeof(uint16_t);

	uint16_t *depth_image = (uint16_t *) malloc(memsize);
	std::memcpy(depth_image, orig_frame, memsize);

	for(int dy = 0; dy < depth_intrin.height; dy++) {
		for(int dx = 0; dx < depth_intrin.width; dx++) {
			if (app->baseline[dy][dx] - depth_image[dy * depth_intrin.width + dx] - depth_deviate * 4 <= 0) {
				//printf("depth_point.z: %f\n", depth_point.z);
				depth_image[dy * depth_intrin.width + dx] = 0x0000;
			}
		}
	}

	return depth_image;
}

uint8_t* beam_get_noise_removed_shape_frame(struct app_context *app, uint8_t *orig_frame)
{
	rs::device *dev = app->rsdev;

	rs::intrinsics color_intrin = dev->get_stream_intrinsics(rs::stream::color);
	std::size_t memsize = color_intrin.height * color_intrin.width * sizeof(uint8_t) * 3;

	uint8_t *color_image = (uint8_t *) malloc(memsize);
	std::memcpy(color_image, orig_frame, memsize);

	for(int dy = 0; dy < color_intrin.height; dy++) {
		for(int dx = 0; dx < color_intrin.width; dx++) {
			// if we need to handle, write here
		}
	}

	return color_image;
}

cv::Mat beam_get_filtered_touch_frame_matrix(struct app_context *app, uint16_t *depth_image)
{
	cv::Mat output, output2;
	cv::Mat src(360, 480, CV_16U, (void *)depth_image);
	//cv::Mat src(depth16.size().height, depth16.size().width, CV_8UC1);

	//output = cv::Mat::zeros(360, 480, CV_8UC1);
	src.convertTo(output2, CV_8UC1);
	//src.convertTo(output2, CV_8UC1);
	//cv::Mat element3 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5));
	//cv::Mat element2(3, 3, CV_8U, cv::Scalar(1));
	//cv::fastNlMeansDenoising(output, output2, 10, 10, 7, 21);
	//cv::GaussianBlur(output2, output2, cv::Size(5, 5), 5);
	//cv::threshold(output2, output2, 1, 255, THRESH_BINARY);
	//cv::Mat element1(2, 2, CV_8U, cv::Scalar(1));
	//cv::morphologyEx(output2, output2, cv::MORPH_OPEN, element1);
	//cv::medianBlur(output2, output2, 11);
	//cv::Canny(output2, output2, 2, 2 * 3, 3);
	//cv::equalizeHist(output2, output2);
	//cv::morphologyEx(output2, output2, cv::MORPH_DILATE, element2);

	//cv::Mat element3 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5));

	/*
	cv::Mat element1(1, 1, CV_8U, cv::Scalar(1));
	cv::morphologyEx(output, output, cv::MORPH_ERODE, element1);
	cv::Mat element2(3, 3, CV_8U, cv::Scalar(1));
	cv::Mat element3 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5));
	cv::morphologyEx(output, output, cv::MORPH_DILATE, element2);
	*/
	//cv::GaussianBlur(output, output, cv::Size(7, 7), 3);

	//cv::blur(output, output, cv::Size(5, 5));
	//cv::threshold(output, output, 1, 255, THRESH_BINARY);
	//cv::medianBlur(output, output, 11);
	//cv::bilateralFilter(output, output, 3, 80, 80);
	//cv::erode(output, output, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5,5), Point(0,0)));
	//cv::dilate(depthImage, depthImage, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(1,1), Point(0,0)));
	//cv::dilate(output, output, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5,5), Point(0,0)));
	//cv::GaussianBlur(output, output, cv::Size(5, 5), 3);
	//cv::blur(output, output, cv::Size(9, 9));
	//cv::GaussianBlur(depthImage, depthImage, cv::Size(5, 5), 3);
	//cv::addWeighted(depthImage, depthImage,
	//cv::dilate(depthImage, depthImage, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(1,1), Point(0,0)));

	/*
	cv::Mat element(7, 7, CV_8U, cv::Scalar(1));
	cv::morphologyEx(depthImage, depthImage, cv::MORPH_CLOSE, element);
	*/
	//cv::erode(depthImage, depthMorph, element);
	//cv::morphologyEx(output, output, cv::MORPH_OPEN, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(360,480)));
	//cv::erode(depthImage, depthErode, cv::Mat(), cv::Point(-1, -1), 1);
	//cv::threshold(depthImage, depthImage, 10, 255, THRESH_BINARY);
	//cv::dilate(depthImage, depthImage, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(1,1), Point(0,0)));
	//cv::blur(depthImage, depthEdges, cv::Size(3, 3));
	//cv::Canny(depthEdges, depthEdges, 2, 2 * 3, 3);

	/*
	depthImageResult = Scalar::all(0);
	depthImage.copyTo(depthImageResult, depthMorph);
	*/
	cv::imshow("filtered touch image", output2);
	cv::waitKey(1);

	return output2.clone();
}

cv::Mat beam_get_filtered_gesture_frame_matrix(struct app_context *app, uint16_t *depth_image)
{
	cv::Mat output, output2;
	cv::Mat src(360, 480, CV_16U, (void *)depth_image);
	src.convertTo(output2, CV_8UC1);
	cv::Mat element1(9, 9, CV_8U, cv::Scalar(1));
	//cv::morphologyEx(output2, output2, cv::MORPH_OPEN, element1);
	cv::morphologyEx(output2, output2, cv::MORPH_CLOSE, element1);
	//cv::medianBlur(output2, output2, 11);
	//cv::GaussianBlur(output2, output2, cv::Size(5, 5), 3);
	//cv::threshold(output2, output2, 1, 255, THRESH_BINARY);
	cv::imshow("filtered gesture image", output2);
	cv::waitKey(1);

	return output2.clone();
}

void beam_get_touch_points(struct app_context *app, cv::Mat& matrix, uint16_t *depth_image)
{
	vector<vector<cv::Point> > contours;
	vector<cv::Vec4i> hierarchy;

	rs::device *dev = app->rsdev;
	rs::intrinsics depth_intrin = dev->get_stream_intrinsics(rs::stream::depth);

	cv::findContours(matrix.clone(), contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));


	cv::Mat dst = cv::Mat::zeros(matrix.size(), CV_8UC3);
	cv::Mat approx;

	std::vector<cv::Point> centroids;

	for (int i = 0; i < contours.size(); i++) {
		if (cv::contourArea(contours[i]) >= 50 && cv::contourArea(contours[i]) <= 1000) {
			cv::Scalar color(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));

			vector<vector<cv::Point> > tcontours;
			tcontours.push_back(contours[i]);
			//cv::drawContours(dst, tcontours, -1, cv::Scalar(0, 0, 255), 2);

			vector<vector<cv::Point> > hulls(1);
			cv::convexHull(cv::Mat(tcontours[0]), hulls[0], false);
			vector<cv::Point> hullContour;
			cv::approxPolyDP(cv::Mat(hulls[0]), hullContour, 0.001, true);

			if (std::fabs(contourArea(cv::Mat(hullContour))) > 300) {
				cv::drawContours(dst, contours, (int) i, color, CV_FILLED, 8, hierarchy, 0, cv::Point());
				cv::drawContours(dst, hulls, -1, cv::Scalar(0, 255, 0), 2);
				// get cetnroid of contour
				cv::Moments mu = cv::moments(contours[i], false);
				cv::Point mc = cv::Point(mu.m10/mu.m00, mu.m01/mu.m00);
				cv::circle(dst, mc, 5, cv::Scalar(255, 255, 255), -1);

				centroids.push_back(mc);
			}
		}
	}

	// JUST for print
	for (auto it = touch_vector.begin(); it != touch_vector.end(); it++) {
		std::vector<cv::Point> &events = *it;
		cv::circle(dst, events.front(), 5, cv::Scalar(0, 255, 255), -1);
	}

	if (centroids.size() > 0) {
		for (auto it = touch_vector.begin(); it != touch_vector.end(); ) {
			std::vector<cv::Point> &events = *it;
			
			cv::Point point(-1, -1);

			for (auto eit = events.crbegin(); eit != events.crend(); eit++) {
				point = *eit;
				if ((point.x == -1) && (point.y == -1)) {
					continue;
				}

				break;
			}

			bool found = false;
			cv::Point mc;
			for (auto cit = centroids.begin(); cit != centroids.end(); ) {
				mc = *cit;

				int dx = mc.x - point.x;
				int dy = mc.y - point.y;

				if (sqrt(dx * dx + dy * dy) < 50) {
					if (!found) {
						events.push_back(mc);
						cit = centroids.erase(cit);
						found = true;
					} else {
						cit = centroids.erase(cit);
					}
					continue;
					//break;
				} 
				cit++;
			}

			if (!found) {
				ostringstream oss;
				for (auto cit = centroids.begin(); cit != centroids.end(); cit++) {
					oss << *cit << ", ";
				}

				cout << "centroid not found: " << oss.str() << endl;
				cout << "compared point: " << point << endl;

				events.push_back(cv::Point(-1, -1));
				int dummyCount = 0;
				for (auto eit = events.crbegin(); eit != events.crend(); eit++) {
					cv::Point point = *eit;
					if ((point.x == -1) && (point.y == -1)) {
						dummyCount++;
						continue;
					}
					break;
				}

				if (dummyCount == 10) {
					cout << "touch up: " << events.front() << endl;

					touchmove_oss.str("");
					touchup_oss.str("");
					touchup_oss << "latest touch up: " << events.front();
					it = touch_vector.erase(it);
				} else {
					it++;
				}
			} else {
				cout << "centroid found: " << mc << "(" << point << ")" << endl;

				int dx = mc.x - events.front().x;
				int dy = mc.y - events.front().y;

				if (sqrt(dx * dx + dy * dy) > 25) {
					events.front() = mc;
					touchup_oss.str("");
					touchmove_oss.str("");
					touchmove_oss << "latest touch move: " << mc;

					for (auto eit = events.begin(); eit != events.end(); eit++) {
						if (*eit != cv::Point(-1, -1)) {
							cv::circle(dst, *eit, 5, cv::Scalar(0, 255, 255), -1);
						}
					}
				}
				it++;
			}

			//break;
		}
	} else {
		for (auto it = touch_vector.begin(); it != touch_vector.end(); ) {
			std::vector<cv::Point> &events = *it;
			events.push_back(cv::Point(-1, -1));

			// check if this is for mouse up
			int dummyCount = 0;
			for (auto eit = events.crbegin(); eit != events.crend(); eit++) {
				cv::Point point = *eit;
				if ((point.x == -1) && (point.y == -1)) {
					dummyCount++;
					continue;
				}
				break;
			}

			if (dummyCount == 10) {
				cout << "touch up: " << events.front() << endl;

				touchmove_oss.str("");
				touchup_oss.str("");
				touchup_oss << "latest touch up: " << events.front();
				it = touch_vector.erase(it);
			} else {
				it++;
			}
		}
	}

	// add new event vector if there is unhandled centroids
	if (centroids.size() > 0) {
		for (auto cit = centroids.begin(); cit != centroids.end(); cit++) {
			std::vector<cv::Point> newEvents;
			newEvents.push_back(*cit);
			touch_vector.push_back(newEvents);
			cout << "touch down: " << *cit << endl;

			touchdown_oss.str("");
			touchup_oss.str("");
			touchmove_oss.str("");
			touchdown_oss << "lastest touch down: " << newEvents.front();
		}
	}

	int row = 0;
	for (auto it = touch_vector.begin(); it != touch_vector.end(); it++) {
		ostringstream oss;
		std::vector<cv::Point> &events = *it;
		oss << "-------------------------" << endl;
		oss << "[" << row << "] ";
		for (auto eit  = events.begin(); eit != events.end(); eit++) {
			oss << *eit << ", ";
		}
		oss << endl;
	 	oss << "-------------------------" << endl;
		cout << oss.str() << endl;
		row++;
	}

	ostringstream oss;
	oss << "current touch down count: " << touch_vector.size();
	cv::putText(dst, oss.str(), cv::Point(10, 10), 1, 1.0, Scalar::all(255));
	cv::putText(dst, touchdown_oss.str(), cv::Point(10, 30), 1, 1.0, Scalar::all(255));
	cv::putText(dst, touchup_oss.str(), cv::Point(10, 50), 1, 1.0, Scalar::all(255));
	cv::putText(dst, touchmove_oss.str(), cv::Point(10, 50), 1, 1.0, Scalar::all(255));

//	detectFingerTip(dst, contours);
	cv::imshow("Depth Touch Contours", dst);
}

void beam_get_gestures(struct app_context *app, cv::Mat& matrix, uint16_t *depth_image)
{
	vector<vector<cv::Point> > contours;
	vector<cv::Vec4i> hierarchy;

	rs::device *dev = app->rsdev;
	rs::intrinsics depth_intrin = dev->get_stream_intrinsics(rs::stream::depth);
	cv::Mat dst = cv::Mat::zeros(matrix.size(), CV_8UC3);

	cv::findContours(matrix.clone(), contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));

	std::vector<cv::Point> tips;
	std::vector<SwipeEdge> tipEdges;
	SwipeEdge edge = SwipeEdge::Invalid;

	for (int i = 0; i < contours.size(); i++) {
		if (cv::contourArea(contours[i]) >= 3000) {

			vector<vector<cv::Point> > tcontours;
			tcontours.push_back(contours[i]);
			cv::drawContours(dst, tcontours, -1, cv::Scalar(0, 0, 255), 2);

			vector<vector<cv::Point> > hulls(1);
			vector<vector<int> > hullsI(1);
			cv::convexHull(cv::Mat(tcontours[0]), hulls[0], false);
			cv::convexHull(cv::Mat(tcontours[0]), hullsI[0], false);

			vector<vector<cv::Vec4i> >convexDefects(contours.size());
			cv::convexityDefects(tcontours[0], hullsI[0], convexDefects[i]);

			vector<cv::Point> hullContour;
			cv::approxPolyDP(cv::Mat(hulls[0]), hullContour, 0.001, true);

			if (std::fabs(contourArea(cv::Mat(hullContour))) > 3000) {
				cv::Scalar color(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
				//cv::drawContours(dst, contours, (int) i, color, CV_FILLED, 8, hierarchy, 0, cv::Point());
				cv::drawContours(dst, hulls, -1, cv::Scalar(0, 255, 0), 2);
				std::vector<cv::Point> &hull = hulls[0];

				cv::Point bottom = cv::Point(-1, -1);

				for (int i = 0; i < hull.size(); i++) {
					//cout << "hull[i]: " << hull[i] << endl;
					if ((hull[i].x >= 0) && (hull[i].x <= 35)) {
						for (int j = i + 1; j < hull.size(); j++) {
							if (hull[j].x >= (hull[i].x - 5) && hull[j].x <= (hull[i].x + 5)) {
								bottom = cv::Point((hull[i].x + hull[j].x) / 2, (hull[i].y + hull[j].y) / 2);
								edge = SwipeEdge::Left;
								break;
							}
						}
						break;
					} else if (((hull[i].x >= (depth_intrin.width - 10)) && (hull[i].x <= depth_intrin.width))) {
						for (int j = i + 1; j < hull.size(); j++) {
							if (hull[j].x >= (hull[i].x - 5) && hull[j].x <= (hull[i].x + 5)) {
								bottom = cv::Point((hull[i].x + hull[j].x) / 2, (hull[i].y + hull[j].y) / 2);
								edge = SwipeEdge::Right;
								break;
							}
						}
						break;
					} else if ((hull[i].y >= 0) && (hull[i].y <= 10)) {
						for (int j = i + 1; j < hull.size(); j++) {
							if (hull[j].y >= (hull[i].y - 5) && hull[j].y <= (hull[i].y + 5)) {
								bottom = cv::Point((hull[i].x + hull[j].x) / 2, (hull[i].y + hull[j].y) / 2);
								edge = SwipeEdge::Top;
								break;
							}
						}
						break;
					} else if ((hull[i].y >= (depth_intrin.height - 10)) && (hull[i].y <= depth_intrin.height)) {
						for (int j = i + 1; j < hull.size(); j++) {
							if (hull[j].y >= (hull[i].y - 5) && hull[j].y <= (hull[i].y + 5)) {
								bottom = cv::Point((hull[i].x + hull[j].x) / 2, (hull[i].y + hull[j].y) / 2);
								edge = SwipeEdge::Bottom;
								break;
							}
						}
						break;
					}
				}

				if (bottom == cv::Point(-1, -1)) {
					continue;
				} 

				int max_distance = 0;
				cv::Point point;

				for (auto it = hull.begin(); it != hull.end(); it++) {
					int dx = bottom.x - (*it).x;
					int dy = bottom.y - (*it).y;

					int distance = sqrt(dx * dx + dy * dy);
					if (distance > max_distance) {
						point = *it;
						max_distance = distance;
					}
				}

				cv::circle(dst, point, 5, cv::Scalar(255, 255, 255), -1);
				tips.push_back(point);
				tipEdges.push_back(edge);
			}
		}
	}

	// recognize tips(end finger tip) is for gesture
	// 0. if there is no the tips point, invalid point (-1,-1) is added into every swipe histories. 
	// 1. find vertical points 
	// 2. get angle and compare direction to existing one to be same
	// 3. check if angle is in threshold angle, or not.
	// . if so, add new point into history, and emit swipe event (move)
	// . if not so, emit swipe event (up) and remove existing history
	// . we also should check if the point is on 4 side edge. if so, we should do the step above.
	// 4. if any tips is not include in this swipe history, add invalid point (-1,-1) 
	// 5. check if this swipe history is ended. if so, emit swipe up event and remove the history
	// 6. if vertical point is new, create new swipe history, and emit swipe down event
	if (tips.size() > 0) {
		cout << "tips size: " << tips.size() << endl;
		for (auto it = swipe_vector.begin(); it != swipe_vector.end(); ) {
			cout << "swipe vector size: " << swipe_vector.size() << endl;
			struct swipe_context *context = *it;
			
			int point_count = 0;
			cv::Point point = cv::Point(-1, -1);
			for (auto hit = context->history.cbegin(); hit != context->history.cend(); hit++) {
				cv::Point tmp = *hit;
				if ((tmp.x == -1) && (tmp.y == -1)) {
					continue;
				}

				point = *hit;
				point_count++;
			}

			bool found = false;
			cv::Point mc;

			auto tit = tips.begin();
		 	auto eit = tipEdges.begin();
			while(tit != tips.end()) {
				mc = *tit;

				int dx = mc.x - point.x;
				int dy = mc.y - point.y;
				int angle;
				int distance = sqrt(dx * dx + dy * dy);

				if ((distance > 0) && (distance < 180)) {
					cv::Point vpoint;
					if ((context->edge == SwipeEdge::Top) || (context->edge == SwipeEdge::Bottom)) {
						vpoint.x = mc.x;
						vpoint.y = point.y;
						angle = beam_get_defect_swipe_angle(mc, vpoint, point);
					} else {
						vpoint.x = mc.x;
						vpoint.y = point.y;
						angle = beam_get_defect_swipe_angle(point, vpoint, mc);
					}

					if (abs(angle) < 30) {
						if (!found) {
							context->history.push_back(mc);
							for (auto hit = context->history.begin(); hit != context->history.end(); hit++) {
								if (*hit != cv::Point(-1, -1)) {
									cv::circle(dst, *hit, 5, context->color, -1);
								}
							}

							if (point_count > 1) {
								//TODO we should send enter event first 
								cout << "swipe move: " << mc << ", angle: " << angle << ", distance: " << distance << ", vpoint: " << vpoint << endl;

								swipeleave_oss.str("");
								swipemove_oss.str("");
								swipemove_oss << "latest swipe move: " << mc << ", angle: " << angle << ", distance: " << distance << ", vpoint: " << vpoint;
							} else {
								// TODO we should set swipe context's dir value here
								cout << "swipe down: " << mc << ", angle: " << angle << ", distance: " << distance << ", vpoint: " << vpoint << endl;

								swipeenter_oss.str("");
								swipeleave_oss.str("");
								swipemove_oss.str("");
								swipemove_oss << "latest swipe down: " << mc << ", angle: " << angle << ", distance: " << ", vpoint: " << vpoint << distance;
							}

							found = true;
						} else {
							// remove noise near already handled point, but this is weak solution..
							// on current data condition, this routine can be not entered here.
							cout << "This is noise point" << endl;
						}
					} else if (distance == 0) {
						// if current point and latest point is same, remove current one
						cout << "current point and latest point is same" << endl;
					}	else {
						cout << "angle problem: " << mc << ", angle: " << angle << ", distance: " << distance << ", vpoint: " << vpoint << endl;
					}
					tit = tips.erase(tit);
					eit = tipEdges.erase(eit);
					continue;
				} 
				tit++;
				eit++;
			}

			if (!found) {
				ostringstream oss;
				for (auto tit = tips.begin(); tit != tips.end(); tit++) {
					oss << *tit << ", ";
				}
				cout << "satisfied tips not found: " << oss.str() << endl;
				cout << "previous point: " << point << endl;
				cout << "this is ignored" << endl << endl;

				context->history.push_back(cv::Point(-1, -1));

				// check this triggeres up event 
				int dummyCount = 0;
				for (auto hit = context->history.crbegin(); hit != context->history.crend(); hit++) {
					cv::Point point = *hit;
					if ((point.x == -1) && (point.y == -1)) {
						dummyCount++;
						continue;
					}
					break;
				}

				int validCount = 0;
				for (auto hit = context->history.crbegin(); hit != context->history.crend(); hit++) {
					cv::Point point = *hit;
					if ((point.x == -1) && (point.y == -1)) {
						continue;
					}
					validCount++;
				}

				if (dummyCount >= 10) {
					if (validCount > 1) {
						cout << "dummy point is fullfilled" << endl;
						cout << "swipe up: " << context->history.at(context->history.size() - 6) << endl << endl;

						swipemove_oss.str("");
						swipeleave_oss.str("");
						swipeleave_oss << "latest swipe up: " << context->history.at(context->history.size() - 6);
					} 

					it = swipe_vector.erase(it);

				} else {
					it++;
				}
			} else {
				if (((mc.x >= 0) && (mc.x <= 35)) ||
						((mc.x >= (depth_intrin.width - 10)) && (mc.x <= depth_intrin.width)) ||
						((mc.y >= 0) && (mc.y <= 10)) ||
						((mc.y >= (depth_intrin.height - 10)) && (mc.y <= depth_intrin.height))) {

					ostringstream oss;
					cout << "last point is on edge" << endl;
					cout << "last point: " << point << endl;
					cout << "swipe up: " << context->history.back() << endl;

					swipemove_oss.str("");
					swipeleave_oss.str("");
					swipeleave_oss << "latest swipe up: " << context->history.back();

					it = swipe_vector.erase(it);
				} else {
					it++;
				}
			}

			//break;
		}
	} else {
		cout << "tips not founds.." << endl;
		for (auto it = swipe_vector.begin(); it != swipe_vector.end(); ) {
			struct swipe_context *context = *it;
			if (context->history.size() > 1) {
				cout << "swipe up: " << context->history.back() << endl;
			}
			it = swipe_vector.erase(it);
		}
	}	

	// add new event vector if there is unhandled centroids
	if (tips.size() > 0) {
		for (int i = 0; i < tips.size(); i++) {
			struct swipe_context *context = new struct swipe_context();
			context->dir = -1;
			context->edge = tipEdges[i];
			context->history.push_back(tips[i]);
			context->color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));

			swipe_vector.push_back(context);
			/*
			cout << "swipe down: " << tips[i] << endl;

			touchdown_oss.str("");
			touchup_oss.str("");
			touchmove_oss.str("");
			touchdown_oss << "lastest swipe down: " << tips[i];
			*/
		}
	}

	int row = 0;
	for (auto it = swipe_vector.begin(); it != swipe_vector.end(); it++) {
		ostringstream oss;
		struct swipe_context *context = *it;
		oss << "-------------------------" << endl;
		oss << "[" << row << "] ";
		for (auto hit  = context->history.begin(); hit != context->history.end(); hit++) {
			oss << *hit << ", ";
		}
		oss << endl;
	 	oss << "-------------------------" << endl;
		cout << oss.str() << endl;
		row++;
	}

	ostringstream oss;
	oss << "current swipe down count: " << swipe_vector.size();
	cv::putText(dst, oss.str(), cv::Point(10, 10), 1, 1.0, Scalar::all(255));
	cv::putText(dst, swipeenter_oss.str(), cv::Point(10, 30), 1, 1.0, Scalar::all(255));
	cv::putText(dst, swipeleave_oss.str(), cv::Point(10, 50), 1, 1.0, Scalar::all(255));
	cv::putText(dst, swipemove_oss.str(), cv::Point(10, 50), 1, 1.0, Scalar::all(255));

//	detectFingerTip(dst, contours);
	cv::imshow("Depth Gesture Contours", dst);
}

static double angle(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
	double dx1 = pt1.x - pt0.x;
	double dy1 = pt1.y - pt0.y;
	double dx2 = pt2.x - pt0.x;
	double dy2 = pt2.y - pt0.y;
	return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

/**
 * Helper function to display text in the center of a contour
 */
void setLabel(cv::Mat& im, const std::string label, std::vector<cv::Point>& contour)
{
	int fontface = cv::FONT_HERSHEY_SIMPLEX;
	double scale = 0.4;
	int thickness = 1;
	int baseline = 0;

	cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);
	cv::Rect r = cv::boundingRect(contour);

	cv::Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));
	cv::rectangle(im, pt + cv::Point(0, baseline), pt + cv::Point(text.width, -text.height), CV_RGB(255,255,255), CV_FILLED);
	cv::putText(im, label, pt, fontface, scale, CV_RGB(0,0,0), thickness, 8);
}


cv::Matx33f find_calibrated_points(std::vector<cv::Point> &approx)
{
	cout << "approx points: " << endl << approx << endl;

	cv::Point2f srcPoints[4];
	cv::Point2f destPoints[4];

	// 1. get start point from apporx points
	int startIdx = 0;
	uint32_t minDistance = 9999999;
	for (int i = 0; i < approx.size(); i++) {
		int distance = sqrt(approx[i].x * approx[i].x + approx[i].y * approx[i].y);
		if (distance < minDistance) {
			minDistance = distance;
			startIdx = i;
		}
	}

	uint32_t dx, dy, line1, line2, preIdx, nextIdx;
	if (startIdx == 0) {
		preIdx = approx.size() - 1;
		nextIdx = startIdx + 1;
		srcPoints[0] = approx[startIdx]; // top-left
		srcPoints[1] = approx[preIdx]; // top-right
		srcPoints[2] = approx[nextIdx]; // bottom-left
		srcPoints[3] = approx[nextIdx + 1]; // bottom-right
	} else if (startIdx == (approx.size() - 1)) {
		preIdx = startIdx - 1;
		nextIdx = 0;
		srcPoints[0] = approx[startIdx]; // top-left
		srcPoints[1] = approx[preIdx]; // top-right
		srcPoints[2] = approx[nextIdx]; // bottom-left
		srcPoints[3] = approx[nextIdx + 1]; // bottom-right
	} else {
		preIdx = startIdx - 1;
		nextIdx = startIdx + 1;
		srcPoints[0] = approx[startIdx]; // top-left
		srcPoints[1] = approx[preIdx]; // top-right
		srcPoints[2] = approx[nextIdx]; // bottom-left
		if (startIdx == 1) {
			srcPoints[3] = approx[nextIdx + 1]; // bottom-right
		} else if (startIdx == 2) {
			srcPoints[3] = approx[preIdx - 1]; // bottom-right
		}
	}

	dx = approx[startIdx].x - approx[preIdx].x;
	dy = approx[startIdx].y - approx[preIdx].y;
	line1 = sqrt(dx * dx + dy * dy);

	dx = approx[startIdx].x - approx[nextIdx].x;
	dy = approx[startIdx].y - approx[nextIdx].y;
	line2 = sqrt(dx * dx + dy * dy);

	uint32_t width, height;
	if (line1 > line2) {
		width = line1;
		height = line2;
	} else {
		width = line2;
		height = line1;
	}
	cout << "start point idx: " << startIdx << ", width: " << width << ", height: " << height << endl;

	destPoints[0] = cv::Point(0, 0);
	destPoints[1] = cv::Point(width, 0);
	destPoints[2] = cv::Point(0, height);
	destPoints[3] = cv::Point(width, height);

	cv::Matx33f transformMatrix = cv::getPerspectiveTransform(srcPoints, destPoints);

	cout << "transform matrix: " << transformMatrix << endl;

	//cv::Point3f transPoint = transformMatrix * cv::Point3f(186, 165, 1);
	for (int i = 0; i < 4; i++) {
		cv::Point3f transPoint = transformMatrix * cv::Point3f(srcPoints[i].x, srcPoints[i].y, 1);
		cout << srcPoints[i] << "->" << transPoint << endl;
	}

	return transformMatrix;
}

cv::Mat beam_get_filtered_shape_frame_matrix(struct app_context *app, uint8_t *color_image)
{
	cv::Mat src(480, 640, CV_8UC3, (void *)color_image);
	cv::cvtColor(src, src, CV_BGR2RGB);
	cv::medianBlur(src, src, 5);
	cv::Mat gray;
	cv::Mat output, tmp;
	cv::cvtColor(src, src, CV_BGR2RGB);
	cv::medianBlur(src, src, 5);
	cv::cvtColor(src, output, CV_BGR2GRAY);
	//cv::threshold(gray, tmp, 32, 255, THRESH_BINARY);
	//cv::bilateralFilter(gray, output, 15, 10, 10);
	//cv::Canny(output, output, 0, 250, 5);
	//cv::Canny(output, output, 0, 100, 5);
	//cv::Mat element1(9, 9, CV_8U, cv::Scalar(1));
	//cv::morphologyEx(output2, output2, cv::MORPH_OPEN, element1);
	//cv::morphologyEx(output2, output2, cv::MORPH_CLOSE, element1);
	//cv::GaussianBlur(output2, output2, cv::Size(5, 5), 3);
	//cv::threshold(output2, output2, 1, 255, THRESH_BINARY);
	cv::imshow("filtered shape image", output);
	cv::waitKey(1);

	return output.clone();
}

void beam_get_shapes(struct app_context *app, cv::Mat& matrix, uint8_t *color_image)
{
	std::vector<std::vector<cv::Point> > contours;

	rs::device *dev = app->rsdev;
	rs::intrinsics color_intrin = dev->get_stream_intrinsics(rs::stream::color);
	cv::Mat dst(480, 640, CV_8UC3, (void *)color_image);

	cv::findContours(matrix.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	std::vector<cv::Point> approx;
	std::vector<cv::Point> maxRect;
	uint32_t maxRectArea = 0;

	for (int i = 0; i < contours.size(); i++)
	{
		// Approximate contour with accuracy proportional
		// to the contour perimeter
		cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.02, true);

		// Skip small or non-convex objects 
		uint32_t curContourArea = cv::contourArea(contours[i]);
		if (curContourArea < 300) {
			continue;
		}
		 
		vector<vector<cv::Point> > hulls(1);
		vector<vector<cv::Point> > tcontours;
		tcontours.push_back(contours[i]);
		cv::convexHull(cv::Mat(tcontours[0]), hulls[0], false);

		if (!cv::isContourConvex(approx)) {
			//cv::drawContours(dst, hulls, -1, cv::Scalar(0, 0, 255), 2);
			continue;
		} else {
			cv::drawContours(dst, hulls, -1, cv::Scalar(0, 255, 0), 2);
		}

		drawContours(dst, tcontours, -1, cv::Scalar(255, 0, 0),2);

		if (approx.size() == 3) {
			setLabel(dst, "TRI", contours[i]);    // Triangles
		} else if (approx.size() >= 4 && approx.size() <= 6) {
			// Number of vertices of polygonal curve
			int vtc = approx.size();

			// Get the cosines of all corners
			std::vector<double> cos;
			for (int j = 2; j < vtc+1; j++)
				cos.push_back(angle(approx[j%vtc], approx[j-2], approx[j-1]));

			// Sort ascending the cosine values
			std::sort(cos.begin(), cos.end());

			// Get the lowest and the highest cosine
			double mincos = cos.front();
			double maxcos = cos.back();

			// Use the degrees obtained above and the number of vertices
			// to determine the shape of the contour
			if (vtc == 4 && mincos >= -0.1 && maxcos <= 0.3)
			{
				setLabel(dst, "RECT", contours[i]);
				if (maxRectArea < curContourArea) {
					maxRectArea = curContourArea;
					maxRect = approx;
				}
			}
			else if (vtc == 5 && mincos >= -0.34 && maxcos <= -0.27)
				setLabel(dst, "PENTA", contours[i]);
			else if (vtc == 6 && mincos >= -0.55 && maxcos <= -0.45)
				setLabel(dst, "HEXA", contours[i]);
		}
		else
		{
			// Detect and label circles
			double area = cv::contourArea(contours[i]);
			cv::Rect r = cv::boundingRect(contours[i]);
			int radius = r.width / 2;

			if (std::abs(1 - ((double)r.width / r.height)) <= 0.5 &&
			    std::abs(1 - (area / (CV_PI * std::pow(radius, 2)))) <= 0.5)
				setLabel(dst, "CIR", contours[i]);
		}
	}

	cv::Mat dst2(480, 640, CV_8UC3);
	if (maxRectArea > 0) {
		cv::Matx33f matrix = find_calibrated_points(maxRect);
		cv::warpPerspective(dst, dst2, matrix, dst.size());
	}

	cv::imshow("Shape Detection", dst);
	cv::imshow("Shape Detection2", dst2);
}

void beam_get_shapes2(struct app_context *app, cv::Mat& matrix, uint8_t *color_image)
{
	cv::Mat dst(480, 640, CV_8UC3, (void *)color_image);
	std::vector<Vec3f> circles;

	cv::HoughCircles(matrix, circles, CV_HOUGH_GRADIENT, 2, matrix.rows/16, 300, 90);


	for (int i = 0; i < circles.size(); i++) {
		Vec3i c = circles[i];
		circle(dst, cv::Point(c[0], c[1]), c[2], cv::Scalar(0, 0, 255), 3, CV_AA);
		circle(dst, cv::Point(c[0], c[1]), 2, cv::Scalar(0, 255, 0), 3, CV_AA);
	}

	ostringstream oss;
	oss << "circle count: " << circles.size();
	cv::putText(dst, oss.str(), cv::Point(10, 10), 1, 1.0, Scalar::all(255));
	cv::imshow("Shape Dection2", dst);

	cv::imshow("Shape Dection2", dst);
}

void beam_show_color_image(struct app_context *app)
{
	struct rs::device *dev = app->rsdev;

  uint8_t *color_image = (uint8_t *)dev->get_frame_data(rs::stream::color);
	cv::Mat mat(480, 640, CV_8UC3, (void *)color_image);
	cv::imshow("Color Image", mat);
	cv::waitKey(1);
}

void beam_init_baseline_frame(struct app_context *app) 
{
	struct rs::device *dev = app->rsdev;
	rs::intrinsics depth_intrin = dev->get_stream_intrinsics(rs::stream::depth);
	app->bottom_depth = beam_get_average_bottom_depth(app, (uint16_t *) dev->get_frame_data(rs::stream::depth));

	for (int i = 0; i < 10; ++i) {
		dev->wait_for_frames();
		baselines[i] = (uint16_t *) dev->get_frame_data(rs::stream::depth);
	}

	for(int dy = 0; dy < depth_intrin.height; dy++) {
		for(int dx = 0; dx < depth_intrin.width; dx++) {
			uint32_t min = 65536;
			ostringstream oss;
			oss << "min: " << min;
			for (int i = 0; i < 10; ++i) {
				if (baselines[i][depth_intrin.width * dy + dx] > 0) {
					if (baselines[i][depth_intrin.width * dy + dx] < min) {
						min = baselines[i][depth_intrin.width * dy + dx];
						oss << " -> " << min;
					}
				}
			}

			if (min > app->bottom_depth + 10) {
				min = app->bottom_depth;
				oss << " -> " << min << " (above bottom case)";
			}

			app->baseline[dy][dx] = min;
			//cout << "dx: " << dx << ", dy: " << dy << ", depth: " << oss.str() << endl;
		}
	}

	cout << "realsense height: " << app->bottom_depth << endl;
}

void beam_run(struct app_context *app)
{
	struct rs::device *dev = app->rsdev;
	uint16_t *touch_frame, *gesture_frame;
  uint8_t *shape_frame;
	cv::Mat matrix;

  rs::intrinsics depth_intrin = dev->get_stream_intrinsics(rs::stream::depth);
	while (true) {
		dev->wait_for_frames();
		uint16_t *depth_image = (uint16_t *) dev->get_frame_data(rs::stream::depth);

		/*
		touch_frame = beam_get_noise_removed_touch_frame(app, depth_image);
		matrix = beam_get_filtered_touch_frame_matrix(app, touch_frame);
		beam_get_touch_points(app, matrix, touch_frame);
		free(touch_frame);

		gesture_frame = beam_get_noise_removed_gesture_frame(app, depth_image);
		matrix = beam_get_filtered_gesture_frame_matrix(app, gesture_frame);
		beam_get_gestures(app, matrix, gesture_frame);
		free(gesture_frame);
		*/

		uint8_t *color_image = (uint8_t *) dev->get_frame_data(rs::stream::color);
		shape_frame = beam_get_noise_removed_shape_frame(app, color_image);
		matrix = beam_get_filtered_shape_frame_matrix(app, shape_frame);
		beam_get_shapes2(app, matrix, shape_frame);
		free(shape_frame);

		//beam_update_used_frame_history(app, depth_image);
		//beam_show_color_image(app);
	}
}

int main() try 
{
	struct app_context *app;

  rs::context ctx;
	if(ctx.get_device_count() == 0) {
	 	return -1;
	} 

	printf("There are %d connected RealSense devices.\n", ctx.get_device_count());
	app = (struct app_context *) malloc(sizeof(app_context));
	//memset(app, 0x0, sizeof(struct app_context));

	app->rsdev = ctx.get_device(0); 

	if(rs_init(app) < 0) {
		printf("failed to init realsense device\n");
		return -1;
	}

	if(opencv_init(app) < 0) {
		printf("failed to init opencv\n");
		return -1;
	}

	beam_init_baseline_frame(app);
	beam_run(app);

	return 0;
}

catch (const rs::error & e)
{
	printf("rs::error was thrown when calling %s(%s):\n", e.get_failed_function().c_str(), e.get_failed_args().c_str());
	printf("    %s\n", e.what());

	return -1;
}

