const { app, ipcMain, BrowserWindow }  = require('electron');
const path = require('path')
const url = require('url')
const co = require('co')
const deepMerge = require('deepmerge')
const Promise = require('bluebird')
const mongo = require('mongodb');
const MongoClient = Promise.promisifyAll(mongo).MongoClient;
const meta = require('./meta.json')
const nodePackage = require('../manifest.json')
const { ShellManager } = require('./lib/shell-manager')

const usableProtocols = [ 'http:', 'https:', 'file:' ]
const switchOptions = [
  ['--touch-events'],
  ['--enable-transparent-visuals'],
  ['--ignore-certificate-errors'],
  ['--ppapi-flash-path', path.join(__dirname, 'lib', 'libpepflashplayer.so')]
]

co(function* () {

  // Keep a global reference of the window object, if you don't, the window will
  // be closed automatically when the JavaScript object is garbage collected.
  for (let option of switchOptions) {
    app.commandLine.appendSwitch(...option)
  }
})


function createWindow (startUrl, appConfig) {
  // Create the browser window.
  let windowOption = appConfig.windowOption

  // Support compatibility for prior configuration
  windowOption.width = appConfig.width || appConfig.windowOption.width
  windowOption.height = appConfig.height || appConfig.windowOption.height

  let mainWindow = new BrowserWindow(windowOption) 
  // and load the index.html of the app.
  mainWindow.loadURL(startUrl)

  // Open the DevTools.
	if (appConfig.debug) {
  	mainWindow.webContents.openDevTools()
	}

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })

	mainWindow.webContents.on('new-window', (event, urlToOpen) => {
		event.preventDefault();
		console.log('url to open: ', urlToOpen);
	});

	mainWindow.on('blur', () => {
		console.log('win blur');
	});

	mainWindow.on('focus', () => {
		console.log('win focus');
	});

	mainWindow.on('resize', () => {
		console.log('win resize');
		mainWindow.webContents.send('window-resize');
	});

	mainWindow.once('ready-to-show', () => {
		mainWindow.show();
	});
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  co(function* () {
    const args = process.argv.slice(2)

    // get runner config
    let runnerConfig = nodePackage.config
    let db = yield MongoClient.connectAsync(meta.nemodbPath)
    let docs = yield db.collection(nodePackage.pkgname).find({}).toArrayAsync()
    if (docs.length) {
      runnerConfig = deepMerge(runnerConfig, docs[0].config)
    } 

    // get app config
    let appConfig = {}
    const configs = args.filter(arg => arg.includes(meta.configOption))
    if (configs.length) {
      appConfig = JSON.parse(configs[0].split(meta.configOption).pop())
    } 
    appConfig = deepMerge(runnerConfig, appConfig)

    // get url
    let urls = args.filter(arg => arg.includes(meta.urlOption))
    if (!urls.length) {
      console.log('error: url option does not exist')
      process.exit(-1)
    }
    const startUrl = urls[0].split(meta.urlOption).pop()

    const protocol = url.parse(startUrl).protocol;
    if (!usableProtocols.includes(protocol)) {
      console.log('error: url is not available')
      process.exit(-1)
    }

    console.log(startUrl)
    console.log(appConfig)
    createWindow(startUrl, appConfig)
  })
})

app.on('window-all-closed', function () {
  app.quit()
})

app.on('login', (event, webContents, request, authInfo, callback) => {
	event.preventDefault();
	callback(appConfig.auth.username, appConfig.auth.secret);
})

app.on('browser-window-blur', function () {
	console.log('browser window blur');
})

app.on('browser-window-focus', function () {
	console.log('browser window focus');
})

ipcMain.on('applaunch-request', (event, data) => {
  co(function* () {
    console.log('applaunch-request event: ', data)
    const shellManager = new ShellManager()

    let metadata = {
      type: data.type,
      path: path.join(meta.pkgsRootPath, data.pkgname, meta.execFileName)
    }

    metadata.args = []
    for (let key in data.param) {
      if (key === meta.optindKey) {
        metadata.args.push(data.param[key])
        continue
      }

      metadata.args.push(...[`--${key}`, data.param[key]])
    }

    yield shellManager.run(metadata)
    event.sender.send('applaunch-response', { result: true, data })
  })
})
