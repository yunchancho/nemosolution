//const ipcRenderer = require('electron').ipcRenderer;
const {ipcRenderer, shell} = require('electron');

var colorTypes = [ "#F7FE2E", "#FA8258", "#58FA82", "#81BEF7", "#888888" ]; 
var colorIndex = 0;
var webMarker;
var webMarkerMode = false;

ipcRenderer.on('webview-input-focus', (event, arg) => {
	console.log('webview-input-focus: ', arg);
});

ipcRenderer.on('window-resize', (event, arg) => {
	if (webMarkerMode) {
		createWebMarker();
	}
});

ipcRenderer.on('asynchronous-message', (event, arg) => {
	console.log('received msg from ui process: ', arg);

	/*
	let msg = document.getElementById("message");
	msg.innerHTML = arg;
	*/
	//webview.setUserAgent("Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25");
	webview.setAttribute('src', arg);

	webview.addEventListener('ipc-message', (event) => {
		console.log('recieved from webview: ', event.channel);
		ipcRenderer.send('webview-input-focus', event.channel);
	});

	webview.addEventListener('enter-html-full-screen', (event) => {
		event.preventDefault();
		console.log('block fullscreen enter');

	});

	webview.addEventListener('leave-html-full-screen', (event) => {
		event.preventDefault();
		console.log('block fullscreen leave');
	});

	webview.addEventListener('console-message', (event) => {
		console.log('webview log: ', event.message);
	});

	webview.addEventListener('new-window', (event) => {
		const protocol = require('url').parse(event.url).protocol;
		if (protocol === 'http:' || protocol === 'https:') {
			console.log('navigate other url in place');
			webview.setAttribute('src', event.url);
		}
	});

	webview.addEventListener('will-navigate', (event) => {
		Pace.start();
		changeNaviBtnColor();
		console.log('-- will-navigate');
	});

	webview.addEventListener('did-start-loading', (event) => {
		console.log('-- did-start-loading');
	});

	webview.addEventListener('dom-ready', (event) => {
		console.log('-- dom-ready');
	});

	webview.addEventListener('did-stop-loading', (event) => {
		console.log('-- did-stop-loading');
		changeNaviBtnColor();
	});

	webview.addEventListener('did-finish-load', (event) => {
		console.log('-- did-finish-load');
		Pace.stop();
	});

	webview.addEventListener('did-fail-load', (event) => {
		console.log('-- did-fail-load');
	});

	webview.addEventListener('did-navigate', (event) => {
		console.log('-- did-navigate');
	});

	webview.addEventListener('did-navigate-in-page', (event) => {
		console.log('-- did-navigate-in-page');
		Pace.stop();
	});

	ipcRenderer.send('asynchronous-message', 'success');
});



function initialize() 
{
	console.log('init');

  window.paceOptions = {
   // target: 'div.footer',
  };

	Pace.start();

	bg = document.getElementById("bg");
	webview = document.querySelector('webview');

	backBtn = document.getElementById("backBtn");
	forwardBtn = document.getElementById("forwardBtn");

	hammer = new Hammer(bg);

	hammer.on("swiperight", function (event) {
		console.log(event.type + ' gesture!');
		webview.goBack();
	});

	hammer.on("swipeleft", function (event) {
		console.log(event.type + ' gesture!');
		webview.goForward();
	});

	if (webMarkerMode) {
		createWebMarker();
	}
}

function changeColor ()
{
	console.log('color: ', colorTypes[colorIndex]);

	colorIndex = (colorIndex + 1) % colorTypes.length;
	bg.style.backgroundColor = colorTypes[colorIndex];
}

function goBack ()
{
	if (webview.canGoBack()) {
		webview.goBack();
	} 
}

function goForward ()
{
	if (webview.canGoForward()) {
		webview.goForward();
	} 
}

function changeNaviBtnColor() {
	if (webview.canGoBack()) {
		backBtn.style.color = "#444444";
	} else {
		backBtn.style.color = "#EEEEEE";
	}

	if (webview.canGoForward()) {
		forwardBtn.style.color = "#444444";
	} else {
		forwardBtn.style.color = "#EEEEEE";
	}
}

function switchMarkerMode () {
	webMarkerMode = !webMarkerMode;

	let markerEl = document.getElementById('marker');
	let markerBtnEl = document.getElementById('markerBtn');
	if (webMarkerMode) {
		markerEl.style.display = 'block';
		markerBtnEl.style.color = '#444444';
		createWebMarker();
	} else {
		markerEl.style.display = 'none';
		markerBtnEl.style.color = '#EEEEEE';
		removeWebMarker();
	}
}

function removeWebMarker () {
	delete webMarker;	
	let markerEl= document.getElementById('marker');
	while (markerEl.hasChildNodes()) {
		markerEl.removeChild(markerEl.firstChild);
	}
}

function createWebMarker () {
	if (webMarker) {
		removeWebMarker();
	}
	webMarker = new DrawingBoard.Board('marker', {
		background: false,
		color: "#ff0",
		size: 30,
		controls: [
			'Color',
			{ 
				Size: { 
					type: "range"
			 	}
		 	},
			{
			 	Navigation: {
					back: true,
					forward: true 
				},
			},
			'DrawingMode'
		],
		controlsPosition: "bottom right",
		webStorage: false,
		//enlargeYourContainer: true
	});
}
