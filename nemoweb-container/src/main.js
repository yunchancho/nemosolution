const {app, ipcMain, BrowserWindow, dialog}  = require('electron');
const path = require('path')
const url = require('url')
const co = require('co')
const deepMerge = require('deepmerge')
const Promise = require('bluebird')
const mongo = require('mongodb');
const MongoClient = Promise.promisifyAll(mongo).MongoClient;
const meta = require('./meta.json')
const nodePackage = require('../manifest.json')

const usableProtocols = [ 'http:', 'https:', 'file:' ]

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
app.commandLine.appendSwitch('--touch-events');
app.commandLine.appendSwitch('--enable-transparent-visuals');
app.commandLine.appendSwitch('--ignore-certificate-errors');
app.commandLine.appendSwitch('--ppapi-flash-path', path.join(__dirname, 'lib', 'libpepflashplayer.so'));

function createWindow (webviewUrl, appConfig) {
  // Create the browser window.
  let windowOption = appConfig.windowOption

  // Support compatibility for prior configuration
  windowOption.width = appConfig.width || appConfig.windowOption.width
  windowOption.height = appConfig.height || appConfig.windowOption.height
  // node integration should be always true 
  windowOption.webPreferences.nodeIntegration = true

  let mainWindow = new BrowserWindow(windowOption) 

  // and load the index.html of the app.
  mainWindow.loadURL(`file:\/\/${path.join(__dirname, meta.startFile)}`)

  // Open the DevTools.
	if (appConfig.debug) {
  	mainWindow.webContents.openDevTools()
	}

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })

  mainWindow.webContents.on('did-finish-load', () => {
		mainWindow.webContents.send('asynchronous-message', webviewUrl);
	});

	mainWindow.webContents.on('new-window', (event, urlToOpen) => {
		event.preventDefault();
		console.log('url to open: ', urlToOpen);
	});

	mainWindow.on('blur', () => {
		console.log('win blur');
	});

	mainWindow.on('focus', () => {
		console.log('win focus');
	});

	mainWindow.on('resize', () => {
		console.log('win resize');
		mainWindow.webContents.send('window-resize');
	});

	mainWindow.once('ready-to-show', () => {
		mainWindow.show();
	});

  mainWindow.webContents.session.on('will-download', (event, item, webContents) => {
    console.log('will-download event fired')
    event.preventDefault()
    /*
     * TODO this makes native window to be opened.
     * But its position is random due to fine platform's xserver policy
     * So we don't show dialog in this case..
     *
    dialog.showMessageBox(mainWindow, {
      type: "info",
      buttons: [ "Ok" ],
      title: "Nemo Alert",
      message: "File downloading is not allowed"
    })
    */
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  co(function* () {
    const args = process.argv.slice(2)

    // get runner config
    let runnerConfig = nodePackage.config
    let db = yield MongoClient.connectAsync(meta.nemodbPath)
    let docs = yield db.collection(nodePackage.pkgname).find({}).toArrayAsync()
    if (docs.length) {
      runnerConfig = deepMerge(runnerConfig, docs[0].config)
    } 

    // get app config
    let appConfig = {}
    const configs = args.filter(arg => arg.includes(meta.configOption))
    if (configs.length) {
      appConfig = JSON.parse(configs[0].split(meta.configOption).pop())
    } 
    appConfig = deepMerge(runnerConfig, appConfig)

    // get url
    let urls = args.filter(arg => arg.includes(meta.urlOption))
    if (!urls.length) {
      console.log('error: url option does not exist')
      process.exit(-1)
    }
    const startUrl = urls[0].split(meta.urlOption).pop()

    const protocol = url.parse(startUrl).protocol;
    if (!usableProtocols.includes(protocol)) {
      console.log('error: url is not available')
      process.exit(-1)
    }

    console.log(startUrl)
    console.log(appConfig)
    createWindow(startUrl, appConfig)
  })
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

app.on('login', (event, webContents, request, authInfo, callback) => {
	event.preventDefault();
	callback(appConfig.auth.username, appConfig.auth.secret);
});

app.on('browser-window-blur', function () {
	console.log('browser window blur');
});

app.on('browser-window-focus', function () {
	console.log('browser window focus');
});

ipcMain.on('asynchronous-message', (event, arg) => {
	console.log('recived msg from renderer process: ', arg);
});

ipcMain.on('webview-input-focus', (event, arg) => {
	// TODO we need to launch nemo keyboard 
	console.log('recieved from renderer: ', arg);
});

