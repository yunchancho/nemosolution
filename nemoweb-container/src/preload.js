const {ipcRenderer} = require('electron');

console.log('set webview focus event in preload.js');

document.addEventListener('click', function (event) {
	const tag = event.target.tagName.toLowerCase();
	const type = event.target.type.toLowerCase();

	if (tag == 'input' && (type == 'text' || type == 'url')) {
		console.log('x: ' + event.screenX + ', y: ' + event.screenY);
		console.log('tagname: ' + tag + ', type: ' + event.target.type);

		ipcRenderer.sendToHost({ x: event.screenX, y: event.screenY });
	}
});

navigator.webkitGetUserMedia({ audio: true, video: true }, function () { console.log('device enabled!!')}, function () { console.log('device failed!!')  })
