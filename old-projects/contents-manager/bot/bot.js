var fs = require('fs');
var fsExtra = require('fs-extra');
var http = require('http');
var spawn = require('child_process').spawn;
var Q = require('q');
var winston = require('winston');
var _ = require('underscore');
var mime = require('mime');
var telebot = require('node-telegram-bot-api');
var exec = require('child_process').exec;

var token = '213654123:AAGDqoEVYKdUBZKvH6hf8nEjVGFJKOLChk4';
var bot;
var db;

// TODO we need to manage this variable per each user
var dirStack = [];
var currentDir;
var currentFiles = [];
var currentMode;

var contentDirPrefix = process.env.NEMOUX_CONTENTS_PATH ? process.env.NEMOUX_CONTENTS_PATH: __dirname + '/uploads';
var contentTmpPrefix = '/tmp';

// bookmark stuff
var bookmarkDir = process.env.NEMOUX_BOOKMARKS_PATH ? process.env.NEMOUX_BOOKMARKS_PATH: __dirname + '/bookmarks';
var currentBms = [];

function getFileInfo(file) {
  var fullPath = this.path + '/' + file;
  var fileInfo = {};
  return Q.nfcall(fs.stat, (fullPath)).then(function (stats) {
    fileInfo.name = file;
    fileInfo.size = stats.size;
    fileInfo.createdAt = stats.birthtime;
    if (stats.isDirectory()) {
      return Q.nfcall(fs.readdir, fullPath)
        .then(function (subfiles) {
          var length = 0;
          _.each(subfiles, function (subfile) {
            console.log(subfile);
            if (subfile.charAt(0) != '.') {
              length++;
            }
          });
          console.log(length);
          fileInfo.count = length;
          fileInfo.type = 0;

          return fileInfo;
        });
    }
    fileInfo.mimetype = mime.lookup(fullPath);
    fileInfo.type = 1;
    return fileInfo;
  });
}

function getDirList(path) {
  var deferred = Q.defer();
  var getFileInfoBind = _.bind(getFileInfo, { path: path });

  Q.nfcall(fs.readdir, path)
  .then(function (files) {
    return _.map(files, getFileInfoBind);
  })
  .then(Q.all)
  .then(function (files) {
		// remove hidden files
		var filtered = _.filter(files, function (file) {
			return file.name[0] === '.' ? false: true;
		});

    deferred.resolve(filtered);
  })
  .catch(function (err) {
    deferred.reject(err);
  });

  return deferred.promise;
}

function convertToMessage(files) {
	var message = '';

	if (!files.length) {
		return "There is no files..\n";
	}

	_.each(files, function(file, index) {
		message += (index + 1) + '. ';
		if (file.type == 0) {
			message += '/dir__';
		} else {
			message += '/file__';
		}
		message += file.name;
		if (file.type == 0) { 
			// in case of folder
			message += ' (' + file.count + ')';
		}
		message += '\n';
	});

	return message;
}

function start(msg, match) {
	console.log(msg);
	var deferred = Q.defer();
	var chatId = msg.from.id;
	var message = '';

	currentDir = contentDirPrefix;
	dirStack = [];

	for (var i = 0; i < 15; i++) {
		message += '&#9603';
	}

	var message = "\n&#9787<b>I am contents bot</b>&#9787\n";
	for (var i = 0; i < 15; i++) {
		message += '&#9603';
	}

	message += '\nI will show root directory of contents';

	bot.sendMessage(chatId, message, { parse_mode: "HTML"})
	.then(function () {
		return sendContentList(chatId, currentDir);
	})
	.catch(function (err) {
		console.log(err.stack);
		bot.sendMessage(chatId, "Something Wrong...");
	});
}

function renameFolderIconFile(filepath, dir) {
	var icontype = filepath.split('.').pop();
	console.log("icontype: " + icontype);

  var newpath;
  var newpath2;

	if (icontype == 'svg') {
		newpath = dir + '/.icon.svg';
		newpath2 = dir + '/icon.svg';
	} else {
		newpath = dir + '/.icon.png';
		newpath2 = dir + '/icon.png';
	}

  var deferred = Q.defer();
  console.log('oldpath: ' + filepath);
  console.log('newpath: ' + newpath);
  fs.rename(filepath, newpath, function (err) {
    if (err) {
      return deferred.reject(err.stack);
    }
		fs.createReadStream(newpath).pipe(fs.createWriteStream(newpath2));
    return deferred.resolve();
  });

  return deferred.promise;
}

function copyRingIconFile(dirPath) {
}

function createDirIcon(chatId, dirPath) {
	var deferred = Q.defer();

	var dirname;
	var diricon;
	bot.sendMessage(chatId, "What is icon file? Please send image file(png,jpg,svg)", {
    reply_markup: {
			force_reply: true
		}
	})
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

		bot.onReplyToMessage(chatId, messageId, function (message) {
			console.log("DIR ICON: ", message);
			var regex = /^[^\\/?%*:|"<>\.]+$/;
			if(!regex.test(message.text)) {
				bot.sendMessage(chatId, "Wrong file name..");
				deferred.reject();
			} else {
				var fileId;
				if (message.photo) {
					if (!message.photo.length) {
						return deferred.reject();
					}
					fileId = message.photo.pop().file_id;
				} else if (message.document) {
					var mime = message.document.mime_type;
					if (mime.indexOf('image') >= 0 || mime.indexOf('svg') >= 0) {
						fileId = message.document.file_id;
					} else {
						return deferred.reject();
					}
				} else {
					return deferred.reject();
				}

				if (!fileId) {
					return deferred.reject();
				}

				bot.downloadFile(fileId, dirPath)
				.then(function (filepath) {
					return renameFolderIconFile(filepath, dirPath);
				})
				.then(function () {
					var ringOriginPath = process.env.NEMOUX_CONTENTS_RING;
					var ringCopyPath = dirPath + '/.ring.svg';
					return Q.nfcall(fsExtra.copy, ringOriginPath, ringCopyPath);
				})
				.then(function () {
					console.log('success to move folder icon');
					deferred.resolve();
				})
				.catch(function (err) {
					console.log('fail to move folder icon: ', err);
					deferred.reject(err);
				});
			}
		});
	})
	.catch(function (err) {
		deferred.reject(err);
	});

	return deferred.promise;
}

function createDirName(chatId) {
	var deferred = Q.defer();

	var dirname;
	var diricon;
	bot.sendMessage(chatId, "What is name of new directory?", {
    reply_markup: {
			force_reply: true
		}
	})
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

		bot.onReplyToMessage(chatId, messageId, function (message) {
			var regex = /^[^\\/?%*:|"<>\.]+$/;
			if(!regex.test(message.text)) {
				bot.sendMessage(chatId, "Wrong directory name..");
				deferred.reject();
			} else {
				var dirpath = currentDir + '/' + message.text;
				fs.mkdir(dirpath, function (err) {
					if (err) {
						return deferred.reject(err);
					}
					deferred.resolve(dirpath);
				});
			}
		});
	})
	.catch(function (err) {
		deferred.reject(err);
	})

	return deferred.promise;
}

function dialogDir(chatId) {
	var dirpath;
	createDirName(chatId).then(function (newdir) {
		dirpath = newdir;
		return createDirIcon(chatId, dirpath);
	})
	.then(function () {
		return bot.sendMessage(chatId, "New directory is added successfully");
	})
	.then(function () {
		sendContentList(chatId, currentDir);
	})
	.catch(function (err) {
		if (dirpath) {
			exec("rm -rf " + dirpath);
		}
		bot.sendMessage(chatId, "New directory can't be added. Try /add again.");
	});
}

function renameFile(oldpath, newpath) {
  var deferred = Q.defer();
  console.log('oldpath: ' + oldpath);
  console.log('newpath: ' + newpath);
  fs.rename(oldpath, newpath, function (err) {
    if (err) {
      return deferred.reject(err.stack);
    }
    return deferred.resolve();
  });

  return deferred.promise;
}

function createFile(chatId, dirPath) {
	var deferred = Q.defer();

	var dirname;
	var diricon;
	bot.sendMessage(chatId, "What do you want to upload? Please send the file", {
    reply_markup: {
			force_reply: true
		}
	})
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

		bot.onReplyToMessage(chatId, messageId, function (message) {
			var fileId;
			if (message.photo) {
				if (!message.photo.length) {
					return deferred.reject();
				}
				fileId = message.photo.pop().file_id;
			} else if (message.video) {
				fileId = message.video.file_id;
			} else if (message.audio) {
				fileId = message.audio.file_id;
			} else if (message.voice) {
				fileId = message.voice.file_id;
			} else if (message.document) {
				fileId = message.document.file_id;
			} else {
				return deferred.reject();
			}

			if (!fileId) {
				return deferred.reject();
			}

			bot.downloadFile(fileId, dirPath)
			.then(function (filepath) {
				var origname = filepath.split('/').pop();
				var tmps = origname.split('.');
				var newname;
				if (tmps.length == 1) {
					newname = tmps[0] + '_' + Date.now();
				} else {
					newname = tmps[0] + '_' + Date.now() + '.' + tmps[1];
				}
				var newpath = currentDir + '/' + newname;

				return renameFile(filepath, newpath);
			})
			.then(function () {
				console.log('success to move folder icon');
				deferred.resolve();
			})
			.catch(function (err) {
				console.log('fail to move folder icon: ', err);
				deferred.reject(err);
			});
		});
	})
	.catch(function (err) {
		deferred.reject(err);
	});

	return deferred.promise;
}


function dialogFile(chatId) {
	createFile(chatId, currentDir)
	.then(function () {
		return bot.sendMessage(chatId, "New file is added successfully");
	})
	.then(function () {
		sendContentList(chatId, currentDir);
	})
	.catch(function (err) {
		bot.sendMessage(chatId, "New file can't be added. Try /add again.");
	});

}

function add(msg, match) {
	var chatId = msg.from.id;

	console.log(msg);
	bot.sendMessage(chatId, "which type do you want to add in this directory?\n\nenter 1 if directory. \nenter 2 if file.", {
		/*
    reply_markup: {
			keyboard: [["directory", "file"]],
			one_time_keyboard: true
    }
		*/
    reply_markup: {
			force_reply: true
		}
	})
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

		bot.onReplyToMessage(chatId, messageId, function (message) {
			console.log('reply to message');
			var isDir = 0;
			if (message.text == '1') {
				isDir = 1;
			} else if (message.text == '2') {
				isDir = 0;
			}
			bot.sendMessage(chatId, "You select '" + (isDir? 'directory': 'file') + "' for adding here")
			.then(function () {
				if (isDir) {
					dialogDir(chatId);
				} else {
					dialogFile(chatId);
				}
			})
			.catch(function (err) {
				console.log(err);
			});
		});
	})
	.catch(function (err) {
		winston.info(err);
	});
}

function del(msg, match) {
	var chatId = msg.from.id;

	bot.sendMessage(chatId, "select number of file or dir that you want to remove.", {
    reply_markup: {
			force_reply: true
    }
	})
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

		bot.onReplyToMessage(chatId, messageId, function (message) {
			var index = parseInt(message.text) - 1;
			if (index < 0 || index >= currentFiles.length) {
				return bot.sendMessage(chatId, name + ' is invalid number. please check it again.');
			}

			var name = currentFiles[index].name;
			var filepath = currentDir + '/' + name;

			exec("rm -rf " + filepath, function (err, stdout, stderr) {
				if (err) {
					bot.sendMessage(chatId, name + ' isn\'t removed. Something wrong... ');
				} else {
					bot.sendMessage(chatId, name + ' is removed successfully');
					sendContentList(chatId, currentDir);
				}
			});
		});
	})
	.catch(function (err) {
		winston.info(err);
	});
}

function file(msg, match) {
	var chatId = msg.from.id;

	var command = '/file__';
	var selIndex = msg.text.indexOf(command);
	var selName = msg.text.slice(selIndex + command.length);

	var found = _.find(currentFiles, function (file) {
		/*
		if (file.name.indexOf(selName) < 0) {
			return false;
		}
		*/
		var tmp = file.name.split('.');
		console.log('tmp[0]: ', tmp[0], ' file.name: ', file.name);
		if (selName !== tmp[0]) {
			return false;
		}

		console.log('Found in list: ', file);
		return true;
	});

	if (!found) {
		return bot.sendMessage(chatId, "Request file doesn't exist in this directory.");
	}

	bot.sendMessage(chatId, 'I am sending ' + found.name + ' to you. File size is '+ found.size + ' bytes. Please wait a moment...')
	.then(function () {
		var filepath = currentDir + '/' + found.name;
		if (found.mimetype.indexOf('image') >= 0) {
			return bot.sendPhoto(chatId, filepath);
		} else if (found.mimetype.indexOf('video') >= 0) {
			return bot.sendVideo(chatId, filepath);
		} else if (found.mimetype.indexOf('audio') >= 0) {
			return bot.sendAudio(chatId, filepath);
		}	else {
			return bot.sendDocument(chatId, filepath);
		}
	})
	.catch(function (err) {
		console.log(err);
		bot.sendMessage(chatId, "Something wrong...");
	});
}

function sendContentList(chatId, dir) {
	var message = 'You are in this directory.\n\n';
	message += currentDir + '\n\n';
	getDirList(currentDir).then(function (files) {
		console.log('files: ', files);
		currentFiles = files;
		message += convertToMessage(files);
		if (!dirStack.length) {
			// root dir
			message += '\nThis is root directory';
		} else {
			message += '\nTo go back, press /back';
		}
		message += '\n\nIf you want to add file or dir, press /add. If you want to delete file or dir, press /del\n';
		bot.sendMessage(chatId, message, { parse_mode: "HTML"});
	})
	.catch(function (err) {
		console.log(err.stack);
		bot.sendMessage(chatId, 'Invalid directory is requested\n\n' + currentDir + '\n\nIf you want to list root directory, press /start');
		currentDir = dirStack.pop();
	});
}

function dir(msg, match) {
	var chatId = msg.from.id;

	var command = '/dir__';
	var dirIndex = msg.text.indexOf(command);
	var dirname = msg.text.slice(dirIndex + command.length);

	dirStack.push(currentDir);
	currentDir = currentDir + '/' + dirname;

	sendContentList(chatId, currentDir);
}

function list(msg, match) {
	var chatId = msg.from.id;
	sendContentList(chatId, currentDir);
}

function back(msg, match) {
	var chatId = msg.from.id;

	if (!dirStack.length) {
		return bot.sendMessage(chatId, 'This is root directory. we can\'t go back any more');
	}
	currentDir = dirStack.pop();
	
	sendContentList(chatId, currentDir);
}

function sendBookmarkList(chatId, dir)
{
	getDirList(dir).then(function (files) {
		// we only deal dir type for bookmark
		currentBms = _.filter(files, function (file) {
			if (file.type == 0) {
				file.path = dir + '/' + file.name + '/.icon.url';
				file.url = fs.readFileSync(file.path, { encoding: 'utf8' });
				file.url = file.url.replace(/\n$/, '');
			}
			return file.type == 0;
		});
		console.log(currentBms);
		// message setting

		var message;
		if (!currentBms.length) {
			message = "There is no bookmarks...\n";
		} else {
			message = 'This is current bookmarks. (/bmlist)\n\n';
			_.each(currentBms, function (bm, index) {
				message += (index + 1) + '. ' + bm.name +  '(' + bm.url + ')\n';
			});
		}

		message += '\nYou can add new bookmark by /bmadd. Or you can delete a bookmark by /bmdel.\n';
		if (currentBms.length) {
			message += '\nYou can check each bookmark by clicking follow buttons.';
		}

		// custom keyboard setting
		var bmkbd = [];
		var row = [];
		var colcount = 1;
		_.each(currentBms, function (bm, index) {
			if (index && index % colcount == 0) {
				bmkbd.push(row);
				row = [];
			}
			var seq = index % colcount;
			row.push({ text: (index + 1) + '. ' + bm.name, url: bm.url });
		});
		bmkbd.push(row);

		var option = {
			parse_mode: "HTML",
			disable_web_page_preview: true
		};
		if (currentBms.length) {
			option.reply_markup = {
				inline_keyboard: bmkbd
			};
		}

		bot.sendMessage(chatId, message, option);
	})
	.catch(function (err) {
		console.log(err);
		bot.sendMessage(chatId, "Something Wrong...");
	});

}

/* bookmark stuff */
function bmlist(msg, match) {
	var chatId = msg.from.id;
	sendBookmarkList(chatId, bookmarkDir);
}

function createBookmarkName(chatId) {
	var deferred = Q.defer();

	var dirname;
	var diricon;
	bot.sendMessage(chatId, "What is name of new bookmark?", {
    reply_markup: {
			force_reply: true
		}
	})
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

		bot.onReplyToMessage(chatId, messageId, function (message) {
			var regex = /^[^\\/?%*:|"<>\.]+$/;
			if(!regex.test(message.text)) {
				bot.sendMessage(chatId, "Wrong bookmark name..");
				deferred.reject();
			} else {
				var dirpath = bookmarkDir + '/' + message.text;
				fs.mkdir(dirpath, function (err) {
					if (err) {
						return deferred.reject(err);
					}
					deferred.resolve(dirpath);
				});
			}
		});
	})
	.catch(function (err) {
		deferred.reject(err);
	})

	return deferred.promise;
}

function createBookmarkUrl(chatId, bmPath) {
	var deferred = Q.defer();

	var dirname;
	var diricon;
	bot.sendMessage(chatId, "What is url of new bookmark?", {
    reply_markup: {
			force_reply: true
		}
	})
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

		bot.onReplyToMessage(chatId, messageId, function (message) {
			var regex = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([/\w \.-]*)*\/?$/
			if(!regex.test(message.text)) {
				bot.sendMessage(chatId, "Wrong bookmark url..").then(function () {
					deferred.reject();
				})
				.catch(function (err) {
					deferred.reject();
				});
			} else {
				fs.writeFile(bmPath + '/.icon.url', message.text, { encoding: 'utf8' }, function (err) {
					if (err) {
						return deferred.reject(err);
					}
					deferred.resolve();
				});
			}
		});
	})
	.catch(function (err) {
		deferred.reject(err);
	})

	return deferred.promise;
}

function bmadd(msg, match) {
	var chatId = msg.from.id;

	var bmPath;
	createBookmarkName(chatId).then(function (path) {
		bmPath = path;
		return createBookmarkUrl(chatId, bmPath);
	})
	.then(function () {
		return createDirIcon(chatId, bmPath); 
	})
	.then(function () {
		return bot.sendMessage(chatId, "New bookmark is added successfully");
	})
	.then(function () {
		sendBookmarkList(chatId, bookmarkDir);
	})
	.catch(function (err) {
		if (bmPath) {
			exec("rm -rf " + bmPath);
		}
		bot.sendMessage(chatId, "New bookmark can't be added. Try /bmadd again.");
	});
}

function bmdel(msg, match) {
	var chatId = msg.from.id;

	bot.sendMessage(chatId, "select bookmark number that you want to remove.", {
    reply_markup: {
			force_reply: true
    }
	})
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

		bot.onReplyToMessage(chatId, messageId, function (message) {
			var index = parseInt(message.text) - 1;
			if (index < 0 || index >= currentBms.length) {
				return bot.sendMessage(chatId, name + ' is invalid number. please check it again.');
			}

			var name = currentBms[index].name;
			var filepath = bookmarkDir + '/' + name;

			exec("rm -rf " + filepath, function (err, stdout, stderr) {
				if (err) {
					bot.sendMessage(chatId, name + ' isn\'t removed. Something wrong... ');
				} else {
					bot.sendMessage(chatId, name + ' is removed successfully');
					sendBookmarkList(chatId, bookmarkDir);
				}
			});
		});
	})
	.catch(function (err) {
		winston.info(err);
	});
}


var initialize = function () {
	bot = new telebot(token, { polling: true });
	bot.getMe().then(function (msg) {
		winston.log('bot info: ', JSON.stringify(msg));
	}).catch(function (err) {
		winston.log('bot err: ', err);
	});

	bot.onText(/\/start/, start);
	bot.onText(/\/add/, add);
	bot.onText(/\/del/, del);
	bot.onText(/\/dir__/, dir);
	bot.onText(/\/file__/, file);
	bot.onText(/\/list/, list);
	bot.onText(/\/back/, back);

	bot.onText(/\/bmlist/, bmlist);
	bot.onText(/\/bmadd/, bmadd);
	bot.onText(/\/bmdel/, bmdel);
	//bot.onText(/^([0-9]|[1-9][0-9]|[1-9][0-9][0-9])$/, select);
	//bot.onText(/\/f([0-9]|[1-9][0-9]|[1-9][0-9][0-9])$/, menuadd);

	return true;
}

module.exports.run = initialize;
