var fs = require('fs');
var fsExtra = require('fs-extra');
var Q = require('q');
var _ = require('underscore');
var mime = require('mime');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var multer = require('multer');
var info = require('./shellinfo');



//TODO 'contentDirPrefix' should be replace to explorer's base path 
var contentDirPrefix = _.findWhere(info.mediaPaths, { name: "content" }).value;
var bgimageDirPrefix = _.findWhere(info.mediaPaths, { name: "background" }).value;
var bookmarkDirPrefix = process.env.NEMOUX_BOOKMARKS_PATH ? process.env.NEMOUX_BOOKMARKS_PATH: __dirname + '/bookmarks';
var shellIconDirPrefix = process.env.NEMOUX_SHELLICONS_PATH ? process.env.NEMOUX_SHELLICONS_PATH: "/usr/share/steshell/main-icons/";

var contentTmpPrefix = './uploads';
var contentIconTmpPrefix = './uploads/__icons__';
var bookmarkTmpPrefix = './uploads';
var exec = require('child_process').exec;
var ringIconName = ".ring.svg";
var bookmarkName = ".icon.url"; 

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, contentTmpPrefix);
  },
  filename: function (req, file, cb) {
    var newname;
    var tmps = file.originalname.split('.'); 

    if (tmps.length == 1) {
      newname = file.originalname + '_' + Date.now();
    } else {
      newname = tmps[0] + '_' + Date.now() + '.' + tmps[1];
    }
    cb(null, newname);
  }
});
var upload = multer({ storage: storage })

function getFileInfo(file) {
  var fullPath = this.path + '/' + file;
  var fileInfo = {};
  return Q.nfcall(fs.stat, (fullPath)).then(function (stats) {
    fileInfo.name = file;
    fileInfo.size = stats.size;
    fileInfo.createdAt = stats.birthtime;
    if (stats.isDirectory()) {
      return Q.nfcall(fs.readdir, fullPath)
        .then(function (subfiles) {
          var length = 0;
          _.each(subfiles, function (subfile) {
            console.log(subfile);
            if (subfile.charAt(0) != '.') {
              length++;
            }
          });
          console.log(length);
          fileInfo.count = length;
          fileInfo.type = 0;

          return fileInfo;
        });
    }
    fileInfo.mimetype = mime.lookup(fullPath);
    fileInfo.type = 1;
    return fileInfo;
  });
}

function getDirList(path) {
  var deferred = Q.defer();
  var getFileInfoBind = _.bind(getFileInfo, { path: path });

  Q.nfcall(fs.readdir, path)
  .then(function (files) {
    return _.map(files, getFileInfoBind);
  })
  .then(Q.all)
  .then(function (files) {
		var filtered = _.filter(files, function (file) {
			return file.name[0] === '.' ? false: true;
		});

    deferred.resolve(filtered);
  })
  .catch(function (err) {
    deferred.reject(err);
  });

  return deferred.promise;
}

function moveFile(file) {
  var oldpath = contentTmpPrefix + '/' + file.filename;
  var newpath = this.dir + '/' + file.filename;

  var deferred = Q.defer();
  console.log(oldpath);
  console.log(newpath);
  fs.rename(oldpath, newpath, function (err) {
    if (err) {
      return deferred.reject(err);
    }
    deferred.resolve();
  });

  return deferred.promise;
}

function moveFolderIconFile(file, dir) {
	var icontype = file.filename.split('.').pop();
	file.iconType = icontype;

	console.log("icontype: " + icontype);

  var oldpath = contentTmpPrefix + '/' + file.filename;
  var newpath;
  var newpath2;
 
	if (icontype == 'svg') {
		newpath = dir + '/.icon.svg';
	} else {
		newpath = dir + '/.icon.png';
	}

  var deferred = Q.defer();
  console.log('oldpath: ' + oldpath);
  console.log('newpath: ' + newpath);
  fs.rename(oldpath, newpath, function (err) {
    if (err) {
      return deferred.reject(err.stack);
    }

    return deferred.resolve();
  });

  return deferred.promise;
}

function getIconName(path){
	var isIconPng = false;
	var isIconSvg = false;

	try {
		if (fs.statSync(path + '/.icon.png')) {
			isIconPng = true;
			icon = 'icon.png';
		}
	} catch (e) {
		isIconPng = false;
	}

	if (!isIconPng) {
		try {
			if (fs.statSync(path + '/.icon.svg')) {
				isIconSvg = true;
				icon = 'icon.svg';
			}
		} catch (e) {
			isIconSvg = false;
		}
	}

	if (!isIconPng && !isIconSvg) {
		icon = '';
	}

	return icon;
}

app.use('/static_content', express.static(contentDirPrefix));
app.use('/static_bookmark', express.static(bookmarkDirPrefix));
app.use('/static_bgimage', express.static(bgimageDirPrefix));
app.use('/static_icon', express.static(contentIconTmpPrefix));
app.use('/static_shellicon', express.static(shellIconDirPrefix));
app.use('/static', express.static('/'));
app.use('/', express.static(__dirname + '/public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  next();
});

app.use('/shell', require('./shell')(app));
app.use('/file', require('./file')(app));
app.use('/auth', require('./auth')(app));

app.get('/t1/:type/:machineid', function (req, res) {
	var abpath;

	if (req.params.type == 'content') {
  	abpath = contentDirPrefix;
  	exec("rm -rf " + contentIconTmpPrefix + '/*');
	} else if (req.params.type == 'bgimage') {
  	abpath = bgimageDirPrefix;
	} else {
		console.log('unrecognized type');
    res.sendStatus(400);
	}

  if (req.query.path) {
    abpath += '/' + req.query.path;
  }

  return Q.nfcall(fs.stat, (abpath))
  .then(function (stats) {
    if (stats.isDirectory()) {
      return getDirList(abpath).then(function (files) {
				_.each(files, function (file) {
					if (req.params.type == 'content' && file.type == 0) {
						var icon;
						icon = getIconName(abpath + '/' + file.name);
						file.icon = file.name + '_' + Date.now();
						fs.symlinkSync(abpath + '/' + file.name + '/.' + icon, contentIconTmpPrefix + '/' + file.icon);
					}
				});
        res.json(files);
      })
      .catch(function (err) {
				console.log(err);
        res.sendStatus(400);
      });
    } 
  })
  .catch(function (err) {
		console.log(err);
    fs.mkdir(abpath, function (err) {
      if (err) {
        return res.sendStatus(400);
      }
      //send blank array
      res.json([]);
    });
  });
});

app.post('/t1/:type/:machineid', upload.any(), function (req, res) {
  console.log(req.params.machineid);
  console.log(req.query);

	var path;

	if (req.params.type == 'content') {
  	path = contentDirPrefix + '/' + req.query.path;
	} else if (req.params.type == 'bgimage') {
  	path = bgimageDirPrefix + '/' + req.query.path;
	} else {
		console.log('unrecognized type');
    res.sendStatus(400);
	}

  // in case of dir creation
  if (req.query.type == 0) {
		var ringOriginPath = process.env.NEMOUX_CONTENTS_RING;
		var ringCopyPath = path + '/' + ringIconName;

		console.log('mkdir: ', path);
    return fs.mkdir(path, function (err) {
      if (err) {
        console.log(err);
        return res.sendStatus(400);
      }
			return res.send({ path: path });

			/*
      return moveFolderIconFile(req.files[0], path)
				.then(function () {
					return Q.nfcall(fsExtra.copy, ringOriginPath, ringCopyPath);
				})
				.then(function () {
					res.sendStatus(200);
				})
				.catch(function (err) {
					console.log(err);
					res.sendStatus(400);
				});
				*/
		});
  }

  // in case of file creation
  var moveFileBind = _.bind(moveFile, { dir: path });

  console.log(req.files);
  return Q.all(_.map(req.files, moveFileBind))
    .then(function () {
      res.sendStatus(200);
    })
    .catch(function (err) {
      res.sendStatus(400);
    });
});

app.put('/t1/:type/:machineid', function (req, res) {

	var prefix;
	if (req.params.type == 'content') {
  	prefix = contentDirPrefix;
	} else if (req.params.type == 'bgimage') {
  	prefix = bgimageDirPrefix;
	} else {
		console.log('unrecognized type');
    res.sendStatus(400);
	}

  var oldpath = prefix + '/' + req.query.oldpath;
  var newpath = prefix + '/' + req.query.newpath;

  console.log(req.params);
  console.log(req.body);
  console.log(req.query);

  var stats = fs.statSync(oldpath);

  if (!stats) {
   return res.sendStatus(400);
  }

  fs.rename(oldpath, newpath, function (err) {
    if (err) {
      return res.sendStatus(400);
    }
    res.sendStatus(200);
  });
});

function removeByCli(path) {
  var deferred = Q.defer();
  exec("rm -rf " + path, function (err, stdout, stderr) {
    if (err) {
      deferred.reject(err);
    } else {
      deferred.resolve(stdout);
    }
  });
  return deferred.promise;
}

app.delete('/t1/:type/:machineid', function (req, res) {

	var prefix;
	if (req.params.type == 'content') {
  	prefix = contentDirPrefix;
	} else if (req.params.type == 'bgimage') {
  	prefix = bgimageDirPrefix;
	} else {
		console.log('unrecognized type');
    res.sendStatus(400);
	}

  var path = prefix + '/' + req.query.path;
	path = path.replace(/ /g, '\\ ');
  console.log(path);

  return removeByCli(path)
    .then(function (result) {
      res.sendStatus(200);
    })
    .catch(function (err) {
        return res.sendStatus(400);
    });
});

function sendBookmarkList(chatId, dir)
{
	getDirList(dir).then(function (files) {
		// we only deal dir type for bookmark
		currentBms = _.filter(files, function (file) {
			if (file.type == 0) {
				file.path = dir + '/' + file.name + '/.icon.url';
				file.url = fs.readFileSync(file.path, { encoding: 'utf8' });
				file.url = file.url.replace(/\n$/, '');
			}
			return file.type == 0;
		});
		console.log(currentBms);
		// message setting

		var message;
		if (!currentBms.length) {
			message = "There is no bookmarks...\n";
		} else {
			message = 'This is current bookmarks. (/bmlist)\n\n';
			_.each(currentBms, function (bm, index) {
				message += (index + 1) + '. ' + bm.name +  '(' + bm.url + ')\n';
			});
		}

		message += '\nYou can add new bookmark by /bmadd. Or you can delete a bookmark by /bmdel.\n';
		if (currentBms.length) {
			message += '\nYou can check each bookmark by clicking follow buttons.';
		}

		// custom keyboard setting
		var bmkbd = [];
		var row = [];
		var colcount = 1;
		_.each(currentBms, function (bm, index) {
			if (index && index % colcount == 0) {
				bmkbd.push(row);
				row = [];
			}
			var seq = index % colcount;
			row.push({ text: (index + 1) + '. ' + bm.name, url: bm.url });
		});
		bmkbd.push(row);

		var option = {
			parse_mode: "HTML",
			disable_web_page_preview: true
		};
		if (currentBms.length) {
			option.reply_markup = {
				inline_keyboard: bmkbd
			};
		}

		bot.sendMessage(chatId, message, option);
	})
	.catch(function (err) {
		console.log(err);
		bot.sendMessage(chatId, "Something Wrong...");
	});

}

// Bookmarks stuff
app.get('/t2/bookmark/:machineid', function (req, res) {

  return Q.nfcall(fs.stat, (bookmarkDirPrefix))
  .then(function (stats) {
    if (stats.isDirectory()) {
      return getDirList(bookmarkDirPrefix).then(function (bms) {
				currentBms = _.filter(bms, function (bm) {
					if (bm.type == 0) {
						var path = bookmarkDirPrefix + '/' + bm.name;
						try {
							bm.url = fs.readFileSync(path + '/.icon.url', { encoding: 'utf8' });
						} catch (e) {
							bm.url = "http://www.google.com?#q=" + bm.name;
						}
						bm.url = bm.url.replace(/\n$/, '');

						var icon = getIconName(path);
						bm.icon = bm.name + '_' + Date.now();
						fs.symlinkSync(path + '/.' + icon, contentIconTmpPrefix + '/' + bm.icon);
					}
					return bm.type == 0;
				});
        res.json(currentBms);
      })
      .catch(function (err) {
				console.log(err.stack);
        res.sendStatus(400);
      });
    } 
  })
  .catch(function (err) {
		console.log(err.stack);
    fs.mkdir(abpath, function (err) {
      if (err) {
        return res.sendStatus(400);
      }
      //send blank array
      res.json([]);
    });
  });
});

app.post('/t2/bookmark/:machineid', upload.any(), function (req, res) {
  console.log(req.params.machineid);
  console.log(req.query);

  var bmPath = bookmarkDirPrefix + '/' + req.query.dir;

  // in case of dir creation
	var ringOriginPath = process.env.NEMOUX_CONTENTS_RING;
	var ringCopyPath = bmPath + '/' + ringIconName;

	return fs.mkdir(bmPath, function (err) {
		if (err) {
			console.log(err);
			return res.sendStatus(400);
		}

		return moveFolderIconFile(req.files[0], bmPath)
			.then(function () {
				return Q.nfcall(fsExtra.copy, ringOriginPath, ringCopyPath);
			})
			.then(function () {
				fs.writeFileSync(bmPath + '/' + bookmarkName, req.query.url, { encoding: 'utf8' });
				res.sendStatus(200);
			})
			.catch(function (err) {
				exec("rm -rf " + bmPath);
				console.log(err);
				res.sendStatus(400);
			});
	});
});

app.put('/t2/bookmark/:machineid', function (req, res) {

  var oldpath = bookmarkDirPrefix + '/' + req.query.olddir;
  var newpath = bookmarkDirPrefix + '/' + (req.query.newdir ? req.query.newdir: req.query.olddir);

	/*
  console.log(req.params);
  console.log(req.body);
  console.log(req.query);
	*/

	console.log(oldpath);
	console.log(newpath);
  var stats = fs.statSync(oldpath);

  if (!stats) {
   return res.sendStatus(400);
  }
	Q.nfcall(fs.rename, oldpath, newpath)
	.then(function () {
		if (req.query.newurl) {
			fs.writeFileSync(newpath + '/' + bookmarkName, req.query.newurl, { encoding: 'utf8' });
		}
		res.sendStatus(200);
	})
	.catch(function () {
  	res.sendStatus(400);
	});
});

app.delete('/t2/bookmark/:machineid', function (req, res) {
  var bmPath = bookmarkDirPrefix + '/' + req.query.dir;
  console.log(bmPath);

  return removeByCli(bmPath)
    .then(function (result) {
      res.sendStatus(200);
    })
    .catch(function (err) {
        return res.sendStatus(400);
    });
});

process.on('uncaughtException', function (err) {
	console.log('uncaughtException: ' + err);
});

var server = app.listen(8000, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log('nemoeditor backend server at http://%s:%s', host, port);
});
