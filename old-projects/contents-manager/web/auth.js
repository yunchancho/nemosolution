const _ = require('underscore');
const express = require('express');
const router = express.Router();
const info = require('./shellinfo.json');

var tmpId = "admin";
var tmpPw = "nemo79";

router.post('/login', (req, res) => {

	var auth = req.body;

	if (!auth.id || !auth.pw) {
		return res.send({ result: false });
	}

	if ((auth.id != tmpId) || (auth.pw != tmpPw)) {
		return res.send({ result: false });
	}

	return res.send({ result: true });
});

module.exports = function (inst) {
	app = inst;
	return router;
};
