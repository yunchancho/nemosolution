const fs = require('fs');
const fsExtra = require('fs-extra');
const express = require('express');
const router = express.Router();
const info = require('./shellinfo.json');
const multer = require('multer');
const _ = require('underscore');
const Q = require('q');

var fileTmpPrefix = './uploads';
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, fileTmpPrefix);
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});

function moveFile(srcPath, destPath) {
  var deferred = Q.defer();
  console.log(srcPath);
  console.log(destPath);
  fs.rename(srcPath, destPath, function (err) {
    if (err) {
      return deferred.reject(err);
    }
    deferred.resolve();
  });

  return deferred.promise;
}

var upload = multer({ storage: storage });

router.post('/single', upload.any(), (req, res) => {
	var mediaPath = _.findWhere(info.mediaPaths, { name: req.query.type });
	var tmpPath = req.files[0].destination + '/' + req.files[0].filename;
	var newPath = mediaPath.value + '/' + req.query.filename;

	return moveFile(tmpPath, newPath)
		.then(function () {
      res.send({
				path: newPath
			});
		})
		.catch(function (err) {
      res.sendStatus(400);
		});
});

module.exports = function (inst) {
	app = inst;
	return router;
};
