angular.module('nemocm', ['ionic', 'ngResource', 'nemocm.controllers', 'nemocm.services', 'nemocm.directives', 'nemocm.filters', 'nemocm.constants'])

.run(function($ionicPlatform, $rootScope, Machine, AppConstants) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

  $rootScope.machineid = Machine.getId();
  $rootScope.contentServerAddr = AppConstants.ContentServerAddr;
	$rootScope.services = [];
	$rootScope.onlines = [];
})

.config(function($stateProvider, $urlRouterProvider, AppConstants) {

  $stateProvider
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'MenuCtrl'
  })

	.state('app.home', {
		url: '/home',
		views: {
			'menuContent': {
				templateUrl: 'templates/home.html',
				controller: 'HomeCtrl'
			}
		}
	})

  .state('app.groups', {
    //cache: false,
    url: '/groups/:key',
    views: {
      'menuContent': {
        templateUrl: 'templates/groups.html',
        controller: 'GroupsCtrl'
      }
    }
  })

  .state('app.contents', {
    //cache: false,
    url: '/contents/:key',
    views: {
      'menuContent': {
        templateUrl: 'templates/contents.html',
        controller: 'ContentsCtrl'
      }
    },
    params: {
      dir: null
    }
  })

  .state('app.bookmarks', {
    //cache: false,
    url: '/bookmarks/:key',
    views: {
      'menuContent': {
        templateUrl: 'templates/bookmarks.html',
        controller: 'BookmarksCtrl'
      }
    },
    params: {
      dir: null
    }
  })

  .state('app.bgimages', {
    //cache: false,
    url: '/bgimages/:key',
    views: {
      'menuContent': {
        templateUrl: 'templates/bgimages.html',
        controller: 'BgimagesCtrl'
      }
    },
    params: {
      dir: null
    }
  })

  .state('app.shells', {
    url: '/shells',
    views: {
      'menuContent': {
        templateUrl: 'templates/shells.html',
        controller: 'ShellsCtrl'
      }
    }
  })

  .state('app.networks', {
    cache: false,
    url: '/networks',
    views: {
      'menuContent': {
        templateUrl: 'templates/networks.html',
        controller: 'NetworksCtrl'
      }
    }
  })

  .state('app.apps', {
    url: '/apps',
    views: {
      'menuContent': {
        templateUrl: 'templates/apps.html',
        controller: 'AppsCtrl'
      }
    }
  })

	.state('app.group', {
		url: '/group/:key',
    views: {
      'menuContent': {
        templateUrl: 'templates/group.html',
        controller: 'GroupCtrl'
      }
    },
    params: {
      group: null
    }
	})

	.state('app.groupedit', {
		url: '/groupedit/:key',
    views: {
      'menuContent': {
        templateUrl: 'templates/groupedit.html',
        controller: 'GroupEditCtrl'
      }
    },
    params: {
			isNewGroup: false,
			group: null
    }
	})

	.state('app.action', {
		url: '/action/:key',
    views: {
      'menuContent': {
        templateUrl: 'templates/action.html',
        controller: 'ActionCtrl'
      }
    },
    params: {
      action: null,
			isNewAction: false,
			group: null,
			actions: []
    }
	})

  .state('app.content', {
    //cache: false,
    url: '/content',
    views: {
      'menuContent': {
        templateUrl: 'templates/content.html',
        controller: 'ContentCtrl'
      }
    },
    params: {
      file: null,
      dir: null
    }
  })

	.state('app.networkethernet', {
		url: '/networkethernet',
		views: {
			'menuContent': {
				templateUrl: 'templates/networkethernet.html',
				controller: 'NetworkEthernetCtrl'
			}
		},
    params: {
			service: {}
		}
	})

	.state('app.networkdetail', {
		url: '/networkdetail',
		views: {
			'menuContent': {
				templateUrl: 'templates/networkdetail.html',
				controller: 'NetworkDetailCtrl'
			}
		},
    params: {
			service: {}
		}
	})

  .state('app.bgimage', {
    //cache: false,
    url: '/bgimage',
    views: {
      'menuContent': {
        templateUrl: 'templates/bgimage.html',
        controller: 'BgimageCtrl'
      }
    },
    params: {
      file: null,
      dir: null
    }
  })

  .state('app.shell', {
    cache: false,
    url: '/shell/:key',
    views: {
      'menuContent': {
        templateUrl: 'templates/shell.html',
        controller: 'ShellCtrl'
      }
    },
    params: {
			namespace: {},
      items: [] 
    }
  })

  .state('app.shelldetail', {
    url: '/shelldetail/:key',
    views: {
      'menuContent': {
        templateUrl: 'templates/shelldetail.html',
        controller: 'ShellDetailCtrl'
      }
    },
    params: {
			namespace: {},
      item: ''
    }
  })

	.state('app.screen', {
		url: '/screen',
		views: {
			'menuContent': {
				templateUrl: 'templates/screen.html',
				controller: 'ScreenCtrl'
			}
		}
	})

	.state('app.screenbasic', {
		url: '/screenbasic',
		views: {
			'menuContent': {
				templateUrl: 'templates/screenbasic.html',
				controller: 'ScreenBasicCtrl'
			}
		}
	})

	.state('app.screenadvanced', {
		url: '/screenadvanced',
		views: {
			'menuContent': {
				templateUrl: 'templates/screenadvanced.html',
				controller: 'ScreenAdvancedCtrl'
			}
		}
	});

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
})

.config(function($ionicConfigProvider) {
  $ionicConfigProvider.views.maxCache(5);

  // note that you can also chain configs
  $ionicConfigProvider.backButton.text('');
  $ionicConfigProvider.backButton.previousTitleText(false);
});
