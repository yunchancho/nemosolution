angular.module('nemocm.constants', [])

.constant('ImagePathConstants', {
  folder: "img/folder.png",
  file: "img/file.png",
})

.constant('AppConstants', {
  ContentsMainKey: "main",
  ContentServerAddr: "./"
})

.constant('ShellConstants', {
  delimiter: " ",
	screenUiType: {
		layout: {
			id: 'layout',
			name: '화면',
		},
		input: {
			id: 'input',
			name: '터치'
		},
		background: {
			id: 'background',
			name: '배경'
		},
		fullscreen: {
			id: 'fullscreen',
			name: '제스처'
		}
	},
	fsTypes: [
		{ val: 'pick', name: 'picking' },
		{ val: 'pitch', name: 'pitching' }
	],
	fsSrcPositions: [
		{ val: 'top', name: 'top side' },
		{ val: 'bottom', name: 'bottom side' },
		{ val: 'left', name: 'left side' },
		{ val: 'right', name: 'right side' }
	],
	fsDestStaticPositions: [
		{ val: { nodeid: -1, screenid: -1 }, name: 'full screens' }
	],
	fsDestRotations: [
		{ val: 0, name: '0deg' },
		{ val: 90, name: '90deg' },
		{ val: 180, name: '180deg' },
		{ val: 270, name: '270deg' }
	],
	inputAreaStaticPositions: [
		{ val: { nodeid: -1, screenid: -1 }, name: 'full screens' }
	],
	bgAreaStaticPositions: [
		{ val: { nodeid: -1, screenid: -1 }, name: 'full screens' }
	],
	bgDefaultInfo: {
		imageFile: "/usr/share/steshell/backgrounds/background-1.png",
		imageApp: "/usr/bin/nemoback-play",
		menuApp: "/usr/bin/nemoback-ste",
	},
	screenNs: "/nemoshell/screen",
	screenModeNs: "/nemoshell/screen/mode",
	inputNs: "/nemoshell/input",
	touchInputNs: "/nemoshell/input/touch",
	bgNs: "/nemoshell/background",
	fsNs: "/nemoshell/fullscreen",
	groupNs: "/nemoshell/group",
	connmandNs: {
		state: "/nemoconnmand/state",
		services: "/nemoconnmand/services",
		wifi: "/nemoconnmand/technology/wifi",
		ethernet: "/nemoconnmand/technology/ethernet"
	},
	actionNs: "/nemoshell/action",
	electronPath: "/usr/bin/electron",
	webContainerPath: "/opt/web-container",
	ringPath: "/usr/share/steshell/main-icons/circle-m.svg",
	argsDelimiter: ";",
	defaultActionName: "No Name",
	defaultGroupName: "No Name",
	defaultActionIcon: "/usr/share/steshell/main-icons/default.png",
	defaultGroupIcon: "/usr/share/steshell/main-icons/default.png"

})

.constant('PathConstants', {
	basePath: 'static'
})

.constant('WSConstants', {
	uidPrefix: 'nemocm',
	port: 30000
});
