angular.module('nemocm.controllers')

.controller('BgimagesCtrl', function($scope, $rootScope, $q, $state, $stateParams, $timeout, $ionicModal, $ionicLoading, $ionicPopup, $ionicListDelegate, $window, Bgimage, ImagePathConstants, AppConstants) {
  var formdata = new FormData();

  function doUpload(dirpath, type) {
    var deferred = $q.defer();
    // type 0 : directory creation, type 1 : file creation
    Bgimage.add({ 
      path: dirpath,
      type: type,
      name: type == 0? $scope.newFolder.name: ''
    }, formdata).$promise
    .then(function (files) {
			console.log('uploaded: ' + JSON.stringify(files));
			formdata = new FormData();
      deferred.resolve();
    })
    .catch(function () {
      deferred.reject();
    });

    return deferred.promise;
  }

  function rename(oldname, newname) {
    Bgimage.edit({ 
      oldpath: $scope.dir + '/' + oldname,
      newpath: $scope.dir + '/' + newname 
    }, { 
      oldpath: $scope.dir + '/' + oldname,
      newpath: $scope.dir + '/' + newname 
    }).$promise
   .then(function (one) {
     return getCurrentDirList();
    })
    .then(function () {
      // TODO do specific ui job like close progress bar
    })
    .catch(function (err) {
      console.log('failed to rename');
    });
  }

  function getCurrentDirList() {
    var deferred = $q.defer();
    var file;
    console.log('current dir: ' + $scope.dir);
    $ionicLoading.show();
    Bgimage.query({
      path: $scope.dir
    })
    .$promise
    .then(function (files) {
      _.each(files, function (file) {
        time = moment(file.createdAt, moment.ISO_8601);
        file.createdAt = time.format("YYYY-MM-DD HH:mm:ss");

        file.path = $rootScope.contentServerAddr + 'static_bgimage';
        file.path += ($stateParams.dir? '/' + $stateParams.dir: '') + '/' + file.name;

        file.iconpath = $rootScope.contentServerAddr + 'static_bgimage';
        file.iconpath += '/' + file.icon;
        if (file.mimetype) {
          file.isImage = !file.mimetype.indexOf("image")? true: false;
          file.isVideo = !file.mimetype.indexOf("video")? true: false;
        } else {
          file.isImage = false;
          file.isVideo = false;
        }
      });

      files = _(files).chain().sortBy('createdAt').reverse().sortBy('type').value();

      $scope.files = [];
      $scope.files = files;
      deferred.resolve();
    })
    .catch(function (err) {
      // TODO add error handling code
      deferred.reject();
    })
    .finally(function () {
      $ionicLoading.hide();
    });

    return deferred.promise;
  }

  angular.extend($scope, {
    dir: $stateParams.dir? $stateParams.dir : '',
    files: [],
    images: {
      folder: ImagePathConstants.folder,
      file: ImagePathConstants.file,
    },
    shouldShowDelete: false,
    listCanSwipe: true,
    newName: {},
    newFiles: {files: {}},
    newFolder: {name: null, file: null},
    isRootPage: $stateParams.key == AppConstants.ContentsMainKey? true: false,
    modal: {}
    
  });

  angular.extend($scope, {
    addFolder: function() {
      if (!$scope.newFolder.name || !$scope.newFolder.file) {
        $window.alert("폴더 이름과 아이콘 이미지를 지정해주세요");
        return;
      }
      $ionicLoading.show();
      doUpload($scope.dir + '/' + $scope.newFolder.name, 0).then(function () {
        return getCurrentDirList();
      })
      .catch(function () {
      })
      .finally(function () {
        $ionicLoading.hide();
				$ionicListDelegate.closeOptionButtons();
        // close progress bar
      });
      $scope.closeFolderModal();
    },
    addFiles: function () {
      if (!$scope.newFiles.files) {
        $window.alert("파일을 선택해주세요");
        return;
      }
      $ionicLoading.show();
      doUpload($scope.dir, 1).then(function () {
        return getCurrentDirList();
      })
      .catch(function () {
      })
      .finally(function () {
        $ionicLoading.hide();
				$ionicListDelegate.closeOptionButtons();
        // close progress bar
      });
      $scope.closeFileModal();
    },
    editName: function(file) {
      $scope.shouldShowDelete = false;
      var option = {
        template: '<input type="text" ng-model="newName.value">',
        title: '이름 변경',
        subTitle: file.name + '<br> 새이름을 지정해주세요',
        scope: $scope,
        buttons: [
          { text: '취소' },
          {
            text: '<b>저장</b>',
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.newName.value) {
                //don't allow the user to close unless he enters foldername 
                e.preventDefault();
              } else {
                return $scope.newName.value;
              }
            }
          }
        ]
      };
      console.log(file);
      $ionicPopup.show(option)
      .then(function (newName) {
        if (!newName) {
          return $q.reject('no name');
        }
        return rename(file.name, newName);
      })
      .then(function () {
        return getCurrentDirList();
      })
      .catch(function (err) {
        console.log(err);
      })
      .finally(function () {
        // close progress bar
				$ionicListDelegate.closeOptionButtons();
        $scope.newName = {};
      });
    },
    loadFolderIconElement: function (element) {
      $scope.$apply(function ($scope) {
        // TODO to display preview image, 
        // we need to use FileReader
        $scope.newFolder.file = element.files;
        console.log($scope.newFolder.file);
        angular.forEach($scope.newFolder.file, function (value, key) {
          console.log("value: " + JSON.stringify(value) + ", key: " + key);
          formdata.append(key, value);
        });
      });
    },
    loadFilesElement: function (element) {
      $scope.$apply(function ($scope) {
        // TODO to display preview image, 
        // we need to use FileReader
        $scope.newFiles.files = element.files;
        console.log($scope.newFiles.files);
        angular.forEach($scope.newFiles.files, function (value, key) {
          console.log("value: " + JSON.stringify(value) + ", key: " + key);
          formdata.append(key, value);
        });
      });
    },
    showFolderModal: function () {
      $scope.shouldShowDelete = false;
      $scope.modal.folder.show();
    },
    showFileModal: function () {
      $scope.shouldShowDelete = false;
      $scope.modal.file.show();
    },
    closeFolderModal: function() {
      $scope.newFolder.file = null;
      $scope.newFolder.name = null;
      angular.element(document.getElementById('newfile'))[0].value = null;
			formdata = new FormData();
      $scope.modal.folder.hide();
    },
    closeFileModal: function() {
      $scope.newFiles.files = null;
      angular.element(document.getElementById('newfile'))[0].value = null;
			formdata = new FormData();
      $scope.modal.file.hide();
    },
    remove: function (file) {
      console.log("removed file: ", file);
			$scope.files.splice($scope.files.indexOf(file), 1);
			$timeout(function () {
				Bgimage.delete({
					path: $scope.dir + '/' + file.name,
				}).$promise
				.then(function () {
					//return getCurrentDirList();
				})
				.then(function () {
				})
				.catch(function () {
					console.log('failed to file or dir');
				});
			}, 1000);
    },
    enter: function (file) {
      var path = $scope.dir + '/' + file.name;
      // in case of directory
      if (file.type == 0) {
        $state.go('app.bgimages', {
          key: (Math.floor((Math.random() * 1000)+1) - 1 ),
          dir: path
        });
      } else {
        $state.go('app.bgimage', {
          file: file,
          dir: $scope.dir 
        });
      }
			$ionicListDelegate.closeOptionButtons();
    },
    showDeleteIcon: function () {
      $scope.shouldShowDelete = !$scope.shouldShowDelete;
    },
    loadNew: function () {
      getCurrentDirList()
      .finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
      });
    },
  });

  $ionicModal.fromTemplateUrl('templates/addfile.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal.file = modal;
  });

  $ionicModal.fromTemplateUrl('templates/addfolder.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal.folder = modal;
  });

  getCurrentDirList();
})

.controller('BgimageCtrl', function($scope, $rootScope, $stateParams, $window) {
  angular.extend($scope, {
    file: $stateParams.file,
    isImage: !$stateParams.file.mimetype.indexOf("image")? true: false,
    isVideo: !$stateParams.file.mimetype.indexOf("video")? true: false
  });

	angular.extend($scope, {
		enter: function() {
			$window.open($scope.path, '_blank', 'location=no');
		}
	});

  $scope.path = $rootScope.contentServerAddr  + 'static_bgimage';
  $scope.path += ($stateParams.dir? $stateParams.dir: '') + '/' + $scope.file.name;

  console.log($scope.file);
  console.log('image: ' + $scope.isImage);
  console.log('video: ' + $scope.isVideo);
  console.log('filepath: ' + $scope.path);

});
