angular.module('nemocm.controllers')

.controller('BookmarksCtrl', function($scope, $rootScope, $q, $state, $stateParams, $timeout, $ionicModal, $ionicLoading, $ionicPopup, $window, Bookmark, ImagePathConstants, AppConstants, $ionicListDelegate) {

  var formdata = new FormData();

  function doUpload(dir, url) {
    var deferred = $q.defer();
    // type 0 : directory creation, type 1 : file creation
    Bookmark.add({ 
      dir: dir,
			url: url
    }, formdata).$promise
    .then(function (files) {
			console.log('uploaded: ' + JSON.stringify(files));
			formdata = new FormData();
      deferred.resolve();
    })
    .catch(function () {
      deferred.reject();
    });

    return deferred.promise;
  }

  function changeInfo(oldbm, newbm) {
		var regex = /^[^\\/?%*:|"<>\.]+$/;
		if(newbm.name && !regex.test(newbm.name)) {
			$window.alert("부적절한 이름입니다. 다시 입력해주세요."); 
       return $q.reject('invalid name');
		}

		regex = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([/\w \.-]*)*\/?$/;
		if(newbm.url && !regex.test(newbm.url)) {
			$window.alert("부적절한 URL 입니다. 다시 입력해주세요.");
      return $q.reject('invalid url');
		}

    return Bookmark.edit({ 
      olddir: oldbm.name,
      newdir: newbm.name,
			newurl: newbm.url
    }, { 
      olddir: oldbm.name,
      newdir: newbm.name,
			newurl: newbm.url
    }).$promise;
  }

  function getBookmarkList() {
    var deferred = $q.defer();
    var file;

    $ionicLoading.show();
    Bookmark.query({
      path: $scope.dir
    }).$promise
		.then(function (bms) {
      _.each(bms, function (bm) {
        bm.iconpath = $rootScope.contentServerAddr + 'static_icon/' + bm.icon;
      });

      $scope.bms = [];
      $scope.bms = bms;
      deferred.resolve();
    })
    .catch(function (err) {
      // TODO add error handling code
      deferred.reject();
    })
    .finally(function () {
      $ionicLoading.hide();
    });

    return deferred.promise;
  }

	angular.extend($scope, {
    shouldShowDelete: false,
		listCanSwipe: true,
    newFolder: { name: null, file: null, url: null },
		editFolder: { name: null, file: null, url: null },
		bms: []
	});

	angular.extend($scope, {
		showBmModal: function () {
      $scope.shouldShowDelete = false;
      $scope.modal.bm.show();
    },
    closeBmModal: function() {
      $scope.newFolder = {};
      angular.element(document.getElementById('newfile'))[0].value = null;
      $scope.modal.bm.hide();
    },
    loadBmIconElement: function (element) {
      $scope.$apply(function ($scope) {
        $scope.newFolder.file = element.files;
        console.log($scope.newFolder.file);
        angular.forEach($scope.newFolder.file, function (value, key) {
          console.log("value: " + JSON.stringify(value) + ", key: " + key);
          formdata.append(key, value);
        });
      });
    },
    addBm: function() {
      if (!$scope.newFolder.name || !$scope.newFolder.url || !$scope.newFolder.file) {
        $window.alert("북마크 이름과 아이콘이미지를 지정해주세요");
        return;
      }

			var regex = /^[^\\/?%*:|"<>\.]+$/;
			if(!regex.test($scope.newFolder.name)) {
				$window.alert("부적절한 이름입니다. 다시 입력해주세요."); 
        return;
			}

			regex = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([/\w \.-]*)*\/?$/;
			if(!regex.test($scope.newFolder.url)) {
				$window.alert("부적절한 URL 입니다. 다시 입력해주세요."); 
        return;
			}

      $ionicLoading.show();
      doUpload($scope.newFolder.name, $scope.newFolder.url)
			.then(function () {
        return getBookmarkList();
      })
      .catch(function () {
      })
      .finally(function () {
        $ionicLoading.hide();
        // close progress bar
      });
      $scope.closeBmModal();
    },
    loadNew: function () {
      getBookmarkList()
      .finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
      });
    },
    remove: function (bm) {
      console.log("removed bookmark: ", bm);
			$scope.bms.splice($scope.bms.indexOf(bm), 1);
			$timeout(function () {
				Bookmark.delete({
					dir: bm.name
				}).$promise
				.then(function () {
					//return getCurrentDirList();
				})
				.then(function () {
				})
				.catch(function () {
					console.log('failed to file or dir');
				});
			}, 1000);
    },
    edit: function(bm) {
      $scope.shouldShowDelete = false;
			$scope.editFolder = bm;
			console.log('edit bm: ' + JSON.stringify($scope.editFolder));
      var option = {
        template: '<input type="text" ng-model="newFolder.name" placeholder="{{editFolder.name}}"><input type="text" ng-model="newFolder.url" placeholder="{{editFolder.url}}">',
        title: '북마크 수정',
        subTitle: bm.name + '<br><br> 이름 또는 URL 을 수정해주세요',
        scope: $scope,
        buttons: [
          { 
						text: '취소',
            onTap: function(e) {
							return false;
						}
				 	},
          {
            text: '<b>저장</b>',
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.newFolder.name && !$scope.newFolder.url) {
                //don't allow the user to close unless he enters foldername 
                e.preventDefault();
              } else {
                return $scope.newFolder;
              }
            }
          }
        ]
      };
      $ionicPopup.show(option)
      .then(function (newFolder) {
				if (newFolder) {
        	return changeInfo($scope.editFolder, $scope.newFolder)
					.then(function () {
						getBookmarkList();
					});
				}
      })
      .catch(function (err) {
        console.log(err);
      })
      .finally(function () {
        // close progress bar
				$scope.newFolder = {};
				$scope.editFolder = {};
				$ionicListDelegate.closeOptionButtons();
      });
    },
		enter: function(bm) {
			$window.open(bm.url, '_blank', 'location=no');
			$ionicListDelegate.closeOptionButtons();
		}
	});

  $ionicModal.fromTemplateUrl('templates/addbookmark.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal.bm = modal;
  });

	getBookmarkList();
});
