angular.module('nemocm.controllers')

.controller('GroupsCtrl', function($rootScope, $scope, $state, Shell, ShellUtil, ShellConstants, PathConstants, $ionicPopup, $ionicLoading, $q, $ionicListDelegate) {
	angular.extend($scope, {
		namespaces: [],
    listCanSwipe: true,
	});

	angular.extend($scope, {
		enter: function (group) {
      $ionicListDelegate.closeOptionButtons();
			$state.go('app.group', {
				key: (Math.floor((Math.random() * 1000)+1) - 1 ),
				group: group
			});
		},
		add: function () {
      $ionicListDelegate.closeOptionButtons();
			$state.go('app.groupedit', {
				key: (Math.floor((Math.random() * 1000)+1) - 1 ),
				group: {
					map: {}
				},
				isNewGroup: true
			});
		},
		edit: function (group) {
      $ionicListDelegate.closeOptionButtons();
			$state.go('app.groupedit', {
				key: (Math.floor((Math.random() * 1000)+1) - 1 ),
				group: group,
				isNewGroup: false
			});
		},
		remove: function (group) {
			var formdata = {
				id: group.id
			};
      var option = {
        title: '정말 삭제하시겠어요?',
        subTitle: group.name,
        scope: $scope,
        buttons: [
          {
					 	text: '취소',
						onTap: function (e) {
							return false;
						}
				 	},
          {
            text: '<b>확인</b>',
            type: 'button-assertive',
            onTap: function(e) {
            	return true;
            }
          }
        ]
      };
      $ionicPopup.show(option)
      .then(function (result) {
				if (result) {
					return Shell.delete({ nsIdx: $scope.groupNsIndex }, formdata).$promise
				} else {
          return $q.reject('user press cancel button');
				}
			})
			.then(function () {
				console.log('success to remove');
				$rootScope.allShellGroups = _.without($rootScope.allShellGroups, group);
			})
			.catch(function (err) {
				console.log('fail: ', err);
			});
		}
	});

	Shell.getNamespaces().$promise
	.then(function (namespaces) {
    $ionicLoading.show();
		console.log('get namespaces');
		$scope.namespaces = namespaces;
		$scope.groupNsIndex = namespaces.indexOf(ShellConstants.groupNs);
		if ($scope.groupNsIndex < 0) {
      return $q.reject('invalid namespace');
		}
		return Shell.get({ nsIdx: $scope.groupNsIndex }).$promise;
	})
	.then(function (groups) {
		console.log('current groups: ', groups);
		_.each(groups, function (group) {
			var kvs = ShellUtil.parseItemString(group);
			var obj = ShellUtil.makeSetFormdata(kvs);
			var icon = $rootScope.contentServerAddr + PathConstants.basePath;
			icon += obj.icon;

			obj.map = {
				iconRemoteUrl: icon
			};
			if (!obj.name) {
				obj.name = ShellConstants.defaultGroupName;
			}
			$rootScope.allShellGroups.push(obj);
		});

		$scope.actionNsIndex = $scope.namespaces.indexOf(ShellConstants.actionNs);
		return Shell.get({ nsIdx: $scope.actionNsIndex }).$promise;
	})
	.then(function (actions) {
		_.each(actions, function (action) {
			var kvs = ShellUtil.parseItemString(action);
			var obj = ShellUtil.makeSetFormdata(kvs);
			var icon = $rootScope.contentServerAddr + PathConstants.basePath;
			icon += obj.icon;
			obj.map = {
				iconRemoteUrl: icon
			};
			$rootScope.allShellActions.push(obj);
		});
	})
	.catch(function (err) {
		console.log(err);
	})
	.finally(function () {
    $ionicLoading.hide();
	});

	$rootScope.allShellGroups = [];
	$rootScope.allShellActions = [];
})

.controller('GroupEditCtrl', function ($scope, $q, $stateParams, Shell, ShellConstants, $ionicHistory, File, GUID, $ionicLoading, $window, $rootScope, PathConstants) {
  var formdata = new FormData();
	angular.extend($scope, {
		group: $stateParams.group,
    newGroup: { iconfile: null },
		isNew: $stateParams.isNewGroup
	});

	angular.extend($scope, {
    save: function() {
			console.log('edit group: ', $scope.group);
      if (!$scope.group.name) {
        $window.alert("폴더 이름과 아이콘 이미지를 지정해주세요");
        return;
      }
      $ionicLoading.show();

			var isFile = angular.element(document.getElementById('iconfile'))[0].value;
			var start = $q.when({ 
				path: $scope.group.icon ? $scope.group.icon: ShellConstants.defaultGroupIcon
			});
			if (isFile) {
				start = File.send({ 
					limit: 'single',
					type: 'icon',
					filename: $scope.group.map.icon
				}, formdata).$promise;
			}

			start.then(function (result) {
				console.log('uploaded: ' + result.path);
				$scope.group.icon = result.path;
				$scope.group.map.iconRemoteUrl	= $rootScope.contentServerAddr + PathConstants.basePath;
				$scope.group.map.iconRemoteUrl += result.path;

				return Shell.set({
				 	nsIdx: $scope.groupNsIndex
			 	}, _.omit($scope.group, 'map')).$promise;
			})
			.then(function () {
				formdata = new FormData();
				if ($scope.isNew) {
					$scope.allShellGroups.push($scope.group);
				}
				$ionicHistory.goBack();
				console.log('success');
			})
			.catch(function (err) {
				console.log(err);
			})
      .finally(function () {
        $ionicLoading.hide();
      });
    },
    loadGroupIconElement: function (element) {
      $scope.$apply(function ($scope) {
        // TODO to display preview image, 
        // we need to use FileReader
        $scope.newGroup.file = element.files;
        console.log($scope.newGroup.file);
        angular.forEach($scope.newGroup.file, function (value, key) {
          console.log("value: " + JSON.stringify(value) + ", key: " + key);
          formdata.append(key, value);
        });
      });
    },
	});

	Shell.getNamespaces().$promise
	.then(function (namespaces) {
		console.log('get namespaces');
		$scope.groupNsIndex = namespaces.indexOf(ShellConstants.groupNs);
	});

	if ($scope.isNew) {
		$scope.group.id = GUID.create();
		$scope.group.ring = ShellConstants.ringPath;
	}

	// TODO we need to set real file extension
	$scope.group.map.icon = 'icon-' + $scope.group.id + '.png';
})

.controller('GroupCtrl', function ($rootScope, $scope, $state, $stateParams, $ionicModal, $ionicListDelegate, $ionicPopup, Shell, ShellUtil, ShellConstants, PathConstants) {
	angular.extend($scope, {
		group: $stateParams.group,
		actions: [],
    listCanSwipe: true
	});

	angular.extend($scope, {
		enter: function (action) {
      $ionicListDelegate.closeOptionButtons();
			$state.go('app.action', {
				key: (Math.floor((Math.random() * 1000)+1) - 1 ),
				action: action,
				isNewAction: false,
				group: $scope.group,
				actions: $scope.actions
			});
		},
		add: function () {
      $ionicListDelegate.closeOptionButtons();
			$state.go('app.action', {
				key: (Math.floor((Math.random() * 1000)+1) - 1 ),
				action: {
					map: {}
				},
				isNewAction: true,
				group: $scope.group,
				actions: $scope.actions
			});
		},
		remove: function (action) {
			var formdata = {
				id: action.id
			};
      var option = {
        title: '정말 삭제하시겠어요?',
        subTitle: action.name,
        scope: $scope,
        buttons: [
          {
					 	text: '취소',
						onTap: function (e) {
							return false;
						}
				 	},
          {
            text: '<b>확인</b>',
            type: 'button-assertive',
            onTap: function(e) {
            	return true;
            }
          }
        ]
      };
      $ionicPopup.show(option)
      .then(function (result) {
				if (result) {
					return Shell.delete({ nsIdx: $scope.actionNsIndex }, formdata).$promise
				} else {
          return $q.reject('user press cancel button');
				}
			})
			.then(function () {
				console.log('success to remove');
				$scope.actions = _.without($scope.actions, action);
				var found = _.findWhere($scope.allShellActions, { id: action.id });
				$rootScope.allShellActions = _.without($scope.allShellActions, found);
				//$scope.actions.splice($scope.actions.indexOf(action), 1);
			})
			.catch(function (err) {
				console.log('fail: ', err);
			});
		},
	});

	_.each($rootScope.allShellActions, function (action) {
		if (action.group == $scope.group.id) {
			if (!action.name) {
				action.name = ShellConstants.defaultActionName;
			}
			$scope.actions.push(action);
		}
	});

	Shell.getNamespaces().$promise
	.then(function (namespaces) {
		console.log('get namespaces');
		$scope.actionNsIndex = namespaces.indexOf(ShellConstants.actionNs);
	});

	Shell.getActionTypes().$promise
	.then(function (types) {
		_.each($scope.actions, function (action) {
			action.map.typename = _.findWhere(types, { value: action.type }).name;
		});
	});
})

.controller('ActionCtrl', function ($rootScope, $scope, $q, $state, $stateParams, Shell, ShellConstants, $ionicPopover, $ionicHistory, $ionicPopup, File, Content, GUID, $ionicLoading, $window, $rootScope, PathConstants) {
  var formdata = new FormData();

	angular.extend($scope, {
		action: $stateParams.action,
		group: $stateParams.group,
		actions: $stateParams.actions,
		actionTypes: [],
    newAction: { iconfile: null },
		isNew: $stateParams.isNewAction
	});
	console.log('isNew: ', $scope.isNew);

	angular.extend($scope, {
    save: function() {
			console.log('new action: ', $scope.action);
      if (!$scope.action.name || !$scope.action.type) {
        $window.alert("폴더 이름과 아이콘 이미지를 지정해주세요");
        return;
      }
      $ionicLoading.show();

			setActionMetaData();
			var isFile = angular.element(document.getElementById('iconfile'))[0].value;
			var start = $q.when({ 
				path: $scope.action.icon ? $scope.action.icon: ShellConstants.defaultActionIcon
			});
			if (isFile) {
				start = File.send({ 
					limit: 'single',
					type: 'icon',
					filename: $scope.action.map.icon
				}, formdata).$promise;
			}

			start.then(function (result) {
				console.log('uploaded: ' + result.path);
				$scope.action.icon = result.path;
				$scope.action.map.iconRemoteUrl	= $rootScope.contentServerAddr + PathConstants.basePath;
				$scope.action.map.iconRemoteUrl += result.path;

				return Shell.set({
				 	nsIdx: $scope.actionNsIndex
			 	}, _.omit($scope.action, 'map')).$promise;
			})
			.then(function () {
				var typename = _.findWhere($scope.actionTypes, { value: $scope.action.type }).name;

				console.log('typename: ', typename);
				if (typename == 'content' && $scope.isNew) {
					console.log('request to create content directory');
					return Content.add({ type: 0, path: $scope.action.id }, formdata).$promise
						.then(function (result) {
							console.log('success to add dir:', result.path);
							var typename = _.findWhere($scope.actionTypes, { value: $scope.action.type }).name;
							if (typename == 'content' && $scope.isNew) {
								$scope.action.path = result.path;
							}
							return Shell.set({
								nsIdx: $scope.actionNsIndex
							}, _.omit($scope.action, 'map')).$promise;
						})
				 		.catch(function (err) {
							console.log(err);
						});	
				}
				return true;
			})
			.then(function (result) {
				formdata = new FormData();
				if ($scope.isNew) {
					$scope.actions.push($scope.action);
					$rootScope.allShellActions.push($scope.action);
				}
				$ionicHistory.goBack();
				console.log('success');
			})
			.catch(function (err) {
				console.log(err);
			})
      .finally(function () {
				$scope.newAction.file = null;
				$scope.newAction.name = null;
				angular.element(document.getElementById('iconfile'))[0].value = null;
				formdata = new FormData();
        $ionicLoading.hide();
      });
    },
    loadActionIconElement: function (element) {
      $scope.$apply(function ($scope) {
        // TODO to display preview image, 
        // we need to use FileReader
        $scope.newAction.file = element.files;
        console.log($scope.newAction.file);
        angular.forEach($scope.newAction.file, function (value, key) {
          console.log("value: " + JSON.stringify(value) + ", key: " + key);
          formdata.append(key, value);
        });
      });
    },
		remove: function () {
			var formdata = {
				id: $scope.action.id
			};
      var option = {
        title: '정말 삭제하시겠어요?',
        subTitle: $scope.action.name,
        scope: $scope,
        buttons: [
          {
					 	text: '취소',
						onTap: function (e) {
							return false;
						}
				 	},
          {
            text: '<b>확인</b>',
            type: 'button-assertive',
            onTap: function(e) {
            	return true;
            }
          }
        ]
      };
      $ionicPopup.show(option)
      .then(function (result) {
				if (result) {
					return Shell.delete({ nsIdx: $scope.actionNsIndex }, formdata).$promise
				} else {
          return $q.reject('user press cancel button');
				}
			})
			.then(function () {
				console.log('success to remove');
				$scope.actions = _.without($scope.actions, $scope.action);
				
				var found = _.findWhere($scope.allShellActions, { id: $scope.action.id });
				$rootScope.allShellActions = _.without($scope.allShellActions, found);
				$ionicHistory.goBack();
			})
			.catch(function (err) {
				console.log('fail: ', err);
			});
		},
		openPopoverGroup: function ($event) {
			$scope.__popover_group__.show($event);
		},
		closePopoverGroup: function () {
			$scope.__popover_group__.hide();
		},
		selectPopoverGroup: function (group) {
			// do something
			if ($scope.action.group != group.id) {
				if ($scope.originGroupId == group.id) {
					$scope.actions.push($scope.action);
				} else {
					$scope.actions = _.without($scope.actions, $scope.action);
				}
			}
			$scope.action.group = group.id;
			$scope.group = group;

			$scope.closePopoverGroup();
		},
		openPopoverActionType: function ($event) {
			$scope.__popover_type__.show($event);
		},
		closePopoverActionType: function () {
			$scope.__popover_type__.hide();
		},
		selectPopoverActionType: function (type) {
			// do something
			console.log('type: ', type);
			$scope.action.type = type.value;
			$scope.action.map.typename = type.value;
			$scope.action.map.typename = _.findWhere($scope.actionTypes, { value: type.value }).name;
			$scope.closePopoverActionType();
		},
		checkWebapp: function () {
			if ($scope.action.path == ShellConstants.electronPath) {
				$scope.action.map.isWebapp = true;
			} else {
				$scope.action.map.isWebapp = false;
			}
		},
		checkIcon: function () {
			var icon = $rootScope.contentServerAddr + PathConstants.basePath;
			icon += action.icon;
			obj.map.iconRemoteUrl = icon;
		},
		viewFiles: function () {
			$state.go('app.contents', {
				key: (Math.floor((Math.random() * 1000)+1) - 1 ),
				dir: $scope.action.id
			});
		}
	});

	function setActionMetaData () {
		if ($scope.action.map.isWebapp) {
			if ($scope.action.map.useWebContainer) {
				$scope.action.args = ShellConstants.webContainerPath + ShellConstants.argsDelimiter + $scope.action.map.args;
			} else {
				$scope.action.args = $scope.action.map.args;
			}
		}

		if ($scope.action.map.resize) {
			$scope.action.resize = 'on';
		} else {
			$scope.action.resize = 'off';
		}

		if ($scope.action.map.scale) {
			$scope.action.scale = 'on';
		} else {
			$scope.action.scale = 'off';
		}
	}

	$ionicPopover.fromTemplateUrl('templates/popover/group.html', {
		scope: $scope
  }).then(function(popover) {
    $scope.__popover_group__ = popover;
  });

	$ionicPopover.fromTemplateUrl('templates/popover/actiontype.html', {
		scope: $scope
  }).then(function(popover) {
    $scope.__popover_type__ = popover;
  });

	Shell.getNamespaces().$promise
	.then(function (namespaces) {
		console.log('get namespaces');
		$scope.actionNsIndex = namespaces.indexOf(ShellConstants.actionNs);
	});

	Shell.getActionTypes().$promise
	.then(function (types) {
		console.log('get action types: ', types);
		$scope.actionTypes = types;
		if (!$scope.isNew) {
			$scope.action.map.typename = _.findWhere(types, { value: $scope.action.type }).name;
		}
	});


	//$scope.action.map = {};	
	if (!$scope.isNew) {
		$scope.checkWebapp();

		if ($scope.action.args) {
			$scope.action.map.args = $scope.action.args;
			var args = $scope.action.map.args.split(ShellConstants.argsDelimiter);
			if (_.contains(args, ShellConstants.webContainerPath)) {
				args = _.without(args, ShellConstants.webContainerPath); 
				$scope.action.map.args = args.join(ShellConstants.argsDelimiter);
				$scope.action.map.useWebContainer = true;
			}
		}

		if (!$scope.action.resize || $scope.action.resize == 'on') {
			$scope.action.map.resize = true;
		} else if ($scope.action.resize == 'off') {
			$scope.action.map.resize = false;
		}

		if (!$scope.action.scale || $scope.action.scale == 'on') {
			$scope.action.map.scale = true;
		} else if ($scope.action.scale == 'off') {
			$scope.action.map.scale = false;
		}
	} else {
		$scope.action.id = GUID.create();
		$scope.action.group = $scope.group.id;
		$scope.action.ring = ShellConstants.ringPath;
		$scope.action.map.resize = true;
		$scope.action.map.scale = true;
	}
	// TODO we need to set real file extension
	$scope.action.map.icon = 'icon-' + $scope.action.id + '.png';
	$scope.originGroupId = $scope.group.id;

	console.log('action: ', $scope.action);
})

