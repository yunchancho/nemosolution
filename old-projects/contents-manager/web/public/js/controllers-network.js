angular.module('nemocm.controllers')

.controller('NetworksCtrl', function($scope, $ionicLoading, Shell, ShellUtil, ShellConstants, $ionicPopup, $q, $state, $location) {
	angular.extend($scope, {
		namespaces: [],
		services: [],
		onlines: [],
		isConnected: false,
		enable: {
			wifi: false,
			ethernet: false
		},
		secret: { password : '' }
	});

	angular.extend($scope, {
		refresh: function () {
			initNetwork();
		},
		existWifi: function () {
			var filtered = _.where($scope.services, { type: 'wifi' });
			return filtered.length ? true: false;
		},
		existEthernet: function () {
			var filtered = _.where($scope.services, { type: 'ethernet' });
			return filtered.length ? true: false;
		},
		selectEth: function (service) {
			console.log('selected service: ', service);
			$state.go('app.networkethernet', {
				service: service
			});
		},
		selectWifi: function (service) {
			var option = {
        template: '<input type="text" ng-model="secret.password">',
				title: service.name,
				subTitle: '와이파이 비밀번호를 입력해주세요.',
				scope: $scope,
        buttons: [
          { text: '취소' },
          {
            text: '완료',
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.secret.password) {
                //don't allow the user to close unless he enters foldername 
                e.preventDefault();
              } else {
                return $scope.secret.password;
              }
            }
          }
        ]
			};

			var promise;
			if (service.security == 'true') {
				promise = $ionicPopup.show(option).then(function (password) {
					if (!password) {
						return $q.reject("blank password");
					}

					return Shell.connectNetwork({ nsIdx: $scope.servicesNsIndex }, {
						path: service.path,
						passwd: password
					}).$promise;
				});
			} else {
				promise = Shell.connectNetwork({ nsIdx: $scope.servicesNsIndex }, {
					path: service.path
				}).$promise;
			}

			promise.then(function (data) {
				$ionicLoading.show();
				console.log("connect result: ", data);
				var kvs = ShellUtil.parseItemString(data.result);
				var obj = ShellUtil.makeSetFormdata(kvs);

				if (obj.result == 'true') {
					if ($location.host() == "127.0.0.1") {
						//$route.reload();
						return initNetwork()
					}

					//TODO popup msg
					console.log("REFESH ME!!");

				} else {
					return $q.reject("fail to connect " + service.path);
				}
			})
			.then(function () {
				console.log('success to connect new network');
			})
			.catch(function (err) {
				console.log(err);
			})
			.finally(function () {
				$ionicLoading.hide();
        $scope.secret = {};
			});
		},
		setWifi: function () {
			console.log('changed wifi: ', $scope.enable.wifi);
			$scope.wifiNsIndex = $scope.namespaces.indexOf(ShellConstants.connmandNs.wifi);
			$ionicLoading.show();
			Shell.setNetwork({ nsIdx: $scope.wifiNsIndex }, {
			 	Powered: $scope.enable.wifi? "on": "off"
		 	}).$promise
			.then(function (data) {
				console.log(data);
				var kvs = ShellUtil.parseItemString(data.result);
				var obj = ShellUtil.makeSetFormdata(kvs);
				if (obj.result == 'false') {
					$scope.enable.wifi = !$scope.enable.wifi;
					return $q.reject('fail to enable wifi to ' + enable.wifi);
				}
				console.log('init network');

				return initNetwork();
			})
			.catch(function (err) {
				console.log(err);
			})
			.finally(function () {
				$ionicLoading.hide();
			});
		},
		setEthernet: function () {
			console.log('changed ethernet: ', $scope.enable.ethernet);
			$scope.ethernetNsIndex = $scope.namespaces.indexOf(ShellConstants.connmandNs.ethernet);
			$ionicLoading.show();
			Shell.setNetwork({ nsIdx: $scope.ethernetNsIndex }, {
			 	Powered: $scope.enable.ethernet? "on": "off"
		 	}).$promise
			.then(function (data) {
				var kvs = ShellUtil.parseItemString(data.result);
				var obj = ShellUtil.makeSetFormdata(kvs);
				if (obj.result == 'false') {
					$scope.enable.ethernet = !$scope.enable.ethernet;
					return $q.reject('fail to enable ethernet to ' + enable.ethernet);
				}
				return initNetwork();
			})
			.catch(function (err) {
				console.log(err);
			})
			.finally(function () {
				$ionicLoading.hide();
			});
		},
		showDetail: function (service) {
			$state.go('app.networkdetail', {
				service: service
			});
		}
	});

	function initNetwork() {
		var deferred = $q.defer();

		Shell.getNamespaces().$promise
		.then(function (namespaces) {
			$ionicLoading.show();
			console.log('get namespaces');
			$scope.namespaces = namespaces;
			$scope.stateNsIndex = namespaces.indexOf(ShellConstants.connmandNs.state);
			if ($scope.stateNsIndex < 0) {
				return $q.reject('invalid namespace');
			}
			return Shell.getNetwork({ nsIdx: $scope.stateNsIndex }).$promise;
		})
		.then(function (data) {
			console.log("state: ", data);
			var kvs = ShellUtil.parseItemString(data.result);
			var obj = ShellUtil.makeSetFormdata(kvs);

			if (obj.result == 'true') {
				$scope.isConnected = true;
			}

			$scope.wifiNsIndex = $scope.namespaces.indexOf(ShellConstants.connmandNs.wifi);
			return Shell.getNetwork({ nsIdx: $scope.wifiNsIndex }).$promise;
		})
		.then(function (data) {
			console.log('wifi powered: ', data);
			var kvs = ShellUtil.parseItemString(data.result);
			var obj = ShellUtil.makeSetFormdata(kvs);
			if (obj.powered == 'true') {
				$scope.enable.wifi = true;
			} else {
				$scope.enable.wifi = false;
			}

			$scope.ethernetNsIndex = $scope.namespaces.indexOf(ShellConstants.connmandNs.ethernet);
			var promise;
			if ($scope.enable.wifi) {
				console.log('scan wifi');
				promise = Shell.scanNetwork({ nsIdx: $scope.wifiNsIndex }, {}).$promise
					.then(function (data) {
						console.log(data);
						return Shell.getNetwork({ nsIdx: $scope.ethernetNsIndex }).$promise;
					});
			} else {
				promise = Shell.getNetwork({ nsIdx: $scope.ethernetNsIndex }).$promise;
			}

			return promise;
		}) 
		.then(function (data) {
			console.log('ethernet powered: ', data);
			var kvs = ShellUtil.parseItemString(data.result);
			var obj = ShellUtil.makeSetFormdata(kvs);
			if (obj.powered == 'true') {
				$scope.enable.ethernet = true;
			} else {
				$scope.enable.ethernet = false;
			}

			$scope.servicesNsIndex = $scope.namespaces.indexOf(ShellConstants.connmandNs.services);
			return Shell.getNetwork({ nsIdx: $scope.servicesNsIndex }).$promise;
		})
		.then(function (data) {
			$scope.services = [];
			var services = data.result.split(';');
			_.each(services, function (service) {
				var str = service.trim();
				var kvs = ShellUtil.parseItemString(str);
				var obj = ShellUtil.makeSetFormdata(kvs);
				$scope.services.push(obj);
			});

			$scope.onlines = _.filter($scope.services, function (service) {
				return (service.state == 'online') || (service.state == 'ready');
			});

			console.log('network services: ', $scope.services);
			console.log('network onlines: ', $scope.onlines);


			_.each($scope.onlines, function (online) {
				$scope.services = _.without($scope.services, online);
			});

			deferred.resolve();
		})
		.catch(function (err) {
			console.log(err);
			deferred.reject();
		})
		.finally(function () {
			$ionicLoading.hide();
		});

		return deferred.promise;
	}

	initNetwork();
})

.controller('NetworkEthernetCtrl', function($scope, $ionicLoading, $stateParams, Shell, ShellUtil, ShellConstants, $ionicPopup, $q, $ionicHistory) {

	angular.extend($scope, {
		service: $stateParams.service,
		mode: { ethMode: 'dhcp' },
		namespaces: [],
		static: {}
	});

	angular.extend($scope, {
		apply: function () {
			var params = {};
			if ($scope.mode.ethMode == 'dhcp') {
				params = {
					path: $scope.service.path
				}
			} else {
				params = {
					path: $scope.service.path,
					address: $scope.static.ip,
					netmask: $scope.static.nm,
					nameserver: $scope.static.ns,
					gateway: $scope.static.gw
				}
			}

			$ionicLoading.show();

			var promise;
			if ($scope.service.connected == 'true') {
				promise = Shell.configEthernet({ nsIdx: $scope.servicesNsIndex }, params).$promise;
			} else {
				promise = Shell.connectNetwork({ nsIdx: $scope.servicesNsIndex }, {
					path: $scope.service.path
				}).$promise
				.then(function (data) {
					return Shell.configEthernet({ nsIdx: $scope.servicesNsIndex }, params).$promise;
				});
			}

			promise.then(function (data) {
				console.log('result: ', data);
			})
			.catch(function (err) {
				console.log(err);
			})
			.finally(function () {
				$ionicHistory.goBack();
				$ionicLoading.hide();
			});
		}
	});

	Shell.getNamespaces().$promise
	.then(function (namespaces) {
		console.log('get namespaces');
		$scope.namespaces = namespaces;
		$scope.servicesNsIndex = namespaces.indexOf(ShellConstants.connmandNs.services);
	});
})

.controller('NetworkDetailCtrl', function($scope, $stateParams, $ionicHistory, $ionicLoading, Shell, ShellConstants) {
	angular.extend($scope, {
		namespaces: [],
		service: $stateParams.service
	});

	angular.extend($scope, {
		disconnect: function () {
			$ionicLoading.show();
			Shell.disconnectNetwork({ nsIdx: $scope.servicesNsIndex }, {
			 	path: $scope.service.path
			}).$promise
			.then(function (data) {
				console.log('result: ', data);
			})
			.catch(function (err) {
				console.log(err);
			})
			.finally(function () {
				$stateParams.service.state = 'idle';
				$ionicLoading.hide();
				$ionicHistory.goBack();
			});
		}
	});

	Shell.getNamespaces().$promise
	.then(function (namespaces) {
		console.log('get namespaces');
		$scope.namespaces = namespaces;
		$scope.servicesNsIndex = namespaces.indexOf(ShellConstants.connmandNs.services);
	});
})
