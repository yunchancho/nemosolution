angular.module('nemocm.controllers')

.controller('ScreenCtrl', function($scope) {
})

.controller('ScreenBasicCtrl', function($rootScope, $scope, $stateParams, $q, $ionicSideMenuDelegate, $ionicScrollDelegate, $ionicModal, $ionicPopover, Shell, ShellUtil, ShellConstants, GUID, $http, $ionicLoading, PathConstants, File) {
	var namespaces;
	var tmpPlugedScreens = [];
	var tmpPlugedInputs = [];
  var formdata = new FormData();

	angular.extend($scope, {
		uiTypeName: ShellConstants.screenUiType,
		uiType: ShellConstants.screenUiType.layout.id,
		fsTypes: ShellConstants.fsTypes,
		fsSrcPositions: ShellConstants.fsSrcPositions,
		fsDestPositions: [],
		fsDestRotations: ShellConstants.fsDestRotations,
		nsIndex: -1,
		inputNsIndex: -1,
		touchInputNsIndex: -1,
		bgNsIndex: -1,
		plugedScreens: [],
		plugedInputs: [],
		inputMapping: [],
		bgMapping: [],
		deletedBgs: [],
		deletedFss: [],
		items: [],
		inputs: [],
		bgs: [],
		fss: [],
		container: {
			width: 0,
			height: 0
		},
		selectedItem: {},
		selectedScreen: { val: {} },
		selectedMode: '',
		screenModes: [],
		selectedFs: null,
		modal: {},
		curLayout: null
	});

	angular.extend($scope, {
		apply: function () {
			var screenPromises = [];
			var inputPromises = [];
			var bgPromises = [];
			var fsPromises = [];
			var deletedScreenPromises = [];
			var deletedInputPromises = [];
			var deletedBgPromises = [];
			var deletedFsPromises = [];

			console.log('curItems: ', $scope.curItems);
			console.log('curInputs: ', $scope.curInputs);

			deletedScreenPromises = _.map($scope.curItems, function (screen) {
				if (!screen.id) {
					return;
				}
				return Shell.delete({ nsIdx: $scope.nsIndex }, { id: screen.id }).$promise;
			});

			deletedInputPromises = _.map($scope.curInputs, function (input) {
				if (!input.id) {
					return;
				}
				return Shell.delete({ nsIdx: $scope.inputNsIndex }, { id: input.id }).$promise;
			});

			deletedBgPromises = _.map($scope.deletedBgs, function (bg) {
				if (!bg.id) {
					return;
				}
				return Shell.delete({ nsIdx: $scope.bgNsIndex }, { id: bg.id }).$promise;
			});
			deletedFsPromises = _.map($scope.deletedFss, function (fs) {
				if (!fs.id) {
					return;
				}
				return Shell.delete({ nsIdx: $scope.fsNsIndex }, { id: fs.id }).$promise;
			});

			screenPromises = _.map($scope.items, function (item) {
				// TODO how to replace item without id to one with id?
				if (item.id === undefined) {
					item["id"] = GUID.create();
				}	
				item.r = -1 * item.r;
				if (!item.map.scope) {
					item.scope = "off";
				} else {
					item.scope = "on";
				}
				console.log('saved item: ', item);
				return Shell.set({ nsIdx: $scope.nsIndex }, _.omit(item, 'map')).$promise;
			});
			inputPromises = _.map($scope.inputs, function (input) {
				if (input.id === undefined) {
					input["id"] = GUID.create();
				}	

				if ($scope.inputs.length == 1 && (!input.devnode)) {
					console.log('non-scope input applied');
					// for fullscreen except non-scope area
					// this works only if input touch device is one
					return Shell.set({ nsIdx: $scope.inputNsIndex }, _.omit(input, 'map', 'devnode')).$promise;
				} else {
					return Shell.set({ nsIdx: $scope.inputNsIndex }, _.omit(input, 'map')).$promise;
				}
			});
			bgPromises = _.map($scope.bgs, function (bg) {
				if (bg.id === undefined) {
					bg["id"] = GUID.create();
				}	
				if (bg.map.file) {
					console.log('ready to uplolad bg file: ', bg.id);
					var formdata = new FormData();
					var fileName = bg.id + '.' + bg.map.file.name.split('.').pop();
					formdata.append(bg.id, bg.map.file, fileName);
					delete bg.map.file;

					return File.send({ 
							limit: 'single',
							type: 'background',
							filename: fileName
						}, formdata).$promise
						.then(function (result) {
							console.log('uploaded: ' + result.path);
							bg.image = result.path;
							bg.map.bgImageRemoteUrl	= $rootScope.contentServerAddr + PathConstants.basePath;
							bg.map.bgImageRemoteUrl += result.path;

							return Shell.set({ nsIdx: $scope.bgNsIndex }, _.omit(bg, 'map')).$promise;
						});
				} 
				return Shell.set({ nsIdx: $scope.bgNsIndex }, _.omit(bg, 'map')).$promise;
			});
			fsPromises = _.map($scope.fss, function (fs) {
				if (fs.id === undefined) {
					fs["id"] = GUID.create();
				}	
				return Shell.set({ nsIdx: $scope.fsNsIndex }, _.omit(fs, 'map')).$promise;
			});

			$q.all([screenPromises, inputPromises, bgPromises, fsPromises]).then(function () {
				console.log('success to apply setting');
			})
			.catch(function (err) {
				console.log('fail to apply setting');
			})
			.finally(function () {
				_.each($scope.items, function (item) {
					item.r = -1 * item.r;
				});
			});

			$scope.curItems = [];
		},
		addBg: function () {
			var newItem = {
				id: GUID.create(),
				x: 0,
				y: 0,
				width: 1920,
				height: 1080,
				// TODO we need to change args, path field to more natural
				image: ShellConstants.bgDefaultInfo.imageFile,
				path: ShellConstants.bgDefaultInfo.imageApp,
				map: {}
			};

			$scope.bgs.push(newItem);
		},
		deleteBg: function (bg) {
			$scope.bgs.splice($scope.bgs.indexOf(bg), 1);
			if (bg.id) {
				$scope.deletedBgs.push(bg);
			}
		},
		addFs: function (type) {
			var newFs = {
				id: GUID.create(),
				sx: 0,
				sy: 0,
				sw: 960,
				sh: 540,
				dx: 0,
				dy: 0,
				dw: 960,
				dh: 540,
				dr: 0,
				map: {}
			};
			newFs.type = type;
			$scope.fss.push(newFs);
		},
		deleteFs: function (fs) {
			$scope.fss.splice($scope.fss.indexOf(fs), 1);
			if (fs.id) {
				$scope.deletedFss.push(fs);
			}
			if (fs.id === $scope.selectedFs.id) {
				$scope.selectedFs = null;
			}
		},
		showFs: function (fs) {
			$scope.selectedFs = fs;
		},
		setUiType: function (type) {
			$scope.uiType = type;
		},
		showLayout: function (layout) {
			console.log('changed layout: ', layout);
			$scope.curLayout = layout;

			if (!$scope.curItems.length) {
				$scope.curItems = $scope.items;
			}
			/*
			$scope.curInputs = $scope.inputs;
			$scope.curBgs = $scope.bgs;
			$scope.curFss= $scope.fss;
			*/

			$scope.items = [];
			/*
			$scope.inputs = [];
			$scope.bgs = [];
			$scope.fss = [];
			*/
			$scope.curContainer = $scope.container;
			$scope.container = { width: 0, height: 0 };

			var numPerLine = 0;
			var maxHeight = 0;
			var maxWidth = 0;
			var curXpos = 0;
			var curYpos = 0;
			if (layout.line === "n") {
				numPerLine = 1;
			} else if (layout.line === "root") {
				numPerLine = Math.sqrt($scope.plugedScreens.length);
			} else {
				numPerLine = $scope.plugedScreens.length / layout.line;
			}

			var sumWidth = 0;
			var sumHeight = 0;

			$scope.maxSumWidth = 0;
			$scope.maxSumHeight = 0;

			_.each($scope.plugedScreens, function (screen, idx) {
				var modes = _.where($scope.screenModes, {
				 	nodeid: screen.nodeid,
				 	screenid: screen.screenid
			 	});
				console.log('modes: ', modes);
				var res = _.max(modes, function (mode) {
					return parseInt(mode.width);
				});
				console.log('max res: ', res);

				if (idx % numPerLine == 0) {
					curXpos = 0;
					curYpos += maxHeight;

					if (layout.angle == 0) {
						if (sumWidth > $scope.maxSumWidth) {
							$scope.maxSumWidth = sumWidth;
						}
						$scope.maxSumHeight += maxHeight;

						sumWidth = 0;
						maxHeight = 0;
						sumHeight = 0;
						maxWidth = 0;
					} else if (layout.angle == 90) {
						if (sumHeight > $scope.maxSumHeight) {
							$scope.maxSumHeight = sumHeight;
						}
						$scope.maxSumWidth += maxWidth;

						sumWidth = 0;
						maxHeight = 0;
						sumHeight = 0;
						maxWidth = 0;
					} else {
					}

				}

				var newItem = {
					id: GUID.create(),
					nodeid: screen.nodeid,
					screenid: screen.screenid,
					width: parseInt(res.width),
					height: parseInt(res.height),
					x: curXpos,
					y: curYpos,
					sx: 1,
					sy: 1,
					r: layout.angle,
					layout: layout.name,
					map: {}
				};

				if (layout.angle == 0) {
					curXpos += newItem.width;
					sumWidth += newItem.width;
					if (newItem.height > maxHeight) {
						maxHeight = newItem.height;
					}
					$scope.container.width += Math.ceil(newItem.width / 10);
					$scope.container.height += Math.ceil(newItem.height / 10);
				} else if (layout.angle == 90) {
					newItem.x += newItem.height;
					curXpos += newItem.height;
					sumHeight += newItem.height;
					if (newItem.width > maxWidth) {
						maxWidth = newItem.width;
					}
					$scope.container.width += Math.ceil(newItem.height / 10);
					$scope.container.height += Math.ceil(newItem.width / 10);
				} else if (layout.angle == 180) {

				} else if (layout.angle == 270) {

				} 

				if (layout.px == "left") {
					newItem.px = 0;
				} else if (layout.px == "right") {
					newItem.px = newItem.width;
				} else if (layout.px == "center") {
					newItem.px = Math.ceil(newItem.width / 2);
				} else {
					newItem.px = Math.ceil(newItem.width / 2);
				}

				if (layout.py == "top") {
					newItem.py = 0;
				} else if (layout.py == "bottom") {
					newItem.py = newItem.height;
				} else if (layout.py == "center") {
					newItem.py = Math.ceil(newItem.height / 2);
				} else {
					newItem.py = Math.ceil(newItem.height / 2);
				}

				var found = _.findWhere($scope.curItems, {
					nodeid: screen.nodeid, screenid: screen.screenid 
				});

				if (found.scope && (found.scope == "off")) {
					newItem.map.scope = false;
				} else {
					newItem.map.scope = true;
				}
					
				$scope.items.push(newItem);
			});

			if (layout.angle == 0) {
				if (sumWidth > $scope.maxSumWidth) {
					$scope.maxSumWidth = sumWidth;
				}
				$scope.maxSumHeight += maxHeight;
			} else if (layout.angle == 90) {
				if (sumHeight > $scope.maxSumHeight) {
					$scope.maxSumHeight = sumHeight;
				}
				$scope.maxSumWidth += maxWidth;
			} else {
			}


			// set background for start menu
			var startBgs = _.filter($scope.bgs, function (bg) {
				if (bg.image) {
					return false;
				}
				return true;
			});
			if (!startBgs.length) {
				var newItem = {
					id: GUID.create(),
					x: 0,
					y: 0,
					width: $scope.container.width * 10,
					height: $scope.container.height * 10,
					path: ShellConstants.bgDefaultInfo.menuApp
				};
				$scope.bgs.push(newItem);
			} else {
				startBgs[0].width = $scope.container.width * 10;
				startBgs[0].height = $scope.container.height * 10;
				console.log('start BG: ', startBgs[0]);
			}

			changeAllByScreenLayout();
		},
		showInput: function (input) {
			console.log('Selected input: ', input);

			var ids = input.map.area.split('x');
			var screen = _.findWhere($scope.items, { nodeid: ids[0], screenid: ids[1] });
			if (!screen) {
				// TODO we need to check othe conditions more
				// but currently now in this case, this dest type is full screens
				input.x = 0;
				input.y = 0;
				input.width = $scope.maxSumWidth;
				input.height = $scope.maxSumHeight;
			} else {
				input.x = screen.x;
				input.y = screen.y;
				input.width = screen.width;
				input.height = screen.height;
			}
		},
		showBackground: function (bg) {
			console.log('Selected background: ', bg);

			var ids = bg.map.area.split('x');
			var screen = _.findWhere($scope.items, { nodeid: ids[0], screenid: ids[1] });
			if (!screen) {
				// TODO we need to check othe conditions more
				// but currently now in this case, this dest type is full screens
				bg.x = 0;
				bg.y = 0;
				bg.width = $scope.maxSumWidth;
				bg.height = $scope.maxSumHeight;
			} else {
				bg.x = screen.x;
				bg.y = screen.y;
				bg.width = screen.width;
				bg.height = screen.height;
			}
		},
		getBgAreaName: function (bg) {
			if (!bg.map.area) {
				return "";
			}
			var ids = bg.map.area.split('x');
			console.log('bgAreaPositions: ', $scope.bgAreaPositions);

			var name;
			_.each($scope.bgAreaPositions, function (p) {
				if (p.val.nodeid == ids[0] && p.val.screenid == ids[1]) {
					name = p.name;
				}
			});
			return name;
		},
		toggleBgGroup: function (group) {
			if ($scope.isBgGroupShown(group)) {
				$scope.shownBgGroup = null;
			} else {
				$scope.shownBgGroup = group;
			}
			//$scope.selectedBg = group;
		},
		isBgGroupShown: function (group) {
			return $scope.shownBgGroup == group;
		},
		bgsFilter: function (bg) {
			if (!bg.image) {
				return false;
			}

			return true;
		},
		fssFilter: function (fs) {
			if (fs.type == 'normal') {
				return false;
			}

			return true;
		},
		toggleFsGroup: function (group) {
			if ($scope.isFsGroupShown(group)) {
				$scope.shownFsGroup = null;
			} else {
				$scope.shownFsGroup = group;
			}
			$scope.selectedFs = group;
		},
		isFsGroupShown: function (group) {
			return $scope.shownFsGroup == group;
		},
		showFsType: function (fs) {
			fs.type = fs.map.type;
			$scope.showFsSrcPosition(fs);
		},
		showFsSrcPosition: function (fs) {
			var space;
			fs.spos = fs.map.src;
			if (fs.type == 'pick') {
				space = Math.ceil($scope.maxSumHeight / 2);
			} else if (fs.type == 'pitch') {
				space = 100;
			}
			if (fs.spos == 'top') {
				fs.sx = 0;
				fs.sy = 0;
				fs.sw = $scope.maxSumWidth;
				fs.sh = space;
			} else if (fs.spos == 'bottom') {
				fs.sx = 0;
				fs.sy = parseInt($scope.maxSumHeight) - space;
				fs.sw = $scope.maxSumWidth;
				fs.sh = space;
			} else if (fs.spos == 'left') {
				fs.sx = 0;
				fs.sy = 0;
				fs.sw = space;
				fs.sh = $scope.maxSumHeight;
			} else if (fs.spos == 'right') {
				fs.sx = $scope.maxSumWidth - space;
				fs.sy = 0;
				fs.sw = space;
				fs.sh = $scope.maxSumHeight;
			} else {
				console.log('unrecognizd src position');
			}
			console.log('changed fs src: ', fs);
		},
		showFsDestPosition: function (fs) {
			var ids = fs.map.dest.split('x');
			var screen = _.findWhere($scope.items, { nodeid: ids[0], screenid: ids[1] });
			if (!screen) {
				// TODO we need to check othe conditions more
				// but currently now in this case, this dest type is full screens
				fs.dx = 0;
				fs.dy = 0;
				fs.dw = $scope.maxSumWidth;
				fs.dh = $scope.maxSumHeight;
			} else {
				fs.dx = screen.x;
				fs.dy = screen.y;
				fs.dw = screen.width;
				fs.dh = screen.height;
			}
		},
		showFsDestRotation: function (fs) {
			fs.dr = fs.map.r;
		},
		changeScreenScope: function () {
			/*
			var nonscopes = _.filter($scope.items, function (item) {
				if (!item.map.scope) {
					return true;
				}
				return false;
			});

			if (nonscopes.length && ($scope.inputs.length == 1)) {
				$scope.inputDevNode = $scope.inputs[0].devnode;
				delete $scope.inputs[0].devnode;
			}

			if (!nonscopes.length && ($scope.inputs.length == 1)) {
				$scope.inputs[0].devnode = $scope.inputDevNode;
			}

			console.log("scope changed, input: ", $scope.inputs);
			*/
		},
		loadBgImageElement: function (element, bg) {
			console.log('selected bg: ', bg);
			console.log('files: ', element.files);
      $scope.$apply(function ($scope) {
				/*
        angular.forEach(element.files, function (value, key) {
					bg.map.file = value;
					bg.map.fileName = bg.id + '.' + value.name.split('.').pop();
				});
				*/

        angular.forEach(element.files, function (value, key) {
          console.log("value: " + JSON.stringify(value) + ", key: " + key);
					var bgName = bg.id + '.' + value.name.split('.').pop();
					if (formdata.has(bg.id)) {
						console.log('update bg: ', bgName);
          	formdata.set(bg.id, value, bgName);
					} else {
						console.log('add bg: ', bgName);
          	formdata.append(bg.id, value, bgName);
					}
        });
      });
		}
	});


	function getCurrentInfo(element)
	{
		var textEl = element.querySelector('p');
		var x = (parseFloat(element.getAttribute('data-x')) || 0);
		var y = (parseFloat(element.getAttribute('data-y')) || 0);
		var angle = getCurrentRotateDegree(element);

		return {
			x: Math.ceil(x),
			y: Math.ceil(y),
			r: Math.ceil(angle)
		}
	}

	function getCurrentRotateDegree(element) 
	{
		var style = window.getComputedStyle(element, null);
		var trans = style.getPropertyValue('transform');

		if (trans == 'none') {
			return 0;
		}

		console.log('matrix: ', trans);

		var values = trans.split('(')[1].split(')')[0].split(',');
		var a = values[0];
		var b = values[1];
		var c = values[2];
		var d = values[3];
		var scale = Math.sqrt(a * a + b * b);
		var radians = Math.atan2(b, a);
		if (radians < 0) {
			radians += (2 * Math.PI);
		}
		var angle = Math.round(radians * (180 / Math.PI));

		return angle;
	}

	$ionicSideMenuDelegate.canDragContent(false);

	Shell.getNamespaces().$promise
	.then(function (curNamespaces) {
    $ionicLoading.show();
		console.log('get namespaces');
		namespaces = curNamespaces;
		$scope.nsIndex = namespaces.indexOf(ShellConstants.screenNs);
		$scope.modeNsIndex = namespaces.indexOf(ShellConstants.screenModeNs);
		if ($scope.nsIndex < 0) {
      return $q.reject('invalid namespace');
		}
		return Shell.getDev({ nsIdx: $scope.nsIndex }).$promise;
	})
	.then(function (plugedScreens) {
		_.each(plugedScreens, function (screen) {
			var kvs = ShellUtil.parseItemString(screen);
			var obj = ShellUtil.makeSetFormdata(kvs);
			tmpPlugedScreens.push(obj);
		});

		var modePromises = [];
		_.each(tmpPlugedScreens, function (screen) {
			console.log('pluged screen: ', screen);
			var promise = Shell.getDev({
			 	nsIdx: $scope.modeNsIndex,
				nodeid: screen.nodeid,
				screenid: screen.screenid
			}).$promise;

			modePromises.push(promise);
		});
		console.log('pluged screen list: ', tmpPlugedScreens);
		return $q.all(modePromises);
	})
	.then(function (screenModes) {
		console.log('returned original screenModes: ', screenModes);
		var modes = [];	
		_.each(screenModes, function (mode) {
			modes = modes.concat(mode);
		});

		modes = _.uniq(modes);
		_.each(modes, function (mode) {
			var kvs = ShellUtil.parseItemString(mode);	
			var obj = ShellUtil.makeSetFormdata(kvs);
			$scope.screenModes.push(obj);
		});
		console.log('all screen modes', $scope.screenModes);
		return Shell.get({ nsIdx: $scope.nsIndex }).$promise;

	})
	.then(function (allItems) {
		console.log('all screens of config: ', allItems);
		_.each(allItems, function (item) {
			var kvs = ShellUtil.parseItemString(item);	
			var obj = ShellUtil.makeSetFormdata(kvs);
			$scope.items.push(obj);
		});

		var founds = [];
		$scope.items = _.filter($scope.items, function (item) {
			var found = _.findWhere(tmpPlugedScreens, {
				nodeid: item.nodeid,
				screenid: item.screenid
			});
			if (!found) {
				return false;
			}

			founds.push(found);
			return true;
		});
		_.each(founds, function (found) {
			tmpPlugedScreens.splice(tmpPlugedScreens.indexOf(found), 1);
		});

		_.each(tmpPlugedScreens, function (screen) {
			var modes = _.where($scope.screenModes, {
				nodeid: screen.nodeid,
				screenid: screen.screenid
			});
			console.log('modes: ', modes);
			var res = _.max(modes, function (mode) {
				return parseInt(mode.width);
			});

			screen.x = 0;
			screen.y = 0;
			screen.r = 0;
			screen.sx = 1;
			screen.sy = 1;
			screen.px = Math.ceil(res.width / 2);
			screen.py = Math.ceil(res.height / 2);
			screen.width = res.width;
			screen.height = res.height;
			screen.map = {
				scope: true
			};
		});
		$scope.items = $scope.items.concat(tmpPlugedScreens);
		$scope.plugedScreens = $scope.items;

		_.each($scope.items, function (item, i) {
			item.r = -1 * item.r;
			$scope.container.width += Math.ceil(item.width / 10);
			$scope.container.height += Math.ceil(item.height / 10);
		});
		$scope.curItems = $scope.items;

		$scope.touchInputNsIndex = namespaces.indexOf(ShellConstants.touchInputNs);
		$scope.inputNsIndex = namespaces.indexOf(ShellConstants.inputNs);
		return Shell.getDev({ nsIdx: $scope.touchInputNsIndex }).$promise;
	})
	.then(function (plugedInputs) {
		_.each(plugedInputs, function (input) {
			var kvs = ShellUtil.parseItemString(input);
			var obj = ShellUtil.makeSetFormdata(kvs);
			tmpPlugedInputs.push(obj);
		});
		console.log('pluged input list: ', tmpPlugedInputs);
		return Shell.get({ nsIdx: $scope.inputNsIndex }).$promise;
	})
	.then(function (allItems) {

		console.log('all inputs of config : ', allItems);
		$scope.inputs = [];
		_.each(allItems, function (item) {
			var kvs = ShellUtil.parseItemString(item);	
			var obj = ShellUtil.makeSetFormdata(kvs);
			$scope.inputs.push(obj);
		});

		var founds = [];
		$scope.inputs = _.filter($scope.inputs, function (input) {
			var found = _.findWhere(tmpPlugedInputs, {
				devnode: input.devnode
			});
			if (!found) {
				return false;
			}

			founds.push(found);
			return true;
		});
		_.each(founds, function (found) {
			tmpPlugedInputs.splice(tmpPlugedInputs.indexOf(found), 1);
		});

		_.each(tmpPlugedInputs, function (input) {
			input.width = 1920;
			input.height = 1080;
			input.sx = 1;
			input.sy = 1;
			input.r = 0;
			input.px = 0;
			input.py = 0;
		});
		$scope.inputs = $scope.inputs.concat(tmpPlugedInputs);
		setInputAreaPositions($scope.items, $scope.inputs);

		$scope.curInputs = $scope.inputs;
		$scope.plugedInputs = tmpPlugedInputs;

		$scope.bgNsIndex = namespaces.indexOf(ShellConstants.bgNs);
		return Shell.get({ nsIdx: $scope.bgNsIndex }).$promise;
	})
	.then(function (bgs) {
		console.log('backgrounds: ', bgs);
		_.each(bgs, function (bg) {
			var kvs = ShellUtil.parseItemString(bg);	
			var obj = ShellUtil.makeSetFormdata(kvs);
			var image = $rootScope.contentServerAddr + PathConstants.basePath;
			image += obj.image;
			obj.map = {
				bgImageRemoteUrl: image
			};
			$scope.bgs.push(obj);
		});
		setBgAreaPositions($scope.items, $scope.bgs);

		$scope.fsNsIndex = namespaces.indexOf(ShellConstants.fsNs);
		return Shell.get({ nsIdx: $scope.fsNsIndex }).$promise;
	})
	.then(function (fss) {
		console.log('fullscreens: ', fss);
		_.each(fss, function (fs) {
			var kvs = ShellUtil.parseItemString(fs);	
			var obj = ShellUtil.makeSetFormdata(kvs);
			$scope.fss.push(obj);
		});

		setFsDestPositions($scope.items, $scope.fss);
		var deferred = $q.defer();
		$http.get('data/layout.json').success(function (data) {
			$scope.layouts = data;
			console.log('Recommended layouts: ', $scope.layouts);
			deferred.resolve();
		});
		return deferred.promise;
	}) 
	.then(function () {
		if (_.has($scope.items[0], 'layout')) {
			$scope.curLayout = _.findWhere($scope.layouts, { name: $scope.items[0].layout });
			if ($scope.curLayout)  {
				$scope.showLayout($scope.curLayout);
			}
		}
	})
	.catch(function (err) {
		console.log(err);
	})
	.finally(function () {
    $ionicLoading.hide();
	});

	function setInputAreaPositions (screens, inputs)
	{
		$scope.inputAreaPositions = [];
		_.each(screens, function (screen, index) {
			$scope.inputAreaPositions.push({
				val: { nodeid: screen.nodeid, screenid: screen.screenid },
				name: 'screen' + index
			});
		});
		_.each(ShellConstants.inputAreaStaticPositions, function (area) {
			$scope.inputAreaPositions.push(area);
		});
		console.log('inputAreaPositions: ', $scope.inputAreaPositions); 

		_.each(inputs, function (input) {
			var screen = _.findWhere(screens, {
				x: input.x,
				y: input.y,
				width: input.width,
				height: input.height
			});

			var area;
			if (!screen) {
				// TODO we need to check othe conditions more
				// but currently now in this case, this dest type is full screens
				area = { nodeid: -1, screenid: -1 };
			} else {
				area = { nodeid: screen.nodeid, screenid: screen.screenid };
			}
			
			input.map = {
				area: area.nodeid + 'x' + area.screenid,
			};
			input.map.areaName = _.filter($scope.inputAreaPositions, function (pos) {
				return (pos.val.nodeid == area.nodeid) && (pos.val.screenid == area.screenid);
			})[0].name;
		});

		console.log('------------ inputs -------------');
		console.log(inputs);

	}

	function setBgAreaPositions (screens, bgs)
	{
		$scope.bgAreaPositions = [];
		_.each(screens, function (screen, index) {
			$scope.bgAreaPositions.push({
				val: { nodeid: screen.nodeid, screenid: screen.screenid },
				name: 'screen' + index
			});
		});
		_.each(ShellConstants.bgAreaStaticPositions, function (area) {
			$scope.bgAreaPositions.push(area);
		});

		_.each(bgs, function (bg) {
			var screen = _.findWhere(screens, {
				x: bg.x,
				y: bg.y,
				width: bg.width,
				height: bg.height
			});

			var area;
			if (!screen) {
				// TODO we need to check othe conditions more
				// but currently now in this case, this dest type is full screens
				area = { nodeid: -1, screenid: -1 };
			} else {
				area = { nodeid: screen.nodeid, screenid: screen.screenid };
			}
			
			bg.map.area = area.nodeid + 'x' + area.screenid;
			bg.map.areaName = _.filter($scope.bgAreaPositions, function (pos) {
				return (pos.val.nodeid == area.nodeid) && (pos.val.screenid == area.screenid);
			})[0].name;

		});

		console.log('bgAreaPositions: ', $scope.bgAreaPositions); 
	}

	function setFsDestPositions (screens, fss)
	{
		$scope.fsDestPositions = [];
		_.each(screens, function (screen, index) {
			$scope.fsDestPositions.push({
				val: { nodeid: screen.nodeid, screenid: screen.screenid },
				name: 'screen' + index
			});
		});
		_.each(ShellConstants.fsDestStaticPositions, function (dest) {
			$scope.fsDestPositions.push(dest);
		});

		_.each(fss, function (fs) {
			var screen = _.findWhere(screens, {
				x: fs.dx,
				y: fs.dy,
				width: fs.dw,
				height: fs.dh
			});

			var dest;
			if (!screen) {
				// TODO we need to check othe conditions more
				// but currently now in this case, this dest type is full screens
				console.log('--- screen unmatched: ' + fs.type + ' -------');
				dest = { nodeid: -1, screenid: -1 };
			} else {
				dest = { nodeid: screen.nodeid, screenid: screen.screenid };
				console.log('--- screen matched: -------');
				console.log('dest: ', dest);
			}
			
			fs.map = {
				type: fs.type,
				src: fs.spos,
				dest: dest.nodeid + 'x' + dest.screenid,
				r: parseInt(fs.dr)
			};

			console.log('fs.map: ', fs.map);
			if (fs.type !== 'normal') {
				var founds = _.filter($scope.fsTypes, function (type) {
					return type.val == fs.map.type;
				});
				if (founds.length) {
					fs.map.typeName  = founds[0].name;
				}

				founds = _.filter($scope.fsSrcPositions, function (pos) {
					return pos.val == fs.map.src;
				});
				if (founds.length) {
					fs.map.srcName = founds[0].name;
				}

				founds = _.filter($scope.fsDestPositions, function (pos) {
					return (pos.val.nodeid == dest.nodeid) && (pos.val.screenid == dest.screenid);
				});
				if (founds.length) {
					fs.map.destName = founds[0].name;
				}

				founds = _.filter($scope.fsDestRotations, function (r) {
					return r.val == fs.map.r;
				});
				if (founds.length) {
					fs.map.rName = founds[0].name;
				}
			}
			console.log('fs.map: ', fs.map);
		});

		console.log('fsDestPositions: ', $scope.fsDestPositions); 
	}

	function changeAllByScreenLayout ()
	{
		console.log('curlayout: ', $scope.curLayout);
		_.each($scope.inputs, function (input) {
			var condition;
			if (input.map) {
			  var ids = input.map.area.split('x');
			  condition = {
				 	nodeid: ids[0],
				 	screenid: ids[1]
			 	};
			} else {
				condition = {
					x: input.x,
					y: input.y,
					width: input.width,
					height: input.height
			 	};
			}

			var screen = _.findWhere($scope.items, condition);
			console.log("FOUND SCREEN: ", screen);

			if (input.map) {
				if (!screen) {
					console.log('--- this doesn\'t exist screen. rotation: ', $scope.curLayout.angle);
					input.x = 0;
					input.y = 0;
					input.width = $scope.maxSumWidth;
					input.height = $scope.maxSumHeight;
					input.r = $scope.curLayout.angle;

					if ($scope.curLayout.px == "left") {
						input.px = 0;
					} else if ($scope.curLayout.px == "right") {
						input.px = input.width;
					} else if ($scope.curLayout.px == "center") {
						input.px = Math.ceil(input.width / 2);
					} else {
						input.px = Math.ceil(input.width / 2);
					}

					if ($scope.curLayout.py == "top") {
						input.py = 0;
					} else if ($scope.curLayout.py == "bottom") {
						input.py = input.height;
					} else if ($scope.curLayout.py == "center") {
						input.py = Math.ceil(input.height / 2);
					} else {
						input.py = Math.ceil(input.height / 2);
					}

					if ($scope.curLayout.angle == 0) {
					} else if ($scope.curLayout.angle == 90) {
						input.x += input.height;
					} else if ($scope.curLayout.angle == 180) {
					} else if ($scope.curLayout.angle == 270) {
					}
				} else {
					input.x = screen.x;
					input.y = screen.y;
					input.r = screen.r;
					input.px = screen.px;
					input.py = screen.py;
					input.width = screen.width;
					input.height = screen.height;
				}
			} else {
				var area;
				if (!screen) {
					// TODO we need to check othe conditions more
					// but currently now in this case, this dest type is full screens
					area = { nodeid: -1, screenid: -1 };
				} else {
					area = { nodeid: screen.nodeid, screenid: screen.screenid };
				}
				input.map = {
					area: area.nodeid + 'x' + area.screenid,
				}
			}
		});


		_.each($scope.bgs, function (bg) {

			if (!bg.image) {
				return;
			}

			var condition;
			if (bg.map) {
			  var ids = bg.map.area.split('x');
			  condition = {
				 	nodeid: ids[0],
				 	screenid: ids[1]
			 	};
			} else {
				condition = {
					x: bg.x,
					y: bg.y,
					width: bg.width,
					height: bg.height
			 	};
			}

			var screen = _.findWhere($scope.items, condition);

			if (bg.map) {
				if (!screen) {
					bg.x = 0;
					bg.y = 0;

					if ($scope.curLayout.angle == 0) {
						bg.width = $scope.maxSumWidth;
						bg.height = $scope.maxSumHeight;
					} else if ($scope.curLayout.angle == 90) {
						bg.width = $scope.maxSumHeight;
						bg.height = $scope.maxSumWidth;
					} else if ($scope.curLayout.angle == 180) {
					} else if ($scope.curLayout.angle == 270) {
					}
				} else {
					bg.x = screen.x;
					bg.y = screen.y;
					bg.width = screen.width;
					bg.height = screen.height;
				}
			} else {
				var area;
				if (!screen) {
					// TODO we need to check othe conditions more
					// but currently now in this case, this dest type is full screens
					area = { nodeid: -1, screenid: -1 };
				} else {
					area = { nodeid: screen.nodeid, screenid: screen.screenid };
				}
				bg.map = {
					area: area.nodeid + 'x' + area.screenid,
				}
			}
		});
	}

	angular.extend($scope, {
		openPopover: function ($event, args) {
			console.log('open popover');
			// info = {
			// 	target: object,
			// 	tattr: string,
			// 	items: array,
			// 	nattr: string,
			// 	vattr: string,
			// 	cb: function
			// }
			$scope.popoverArgs = args;
			$scope.popover.show($event);
		},
		closePopover: function () {
			$scope.popoverArgs = {};
			$scope.popover.hide();
		},
		selectPopover: function (item) {
			$scope.$broadcast('PopoverSelected', item);
			$scope.closePopover();
	  },	
		selectInputOption (target, item) {
			var args = $scope.popoverArgs;
			target.area = item.val.nodeid + 'x' + item.val.screenid;
			target.areaName = _.filter($scope.inputAreaPositions, function (pos) {
				return (pos.val.nodeid == item.val.nodeid) && (pos.val.screenid == item.val.screenid);
			})[0].name;
			$scope.showInput(args.object);
		},
		selectBgOption (target, item) {
			var args = $scope.popoverArgs;
			target.area = item.val.nodeid + 'x' + item.val.screenid;
			target.areaName = _.filter($scope.bgAreaPositions, function (pos) {
				return (pos.val.nodeid == item.val.nodeid) && (pos.val.screenid == item.val.screenid);
			})[0].name;
			$scope.showBackground(args.object);
		},
		selectFsTypeOption (target, item) {
			var args = $scope.popoverArgs;
			target.type = item.val;
			target.typeName = item.name;
			$scope.showFsType(args.object);
		},
		selectFsSrcPositionOption (target, item) {
			var args = $scope.popoverArgs;
			target.src = item.val;
			target.srcName = item.name;
			$scope.showFsSrcPosition(args.object);
		},
		selectFsDestPositionOption (target, item) {
			var args = $scope.popoverArgs;
			target.dest = item.val.nodeid + 'x' + item.val.screenid;
			target.destName = _.filter($scope.fsDestPositions, function (pos) {
				return (pos.val.nodeid == item.val.nodeid) && (pos.val.screenid == item.val.screenid);
			})[0].name;
			$scope.showFsDestPosition(args.object);
		},
		selectFsDestRotationOption (target, item) {
			var args = $scope.popoverArgs;
			target.r = item.val;
			target.rName = item.name;
			$scope.showFsDestRotation(args.object);
		},
	});
	
	$scope.$on('PopoverSelected', function (event, item) {
		console.log('selected called');

		var args = $scope.popoverArgs;

		/*
		if (args.vattr) {
			args.target[args.tattr] = item[args.vattr];
		}
		*/

		if (args.cb) {
			return args.cb(args.target, item);
		}
	});

  $ionicModal.fromTemplateUrl('templates/screenadvancedfs.html', {
    scope: $scope,
		animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal.fs = modal;
  });
	$ionicPopover.fromTemplateUrl('templates/popover/dropdown.html', {
		scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

	//$scope.$watch('items', setFsDestPositions);
	//$scope.$watch('items', setInputAreaPositions);

});
