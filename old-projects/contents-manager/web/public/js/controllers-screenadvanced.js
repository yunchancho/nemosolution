angular.module('nemocm.controllers')

.controller('ScreenAdvancedCtrl', function($scope, $stateParams, $q, $ionicSideMenuDelegate, $ionicScrollDelegate, $ionicModal, Shell, ShellUtil, ShellConstants, GUID, $ionicLoading) {

	var namespaces;
	var tmpPlugedScreens = [];
	var tmpPlugedInputs = [];

	angular.extend($scope, {
		uiTypeName: ShellConstants.screenUiType,
		uiType: ShellConstants.screenUiType.layout.id,
		nsIndex: -1,
		inputNsIndex: -1,
		touchInputNsIndex: -1,
		bgNsIndex: -1,
		plugedScreens: [],
		plugedInputs: [],
		deletedBgs: [],
		deletedFss: [],
		items: [],
		inputs: [],
		bgs: [],
		fss: [],
		container: {
			width: 0,
			height: 0
		},
		selectedItem: {},
		selectedMode: '',
		screenModes: [],
		selectedFs: null,
		modal: {}
	});

	angular.extend($scope, {
		closeScreenModal: function() {
			$scope.screenModes = [];
			$scope.selectedMode = '';
			$scope.modal.screen.hide();
		},
		openScreenModal: function(item) {
			$scope.selectedItem = item;
			Shell.getDev({
			 	nsIdx: $scope.modeNsIndex,
				nodeid: item.nodeid,
				screenid: item.screenid
			}).$promise
			.then(function (modes) {
				console.log('screen mode: ', modes);
				$scope.selectedMode = item.width + ' x ' + item.height;
				_.each(modes, function (mode) {
					var kvs = ShellUtil.parseItemString(mode);
					var obj = ShellUtil.makeSetFormdata(kvs);
					obj.res = obj.width + ' x ' + obj.height;
					$scope.screenModes.push(obj);
				});
				console.log('selected Mode before: ', $scope.selectedMode);
				$scope.modal.screen.show();
			})
			.catch(function (err) {
				console.log(err);
			});
		},
		updateResOption: function () {
			$scope.selectedMode = this.selectedMode;
		},
		saveScreenModal: function () {
			console.log('selected item: ', $scope.selectedItem);
			console.log('available mode: ', $scope.screenModes);
			console.log('selected mode: ', $scope.selectedMode);

			// TODO we need to handle $scope.selectedMode
			var res = $scope.selectedMode.split(' x ');
			$scope.container.width -= Math.ceil($scope.selectedItem.width / 10);
			$scope.container.height -= Math.ceil($scope.selectedItem.height / 10);

			$scope.selectedItem.width = res[0];
			$scope.selectedItem.height = res[1];

			$scope.container.width += Math.ceil($scope.selectedItem.width / 10);
			$scope.container.height += Math.ceil($scope.selectedItem.height / 10);

			$scope.closeScreenModal();
		},
		closeInputModal: function() {
			$scope.modal.input.hide();
		},
		openInputModal: function(item) {
			$scope.selectedItem = item;
			$scope.modal.input.show();
		},
		saveInputModal: function () {
			//TODO we need to move touch circle object on correct screen position
			//
			if ($scope.selectedItem.useTransform) {
				delete $scope.selectedItem.nodeid;
				delete $scope.selectedItem.screenid;
			} else {
				$scope.selectedItem = {
					devnode: $scope.selectedItem.devnode,
					nodeid: $scope.selectedItem.nodeid,
					screenid: $scope.selectedItem.screenid,
					useTransform: $scope.selectedItem.useTransform,
				};
			}	
			console.log('selected item: ', $scope.selectedItem);

			$scope.closeInputModal();
		},
		changeInputTransformFlag: function () {
			console.log('current transform flag: ', $scope.selectedItem.useTransform);
			if ($scope.selectedItem.useTransform) {
				if ($scope.selectedItem.x === undefined) $scope.selectedItem.x = 0;
				if ($scope.selectedItem.y === undefined) $scope.selectedItem.y = 0;
				if ($scope.selectedItem.width === undefined) $scope.selectedItem.width = 1920;
				if ($scope.selectedItem.height === undefined) $scope.selectedItem.height = 1080;
				if ($scope.selectedItem.r === undefined) $scope.selectedItem.r = 0;
				if ($scope.selectedItem.sx === undefined) $scope.selectedItem.sx = 1;
				if ($scope.selectedItem.sy === undefined) $scope.selectedItem.sy = 1;
				if ($scope.selectedItem.px === undefined) $scope.selectedItem.px = 960;
				if ($scope.selectedItem.py === undefined) $scope.selectedItem.py = 540;
			}
		},
		closeBgModal: function() {
			$scope.modal.bg.hide();
		},
		openBgModal: function(item) {
			$scope.selectedItem = item;
			$scope.modal.bg.show();
		},
		saveBgModal: function () {
			console.log('selected item: ', $scope.selectedItem);
			$scope.closeBgModal();
		},
		closeFsModal: function() {
			$scope.modal.fs.hide();
		},
		openFsModal: function(item) {
			$scope.selectedItem = item;
			$scope.modal.fs.show();
		},
		saveFsModal: function () {
			console.log('selected item: ', $scope.selectedItem);
			$scope.closeFsModal();
		},
		apply: function () {
			var screenPromises = [];
			var inputPromises = [];
			var bgPromises = [];
			var fsPromises = [];
			var deletedScreenPromises = [];
			var deletedInputPromises = [];
			var deletedBgPromises = [];
			var deletedFsPromises = [];

			deletedScreenPromises = _.map($scope.plugedScreens, function (screen) {
				if (!screen.id) {
					return;
				}
				return Shell.delete({ nsIdx: $scope.nsIndex }, { id: screen.id }).$promise;
			});

			deletedInputPromises = _.map($scope.plugedInputs, function (input) {
				if (!input.id) {
					return;
				}
				return Shell.delete({ nsIdx: $scope.inputNsIndex }, { id: input.id }).$promise;
			});

			deletedBgPromises = _.map($scope.deletedBgs, function (bg) {
				if (!bg.id) {
					return;
				}
				return Shell.delete({ nsIdx: $scope.bgNsIndex }, { id: bg.id }).$promise;
			});
			deletedFsPromises = _.map($scope.deletedFss, function (fs) {
				if (!fs.id) {
					return;
				}
				return Shell.delete({ nsIdx: $scope.fsNsIndex }, { id: fs.id }).$promise;
			});

			screenPromises = _.map($scope.items, function (item) {
				// TODO how to replace item without id to one with id?
				if (item.id === undefined) {
					item["id"] = GUID.create();
				}	
				return Shell.set({ nsIdx: $scope.nsIndex }, item).$promise;
			});
			inputPromises = _.map($scope.inputs, function (input) {
				if (input.id === undefined) {
					input["id"] = GUID.create();
				}	
				return Shell.set({ nsIdx: $scope.inputNsIndex }, input).$promise;
			});
			bgPromises = _.map($scope.bgs, function (bg) {
				if (bg.id === undefined) {
					bg["id"] = GUID.create();
				}	
				console.log('updated bg id: ', bg.id);
				return Shell.set({ nsIdx: $scope.bgNsIndex }, bg).$promise;
			});
			fsPromises = _.map($scope.fss, function (fs) {
				if (fs.id === undefined) {
					fs["id"] = GUID.create();
				}	
				return Shell.set({ nsIdx: $scope.fsNsIndex }, fs).$promise;
			});

			$q.all([screenPromises, inputPromises, bgPromises, fsPromises]).then(function () {
				console.log('success to apply setting');
			})
			.catch(function (err) {
				console.log('fail to apply setting');
			});
		},
		useScreen: function (screen) {
			var newItem = {};
			Shell.getDev({
			 	nsIdx: $scope.modeNsIndex,
				nodeid: screen.nodeid,
				screenid: screen.screenid
			}).$promise
			.then(function (modes) {
				var kvs = ShellUtil.parseItemString(modes[0]);
				var obj = ShellUtil.makeSetFormdata(kvs);
				newItem = {
					nodeid: screen.nodeid,
					screenid: screen.screenid,
					width: obj.width,
					height: obj.height,
					x: 0,
					y: 0,
					sx: 1,
					sy: 1,
					r: 0
				};
				$scope.items.push(newItem);
				$scope.plugedScreens.splice($scope.plugedScreens.indexOf(screen), 1);
				$scope.container.width += Math.ceil(newItem.width / 10);
				$scope.container.height += Math.ceil(newItem.height / 10);
			})
			.catch(function (err) {
				console.log(err);
			})
		},
		unUseScreen: function (item) {
			$scope.items.splice($scope.items.indexOf(item), 1);
			$scope.plugedScreens.push({
				nodeid: item.nodeid,
				screenid: item.screenid,
				id: item.id,
			});
			$scope.container.width -= Math.ceil(item.width / 10);
			$scope.container.height -= Math.ceil(item.height / 10);
		},
		useTouchInput: function (input) {
			$scope.inputs.push(input);
			$scope.plugedInputs.splice($scope.plugedInputs.indexOf(input), 1);
		},
		unUseTouchInput: function (input) {
			$scope.inputs.splice($scope.inputs.indexOf(input), 1);
			$scope.plugedInputs.push({
				devnode: input.devnode,
				id: input.id
			});
		},
		addBg: function () {
			var newItem = {
				id: GUID.create(),
				x: 0,
				y: 0,
				width: 1920,
				height: 1080,
				// TODO we need to change args, path field to more natural
				image: '/usr/share/steshell/backgrounds/background-1.png',
				path: '/usr/bin/nemoback-play'
			};

			$scope.bgs.push(newItem);
			$scope.openBgModal(newItem);
		},
		deleteBg: function (bg) {
			$scope.bgs.splice($scope.bgs.indexOf(bg), 1);
			if (bg.id) {
				$scope.deletedBgs.push(bg);
			}
		},
		addFs: function (type) {
			var newFs = {
				id: GUID.create(),
				sx: 0,
				sy: 0,
				sw: 960,
				sh: 540,
				dx: 0,
				dy: 0,
				dw: 960,
				dh: 540,
				dr: 0
			};
			newFs.type = type;
			$scope.fss.push(newFs);
		},
		deleteFs: function (fs) {
			$scope.fss.splice($scope.fss.indexOf(fs), 1);
			if (fs.id) {
				$scope.deletedFss.push(fs);
			}
			if (fs.id === $scope.selectedFs.id) {
				$scope.selectedFs = null;
			}
		},
		showFs: function (fs) {
			$scope.selectedFs = fs;
		},
		setUiType: function (type) {
			$scope.uiType = type;
			if ($scope.uiType == $scope.uiTypeName.layout.id) {
				interact('.draggable-screen').draggable(true);
				interact('.draggable-screen').resizable(true);
				interact('.draggable-screen').gesturable(true);
				interact('.draggable-screen').dropzone(false);
				interact('.transform-screen').draggable(true);
				interact('.transform-screen').resizable(true);
				interact('.transform-screen').gesturable(true);
			} else if ($scope.uiType == $scope.uiTypeName.input.id) {
				interact('.draggable-screen').draggable(false);
				interact('.draggable-screen').resizable(false);
				interact('.draggable-screen').gesturable(false);
				interact('.draggable-screen').dropzone(true);
				interact('.transform-screen').draggable(false);
				interact('.transform-screen').resizable(false);
				interact('.transform-screen').gesturable(false);
			} else if ($scope.uiType == $scope.uiTypeName.background.id) {
				interact('.draggable-screen').draggable(false);
				interact('.draggable-screen').resizable(false);
				interact('.draggable-screen').gesturable(false);
				interact('.draggable-screen').dropzone(false);
				interact('.transform-screen').draggable(false);
				interact('.transform-screen').resizable(false);
				interact('.transform-screen').gesturable(false);
			} else if ($scope.uiType == $scope.uiTypeName.fullscreen.id) {
				interact('.draggable-screen').draggable(false);
				interact('.draggable-screen').resizable(false);
				interact('.draggable-screen').gesturable(false);
				interact('.draggable-screen').dropzone(false);
				interact('.transform-screen').draggable(false);
				interact('.transform-screen').resizable(false);
				interact('.transform-screen').gesturable(false);
			} else {
				console.log('invalid ui type');
			}
		}
	});

	interact('.draggable-screen')
	.draggable({
		inertia: true,
		restrict: {
			//restriction: "parent",
			endOnly: true,
			elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
		},
		//autoScroll: true,
		onmove: dragMoveListener,
		onend: function (event) {
			console.log("onend event");
			/*
			var content = getCurrentInfo(event.target);
			event.target.textContent = '(' + content.x * 10 + ', ' + content.y * 10 + ')';
			event.target.textContent += ', r: ' + content.r;
			*/
		}
	})
	.on('dragstart', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(true);
	})
	.on('dragend', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(false);
	})
	.dropzone({
		accept: '.draggable-input',
		overlap: 0.0001,

		ondropactivate: function (event) {
			console.log('ondropactivate');
			event.target.classList.add('drop-active');
		},

		ondragenter: function (event) {
			var dragEl = event.relatedTarget;
			var dropzoneEl = event.target;

			dropzoneEl.classList.add('drop-target');
			dragEl.classList.add('can-drop');
			dragEl.style.backgroundColor = 'rgba(0,0,255,1.0)';
			var nodeid = dropzoneEl.getAttribute('nodeid');
			var screenid = dropzoneEl.getAttribute('screenid');
			var devnode = dragEl.getAttribute('devnode');
			var input = _.findWhere($scope.inputs, {
				devnode: devnode
			});

			$scope.$apply(function () {
				input.nodeid = nodeid;
				input.screenid = screenid;
			});
			console.log('set screen info: ', input);
		},
		ondragleave: function (event) {
			var dragEl = event.relatedTarget;
			var dropzoneEl = event.target;

			dropzoneEl.classList.remove('drop-target');
			dragEl.classList.remove('can-drop');
			dragEl.style.backgroundColor = 'rgba(0,0,255,0.2)';

			var devnode = dragEl.getAttribute('devnode');
			var input = _.findWhere($scope.inputs, {
				devnode: devnode
			});

			$scope.$apply(function () {
				delete input.nodeid;
				delete input.screenid;
			});
			console.log('unset screen info: ', input);
		},
		ondrop: function (event) {
			var dragEl = event.relatedTarget;
			var dropzoneEl = event.target;

			dragEl.style.backgroundColor = 'rgba(0,0,255,1.0)';
		},
		ondropdeactivate: function (event) {
			var dropzoneEl = event.target;
			console.log('ondropdeactivate');
			dropzoneEl.classList.remove('drop-active');
			dropzoneEl.classList.remove('drop-target');
		}
	})
	.on('tap', function (event) {
	//	event.target.style.zIndex++;
	//	event.preventDefault();
	});

	interact('.transform-screen')
	.draggable({
		inertia: true,
		restrict: {
			//restriction: "parent",
			endOnly: true,
			elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
		},
		//autoScroll: true,
		onmove: dragMoveListener,
		onend: function (event) {
			console.log("onend event");
			/*
			var content = getCurrentInfo(event.target);
			event.target.textContent = '(' + content.x * 10 + ', ' + content.y * 10 + ')';
			event.target.textContent += ', r: ' + content.r;
			*/
		}
	})
	.on('dragstart', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(true);
	})
	.on('dragend', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(false);
	})
	.resizable({
		preserveAspectRatio: true,
		edges: {
			left: true, right: true, bottom: true, top: true
		},
		//squareResize: true
	})
	.on('resizestart', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(true);
	})
	.on('resizeend', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(false);
	})
	.on('resizemove', function (event) {

		var target = event.target;
		console.log('event.rect: ', event.rect);
		console.log('event.deltaRect: ', event.deltaRect);

		var nodeid = target.getAttribute('nodeid');
		var screenid = target.getAttribute('screenid');
		var item = _.findWhere($scope.items, {
			nodeid: nodeid, screenid: screenid
		});

		console.log('event.deltaRect: ', event.deltaRect);
		$scope.$apply(function () {
			//item.x = (parseFloat(item.x) || 0) + Math.ceil(event.deltaRect.left * 10);
			//item.y = (parseFloat(item.y) || 0) + Math.ceil(event.deltaRect.top * 10);
			item.sx = (event.rect.width * 10 / item.width).toFixed(3);
			item.sy = (event.rect.height* 10 / item.height).toFixed(3);
			//item.width = Math.ceil(event.rect.width * 10);
			//item.height = Math.ceil(event.rect.height * 10);
		});
	})
	.gesturable({
		onmove: function (event) {
			var nodeid = event.target.getAttribute('nodeid');
			var screenid = event.target.getAttribute('screenid');

			var item = _.findWhere($scope.items, {
				nodeid: nodeid, screenid: screenid
			});

			$scope.$apply(function () {
				item.r = (parseFloat(item.r) || 0) + Math.ceil(event.da);
			});
		}
	})

	interact('.draggable-input')
	.draggable({
		inertia: true,
		restrict: {
			restriction: "ion-content",
			endOnly: true,
			elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
		},
		//autoScroll: true,
		onmove: intputDragMoveListener,
		onend: function (event) {
			console.log("onend event");
			/*
			var content = getCurrentInfo(event.target);
			event.target.textContent = '(' + content.x * 10 + ', ' + content.y * 10 + ')';
			event.target.textContent += ', r: ' + content.r;
			*/
		}
	})
	.on('dragstart', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(true);
	})
	.on('dragend', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(false);
	})

	interact('.transform-input')
	.draggable({
		inertia: true,
		restrict: {
			//restriction: "parent",
			endOnly: true,
			elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
		},
		//autoScroll: true,
		onmove: intputTransformDragMoveListener,
		onend: function (event) {
			console.log("onend event");
			/*
			var content = getCurrentInfo(event.target);
			event.target.textContent = '(' + content.x * 10 + ', ' + content.y * 10 + ')';
			event.target.textContent += ', r: ' + content.r;
			*/
		}
	})
	.on('dragstart', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(true);
	})
	.on('dragend', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(false);
	})
	.resizable({
		//preserveAspectRatio: true,
		edges: {
			left: true, right: true, bottom: true, top: true
		},
		//squareResize: true
	})
	.on('resizestart', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(true);
	})
	.on('resizeend', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(false);
	})
	.on('resizemove', function (event) {

		var devnode = event.target.getAttribute('devnode');
		var input = _.findWhere($scope.inputs, {
			devnode: devnode
		});

		$scope.$apply(function () {
			input.sx = (event.rect.width * 10 / input.width).toFixed(3);
			input.sy = (event.rect.height* 10 / input.height).toFixed(3);
		});
	})
	.gesturable({
		onmove: function (event) {
			var devnode = event.target.getAttribute('devnode');
			var input = _.findWhere($scope.inputs, {
				devnode: devnode
			});

			$scope.$apply(function () {
				input.r = (parseFloat(input.r) || 0) + Math.ceil(event.da);
			});
		}
	})

	interact('.draggable-bg')
	.draggable({
		inertia: true,
		restrict: {
			//restriction: "parent",
			endOnly: true,
			elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
		},
		//autoScroll: true,
		onmove: bgDragMoveListener,
		onend: function (event) {
			console.log("onend event");
		}
	})
	.on('dragstart', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(true);
	})
	.on('dragend', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(false);
	})
	.resizable({
		//preserveAspectRatio: true,
		edges: {
			left: true, right: true, bottom: true, top: true
		}
	})
	.on('resizestart', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(true);
	})
	.on('resizeend', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(false);
	})
	.on('resizemove', function (event) {

		console.log('event.rect: ', event.rect);
		console.log('event.deltaRect: ', event.deltaRect);

		var id = event.target.getAttribute('id');
		var bg = _.findWhere($scope.bgs, {
			id: id
		});

		$scope.$apply(function () {
			bg.x = parseFloat(bg.x) + Math.ceil(event.deltaRect.left * 10);
			bg.y = parseFloat(bg.y) + Math.ceil(event.deltaRect.top * 10);
			bg.width = Math.ceil(event.rect.width * 10);
			bg.height = Math.ceil(event.rect.height * 10);
		});
	})

	interact('.draggable-fs')
	.draggable({
		inertia: true,
		restrict: {
			//restriction: "parent",
			endOnly: true,
			elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
		},
		//autoScroll: true,
		onmove: fsDragMoveListener,
		onend: function (event) {
			console.log("onend event");
		}
	})
	.on('dragstart', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(true);
	})
	.on('dragend', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(false);
	})
	.resizable({
		//preserveAspectRatio: true,
		edges: {
			left: true, right: true, bottom: true, top: true
		}
	})
	.on('resizestart', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(true);
	})
	.on('resizeend', function (event) {
		$ionicScrollDelegate.freezeAllScrolls(false);
	})
	.on('resizemove', function (event) {

		console.log('event.rect: ', event.rect);
		console.log('event.deltaRect: ', event.deltaRect);

		var id = event.target.getAttribute('id');
		var type = event.target.getAttribute('type');
		var fs = _.findWhere($scope.fss, {
			id: id
		});

		$scope.$apply(function () {
			if (type == 'src') {
				fs.sx = parseFloat(fs.sx) + Math.ceil(event.deltaRect.left * 10);
				fs.sy = parseFloat(fs.sy) + Math.ceil(event.deltaRect.top * 10);
				fs.sw = Math.ceil(event.rect.width * 10);
				fs.sh = Math.ceil(event.rect.height * 10);
			} else {
				fs.dx = parseFloat(fs.dx) + Math.ceil(event.deltaRect.left * 10);
				fs.dy = parseFloat(fs.dy) + Math.ceil(event.deltaRect.top * 10);
				fs.dw = Math.ceil(event.rect.width * 10);
				fs.dh = Math.ceil(event.rect.height * 10);
			}
		});
	})
	.gesturable({
		onmove: function (event) {
			var id = event.target.getAttribute('id');
			var type = event.target.getAttribute('type');
			if (type == 'src') {
				return;
			}

			var fs = _.findWhere($scope.fss, {
				id: id
			});

			$scope.$apply(function () {
				fs.dr = (parseFloat(fs.dr) || 0) + Math.ceil(event.da);
			});
		}
	})

	function getCurrentInfo(element)
	{
		var textEl = element.querySelector('p');
		var x = (parseFloat(element.getAttribute('data-x')) || 0);
		var y = (parseFloat(element.getAttribute('data-y')) || 0);
		var angle = getCurrentRotateDegree(element);

		return {
			x: Math.ceil(x),
			y: Math.ceil(y),
			r: Math.ceil(angle)
		}
	}

	function getCurrentRotateDegree(element) 
	{
		var style = window.getComputedStyle(element, null);
		var trans = style.getPropertyValue('transform');

		if (trans == 'none') {
			return 0;
		}

		console.log('matrix: ', trans);

		var values = trans.split('(')[1].split(')')[0].split(',');
		var a = values[0];
		var b = values[1];
		var c = values[2];
		var d = values[3];
		var scale = Math.sqrt(a * a + b * b);
		var radians = Math.atan2(b, a);
		if (radians < 0) {
			radians += (2 * Math.PI);
		}
		var angle = Math.round(radians * (180 / Math.PI));

		return angle;
	}

	function dragMoveListener (event) {
		var target = event.target;

		var nodeid = target.getAttribute('nodeid');
		var screenid = target.getAttribute('screenid');

		var item = _.findWhere($scope.items, {
			nodeid: nodeid, screenid: screenid
		});

		$scope.$apply(function () {
			item.x = (parseFloat(item.x) || 0)+ Math.ceil(event.dx) * 10;
			item.y = (parseFloat(item.y) || 0) + Math.ceil(event.dy) * 10;
		});

		/*
		 * TODO this block div dragged 
		 *
		var content = getCurrentInfo(event.target);
		target.textContent = 'x: ' + Math.ceil(content.x * 10) ;
		target.textContent += ', y: ' + Math.ceil(content.y * 10);
		target.textContent += ', r: ' + Math.ceil(content.r;
		*/
	}

	function intputDragMoveListener (event) {
		var target = event.target;
		var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
		var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
		var angle = getCurrentRotateDegree(event.target);

		target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
		target.style.transform += 'rotate(' + angle + 'deg)';
		target.setAttribute('data-x', x);
		target.setAttribute('data-y', y);

		var nodeid = target.getAttribute('nodeid');
		var screenid = target.getAttribute('screenid');

		var input = _.findWhere($scope.inputs, {
			nodeid: nodeid, screenid: screenid
		});

		/*
		 * TODO this block div dragged 
		 *
		var content = getCurrentInfo(event.target);
		target.textContent = 'x: ' + Math.ceil(content.x * 10) ;
		target.textContent += ', y: ' + Math.ceil(content.y * 10);
		target.textContent += ', r: ' + Math.ceil(content.r;
		*/
	}

	function intputTransformDragMoveListener (event) {
		var devnode = event.target.getAttribute('devnode');

		var input = _.findWhere($scope.inputs, {
			devnode: devnode
		});

		$scope.$apply(function () {
			input.x = (parseFloat(input.x) || 0)+ Math.ceil(event.dx) * 10;
			input.y = (parseFloat(input.y) || 0) + Math.ceil(event.dy) * 10;
		});
	}

	function bgDragMoveListener (event) {
		var target = event.target;

		var id = target.getAttribute('id');

		var bg = _.findWhere($scope.bgs, {
			id: id
		});

		$scope.$apply(function () {
			bg.x = (parseFloat(bg.x) || 0) + Math.ceil(event.dx * 10);
			bg.y = (parseFloat(bg.y) || 0) + Math.ceil(event.dy * 10);
		});
	}

	function fsDragMoveListener (event) {
		var target = event.target;

		var id = target.getAttribute('id');
		var type = target.getAttribute('type');

		var fs = _.findWhere($scope.fss, {
			id: id
		});

		$scope.$apply(function () {
			if (type == 'src') {
				fs.sx = (parseFloat(fs.sx) || 0) + Math.ceil(event.dx * 10);
				fs.sy = (parseFloat(fs.sy) || 0) + Math.ceil(event.dy * 10);
			} else {
				fs.dx = (parseFloat(fs.dx) || 0) + Math.ceil(event.dx * 10);
				fs.dy = (parseFloat(fs.dy) || 0) + Math.ceil(event.dy * 10);
			}
		});
	}

	$ionicSideMenuDelegate.canDragContent(false);

	Shell.getNamespaces().$promise
	.then(function (curNamespaces) {
    $ionicLoading.show();
		console.log('get namespaces');
		namespaces = curNamespaces;
		$scope.nsIndex = namespaces.indexOf(ShellConstants.screenNs);
		$scope.modeNsIndex = namespaces.indexOf(ShellConstants.screenModeNs);
		if ($scope.nsIndex < 0) {
      return $q.reject('invalid namespace');
		}
		return Shell.getDev({ nsIdx: $scope.nsIndex }).$promise;
	})
	.then(function (plugedScreens) {
		_.each(plugedScreens, function (screen) {
			var kvs = ShellUtil.parseItemString(screen);
			var obj = ShellUtil.makeSetFormdata(kvs);
			tmpPlugedScreens.push(obj);
		});
		console.log('pluged screen list: ', tmpPlugedScreens);
		return Shell.get({ nsIdx: $scope.nsIndex }).$promise;
	})
	.then(function (allItems) {

		console.log('all screen list: ', allItems);
		_.each(allItems, function (item) {
			var kvs = ShellUtil.parseItemString(item);	
			var obj = ShellUtil.makeSetFormdata(kvs);
			$scope.items.push(obj);
		});

		var founds = [];
		$scope.items = _.filter($scope.items, function (item) {
			var found = _.findWhere(tmpPlugedScreens, {
				nodeid: item.nodeid,
				screenid: item.screenid
			});
			if (!found) {
				return false;
			}

			//$scope.plugedScreens = _.without(tmpPlugedScreens, found);
			founds.push(found);
			return true;
		});

		_.each(founds, function (found) {
			tmpPlugedScreens.splice(tmpPlugedScreens.indexOf(found), 1);
		});
		$scope.plugedScreens = tmpPlugedScreens;

		console.log('available screen on shell: ', $scope.items);
		console.log('remaining pluged screen device: ', $scope.plugedScreens);

		_.each($scope.items, function (item) {
			$scope.container.width += Math.ceil(item.width / 10);
			$scope.container.height += Math.ceil(item.height / 10);
		});
	})
	.catch(function (err) {
		console.log(err.stack);
	})
	.finally(function () {
    $ionicLoading.hide();
	});

	Shell.getNamespaces().$promise
	.then(function (curNamespaces) {
		namespaces = curNamespaces;
		$scope.touchInputNsIndex = namespaces.indexOf(ShellConstants.touchInputNs);
		$scope.inputNsIndex = namespaces.indexOf(ShellConstants.inputNs);
		return Shell.getDev({ nsIdx: $scope.touchInputNsIndex }).$promise;
	})
	.then(function (plugedInputs) {
		_.each(plugedInputs, function (input) {
			var kvs = ShellUtil.parseItemString(input);
			var obj = ShellUtil.makeSetFormdata(kvs);
			tmpPlugedInputs.push(obj);
		});
		console.log('pluged input list: ', tmpPlugedInputs);
		return Shell.get({ nsIdx: $scope.inputNsIndex }).$promise;
	})
	.then(function (allItems) {

		console.log('all input list: ', allItems);
		_.each(allItems, function (item) {
			var kvs = ShellUtil.parseItemString(item);	
			var obj = ShellUtil.makeSetFormdata(kvs);
			if ((obj.nodeid === undefined) || (obj.screenid === undefined)) {
				obj.useTransform = true;
			} else {
				obj.useTransform = false;
			}
			$scope.inputs.push(obj);
		});

		var founds = [];
		$scope.inputs = _.filter($scope.inputs, function (input) {
			var found = _.findWhere(tmpPlugedInputs, {
				devnode: input.devnode
			});
			if (!found) {
				return false;
			}

			founds.push(found);
			return true;
		});

		_.each(founds, function (found) {
			tmpPlugedInputs.splice(tmpPlugedInputs.indexOf(found), 1);
		});
		$scope.plugedInputs = tmpPlugedInputs;

		console.log('available inputs on shell: ', $scope.inputs);
		console.log('remaining pluged input device: ', $scope.plugedInputs);
	})
	.catch(function (err) {
		console.log(err.stack);
	});

	Shell.getNamespaces().$promise
	.then(function (curNamespaces) {
		namespaces = curNamespaces;
		$scope.bgNsIndex = namespaces.indexOf(ShellConstants.bgNs);
		return Shell.get({ nsIdx: $scope.bgNsIndex }).$promise;
	})
	.then(function (bgs) {
		console.log('backgrounds: ', bgs);
		_.each(bgs, function (bg) {
			var kvs = ShellUtil.parseItemString(bg);	
			var obj = ShellUtil.makeSetFormdata(kvs);
			$scope.bgs.push(obj);
		});
	})
	.catch(function (err) {
		console.log(err);
	});

	Shell.getNamespaces().$promise
	.then(function (curNamespaces) {
		namespaces = curNamespaces;
		$scope.fsNsIndex = namespaces.indexOf(ShellConstants.fsNs);
		return Shell.get({ nsIdx: $scope.fsNsIndex }).$promise;
	})
	.then(function (fss) {
		console.log('fullscreens: ', fss);
		_.each(fss, function (fs) {
			var kvs = ShellUtil.parseItemString(fs);	
			var obj = ShellUtil.makeSetFormdata(kvs);
			$scope.fss.push(obj);
		});
	})
	.catch(function (err) {
		console.log(err);
	});

  $ionicModal.fromTemplateUrl('templates/screenadvancedlayout.html', {
    scope: $scope,
		animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal.screen = modal;
  });

  $ionicModal.fromTemplateUrl('templates/screenadvancedinput.html', {
    scope: $scope,
		animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal.input = modal;
  });

  $ionicModal.fromTemplateUrl('templates/screenadvancedbg.html', {
    scope: $scope,
		animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal.bg = modal;
  });

  $ionicModal.fromTemplateUrl('templates/screenadvancedfs.html', {
    scope: $scope,
		animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal.fs = modal;
  });
});
