angular.module('nemocm.controllers')

.controller('ShellsCtrl', function($scope, $state, Shell) {
	angular.extend($scope, {
    loadNew: function () {
			Shell.getNamespaces().$promise
			.then(function (namespaces) {
				console.log('get namespaces');
				$scope.namespaces = namespaces;
			})
      .finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
      });
    },
    enter: function (index) {
			$state.go('app.shell', {
				key: (Math.floor((Math.random() * 1000)+1) - 1 ),
				namespace: { index: index, name: $scope.namespaces[index] }
			});
    } 
	});

	Shell.getNamespaces().$promise
	.then(function (namespaces) {
		console.log('get namespaces');
		$scope.namespaces = namespaces;
	})
	.catch(function (err) {
		console.log(err.stack);
	});
})

.controller('ShellCtrl', function($scope, $state, $stateParams, $ionicListDelegate, $ionicLoading, $ionicModal, $ionicPopup, $q, Shell, GUID, ShellConstants) {
  angular.extend($scope, {
    shouldShowDelete: false,
    listCanSwipe: true,
		namespace: $stateParams.namespace,
		items: $stateParams.items,
		newkvs: []
  });

	angular.extend($scope, {
    loadNew: function () {
			Shell.get({ nsIdx: $scope.namespace.index }).$promise
			.then(function (items) {
				console.log('success refresh');
				$scope.items = items;
			})
			.catch(function (err) {
				console.log(err.stack);
			})
      .finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
      });
    },
		enter: function(item) {
			$state.go('app.shelldetail', {
				key: (Math.floor((Math.random() * 1000)+1) - 1 ),
				namespace: $scope.namespace,
				item: item 
			});
			$ionicListDelegate.closeOptionButtons();
		},
		addItem: function () {
			console.log('start completing');

			var hasAttr = false;
			_.each($scope.newkvs, function (kv) {
				if (kv[0] === "" || kv[1] === "") { return; }
				hasAttr = true;
			});

			if (!hasAttr) {
				return $ionicPopup.alert({
					title: '알림',
					subTitle: '속성과 값 쌍을 최소 하나 이상 만들어주세요.',
					okText: '확인'
				});
			}

			var formdata = {};

			formdata["id"] = GUID.create();
			_.each($scope.newkvs, function (kv) {
				if (kv[0] === "" || kv[1] === "") { return; }
				formdata[kv[0]] = kv[1];
			});

      $ionicLoading.show();
			Shell.set({ nsIdx: $scope.namespace.index }, formdata).$promise
			.then(function () {
				console.log('success to add ns item');

				// convert formdata to string with delimiter
				console.log('success: ', formdata);
				var str = '';
				for (prop in formdata) {
					str += prop + ShellConstants.delimiter + formdata[prop];
					str += ShellConstants.delimiter;
				}
				$scope.items.push(str);
			})
			.catch(function (err) {
				console.log(err);
			})
      .finally(function () {
        $ionicLoading.hide();
				$ionicListDelegate.closeOptionButtons();
        // close progress bar
      });
      $scope.closeModal();
		},
		remove: function (item) {

			var tokens = item.split(ShellConstants.delimiter);
			var kv = [];
			var kvs = [];
			_.each(tokens, function (token, idx) {
				if (idx % 2) {
					kv.push(token);
					kvs.push(kv);
					kv = [];
					return;
				}
				kv.push(token);
			});

			var formdata = {};
			_.each(kvs, function (kv) {
				if (kv[0] === 'id') {
					return formdata = { 'id': kv[1] };
				}
			});

			if (!formdata.id) {
				// TODO open error popup
				$ionicPopup.alert({
					title: '알림',
					subTitle: 'id 없는 아이템은 삭제할 수 없어요.',
					okText: '확인'
				});
				return $ionicListDelegate.closeOptionButtons();
			}

      var option = {
        title: '정말 삭제하시겠어요?',
        subTitle: $scope.namespace.name + '<br>' + formdata.id, 
        scope: $scope,
        buttons: [
          {
					 	text: '취소',
						onTap: function (e) {
							return false;
						}
				 	},
          {
            text: '<b>확인</b>',
            type: 'button-assertive',
            onTap: function(e) {
            	return true;
            }
          }
        ]
      };
      $ionicPopup.show(option)
      .then(function (result) {
				if (result) {
					return Shell.delete({ nsIdx: $scope.namespace.index }, formdata).$promise
				} else {
          return $q.reject('user press cancel button');
				}
			})
			.then(function () {
				console.log('success to remove');
				$scope.items = _.without($scope.items, item);
			})
			.catch(function (err) {
				console.log('fail: ', err);
			});
		},
    showModal: function () {
			$ionicListDelegate.closeOptionButtons();
			$scope.newkvs = [];
			$scope.newkvs.push(["", ""]);
      $scope.modal.nsitem.show();
    },
    closeModal: function() {
      $scope.modal.nsitem.hide();
    },
		addAttr: function () {
			$scope.newkvs.push(["", ""]);
		}
	});

  $ionicModal.fromTemplateUrl('templates/addnsitem.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal.nsitem = modal;
  });

	Shell.get({ nsIdx: $scope.namespace.index }).$promise
	.then(function (items) {
		$scope.items = items;
	});
})

.controller('ShellDetailCtrl', function($scope, $stateParams, $ionicPopup, $q, $ionicHistory, $ionicListDelegate, Shell, ShellConstants) {
  angular.extend($scope, {
		namespace: $stateParams.namespace,
		kvs: [],
  });

	angular.extend($scope, {
		save: function() {
			var formdata = {};
			_.each($scope.kvs, function (kv) {
				formdata[kv[0]] = kv[1];
			});

			console.log("kvs: ", formdata);
			
			Shell.set({ nsIdx: $scope.namespace.index }, formdata).$promise
			.then(function () {
				return $ionicPopup.alert({
					title: '알림',
					subTitle: '성공적으로 저장되었어요.',
					okText: '확인'
				});
			})
			.catch(function (err) {
				console.log('fail: ', err.stack);
			});
		},
		remove: function () {
			var formdata = {};
			_.each($scope.kvs, function (kv) {
				if (kv[0] === 'id') {
					return formdata = { 'id': kv[1] };
				}
			});

			if (!formdata.id) {
				// TODO open error popup
				$ionicPopup.alert({
					title: '알림',
					subTitle: 'id 없는 아이템은 삭제할 수 없어요.',
					okText: '확인'
				});
				return $ionicListDelegate.closeOptionButtons();
			}

      var option = {
        title: '정말 삭제하시겠어요?',
        subTitle: $scope.namespace.name + '<br>' + formdata.id, 
        scope: $scope,
        buttons: [
          {
					 	text: '취소',
						onTap: function (e) {
							return false;
						}
				 	},
          {
            text: '<b>확인</b>',
            type: 'button-assertive',
            onTap: function(e) {
            	return true;
            }
          }
        ]
      };
      $ionicPopup.show(option)
      .then(function (result) {
				if (result) {
					return Shell.delete({ nsIdx: $scope.namespace.index }, formdata).$promise
				} else {
          return $q.reject('user press cancel button');
				}
			})
			.then(function () {
				console.log('success to remove');
				/*
			  $ionicHistory.backView().stateParams.items =	_.without(
					$ionicHistory.backView().stateParams.items,
					$stateParams.item
				);
				*/
				$ionicHistory.goBack();
			})
			.catch(function (err) {
				console.log('fail: ', err);
			});
		}
	});

	var tokens = $stateParams.item.split(ShellConstants.delimiter);

	var kv = [];
	_.each(tokens, function (token, idx) {
		if (idx % 2) {
			kv.push(token);
			$scope.kvs.push(kv);
			kv = [];
			return;
		}
		kv.push(token);
	});
});
