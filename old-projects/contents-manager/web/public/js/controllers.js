angular.module('nemocm.controllers', [])

.controller('MenuCtrl', function($scope, $ionicModal, $timeout) {
  console.log('hello');
})

.controller('HomeCtrl', function($rootScope, $scope, $ionicModal, $q, $ionicLoading, Shell, ShellConstants, ShellUtil, Auth) {
	angular.extend($scope, {
		loginData: {}
	});

	angular.extend($scope, {
		openLogin: function () {
    	$scope.modal.show();
		},
		closeLogin: function () {
    	$scope.modal.hide();
		},
		login: function() {
			console.log('Doing login', $scope.loginData);
			Auth.login({}, { id: $scope.loginData.id, pw: $scope.loginData.pw }).$promise
			.then(function (data) {
				if (data.result) {
					$rootScope.sessionUser = true;
					$scope.closeLogin();
				} else {
					$rootScope.sessionUser = false;
				}	
			})
			.catch(function () {
				$rootScope.sessionUser = false;
			});
		}
	});

	function initNetwork() {
		var deferred = $q.defer();

		$ionicLoading.show();
		Shell.getNamespaces().$promise
		.then(function (namespaces) {
			console.log('get namespaces');
			$scope.namespaces = namespaces;

			$scope.servicesNsIndex = $scope.namespaces.indexOf(ShellConstants.connmandNs.services);
			return Shell.getNetwork({ nsIdx: $scope.servicesNsIndex }).$promise;
		})
		.then(function (data) {
			var services = data.result.split(';');
			_.each(services, function (service) {
				var str = service.trim();
				var kvs = ShellUtil.parseItemString(str);
				var obj = ShellUtil.makeSetFormdata(kvs);
				$rootScope.services.push(obj);
			});

			$rootScope.onlines = _.filter($rootScope.services, function (service) {
				return (service.state == 'online') || (service.state == 'ready');
			});

			console.log('network services: ', $scope.services);
			console.log('network onlines: ', $scope.onlines);

			_.each($rootScope.onlines, function (online) {
				$rootScope.services = _.without($rootScope.services, online);
			});

			deferred.resolve();
		})
		.catch(function (err) {
			console.log(err);
			deferred.reject();
		})
		.finally(function () {
			$ionicLoading.hide();
		});

		return deferred.promise;
	}

  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
		if (!$rootScope.sessionUser) {
			$scope.openLogin();
		}
  });

	initNetwork();
})
