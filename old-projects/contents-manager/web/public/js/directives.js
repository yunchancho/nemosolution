angular.module('nemocm.directives', [])

.directive('fileModels', function ($parse) {
// usage: <input type="file" multiple file-model="newFiles.files" /> 
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;

      element.bind('change', function(){
        scope.$apply(function(){
          if (attrs.multiple) {
            modelSetter(scope, element[0].files);
          } else {
            modelSetter(scope, element[0].files[0]);
          }
        });
      });
    }
  };
})

.directive('ngFiles', function ($parse) {
// usage: <input type="file" multiple name="file" ng-files="getTheFiles($files)" />
  function fn_link(scope, element, attrs) {
    var onChange = $parse(attrs.ngFiles);
    element.on('change', function (event) {
      onChange(scope, { $files: event.target.files });
    });
  };

  return {
    link: fn_link
  };
})

.directive('customFileInput', function() {
/*
 * usage: 
 * <div custom-file-input ng-model="newFiles.files">
 *  <input type="file" name="image" ng-model="newFiles.files" /> 
 * </div>
 */
  return {
    restrict: 'EA',
    require: 'ngModel',
    link: function(scope, element, attrs, ngModelCtrl) {
      var fileInput = element[0].querySelector('input[type=file]');
      fileInput.addEventListener('change', handleFileInput);
      scope.$on('$destroy', function() {
        fileInput.removeEventListener('change', handleFileInput);
      });

      function handleFileInput(evt) {
        if (!this.files || !this.files[0]) { return; }
        var loadedFile = this.files[0];

        scope.$apply(function() {
          ngModelCtrl.$setViewValue(loadedFile);
        });
      }
    }
  };
})

.directive('screenLayoutBind', function () {
	return {
		restrict: 'A',
		scope: {
			item: '='
		},
		link: function(scope, element, attrs) {

			function updateAttr() {
				element.css('transform', 'translate(' + Math.ceil(scope.item.x / 10) + 'px, ' + Math.ceil(scope.item.y / 10) + 'px)');
				element.css('width', Math.ceil((scope.item.width / 10)) + 'px');
				element.css('height', Math.ceil((scope.item.height / 10)) + 'px');
				element.css('background-color', 'rgba(255,0,0,0.5)');
				element.css('z-index', '5');

				element.attr('nodeid', scope.item.nodeid);
				element.attr('screenid', scope.item.screenid); 
				element.attr('data-sx', scope.item.sx); 
				element.attr('data-sy', scope.item.sy); 
				element.attr('data-px', scope.item.px); 
				element.attr('data-py', scope.item.py); 
			}

			scope.$watch('item', function (newval, oldval) {
				updateAttr();
			}, true);
		}
	};
})

.directive('screenTransformBind', function () {
	return {
		restrict: 'A',
		scope: {
			item: '='
		},
		link: function(scope, element, attrs) {

			function updateAttr() {
				element.css('transform', 'translate(' + Math.ceil(scope.item.x / 10) + 'px, ' + Math.ceil(scope.item.y / 10) + 'px) rotate(' + (Math.ceil(scope.item.r) || 0) + 'deg) scale(' + scope.item.sx + ', ' + scope.item.sy + ')');
				element.css('width', Math.ceil((scope.item.width / 10)) + 'px');
				element.css('height', Math.ceil((scope.item.height / 10)) + 'px');
				element.css('background-color', 'rgba(0,0,255,0.5)');
				element.css('z-index', '6');
				element.css('color', 'blue');

				element.attr('nodeid', scope.item.nodeid);
				element.attr('screenid', scope.item.screenid); 
			}

			scope.$watch('item', function (newval, oldval) {
				updateAttr();
			}, true);
		}
	};
})

.directive('screenTransformBasicBind', function () {
	return {
		restrict: 'A',
		scope: {
			item: '='
		},
		link: function(scope, element, attrs) {

			function updateAttr() {
				element.css('transform-origin', Math.ceil((scope.item.px / scope.item.width) * 100) + '% ' + Math.ceil((scope.item.py / scope.item.height) * 100) + '%');
				element.css('transform', 'translate(' + Math.ceil(scope.item.x / 10) + 'px, ' + Math.ceil(scope.item.y / 10) + 'px) rotate(' + (Math.ceil(scope.item.r) || 0) + 'deg) scale(' + scope.item.sx + ', ' + scope.item.sy + ')');
				element.css('width', Math.ceil((scope.item.width / 10)) + 'px');
				element.css('height', Math.ceil((scope.item.height / 10)) + 'px');
				element.css('background-color', scope.item.map.scope? 'rgba(255,0,0,0.5)': 'rgba(0,0,0,0.5)');
				element.css('z-index', '6');
				element.css('color', 'blue');

				element.attr('nodeid', scope.item.nodeid);
				element.attr('screenid', scope.item.screenid); 
			}

			scope.$watch('item', function (newval, oldval) {
				updateAttr();
			}, true);
		}
	};
})

.directive('inputMappingBind', function () {
	return {
		restrict: 'A',
		scope: {
			input: '=',
			items: '='
		},
		link: function(scope, element, attrs) {

			function updateAttr() {
				console.log('scope.input: ', scope.input);

				var found = _.findWhere(scope.items, { 
					nodeid: scope.input.nodeid,
					screenid: scope.input.screenid
				});
				var x, y;
				if (found) {
					x = Math.ceil(found.x / 10) + Math.ceil(found.width / 2 / 10) - 25 + 'px';
					y = Math.ceil(found.y / 10) + Math.ceil(found.height/ 2 / 10) - 25 + 'px';
				} else {
					x = -100 + 'px';
					y = -100 + 'px';
				}
				console.log('input: ', scope.input.devnode, ', x: ', x, ', y: ', y);
				element.css('width', '50px');
				element.css('height', '50px');
				element.css('transform', 'translate(' + x + ', ' + y + ')');
				element.css('background-color', found? 'rgba(0,0,255,1.0)': 'rgba(0,0,255,0.2)'); 
				element.css('border-radius', '25px');
				element.css('padding-top', '13px');
				element.css('font-size', '20px');
				element.css('z-index', '10');
				element.css('border', '1px solid blue');

				element.attr('data-x', x);
				element.attr('data-y', y);
				element.attr('devnode', scope.input.devnode);
			}

			updateAttr();
			/*
			scope.$watch('input', function (newval, oldval) {
				updateAttr();
			}, true);
			*/
		}
	};
})

.directive('inputTransformBind', function () {
	return {
		restrict: 'A',
		scope: {
			input: '=',
			items: '='
		},
		link: function(scope, element, attrs) {

			function updateAttr() {
				console.log('scope.input: ', scope.input);
				var	x = Math.ceil(scope.input.x / 10) + 'px';
				var	y = Math.ceil(scope.input.y / 10) + 'px';

				element.css('width', Math.ceil((scope.input.width / 10)) + 'px');
				element.css('height', Math.ceil((scope.input.height / 10)) + 'px');
				element.css('transform', 'translate(' + Math.ceil(scope.input.x / 10) + 'px, ' + Math.ceil(scope.input.y / 10) + 'px) rotate(' + (Math.ceil(scope.input.r) || 0) + 'deg) scale(' + scope.input.sx + ', ' + scope.input.sy + ')');
				element.css('background-color', 'rgba(0,0,255,0.2)');
				element.css('padding-top', '13px');
				element.css('font-size', '20px');
				element.css('z-index', '10');
				element.css('border', '1px solid blue');

				element.attr('devnode', scope.input.devnode);
			}

			scope.$watch('input', function (newval, oldval) {
				updateAttr();
			}, true);
		}
	};
})

.directive('inputMappingBasicBind', function () {
	return {
		restrict: 'A',
		scope: {
			input: '=',
			items: '='
		},
		link: function(scope, element, attrs) {

			function updateAttr() {
				console.log('scope.input: ', scope.input);

				element.css('width', Math.ceil(scope.input.width / 10) + 'px');
				element.css('height', Math.ceil(scope.input.height / 10) + 'px');
				element.css('transform-origin', Math.ceil((scope.input.px / scope.input.width) * 100) + '% ' + Math.ceil((scope.input.py / scope.input.height) * 100) + '%');
				element.css('transform', 'translate(' + Math.ceil(scope.input.x / 10) + 'px, ' + Math.ceil(scope.input.y / 10) + 'px) rotate(' + (Math.ceil(scope.input.r) || 0) + 'deg) scale(' + scope.input.sx + ', ' + scope.input.sy + ')');
				element.css('background-color', 'rgba(0,0,255,0.5)'); 
				element.css('z-index', '10');
				element.css('border', '1px solid blue');
				element.css('line-height', Math.ceil(scope.input.height / 10) + 'px');
				element.css('text-align', 'center');
				element.css('font-size', '20px');

				element.attr('devnode', scope.input.devnode);
			}

			scope.$watch('input', function (newval, oldval) {
				updateAttr();
			}, true);
		}
	};
})


.directive('bgBind', function () {
	return {
		restrict: 'A',
		scope: {
			bg: '=',
			items: '='
		},
		link: function(scope, element, attrs) {

			function updateAttr() {
				console.log('scope.bg: ', scope.bg);

			  var x = Math.ceil(scope.bg.x / 10) + 'px';
				var y = Math.ceil(scope.bg.y / 10) + 'px';
				var width = Math.ceil(scope.bg.width / 10) + 'px';
				var height = Math.ceil(scope.bg.height / 10) + 'px';

				element.css('transform', 'translate(' + x + ', ' + y + ')');
				element.css('width', width);
				element.css('height', height);
				element.css('background-image', 'url(' + scope.bg.map.bgImageRemoteUrl + ')'); 
				element.css('background-size', width + ' ' + height);
				element.css('opacity', '0.6');
				element.css('z-index', '10');
				element.css('border', '1px solid pink');
				element.css('line-height', height);

				element.attr('data-x', x);
				element.attr('data-y', y);
				element.attr('id', scope.bg.id);
			}

			scope.$watch('bg', function (newval, oldval) {
				updateAttr();
			}, true);
		}
	};
})

.directive('fsSrcBind', function () {
	return {
		restrict: 'A',
		scope: {
			fs: '='
		},
		link: function(scope, element, attrs) {

			function updateAttr() {
				console.log('scope.fs: ', scope.fs);
				var	x = Math.ceil(scope.fs.sx / 10) + 'px';
				var	y = Math.ceil(scope.fs.sy / 10) + 'px';

				element.css('width', Math.ceil((scope.fs.sw / 10)) + 'px');
				element.css('height', Math.ceil((scope.fs.sh / 10)) + 'px');
				element.css('transform', 'translate(' + Math.ceil(scope.fs.sx / 10) + 'px, ' + Math.ceil(scope.fs.sy / 10) + 'px)');
				element.css('background-color', 'rgba(255,0,0,0.5)');
				element.css('z-index', '11');
				element.css('border', '1px solid blue');
				element.css('line-height', Math.ceil((scope.fs.sh / 10)) + 'px');
				element.attr('id', scope.fs.id);
				element.attr('type', 'src');
			}

			scope.$watch('fs', function (newval, oldval) {
				updateAttr();
			}, true);
		}
	};
})

.directive('fsDestBind', function () {
	return {
		restrict: 'A',
		scope: {
			fs: '='
		},
		link: function(scope, element, attrs) {

			function updateAttr() {
				console.log('scope.fs: ', scope.fs);
				var	x = Math.ceil(scope.fs.dx / 10) + 'px';
				var	y = Math.ceil(scope.fs.dy / 10) + 'px';

				element.css('width', Math.ceil((scope.fs.dw / 10)) + 'px');
				element.css('height', Math.ceil((scope.fs.dh / 10)) + 'px');
				element.css('transform', 'translate(' + Math.ceil(scope.fs.dx / 10) + 'px, ' + Math.ceil(scope.fs.dy / 10) + 'px) rotate(' + (Math.ceil(scope.fs.dr) || 0) + 'deg)');
				element.css('background-color', 'rgba(0,0,255,0.5)');
				element.css('z-index', '10');
				element.css('border', '1px solid blue');
				element.css('line-height', Math.ceil((scope.fs.dh / 10)) + 'px');
				element.css('text-align', 'center');
				element.attr('id', scope.fs.id);
				element.attr('type', 'dest');
			}

			scope.$watch('fs', function (newval, oldval) {
				updateAttr();
			}, true);
		}
	};
})

.directive('fileModel', function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;

			element.bind('change', function () {
				modelSetter(scope, element[0].files[0]);
			});
		}

	};
})
