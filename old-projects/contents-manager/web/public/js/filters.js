angular.module('nemocm.filters', [])

.filter("trustUrl", function ($sce) {
  return function (url) {
    return $sce.trustAsResourceUrl(url);
  };
})

.filter('notHiddenFile', function() {
  return function (files) {
    var filtered = [];
    _.each(files, function (file) {
      if (file.name.charAt(0) != '.') {
        filtered.push(file);
      }
    });
    return filtered;
  };
});
