angular.module('nemocm.services', [])

.factory('Machine', function () {
  return {
    getId: function () {
      return "a13dbakel135l";
    }
  };
})

.factory('Auth', function ($resource, $rootScope) {
	return $resource($rootScope.contentServerAddr + 'auth/login', {},
		{ 
			login: { 
				method: 'POST',
				headers : {
					'Content-Type': 'application/json'
				},
				params: {}
			}
		}
	);
})	

.factory('Content', function ($resource, $rootScope) {
  return $resource($rootScope.contentServerAddr + 't1/content/:machineid',
    { machineid: $rootScope.machineid },
    {
      // predefined method 'query' is used for get file list in a directory
      // 'get' method is used for get specific file itself
      get: { method: 'GET' },
      add: { 
        method: 'POST', 
        transformRequest: angular.identity,
        headers : {
          'Content-Type': undefined
        }
      },
      delete: { method: 'DELETE' },
      edit: {
        method: 'PUT',
        headers : {
          'Content-Type': 'x-www-form-urlencoded'
        }
      }
    }
  );
})

.factory('Bookmark', function ($resource, $rootScope) {
  return $resource($rootScope.contentServerAddr + 't2/bookmark/:machineid',
    { machineid: $rootScope.machineid },
    {
      // predefined method 'query' is used for get file list in a directory
      // 'get' method is used for get specific file itself
      get: { method: 'GET' },
      add: { 
        method: 'POST', 
        transformRequest: angular.identity,
        headers : {
          'Content-Type': undefined
        }
      },
      delete: { method: 'DELETE' },
      edit: {
        method: 'PUT',
        headers : {
          'Content-Type': 'x-www-form-urlencoded'
        }
      }
    }
  );
})

.factory('Bgimage', function ($resource, $rootScope) {
  return $resource($rootScope.contentServerAddr + 't1/bgimage/:machineid',
    { machineid: $rootScope.machineid },
    {
      // predefined method 'query' is used for get file list in a directory
      // 'get' method is used for get specific file itself
      get: { method: 'GET' },
      add: { 
        method: 'POST', 
        transformRequest: angular.identity,
        headers : {
          'Content-Type': undefined
        }
      },
      delete: { method: 'DELETE' },
      edit: {
        method: 'PUT',
        headers : {
          'Content-Type': 'x-www-form-urlencoded'
        }
      }
    }
  );
})

.factory('File', function ($resource, $rootScope) {
  return $resource($rootScope.contentServerAddr + 'file/:limit',
		{ limit: '@limit' }, 
		{
			send: {
				method: 'POST',
        transformRequest: angular.identity,
        headers : {
          'Content-Type': undefined
        },
				params: {}
			}
		}
	);
})

.factory('Shell', function ($resource, $rootScope) {
  return $resource($rootScope.contentServerAddr + 'shell/namespace/:nsIdx',
    { nsIdx: '@id' },
    {
      // predefined method 'query' is used for get file list in a directory
      // 'get' method is used for get specific file itself
      getNamespaces: { 
				url: $rootScope.contentServerAddr + 'shell/namespaces',
				method: 'GET',
				isArray: true
		 	},
			get: {
			 	method: 'GET',
				isArray: true
		 	},
			getDev: {
				url: $rootScope.contentServerAddr + 'shell/namespace/:nsIdx/dev',
        headers : {
          'Content-Type': 'application/json'
        },
				method: 'GET',
				isArray: true
			},
      set: {
        method: 'POST',
        headers : {
          'Content-Type': 'application/json'
        },
				params: {}
      },
      delete: {
			 	method: 'PUT',
        headers : {
          'Content-Type': 'application/json'
        },
				params: {}
		 	},
			getActionTypes: {
				url: $rootScope.contentServerAddr + 'shell/actiontypes',
				method: 'GET',
				isArray: true
			},
      getNetwork: {
        method: 'GET',
				url: $rootScope.contentServerAddr + 'shell/namespace/:nsIdx/network',
        headers : {
          'Content-Type': 'application/json'
        }
      },
      setNetwork: {
        method: 'POST',
				url: $rootScope.contentServerAddr + 'shell/namespace/:nsIdx/network',
        headers : {
          'Content-Type': 'application/json'
        },
				params: {}
      },
      scanNetwork: {
        method: 'POST',
				url: $rootScope.contentServerAddr + 'shell/namespace/:nsIdx/network/scan',
        headers : {
          'Content-Type': 'application/json'
        },
				params: {}
      },
      connectNetwork: {
        method: 'POST',
				url: $rootScope.contentServerAddr + 'shell/namespace/:nsIdx/network/connect',
        headers : {
          'Content-Type': 'application/json'
        },
				params: {}
      },
      disconnectNetwork: {
        method: 'POST',
				url: $rootScope.contentServerAddr + 'shell/namespace/:nsIdx/network/disconnect',
        headers : {
          'Content-Type': 'application/json'
        },
				params: {}
      },
      configEthernet: {
        method: 'POST',
				url: $rootScope.contentServerAddr + 'shell/namespace/:nsIdx/network/config',
        headers : {
          'Content-Type': 'application/json'
        },
				params: {}
      }
    }
  );
})

.factory('ShellUtil', function (ShellConstants) {
	function parseItemString(str) 
	{
		var kv = [];
		var kvs = [];

		str = str.replace(/\"/g, "");

		console.log(str);

		var tokens = str.split(ShellConstants.delimiter);

		_.each(tokens, function (token, idx) {
			if (idx % 2) {
				kv.push(token);
				kvs.push(kv);
				kv = [];
				return;
			}
			kv.push(token);
		});

		return kvs;
	}

	function makeSetFormdata(kvs)
	{
		var formdata = {};
		_.each(kvs, function (kv) {
			formdata[kv[0]] = kv[1];
		});
		return formdata;
	}

	function getFilename (path)
	{
		return path.replace(/\\/g,'/').replace(/.*\//, '');
	}

	return {
		parseItemString: parseItemString,
		makeSetFormdata: makeSetFormdata,
		getFilename: getFilename
	}
})

.factory('GUID', function () {
	function pickOne (c) {
		var num = Math.random() * 16 | 0;
		var v = (c === 'x') ? num : (num & 0x3 | 0x8);
		return v.toString(16);
	}

	return {
		create: function () {
			var uid = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'.replace(/[x]/g, pickOne);
			return uid;
		}
	}
})

.factory('Dropdown', function ($ionicPopover) {

	function create (scope, info) {
		scope.__dropdown__ = info;
		$ionicPopover.fromTemplate('templates/dropdown.html', {
			scope: scope
		})
		.then(function (popover) {
			return scope.popover;
		});
	}
	
	function open (items) {
	}

	function hide () {
	}

	return {
		open: open, 
		hide: hide
	}
})

.factory('End', function () {
});
