const dgram = require('dgram');
const client = dgram.createSocket('udp4');
const _ = require('underscore');
const express = require('express');
const router = express.Router();
const info = require('./shellinfo.json');
const EventEmitter = require('events');
class ShellEmitter extends EventEmitter {}

var app;
var reqQueue = [];
var shellEmitter = new ShellEmitter();

router.get('/namespaces', (req, res) => {
	res.send(_.pluck(info.namespaces, "object"));
});

router.get('/namespace/:nid', (req, res) => {

	var nid = req.params.nid;
	if (nid === 'undefined') {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	if (nid < 0 || nid >= info.namespaces.length) {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	var formdata = req.query;
	console.log('req.query: ', formdata);
	var object = info.namespaces[nid].object;
	reqQueue.push({ object: object, res: res, result: [] });

	var command = info.socket.clientuid + ' ';
  command += info.namespaces[nid].subject + ' get ' + object;
	for(var prop in formdata) {
		command += ' ' + prop + ' "' + formdata[prop] + '"';
	}
	console.log('command: ', command);

	client.send(command, info.socket.serverport, info.socket.serverhost);

	setTimeout(function () {
		let req = _.findWhere(reqQueue, { object: object, res: res });
		reqQueue = _.without(reqQueue, req);

		// remove duplicate result
		req.result = _.uniq(req.result);
		return res.send(req.result);
	}, info.socket.timeout);
});

router.get('/namespace/:nid/dev', (req, res) => {

	var nid = req.params.nid;
	if (nid === 'undefined') {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	if (nid < 0 || nid >= info.namespaces.length) {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	var formdata = req.query;
	console.log('req.query: ', formdata);
	var object = info.namespaces[nid].object;
	reqQueue.push({ object: object, res: res, result: [] });

	var command = info.socket.clientuid + ' ';
  command += info.namespaces[nid].subject + ' dev ' + object;
	for(var prop in formdata) {
		command += ' ' + prop + ' "' + formdata[prop] + '"';
	}

	console.log('command: ', command);
	client.send(command, info.socket.serverport, info.socket.serverhost);

	setTimeout(function () {
		let req = _.findWhere(reqQueue, { object: object, res: res });
		reqQueue = _.without(reqQueue, req);

		// remove duplicate result
		req.result = _.uniq(req.result);
		return res.send(req.result);
	}, info.socket.timeout);
});

router.post('/namespace/:nid', (req, res) => {
	var nid = req.params.nid;
	if (nid === 'undefined') {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	if (nid < 0 || nid >= info.namespaces.length) {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	var formdata = req.body;
	var object = info.namespaces[nid].object;
	reqQueue.push({ object: object, res: res, result: []});

	var command = info.socket.clientuid + ' ';
	command += info.namespaces[nid].subject + ' set ' + object;

	for(var prop in formdata) {
		command += ' ' + prop + ' "' + formdata[prop] + '"';
	}

	console.log('command: ', command);
	client.send(command, info.socket.serverport, info.socket.serverhost);
	setTimeout(function () {
		let req = _.findWhere(reqQueue, { object: object, res: res });
		reqQueue = _.without(reqQueue, req);
		return res.send({ success: true });
	}, info.socket.timeout);
});

router.put('/namespace/:nid', (req, res) => {
	var nid = req.params.nid;
	if (nid === 'undefined') {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	if (nid < 0 || nid >= info.namespaces.length) {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	var formdata = req.body;
	var object = info.namespaces[nid].object;
	reqQueue.push({ object: object, res: res, result: []});

	var command = info.socket.clientuid + ' ';
	command += info.namespaces[nid].subject + ' put ' + object;

	console.log('formdata: ', formdata);
	for(var prop in formdata) {
		command += ' ' + prop + ' "' + formdata[prop] + '"';
	}

	console.log('command: ', command);
	client.send(command, info.socket.serverport, info.socket.serverhost);
	setTimeout(function () {
		let req = _.findWhere(reqQueue, { object: object, res: res });
		reqQueue = _.without(reqQueue, req);
		return res.send({ success: true });
	}, info.socket.timeout);
});


router.get('/namespace/:nid/network', (req, res) => {

	var nid = req.params.nid;
	if (nid === 'undefined') {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	if (nid < 0 || nid >= info.namespaces.length) {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	var formdata = req.query;
	console.log('req.query: ', formdata);
	var object = info.namespaces[nid].object;
	//reqQueue.push({ object: object, res: res, result: [] });

	var command = info.socket.clientuid + ' ';
  command += info.namespaces[nid].subject + ' get ' + object;
	for(var prop in formdata) {
		command += ' ' + prop + ' "' + formdata[prop] + '"';
	}
	console.log('command: ', command);

	client.send(command, info.socket.serverport, info.socket.serverhost);

	shellEmitter.once(object, (result) => {
		console.log('responsed result: ', result);
		return res.send({ result: result });
	});
});

router.post('/namespace/:nid/network', (req, res) => {
	var nid = req.params.nid;
	if (nid === 'undefined') {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	if (nid < 0 || nid >= info.namespaces.length) {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	var formdata = req.body;
	var object = info.namespaces[nid].object;
	//reqQueue.push({ object: object, res: res, result: []});

	var command = info.socket.clientuid + ' ';
	command += info.namespaces[nid].subject + ' set ' + object;

	for(var prop in formdata) {
		command += ' ' + prop + ' "' + formdata[prop] + '"';
	}

	console.log('command: ', command);
	client.send(command, info.socket.serverport, info.socket.serverhost);
	shellEmitter.once(object, (result) => {
		console.log('responsed result: ', result);
		return res.send({ result: result });
	});
});

router.post('/namespace/:nid/network/:cmd', (req, res) => {
	var nid = req.params.nid;
	if (nid === 'undefined') {
		return res.send(500).json({ error: 'invalid namespace' });
	}

	if (nid < 0 || nid >= info.namespaces.length) {
		return res.send(500).json({ error: 'invalid namespace' });
	}
	
	if (!_.contains(info.networkNsCommands, req.params.cmd)) {
		return res.send(500).json({ error: 'invalid network command' });
	}

	var formdata = req.body;
	var object = info.namespaces[nid].object;
	//reqQueue.push({ object: object, res: res, result: []});

	var command = info.socket.clientuid + ' ';
	command += info.namespaces[nid].subject + ' ' + req.params.cmd + ' ' + object;

	for(var prop in formdata) {
		command += ' ' + prop + ' "' + formdata[prop] + '"';
	}

	console.log('command: ', command);
	client.send(command, info.socket.serverport, info.socket.serverhost);

	shellEmitter.once(object, (result) => {
		console.log('responsed result: ', result);
		return res.send({ result: result });
	});
});

router.get('/actiontypes', (req, res) => {
	res.send(info.actionTypes);
});

//TODO add POST, DELETE 

// initialize UDP socket 
client.on('message', (msgBuffer, rinfo) => {
	console.log('Recieved:', msgBuffer.toString('utf8'));

	var data = msgBuffer.toString().replace(/\0/g, '')
	var tokens = data.split(info.delimiter);
	var namespace = tokens[3];

	// TODO we need to find out obsject for this event more flexiblely
	var reqs = _.where(reqQueue, { object: namespace });
	if (msgBuffer.toString('utf8') == info.socket.livemsg) {
		console.log('this is livemsg');
		return;
	}

	tokens.splice(0, 4); // remove unnecessary info
	_.each(reqs, function (req) {
		let str = tokens.join(info.delimiter);
		if (req.result.indexOf(str) >= 0) {
			return;	
		}
		str = str.replace(/\"/g, "");
		req.result.push(str);
	});

	let str = tokens.join(info.delimiter).replace(/\"/g, "");
	console.log("sended string: ", str);
	shellEmitter.emit(namespace, str);
});

var livemsg = info.socket.clientuid + ' ' + info.socket.livemsg;
client.bind(info.socket.clientport, info.socket.clienthost);
setInterval(function () {
	client.send(livemsg, info.socket.serverport, info.socket.serverhost);
}, 2000);

process.on('uncaughtException', function (err) {
	console.log('Hur!!: ', err.stack);
});

module.exports = function (inst) {
	app = inst;
	return router;
};
