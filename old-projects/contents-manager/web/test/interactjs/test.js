var angle = 0;

interact('.draggable')
.draggable({
	inertia: true,
	restrict: {
		restriction: "parent",
		endOnly: true,
		elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
	},
	//autoScroll: true,
	onmove: dragMoveListener,
	onend: function (event) {
		console.log("onend event");
		var content = getCurrentInfo(event.target);
		event.target.textContent = 'x: ' + content.x;
		event.target.textContent += ', y: ' + content.y;
		event.target.textContent += ', r: ' + content.r;
	},
	/*
	snap: {
		targets: [
			interact.createSnapGrid({ x: 1, y: 1 })
		],
		range:Infinity,
		relativePoints: [{ x: 0, y: 0 }]
	}
	*/
})
.resizable({
	preserveAspectRatio: true,
	edges: {
		left: true, right: true, bottom: true, top: true
	}
})
.on('resizemove', function (event) {
	var target = event.target;
	var x = (parseFloat(target.getAttribute('data-x')) || 0);
	var y = (parseFloat(target.getAttribute('data-y')) || 0);

	var delta = event.rect.width - target.style.width;
	var deltadir = 1;

	console.log('event.rect: ', event.rect);
	console.log('event.deltaRect: ', event.deltaRect);
	/*
	if (delta < 0) {
		deltadir = -1;
	}
	target.style.width = target.style.width + delta - deltadir * (Math.abs(delta) % 10);
	target.style.width += 'px';
	console.log('snaped delta width: ', delta - deltadir * (Math.abs(delta) % 10));

	delta = event.rect.height - target.style.height;
	deltadir = 1;
	if (delta < 0) {
		deltadir = -1;
	}
	target.style.height = target.style.height + delta - deltadir * (Math.abs(delta) % 10);
	target.style.height += 'px';
	console.log('snaped delta height: ', delta - deltadir * (Math.abs(delta) % 10));
	*/

	target.style.width = Math.round(event.rect.width / 10.0) * 10 + 'px';
	target.style.height = Math.round(event.rect.height/ 10.0) * 10 + 'px';

	x += event.deltaRect.left;
	y += event.deltaRect.top;

	target.setAttribute('data-x', x);
	target.setAttribute('data-y', y);

	var angle = getCurrentRotateDegree(event.target);
	target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
	target.style.transform += 'rotate(' + angle + 'deg)';

})
.gesturable({
	onmove: function (event) {
		var angle = getCurrentRotateDegree(event.target);
		console.log('current angle: ', angle);
		console.log('delta angle: ', event.da);
		angle += event.da;

		var x = (parseFloat(event.target.getAttribute('data-x')) || 0);
		var y = (parseFloat(event.target.getAttribute('data-y')) || 0);
		event.target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
		event.target.style.transform += 'rotate(' + angle + 'deg)';

		var content = getCurrentInfo(event.target);
		event.target.textContent = 'x: ' + content.x;
		event.target.textContent += ', y: ' + content.y;
		event.target.textContent += ', r: ' + content.r;
	}
})
.on('tap', function (event) {
	event.target.style.zIndex++;
	event.preventDefault();
});

function getCurrentInfo(element)
{
	var textEl = element.querySelector('p');
	var x = (parseFloat(element.getAttribute('data-x')) || 0);
	var y = (parseFloat(element.getAttribute('data-y')) || 0);
	var angle = getCurrentRotateDegree(element);

	return {
		x: Math.ceil(x),
		y: Math.ceil(y),
		r: Math.ceil(angle)
	}
}

function getCurrentRotateDegree(element) 
{
	var style = window.getComputedStyle(element, null);
	var trans = style.getPropertyValue('transform');

	if (trans == 'none') {
		return 0;
	}

	console.log('matrix: ', trans);

	var values = trans.split('(')[1].split(')')[0].split(',');
	var a = values[0];
	var b = values[1];
	var c = values[2];
	var d = values[3];
	var scale = Math.sqrt(a * a + b * b);
	var radians = Math.atan2(b, a);
	if (radians < 0) {
		radians += (2 * Math.PI);
	}
	var angle = Math.round(radians * (180 / Math.PI));

	return angle;
}

function dragMoveListener (event) {
	var target = event.target;
	var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
	var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
	var angle = getCurrentRotateDegree(event.target);

	target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
	target.style.transform += 'rotate(' + angle + 'deg)';
	target.setAttribute('data-x', x);
	target.setAttribute('data-y', y);
}

/*
interact('.dropzone')
.dropzone({
	accept: '#drag-1, #drag-2',
	overlap: 0.75,

	ondropactivate: function (event) {
		event.target.classList.add('drop-active');
	},

	ondragenter: function (event) {
		var dragEl = event.relatedTarget;
		var dropzoneEl = event.target;

		dropzoneEl.classList.add('drop-target');
		dragEl.classList.add('can-drop');
		dragEl.textContent = 'Dragged in';
	},
	ondragleave: function (event) {
		var dragEl = event.relatedTarget;
		var dropzoneEl = event.target;

		dropzoneEl.classList.remove('drop-target');
		dragEl.classList.remove('can-drop');
		dragEl.textContent = 'Dragged out';
	},
	ondrop: function (event) {
		var dragEl = event.relatedTarget;
		dragEl.textContent = 'Dropped';
	},
	ondropdeactivate: function (event) {
		var dropzoneEl = event.target;
		dropzoneEl.classList.remove('drop-active');
		dropzoneEl.classList.remove('drop-target');
	}
});
*/

//interact.maxInteractions(Infinity);
