var winston = require('winston');
var storage = require('./storage');
var bot = require('./bot');

var dbpath = ".bot.db";

// setting winston log
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {
	colorize: true,
	timestamp: true,
	humanReadableUnhandledException: true,
});

/*
winston.add(winston.transports.File, {
	filename: '/opt/rdata-daemon-log',
	colorize: true,
	json: false,
	maxsize: 10 * 1024 * 1024,
	maxFiles: 100
});
*/


storage.prepare(dbpath)
.then(function () {
	if (!bot.run(storage)) {
		process.exit(-1);
	}
	winston.info('storage prepared!');
})
.catch(function (err) {
	console.log(err);
	process.exit(-1);
});

process.on('unhandledRejection', function (reason, p) {
	winston.info("Unhandled Rejection at: ", p, " reason: ", reason);
});

process.on('uncaughtException', function (err) {
	winston.info('Uncaught Exception: ', err);
});
