var fs = require('fs');
var spawn = require('child_process').spawn;
var Q = require('q');
var winston = require('winston');
var _ = require('underscore');
var telebot = require('node-telegram-bot-api');
var webshot = require('webshot');
var meta = require('./meta');
var chart = require('./chart');

var token = '215856904:AAHNMa5w9FH4g8Tu1hfcFWvyYv6QMAGweiE';
var bot;
var db;
var ffs = [];
var fds = [];
var allDone = false;

function getJoinMembers()
{
	var deferred = Q.defer();
	var members = [];

	if (!db) {
		return deferred.promise.theReject("wrong db");
	}

	db.select("member").then(function (items) {
		deferred.resolve(items);
	})
	.catch(function (err) {
		winston.info(err);
		deferred.reject(err);
	});

	return deferred.promise;
}	

function initDefaultFoods() {
	var init_food_num = 2;
	db.select('menu').then(function (foods) {
		if (!foods.length) {
			winston.info("available foods not exits");
			return;
		}

		var rand;
		var tmps = [];
		fds = [];
		for (var i = 0; i < init_food_num; i++) {
			while (rand = getRandomInt(0, foods.length - 1)) {
				if (_.contains(tmps, rand)) {
					continue;
				}
				fds.push(foods[rand]);
				tmps.push(rand);
				break;
			}
		}
		winston.info("selected random foods: ", fds);
	});
}

// JUST FOR TEST FUNC
function echo(msg, match) {
	winston.log('/echo', msg);
	var fromId = msg.from.id;
	var resp = match[1];

	bot.sendMessage(fromId, resp, {
    reply_to_message_id: msg.message_id,
    reply_markup: {
      one_time_keyboard : true,
			//force_reply: true,
      inline_keyboard : [
				[
				{ text: 'hello', callback_data: '1st' },
			 	{ text: 'world', callback_data: '2nd' },
			 	{ text: 'yunchan', callback_data: '3rd' }
				]
			]
    }
  })
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;
		bot.onReplyToMessage(chatId, messageId, function (message) {
			winston.log('user reply is %s', message.text);
		});
		winston.log('success to send message');
	})
	.catch(function (err) {
		winston.log(err);
	});
}

function start(msg, match) {
	console.log(msg);
	var deferred = Q.defer();
	var menus = [];
	var members = [];
	var chatId = msg.chat.id;
	var message = '';

	for (var i = 0; i < 15; i++) {
		message += '&#9603';
	}

	var message = "\n&#9787<b>오늘 뭐 먹을까? </b>&#9787\n";
	for (var i = 0; i < 15; i++) {
		message += '&#9603';
	}

	if (msg.updateReason) {
		message += msg.updateReason;
	}
	message += "\n\n";
	members = _.filter(ffs, function (ff) {
		return ff.join;
	});
	message += "<strong>참가 푸드 파이터(" + members.length + "명): </strong>\n";

	_.each(members, function (member) {
		if (member && member.select) {
			message += '&#9745 ';
		} else {
			message += '&#32&#32&#32&#32&#32 ';
		}

		message += member.info.name;
		if (member.info.id != '') {
			message += ' (@' + member.info.id + ')';
		}
		message += '\n';
	});

	if (!members.length) {
		message += "등록된 파이터가 없어요&#9785 \n\n";
	}

	if (!_.findWhere(members, { tid: chatId })) {
		message += "\n푸드파이터로 참가해야 투표가 가능합니다.\n/join 눌러주세요.\n\n";
	}
	if (!allDone) {
		message += "\n아래 음식 중 한개를 골라 숫자를 입력해주세요. 먹고 싶은게 없으면 /foodlist 를 눌러서 추가해주세요.";
	} else {
		message += "\n투표 완료!!!!!!!!!!";
	}

	var row = [];
	// if you want to change col count, change this
	var colcount = 1;
	_.each(fds, function (item, index) {
		if (index && index % colcount == 0) {
			menus.push(row);
			row = [];
		}
		var seq = index % colcount;
		row.push({ text: (index + 1) + '. ' + item.name, url: item.url });
	});
	menus.push(row);
	winston.info(JSON.stringify(menus));

	var option = {
		parse_mode: "HTML"
	};
	if (fds.length) {
		option.reply_markup = {
			inline_keyboard: menus
		};
	}

	bot.sendMessage(chatId, message, option)
	.then(function (sended) {
		var ff = _.findWhere(ffs, { tid: sended.chat.id });
		if (!ff) {
			ffs.push({ tid: sended.chat.id, msgid: sended.message_id, select: 0, join: 0 });
		} else {
			ff.msgid = sended.message_id;
		}
		winston.info('success to send message');
		deferred.resolve();
	})
	.catch(function (err) {
		winston.info(err);
		deferred.resolve();
	});

	return deferred.promise;
}

function join(msg, match) {
	var chatId = msg.chat.id;
	var table = _.findWhere(meta.tables, { name: "member" });
	var keys = [];
	var row = [];

	_.each(table.keys, function (key) {
		keys.push(key.name);
	});

	// tid
	row.push(msg.from.id); 
	// name
	if (msg.from.last_name || msg.from.first_name) {
		row.push(msg.from.last_name + msg.from.first_name); 
	} else {
		row.push(msg.from.username);
	}
	//id
	if (msg.from.username) {
		row.push(msg.from.username);
	} else {
		row.push('');
	}
	row.push(new Date());

	db.insert(table.name, keys, row)
	.then(function (lastId) {
		console.log(lastId);
		return db.select(table.name, ["tid"], [msg.from.id]);
	})
	.then(function (rows) {
		// rows length should be only 1
		console.log(rows);
		var ff = _.findWhere(ffs, { tid: rows[0].tid });
		if (!ff) {
			// TODO error handling
			//return Q.defer().promise.theReject(new Error);
			throw new Error("there is no fighter chatting with chat bot");
		}
		ff.join = 1;
		ff.select = 0;
		ff.info = rows[0];

		return bot.sendMessage(chatId, "푸드파이터가 되셨어요. 먹고싶은 번호를 입력해주세요. ");
	})
	.then(function (sended) {
		var reason = "\n" + msg.from.last_name + msg.from.first_name + " 님이 참가했어요.\n";
		_.each(ffs, function (ff) {
			var msg = {
				chat: {
					id : ff.tid
				},
				updateReason: reason
			}
			start(msg);
		});
	})
	.catch(function (err) {
		console.log("Join err: " + err.stack);
		bot.sendMessage(chatId, "아앗! 뭔가 잘못처리됐어요..");
	});
}

function unjoin(msg, match) {
	var chatId = msg.chat.id;
	var table = _.findWhere(meta.tables, { name: "member" });
	var keys = ['tid'];
	var row = [];

	row.push(msg.from.id); 

	db.remove(table.name, keys, row)
	.then(function (count) {
		if (count) {
			ff = _.findWhere(ffs, { tid: msg.from.id, join: 1 });
			if (!ff) {
				throw new Error("there is no fighter chatting with chat bot");
			}
			ff.join = 0;

			bot.sendMessage(chatId, "이제 푸드파이터가 아니네요. 푸드파이터가 되고싶으면 언제든 /join 입력해주세요. ")
			.then(function (sended) {
				var reason = "\n" + msg.from.last_name + msg.from.first_name + "님이 퇴장했어요.\n";
				_.each(ffs, function (ff) {
					var msg = {
						chat: {
							id : ff.tid
						},
						updateReason: reason
					}
					start(msg);
				});
			})
			.catch(function () {
				bot.sendMessage(chatId, "아앗, 뭔가 잘못됐어요.");
			});
		} else {
			bot.sendMessage(chatId, "엇, 이미 탈퇴하셨어요~");
		}
	})
	.catch(function () {
		bot.sendMessage(chatId, "아앗, 뭔가 잘못됐어요.");
	});
}

function countSelections() {
	var result = [];

	_.each(fds, function (fd, index) {
		var tmp = _.filter(ffs, function (ff) {
			return ff.join && (ff.select == (index + 1));
		});
		console.log('name: ', fd.name, '(', tmp, ')');
		result.push({ name: fd.name, count: tmp.length, info: fd });
	});

	return result;
}

function result() {
	allDone = true;

	_.each(ffs, function (ff) {
		bot.sendMessage(ff.tid, "투표가 종료되었어요. 결과 사진을 보시죠!");
	});

	var selections = countSelections();

	var svg = chart.getBarChart({
		data: selections,
		width: 800,
		height: 600,
		xAxisLabel: 'menu',
		yAxisLabel: 'votes',
		containerId: 'bar-chart-large'
	});

	webshot(svg, 'result.png', {
		screenSize: { width: 800, height: 600 },
		siteType: 'html',
		// we should use phantom v1.9 not 2.x
		phantomPath: require('phantomjs').path,
	}, function (err) {
		if (err) {
			return;
		}
		_.each(ffs, function (ff) {
			bot.sendPhoto(ff.tid, 'result.png');
		});
		spawn('/usr/bin/foodfighter', ['-p', './result.png']);
	});

	var table = _.findWhere(meta.tables, { name: "result" });
	var keys = [];
	var row = [];

	_.each(table.keys, function (key) {
		if (!key.auto) {
			keys.push(key.name);
		}
	});

	var maxsel = _.max(selections, function (sel) {
		return sel.count;
	});

	console.log("maxsel: ", maxsel);

	row.push(maxsel.info.id); 
	row.push(maxsel.info.name); 
	row.push(ffs.length); 
	row.push(maxsel.count); 
	row.push(new Date());

	db.insert(table.name, keys, row)
	.then(function () {
		winston.info("result is saved: " + JSON.stringify(maxsel));
	});
}

function select(msg, match) {
	if (allDone) {
		bot.sendMessage(msg.chat.id, "이미투표가 종료되었습니다. 모든 파이터들과 다시 투표하려면 /reset 을 눌러주세요.");
		return;
	}

	var ff = _.findWhere(ffs, { tid: msg.from.id, join: 1 });
	if (!ff) {
		return bot.sendMessage(msg.chat.id, "죄송해요. 먼저 푸드파이터로 등록해주세요. /join 을 누르시면 되요:)");
	}
 
	if (parseInt(match[0]) > fds.length) {
		return bot.sendMessage(msg.chat.id, "잘못된 번호를 선택하셨어요. 다시부탁해요 :)");
	}

	ff.select = parseInt(match[0]);
	bot.sendMessage(msg.chat.id, ff.select + "번을 선택하셨어요!");
	
	allDone = true;
	var reason =  "\n" + ff.info.name + "님이 투표했어요!\n";
	Q.all(_.map(ffs, function (ff) {
		if (!ff.select) {
			allDone = false;
		}
		console.log("select: " + msg);
		var msg = {
			chat: {
				id : ff.tid
			},
			updateReason: reason
		}

		return start(msg);
	}))
	.then(function () {
		if (allDone) {
			console.log("RESULT: ", ffs);
			result();
		}
	})
	.catch(function (err) {
		winston.info(err);
	});

}

function reset(msg, match) {
	allDone = false;
	console.log("reset: ", ffs);

	var ff = _.findWhere(ffs, { tid: msg.from.id, join: 1 });
	if (!ff) {
		return bot.sendMessage(msg.chat.id, "죄송해요. 먼저 푸드파이터로 등록해주세요. /join 을 누르시면 되요:)");
	}
	
	// reset default menu
	initDefaultFoods();

	var requester = ff.info.name;

	_.each(ffs, function (ff) {
		ff.select = 0;
		bot.sendMessage(ff.tid, requester + "님 요청으로 지금까지 없던걸로 하고 다시 선택할게요.")
		.then(function () {
			var reason = "\n다시 시작할게요! (by " + requester + ")\n";
			var msg = {
				chat: {
					id : ff.tid
				}
			}
			msg.updateReason = reason;
			start(msg);
		})
		.catch(function (err) {
			console.log(err.stack);
			bot.sendMessage(ff.tid, "죄송해요. 다시 시작할수가 없네요. ");
		});
	});
}

function foodlist(msg, match) {
	db.select('menu').then(function (foods) {
		if (!foods.length) {
			return bot.sendMessage(msg.from.id, "현재 등록된 음식이 하나도 없어요..ㅠㅠ /foodadd 를 클릭하여 음식을 추가해주세요.");
		}

		var resp = '현재 네모유엑스팀이 보유하고 있는 음식 목록이예요. 오늘 투표에 음식을 추가하고 싶다면, 음식이름 옆의 버튼(f1, f2, ..)을 클릭해주세요.\n\n';
		db.select('menu').then(function (foods) {
			_.each(foods, function (food, index) {
				resp += (index + 1) + ". " + food.name;
				resp += "(/f" + (index + 1) + ") ";
				resp += "&#9758 <a href=\"" + food.url + "\">blog</a>\n"; 
			});

			resp += "\n새 음식을 목록에 등록 하려면, /foodadd 클릭해주세요.";

			bot.sendMessage(msg.from.id, resp, {
			 	parse_mode: "HTML",
				disable_web_page_preview: true
		 	})
			.then(function () {
			})
			.catch(function (err) {
				console.log(err.stack);
			});
		});
	})
	.catch(function (err) {
		console.log(err.stack);
		return bot.sendMessage(msg.from.id, "뭔가 잘못 처리됐어요..");
	});
}

function foodadd(msg, match) {
	var chatId = msg.from.id;
	var message = "<b>아래처럼 새 음식을 입력해주세요.</b> 관련 블로그 주소는 안넣어도 되요.\n";
	
	message +=	"몬트락 meritzjun.com/220671365985\n";

	if (!_.findWhere(ffs, { tid: msg.from.id, join: 1 })) {
  	return bot.sendMessage(chatId, "먼저 푸드파이터로 등록해주세요. /join 을 입력하시거나 누르시면 참가 가능합니다.");
	}

	bot.sendMessage(chatId, message, {
    //reply_to_message_id: msg.message_id,
	 	parse_mode: "HTML",
    reply_markup: {
			force_reply: true
    }
  })
	.then(function (sended) {
		var chatId = sended.chat.id;
		var messageId = sended.message_id;

		bot.onReplyToMessage(chatId, messageId, function (message) {
			winston.info('user reply is', JSON.stringify(message));

			var data = message.text.split(' ');
			var table = _.findWhere(meta.tables, { name: "menu" });
			var keys = [];
			var row = [];

			_.each(table.keys, function (key) {
				if (!key.auto) {
					keys.push(key.name);
				}
			});

			row.push(data[0]); 
			row.push(msg.from.id); 
			if (msg.from.username) {
				row.push(msg.from.username);
			} else {
				row.push(msg.from.last_name + msg.from.first_name); 
			}
			if (data[1]) {
				row.push(data[1]);
			} else {
				row.push("http://www.naver.com");
			}
		
			row.push(''); // dummy for photo url
			row.push(1); // dummy for food type
			row.push(new Date());

			db.insert(table.name, keys, row)
			.then(function () {
				return bot.sendMessage(chatId, data[0] + ". 음식 목록에 등록되었어요. /foodlist 에서 확인해볼 수 있어요.");
			});
		});
		winston.log('success to send message');
	})
	.catch(function (err) {
		winston.log(err);
	});
}


function menuadd(msg, match) {
	var ff = _.findWhere(ffs, { tid: msg.from.id, join: 1 });
	if (!ff) {
		return bot.sendMessage(msg.chat.id, "죄송해요. 먼저 푸드파이터로 등록해주세요. /join 을 누르시면 되요:)");
	}

	db.select('menu').then(function (foods) {
		var sel = parseInt(match[1]);
		if (sel > foods.length) {
			return bot.sendMessage(msg.from.id, "요청하신 음식은 존재하지 않습니다.");
		}

		if (_.findWhere(fds, { id: foods[sel - 1].id })) {
			return bot.sendMessage(msg.from.id, "하하, 추가하려고 했던 " + foods[sel - 1].name + "가 이미 메뉴에 있네요!.");
		}

		fds.push(foods[sel - 1]);
		bot.sendMessage(msg.from.id, foods[sel - 1].name + ". 오늘의 투표 메뉴에 추가되었어요.")
		.then(function () {
			var requester =  msg.from.last_name + msg.from.first_name;
			var reason = "\n" + foods[sel - 1].name + ". 오늘의 메뉴에 추가되었어요. (by " + requester + ")\n";
			_.each(ffs, function (ff) {
				var msg = {
					chat: {
						id : ff.tid
					},
					updateReason: reason
				}
				start(msg);
			});
		})
		.catch(function (err) {
		});
	})
	.catch(function (err) {
		winston.info(err.stack);
		bot.sendMessage(msg.from.id, "아앗, 뭔가 잘못처리됐어요..");
	});
}

function getRandomInt(low, high) 
{
	return Math.floor(Math.random() * (high - low + 1) + low);
}

var initialize = function (storage) {
	if (!storage) {
		return false;
	}

	db = storage;
	bot = new telebot(token, { polling: true });
	bot.getMe().then(function (msg) {
		winston.log('bot info: ', JSON.stringify(msg));
	}).catch(function (err) {
		winston.log('bot err: ', err);
	});

	db.select('member').then(function (members) {
		_.each(members, function (member) {
			ffs.push({ tid: member.tid, msgid: 0, select: 0, join: 1, info: member });
		});
	});

	initDefaultFoods();

	bot.onText(/\/echo (.+)/, echo);
	bot.onText(/\/start/, start);
	bot.onText(/\/join/, join);
	bot.onText(/\/unjoin/, unjoin);
	bot.onText(/^([0-9]|[1-9][0-9]|[1-9][0-9][0-9])$/, select);
	bot.onText(/reset/, reset);
	bot.onText(/\/result/, result);
	bot.onText(/\/foodlist/, foodlist);
	bot.onText(/\/foodadd/, foodadd);
	bot.onText(/\/f([0-9]|[1-9][0-9]|[1-9][0-9][0-9])$/, menuadd);
	
	return true;
}

module.exports.run = initialize;
