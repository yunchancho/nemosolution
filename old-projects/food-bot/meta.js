var tables = [ 
	{ 
		name: "member", 
		keys: [
			{	name: "tid", type: "INTEGER PRIMARY KEY" },
			{	name: "name", type: "TEXT" },
			{	name: "id", type: "TEXT" },
			{	name: "createdAt", type: "DATETIME" },
		]
	},
	{ 
		name: "menu", 
		keys: [
			{	name: "id", type: "INTEGER PRIMARY KEY AUTOINCREMENT", auto: true },
			{	name: "name", type: "TEXT" },
			{	name: "tid", type: "INT" },
			{	name: "author", type: "TEXT" },
			{	name: "url", type: "TEXT" },
			{	name: "photo", type: "TEXT" },
			{	name: "type", type: "INT" },
			{	name: "createdAt", type: "DATETIME" },
		]
	},
	{ 
		name: "result", 
		keys: [
			{	name: "id", type: "INTEGER PRIMARY KEY AUTOINCREMENT", auto: true },
			{	name: "mid", type: "INT" }, // menu table's id
			{	name: "name", type: "TEXT" },
			{	name: "total", type: "INT" },
			{	name: "vote", type: "INT" },
			{	name: "createdAt", type: "DATETIME" },
		]
	},
];

module.exports = { tables: tables };
