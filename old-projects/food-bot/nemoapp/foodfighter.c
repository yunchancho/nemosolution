#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#include <showcanvas.h>
#include <showhelper.h>
#include <nemodavi.h>
#include <nemoutil/config.h>

#define SCENE_W	  800
#define SCENE_H		600

struct appcontext {
	struct nemoshow *show;
	struct nemotool *tool;
	
	// canvas
	struct showone *canvas;
	struct nemodavi *davi;
	char *pngpath;
};

static int prepare(struct appcontext *ctx)
{
	double tx, ty;
	struct nemodavi_data *data;
	struct nemodavi *davi;
	struct nemodavi_selector *sel;

	if (!ctx) {
		return -1;
	}

	tx = SCENE_W / 2;
	ty = SCENE_H / 2;

	davi = ctx->davi = nemodavi_create(ctx->canvas, NULL);

	nemodavi_append_selector(davi, "chart", NEMOSHOW_IMAGE_ITEM);
	sel = nemodavi_selector_get(davi, "chart");
	nemodavi_set_dattr(sel, "ax", 0.5f);
	nemodavi_set_dattr(sel, "ay", 0.5f);
	nemodavi_set_dattr(sel, "tx", tx);
	nemodavi_set_dattr(sel, "ty", ty);
	nemodavi_set_dattr(sel, "width", SCENE_W);
	nemodavi_set_dattr(sel, "height", SCENE_H);
	nemodavi_set_dattr(sel, "alpha", 1.0f);
	nemodavi_set_sattr(sel, "image-uri", ctx->pngpath);

	return 0;
}

static int dispatch_tap_grab(struct nemoshow *show, struct showgrab *grab, void *event)
{
	uint32_t gtag;
	struct appcontext *ctx;

 	ctx	= (struct appcontext *)nemoshow_grab_get_userdata(grab);
	gtag = nemoshow_grab_get_tag(grab);

	if (nemoshow_event_is_down(show, event) || nemoshow_event_is_up(show, event)) {
		nemoshow_event_update_taps(show, ctx->canvas, event);

		if (nemoshow_event_is_single_tap(show, event)) {
			nemoshow_view_move(show, nemoshow_event_get_serial_on(event, 0));
		} else if (nemoshow_event_is_many_taps(show, event)) {
			nemoshow_view_pick_distant(show, event, NEMOSHOW_VIEW_PICK_ALL_TYPE);
		}
	}

	if (gtag == 1 && nemoshow_event_is_single_click(show, event)) {
		uint32_t tag = nemoshow_canvas_pick_tag(ctx->canvas,
				nemoshow_event_get_x(event),
				nemoshow_event_get_y(event));

		/*
		ctx->clickx = nemoshow_event_get_x(event);
		ctx->clicky = nemoshow_event_get_y(event);

		nemodavi_emit_event(NEMODAVI_EVENT_CLICK, tag);
		*/
	}

	nemoshow_dispatch_frame(show);

	if (nemoshow_event_is_up(show, event)) {
		nemoshow_grab_destroy(grab);
		return 0;
	}

	return 1;
}

static void dispatch_canvas_event(struct nemoshow *show, struct showone *canvas, void *event)
{
	uint32_t i, serial0, serial1;
	struct appcontext *ctx = (struct appcontext *) nemoshow_get_userdata(show);

	if (nemoshow_event_is_down(show, event)) {
		struct showgrab *grab;
		grab = nemoshow_grab_create(show, event, dispatch_tap_grab);
		nemoshow_grab_set_userdata(grab, ctx);
		//nemoshow_grab_set_tag(grab, ctx->ontop == 1 ? 1 : 0);
		nemoshow_dispatch_grab(show, event);
	}
}

static struct appcontext* init_app() {
	int winw, winh, scenew, sceneh;
	float scenex, sceney, vpx, vpy;
	struct appcontext *ctx;
	struct nemotool *tool;
	struct nemoshow *show;
	struct showone *scene;
	struct showone *bgcanvas, *fcanvas;
	struct showone *one;

	config_load_get_base(&winw, &winh, NULL, NULL, NULL, NULL);
	printf("load winw: %d, winh: %d\n", winw, winh);

	winw = 800;
	winh = 600;
	ctx = (struct appcontext *)malloc(sizeof(struct appcontext));
	memset(ctx, 0, sizeof(struct appcontext));

	tool = ctx->tool = nemotool_create();
	nemotool_connect_wayland(tool, NULL);

	show = ctx->show = nemoshow_create_view(tool, winw, winh);

	nemoshow_view_set_layer(show, "service");
	nemoshow_view_put_state(show, "keypad");
	nemoshow_view_put_state(show, "sound");
	nemoshow_view_set_state(show, "layer");
 	//nemoshow_set_dispatch_layer(show, dispatch_show_layer);
	//nemoshow_set_dispatch_resize(show, dispatch_resize);

	scene = nemoshow_scene_create();
	nemoshow_scene_set_width(scene, SCENE_W);
	nemoshow_scene_set_height(scene, SCENE_H);
	nemoshow_set_scene(show, scene);

	bgcanvas = nemoshow_canvas_create();
	nemoshow_canvas_set_width(bgcanvas, SCENE_W);
	nemoshow_canvas_set_height(bgcanvas, SCENE_H);
	nemoshow_canvas_set_type(bgcanvas, NEMOSHOW_CANVAS_BACK_TYPE);
	nemoshow_canvas_set_fill_color(bgcanvas, 0xff, 0xff, 0xff, 0xff);
	nemoshow_canvas_set_alpha(bgcanvas, 1.0f);
	nemoshow_one_attach(scene, bgcanvas);

	fcanvas = ctx->canvas = nemoshow_canvas_create();
	nemoshow_canvas_set_width(fcanvas, SCENE_W);
	nemoshow_canvas_set_height(fcanvas, SCENE_H);
	nemoshow_canvas_set_type(fcanvas, NEMOSHOW_CANVAS_VECTOR_TYPE);
	nemoshow_canvas_set_dispatch_event(fcanvas, dispatch_canvas_event);
	nemoshow_canvas_set_alpha(fcanvas, 1.0);
	nemoshow_one_attach(scene, fcanvas);

	nemoshow_set_userdata(show, ctx);

	return ctx;
}

static void deinit_app(struct appcontext* ctx) {
	nemoshow_destroy_view(ctx->show);
	nemotool_disconnect_wayland(ctx->tool);
	nemotool_destroy(ctx->tool);
	free(ctx);
}

int main (int argc, char *argv[])
{
	int opt;
	char *pngpath;
	struct appcontext *ctx;
	struct option options[] = {
		{ "path",	required_argument, NULL, 'p' },
		{ 0 }
	};

	while (opt = getopt_long(argc, argv, "p:", options, NULL)) {
		if (opt == -1)
			break;

		switch (opt) {
			case 'p':
				pngpath = optarg ? strdup(optarg) : NULL;
				break;

			default:
				break;
		}
	}
	
	if (!pngpath) {
		printf("no png path\n");
		return -1;
	}

	ctx = init_app();
	ctx->pngpath = pngpath;
	printf("image path: %s\n", ctx->pngpath);
	if (!ctx) {
		printf("fail to create appcontext\n");
		return -1;
	}

	if (prepare(ctx) < 0) {
		printf("fail to prepare view\n");
		return -1;
	}

	nemoshow_dispatch_frame(ctx->show);
	nemotool_run(ctx->tool);

	deinit_app(ctx);
}
