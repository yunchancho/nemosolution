var fs = require('fs');
var Q = require('q');
var _ = require('underscore');
var sqlite3 = require('sqlite3').verbose();
var winston = require('winston');

var meta = require('./meta');
var db;

function getCreateQueryString(tableName) {
	var query = "CREATE TABLE " + tableName + " (";
	var keys = _.findWhere(meta.tables, { name: tableName }).keys;

	var count = keys.length;
	for (var i = 0; i < count; i++) {
		query += keys[i].name + ' ' + keys[i].type;
		if (i != (count - 1)) {
			query += ", ";
		}
	}
	query += ")";

	//winston.info(query);
	return query;
}

function getInsertQueryString(table, keys) {
	var query = "INSERT OR IGNORE INTO " + table  + " (";

	var count = keys.length;
	for (var i = 0; i < count; i++) {
		query += keys[i];
		if (i != (count - 1)) {
			query += ", "
		}
	}
	query += ") VALUES (";
	for (var i = 0; i < count; i++) {
		query += "?";
		if (i != (count - 1)) {
			query += ", "
		}
	}
	query += ")";

	winston.info(query);
	return query;
}

function getUpdateQueryString(table, keys, whereKeys) {
	var query = "UPDATE " + table  + " SET ";

	var count = keys.length;
	for (var i = 0; i < count; i++) {
		query += keys[i] + ' = ?';
		if (i != (count - 1)) {
			query += ", "
		}
	}
	
	query += " WHERE ";

	_.each(whereKeys, function (key, i) {
		query += key + ' = ?';
		if (whereKeys.length != i + 1) {
			query += ' AND ';
		}
	});

	//winston.info(query);
	return query;
}

function getRemoveQueryString(table, keys, row) {
	var query = "DELETE FROM " + table + " WHERE ";
	var last = _.last(keys);
	_.each(keys, function (key, i) {
		query += key + " = " + row[i];
		if (last != key) {
			query += " AND ";
		}
	});
	return query;
}

function getSelectQueryString(table, keys, row) {
	var query = "SELECT * FROM " + table;
	if (!keys || !row) {
		return query;
	}
	query += " WHERE ";
	var last = _.last(keys);
	_.each(keys, function (key, i) {
		query += key + " = " + row[i];
		if (last != key) {
			query += " AND ";
		}
	});
	return query;
}

function initializeDatabase() 
{
	db.serialize(function () {
		_.each(meta.tables, function (table) {
			var query = getCreateQueryString(table.name);
			db.run(query);
		});
	});
}

function prepare(dbpath) 
{
	var deferred = Q.defer();

	if (!dbpath) {
		return deferred.promise.thenReject("wrong path");
	}

	fs.stat(dbpath, function (err, stats) {
		db = new sqlite3.Database(dbpath);
		if (err) {
			if (err.code == 'ENOENT') {
				initializeDatabase(db);
				return deferred.resolve();
			} 
			return deferred.reject(err);
		} 
		deferred.resolve();
	});

	return deferred.promise;
}

function select(table, keys, row) 
{
	var query = getSelectQueryString(table, keys, row);
	var deferred = Q.defer();

	winston.info(query);
	db.serialize(function () {
		db.all(query, function (err, rows) {
			if (err) {
				deferred.reject(err);
			} else {
				deferred.resolve(rows);
			}
		});
	});
	return deferred.promise;
}

function insert(table, keys, row) 
{
	winston.info('insert: ' + row);
	var query = getInsertQueryString(table, keys);
	var deferred = Q.defer();

	db.serialize(function () {
		db.run("BEGIN TRANSACTION");
		var stmt = db.prepare(query);
		stmt.run(row, function (err) {
			if (err) {
				return deferred.reject(err);
			}
			deferred.resolve(this.lastID);
		});
		stmt.finalize();
		db.run("END");
	});
	return deferred.promise;
}

function remove(table, keys, row) 
{
	winston.info('remove: ' + row);
	var query = getRemoveQueryString(table, keys, row);
	var deferred = Q.defer();

	db.serialize(function () {
		db.run(query, function (err) {
			if (err) {
				deferred.reject(err);
			} else {
				deferred.resolve(this.changes);
			}
		});
	});
	return deferred.promise;
}

function finish() {
	var deferred = Q.defer();

	db.close(function (err) {
		if (err) {
			return deferred.reject(err);
		}
		return deferred.resolve();
	});

	return deferred.promise;
}

module.exports.prepare = prepare;
module.exports.insert = insert;
module.exports.remove = remove;
module.exports.select = select;
module.exports.finish = finish;
