const telebot = require('node-telegram-bot-api');
const _ = require('underscore');
const co = require("co");
const Promise = require("bluebird");
const meta = require(process.argv[2]);
const proc = require('child_process');

let bot;

function start(msg, match) {
	console.log(msg);
	var chatId = msg.from.id;
	var message = "\n&#9787<b>i am nemoux bot for ces 2017</b>&#9787\n";

	bot.sendMessage(chatId, message, { parse_mode: "HTML"});
}

function list(msg, match) {
	console.log(msg);
	var chatId = msg.from.id;
	var message = 'the followings are available:\n\n';
	for (let shell of meta.shells) {
		message += '/' + shell.name + '\n';
	}

	message += '\nClick one if you want to change shell.';
	bot.sendMessage(chatId, message, { parse_mode: "HTML"});
}

function runCmd(cmd, params) {
	return new Promise((resolve, reject) => {
		let child = proc.spawn(cmd, params, {
		 	stdio: [ 'inherit', 'inherit', 'inherit', 'ipc' ]
		}, {
			shell: true
		});

		let result = null;
		if (cmd == "killall") {
			resolve(result);
		} else if (cmd == "ste-shell") {
			setTimeout(() => child.connected? resolve(): reject(), 1000);
		} else {
			reject();
		}
	});
}


function sleep (time) {
	return new Promise((resolve) => setTimeout(resolve, time));
}

function switchShell(path) {
	console.log('running path: ', path);
	let coroutine = co.wrap(function* () {
		let resultMessage;
		try {
			yield runCmd('killall', ['-9', 'ste-shell']);
			yield sleep(meta.sleepTime);
			yield runCmd('ste-shell', ['-c', path ]);
			resultMessage = "Success!!!";
		} catch (e) {
			console.log(e);
			resultMessage = "Ooops. Fail...";
		}

		return resultMessage;
	});

	return coroutine();

}

function fivefingers(msg, match) {
	console.log(msg);
	let shell = _.findWhere(meta.shells, { name: 'fivefingers' });

	switchShell(shell.path).then((message) => { 
		bot.sendMessage(msg.from.id, message, { parse_mode: "HTML"});
	})
}

function starwars(msg, match) {
	console.log(msg);
	let shell = _.findWhere(meta.shells, { name: 'starwars' });

	switchShell(shell.path).then((message) => { 
		bot.sendMessage(msg.from.id, message, { parse_mode: "HTML"});
	})
}

function hyundai(msg, match) {
	console.log(msg);
	let shell = _.findWhere(meta.shells, { name: 'hyundai' });

	switchShell(shell.path).then((message) => { 
		bot.sendMessage(msg.from.id, message, { parse_mode: "HTML"});
	})
}

function lotte(msg, match) {
	console.log(msg);
	let shell = _.findWhere(meta.shells, { name: 'lotte' });

	switchShell(shell.path).then((message) => { 
		bot.sendMessage(msg.from.id, message, { parse_mode: "HTML"});
	})
}

function sge(msg, match) {
	console.log(msg);
	let shell = _.findWhere(meta.shells, { name: 'sge' });

	switchShell(shell.path).then((message) => { 
		bot.sendMessage(msg.from.id, message, { parse_mode: "HTML"});
	})
}

var initialize = function () {
	bot = new telebot(meta.token, { polling: true });
	bot.onText(/\/start/, start);
	bot.onText(/\/listshell/, list);
	bot.onText(/\/fivefingers/, fivefingers);
	bot.onText(/\/starwars/, starwars);
	bot.onText(/\/hyundai/, hyundai);
	bot.onText(/\/lotte/, lotte);
	bot.onText(/\/sge/, lotte);

	return true;
}

module.exports.run = initialize;
