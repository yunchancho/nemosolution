#!/bin/bash

echo "This script requires superuser access"
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

curl https://s3-us-west-2.amazonaws.com/brave-apt/keys.asc | sudo apt-key add -
echo "deb [arch=amd64] https://s3-us-west-2.amazonaws.com/brave-apt `lsb_release -sc` main" | sudo tee -a /etc/apt/sources.list.d/brave-`lsb_release -sc`.list

apt update
apt install brave

echo "succeed to set brave browser"
