#!/bin/bash

echo "This script requires superuser access"
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

wget -c wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -i google-chrome-*.deb
apt-get install -f -y

echo "succeed to set chrome browser"
