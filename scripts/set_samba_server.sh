#!/bin/bash 

samba_dir="/samba"

echo "This script requires superuser access"
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

apt-get purge -y samba samba-common
apt-get install -y samba
mkdir $samba_dir && chmod 777 $samba_dir

shared_config="[NEMO-UX]
    comment = Nemoux Device Shared Folder
    path = $samba_dir
    read only = no
    guest ok = yes
    browseable = yes
    directory mask = 0755
    create mask = 0755
    security mask = 0777
    directory security mask = 0777
    force security mode = 0
    force create mode = 0
    force directory mode = 0
    force directory security mode = 0"

echo "$shared_config" >> /etc/samba/smb.conf
systemctl restart smbd

echo "succeed to install nemoux samba"
