var fs = require('fs');
var express = require('express');
var app = express();
var google = require('googleapis');
var multer = require('multer');
var upload = multer({ storage: multer.memoryStorage({}) });
var Canvas = require('canvas');
var canvas = new Canvas(200, 200)

var Image = Canvas.Image;
var sizeOf = require('image-size');
var ctx = canvas.getContext('2d')

var visionAPI = 'https://vision.googleapis.com/';
var APIKey = 'AIzaSyBme6bE0JVCpf15BcFbS9i_4fTwehPZIfM';
var vision = require('request-json').createClient(visionAPI);

var colorList = ['red', 'yellow', 'pink', 'green', 'blue'];

var uploadImg = "./public/upload.png";
var detectImg = "./public/detect.png";

function getColor (index) {
  return colorList[index % colorList.length];
}

function draw(imgfile, outfile, opts, cb){
  fs.readFile(imgfile, function(err, squid){
    if (err) throw err;
    img = new Image;
    img.src = squid;

    if (opts && opts.filter) {
      opts.filter(ctx);
    }
    ctx.drawImage(img , 0, 0, img.width , img.height );
    ctx.globalAlpha = .5;

    var i = 0;
    function doit(vec) {
      var color = Math.random() * 1000 % 255;
      ctx.lineWidth = 4;
      ctx.beginPath();
      ctx.moveTo(vec[0].x, vec[0].y);
      ctx.lineTo(vec[1].x, vec[1].y);
      ctx.lineTo(vec[2].x, vec[2].y);
      ctx.lineTo(vec[3].x, vec[3].y);
      ctx.closePath();
      ctx.strokeStyle = getColor(i);
      i++;
      ctx.stroke();
    }

    opts.vec.forEach(function(v) {
      doit(v)
    });
    fs.writeFileSync(outfile, canvas.toBuffer());
    cb();
  });
}

app.use(express.static('public'));
//app.use(bodyParser.urlencoded({ extended: true })); 

app.get('/', function (req, res) {
  res.send('hello, world');
});

app.post('/findface', upload.single('image'), function (req, res) {
  var raw = req.file.buffer.toString('base64');
  fs.writeFile(uploadImg, raw, 'base64', function (err) {
    console.error(err);
  });

  var data = {
    requests: [
      {
        image: {
          //content: getImgBase64("./" + req.params.image)
          content: req.file.buffer.toString('base64')

        }, 
        features: [
          {
            type: "FACE_DETECTION",
            maxResults: 10
          }
        ]
      }
    ]
  };

  vision.post('v1alpha1/images:annotate?key=' + APIKey, data, function (err, response, body) {
    if (!body.responses[0].faceAnnotations) {
      return res.send('no faces');
    }
    var vectors = [];
    body.responses[0].faceAnnotations.forEach(function (annotation){
        vectors.push(annotation.boundingPoly.vertices);
    })
    console.log(vectors);

    var imgSize = sizeOf(uploadImg);
    canvas.width = imgSize.width;
    canvas.height = imgSize.height;

    draw(uploadImg, detectImg, {
      vec: vectors,
      filter: function(ctx) {
        //ctx.strokeStyle = 'rgba(255,0,0,.5)';
      }
    }, function(err) {
      if(err) {
        return res.send('failed to create image with detected face');
      }
      res.redirect('detect.png'); 
    });
  });
});

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log('example app at http://%s:%s', host, port);
});

