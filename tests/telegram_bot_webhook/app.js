var fs = require('fs');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var http = require('http');
var https = require('https');

var telebot = require('node-telegram-bot-api');

var token = '213654123:AAGDqoEVYKdUBZKvH6hf8nEjVGFJKOLChk4';

var bot = new telebot(token, { polling: false });

bot.getMe()
.then(function (msg) {
	console.log('bot info: ', JSON.stringify(msg));
})
.catch(function (err) {
	console.log('bot err: ', err);
});

bot.onText(/\/echo (.+)/, function (msg, match) {
	console.log('/echo', msg);
	var fromId = msg.from.id;
	var resp = match[1];

	bot.sendMessage(fromId, resp, {
    reply_to_message_id: msg.message_id,
    reply_markup: {
      one_time_keyboard : true,
      keyboard : [[{ text: 'hello' }, { text: 'world'}, { text: 'yunchan' }], [{ text: "1" }, { text: "2" }]]
    }
  })
	.then(function () {
		console.log('success to send message');
	})
	.catch(function (err) {
		console.log(err);
	});
});

bot.onText(/\/list (.+)/, function (msg, match) {
	console.log('/list', msg);
	console.log('/list(match)', match);
	var fromId = msg.from.id;
	var resp = "this is list";

	bot.sendMessage(fromId, resp)
	.then(function () {
		console.log('success to send message');
	})
	.catch(function (err) {
		console.log(err);
	});
});

bot.on('text', function (msg) {
  console.log('text event');
});

bot.on('document', function (msg) {
  console.log('document event');
});

bot.on('photo', function (msg) {
  console.log('photo event');
});

bot.on('video', function (msg) {
  console.log('video event');
});

bot.on('voice', function (msg) {
  console.log('voice event');
});

bot.on('audio', function (msg) {
  console.log('audio event');
});

bot.on('sticker', function (msg) {
  console.log('sticker event');
});

app.use('/', express.static(__dirname + '/public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// telegram bot send requests to us using only POST method
app.post('/bot', function (req, res) {
  console.log('post ok');
  console.log(JSON.stringify(req.body));

  bot.processUpdate(req.body);
  res.send("post ok");
});

http.createServer(app).listen(80);
https.createServer({
  key: fs.readFileSync('./certificates/private.key'),
  cert: fs.readFileSync('./certificates/public.pem')
}, app).listen(443);
