# creating self signed certificate for telegram webhook
openssl req -newkey rsa:2048 -sha256 -nodes -keyout private.key -x509 -days 365 -out public.pem -subj "/C=US/ST=New York/O=Nemoux/CN=ec2-52-196-193-209.ap-northeast-1.compute.amazonaws.com"

# setting webhook with my self signed certificates 
curl -F "url=https://ec2-52-196-193-209.ap-northeast-1.compute.amazonaws.com/bot" -F "certificate=@public.pem" https://api.telegram.org/bot213654123:AAGDqoEVYKdUBZKvH6hf8nEjVGFJKOLChk4/setWebhook
